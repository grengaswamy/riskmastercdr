/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 * 01/28/2010 | SI06650 |    JTC     | Fix for reading .Config file from a web site
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using CCP.Security.Encryption;
using System.IO;
using System.Configuration; //SIN204

namespace CCP.Common
{
    public class Parameters
    {
        private Dictionary<string, string> m_ParamList;
        private Debug m_Dbg;
        private DTGCrypt32 m_Crypto;
        // Start SI06023
        private CommonFunctions m_Funcs;
        private string mvarParameterFile;
        private string mvarCommandLine;
        private string m_CommentIndicator;
        private bool m_ClearOnParse;
        private bool b;
        private string m_CCStart;
        private string m_CCEnd;
        private string m_ReplacementCharacter;
        private string m_EncryptionCharacter;
        private string m_FunctionCharacter;
        private string m_FunctionSplitChar;
        private string m_SpaceCharacter;
        private string m_LabelQualifier;
        private string[] m_Counter;

        public Parameters()
        {
            m_ParamList = new Dictionary<string, string>();
            m_ClearOnParse = true;
            m_CommentIndicator = Constants.sCommentDelimiter;
            m_ReplacementCharacter = Constants.sReplacementCharacter;
            m_EncryptionCharacter = Constants.sEncryptionCharacter;
            m_FunctionCharacter = Constants.sFunctionCharacter;
            m_FunctionSplitChar = Constants.sFunctionSplitChar;
            m_SpaceCharacter = Constants.sSpaceCharacter;
            m_CCStart = Constants.sCCStart;
            m_CCEnd = Constants.sCCEnd;
            //Start SIN204
            Array.Resize<string>(ref m_Counter, 1);
            //m_Counter.SetValue(0, 0);
            m_Counter.SetValue((0).ToString(), 0);
            //End SIN204
            //m_Double[0] = 0;
            //m_UseIndxQual = false;
            m_LabelQualifier = Constants.sLabelQualifier;
            m_Crypto = null;
            m_Dbg = null;

            // End SI06023

            //src[0] = Constants.sRplcDate;
            //src[1] = Constants.sRplcTime;
            //src[2] = Constants.sRplcAppName;
            //src[3] = Constants.sRplcAppPath;
            //src[4] = Constants.sRplcAppVrsn;
            //src[5] = Constants.sRplcDatabase;
            //src[6] = Constants.sRplcSwitch;
            //src[7] = Constants.sRplcGUID;
            //src[8] = Constants.sRplcIncrement;
            //src[9] = Constants.sRplcDecrement;
            //src[10] = Constants.sRplcCounter;
            //src[11] = Constants.sRplcQuote;
            //src[12] = Constants.sRplcIniValue;
        }

        public Debug Debug
        {
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.Parameters = this;
                }
                return m_Dbg;
            }
            set
            {
                if ((m_Dbg != null) && (value != null) && (m_Dbg.DebugGUID == value.DebugGUID))
                    return;
                if (value == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.Parameters = this;
                }
                else
                    m_Dbg = value;
            }
        }
        //Start SI06023
        public CommonFunctions CommonFunctions
        {
            get
            {
                if (m_Funcs == null)
                {
                    m_Funcs = new CommonFunctions();
                    m_Funcs.Debug = this.Debug;
                }
                return m_Funcs;
            }
            set
            {
                m_Funcs = value;
                if (m_Funcs != null)
                    m_Funcs.Debug = this.Debug;
            }
        }

        
        public string Parameter(string sSwitch)
        {
            string prmValue;
            prmValue = ParamValue(sSwitch);
            if (StringUtils.Left(prmValue, 1) == m_EncryptionCharacter)
            {
                prmValue = StringUtils.Mid(prmValue, 2);
                prmValue = Crypto.DecryptString(prmValue, PrivateConstants.CRYPTKEY);
            }
            return prmValue;
        }
        public void Parameter(string sSwitch, string val)
        {
            string prmValue;
            if (StringUtils.Left(val, 1) == m_EncryptionCharacter)
            {
                prmValue = Crypto.EncryptString(StringUtils.Mid(val, 2), PrivateConstants.CRYPTKEY);
                val = m_EncryptionCharacter + prmValue;
            }
            val = ValueParms(val);
            val = processFunction(val);
            ParamValue(sSwitch, val);
        }
        public string ParamValue(string sSwitch)
        {
            string s;
            s = FormatSwitch(sSwitch);
            if (m_ParamList.ContainsKey(s))
                return m_ParamList[s].ToString();
            else
                return "";
        }
        public void ParamValue(string sSwitch, string val)
        {
            string s;
            s = FormatSwitch(sSwitch);
            if (m_ParamList.ContainsKey(s))
                m_ParamList[s] = val;
            else
                m_ParamList.Add(s, val);
        }
        // End SI06023
        public DTGCrypt32 Crypto
        {
            get
            {
                if (m_Crypto == null)
                    m_Crypto = new DTGCrypt32();
                return m_Crypto;
            }
        }
        // start SI06023
        public string ReplacementCharacter
        {
            get
            {
                return m_ReplacementCharacter;
            }
            set
            {
                m_ReplacementCharacter = value;
                if (m_ReplacementCharacter == "")
                    m_ReplacementCharacter = Constants.sReplacementCharacter;
                m_ReplacementCharacter = StringUtils.Left(m_ReplacementCharacter, 1);
            }
        }
        public string FunctionCharacter
        {
            get { return m_FunctionCharacter; }
            set
            {
                m_FunctionCharacter = value;
                if (m_FunctionCharacter == "")
                    m_FunctionCharacter = Constants.sFunctionCharacter;

                m_FunctionCharacter = StringUtils.Left(m_FunctionCharacter, 1);
            }
        }
        public string FunctionSplitCharacter
        {
            get { return m_FunctionSplitChar; }
            set
            {
                m_FunctionSplitChar = value;
                if (m_FunctionSplitChar == "")
                    m_FunctionSplitChar = Constants.sFunctionSplitChar;

                m_FunctionSplitChar = StringUtils.Left(m_FunctionSplitChar, 1);
            }
        }
        public string EncryptionCharacter
        {
            get { return m_EncryptionCharacter; }
            set
            {
                m_EncryptionCharacter = value;
                if (m_EncryptionCharacter == "")
                    m_EncryptionCharacter = Constants.sEncryptionCharacter;

                m_EncryptionCharacter = StringUtils.Left(m_EncryptionCharacter, 1);
            }
        }

        public string SpaceCharacter
        {
            get { return m_SpaceCharacter; }
            set
            {
                m_SpaceCharacter = value;
                if (m_SpaceCharacter == "")
                    m_SpaceCharacter = Constants.sSpaceCharacter;
            }
        }
        public string ControlCharacters
        {
            get
            {
                return m_CCStart + m_CCEnd;
            }
            set
            {
                if (value == "")
                {
                    m_CCStart = Constants.sCCStart;
                    m_CCEnd = Constants.sCCEnd;
                }
                else
                {
                    m_CCStart = value.Substring(0, 1);
                    m_CCEnd = value.Substring(value.Length - 2, 1);
                }
            }
        }
        public string CommentDelimiter
        {
            get
            {
                return m_CommentIndicator;
            }
            set
            {
                m_CommentIndicator = value;
                if (m_CommentIndicator == "")
                    m_CommentIndicator = Constants.sCommentDelimiter;
            }
        }
        public string LabelQualifier
        {
            get
            {
                return m_LabelQualifier;
            }
            set
            {
                m_LabelQualifier = value;
                if (m_LabelQualifier == "")
                    m_LabelQualifier = Constants.sLabelQualifier;
                if (StringUtils.Left(m_LabelQualifier, 1) != ".")
                    m_LabelQualifier = "." + m_LabelQualifier;
            }
        }

        //Start SIN204
        public string DatabaseSW
        {
            get { return Constants.sSwitchDatabase; }
        }
        //End SIN204

        public string DataSource
        {
            get
            {
                return Parameter(Constants.sSwitchDataSource); 
            }
            set
            {
                Parameter(Constants.sSwitchDataSource, value); 
            }
        }
        public void RemoveParameter(string sSwitch)
        {
            string s;
            s = FormatSwitch(sSwitch);
            if (m_ParamList.ContainsKey(s))
                m_ParamList.Remove(s);
        }
        public bool IsBool(string sSwitch)
        {
            return Debug.CommonFunctions.IsBool(Parameter(sSwitch));
        }
        public bool IsTrue(string sSwitch)
        {
            if (IsSwitch(sSwitch))
                return !Debug.CommonFunctions.IsFalse(Parameter(sSwitch));
            else
                return false;
        }
        public bool IsSwitch(string sSwitch)
        {
            string s;
            s = FormatSwitch(sSwitch);
            return m_ParamList.ContainsKey(s);
        }

        /*public void RemoveParameter(string sSwitch)
        {
            string s;
            s = FormatSwitch(sSwitch);
            if (m_ParamList.Exists(s))
                m_ParamList.Remove(s);
        }*/
        public bool ClearLog
        {
            get { return IsTrue(Constants.sSwitchClearLog); }
            set
            {
                if (value.ToString() == "")
                    value = true;

                Parameter(Constants.sSwitchClearLog, value.ToString());
            }
        }
        public bool NoRead
        {
            get
            {
                return IsTrue(Constants.sSwitchNoRead);
            }
            set
            {
                //SIN204 if (value.ToString() == "")
                //SIN204 {
                //SIN204     value = true;
                Parameter(Constants.sSwitchNoRead, value.ToString());
                //SIN204 }
            }
        }
        //SIN204 public bool NoReadSW
        public string NoReadSW  //SIN204
        {
            get
            {
                //SIN204 return Convert.ToBoolean(Constants.sSwitchNoRead);
                return Constants.sSwitchNoRead; //SIN204
            }
        }
        public bool NoMigrate
        {
            get
            {
                return IsTrue(Constants.sSwitchNoMigrate);
            }
        }
        public bool ClearWhenParsing
        {
            get
            {
                //SIN204 ClearWhenParsing = m_ClearOnParse;
                //SIN204 return ClearWhenParsing;
                return m_ClearOnParse;  //SIN204
            }
            set
            {
                m_ClearOnParse = b;
            }
        }
        public void PreserveOnParse()
        {
            m_ClearOnParse = false;
        }
        public bool ReadParams
        {
            get { return IsTrue(Constants.sSwitchReadParams); }
            set
            {
                if (value.ToString() == "")
                    Parameter(Constants.sSwitchReadParams, value.ToString());
            }
        }
        public String ReadParamsSW
        {
            get { return Constants.sSwitchReadParams; }
        }
        public string SaveParamsValue
        {
            get
            {
                if (!IsBool(Constants.sSwitchSaveParams))
                    return Parameter(Constants.sSwitchSaveParams);
                else
                    return "";
            }
            set
            { Parameter(Constants.sSwitchSaveParams, value); }
        }
        public bool SaveParams
        {
            get
            {
                return IsTrue(Constants.sSwitchSaveParams);
            }
            set
            {
                if (value.ToString() == "") value = true;
                Parameter(Constants.sSwitchSaveParams, value.ToString());
            }
        }
        public string SaveParamsSW
        {
            get
            {
                return Constants.sSwitchSaveParams;
            }
        }
        public string Parse(string cmdline)
        {
            string strCmdLine;
            string[] arrCmdOptions;
            int intNumOptions, idx, jdx;
            string qstr;
            Debug.PushProc("Parameters.Parse");

            if (cmdline != "" && cmdline != null)
                CommandLine = cmdline;

            if (m_ClearOnParse) Clear();
            strCmdLine = CommandLine;
            char chr = Convert.ToChar(34);
            idx = StringUtils.InStrPos(strCmdLine, chr.ToString(), 1);
            //SIN204 while (idx != 0)
            while (idx != -1)   //SIN204
            {
                jdx = StringUtils.InStrPos(strCmdLine, chr.ToString(), idx + 1);
                //SIN204 if (jdx == 0)
                if (jdx == -1)  //SIN204
                    jdx = strCmdLine.Length;
                qstr = StringUtils.Mid(strCmdLine, idx, jdx - idx + 1);
                qstr = qstr.Replace(" ", SpaceCharacter);
                qstr = qstr.Replace(chr.ToString(), "");
                qstr = qstr.Trim();
                strCmdLine = StringUtils.Left(strCmdLine, idx - 1) + qstr + StringUtils.Mid(strCmdLine, jdx + 1);
                jdx = jdx - 2;
                idx = StringUtils.InStrPos(strCmdLine, chr.ToString(), jdx + 1);
            }
            arrCmdOptions = strCmdLine.Split(' ');
            intNumOptions = StringUtils.UBound(arrCmdOptions);

            for (idx = 0; idx < intNumOptions; idx++)
            {
                putParam(arrCmdOptions.GetValue(idx).ToString(), m_ParamList);
            }
            forwardReferences(m_ParamList);
            processFunctions(m_ParamList);

            if (IsTrue(Constants.sSwitchDumpParams))
                DumpParms();
            Debug.PopProc();
            return m_ParamList.Count.ToString();
        }
        public string InternalCounter(int indx)
        {
            if (indx < 0) indx = 0;
            if (indx > StringUtils.UBound(m_Counter))
                m_Counter.SetValue(m_Counter.GetValue(indx).ToString(), indx);
            return m_Counter.GetValue(indx).ToString();
        }
        public void InternalCounter(int indx, string value)
        {
            if (indx < 0) indx = 0;
            if (indx > StringUtils.UBound(m_Counter))
                m_Counter.SetValue(m_Counter.GetValue(indx).ToString(), indx);
            m_Counter.SetValue(Debug.CommonFunctions.CnvData(value, CommonFunctions.ufDataTypes.ufdtLong), indx);
        }
        private void putParam(string strParam, Dictionary<string, string> paramList)
        {
            string[] arrCmdOption;
            string strParameter, strSwitch, strValue;
            int indx, iIndxQual;
            CommonFunctions cFunc;

            cFunc = new CommonFunctions();
            strParameter = strParam;
            if (strParameter != "" && StringUtils.Left(strParameter, 1) != "*")
            {
                strParameter = ValueParms(strParameter);

                strParameter = Debug.CommonFunctions.ReplaceString(strParameter, SpaceCharacter.ToUpper(), " ", 1, 999);
                strParameter = Debug.CommonFunctions.ReplaceString(strParameter, m_CCStart + Constants.sCR + m_CCEnd, (Convert.ToChar(13)).ToString(), 1, 999);
                strParameter = Debug.CommonFunctions.ReplaceString(strParameter, m_CCStart + Constants.sLF + m_CCEnd, (Convert.ToChar(10)).ToString(), 1, 999);
                strParameter = Debug.CommonFunctions.ReplaceString(strParameter, m_CCStart + Constants.sCRLF + m_CCEnd, (Convert.ToChar(13)).ToString() + (Convert.ToChar(10)).ToString(), 1, 999);
                strParameter = Debug.CommonFunctions.ReplaceString(strParameter, m_CCStart + Constants.sTAB + m_CCEnd, (Convert.ToChar(9)).ToString(), 1, 999);

                arrCmdOption = strParameter.Split('=');
                strSwitch = arrCmdOption.GetValue(0).ToString().ToUpper();

                if (StringUtils.UBound(arrCmdOption) > 0)
                    strValue = arrCmdOption.GetValue(1).ToString();
                else
                    strValue = "";

                strSwitch = strSwitch.Trim();
                strValue = strValue.Trim();

                if (strSwitch == "")
                    return;

                if (StringUtils.Left(strSwitch, 1) == "/")
                {
                    if (!NoMigrate)
                    {
                        switch (strSwitch)
                        {
                            case Constants.sSwitchInputPath:
                                strSwitch = Constants.sSwitchInPath;
                                break;
                            case Constants.sSwitchInputFile:
                                strSwitch = Constants.sSwitchInFile;
                                break;
                        }
                    }
                    //SIN204 }

                    ParamValue(strSwitch, strValue);
                    //SIN204 if (paramList.ContainsKey(strSwitch))
                    //SIN204     paramList[strSwitch] = strValue;
                    //SIN204 else
                    //SIN204     paramList.Add(strSwitch, strValue);

                    if (StringUtils.Left(strSwitch, 6) == "/PARAM")
                    {
                        string sSW;
                        iIndxQual = Index;
                        Index = 0;
                        sSW = strSwitch;
                        indx = GetParamValue(ref sSW);
                        switch (sSW)
                        {
                            case Constants.sSwitchComment:
                                if (Parameter(strSwitch) != "")
                                    CommentDelimiter = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchCounter:
                                InternalCounter(indx, Parameter(strSwitch));
                                break;
                            case Constants.sSwitchRplcChar:
                                ReplacementCharacter = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchClear:
                                foreach (string key in paramList.Keys)
                                {
                                    paramList.Remove(key);
                                }
                                break;
                            case Constants.sSwitchEncryptChar:
                                EncryptionCharacter = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchSpaceChar:
                                SpaceCharacter = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchCntrlChar:
                                ControlCharacters = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchLabelQual:
                                LabelQualifier = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchFuncChar:
                                FunctionCharacter = Parameter(strSwitch);
                                break;
                            case Constants.sSwitchFuncSplit:
                                FunctionSplitCharacter = Parameter(strSwitch);
                                break;
                        }
                        Index = iIndxQual;
                    }
                    else if (strSwitch == Constants.sSwitchReadParams)
                    {
                        if (ReadParamsValue != "")
                            ReadParamFile(ReadParamsValue);
                    }
                    else if (strSwitch == Constants.sSwitchSaveParams)
                    {
                        if (SaveParamsValue != "")
                            SaveParamFile(SaveParamsValue);
                    }
                }	//SIN204
                else
                    ReadParamFile(strSwitch);
            }
        }
        private int GetParamValue(ref string s)
        {
            string v;
            int i;
            string w;
            string X;
            if (s == "")
                i = 0;
            else if (s.Length <= 2)
                i = 0;
            //SIN204 else if (StringUtils.InStrPos(s, "(", 1) == 0 || StringUtils.Right(s, 1) != ")")
            else if (StringUtils.InStrPos(s, "(", 1) == -1 || StringUtils.Right(s, 1) != ")")   //SIN204
                i = 0;
            else
            {
                i = StringUtils.InStrPos(s, "(", 1);
                w = StringUtils.Left(s, i - 1);
                X = StringUtils.Mid(s, i);
                v = StringUtils.Mid(X, 2, X.Length - 2);
                i = Convert.ToInt32(Debug.CommonFunctions.CnvData(v, CommonFunctions.ufDataTypes.ufdtInteger));
                s = w;
            }
            return i;
        }
        public string ValueParms(string str)
        {// pending
            int sid;
            int xid;
            int lID;
            int indx;
            string snew;
            string sadd ="";
            string snme;
            string scur;
            string snxt;
            string sChr="";
            int iLenRplcChr;
            GUIDGenerator m_GUIDGen;


            snew = str;
            // Logic restructured to react to replacement chacters in string rather
            //then search for all possibilities
            iLenRplcChr = m_ReplacementCharacter.Length;
            //SIN204 sid = StringUtils.InStrPos(snew, m_ReplacementCharacter, 1);
            sid = StringUtils.InStrPos(snew, m_ReplacementCharacter, 0);    //SIN204
            //SIN204 while (sid != 0)
            while (sid != -1)	//SIN204
            {
                sChr = StringUtils.Mid(snew, sid + iLenRplcChr, 1);
                snxt = StringUtils.Mid(snew, sid + iLenRplcChr + 1, 1);
                scur = snew;
                sadd = "";
                switch (sChr.ToUpper())
                {
                    case Constants.sReplacementCharacter:
                        sadd = m_ReplacementCharacter;
                        break;
                    case Constants.sRplcDate:
                        sadd = DateTime.Now.Date.ToString("yyyymmdd");
                        break;
                    case Constants.sRplcTime:
                        sadd = DateTime.Now.TimeOfDay.ToString();
                        break;
                    case Constants.sRplcAppName:
                        sadd = Debug.AppTitle;
                        break;
                    case Constants.sRplcAppPath:
                        sadd = Debug.AppPath;
                        break;
                    case Constants.sRplcAppVrsn:
                        sadd = Debug.AppVersion;
                        break;
                    case Constants.sRplcDatabase:
                        sadd = Database;
                        break;
                    case Constants.sRplcGUID:
                        m_GUIDGen = new GUIDGenerator();
                        sadd = m_GUIDGen.CreateGUID();
                        m_GUIDGen = null;
                        break;
                    case Constants.sRplcIncrement:
                    case Constants.sRplcDecrement:
                    case Constants.sRplcCounter:
                        string sSW;
                        int X;
                        sSW = sChr;
                        if (snxt == "(")
                        {
                            X = StringUtils.InStrPos(snew, ")", sid + iLenRplcChr + 2);
                            //SIN204 if (X != 0)
                            if (X != -1)    //SIN204
                            {
                                snxt = StringUtils.Mid(snew, sid + iLenRplcChr + 1, X - (sid + iLenRplcChr));
                                sChr = sChr + snxt;
                            }
                        }
                        indx = GetParamValue(ref snxt);
                        switch (sSW)
                        {
                            case Constants.sRplcIncrement:
                                InternalCounter(indx, Convert.ToString(Convert.ToInt32(InternalCounter(indx)) + 1));
                                sadd = m_Counter.GetValue(indx).ToString();
                                break;
                            case Constants.sRplcDecrement:
                                InternalCounter(indx, Convert.ToString(Convert.ToInt32(InternalCounter(indx)) - 1));
                                sadd = m_Counter.GetValue(indx).ToString();
                                break;
                            case Constants.sRplcCounter:
                                sadd = InternalCounter(indx).ToString();
                                break;
                        }
                        break;
                    case Constants.sRplcWSName:
                        sadd = Debug.CommonFunctions.ComputerName();
                        break;
                    case Constants.sRplcLangCode:
                        sadd = Debug.CommonFunctions.WSLanguageCode;
                        break;
                    case Constants.sRplcCharacter:
                        // #Cnnn# Replace nnn with character value
                        xid = StringUtils.InStrPos(snew, m_ReplacementCharacter, sid + iLenRplcChr);
                        lID = iLenRplcChr + 1;
                        //SIN204 if (xid == 0) xid = snew.Length + 1;
                        if (xid == -1) xid = snew.Length + 1;   //SIN204
                        if (xid > sid)
                        {
                            snme = StringUtils.Mid(snew, sid + lID, xid - sid - lID);
                            sChr = sChr + snme + m_ReplacementCharacter;
                            sadd = Convert.ToChar(Convert.ToInt32(snme)).ToString();
                        }
                        break;
                    case Constants.sRplcASCII:
                        // #Ac Replace c with its ASCII value
                        snme = StringUtils.Mid(snew, sid + iLenRplcChr + 1, 1);
                        sChr = sChr + snme;
                        sadd = Convert.ToInt32(snme).ToString();
                        break;
                    case Constants.sRplcSwitch:
                        // More complicated, extracts the swich name
                        xid = StringUtils.InStrPos(snew, m_ReplacementCharacter, sid + iLenRplcChr);
                        lID = iLenRplcChr + 1;
                        //SIN204 if (xid == 0) xid = snew.Length + 1;
                        if (xid == -1) xid = snew.Length + 1;   //SIN204
                        if (xid > sid)
                        {
                            snme = StringUtils.Mid(snew, sid + lID, xid - sid - lID);
                            sChr = sChr + snme + m_ReplacementCharacter;
                            sadd = Parameter(snme);
                        }
                        break;
                    case Constants.sRplcQuote:
                        sadd = Constants.sQuote;
                        break;
                    case Constants.sRplcIniValue:
                        // Replaces the switch value with a read to system settings.
                        //#IGroupname&TagName[&FileName]
                        string[] iniItem;
                        //Set sysInfo = New CCPUF.SystemInfo                       'SI05869
                        xid = StringUtils.InStrPos(snew, m_ReplacementCharacter, sid + 1);
                        lID = iLenRplcChr + 1;
                        //SIN204 if (xid == 0) xid = snew.Length + 1;
                        if (xid == -1) xid = snew.Length + 1;   //SIN204
                        if (xid > sid)
                        {
                            snme = StringUtils.Mid(snew, sid + lID, xid - sid - lID);
                            sChr = sChr + snme + m_ReplacementCharacter;
                            iniItem = snme.Split('&');
                            if (StringUtils.UBound(iniItem) >= 1)
                            {
                                if (StringUtils.UBound(iniItem) > 1)
                                    sadd = Debug.SystemInfo.GetSystemInfo(iniItem.GetValue(0).ToString(), iniItem.GetValue(1).ToString(), iniItem.GetValue(2).ToString());
                                else
                                    sadd = Debug.SystemInfo.GetSystemInfo(iniItem.GetValue(0).ToString(), iniItem.GetValue(1).ToString(), "");
                            }
                        }
                        break;
                }
                //SIN204 }
                if (sadd != "")
                    snew = Debug.CommonFunctions.ReplaceString(snew, m_ReplacementCharacter + sChr, sadd, 1, 999);
                sid = StringUtils.InStrPos(snew, m_ReplacementCharacter, sid + iLenRplcChr);
            }   //SIN204
            return snew;
        }
        public void ReadParamFile(string paramFile)
        {
            FileFunctions m_FLFunc;

            try
            {
                string strInputLine;
                string spf;
                int idx;

                Debug.PushProc("CParameters.ReadParamFile", paramFile, ParamFileParameter, Type.Missing, Type.Missing);
                m_FLFunc = new FileFunctions();
                m_FLFunc.CDebug = Debug;

                //Use this option to ignore all input parameter files.
                //Switch is effective when encountered in the input data stream
                //If IsSwitch(NoReadSW) And NoRead Then
                if (IsSwitch(ReadParamsSW))
                {
                    if (!ReadParams)
                        return;
                }

                spf = paramFile;
                if (StringUtils.Left(spf.ToLower(), 4) == "temp" || StringUtils.Left(spf.ToLower(), 3) == "tmp")
                    m_FLFunc.GetTempFileName(spf, "");
                else if (spf == "")
                    spf = ParamFileParameter;

                //SIN204 if (spf == "")
                    //SIN204 spf = m_FLFunc.JoinFileName(Debug.AppPath, Debug.AppTitle + ".dat");


                //SIN204 if (spf != "")
                ParameterFile = spf;
                
                //Start SIN204
                if (ParameterFile == "")
                {
                    ReadConfigFile(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));
                }
                else if (ParameterFile.EndsWith(".CONFIG", StringComparison.CurrentCultureIgnoreCase))
                {
                    ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                    //SI06650 configFileMap.ExeConfigFilename = ParameterFile;
                    configFileMap.ExeConfigFilename = Debug.FileFunctions.JoinFileName(Debug.AppPath, ParameterFile);   //SI06650
                    ReadConfigFile(ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None));
                }
                else
                {   //End SIN204
                    if (m_FLFunc.FileOpen("", spf, FileFunctions.cFileAccess.enFAForReading, false) != null)
                    {
                        StreamReader textStream = (StreamReader)m_FLFunc.FileTextStream;
                        while (!textStream.EndOfStream)
                        {
                            strInputLine = m_FLFunc.FileRead();
                            idx = StringUtils.InStrPos(strInputLine, m_CommentIndicator, 1);
                            //SIN204 if (idx > 0)
                            if (idx > -1)   //SIN204
                                strInputLine = StringUtils.Left(strInputLine, idx - 1).Trim();
                            strInputLine = Debug.CommonFunctions.RemoveString(strInputLine, Convert.ToChar(9).ToString(), 1, 999);
                            putParam(strInputLine, m_ParamList);
                        }
                        m_FLFunc.FileClose();
                        Debug.LogEntry("Input Parameter File: '" + spf + "' Processed");
                    }
                }   //SIN204
                forwardReferences(m_ParamList);
                processFunctions(m_ParamList);
            }
            catch (Exception e)
            {
                string exc = e.Message;
                return;
                //string response = Debug.Errors.ProcessError(1,CCP.Constants.ErrorGlobalConstants.ERRC_PARAM_FILE_WRITE_GENERAL,e);
                //Select Case response
                //   Case vbIgnore
                //      Resume Next
                //   Case vbRetry
                //      Resume 0
                //End Select
                //Resume sspf_exit
            }
            finally
            {
                m_FLFunc = null;
                Debug.PopProc();
            }
        }

        //Start SIN204
        private void ReadConfigFile(Configuration config)
        {
            string strInputLine;
            foreach (KeyValueConfigurationElement configParam in config.AppSettings.Settings)
            {
                strInputLine = configParam.Key + "=" + configParam.Value;
                putParam(strInputLine, m_ParamList);
            }
        }
        //End SIN204

        private int SwitchIndexValue(string s)
        {
            int idx;
            int i;
            string str;
            idx = 0;
            if (StringUtils.Right(s, 1) == ")")
            {
                i = StringUtils.InStrRev(s, "(");
                if (i > 0)
                {
                    str = StringUtils.Mid(s, i + 1, s.Length - i - 1);    //switch(nn) : i=8, len(s)=11
                    idx = Convert.ToInt32(Debug.CommonFunctions.CnvData(str, CommonFunctions.ufDataTypes.ufdtInteger));
                }
            }
            return idx;
        }
        public void SaveParamFile(string spf)
        {
            FileFunctions m_FLFunc;
            try
            {
                bool bAll;
                string sPArray = "";
                int i;
                int j;
                bool bSave;
                int iSaveIdx;
                string sTemp;
                FileFunctions.cFileAccess iFileOW;

                Debug.PushProc("CParameters.SaveParamFile", spf, SaveParams, Type.Missing, Type.Missing);
                m_FLFunc = new FileFunctions();
                m_FLFunc.CDebug = Debug;

                //If the SaveParams switch is specifically off, then skip this process
                if (IsSwitch(SaveParams.ToString()) && !SaveParams)
                    return;

                if (StringUtils.Left(spf.ToLower(), 4) == "temp" || StringUtils.Left(spf.ToLower(), 3) == "tmp")
                    m_FLFunc.GetTempFileName(spf, "");
                else if (spf == "")
                    spf = ParamFileParameter;

                if (spf == "")
                    spf = m_FLFunc.JoinFileName(Debug.AppPath, Debug.AppTitle + ".dat");

                //m_FLFunc.OverWriteFile = Me.OverWriteParamFile;
                if (Parameter(Constants.sSwitchParamFileOW).ToUpper() == "APPEND")
                    iFileOW = FileFunctions.cFileAccess.enFAForAppending;
                else
                    iFileOW = FileFunctions.cFileAccess.enFAForWriting; ;

                if (m_FLFunc.FileOpen("", spf, iFileOW, true) != null)
                {
                    m_FLFunc.FileWrite("* Application:" + Debug.AppTitle + ":" + Debug.AppVersion);
                    m_FLFunc.FileWrite("* Parameter File Saved:" + DateTime.Now.Date.ToString("yyyymmdd") + " " + DateTime.Now.TimeOfDay.ToString());

                    if (ParameterFile != "" && ParamFileParameter == "")
                        ParamFileParameter = ParameterFile;

                    //If specific values are passed in the SaveParams switch, only those values are
                    //saved to the target file.
                    iSaveIdx = -1;
                    //SIN204 if (!IsSwitch(SaveParamsSW) || IsSwitch(SaveParamsSW) && Parameter(SaveParamsSW) == "" || StringUtils.InStrPos("ALL:TRUE:YES:1:ON", Parameter(SaveParamsSW).ToUpper(), 1) > 0)
                    if (!IsSwitch(SaveParamsSW) || IsSwitch(SaveParamsSW) && Parameter(SaveParamsSW) == "" || StringUtils.InStrPos("ALL:TRUE:YES:1:ON", Parameter(SaveParamsSW).ToUpper(), 1) > -1) //SIN204
                        bAll = true;
                    else
                    {
                        bAll = false;
                        sPArray = ":" + Parameter(SaveParamsSW).ToUpper() + ":";
                        i = StringUtils.InStrPos(sPArray, "INDEX=", 1);
                        //SIN204 if (i > 0)
                        if (i > -1) //SIN204
                        {
                            j = StringUtils.InStrPos(sPArray, ":", i);
                            //SIN204 if (j == 0) j = sPArray.Length;
                            if (j == -1) j = sPArray.Length;    //SIN204
                            sTemp = StringUtils.Mid(sPArray, i + 6, j - i - 6); //a:b:INDEX=45:C:D'
                            iSaveIdx = Convert.ToInt32(Debug.CommonFunctions.CnvData(sTemp, CommonFunctions.ufDataTypes.ufdtInteger));
                            sPArray = StringUtils.Left(sPArray, i - 1) + StringUtils.Mid(sPArray, j);
                            sPArray = sPArray.Trim();
                            if (sPArray == "::")
                                sPArray = "";
                            Index = iSaveIdx;
                        }
                    }
                    foreach (string key in m_ParamList.Keys)
                    {
                        bSave = true;
                        if (m_ParamList[key] == "")
                            bSave = false;
                        else if (!bAll)//Save indicated index or named parameters
                        {
                            if (iSaveIdx >= 0 && iSaveIdx != SwitchIndexValue(key))
                                bSave = false;
                            //SIN204 if (sPArray != "" && StringUtils.InStrPos(sPArray, ":" + key + ":", 1) == 0)
                            if (sPArray != "" && StringUtils.InStrPos(sPArray, ":" + key + ":", 1) == -1)   //SIN204
                                bSave = false;
                            if (key == IndexSW)
                                bSave = true;
                        }
                        if (bSave)
                        {
                            if (m_ParamList[key] != "")
                                m_FLFunc.FileWrite(key + "=" + m_ParamList[key]);
                            else
                                m_FLFunc.FileWrite(key.ToString());
                        }
                    }
                    m_FLFunc.FileClose();
                    Debug.LogEntry("Parameter file saved to:'" + spf + "'");

                }
            }

            catch (Exception ex)
            {
                return;
                //string response = Debug.Errors.ProcessError(1, CCP.Constants.ErrorGlobalConstants.ERRC_PARAM_FILE_WRITE_GENERAL.ToString(), ex).ToString();
                //Select Case response
                //   Case vbIgnoreB
                //      Resume Next
                //   Case vbRetry
                //      Resume 0
                //End Select
                //Resume sspf_exit
            }
            finally
            {
                m_FLFunc = null;
                Debug.PopProc();
            }
        }
        public void DumpParms()
        {
            int idx = 0;
            Debug.PushProc("Parameters.DumpParms");

            foreach (string key in m_ParamList.Keys)
            {
                idx++;
                if (key != "")
                    Debug.LogEntry(idx + ":" + key + "=" + m_ParamList[key]);
            }
            Debug.PopProc();
        }
        public string CommandLine
        {
            //SIN204 get { CommandLine = mvarCommandLine; return CommandLine; }
            get { return mvarCommandLine; } //SIN204
            set { mvarCommandLine = value; }
        }
        public void Clear()
        {
            m_ParamList.Clear();
        }
        public string ParameterFile
        {
            get { return mvarParameterFile; }
            set
            {
                mvarParameterFile = value;
            }
        }
        public string ParamFileParameter
        {
            get { return Parameter(Constants.sSwitchParamFile); }
            set
            {
                Parameter(Constants.sSwitchParamFile, value);
            }
        }
        public string GUISW
        {
            get { return Constants.sSwitchGUI; }
        }
        public string GUIDLogSW
        {
            get { return Constants.sSwitchGuidLog; }
        }

        //Start SIN204
        public bool Debugging
        {
            get
            {
                return IsTrue(Constants.sSwitchDebug);
            }
            set
            {
                Parameter(Constants.sSwitchDebug, value.ToString());
            }
        }
        //End SIN204

        public string DebuggingSW
        {
            get { return Constants.sSwitchDebug; }
        }

        //Start SIN204
        public bool DesignMode
        {
            get
            {
                return IsTrue(Constants.sSwitchDesignMode);
            }
            set
            {
                Parameter(Constants.sSwitchDesignMode, value.ToString());
            }
        }
        //End SIN204

        public string DesignModeSW
        {
            get { return Constants.sSwitchDesignMode; }
        }

        //Start SIN204
        public string LogFile
        {
            get
            {
                return Parameter(Constants.sSwitchLogFile);
            }
            set
            {
                Parameter(Constants.sSwitchLogFile, value);
            }
        }
        //End SIN204

        public string LogFileSW
        {
            get { return Constants.sSwitchLogFile; }
        }

        //Start SIN204
        public string LogPath
        {
            get
            {
                return Parameter(Constants.sSwitchLogPath);
            }
            set
            {
                Parameter(Constants.sSwitchLogPath, value);
            }
        }
        //End SIN204

        public string LogPathSW
        {
            get { return Constants.sSwitchLogPath; }
        }

        //Start SIN204
        public bool Logging
        {
            get
            {
                return IsTrue(Constants.sSwitchLogging);
            }
            set
            {
                Parameter(Constants.sSwitchLogging, value.ToString());
            }
        }
        //End SIN204

        public string LoggingSW
        {
            get { return Constants.sSwitchLogging; }
        }
        public string LogRetry
        {
            get { return Parameter(Constants.sSwitchLogRetry); }
            set
            {
                Parameter(Constants.sSwitchLogRetry, value);
            }
        }

        //Start SIN204
        public string LogRetrySW
        {
            get
            {
                return Constants.sSwitchLogRetry;
            }
        }
        //End SIN204

        //Start SIN204

        public string InputFile
        {
            get
            {
                return Parameter(Constants.sSwitchInFile);
            }
            set
            {
                Parameter(Constants.sSwitchInFile, value);
            }
        }

        public string InputFileSW
        {
            get
            {
                return Constants.sSwitchInFile;
            }
        }

        public string XMLFile
        {
            get
            {
                return Parameter(Constants.sSwitchXMLInput);
            }
            set
            {
                Parameter(Constants.sSwitchXMLInput, value);
            }
        }

        public string XMLFileSW
        {
            get
            {
                return Constants.sSwitchXMLInput;
            }
        }

        public string UserID
        {
            get
            {
                return Parameter(Constants.sSwitchUserID);
            }
            set
            {
                Parameter(Constants.sSwitchUserID, value);
            }
        }

        public string UserIDSW
        {
            get
            {
                return Constants.sSwitchUserID;
            }
        }

        public string Password
        {
            get
            {
                return Parameter(Constants.sSwitchPassword);
            }
            set
            {
                Parameter(Constants.sSwitchPassword, value);
            }
        }

        public string PasswordSW
        {
            get
            {
                return Constants.sSwitchPassword;
            }
        }

        public string OutFile
        {
            get
            {
                return Parameter(Constants.sSwitchOutFile);
            }
            set
            {
                Parameter(Constants.sSwitchOutFile, value);
            }
        }

        public string OutFileSW
        {
            get
            {
                return Constants.sSwitchOutFile;
            }
        }

        public string OutPath
        {
            get
            {
                return Parameter(Constants.sSwitchOutPath);
            }
            set
            {
                Parameter(Constants.sSwitchOutPath, value);
            }
        }

        public string OutPathSW
        {
            get
            {
                return Constants.sSwitchOutPath;
            }
        }
        //End SIN204

        //Start SIN204
        public bool IgnoreErrors
        {
            get
            {
                return IsTrue(Constants.sSwitchIgnoreErrors);
            }
            set
            {
                Parameter(Constants.sSwitchIgnoreErrors, value.ToString());
            }
        }
        //End SIN204

        public string IgnoreErrorsSW
        {
            get { return Constants.sSwitchIgnoreErrors; }
        }
        public string ReportSW
        {
            get { return Constants.sSwitchReport; }
        }
        public string IndexSW
        {
            get { return Constants.sSwitchIndex; }
        }
        public int Index
        {
            get
            {
                string v;
                v = Parameter(Constants.sSwitchIndex);
                if (NumericUtils.IsNumeric(v))
                {
                    return Convert.ToInt32(Debug.CommonFunctions.CnvData(v.ToString(), CommonFunctions.ufDataTypes.ufdtInteger));

                }
                else
                    return 0;
            }
            set
            {
                long l;
                bool b;
                if (NumericUtils.IsNumeric(value))
                {
                    l = Convert.ToInt64(Debug.CommonFunctions.CnvData(value.ToString(), CommonFunctions.ufDataTypes.ufdtInteger));
                    if (l < 0) l = 0;
                }
                else
                {
                    b = Convert.ToBoolean(Debug.CommonFunctions.CnvData(value.ToString(), CommonFunctions.ufDataTypes.ufdtBool));
                    if (!b)
                        l = 0;
                    else
                        l = 1;
                }
                Parameter(Constants.sSwitchIndex, "1");
            }
        }
        //Added by bsharma33
        public string Rerun
        {
           get { return Parameter(CCP.Constants.Constants.sSwitchRerun);}

            set { Parameter(CCP.Constants.Constants.sSwitchRerun, Convert.ToString(value)); }
        }
        
        public string RerunSW
        {
            get { return CCP.Constants.Constants.sSwitchRerun; }
        }
        
        public string GUIDLog
        {
            get { return Parameter(CCP.Constants.Constants.sSwitchGuidLog); }

            set { Parameter(CCP.Constants.Constants.sSwitchGuidLog, Convert.ToString(value)); }

        }
        
        public string Database
        {
            get
            {
                return Parameter(Constants.sSwitchDatabase);
            }
            set
            {
                Parameter(Constants.sSwitchDatabase, value);
            }
        }
        
        public string InputPath
        {
            get
            {
                return Parameter(Constants.sSwitchInPath);
            }
            set
            {
                Parameter(Constants.sSwitchInPath, value);
            }
        }

        public string InputPathSW
        {
            get
            {
                return Constants.sSwitchInPath;
            }
        }
        //End

        private void forwardReferences(Dictionary<string, string> paramList)
        {
            foreach (string key in paramList.Keys)
            {
                //SIN204 if (paramList[key].IndexOf(m_ReplacementCharacter + Constants.sRplcSwitch, 1) != 0)
                if (paramList[key].IndexOf(m_ReplacementCharacter + Constants.sRplcSwitch, 0) != -1)    //SIN204
                {
                    string parm = key + "=" + paramList[key];
                    putParam(parm, paramList);
                }
            }
        }
        private void processFunctions(Dictionary<string, string> paramList)
        {
            string parm, sValue;
            foreach (string key in paramList.Keys)
            {
                sValue = processFunction(paramList[key]);
                if (sValue != paramList[key])
                {
                    parm = key + "=" + sValue;
                    putParam(parm, paramList);
                }
            }
        }
        private string processFunction(string strOrigValue)
        {
            string[] vArgArry;
            string sValue, sArg, sFnc, sRslt, vRslt;
            int sid, xid, lID, iLenChr;

            iLenChr = m_FunctionCharacter.Length;
            sValue = strOrigValue;
            //SIN204 sid = StringUtils.InStrPos(sValue, m_FunctionCharacter, 1);
            sid = StringUtils.InStrPos(sValue, m_FunctionCharacter, 0); //SIN204
            //SIN204 while (sid > 0)
            while (sid > -1)    //SIN204
            {
                xid = StringUtils.InStrPos(sValue, m_FunctionCharacter, sid + iLenChr);
                lID = iLenChr + 1;
                if (xid > sid)
                {
                    sFnc = StringUtils.Mid(sValue, sid + iLenChr, 1).ToUpper();
                    sArg = StringUtils.Mid(sValue, sid + lID, xid = sid - lID);
                    sRslt = "";
                    switch (sFnc)
                    {
                        case Constants.sFuncFileName:
                            sRslt = Debug.FileFunctions.FileName(sArg);
                            break;
                        case Constants.sFuncShortFileName:
                            sRslt = Debug.FileFunctions.FileShortName(sArg);
                            break;
                        case Constants.sFuncFilePath:
                            sRslt = Debug.FileFunctions.FilePath(sArg);
                            break;
                        case Constants.sFuncFileType:
                            sRslt = Debug.FileFunctions.FileType(sArg);
                            break;
                        case Constants.sFuncYear:
                        case Constants.sFuncMonth:
                        case Constants.sFuncDay:
                            vRslt = Debug.CommonFunctions.CnvData(sArg, CommonFunctions.ufDataTypes.ufdtDate);
                            if (vRslt.ToString() != "")
                            {
                                switch (sFnc)
                                {
                                    case Constants.sFuncYear:
                                        sRslt = (Convert.ToDateTime(vRslt)).Year.ToString();
                                        break;
                                    case Constants.sFuncMonth:
                                        sRslt = (Convert.ToDateTime(vRslt)).Month.ToString();
                                        break;
                                    case Constants.sFuncDay:
                                        sRslt = (Convert.ToDateTime(vRslt)).Day.ToString();
                                        break;
                                }
                            }
                            else
                                sRslt = sArg;
                            break;
                        case Constants.sFuncRight:
                        case Constants.sFuncLeft:
                            vArgArry = sArg.Split(Convert.ToChar(m_FunctionCharacter));
                            if (StringUtils.UBound(vArgArry) < 1)
                                sRslt = sArg;
                            else if (StringUtils.UBound(vArgArry) >= 2)
                            {
                                if (vArgArry.GetValue(0).ToString() == "")
                                {
                                    vArgArry.SetValue(vArgArry.GetValue(1).ToString(), 0);
                                    vArgArry.SetValue(vArgArry.GetValue(2).ToString(), 1);
                                }
                                else
                                    vArgArry.SetValue(vArgArry.GetValue(1).ToString() + m_FunctionSplitChar + vArgArry.GetValue(2).ToString(), 1);
                            }
                            vArgArry.SetValue(Debug.CommonFunctions.CnvData(vArgArry.GetValue(0).ToString(), CommonFunctions.ufDataTypes.ufdtInteger), 0);
                            switch (sFnc)
                            {
                                case Constants.sFuncRight:
                                    sRslt = vArgArry.GetValue(1).ToString().Substring(vArgArry.GetValue(0).ToString().Length);
                                    break;
                                case Constants.sFuncLeft:
                                    sRslt = vArgArry.GetValue(1).ToString().Substring(0, vArgArry.GetValue(0).ToString().Length);
                                    break;
                            }
                            break;
                        case Constants.sFuncSubStr:
                            vArgArry = sArg.Split(Convert.ToChar(m_FunctionCharacter));
                            if (StringUtils.UBound(vArgArry) < 2)
                                sRslt = sArg;
                            else if (StringUtils.UBound(vArgArry) >= 3)
                            {
                                if (vArgArry.GetValue(0).ToString() == "")
                                {
                                    vArgArry.SetValue(vArgArry.GetValue(1).ToString(), 0);
                                    vArgArry.SetValue(vArgArry.GetValue(2).ToString(), 1);
                                    vArgArry.SetValue(vArgArry.GetValue(3).ToString(), 2);
                                }
                                else
                                    vArgArry.SetValue(vArgArry.GetValue(2).ToString() + m_FunctionSplitChar + vArgArry.GetValue(3).ToString(), 2);
                            }
                            vArgArry.SetValue(Debug.CommonFunctions.CnvData(vArgArry.GetValue(0).ToString(), CommonFunctions.ufDataTypes.ufdtInteger), 0);
                            vArgArry.SetValue(Debug.CommonFunctions.CnvData(vArgArry.GetValue(1).ToString(), CommonFunctions.ufDataTypes.ufdtInteger), 1);
                            sRslt = StringUtils.Mid(vArgArry.GetValue(2).ToString(), vArgArry.GetValue(2).ToString().IndexOf(vArgArry.GetValue(0).ToString()), vArgArry.GetValue(2).ToString().IndexOf(vArgArry.GetValue(1).ToString()));
                            break;
                        case Constants.sFuncReplace:
                            vArgArry = sArg.Split(Convert.ToChar(m_FunctionCharacter));
                            if (StringUtils.UBound(vArgArry) < 2)
                                sRslt = sArg;
                            else if (StringUtils.UBound(vArgArry) >= 3)
                            {
                                if (vArgArry.GetValue(0).ToString() == "")
                                {
                                    vArgArry.SetValue(vArgArry.GetValue(1).ToString(), 0);
                                    vArgArry.SetValue(vArgArry.GetValue(2).ToString(), 1);
                                    vArgArry.SetValue(vArgArry.GetValue(3).ToString(), 2);
                                }
                                else
                                    vArgArry.SetValue(vArgArry.GetValue(2).ToString() + m_FunctionSplitChar + vArgArry.GetValue(3).ToString(), 2);

                            }
                            sRslt = vArgArry.GetValue(2).ToString().Replace(vArgArry.GetValue(0).ToString(), vArgArry.GetValue(1).ToString());
                            break;
                        default:
                            sRslt = sArg;
                            break;
                    }
                    sValue = StringUtils.Left(sValue, sid - 1) + sRslt + StringUtils.Mid(sValue, xid + 1);
                    sid = StringUtils.InStrPos(sValue, m_FunctionCharacter, 1);
                }
                else
                    sid = 0;
            }
            return sValue;
        }
        public string ReadParamsValue
        {
            get
            {
                if (!Convert.ToBoolean(Constants.sSwitchReadParams))
                    return Parameter(Constants.sSwitchReadParams);
                else
                    return "";
            }
            set
            {
                Parameter(Constants.sSwitchReadParams, value.ToString());
            }
        }
        public string FormatSwitch(string s)
        {
            s = s.ToUpper().Trim();

            //1: Make sure a '/' is in front of the switch
            if (StringUtils.Left(s, 1) != "/")
                s = "/" + s;

            //2: Apply indexing where applicable
            //2.a: Remove .NX from name and do not index
            if (StringUtils.Right(s, Constants.sDoNotIndex.Length) == Constants.sDoNotIndex)
                s = StringUtils.Left(s, s.Length - Constants.sDoNotIndex.Length);
            else if (s == IndexSW) //2.b: Do not index the /INDEX parameter
            { }
            else if (StringUtils.Right(s, LabelQualifier.Length) == LabelQualifier)//2.c: Do not index .LABEL parameters
            { }
            else//2.d: Index everything else
                s = s + IndexQualifier(0);

            s = s.Trim();
            return s;
        }
        public string IndexQualifier(int iIndex)
        {
            string s;
            int i;
            i = iIndex;
            if (i == 0)
                i = Index;

            s = "";
            if (i != 0)
                s = StringUtils.Left(Constants.sIndxParameters, 1) + i.ToString() + StringUtils.Right(Constants.sIndxParameters, 1);

            return s;
        }
        // End SI06023
    }
}
