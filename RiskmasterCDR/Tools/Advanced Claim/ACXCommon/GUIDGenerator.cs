/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace CCP.Common
{
    public class GUIDGenerator
    {
        private Debug m_Dbg;

        public GUIDGenerator()
        {
            m_Dbg = null;
        }

        public Debug Debug
        {
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    //m_Dbg.GUIDGenerator = this;
                }
                return m_Dbg;
            }
            set
            {
                if ((m_Dbg != null) && (value != null) && (m_Dbg.DebugGUID == value.DebugGUID))
                    return;
                if (value == null)
                {
                    m_Dbg = new Debug();
                    //m_Dbg.GUIDGenerator = this;
                }
                else
                    m_Dbg = value;
            }
        }

        public string CreateGUID()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
