/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Security.Encryption;
using System.Reflection;	//SIN204

namespace CCP.Common
{
    public class SystemInfo
    {
        private Debug m_Dbg;
        private string m_EncryptionCharacter;
        private DTGCrypt32 m_Crypto;

        public SystemInfo()
        {
            m_Dbg = null;
            m_EncryptionCharacter = Constants.sEncryptionCharacter;
        }

        public Debug Debug
        {
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.SystemInfo = this;
                }
                return m_Dbg;
            }
            set
            {
                if ((m_Dbg != null) && (value != null) && (m_Dbg.DebugGUID == value.DebugGUID))
                    return;
                if (value == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.SystemInfo = this;
                }
                else
                {
                    m_Dbg = value;
                }
            }
        }

        public Debug Dbg
        {
            get { return Debug; }
        }

        public Parameters Parameters
        {
            get
            {
                return Dbg.Parameters;
            }
            set
            {
                Dbg.Parameters = value;
            }
        }

        public CommonFunctions CommonFunctions
        {
            get
            {
                return Dbg.CommonFunctions;
            }
        }
        public FileFunctions FileFunctions
        {
            get
            {
                return Dbg.FileFunctions;
            }
        }

        public DTGCrypt32 Crypto
        {
            get
            {
                if (m_Crypto == null)
                    m_Crypto = new DTGCrypt32();
                return m_Crypto;
            }
        }

        //Start SIN204
        //[Obsolete("Please use new version without Section name for use with Config files")] //SIN204
        //public long lGetSystemInfo(string sSection, string sKey, string sFileName, string sSpecifiedFile, long lDefault)
        //{            
        //    string sInfo;
        //    sInfo = GetSystemInfo(sSection, sKey, sFileName);
        //    return Convert.ToInt64(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtLong));
        //}

        //[Obsolete("Please use new version without Section name for use with Config files")] //SIN204
        //public string GetSystemInfo(string sSection, string sKey, string sFileName)
        //{
        //    string sInfo;
        //    string[] sArgArray = { "" };
        //    Dbg.PushProc("GetSystemInfo", sSection, sKey, sFileName, null);

        //    try
        //    {
        //        if (sFileName == "")
        //            sFileName = "SystemSettings.dat";

        //        sInfo = Utils.GetSystemInfoPrivate(sSection, sKey, sFileName);

        //        if (sInfo != "")
        //        {
        //            sInfo = CommonFunctions.ResolveStringValues(sInfo, sArgArray);
        //            if (StringUtils.Left(sInfo, 1) == m_EncryptionCharacter)
        //            {
        //                sInfo = StringUtils.Mid(sInfo, 2);
        //                sInfo = Crypto.DecryptString(sInfo, PrivateConstants.CRYPTKEY);
        //            }
        //        }
        //        return sInfo;
        //    }
        //    catch (Exception e)
        //    {
        //        Dbg.PopProc(e.GetType(), e.Message, e.Source, null);
        //        return "";
        //    }
        //}
        //End SIN204

        //Start SIN204
        public int iGetSystemInfo(string sKey, string sFileName)
        {
            int iDefault = 0;
            return iGetSystemInfo(sKey, sFileName, iDefault);
        }

        public int iGetSystemInfo(string sKey, string sFileName, int iDefault)
        {
            string sInfo = GetSystemInfo(sKey, sFileName, iDefault);
            return Convert.ToInt32(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtLong));
        }

        public bool bGetSystemInfo(string sKey, string sFileName)
        {
            bool bDefault = false;
            return bGetSystemInfo(sKey, sFileName, bDefault);
        }

        public bool bGetSystemInfo(string sKey, string sFileName, bool bDefault)
        {
            string sInfo = GetSystemInfo(sKey, sFileName, bDefault);
            return Convert.ToBoolean(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtBool));
        }

        public double dGetSystemInfo(string sKey, string sFileName)
        {
            double dDefault = 0d;
            return dGetSystemInfo(sKey, sFileName, dDefault);
        }

        public double dGetSystemInfo(string sKey, string sFileName, double dDefault)
        {
            string sInfo = GetSystemInfo(sKey, sFileName, dDefault);
            return Convert.ToDouble(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtDouble));
        }

        public DateTime dtGetSystemInfo(string sKey, string sFileName)
        {
            DateTime dtDefault = new DateTime(9999, 12, 31);
            return dtGetSystemInfo(sKey, sFileName, dtDefault);
        }

        public DateTime dtGetSystemInfo(string sKey, string sFileName, DateTime dtDefault)
        {
            string sInfo = GetSystemInfo(sKey, sFileName, dtDefault);
            return Convert.ToDateTime(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtDate));
        }

        public DateTime tmGetSystemInfo(string sKey, string sFileName)
        {
            DateTime dtDefault = new DateTime(9999, 12, 31, 0, 0, 0);
            return tmGetSystemInfo(sKey, sFileName, dtDefault);
        }

        public DateTime tmGetSystemInfo(string sKey, string sFileName, DateTime dtDefault)
        {
            string sInfo = GetSystemInfo(sKey, sFileName, dtDefault);
            return Convert.ToDateTime(Debug.CommonFunctions.CnvData(sInfo, CommonFunctions.ufDataTypes.ufdtTime));
        }

        public string GetSystemInfo(string sKey, string sFileName, object oDefault)
        {
            string sInfo;
            string[] sArgArray = { "" };
            Dbg.PushProc("GetSystemInfo", sKey, sFileName, null, null);

            try
            {
                sInfo = Utils.GetSystemInfoPrivate(sKey, sFileName);

                if (sInfo == "")
                    sInfo = oDefault.ToString();

                sInfo = CommonFunctions.ResolveStringValues(sInfo, sArgArray);
                if (StringUtils.Left(sInfo, 1) == m_EncryptionCharacter)
                {
                    sInfo = StringUtils.Mid(sInfo, 2);
                    sInfo = Crypto.DecryptString(sInfo, PrivateConstants.CRYPTKEY);
                }
                return sInfo;
            }
            catch (Exception e)
            {
                Dbg.PopProc(e.GetType(), e.Message, e.Source, null);
                return "";
            }
        }
        //End SIN204

        // Start SI06023

        [Obsolete("Please use the version without group name parameter")] //SIN204
        public string GetParameter(string sParameterName, string sGroupName, string sItemName,
                                   string sAlternateDATFile, string sDefaultValue)
        {
            return GetParameter(sParameterName, sGroupName, sItemName, sAlternateDATFile, Convert.ToInt32(sDefaultValue), false, false);
        }

        [Obsolete("Please use the version without group name parameter")] //SIN204
        public string GetParameter(string sParameterName, string sGroupName, string sItemName,
                                   string sAlternateDATFile, int iDefaultValue, bool bResetParameter,
                                   bool bIsBool)
        {
            string sValue = "";
            Dbg.PushProc("GetParameter", sParameterName, sGroupName, sItemName, sAlternateDATFile);

            if (bIsBool)
            {
                if (Parameters.IsTrue(sParameterName) && (Parameters.Parameter(sParameterName) == ""))
                    Parameters.Parameter(sParameterName, "True)");
            }

            if (bResetParameter)
                Parameters.RemoveParameter(sParameterName);
            else
                sValue = Parameters.Parameter(sParameterName);

            if (sItemName == "")
                sItemName = sParameterName;

            if (sValue == "")
                if (sItemName != "")
                {
                    if (sAlternateDATFile != null)
                        sValue = GetSystemInfo(Dbg.AppTitle, sItemName, sAlternateDATFile);
                    if (sValue == "")
                        sValue = GetSystemInfo(Dbg.AppTitle, sItemName, "");
                }

            if ((sValue == "") && (sGroupName != ""))
                if (sItemName != "")
                {
                    if (sAlternateDATFile != "")
                        sValue = GetSystemInfo(sGroupName, sItemName, sAlternateDATFile);
                    if (sValue == "")
                        sValue = GetSystemInfo(sGroupName, sItemName, "");
                }

            if (sValue == "")
            {
                sGroupName = Constants.SYSSET_DEFAULT;
                if (sItemName == "")
                    sItemName = sParameterName;
                if (sItemName != "")
                {
                    if (sAlternateDATFile != "")
                        sValue = GetSystemInfo(sGroupName, sItemName, sAlternateDATFile);
                    if (sValue == "")
                        sValue = GetSystemInfo(sGroupName, sItemName, "");
                }
            }

            if (sValue == "")
                sValue = iDefaultValue.ToString();

            if (sValue != "")
                if (sParameterName != "")
                    Parameters.Parameter(sParameterName, sValue);

            if (bIsBool)
            {
                Dbg.PopProc();
                return Parameters.IsTrue(sParameterName).ToString();
            }
            else
            {
                Dbg.PopProc();
                return Parameters.Parameter(sParameterName);
            }
        }




             //Start SIN204
        public string GetParameter(string sParameterName, string sAltConfigFile, string sDefaultValue)
        {
            return GetParameter(sParameterName, sAltConfigFile, sDefaultValue, false, false);
        }

        public string GetParameter(string sParameterName, string sAltConfigFile, string sDefaultValue, bool bResetParameter, bool bIsBool)
        {
            string sValue = "";
            Dbg.PushProc("GetParameter", sParameterName, null, null, null);

            if (bIsBool)
            {
                if (Parameters.IsTrue(sParameterName) && (Parameters.Parameter(sParameterName) == ""))
                    Parameters.Parameter(sParameterName, "True");
            }

            if (bResetParameter)
                Parameters.RemoveParameter(sParameterName);
            else
                sValue = Parameters.Parameter(sParameterName);

            if (sValue == "")
            {
                if (sAltConfigFile != null && sAltConfigFile != "")
                    sValue = GetSystemInfo(sParameterName, sAltConfigFile, sDefaultValue);
                if (sValue == "")
                    sValue = GetSystemInfo(sParameterName, "", sDefaultValue);
            }

            if (sValue != "")
                if (sParameterName != "")
                    Parameters.Parameter(sParameterName, sValue);

            Dbg.PopProc();
            if (bIsBool)
                return Parameters.IsTrue(sParameterName).ToString();
            else
                return Parameters.Parameter(sParameterName);

        }

        public void GetStandardParameters(string sCmdLine)
        {
            string sAltConfigFile = String.Empty;
            GetStandardParameters(sCmdLine, sAltConfigFile);
        }

        public void GetStandardParameters(string sCmdLine, string sAltConfigFile)
        {
            string sValue = ""; bool b;
            Dbg.PushProc("GetStandardParameters", sCmdLine, null, null, null);

            if (sCmdLine != "")
            {
                Parameters.NoRead = true;
                b = Parameters.ClearWhenParsing;
                Parameters.PreserveOnParse();
                Parameters.ReadParams = false;
                Parameters.Parse(sCmdLine);
                Parameters.ReadParams = true;
                Parameters.ClearWhenParsing = b;
                //In case of non-blank command, if not specified the NOREAD is set to true which
                //prevents reading the application specific save file.
                if (!Parameters.IsSwitch(Parameters.NoReadSW.ToString()))
                    Parameters.NoRead = true;
            }

            if (!Parameters.NoRead)
            {
                sValue = FileFunctions.GetTempFileName(Dbg.AppTitle, "");
                if (FileFunctions.FileExists("", sValue))
                    sCmdLine = (char)34 + sValue + (char)34 + " " + sCmdLine;
                if (sCmdLine != "")
                {
                    Dbg.LogEntry("Parsing Command Line: " + sCmdLine);
                    Parameters.Parse(sCmdLine);
                }
            }

            Dbg.AppTitle = GetParameter(Constants.sSwitchAppTitle, sAltConfigFile, Dbg.AppTitle);
            GetParameter(Constants.sSwitchCompany, sAltConfigFile, GetAssemblyCompany());
            GetParameter(Constants.sSwitchProdID, sAltConfigFile, GetAssemblyProductID());
            Dbg.Interactive = Convert.ToBoolean(GetParameter(Parameters.GUISW, sAltConfigFile, Dbg.Interactive.ToString(), false, true));
            Dbg.Debugging = Convert.ToBoolean(GetParameter(Parameters.DebuggingSW, sAltConfigFile, Dbg.Debugging.ToString(), false, true));
            Dbg.DesignMode = Convert.ToBoolean(GetParameter(Parameters.DesignModeSW, sAltConfigFile, Dbg.DesignMode.ToString(), false, true));
            Dbg.LogPath = GetParameter(Parameters.LogPathSW, sAltConfigFile, Dbg.LogPath);
            Dbg.LogFile = GetParameter(Parameters.LogFileSW, sAltConfigFile, Dbg.LogFile);
            Dbg.Logging = Convert.ToBoolean(GetParameter(Parameters.LoggingSW, sAltConfigFile, Dbg.Logging.ToString(), false, true));
            Dbg.LogRetry = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Parameters.LogRetrySW, sAltConfigFile, Dbg.LogRetry.ToString()),CommonFunctions.ufDataTypes.ufdtInteger));
            Dbg.DiscreteLogging = Convert.ToBoolean(GetParameter(Constants.sSwitchDiscreteLog, sAltConfigFile, Dbg.DiscreteLogging.ToString(), false, true));
            Dbg.IgnoreErrors = Convert.ToBoolean(GetParameter(Parameters.IgnoreErrorsSW, sAltConfigFile, Dbg.IgnoreErrors.ToString(), false, true));
            Dbg.LogFileMaxSize = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.sSwitchLogMaxSize, sAltConfigFile, Dbg.LogFileMaxSize.ToString()), CommonFunctions.ufDataTypes.ufdtInteger));
            Dbg.LogFileNumBUps = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.sSwitchLogNumBUps, sAltConfigFile, Dbg.LogFileNumBUps.ToString()), CommonFunctions.ufDataTypes.ufdtInteger));
            Dbg.LogFileRetentionDays = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.sSwitchLogRetention, sAltConfigFile, Dbg.LogFileRetentionDays.ToString()), CommonFunctions.ufDataTypes.ufdtInteger));
            GetParameter(Parameters.GUIDLogSW, sAltConfigFile, FileFunctions.FileShortName(Dbg.LogFile) + "_GUID.log");
            GetParameter(Parameters.ReportSW, sAltConfigFile, FileFunctions.FileShortName(Dbg.LogFile) + ".RPT");

            if (Parameters.ClearLog && Dbg.Logging)
                Dbg.ClearLog();

            Dbg.PopProc();
        }

        public string GetAssemblyProductID()
        {
            object[] customAttributes = Assembly.GetCallingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);

            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                return ((AssemblyProductAttribute)customAttributes[0]).Product;
            }

            return "";
        }

        public string GetAssemblyCompany()
        {
            object[] customAttributes = Assembly.GetCallingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);

            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                return ((AssemblyCompanyAttribute)customAttributes[0]).Company;
            }

            return "";
        }
        //End SIN204

        [Obsolete("Please use the version with only sCmdLine parameter")] //SIN204
        public void GetStandardParameters(string sCmdLine, string sGroupName,
                                  string sAlternateDATFile, bool bAllowInteractive)
        {
            string sValue = ""; bool b;
            Dbg.PushProc("GetStandardParameters", sCmdLine, sGroupName, sAlternateDATFile, null);

            bAllowInteractive = true;
            if (sCmdLine != "")
            {
                Parameters.NoRead = true;
                b = Parameters.ClearWhenParsing;
                Parameters.PreserveOnParse();
                Parameters.ReadParams = false;
                Parameters.Parse(sCmdLine);
                Parameters.ReadParams = true;
                Parameters.ClearWhenParsing = b;
                //In case of non-blank command, if not specified the NOREAD is set to true which
                //prevents reading the application specific save file.
                if (!Parameters.IsSwitch(Parameters.NoReadSW.ToString()))
                    Parameters.NoRead = true;
            }
            else if (bAllowInteractive)//When no command line then set the interactive switch appropriately
                sCmdLine = Parameters.GUISW;

            if (!Parameters.NoRead)
            {
                sValue = FileFunctions.GetTempFileName(Dbg.AppTitle, "");
                if (FileFunctions.FileExists("", sValue))
                    sCmdLine = (char)34 + sValue + (char)34 + " " + sCmdLine;
            }
            if (sCmdLine != "")
            {
                Dbg.LogEntry("Parsing Command Line: " + sCmdLine);
                Parameters.Parse(sCmdLine);
            }
            Dbg.AppTitle = GetParameter(Dbg.AppTitle, Dbg.AppTitle, "", sAlternateDATFile, Dbg.AppTitle);

            // App conversion 
            //GetParameter(Constants.SYSSET_COMPANY, Constants.SYSSET_CAPTIONS, "", sAlternateDATFile, App.CompanyName);

            GetParameter(Constants.SYSSET_PRODUCTID, Constants.SYSSET_CAPTIONS, "", sAlternateDATFile, Constants.conPRODUCTID);

            // //ToDo: helpfile not needed or supported?
            //Dbg.AppHelpFile = GetParameter(Constants.SYSSET_HELPFILE, "", "", sAlternateDATFile, Dbg.AppHelpFile);

            Dbg.Interactive = Convert.ToBoolean(GetParameter(Parameters.GUISW, sGroupName, "", sAlternateDATFile, Convert.ToInt32(Dbg.Interactive), false, true));

            Dbg.Debugging = Convert.ToBoolean(GetParameter(Parameters.DebuggingSW, sGroupName, Constants.SYSSET_DEBUGGING, sAlternateDATFile, Convert.ToInt32(Dbg.Debugging), false, true));

            Dbg.DesignMode = Convert.ToBoolean(GetParameter(Parameters.DesignModeSW, sGroupName, Constants.SYSSET_DSGNMODE, sAlternateDATFile, Convert.ToInt32(Dbg.DesignMode),false, true));

            Dbg.LogPath = GetParameter(Parameters.LogPathSW, sGroupName, Constants.SYSSET_LOGPATH, sAlternateDATFile, Dbg.LogPath);

            Dbg.LogFile = GetParameter(Parameters.LogFileSW, sGroupName, Constants.SYSSET_LOGFILE, sAlternateDATFile, Dbg.LogFile);

            Dbg.Logging = Convert.ToBoolean(GetParameter(Parameters.LoggingSW, sGroupName, Constants.SYSSET_LOGGING, sAlternateDATFile,  Convert.ToInt32(Dbg.Logging), false, true));

            Dbg.LogRetry =Convert.ToInt32(GetParameter(Parameters.LogRetry, sGroupName, Constants.SYSSET_LOGGINGRETRY, sAlternateDATFile, Dbg.LogRetry.ToString()));

            Dbg.DiscreteLogging = Convert.ToBoolean(GetParameter(Constants.SYSSET_DISCRETELOG, sGroupName, "", sAlternateDATFile, Convert.ToInt32(Dbg.DiscreteLogging), false, true));

            Dbg.IgnoreErrors = Convert.ToBoolean(GetParameter(Parameters.IgnoreErrorsSW, sGroupName, "", sAlternateDATFile, Convert.ToInt32(Dbg.IgnoreErrors), false, true));

            Dbg.LogFileMaxSize = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.SYSSET_LOGFILE_SIZE, sGroupName, Constants.SYSSET_LOGFILE_SIZE, sAlternateDATFile, Dbg.LogFileMaxSize.ToString()), CommonFunctions.ufDataTypes.ufdtLong));

            Dbg.LogFileNumBUps = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.SYSSET_LOGFILE_NUMB, sGroupName, Constants.SYSSET_LOGFILE_NUMB, sAlternateDATFile, Dbg.LogFileNumBUps.ToString()), CommonFunctions.ufDataTypes.ufdtLong));

            //SIN204 Dbg.LogFileRetentionDays = Convert.ToInt64(CommonFunctions.CnvData(GetParameter(Constants.SYSSET_RETENTION, sGroupName, Constants.SYSSET_RETENTION, sAlternateDATFile, Dbg.LogFileRetentionDays.ToString()), CommonFunctions.ufDataTypes.ufdtLong));
            Dbg.LogFileRetentionDays = Convert.ToInt32(CommonFunctions.CnvData(GetParameter(Constants.SYSSET_RETENTION, sGroupName, Constants.SYSSET_RETENTION, sAlternateDATFile, Dbg.LogFileRetentionDays.ToString()), CommonFunctions.ufDataTypes.ufdtLong));    //SIN204

            GetParameter(Parameters.GUIDLogSW, sGroupName, Constants.SYSSET_GUIDLOGFILE, sAlternateDATFile, FileFunctions.FileShortName(Dbg.LogFile) + "_GUID.log");

            GetParameter(Parameters.ReportSW, sGroupName, "", sAlternateDATFile, FileFunctions.FileShortName(Dbg.LogFile) + ".RPT");

            if (Parameters.ClearLog && Dbg.Logging)
                 Dbg.ClearLog();

            Dbg.PopProc();
        }
        // End SI06023
    }
}
