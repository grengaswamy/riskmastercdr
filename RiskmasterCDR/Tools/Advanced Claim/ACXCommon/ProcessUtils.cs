using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace CCP.Common
{
    public class ProcessUtils
    {
        public void ExecShell(string ProgramPath, string ProgramName,string ParmString)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = false;
            startInfo.UseShellExecute = true;
            if (ProgramPath != "")
                startInfo.WorkingDirectory = ProgramPath;
            if (ParmString != "")
                startInfo.Arguments = ParmString;
            startInfo.FileName = ProgramName;
            Process process = Process.Start(startInfo);
        }

        public void ExecShellAndWait(string ProgramPath, string ProgramName, string ParmString)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = false;
            startInfo.UseShellExecute = true;
            if (ProgramPath != "")
                startInfo.WorkingDirectory = ProgramPath;
            if (ParmString != "")
                startInfo.Arguments = ParmString;
            startInfo.FileName = ProgramName;
            Process process = Process.Start(startInfo);
            process.WaitForExit();
        }

        public void ExecShellAndWait(string ProgramPath, string ProgramName, string ParmString, int TimeOut)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = false;
            startInfo.UseShellExecute = true;
            if (ProgramPath != "")
                startInfo.WorkingDirectory = ProgramPath;
            if (ParmString != "")
                startInfo.Arguments = ParmString;
            startInfo.FileName = ProgramName;
            Process process = Process.Start(startInfo);
            process.WaitForExit(TimeOut);
        }

    }
}
