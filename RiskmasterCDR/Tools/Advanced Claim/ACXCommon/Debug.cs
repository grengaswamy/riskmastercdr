/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 * 01/28/2010 | SI06650 |    JTC     | Fix for reading .Config file from a web site
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CCP.Constants;
using System.Reflection;    //SIN204

namespace CCP.Common
{
    public class Debug
    {
        private string m_GUID;
        private FileFunctions m_FileFunc;
        private Errors m_Errors;
        private bool m_bInteractive;
        private bool m_bIgnoreErrors;
        private bool m_bDesignMode;
        private int m_ProcCount;
        private ArrayList m_ProcStack;
        //private FileFunctions m_LogFileFunc;
        private string m_AppTitle;
        private bool m_bDebugging;
        private string m_LogFile;
        private string m_LogPath;
        private bool m_InitLogging;
        private int m_MaxLogFileSize;
        private int m_NumLogFileBUs;
        private SystemInfo m_SysSet;
        private FileFunctions m_LogFileFunc;
        private bool bLogging;
        private int m_LogRetry;
        private string m_sSessionID;
        private string m_sUserID;
        private bool m_bDiscreteLogging;
        private string strLogFile;
        private Parameters m_Parameters;
        private string m_AppPath;
        private string m_AppVersion;
        private CommonFunctions m_Funcs;
        private string m_ProfileFile;// SI06023
        private string m_LogFileRetention;// SI06023     

        public Debug()
        {
            m_ProcCount = 0;
            m_ProcStack = new ArrayList(0);
            m_LogFileFunc = null;
            m_Errors = null;
            //m_Funcs = null;
            m_Parameters = null;
            //m_DBFuncs = null;
            m_SysSet = null;
            //m_FileFunc = null;
            //m_GUIDGen = null;
            //m_Shell = null;
            m_ProfileFile = Constants.sIniProfileFile;// SI06023
            bLogging = false;
            Debugging = false;
            Interactive = false;
            DesignMode = false;
            IgnoreErrors = false;
            DiscreteLogging = false;
            AppTitle = Application.ProductName;
            AppPath = Directory.GetCurrentDirectory();
            AppVersion = Application.ProductVersion;
            //ToDo: helpfile not needed or supported?
            //AppHelpFile = Application.
            LogPath = "";
            LogFile = "";
            LogRetry = 10;

            m_GUID = Guid.NewGuid().ToString();
            m_MaxLogFileSize = Constants.lLOGFILE_SIZE;
            m_NumLogFileBUs = Constants.iLOGFILE_NUMB;
            m_LogFileRetention = Constants.iLOGFILE_RETN.ToString();// SI06023
            //m_InitLogging = true;
        }

        ~Debug()
        {
            LogFileFunc.FileClose();
        }

        public string DebugGUID
        {
            get
            {
                return m_GUID;
            }
        }

        public FileFunctions FileFunctions
        {
            get
            {
                if (m_FileFunc == null)
                {
                    m_FileFunc = new FileFunctions();
                    m_FileFunc.CDebug = this;
                }
                return m_FileFunc;
            }
            set
            {
                m_FileFunc = value;
                if (m_FileFunc != null)
                {
                    m_FileFunc.CDebug = this;
                }
            }
        }

        public Errors Errors
        {
            get
            {
                if (m_Errors == null)
                {
                    m_Errors = new Errors();
                    m_Errors.Debug = this;
                }
                return m_Errors;
            }
            set
            {
                m_Errors = value;
                if (m_Errors != null)
                {
                    m_Errors.Debug = this;
                }
            }
        }

        public CommonFunctions CommonFunctions
        {
            get
            {
                if (m_Funcs == null)
                {
                    m_Funcs = new CommonFunctions();
                    m_Funcs.Debug = this;
                }
                return m_Funcs;
            }
            set
            {
                m_Funcs = value;
                if (m_Funcs != null)
                    m_Funcs.Debug = this;
            }
        }

        public string CurrProcedure
        {
            get
            {
                int idx;
                if (Debugging)
                    idx = m_ProcCount - 1;
                else
                    idx = m_ProcCount - 2;
                if (idx < 0)
                    idx = 0;
                return (string)m_ProcStack[idx];
            }
        }

        public string Stack
        {
            get
            {
                string str;
                str = "";
                for (int i = 0; i < m_ProcCount - 1; i++)
                {
                    str = str + "->" + m_ProcStack[i];
                }
                return str;
            }
        }

        //ToDo: may be obsolete
        /*public string AppHelpFile
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }*/

        public string AppTitle
        {
            get
            {
                return m_AppTitle;
            }
            set
            {
                m_AppTitle = value;
                if (m_AppTitle == "" || m_AppTitle == null)
                    m_AppTitle = Application.ProductName;
            }
        }

        public string AppPath
        {
            get
            {
                return m_AppPath;
            }
            set
            {
                m_AppPath = value;
				//Start SIN204
                if (m_AppPath.StartsWith("file:\\"))
                    m_AppPath = m_AppPath.Substring(6);
				//End SIN204
                if (m_AppPath == "" || m_AppPath == null)
                    m_AppPath = Directory.GetCurrentDirectory();
            }
        }

        /*public string ComputerName
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }*/

        public void CloseLog()
        {
            subLogFile("Close","");
        }

        public void DebugTrace(object p1, object p2, object p3,
                               object p4, object p5)
        {
            string status;
            ArrayList sp;
            ArrayList p;
            int idx;
            sp = new ArrayList();
            p = new ArrayList();

            if (!Debugging)
                return;

            status = CurrProcedure + ":";

            Debugging = false;
            PushProc("Debug.DebugTrace");

            //Start SIN204
            //p[0] = p1;
            //p[1] = p2;
            //p[2] = p3;
            //p[3] = p4;
            //p[4] = p5;
            p.Insert(0, p1);
            p.Insert(1, p2);
            p.Insert(2, p3);
            p.Insert(3, p4);
            p.Insert(4, p5);
            //End SIN204

            for (idx = 0; idx <= 4; idx++)
            {
                //SIN204 if (p[idx] == null || (string)p[idx] == "")
                if (p[idx] == null || p[idx].ToString() == "")  //SIN204
                    //SIN204 sp[idx] = "NULL";
                    sp.Insert(idx, "NULL"); //SIN204
                else
                    //SIN204 sp[idx] = p[idx].ToString();
                    sp.Insert(idx, p[idx].ToString());  //SIN204
                //SIN204 if ((string)sp[idx] != "")
                if(sp[idx].ToString() != "")    //SIN204
                    //SIN204 status = status + "Param" + (idx + 1) + "=" + " : ";
                    status = status + "Param" + (idx + 1) + "=" + sp[idx] + " : ";	//SIN204
            }

            status = status + " :Call Stack:" + Stack;

            LogEntry(status);

            PopProc();
            Debugging = true;
        }

        public string LogEntry(string ln)
        {
            return subLogFile("Log", ln);
        }

        public bool Debugging
        {
            get
            {
                return m_bDebugging;
            }
            set
            {
                m_bDebugging = value;
            }
        }

        public int ProcCount
        {
            get
            {
                return m_ProcCount;
            }
            set
            {
                m_ProcCount = value;
                if (m_ProcCount < 0)
                    m_ProcCount = 0;
            }
        }

        public void PopProc(object dbg1, object dbg2, object dbg3,
                             object dbg4)
        {
            if (Debugging)
                DebugTrace("*Leaving*", dbg1, dbg2, dbg3, dbg4);
            ProcCount = ProcCount - 1;
        }

        public void PopProc()
        {
            PopProc(null, null, null, null);
        }

        public void PushProc(string CurrProc, object dbg1, object dbg2, 
                             object dbg3, object dbg4)
        {
            ProcCount = ProcCount + 1;
            if (Debugging)
                DebugTrace("*Entering:*" + CurrProc, dbg1, dbg2, dbg3, dbg4);
            if (m_ProcStack.Count < m_ProcCount)
                m_ProcStack.Insert(m_ProcCount - 1, CurrProc);
            else
                m_ProcStack[m_ProcCount - 1] = CurrProc;
        }

        public void PushProc(string CurrProc)
        {
            PushProc(CurrProc, null, null, null, null);
        }

        public SystemInfo SystemInfo
        {
            get
            {
                if (m_SysSet == null)
                {
                    m_SysSet = new SystemInfo();
                    m_SysSet.Debug = this;
                }
                return m_SysSet;
            }
            set
            {
                m_SysSet = value;
                if (m_SysSet != null)
                    m_SysSet.Debug = this;
            }
        }

        public int LogFileMaxSize
        {
            get
            {
                //SIN204 return m_MaxLogFileSize;
                return getLogFileMaxSize(); //SIN204
            }
            set
            {
                //Start SIN204
                getLogFileMaxSize(value);
                //object v;
                //m_MaxLogFileSize = value;
                //if (m_MaxLogFileSize <= 0)
                //{
                    //v = SystemInfo.GetParameter(Constants.SYSSET_LOGFILE_SIZE, Constants.SYSSET_LOGGING,
                    //                            Constants.SYSSET_LOGFILE_SIZE, "", Constants.lLOGFILE_SIZE);
                    //v = Conversion.ConvertData(v, Conversion.cmnDataTypes.cdtLong);
                    //if ((int)v == 0)
                    //    v = Constants.lLOGFILE_SIZE;
                //    m_MaxLogFileSize = Constants.lLOGFILE_SIZE;
                //}
                //End SIN204
            }
        }

        //Start SIN204
        private int getLogFileMaxSize()
        {
            return getLogFileMaxSize(0);
        }

        private int getLogFileMaxSize(string sValue)
        {
            return getLogFileMaxSize(CommonFunctions.CnvData(sValue, CommonFunctions.ufDataTypes.ufdtInteger));
        }

        private int getLogFileMaxSize(int iValue)
        {
            if (iValue <= 0)
                iValue = Convert.ToInt32(CommonFunctions.CnvData(Parameters.Parameter(Constants.sSwitchLogMaxSize), CommonFunctions.ufDataTypes.ufdtInteger));
            if (iValue <= 0)
                iValue = Constants.lLOGFILE_SIZE;
            m_MaxLogFileSize = iValue;
            return m_MaxLogFileSize;
        }
        //End SIN204

        public int LogFileNumBUps
        {
            get
            {
                //SIN204 return m_NumLogFileBUs;
                return getLogFileNumBUps();	//SIN204
            }
            set
            {
                //Start SIN204
                getLogFileNumBUps(value);
                //object v;
                //m_NumLogFileBUs = value;
                //if (m_NumLogFileBUs <= 0)
                //{
                    //v = SystemInfo.GetParameter(Constants.SYSSET_LOGFILE_NUMB, Constants.SYSSET_LOGGING,
                    //                            Constants.SYSSET_LOGFILE_NUMB, "", Constants.iLOGFILE_NUMB);
                    //v = Conversion.ConvertData(v, Conversion.cmnDataTypes.cdtLong);
                    //if ((int)v == 0)
                     //   v = Constants.iLOGFILE_NUMB;
                //    m_NumLogFileBUs = Constants.iLOGFILE_NUMB;
                //}
                //End SIN204
            }
        }

        //Start SIN204
        private int getLogFileNumBUps()
        {
            return getLogFileNumBUps(0);
        }

        private int getLogFileNumBUps(string sValue)
        {
            return getLogFileNumBUps(Convert.ToInt32(CommonFunctions.CnvData(sValue, CommonFunctions.ufDataTypes.ufdtInteger)));
        }

        private int getLogFileNumBUps(int iValue)
        {
            if (iValue <= 0)
                iValue = Convert.ToInt32(CommonFunctions.CnvData(Parameters.Parameter(Constants.sSwitchLogNumBUps),CommonFunctions.ufDataTypes.ufdtInteger));
            if (iValue <= 0)
                iValue = Constants.iLOGFILE_NUMB;
            m_NumLogFileBUs = iValue;
            return m_NumLogFileBUs;
        }
        //End SIN204

        public FileFunctions LogFileFunc
        {
            get
            {
                if (m_LogFileFunc == null)
                {
                    m_LogFileFunc = new FileFunctions();
                    m_LogFileFunc.CDebug = this;
                }
                return m_LogFileFunc;
            }
        }

        public int LogRetry
        {
            get
            {
                return m_LogRetry;
            }
            set
            {
                m_LogRetry = value;
            }
        }

        public string OpenLogFile()
        {
            string sNewName = "";
            PushProc("OpenLogFile", bLogging, m_LogPath, m_LogFile,null);
            if (bLogging)
            {
                if (LogFileFunc.FileTextStream == null)
                {
                    if (!LogFileFunc.FolderExists(m_LogPath))
                    {
                        string sh;
                        sh = m_LogPath;
                        LogPath = "";
                        m_Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_NO_FOLDER, MessageBoxButtons.AbortRetryIgnore,
                                                 MessageBoxIcon.Exclamation, "Invalid Log Folder",
                                                 "Log Folder Replaced with " + m_LogPath, sh);
                    }
                    LogFileFunc.FileOpen(m_LogPath,m_LogFile,(FileFunctions.cFileAccess)CommonGlobalConstants.FileAccessTypes.ForAppending,false,LogRetry);
                    if (LogFileFunc.FileFile.Length > m_MaxLogFileSize)
                    {
                        LogFileFunc.FileClose();
                        sNewName = LogFileFunc.FileShortName(m_LogFile) + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") +
                                   "." + LogFileFunc.FileType(m_LogFile);
                        try
                        {
                            LogFileFunc.FileRename(m_LogPath, m_LogFile, m_LogPath,sNewName);
                        }
                        catch { }
                        bool berror = true;
                        while (berror)
                        {
                            try
                            {
                                LogFileFunc.FileOpen(m_LogPath, m_LogFile, (FileFunctions.cFileAccess)CommonGlobalConstants.FileAccessTypes.ForAppending, false, LogRetry);
                                berror = false;
                            }
                            catch { }
                        }
                        if(LogFileFunc.FileExists(m_LogPath,sNewName))
                            LogEntry("Log File Backed Up to:" + sNewName);
                        else
                            sNewName = "";
                    }
                    if (sNewName != "" && sNewName != null)
                    {
                        ArrayList fls;
                        object[,] arrFle;
                        int i,j;
                        DateTime dte;
                        string nme;
                        fls = LogFileFunc.FileList(m_LogPath, LogFileFunc.FileShortName(m_LogFile) + "*." +
                                                              LogFileFunc.FileType(m_LogFile));
                        arrFle = new string[fls.Count, 2];
                        i = 0;
                        foreach (FileInfo finfo in fls)
                        {
                            arrFle[i, 0] = finfo.LastAccessTime;
                            arrFle[i, 1] = finfo.Name;
                            for (j = i; j > 0; j--)
                            {
                                if ((DateTime)arrFle[j, 0] < (DateTime)arrFle[j - 1, 0])
                                {
                                    dte = (DateTime)arrFle[j, 0];
                                    nme = (string)arrFle[j, 1];
                                    arrFle[j, 0] = arrFle[j - 1, 0];
                                    arrFle[j, 1] = arrFle[j - 1, 1];
                                    arrFle[j - 1, 0] = dte;
                                    arrFle[j - 1, 1] = nme;
                                }
                                else
                                    break;
                            }
                            i++;
                        }

                        for(i = 0;i < fls.Count - m_NumLogFileBUs;i++)
                            try
                            {
                                LogFileFunc.FileDelete(m_LogPath, (string)arrFle[i, 1]);
                            }
                            catch { }
                    }
                }
            }
            PopProc();
            return "Opened";
        }

        public string SessionID
        {
            get
            {
                return m_sSessionID;
            }
            set
            {
                m_sSessionID = value;
            }
        }

        public string UserID
        {
            get
            {
                return m_sUserID;
            }
            set
            {
                m_sUserID = value;
            }
        }

        public bool DiscreteLogging
        {
            get
            {
                return m_bDiscreteLogging;
            }
            set
            {
                m_bDiscreteLogging = value;
            }
        }

        public Parameters Parameters
        {
            get
            {
                if (m_Parameters == null)
                {
                    m_Parameters = new Parameters();
                    m_Parameters.Debug = this;
                }
                return m_Parameters;
            }
            set
            {
                m_Parameters = value;
                if (m_Parameters != null)
                    m_Parameters.Debug = this;
            }
        }

        public string subLogFile(string func, string ln)
        {
            string strLogLine = "";
            bool bHoldDebug;
            int idx;

            PushProc("Debug.subLogFile",func,ln,m_LogPath,m_LogFile);
            bHoldDebug = Debugging;
            Debugging = false;
            try
            {
                if (m_InitLogging)
                {
                    m_InitLogging = false;
                    LogFileMaxSize = 0;
                    LogFileNumBUps = 0;
                }

                switch (func.ToUpper())
                {
                    case "OPEN":
                        string ret = OpenLogFile();
                        PopProc();
                        Debugging = bHoldDebug;
                        return ret;
                    case "CLOSE":
                        LogFileFunc.FileClose();
                        PopProc();
                        Debugging = bHoldDebug;
                        return "Closed";
                    case "LOG":
                        idx = m_ProcCount -2;
                        if(idx < 0)
                            idx = 0;
                        if(ln == "" || ln == null)
                            strLogLine = "";
                        else
                            strLogLine = AppTitle + ":" + SessionID + ":" + UserID + ":" +
                                         DateTime.Now.ToString("yyyyMMdd.hhmmss") + ":" + m_ProcStack[idx] + ":" + ln;
                            //strLogLine = ComputerName + ":" + AppTitle + ":" + SessionID + ":" + UserID + ":" +
                            //             DateTime.Now.ToString("yyyyMMdd.hhmmss") + ":" + m_ProcStack[idx] + ":" + ln;
                        if (bLogging)
                        {
                            if (LogFileFunc.FileTextStream == null)
                                subLogFile("Open", "");
                            LogFileFunc.FileWrite(strLogLine);
                            if (DiscreteLogging)
                                subLogFile("Close", "");
                        }
                        PopProc();
                        Debugging = bHoldDebug;
                        return strLogLine;
                    case "CLEAR":
                        subLogFile("Close", "");
                        if (LogFileFunc.FileExists(m_LogPath, m_LogFile))
                        {
                            LogFileFunc.GetFile(m_LogPath, m_LogFile);
                            LogFileFunc.FileFile.Delete();
                        }
                        subLogFile("Open","");
                        LogEntry("Log File Cleared");
                        subLogFile("Close","");
                        PopProc();
                        Debugging = bHoldDebug;
                        return "Cleared";
                    case "GETNAME":
                        if (LogFileFunc.FileFile != null)
                        {
                            strLogFile = LogFileFunc.FileFile.FullName;
                        }
                        PopProc();
                        Debugging = bHoldDebug;
                        return strLogFile;
                }
            }
            catch
            {
                if (func.ToUpper() == "CLOSE")
                {
                    PopProc();
                    Debugging = bHoldDebug;
                    return "Closed";
                }
            }
            PopProc();
            Debugging = bHoldDebug;
            return "";
        }

        public bool Interactive
        {
            get
            {
                return m_bInteractive;
            }
            set
            {
                m_bInteractive = value;
            }
        }

        public bool IgnoreErrors
        {
            get
            {
                return m_bIgnoreErrors;
            }
            set
            {
                m_bIgnoreErrors = value;
            }
        }

        public bool DesignMode
        {
            get
            {
                return m_bDesignMode;
            }
            set
            {
                m_bDesignMode = value;
            }
        }

        public string LogPath
        {
            get
            {
                //SIN204 return m_LogPath;
                return getLogPath();    //SIN204
            }
            set
            {
                //Start SIN204
                getLogPath(value);
                //string sNewPath;
                //bool bNotGood;
                //sNewPath = "";
                //if (sNewPath == "" || sNewPath == null)
                //    sNewPath = m_AppPath;
                //if (sNewPath == "" || sNewPath == null)
                //    sNewPath = Directory.GetCurrentDirectory();
                //if (StringUtils.Right(sNewPath, 1) != "\\")
                //    sNewPath = sNewPath + "\\";
                //if (sNewPath != m_LogPath)
                //{
                //    bNotGood = true;
                //    while (bNotGood)
                //    {
                //        if (LogFileFunc.FolderExists(sNewPath))
                //        {
                //            subLogFile("Close", "");
                //            m_LogPath = sNewPath;
                //            bNotGood = false;
                //        }
                //        else
                //            if (m_LogPath == "" || m_LogPath == null)
                //                sNewPath = Directory.GetCurrentDirectory();
                //            else
                //                sNewPath = m_LogPath;
                //    }
                //}
                //End SIN204
            }
        }

        //Start SIN204
        public string getLogPath()
        {
            return getLogPath(null);
        }

        public string getLogPath(string sValue)
        {
            string sNewPath;
            bool bNotGood;

            if (sValue != null)
                sNewPath = sValue;
            else
                sNewPath = m_LogPath;

            if (sNewPath == "" || sNewPath == null)
                sNewPath = Parameters.Parameter(Constants.sSwitchLogPath);

            if (sNewPath == "" || sNewPath == null)
                sNewPath = m_AppPath;
            if (sNewPath == "" || sNewPath == null)
                sNewPath = Directory.GetCurrentDirectory();
            if (StringUtils.Right(sNewPath, 1) != "\\")
                sNewPath = sNewPath + "\\";
            if (sNewPath != m_LogPath)
            {
                bNotGood = true;
                int index = 0;
                while (bNotGood)
                {
                    if (!sNewPath.EndsWith("\\"))
                        sNewPath += "\\";
                    if (LogFileFunc.FolderExists(sNewPath))
                    {
                        subLogFile("Close", "");
                        bNotGood = false;
                    }
                    else
                    {
                        switch (index)
                        {
                            case 0:
                                sNewPath = m_AppPath;
                                break;
                            case 1:
                                sNewPath = Directory.GetCurrentDirectory();
                                break;
                            case 2:
                                sNewPath = m_LogPath;
                                bNotGood = false;
                                break;

                        }
                    }
                }
            }
            m_LogPath = sNewPath;
            return m_LogPath;
        }
        //End SIN204

        public string LogFile
        {
            get
            {
                //SIN204 return m_logFile;
                return getLogFile();    //SIN204
            }
            set
            {
                //Start SIN204
                getLogFile(value);
                //string sNewFile;
                //sNewFile = value;
                //if (sNewFile == "" || sNewFile == null)
                //    sNewFile = m_AppTitle;
                //if (sNewFile == "" || sNewFile == null)
                //    sNewFile = Application.ProductName;
                ////if (StringUtils.InStr(sNewFile, ".") == -1)
                //if(sNewFile.IndexOf(".") == -1)
                //    sNewFile = sNewFile + ".LOG";
                //if (sNewFile != m_LogFile)
                //{
                //    subLogFile("Close", "");
                //    m_LogFile = sNewFile;
                //}
                //End SIN204
            }
        }

        //Start SIN204
        public string getLogFile()
        {
            return getLogFile("");
        }

        public string getLogFile(string sValue)
        {
            string sFile;

            if (sValue != "" && sValue != null)
                sFile = sValue;
            else
                sFile = m_LogFile;

            if (sFile == "" || sFile == null)
                sFile = Parameters.Parameter(Constants.sSwitchLogFile);

            if (sFile == "" || sFile == null)
                sFile = m_AppTitle;
            if (sFile == "" || sFile == null)
                sFile = Application.ProductName;

            if (FileFunctions.FileShortName(sFile) == "")
            {
                sFile = m_AppTitle;
                if (FileFunctions.FileName(sFile) == "")
                {
                    sFile = Application.ProductName;
                    if (sFile == "")
                        sFile = "LOGFILE";
                }
            }

            if (!sFile.Contains("."))
                sFile += ".LOG";

            if (sFile != m_LogFile)
                subLogFile("Close", "");

            m_LogFile = sFile;
            return m_LogFile;
        }
        //End SIN204

        // Start SI06023
        //SIN204 public long LogFileRetentionDays
        public int LogFileRetentionDays //SIN204
        {
            get
            //SIN204 { return getLogFileRetentionDays(Convert.ToInt64(m_LogFileRetention)); }
            { return getLogFileRetentionDays(Convert.ToInt32(m_LogFileRetention)); }    //SIN204
            set
            {
                getLogFileRetentionDays(value);
            }
        }
        //SIN204 private long getLogFileRetentionDays(long l)
        private int getLogFileRetentionDays(int l)	//SIN204
        {
            if (l <= 0)
                //SIN204 l = SystemInfo.lGetSystemInfo(Constants.SYSSET_LOGGING, Constants.SYSSET_RETENTION, m_ProfileFile, Parameters.Database, Constants.iLOGFILE_RETN);
                l = Convert.ToInt32(CommonFunctions.CnvData(Parameters.Parameter(Constants.sSwitchLogRetention), CommonFunctions.ufDataTypes.ufdtInteger)); //SIN204

            if (l <= 0)
                l = Constants.iLOGFILE_RETN;

            return l;
        }
        // End SI06023
        public string AppVersion
        {
            get
            {
                return m_AppVersion;
            }
            set
            {
                m_AppVersion = value;
                if (m_AppVersion == "" || m_AppVersion == null)
                    m_AppVersion = Application.ProductVersion;
            }
        }

        public void ClearLog()
        {
            subLogFile("Clear", "");
        }

        public bool Logging
        {
            get
            {
                return bLogging;
            }
            set
            {
                bLogging = value;
                if (bLogging == false)
                    subLogFile("Close", "");
            }
        }

        //Start SIN204
        public void SetupDebug(string sCmdLine)
        {
            bool bInitDebug = true;
            SetupDebug(sCmdLine, bInitDebug);
        }

        //Start SI06650
        private AssemblyName GetAssemblyName()
        {
            Assembly entryAssembly;
            entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly == null)
            {
                var trace = new System.Diagnostics.StackTrace();
                foreach (var frame in trace.GetFrames())
                {
                    var assembly = Assembly.GetCallingAssembly();
                    if (assembly.FullName.ToUpper().Contains("CCP"))
                    {
                        entryAssembly = assembly;
                        //break;
                    }
                }
            }
            return entryAssembly.GetName();
        }
        //End SI06650

        public void SetupDebug(string sCmdLine, bool bInitDebug)
        {
            //SI06650 PushProc("SetupDebug", Assembly.GetEntryAssembly().GetName().Name, null, null, null);
            PushProc("SetupDebug", GetAssemblyName().Name, null, null, null);   //SI06650

            //Get application details
            //SI06650 AppTitle = Assembly.GetEntryAssembly().GetName().Name;
            //SI06650 AppPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().GetName().CodeBase);
            AppTitle = GetAssemblyName().Name;   //SI06650
            AppPath = Path.GetDirectoryName(GetAssemblyName().CodeBase);   //SI06650
            AppVersion = GetCallingAppVersion();

            Errors.Clear();

            //Read current applications config file
            Parameters.ReadParamFile("");

            //Process command line
            if (sCmdLine != null && sCmdLine.Length != 0)
            {
                Parameters.PreserveOnParse();
                Parameters.Parse(sCmdLine);
            }

            //Setup Default Parameters
            Parameters.NoRead = true;
            Debugging = Parameters.Debugging;
            Interactive = true;
            DesignMode = Parameters.DesignMode;
            IgnoreErrors = Parameters.IgnoreErrors;
            DiscreteLogging = true;
            LogPath = Parameters.LogPath;
            LogFile = Parameters.LogFile;
            Logging = Parameters.Logging;
            LogRetry = 10;
            LogFileRetentionDays = Constants.iLOGFILE_RETN;
            LogFileMaxSize = Constants.lLOGFILE_SIZE;
            LogFileNumBUps = Constants.iLOGFILE_NUMB;

            SystemInfo.GetStandardParameters(sCmdLine);
        }

        private string GetCallingAppVersion()
        {
            string version = "";
            //SI06650 version += Assembly.GetEntryAssembly().GetName().Version.Major;
            version += GetAssemblyName().Version.Major;
            version += ".";
            //SI06650 version += Assembly.GetEntryAssembly().GetName().Version.Minor;
            version += GetAssemblyName().Version.Minor;
            version += ".";
            //SI06650 version += Assembly.GetEntryAssembly().GetName().Version.Revision
            version += GetAssemblyName().Version.Revision;
            return version;
        }
        //End SIN204
    }
}
