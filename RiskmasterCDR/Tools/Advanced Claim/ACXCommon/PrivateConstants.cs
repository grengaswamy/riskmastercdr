/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

using CCP.ExceptionTypes;

namespace CCP.Common
{
    internal static class PrivateConstants
    {
        public const string CRYPTKEY = "6378b87457a5ecac8674e9bac12e7cd9";
    } //End Class cPrivateConstants
}
