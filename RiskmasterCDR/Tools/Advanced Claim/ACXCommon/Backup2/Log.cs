using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Logging;


namespace CCP.Common
{
	/// <summary>
	/// Summary description for LoggingManager.
	/// </summary>
	/// 
		public class Log
		{
		
			//BSB Used by EntLib as an error category to route log entries.
			// Note the convention to start with LOG_CATEGORY this  
			// organizes the entries in Intellisense...
			public const string LOG_CATEGORY_SCRIPT = "Scripting";
			public const string LOG_CATEGORY_DEFAULT = "Default";
			
			//Log just a string message with Timestamp
			static public void Write(string sMessage)
			{
				
				if(!sMessage.Equals(""))
				{
					// Creates and fills the log entry with user information
					LogEntry log = new LogEntry();
					log.EventId = 1001;
					log.Message = sMessage;
					log.Category = LOG_CATEGORY_DEFAULT;
					log.Priority = 0;

					// Writes the log entry.
					Logger.Write(log);								
				}
			}
			//Log String Message and select category (log type)
			static public void Write(string sMessage, string sCategory)
			{
				
				if(!sMessage.Equals(""))
				{
					// Creates and fills the log entry with user information
					LogEntry log = new LogEntry();
					log.EventId = 1001;
					log.Message = sMessage;
					log.Category = sCategory;
					log.Priority = 0;

					// Writes the log entry.
					Logger.Write(log);							
					
				}
			}
			static public void Write(LogItem eventItem)
			{
			
				if (eventItem != null)
				{
					try
					{
						// Creates and fills the log entry with user information
						LogEntry log = new LogEntry();
						log.EventId = eventItem.EventId;
						log.Message = eventItem.Message;
						log.Category = eventItem.Category;
						log.Priority = eventItem.Priority;
						
						if((eventItem.ACParamList != null) && (eventItem.ACParamList.Count > 0))
							log.ExtendedProperties = eventItem.ACParamList;

						// Writes the log entry.
						Logger.Write(log);	
						
					}
					finally
					{
						eventItem = null;
					}
				}
			}
		}
		
}
