using System;
using System.Collections;
using CCP.Db;
using CCP.ExceptionTypes;
using System.Data;
using System.Text;
using System.Xml;
using System.IO;

namespace CCP.Common
{
	/// <summary>
	/// Utilities is a static class library of commonly used functions
	/// </summary>
	/// <summary>		
	public class Utilities
	{
		/// <summary>
		/// SoundEx is a simple scheme for assigning searchable 
		/// keys to Names based on Sound instead of spelling.
		/// Initially used by the U.S. Census.  See:
		/// www.archives.gov/research_room/geneology/censuse/soundex.html
		/// for implementation specifics.
		/// </summary>
		/// <param name="lastName"></param>
		/// <returns></returns>
		public static string FindSoundexValue(string lastName)
		{
			string sLastLetter,sLetter;
			string sNewLastName;
			string sTotal;

			if(lastName==null || lastName=="")
				return "0000";

			sNewLastName = lastName.Trim().ToUpper();
			sTotal =sNewLastName.Substring(0, 1);
			sLastLetter = sTotal;
			for (int i = 1; i < sNewLastName.Length; i++)
			{
				sLetter = sNewLastName.Substring(i, 1);
				if (sLetter !=  sLastLetter)
					switch(sLetter)
					{
						case "B": case "F": case "P": case "V":
							sTotal += "1";
							break;
						case "C": case "G": case "J": case  "K": case "S": case "X": case  "Z": case "Q":
							sTotal +="2";
							break;
						case "D": case "T":
							sTotal += "3";
							break;
						case "L":
							sTotal += "4";
							break;
						case "M": case "N":
							sTotal += "5";
							break;
						case "R":
							sTotal += "6";
							break;
					}
				sLastLetter = sLetter;
			} //end foreach character
			if(sTotal.Length >4)
				return sTotal.Substring(0,4);
			else
				return sTotal.PadRight(4,'0');
		}
		/// <summary>		
		/// This method checks whether the string passed is numeric or not		
		/// </summary>		
		/// <param name="p_sCheckString">The string that has to be checked for valid number</param>												
		/// <returns>True/False</returns>
		public  static  bool IsNumeric(string p_sCheckString)
		{
			try 
			{
				p_sCheckString = p_sCheckString.Trim();
				Int32.Parse(p_sCheckString);
			}
			catch 
			{
				return false;
			}
			return true;
		}
		/// <summary>
		/// Appends "," in the input string parameter
		/// </summary>
		/// <param name="p_sText"></param>
		/// <returns>string appended with ',' character</returns>
		public static string AppendComma(string p_sText)
		{
			string sComma = "";
			try
			{
				if (p_sText != "") 
					sComma = "," + p_sText;
				else 
					sComma = "";
			}
			catch 
			{
				return ("");
			}
			return (sComma);
		}
		
		/// <summary>
		/// Finds the index corresponding to the input string in the Arraylist
		/// </summary>
		/// <param name="p_arrlstArray">Arraylist containing display table names</param>
		/// <param name="p_sItem">string for which index is to be find</param>
		/// <returns>Arraylist index corresponding to the input string</returns>
		public static int IndexInArray(ArrayList p_arrlstArray, string p_sItem)
		{
			int iRet =0;
			try
			{
				for (int iCnt = 0; iCnt< p_arrlstArray.Count ;iCnt++)
				{
					if (p_arrlstArray[iCnt].ToString() == p_sItem)
					{
						iRet = iCnt;
						break;
					}
					
				}
			}
			catch
			{
				return (-1);
			}
			return (iRet);
		}

		/// <summary>
		/// Finds whether a string is present in an Arraylist or not
		/// </summary>
		/// <param name="p_arrlstArray">Arraylist containing name of the Query tables</param>
		/// <param name="p_sItem">string containing name of a single Query table</param>
		/// <returns>True if string is present in an ArrayList else false</returns>
		public static bool ExistsInArray(ArrayList p_arrlstArray, string p_sItem)
		{				
			int iCnt = 0;
			bool bRet = false;
			try
			{
				for (iCnt = 0; iCnt<p_arrlstArray.Count; iCnt++)
				{
					if (Convert.ToString(p_arrlstArray[iCnt]) == p_sItem)
					{
						bRet = true;
						break;
					}
				}
			}
			catch
			{
				return (false);
			}
			return (bRet);
		}

		/// <summary>
		/// This function formats a string into a valid date.
		/// </summary>
		/// <returns>True if string is successfully formatted into date.</returns>
		public static bool IsDate(string p_sDate)
		{ 
			bool bRetVal=false;
			try
			{
				DateTime.Parse(p_sDate);
				bRetVal=true;
			}
			catch
			{
				return (false);
			}
			return bRetVal;
		}

		/// Name		: FormatSqlFieldValue
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns a string value to be used for insertion/updation into database
		/// Quotes the string if not empty. Double quotes any single quotes so they will work.
		/// </summary>
		/// <param name="p_sArg">SQL query to be formatted</param>
		/// <returns>Formatted SQL query</returns>
		public static string FormatSqlFieldValue(string p_sArg)
		{
			string sTmp = "";
			try
			{
				if (p_sArg == "")
				{
					sTmp = "null";
				}
				else
				{
					sTmp = "'" + p_sArg.Replace("'", "''") + "'";
				}
			}
			catch {} //Empty catch as simple string manipulation is taking place
			return (sTmp);
		}

		#region Add Delimiter
		/// <summary>
		/// creates string by concatanating a given string i.e. A and another string i.e B seperated by given seperator string
		/// </summary>
		/// <param name="p_sOriginal">string value passed i.e. A</param>
		/// <param name="p_sToAppend">string value to append i.e. B</param>
		/// <param name="p_sSep">seperator string value</param>
		public static void AddDelimited(ref string p_sOriginal, string p_sToAppend, string p_sSep)
		{
			try
			{
				if(p_sSep==null)
				{
					p_sSep=",";
				}
				p_sOriginal=AddSep(p_sOriginal,p_sSep)+p_sToAppend;

			}
			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("Utilities.AddDelimited.GeneralError"),p_objException);
			}
		}

		/// <summary>
		/// Creates a string concatanating a given string value A and a seperator string B
		/// </summary>
		/// <param name="p_sStr">string value passed i.e A</param>
		/// <param name="p_sSep">seperator i.e. B</param>
		/// <returns></returns>
		public static string AddSep(string p_sStr,string p_sSep)
		{
			string sRetVal="";
			try
			{
				if(p_sSep==null)
				{
					p_sSep=",";
				}
				sRetVal=p_sStr!="" ? p_sStr+p_sSep : p_sStr;
			}
			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("Utilities.AddSep.GeneralError"),p_objException);
			}
			return sRetVal;
		}
		#endregion

		#region Get Next Unique ID for a database table

		/// Name			: GetNextUID
		/// Author			: Rajeev Chauhan
		/// Date Created	: 08-Feb-2005
		/// **************************************************************************
		/// Amendment History
		/// **************************************************************************
		/// Date Amended	*   Amendment							*   Author
		///					*										*
		///	10-Feb-2005		*	Pass the open connection and close	*	Rajeev Chauhan
		/// **************************************************************************
		/// <summary>
		/// Retrieves and increments the current unique id counter for 
		/// "p_sTableName" from the AC Glossary.
		/// </summary>
		/// <param name="p_sConnectString">
		///		Database connection string.
		///	</param>
		/// <param name="p_sTableName">
		///		The name of the table who's next unique row id should be
		///		retrieved and incremented.
		///	</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		public static int GetNextUID(string p_sConnectString, string p_sTableName)
		{
			DbConnection oCn = null;
			try
			{
				oCn = Db.DbFactory.GetDbConnection(p_sConnectString);
				oCn.Open(); 
				return GetNextUID(oCn, p_sTableName);
			}
			catch(Exception p_oException)
			{
				throw new ACAppException(Globalization.GetString("Utilities.GetNextUID.ConnectError"), p_oException);
			}
			finally
			{
				if(oCn != null)
				{
					oCn.Close();
					oCn.Dispose();
				}
			}
		}


		/// Name			: GetNextUID
		/// Author			: Aditya Babbar
		/// Date Created	: 10-Nov-2004
		/// ***********************************************************************
		/// Amendment History
		/// ***********************************************************************
		/// Date Amended	*   Amendment						*    Author
		///					*									*
		///	08-Feb-2005		*	Added connection in parameter	*    Rajeev Chauhan	
		///	10-Feb-2005		*	Accepts the open connection and *    Rajeev Chauhan
		///					*   leaves the connection opened	*    	
		///	08-Sep-2005		*	Modified to call overloaded		*	 Aditya Babbar
		///						GetNextUID() with null			*
		///						DbTransaction object			*    
		/// ***********************************************************************
		/// <summary>
		/// Retrieves and increments the current unique id counter for 
		/// "p_sTableName" from the AC Glossary.
		/// </summary>
		/// <param name="p_oDBConnection">
		///		Connection object assumed to be open
		///	</param>
		/// <param name="p_sTableName">
		///		The name of the table who's next unique row id should be
		///		retrieved and incremented.
		///	</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		/// <remarks>Let the connection be open</remarks> 
		public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName)
		{			
			return GetNextUID(p_oDBConnection,p_sTableName,null);  
		}


		/// Name			: GetNextUID
		/// Author			: Aditya Babbar
		/// Date Created	: 08-Sep-2004
		/// ***********************************************************************
		/// Amendment History
		/// ***********************************************************************
		/// Date Amended	*   Amendment						*    Author
		///					*									*
		/// ***********************************************************************
		/// <summary>
		/// Retrieves and increments the current unique id counter for 
		/// "p_sTableName" from the AC Glossary.
		/// </summary>
		/// <param name="p_oDBConnection">
		///		Connection object assumed to be open
		///	</param>
		/// <param name="p_sTableName">
		///		The name of the table who's next unique row id should be
		///		retrieved and incremented.
		///	</param>
		///	<param name="p_objTrans">
		///		Existing transaction context.
		///	</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		/// <remarks>Let the connection be open</remarks> 
		public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName, DbTransaction p_objTrans)
		{
			string sSQL=string.Empty;
			int iTableID=0;
			int iNextUID=0;
			int iOrigUID=0;
			int iRows=0;
			int iCollisionRetryCount=0;
			int iErrRetryCount=0;
			
			DbReader objDbReader = null;

			try
			{
				do
				{
					if(p_oDBConnection.State != System.Data.ConnectionState.Open)  
						p_oDBConnection.Open();
					
					//Get Current Id.
					objDbReader = p_oDBConnection.ExecuteReader("SELECT TABLE_ID, NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'",p_objTrans);
					if(!objDbReader.Read())
					{	
						objDbReader.Close();
						objDbReader = null;
						throw new ACAppException(Globalization.GetString("Utilities.GetNextUID.Exception.NoSuchTable"));
					}
					iTableID = objDbReader.GetInt("TABLE_ID");
					iNextUID = objDbReader.GetInt("NEXT_UNIQUE_ID");
					objDbReader.Close();
					objDbReader = null;
            
					// Compute next id
					iOrigUID = iNextUID;
					if(iOrigUID !=0)
						iNextUID++;
					else 
						iNextUID = 2;
        
					// try to reserve id (searched update)
					sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextUID 
						+ " WHERE TABLE_ID = " + iTableID;
    
					// only add searched clause if no chance of a null originally
					// in row (only if no records ever saved against table)   JP 12/7/96
					if(iOrigUID !=0)
						sSQL +=" AND NEXT_UNIQUE_ID = " + iOrigUID;
                
					// Try update
					try
					{
						iRows = p_oDBConnection.ExecuteNonQuery(sSQL,p_objTrans);
					}
					catch(Exception e)
					{
						iErrRetryCount++;
						if (iErrRetryCount >= 5)
						{
							throw new ACAppException(Globalization.GetString("Context.GetNextUID.Exception.ErrorModifyingDB"), e);
						}
					}
					// if success, return
					if( iRows == 1)
					{
						return iNextUID - 1;
					}
					else
					{
						// collided with another user - try again (up to 1000 times)
						iCollisionRetryCount++;   
						// Close the connection to be reopened
						p_oDBConnection.Close();
					}
				}
				while ((iErrRetryCount < 5) && (iCollisionRetryCount < 1000));
     
				if(iCollisionRetryCount >= 1000)
				{
					throw new ACAppException(Globalization.GetString("Utilities.GetNextUID.Exception.CollisionTimeout"));
				}
			}
			catch(Exception p_oException)
			{
				throw new ACAppException(Globalization.GetString("Utilities.GetNextUID.Exception"), p_oException);
			}
			//shouldn't get here under normal conditions.
			return 0;  
		}


		#endregion

		#region BSB Xml functions
		/// <summary>
		/// Picks an element from srcDom using sXPath to be returned as it's own new document.
		/// </summary>
		/// <param name="sXPath"></param>
		/// <param name="srcDom"></param>
		/// <returns></returns>
		public static  XmlDocument XPointerDoc(string sXPath, XmlDocument srcDom)
		{
			XmlDocument ret = null;
			XmlNode nd = srcDom.SelectSingleNode(sXPath);
			if(nd!=null)
			{
				ret =  new XmlDocument();
				ret.LoadXml(nd.OuterXml);
			}
			return ret;
		}
		/// <summary>
		/// Copies an element into it's own new XmlDocument..
		/// </summary>
		/// <param name="elt"></param>
		/// <returns></returns>
	
		public static XmlDocument XmlElement2XmlDoc(XmlElement elt)
		{
			XmlDocument ret = null;
			if(elt!=null)
			{
				ret =  new XmlDocument();
				ret.LoadXml(elt.OuterXml);
			}
			return ret;
		}
		
		#endregion

		#region Get difference between two dates in years
		/// Name		: GetDateDiff
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the difference between two dates in years
		/// </summary>
		/// <param name="p_sDateFrom">From date</param>
		/// <param name="p_sDateTo">To date</param>
		/// <returns>Difference between two dates in years</returns>
		public static int GetDateDiffInYears(string p_sDateFrom,string p_sDateTo)
		{
			DateTime datFrom=CCP.Common.Conversion.ToDate(p_sDateFrom);
			DateTime datTo=CCP.Common.Conversion.ToDate(p_sDateTo);
			return datTo.Year-datFrom.Year;
		}
		#endregion
		/// Name			: ConvertFilestreamToMemorystream
		/// Author			: Tanuj Narula
		/// Date Created	: 30-Mar-2005
		/// **************************************************************************
		/// Amendment History
		/// **************************************************************************
		/// Date Amended	*   Amendment							*   Author
		/// **************************************************************************
		/// <summary>
		/// This method converts file stream to memory stream.
		/// </summary>
		/// <param name="p_sFilePath">Name of the file to be converted to memory stream.</param>
		/// <returns>Memory stream</returns>
		public static MemoryStream ConvertFilestreamToMemorystream(string p_sFilePath)
		{
			MemoryStream objMs=new MemoryStream();
			FileStream objFilestream=null;
			byte[] arrBytes=new byte[1024];
			try
			{
				if(p_sFilePath==null || p_sFilePath.Trim()=="")
				{
					throw new ACAppException(Globalization.GetString("Utilities.ConvertFilestreamToMemorystream.FilePathError"));
				}
				objFilestream=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);
				while(true)
				{
					int iNoOfBytes=objFilestream.Read(arrBytes,0,1024);
					if(iNoOfBytes==0)
					{
						break;
					}
					objMs.Write(arrBytes,0,iNoOfBytes);
				}
				objFilestream.Close();
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objFilestream!=null)
				{
					objFilestream.Close();
					objFilestream=null;
				}
				arrBytes=null;
			}
			return objMs;
		}

		/// <summary>
		///GetOrgTableName returns the Table Name.
		/// </summary>
		/// <param name="p_iTableId">Table ID for which the Table Name is required</param>
		public static string GetOrgTableName(int p_iTableId)
		{	
			switch (p_iTableId)
			{
				case 1005:
					return "CLIENT";
				case 1006:
					return "COMPANY";
				case 1007:
					return "OPERATION";
				case 1008:
					return "REGION";
				case 1009:
					return "DIVISION";
				case 1010:
					return "LOCATION";
				case 1011:
					return "FACILITY";
				case 1012:
					return "DEPARTMENT";
				default:
					return "";
			}
		}

		/// Name			: ReadFileStream
		/// Author			: Jasbinder Singh Bali
		/// Date Created	: 05-May-2005
		/// **************************************************************************
		/// Amendment History
		/// **************************************************************************
		/// Date Amended	*   Amendment							*   Author
		/// **************************************************************************
		/// <summary>
		/// This method read a file stream 
		/// </summary>
		/// <param name="p_sFilePath">Name of the file to be be read.</param>
		/// <returns>String (File Content)</returns>
		public static string ReadFileStream(string p_sFilePath)
		{
			FileStream objFileStream = null;	
			StreamReader objStreamReader = null;
			string FileContent;
			
			try
			{
				if(p_sFilePath==null || p_sFilePath.Trim()=="")
				{
					throw new ACAppException(Globalization.GetString("Utilities.ReadFileStream.FilePathError"));
				}
				objFileStream=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);

				objStreamReader = new StreamReader(objFileStream);
				
				FileContent = objStreamReader.ReadToEnd();
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objFileStream!=null)
				{
					objFileStream.Close();	
					objStreamReader.Close();
				}
				
			}
			return FileContent;
		}

		/// Name		: FileTransfer
		/// Author		: Ratheen Chaturvedi
		/// Date Created:13-September-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <param name="p_sValue">Value</param>
		/// <param name="p_sFieldName">Field name</param>
		/// <summary>
		/// writes the file on the hard disk
		/// </summary>
		/// <param name="p_sXml">Xml string containing report data</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public static bool FileTranfer(string p_sFileContent,string p_sFileName)
		{			
			try
			{	
				if((p_sFileContent!="")&&(p_sFileName!=""))
				{
					char[] b64Data=p_sFileContent.ToCharArray();
					byte[] decodeData=Convert.FromBase64CharArray(b64Data,0,b64Data.Length);
					FileStream fs = new FileStream(p_sFileName, FileMode.Create, FileAccess.Write, FileShare.None);
					fs.Write(decodeData, 0, decodeData.Length);
					fs.Close();
					return true;
				}
				return false;
			}
			catch (Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("UploadFile.FileTransfer.Error"),p_objException);
			}
		}
	}
}
