/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 11/10/2008 | SI06468 |    ASM     | XML Code ID Values
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace CCP.Common
{
    public class CommonFunctions
    {
        private Debug m_Dbg;

        public CommonFunctions()
        {
            m_Dbg = null;
        }

        public Debug Debug
        {
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    //m_Dbg.Functions = this;
                }
                return m_Dbg;
            }
            set
            {
                if ((m_Dbg != null) && (value != null) && (m_Dbg.DebugGUID == value.DebugGUID))
                    return;
                if (value == null)
                {
                    m_Dbg = new Debug();
                    //m_Dbg.Functions = this;
                }
                else
                    m_Dbg = value;
            }
        }

        public Debug Dbg
        {
            get
            {
                return Debug;
            }
        }// Start SI06023
        public enum ufDataTypes
        {
            ufdtString,
            ufdtInteger,
            ufdtLong,
            ufdtDecimal,
            ufdtDouble,
            ufdtCurrency,
            ufdtDate,
            ufdtBool,
            ufdtByte,
            ufdtSingle,
            ufdtTime,
            ufdtDTTM,
        }
        public Errors Errors
        {
            get
            {
                return Dbg.Errors;
            }
        }
        // end SI06023
        public string ResolveConstantsWParray(string sString, string[] vArgArray)
        {
            string[] v = new string[vArgArray.GetUpperBound(0) + 1];
            int i;
            if (vArgArray.GetUpperBound(0) >= vArgArray.GetLowerBound(0))
            {
                for (i = 0; i < vArgArray.Length; i++)
                {
                    v[i] = vArgArray[i];
                }
            }
            else
            {
                v = new string[1];
                v[0] = "";
            }
            return ResolveConstants(sString, v);
        }

        public string ResolveConstants(string sString, string[] vArgArray)
        {
            string sStr;
            int i, j, k;

            sStr = sString;

            i = sStr.IndexOf("<!");
            //SIN204 while (i > 0)
            while (i > -1)  //SIN204
            {
                j = sStr.IndexOf("!>");
                //SIN204 if (j == 0)
                if (j == -1)    //SIN204
                    j = sStr.Length;
                sStr = StringUtils.Left(sStr, i - 1) + StringUtils.Mid(sStr, j + 2);
                i = sStr.IndexOf("<!");
            }
            i = sStr.IndexOf("<*");
            //SIN204 while (i > 0)
            while (i > -1)  //SIN204
            {
                j = sStr.IndexOf("*>");
                //SIN204 if (j == 0)
                if (j == -1)    //SIN204
                    j = sStr.Length;
                sStr = StringUtils.Left(sStr, i - 1) + StringUtils.Mid(sStr, j + 2);
                i = sStr.IndexOf("<*");
            }

            for (j = 1; j <= vArgArray.Length; j++)
            {
                i = sStr.IndexOf("<%" + j + ">");
                k = 0;
                //SIN204 while (i != 0 && k <= 2)
                while (i != -1 && k <= 2)   //SIN204
                {
                    sStr = sStr.Replace("<%" + j + ">", vArgArray[j - 1]);
                    i = sStr.IndexOf("<%" + j + ">");
                    k++;
                }
            }

            //Start SIN204
            while (sStr.IndexOf("<CRLF>") > -1)
                sStr = sStr.Replace("<CRLF>", "\r\n");
            while (sStr.IndexOf("<LF>") > -1)
                sStr = sStr.Replace("<LF>", "\n");
            while (sStr.IndexOf("<FF>") > -1)
                sStr = sStr.Replace("<FF>", "\f");
            while (sStr.IndexOf("<TAB>") > -1)
                sStr = sStr.Replace("<TAB>", "\t");
            //Start SI06468
            while (sStr.IndexOf("<B>") > -1)
                sStr = sStr.Replace("<B>", "");
            while (sStr.IndexOf("<BLANK>") > -1)
                sStr = sStr.Replace("<BLANK>", "");
            while (sStr.IndexOf("<SPACE>") > -1)
                sStr = sStr.Replace("<SPACE>", " ");
            //End SI06468
            while (sStr.IndexOf("<TODAY>") > -1)
                sStr = sStr.Replace("<TODAY>", DateTime.Now.ToString("yyyyMMdd"));
            while (sStr.IndexOf("<NOW>") > -1)
                sStr = sStr.Replace("<NOW>", DateTime.Now.ToString("hhmmss"));
            while (sStr.IndexOf("<DTTM>") > -1)
                sStr = sStr.Replace("<DTTM>", DateTime.Now.ToString("yyyyMMddhhmmss"));
            while (sStr.IndexOf("<PATH>") > -1)
                sStr = sStr.Replace("<PATH>", Dbg.AppPath);
            while (sStr.IndexOf("<APP>") > -1)
                sStr = sStr.Replace("<APP>", Dbg.AppTitle);
            while (sStr.IndexOf("<APPVER>") > -1)
                sStr = sStr.Replace("<APPVER>", Dbg.AppVersion);
            while (sStr.IndexOf("<APP.VERSION>") > -1)
                sStr = sStr.Replace("<APP.VERSION>", Dbg.AppVersion);
            while (sStr.IndexOf("<APP.PATH>") > -1)
                sStr = sStr.Replace("<APP.PATH>", Dbg.AppPath);
            while (sStr.IndexOf("<APP.NAME>") > -1)
                sStr = sStr.Replace("<APP.NAME>", Dbg.AppTitle);
            while (sStr.IndexOf("<LOGPATH>") > -1)
                sStr = sStr.Replace("<LOGPATH>", Dbg.LogPath);
            while (sStr.IndexOf("<LOGFILE>") > -1)
                sStr = sStr.Replace("<LOGFILE>", Dbg.LogFile);
            while (sStr.IndexOf("<LOG.PATH>") > -1)
                sStr = sStr.Replace("<LOG.PATH>", Dbg.LogPath);
            while (sStr.IndexOf("<LOG.FILE>") > -1)
                sStr = sStr.Replace("<LOG.FILE>", Dbg.LogFile);
            //End SIN204

            return sStr.Trim();
        }

        public string ResolveStringValues(string sString, string[] sArgArray)
        {
            string sStr;
            sStr = ResolveConstants(sString, sArgArray);
            //sStr = ResolveFunctions(sStr); //Needs to be coded
            return sStr.Trim();
        }
		// Start SI06023
        public string RemoveString(string sStrg, string sFindStrg, int iStrt, int iNum)
        {
            int i;
            string l;
            string r;
            string s;
            int c = 0;

            if (iStrt < 0 || iStrt > sStrg.Length)
            {
                RemoveString(sStrg, "", 1, 999);
                return "";
            }
            s = sStrg;
            i = StringUtils.InStrPos(s, sFindStrg, iStrt);

            //SIN204 while (i != 0)
            while (i != -1) //SIN204
            {
                c = c + 1;
                l = StringUtils.Left(s, i - 1);
                r = StringUtils.Right(s, s.Length - i + 1 - sFindStrg.Length);
                s = l + r;
                if (c >= iNum) break;
                i = StringUtils.InStrPos(s, sFindStrg, iStrt);
            }
            return s;

        }
        public string ReplaceString(string sStrg, string sOStrg, string sNStrg, int iStrt, int iNum)
        {
            int i;
            int c = 0;
            string l, r, s;

            if (iStrt < 0 || iStrt > sStrg.Length)
                return sStrg;

            s = sStrg;
            i = s.IndexOf(sOStrg, iStrt);

            //SIN204 while (i != 0)
            while (i != -1) //SIN204
            {
                c = c + 1;
                //SIN204 l = s.Substring(0, i);
                l = StringUtils.Left(s, i); //SIN204
                //SIN204 r = s.Substring(s.Length - i + 1 - sOStrg.Length);
                r = StringUtils.Right(s, (s.Length - i) - sOStrg.Length);	//SIN204
                s = l + sNStrg + r;
                if (c >= iNum)
                    break;
                i = s.IndexOf(sOStrg, iStrt);
            }
            return s;
        }
        public bool IsFalse(string sInString)
        {
            //SIN204 if (StringUtils.InStrPos(Constants.sBoolFalse, ":" + sInString.ToUpper() + ":", 1) < 0)
            if (StringUtils.InStrPos(Constants.sBoolFalse, ":" + sInString.ToUpper() + ":", 1) != -1)   //SIN204
                return true;
            else
                return false;
        }
        public bool IsBool(string sInString)
        {
            //SIN204 if (StringUtils.InStrPos(Constants.sBoolFalse+Constants.sBoolTrue, ":" + sInString.ToUpper() + ":", 1) != 0)
            if (StringUtils.InStrPos(Constants.sBoolFalse, ":" + sInString.ToUpper() + ":", 1) != -1 ||
                StringUtils.InStrPos(Constants.sBoolTrue, ":" + sInString.ToUpper() + ":", 1) != -1)    //SIN204
                return true;
            else
                return false;
        }
        public string CnvData(string indata, ufDataTypes totype)
        {
            try
            {
                object inv;
                string vlu, ini, ins;
                int i;
                if (indata == null || indata.ToString() == "")
                {
                    inv = "";
                    ini = "0";
                    vlu = "";
                }
                else
                {
                    if (indata.GetType().Name == "String")
                        indata = indata.Trim();
                    inv = indata;
                    ini = indata;
                    vlu = indata;
                }
                switch (totype)
                {
                    case ufDataTypes.ufdtString:
                        vlu = inv.ToString();
                        break;
                    case ufDataTypes.ufdtInteger:
                    case ufDataTypes.ufdtLong:
                    case ufDataTypes.ufdtDouble:
                    case ufDataTypes.ufdtDecimal:
                    case ufDataTypes.ufdtCurrency:
                        bool bNegative = false;
                        bool bPercent = false;
                        ini = ini.Replace(",", "");
                        ini = ini.Replace("(", "");
                        ini = ini.Replace(")", "");
                        ini = ini.Replace("$", "");
                        i = ini.IndexOf('+', 1);
                        //SIN204 if (i > 0)
                        if (i > -1) //SIN204
                        {
                            ini = ini.Replace("+", "");
                            bNegative = false;
                        }
                        i = ini.IndexOf('-', 1);
                        //SIN204 if (i > 0)
                        if (i > -1) //SIN204
                        {
                            ini = ini.Replace("-", "");
                            bNegative = true;
                        }
                        i = ini.IndexOf('%', 1);
                        //SIN204 if (i > 0)
                        if (i > -1) //SIN204
                        {
                            ini = ini.Replace("%", "");
                            bPercent = true;
                        }
                        if (!NumericUtils.IsNumeric(ini)) ini = "0";

                        switch (totype)
                        {
                            case ufDataTypes.ufdtSingle:
                                vlu = Convert.ToSingle(ini).ToString();
                                break;
                            case ufDataTypes.ufdtCurrency:
                                vlu = ini.ToString(); //currency conversion in c#
                                break;
                            case ufDataTypes.ufdtDecimal:
                                vlu = Convert.ToDecimal(ini).ToString();
                                break;
                            case ufDataTypes.ufdtInteger:
                                vlu = Convert.ToInt32(ini).ToString();
                                break;
                            case ufDataTypes.ufdtLong:
                                vlu = Convert.ToInt64(ini).ToString();
                                break;
                            case ufDataTypes.ufdtDouble:
                                vlu = Convert.ToDouble(ini).ToString();
                                break;
                        }
                        if (bNegative)
                            vlu = Convert.ToString((Convert.ToInt32(vlu) * -1));
                        if (bPercent)
                            vlu = Convert.ToString((Convert.ToInt32(vlu) / 100));
                        break;
                    case ufDataTypes.ufdtDTTM:
                        if (DateTimeUtils.IsDate(inv.ToString()))
                            vlu = (Convert.ToDateTime(inv)).Date.ToString("yyyymmdd");
                        else
                        {
                            ins = inv.ToString() + "00000000000000";
                            //SIN204 if (ins.IndexOf('/', 1) == 0)
                            if (ins.IndexOf('/', 1) == -1)  //SIN204
                            {
                                DateTime dt = new DateTime(Convert.ToInt32(ins.Substring(0, 4)), Convert.ToInt32(ins.Substring(5, 2)), Convert.ToInt32(ins.Substring(7, 2)), Convert.ToInt32(ins.Substring(9, 2)), Convert.ToInt32(ins.Substring(11, 2)), Convert.ToInt32(ins.Substring(13, 2)));
                                inv = dt.Date.ToString("yyyymmdd") + " " + dt.Date.ToString("hhmmss");
                            }
                            if (DateTimeUtils.IsDate(inv.ToString()))
                                vlu = (Convert.ToDateTime(inv.ToString())).Date.ToString("yyyymmdd");
                        }
                        break;
                    case ufDataTypes.ufdtDate:
                        if (DateTimeUtils.IsDate(inv.ToString()))
                            vlu = (Convert.ToDateTime(inv.ToString())).Date.ToString("yyyymmdd");
                        else
                        {
                            ins = inv.ToString();
                            //SIN204 if (ins.IndexOf('/', 1) == 0)
                            if (ins.IndexOf('/', 1) == -1)  //SIN204
                            {
                                if (ins.Length >= 8)
                                {
                                    DateTime dt = new DateTime(Convert.ToInt32(ins.Substring(0, 4)), Convert.ToInt32(ins.Substring(5, 2)), Convert.ToInt32(ins.Substring(7, 2)));
                                    inv = dt.Date.ToString("yyyymmdd");
                                }
                                else if (ins.Length >= 6)
                                {
                                    DateTime dt = new DateTime(Convert.ToInt32("20" + ins.Substring(0, 1)), Convert.ToInt32(ins.Substring(3, 2)), Convert.ToInt32(ins.Substring(5, 2)));
                                    inv = dt.Date.ToString("yyyymmdd");
                                }
                            }
                            if (DateTimeUtils.IsDate(inv.ToString()))
                                vlu = (Convert.ToDateTime(inv.ToString())).Date.ToString("yyyymmdd");
                        }
                        break;
                    case ufDataTypes.ufdtTime:
                        if (DateTimeUtils.IsDate(inv.ToString()))
                            vlu = (Convert.ToDateTime(inv.ToString())).TimeOfDay.ToString();
                        else
                        {
                            ins = inv.ToString();
                            //SIN204 if (ins.IndexOf(':', 1) == 0)
                            if (ins.IndexOf(':', 1) == -1)  //SIN204
                            {
                                //SIN204 if (ins.IndexOf(' ', 1) == 0)
                                if (ins.IndexOf(' ', 1) == -1)  //SIN204
                                {
                                    if (ins.Length >= 6)
                                    {
                                        DateTime dt = new DateTime(1900, 01, 01, Convert.ToInt32(ins.Substring(1, 2)), Convert.ToInt32(ins.Substring(3, 2)), Convert.ToInt32(ins.Substring(5, 2)));
                                        inv = dt.TimeOfDay.ToString();
                                    }
                                    else if (ins.Length >= 4)
                                    {
                                        DateTime dt = new DateTime(1900, 01, 01, Convert.ToInt32(ins.Substring(1, 2)), Convert.ToInt32(ins.Substring(3, 2)), 0);
                                        inv = dt.TimeOfDay.ToString();
                                    }
                                }
                                else if (ins.IndexOf(' ', 1) >= 6)
                                    inv = ins.Substring(1, 2) + ":" + ins.Substring(3, 2) + ":" + ins.Substring(5, 2) + " " + ins.Substring(7, 2);
                                else if (ins.IndexOf(' ', 1) >= 4)
                                    inv = ins.Substring(1, 2) + ":" + ins.Substring(3, 2) + " " + ins.Substring(5);
                            }
                            if (DateTimeUtils.IsDate(inv.ToString()))
                                vlu = (Convert.ToDateTime(inv.ToString())).TimeOfDay.ToString();
                        }
                        break;
                    case ufDataTypes.ufdtBool://inner code pending                       
                        if (inv is string)
                        {
                            if (inv.ToString() == "")
                                inv = false;
                            //SIN204 else if (StringUtils.InStrPos(Constants.sBoolFalse, ":" + inv.ToString().ToUpper() + ":", 1) != 0)
                            else if (StringUtils.InStrPos(Constants.sBoolFalse, ":" + inv.ToString().ToUpper() + ":", 1) != -1) //SIN204
                                inv = false;
                            //SIN204 else if (StringUtils.InStrPos(Constants.sBoolTrue, ":" + inv.ToString().ToUpper() + ":", 1) != 0)
                            else if (StringUtils.InStrPos(Constants.sBoolTrue, ":" + inv.ToString().ToUpper() + ":", 1) != -1)  //SIN204
                                inv = true;
                            else
                                inv = false;

                        }
                        else if (inv == null) { inv = false; }
                        else if (inv is Int32 || inv is Int64 || inv is Int16 || inv is Single || inv is Decimal)
                        {
                            if (Convert.ToInt32(inv.ToString()) == 0)
                                inv = false;
                            else
                                inv = true;
                        }
                        vlu = Convert.ToBoolean(ini).ToString();
                        break;
                    case ufDataTypes.ufdtByte:
                        vlu = Convert.ToByte(ini).ToString();
                        break;
                    case ufDataTypes.ufdtSingle:
                        vlu = Convert.ToSingle(ini).ToString();
                        break;
                }
                return vlu;
            }
            catch (Exception e)
            {
                Debug.Errors.ProcessError(CCP.Constants.ErrorGlobalConstants.ERRC_INVALID_VALUE, Debug.AppTitle + ".CnvData", e);
                return "";
            }
        }
        public string ComputerName()
        {
            string sCompName = "";
            string sName;
            //pending
            //GetComputerName(sCompName, sCompName.Length);
            sCompName = ReplaceString(sCompName, Convert.ToChar(0).ToString(), " ", 1, 999);
            sName = sCompName.Trim();
            return sName;
        }
        public string WSLanguageCode
        {
            get
            {
                string sLang ="";
                //pending
                //RegisterQueryValueEx(HKEY_LOCAL_MACHINE, CCP_REG_KEY, CCP_REG_VAL_LANGUAGE, sLang);
                if (sLang == "")
                    sLang = Constants.SYS_DFLT_LANGUAGE_CODE;
                return sLang;
            }
            set
            {
                string sLang;
                if (value != "")
                    sLang = value;
                else
                    sLang = Constants.SYS_DFLT_LANGUAGE_CODE;
                //pending
                //RegisterSetKeyValue(CCP_REG_KEY, CCP_REG_VAL_LANGUAGE, sLang);
            }
        }

        // end SI06023
    }
}
