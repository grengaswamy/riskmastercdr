using System;
using System.Xml;
using System.IO;
using CCP.ExceptionTypes;
using System.Collections; 
using System.Collections.Specialized;
using System.Configuration;
using GotDotNet.XInclude;
namespace CCP.Common
{
	/// <summary>
	///Author  :   Sumeet Rathod & Parag Sarin
	///Dated   :   5th Oct 2004
	///Purpose :   This class reads & loads the configuration information.
	/// </summary>
	public class ACConfigurator
	{
		/// <summary>
		/// XML document containing configuration information
		/// </summary>
		private static XmlDocument m_objDoc;
		private const string APP_CONFIG_PATH = "ACConfigPath";

		private const string STANDARD_PATHS_NODE_NAME = "StandardPaths";
		private const string BASE_PATH_NODE_NAME = "BasePath";
		private const string SEPARATOR = "%";


		/// <summary>
		/// Static Constructor.
		/// Loads the custom AC.config file into m_objDoc
		/// </summary>
		static ACConfigurator(){LoadFile();}

		/// <summary>
		/// Loads the configuration file
		/// First attempt is to look in the applicable .Net config file using the setting APP_CONFIG_PATH
		/// stored there to pick up the desired configuration file location.
		/// Second attempt is to look in the current directory.
		/// Third attempt is taken from the structure of the Source Code in the AC .Net Solution file.
		/// </summary>
		private static void LoadFile()
		{
			ACConfigurator.m_objDoc = new XmlDocument();
			string sConfigPath = "";

			try{sConfigPath = ConfigurationSettings.AppSettings[APP_CONFIG_PATH];}
			catch{}

			if(!File.Exists(sConfigPath))
				sConfigPath = AppDomain.CurrentDomain.BaseDirectory + "\\AC.config";
			 
			if(!File.Exists(sConfigPath))
				sConfigPath = AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\..\\..\\AC.config";

			if(!File.Exists(sConfigPath))
				throw new FileInputOutputException(String.Format(Globalization.GetString("ACConfigurator.LoadFile.FileNotFound"),System.AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,sConfigPath));

			try
			{
				//BSB 09.02.2005 Added logic for X-Inlcude processing.
				m_objDoc.Load(new XIncludingReader(sConfigPath));

				//m_objDoc.Load(sConfigPath);
				
				// Aditya - 29-Aug-2005 - Subsitute standard paths in m_objDoc
				ReplaceStandardPathsInXmlCache();
				
			}
			catch(ACAppException p_objACAppException)
			{
				throw p_objACAppException;
			}
			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.LoadFile.GeneralError"),p_objException);
			}
		}


		/// Target XML Like:
		/// <StandardPaths>
		///		<BasePath>c:\AC</BasePath>
		///		<CWSPath>%BasePath%\cws</CWSPath>
		///		<LogPath>%BasePath%\logs</LogPath>
		///		<AppFilesPath>%BasePath%\appfiles</AppFilesPath>
		///		<UserDataPath>%BasePath%\userdata</UserDataPath>
		///		<TempPath>%BasePath%\temp</TempPath>
		///	</StandardPaths>
		private static void ReplaceStandardPathsInXmlCache()
		{
			XmlElement objRootElement = m_objDoc.DocumentElement; 

			XmlNode objPathsNode = objRootElement.SelectSingleNode(STANDARD_PATHS_NODE_NAME);
			if(objPathsNode==null)
				throw new ACAppException(String.Format(Globalization.GetString("ACConfigurator.NodeNotFoundError"),STANDARD_PATHS_NODE_NAME));
			
			XmlNode objBasePathNode = objPathsNode.SelectSingleNode(BASE_PATH_NODE_NAME);
			if(objBasePathNode==null)
				throw new ACAppException(String.Format(Globalization.GetString("ACConfigurator.NodeNotFoundError"),BASE_PATH_NODE_NAME));
			
			//Process the "BasePath" preliminary substitution into other Path Entries.
			string sBasePath = objBasePathNode.InnerText;
			string sXML = objPathsNode.InnerXml;
			//Apply the Change
			objPathsNode.InnerXml = sXML.Replace(SEPARATOR+BASE_PATH_NODE_NAME+SEPARATOR,sBasePath);

			//Process the "BasePath" and remaining substitutions across the entire document.
			sXML = objRootElement.InnerXml;
			foreach(XmlNode objXmlNode in objPathsNode.ChildNodes)
				sXML = sXML.Replace(SEPARATOR+objXmlNode.Name+SEPARATOR,objXmlNode.InnerText);
			//Apply the Changes
			objRootElement.InnerXml= sXML;

			return;
		}

		#region Configuration Accessors
		/// <summary>
		/// Gets node value from XML  Deprecated - use ACConfigurator.Value() instead.
		/// </summary>
		/// <param name="p_sXmlNode">Node name</param>
		/// <returns>value for passed node</returns>
		public string GetValue(string p_sXmlNode) {return ACConfigurator.Value(p_sXmlNode); }

		static public string Value(string p_sXmlNode)
		{
			try
			{
				XmlNode objXmlTargetNode = ACConfigurator.m_objDoc.DocumentElement.SelectSingleNode("//"+p_sXmlNode);
				return (string)(objXmlTargetNode ==null ? null : objXmlTargetNode.InnerXml);
			}
			catch(Exception e)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.GetValue.Exception"),e);
			}
		}

		/// <summary>
		/// Gets node  from AC.config.  Deprecated - use ACConfigurator.NamedNode() instead.
		/// </summary>
		/// <param name="p_sNodeName">Node name</param>
		/// <returns>Node for passed nodename</returns>
		public XmlNode GetNamedNode(string p_sNodeName){return ACConfigurator.NamedNode(p_sNodeName);}

		static public XmlNode NamedNode(string p_sNodeName)
		{
			try
			{
				return m_objDoc.SelectSingleNode("//" + p_sNodeName);
			}
			catch(Exception e)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.GetNamedNode.Exception"),e);
			}
		}

		/// <summary>
		/// Gets a NameValueCollection from AC.config file.
		/// Note: Node requested must have the proper form and be marked with the attribute:   type="NameValueCollection"
		/// Otherwise an empty NameValueCollection is returned.
		/// </summary>
		/// <param name="p_sXmlNode">Node name</param>
		/// <returns>value for passed node</returns>
		static public NameValueCollection NameValueCollection(string p_sXmlNode)
		{
			try
			{
				NameValueCollection objCol = new NameValueCollection();
				XmlNodeList nodes = ACConfigurator.m_objDoc.DocumentElement.SelectNodes("//"+p_sXmlNode + "[@type='NameValueCollection']/add");
				foreach(XmlNode nd in nodes)
					objCol.Add(nd.Attributes["key"].Value, nd.Attributes["value"].Value);
				return objCol;
			}
			catch(Exception e)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.GetNameValueCollection.Exception"),e);
			}
		}
		#endregion
	
		#region Gets the value of the attribute
		/// Name			: GetAttributeValue
		/// Author			: Nikhil Kr. Garg
		/// Date Created	: 02-15-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the value of the required attribute
		/// </summary>
		/// <param name="p_sXmlNode">Node name to search</param>
		/// <param name="p_sAttributeName">Attribute name to fetch value for</param>
		/// <returns>Value of the Required Attribute</returns>
		static public string AttributeValue(string p_sXmlNode, string p_sAttributeName)
		{
			string sReturnValue="";
			XmlNode objRoot=null;
			XmlNode objXmlTargetNode = null ;

			try
			{
				if (m_objDoc == null)
					LoadFile ();

				objRoot = m_objDoc.DocumentElement;
				// check for null
				objXmlTargetNode = objRoot.SelectSingleNode("//"+p_sXmlNode) ;
				if( objXmlTargetNode != null )
				{
					sReturnValue= objXmlTargetNode.Attributes[p_sAttributeName].Value;
				}
				else
					sReturnValue = null ;
			}

			catch (FileInputOutputException p_objException)
			{
				throw p_objException;
			}

			catch(ACAppException p_objException)
			{
				throw p_objException;
			}

			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.GetAttributeValue.GeneralError"),p_objException);
			}
			finally
			{
				objRoot=null;
			}
			return (sReturnValue);
		}

		/// Name			: GetAttributeValue
		/// Author			: Nikhil Kr. Garg
		/// Date Created	: 02-07-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the value of the required attribute
		/// </summary>
		/// <param name="p_sXmlNode">Node name to search</param>
		/// <param name="p_sAttributeName">Attribute name to fetch value for</param>
		/// <returns>Value of the Required Attribute</returns>
		public string GetAttributeValue(string p_sXmlNode, string p_sAttributeName)
		{
			return ACConfigurator.AttributeValue(p_sXmlNode,p_sAttributeName);
		}


		/// Name			: GetAttributeValue
		/// Author			: Nikhil Kr. Garg
		/// Date Created	: 02-15-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the value of the required attribute
		/// It searches for a particular node depending on the passed parameter and value
		/// </summary>
		/// <param name="p_sXmlNode">Node name to search</param>
		/// <param name="p_sAttributeName">Attribute name to fetch value for</param>
		/// <param name="p_sAttributeToMatch">Attribute to search on</param>
		/// <param name="p_sAttributeToMatchValue">Attribute value to match</param>
		/// <returns>Value of the Required Attribute</returns>
		static public string AttributeValue(string p_sXmlNode, string p_sAttributeName, 
			string p_sAttributeToMatch, string p_sAttributeToMatchValue)
		{
			string sReturnValue="";
			XmlNode objRoot=null;
			XmlNode objXmlTargetNode = null ;

			try
			{
				if (m_objDoc == null)
					LoadFile ();

				objRoot = m_objDoc.DocumentElement;
				// Added check for null.
				objXmlTargetNode = objRoot.SelectSingleNode("//"+p_sXmlNode+"[@"+p_sAttributeToMatch+"='"+p_sAttributeToMatchValue+"']") ;
				if( objXmlTargetNode != null )
				{
					sReturnValue= objXmlTargetNode.Attributes[p_sAttributeName].Value;
				}
				else
					sReturnValue = null ;
			}

			catch (FileInputOutputException p_objException)
			{
				throw p_objException;
			}

			catch(ACAppException p_objException)
			{
				throw p_objException;
			}

			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.GetAttributeValue.GeneralError"),p_objException);
			}
			finally
			{
				objRoot=null;
			}
			return (sReturnValue);
		}
		/// Name			: GetAttributeValue
		/// Author			: Nikhil Kr. Garg
		/// Date Created	: 02-07-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the value of the required attribute
		/// It searches for a particular node depending on the passed parameter and value
		/// </summary>
		/// <param name="p_sXmlNode">Node name to search</param>
		/// <param name="p_sAttributeName">Attribute name to fetch value for</param>
		/// <param name="p_sAttributeToMatch">Attribute to search on</param>
		/// <param name="p_sAttributeToMatchValue">Attribute value to match</param>
		/// <returns>Value of the Required Attribute</returns>
		public string GetAttributeValue(string p_sXmlNode, string p_sAttributeName, 
			string p_sAttributeToMatch, string p_sAttributeToMatchValue)
		{
			return ACConfigurator.AttributeValue( p_sXmlNode,  p_sAttributeName, 
				p_sAttributeToMatch,  p_sAttributeToMatchValue);
		}
		#endregion
		
		#region Get The Inner Text
		/// Name			: Text
		/// Author			: Anurag Agarwal
		/// Date Created	: 02-June-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Gets the Innertext of the required attribute
		/// It searches for a particular node depending on the passed parameter and value
		/// </summary>
		/// <param name="p_sXmlNode">Node name to search</param>
		/// <param name="p_sAttributeToMatch">Attribute to search on</param>
		/// <param name="p_sAttributeToMatchValue">Attribute value to match</param>
		/// <returns>InnerText of the Required Attribute</returns>
		static public string Text(string p_sXmlNode, string p_sAttributeToMatch, 
			string p_sAttributeToMatchValue)
		{
			string sReturnValue="";
			XmlNode objRoot=null;
			XmlNode objXmlTargetNode = null ;

			try
			{
				if (m_objDoc == null)
					LoadFile ();

				objRoot = m_objDoc.DocumentElement;
				// Added check for null.
				objXmlTargetNode = objRoot.SelectSingleNode("//"+p_sXmlNode+"[@"+p_sAttributeToMatch+"='"+p_sAttributeToMatchValue+"']") ;
				if( objXmlTargetNode != null )
				{
					sReturnValue= objXmlTargetNode.InnerText;
				}
				else
					sReturnValue = null ;
			}

			catch (FileInputOutputException p_objException)
			{
				throw p_objException;
			}

			catch(ACAppException p_objException)
			{
				throw p_objException;
			}

			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("ACConfigurator.Text.GeneralError"),p_objException);
			}
			finally
			{
				objRoot=null;
			}
			return (sReturnValue);
		}

		#endregion
	}
}
