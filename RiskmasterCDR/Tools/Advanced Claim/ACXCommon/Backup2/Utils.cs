/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CCP.Constants;
using System.Collections;
using System.Configuration; //SIN204

namespace CCP.Common
{
    internal class Utils
    {
        public static DialogResult DisplayMsgBox(Debug Dbg, string sMsg, MessageBoxButtons button,
                                                 MessageBoxIcon icon, string sTitle)
        {
            return MessageBox.Show(sMsg, sTitle, button, icon);
        }

        public static void subLoadErrorCodes(Errors cErrs)
        {
            string str;
            string sfl;
            string[] cds;

            cErrs.Debug.PushProc("Utils.subLoadErrorCodes");

            if (Globals.errCodes == null || Globals.errCodes.Count == 0)
            {
                Globals.errCodes = new Dictionary<int, string>();
                Globals.errXRef = new Dictionary<object, object>();
                Globals.errHelp = new Dictionary<int, int>();
                //sfl = ErrorGlobalConstants.sErrCodesFile + cErrs.Debug.LanguageCode + ErrorGlobalConstants.sErrCodesFileExt;
                //if(!cErrs.Debug.FileFunctions.FileExists(Directory.GetCurrentDirectory(),sfl))
                sfl = ErrorGlobalConstants.sErrCodesFile + Constants.SYS_DFLT_LANGUAGE_CODE + ErrorGlobalConstants.sErrCodesFileExt;
                if(!cErrs.Debug.FileFunctions.FileExists(Directory.GetCurrentDirectory(),sfl))
                    sfl = ErrorGlobalConstants.sErrCodesFile + ErrorGlobalConstants.sErrCodesFileExt;
                if (cErrs.Debug.FileFunctions.FileExists(Directory.GetCurrentDirectory(), sfl))
                {
                    try
                    {
                        if (cErrs.Debug.FileFunctions.FileOpen(Directory.GetCurrentDirectory(), sfl, FileFunctions.cFileAccess.enFAForReading, true) != null)
                        {
                            while (!((StreamReader)(cErrs.Debug.FileFunctions.FileTextStream)).EndOfStream)
                            {
                                try
                                {
                                    str = cErrs.Debug.FileFunctions.FileRead();
                                    str = str.Trim();
                                    string left = "";
                                    if (str != "" && str != null)
                                        left = StringUtils.Left(str, 1);
                                    if ((str != "" && str != null) && (left != "'") && (left != "/") && (left != "*"))
                                    {
                                        cds = str.Split(new char[] { ',' });
                                        if (cds.GetUpperBound(0) >= 3)
                                        {
                                            int var1, var2;
                                            Int32.TryParse(cds[1], out var1);
                                            if (!Globals.errCodes.ContainsKey(var1))
                                            {
                                                Globals.errCodes.Add(var1, cds[3]);
                                                Int32.TryParse(cds[2], out var2);
                                                Globals.errHelp.Add(var1, var2);
                                                Globals.errXRef.Add(var1, cds[0]);
                                                Globals.errXRef.Add(cds[0], var1);
                                            }
                                        }
                                    }
                                }
                                catch { }
                            }
                            try
                            {
                                cErrs.Debug.FileFunctions.FileClose();
                            }
                            catch { }
                        }
                    }
                    catch
                    {
                        try
                        {
                            cErrs.Debug.FileFunctions.FileClose();
                        }
                        catch { }
                    }
                }
            }
            cErrs.Debug.PopProc();
        }

        public static void setUpArray(ArrayList arr)
        {
            for (int i = arr.Count; i < arr.Capacity; i++)
            {
                arr.Add(null);
            }
        }

        [Obsolete("Please use new version without Section name for use with Config files")] //SIN204
        internal static string GetSystemInfoPrivate(string sSection, string sKey, string sFileName)
        {
            string sDefault;
            string sProfileFile;
            int iRetValue;
            int i;
            string sKeyValue = "";
            StringBuilder lpReturn;
            int nBufferSize = 2048;

            sProfileFile = Directory.GetCurrentDirectory() + "\\" + sFileName;

            if (StringUtils.Left(sKey, 1) == "/")
                sKey = StringUtils.Mid(sKey, 2);

            lpReturn = new StringBuilder(nBufferSize);


            iRetValue = Win32.Functions.GetPrivateProfileStringA(sSection, sKey, "", lpReturn, nBufferSize, sProfileFile);

            sKeyValue = lpReturn.ToString();
            sKeyValue = StringUtils.Trim(StringUtils.Left(sKeyValue, iRetValue));
            i = StringUtils.InStr(sKeyValue, "'");
            if (i > 0)
                sKeyValue = StringUtils.Trim(StringUtils.Left(sKeyValue, i - 1));
            sKeyValue.Replace((char)27,char.Parse(" "));
            sKeyValue.Replace((char)9,char.Parse(" "));
            sKeyValue.Replace("\r\n", "");
            return StringUtils.Trim(sKeyValue);
        }

        //Start SIN204
        internal static string GetSystemInfoPrivate(string sSettingName)
        {
            string sConfigFileName = String.Empty;
            return GetSystemInfoPrivate(sSettingName, sConfigFileName);
        }

        internal static string GetSystemInfoPrivate(string sSettingName, string sConfigFileName)
        {
            string retVal = "";
            if (!sConfigFileName.EndsWith(".config", StringComparison.OrdinalIgnoreCase) && sConfigFileName != "")
                sConfigFileName += ".config";
            if (GetConfigFile(sConfigFileName).AppSettings.Settings[sSettingName] != null)
                retVal = GetConfigFile(sConfigFileName).AppSettings.Settings[sSettingName].Value;
            else
                retVal = "";

            int i = StringUtils.InStr(retVal, "'");
            if (i > -1)
                retVal = StringUtils.Trim(StringUtils.Left(retVal, i - 1));
            retVal.Replace((char)27, char.Parse(" "));
            retVal.Replace((char)9, char.Parse(" "));
            retVal.Replace("\r\n", "");
            return StringUtils.Trim(retVal);
        }

        private static Configuration GetConfigFile(string sAlternateConfigFile)
        {

            if (sAlternateConfigFile == "")
                return ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            else
            {
                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = sAlternateConfigFile;
                return ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
            }
        }
        //End SIN204
    }
}
