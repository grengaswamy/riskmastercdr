
// Copyright � 2005 CSC.  All rights reserved.

using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;


namespace CCP.Common
{
	/// <summary>
	/// Summary description for EventInformationForm.
	/// </summary>
		public class LogItem
		{
			// Event information fields
			private int eventId;
			private string message;
			private string category;
			private int priority;
			private string ACexceptionMessage;
			private string ACexceptionType;
			private string ACexceptionStackTrace;
			private string ACexceptionSource;
			private Hashtable parmList = null;  

			public LogItem()
			{
			
			}		
			/// <summary>
			/// Unique identification for event to be logged.
			/// </summary>
			public int EventId
			{
				get { return this.eventId; }
				set { this.eventId = value; }
			}

			/// <summary>
			/// Message for event to be logged.
			/// </summary>
			public string Message
			{
				get { return this.message; }
				set { this.message = value; }
			}

			/// <summary>
			/// Category of event to be logged.
			/// </summary>
			public string Category
			{
				get { return this.category; }
				set { this.category = value; }
			}

			/// <summary>
			/// Priority of event to be logged.
			/// </summary>
			public int Priority
			{
				get { return this.priority; }
				set { this.priority = value; }
			}

			/// <summary>
			/// AC Exception Message to be logged.
			/// </summary>
			public string ACExeptMessage
			{
				get { return this.ACexceptionMessage; }
				set { this.ACexceptionMessage = value; }
			}
			/// <summary>
			/// AC Exception Type to be logged.
			/// </summary>
			public string ACExceptType
			{
				get { return this.ACexceptionType; }
				set { this.ACexceptionType = value; }
			}
			/// <summary>
			/// AC Exception Source to be logged.
			/// </summary>
			public string ACExceptSource
			{
				get { return this.ACexceptionSource; }
				set { this.ACexceptionSource = value; }
			}
			/// <summary>
			/// AC Exception Stack Trace to be logged.
			/// </summary>
			public string ACExceptStack
			{
				get { return this.ACexceptionStackTrace; }
				set { this.ACexceptionStackTrace = value; }
			}
			/// <summary>
			/// AC StringDictionary list of additional items to be logged.
			/// </summary>
			public Hashtable ACParamList
			{
				get { 
					if (parmList == null)
						parmList = new Hashtable();

					return parmList; 
				}
				set { 
					if(parmList != null)
					{
						this.parmList = value; 
					}
					else
					{
						this.parmList.Clear();
						this.parmList = value;
					}
				}
			}
		}
	}

