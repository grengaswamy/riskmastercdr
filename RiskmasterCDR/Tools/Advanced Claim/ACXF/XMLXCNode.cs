/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 01/27/2009 | SIW139  |    JTC     | Field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 04/15/2010 | SIW371  |    AS      | Update in SerializeNode function for boolean / entity fields
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.Common;
using CCP.Constants;

namespace CCP.XmlFormatting
{
    public abstract class XMLXCNode:XMLACNode
    {
        //Provides an abstract class with functionality common to most classes in XmlComponents.
        private XML m_xml = null;
        private bool m_UseSharedXML = true;
        private string m_NodeName = String.Empty;

        public virtual string SerializeNode()
        {
            if (null != putNode)
            {
                if (!String.IsNullOrEmpty(putNode.OuterXml)) //SIW371
                {
                    return putNode.OuterXml;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public virtual XmlNode DeSerializeNode(string sXML)
        {
            if (null != Node)
            {
                XmlDocumentFragment xmlImportDocFrag = (new XmlDocument()).CreateDocumentFragment();
                xmlImportDocFrag.InnerXml = sXML;
                XmlNode xmlImportNode = xmlImportDocFrag.SelectSingleNode(NodeName);
                if ((!String.IsNullOrEmpty(xmlImportNode.InnerXml)) && (!String.IsNullOrEmpty(xmlImportNode.Value)))
                {
                    InsertInDocument(null, xmlImportNode);
                }
                ResetParent();
                return Node;
            }
            else
            {
                return null;
            }
        }

        //access to the Errors object
        public override Errors Errors
        {
            get
            {
                return XMLGlobals.gControl.Errors;
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return sThisNodeOrder;
            }
        }

        public override XmlDocument Document
        {
            get
            {
                return XML.Document;
            }
            set
            {
                XML.Document = value;
                ResetParent();
            }
        }

        //access to the XML formatting functions
        public virtual XML XML
        {
            get
            {
                if (m_UseSharedXML == true)
                {
                    if (null == XMLGlobals.gControl)
                    {
                        return null;
                    }
                    else
                    {
                        return XMLGlobals.gControl.XML;
                    }
                }
                else
                {
                    return m_xml;
                }
            }
            //Start SIW139
            set
            {
                if (m_UseSharedXML == true)
                {
                    XMLGlobals.gControl.XML = value;
                }
                else
                {
                    m_xml = value;
                }
                ResetParent();
            }
            //End SIW139
        }

        public virtual bool UseSharedXMLDocument
        {
            get
            {
                return m_UseSharedXML;
            }
            set
            {
                if (value != m_UseSharedXML)
                {
                    m_UseSharedXML = value;
                    if (value == false)
                    {
                        XML = new XML();
                        XML.Document = new XmlDocument();
                        XML.Document.LoadXml(XMLGlobals.gControl.XML.Document.OuterXml);                        
                    }
                }
                m_UseSharedXML = value;
            }
        }

        public Debug Debug
        {
            get
            {
                return XMLGlobals.gControl.Debug;
            }
        }

        //IMPORTANT NOTE: If this Parent property is hidden with the keywork 'new' in a subclass,
        //need to override InsertInDocument, Parent_Node, Parent_NodeOrder as well
        //(could just copy/paste from below if retaining functionality, change virtual to override)
        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = XML;
                    ResetParent();
                }
                return m_Parent;
            }
        }

        protected virtual void ResetParent() { }

        //returns the parent node
        public virtual XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        //returns the node order list from the parent
        public virtual ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public virtual void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        //returns the Xml associated with this node
        public string sXML
        {
            get
            {
                string spn = "XMLXCNode.sXML(Get)";
                if (xmlThisNode != null)
                    return xmlThisNode.OuterXml;
                else
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);
                    return "";
                }
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create();
                return Node;
            }
        }

        protected virtual XmlNode CreateNode()
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                InsertInDocument(null, null);
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, this.GetType().ToString() + ".CreateNode");
                return null;
            }

        }
        //needed in classes that don't override the putNode property from here
        //if not overridden in a class, will throw not implemented exception
        public virtual XmlNode Create()
        {
            return CreateNode();    
        }

        protected abstract string DefaultNodeName
        {
            get;
        }
        
        public virtual string NodeName
        {
            get
            {
                if (String.IsNullOrEmpty(m_NodeName))
                {
                    ResetNodeName();
                }
                return m_NodeName;
            }
            set
            {
                if (AllowNodeNameOverride == true)
                {
                    m_NodeName = value;
                    if (String.IsNullOrEmpty(m_NodeName))
                    {
                        ResetNodeName();
                    }
                }
                else
                {                    
                    throw new NotImplementedException("Set method is not appropriate for the Class");
                }
            }
        }

        protected virtual bool AllowNodeNameOverride
        {
            get { return false; }
        }

        public virtual void ResetNodeName()
        {
            m_NodeName = DefaultNodeName;
        }

        public virtual void LinkChildXML(XMLXCNode xmlChild)
        {
            xmlChild.UseSharedXMLDocument = this.UseSharedXMLDocument;
            xmlChild.XML = this.XML;
        }

    }
}
