/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 * 06/28/2010 | SIW452  |    AS      | Add severity attribute to error node
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlFormatting
{
    /*****************************************************************************
    ' The CCPXMLMessage class provides the functionality to support and manage a
    ' ClaimsPro XML Document Message Block
    '*****************************************************************************
    '<PROCESSING_MESSAGE TYPE="[REQUEST,RESPONSE]">
    '   <FUNCTION>Function</FUNCTION>
    '   <RESPOND_TO>URL to send a Request Reponse to {string}</RESPOND_TO>                      'SI06061
    '   <RESPOND_BY>Number of seconds that a response is required by {integer}</RESPOND_BY>     'SI06061
    '   <PARAMETERS COUNT={integer}>
    '      <PARAMETER NAME="Parameter Name" TYPE="[NUMBER,DATE,TIME,STRING,CODE]" [CODE="code"|
    '         YEAR="year" MONTH="month" DAY="day"|
    '         HOUR="hour" MINUTE="minute" SECOND="second" TIME_ZONE="timezone"]>
    '         optional value
    '      </PARAMETER>
    '      <!-- Multiple Parameters Allowed -->
    '   </PARAMETERS>
    '   <REPONSE_MESSAGE RESPONSE_TYPE="{string} [FILE, URL, TEXT]">
    '      <ERRORS COUNT="errorcount">                                   'SI05490
    '         <ERROR CODE="errorcode"                                    'SI05490
    '                NUMBER="errornumber"                                'SI05490
    '                MNEMONIC="text"                                     'SI05490
    '                SOURCE="errorsource"                                'SI05490
    '                SEVERITY="errorSeverity">description</Error>        'SIW452    
    '      </ERRORS>
    '      <MESSAGE>text response message or the address of a file containing the response {string}</MESSAGE>
    '      <REQUESTING_DOC>
    '         <NAME>Requesting document name. From request Message-&gt;NAME {string}</NAME>
    '         <RqUID>{UID of requesting documment}</RqUID>
    '         <APPLICATION>Name of application issuing the original request. From request Message-&gt;APPLICATION {string}</APPLICATION>
    '         <SOURCEDB>Database name of where original request originated.  From request Message-&gt;SOURCEDB {string}</SOURCEDB>
    '         <DOC_DATE YEAR="Year (4) {Integer} [1900 - 9999]"
    '                   MONTH="Month (2) {Integer} [1-12]"
    '                   DAY="Day (2) {Integer} [1-31]"/>
    '         <DOC_TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"
    '                   HOUR="Hour (2) {Integer} [0-23]"
    '                   MINUTE="Minute (2) {Integer} [0-59]"
    '                   SECOND="Second (2) {Integer} [0-59]"/>
    '      </REQUESTING_DOC>
    '   </REPONSE_MESSAGE>
    '</PROCESSING_MESSAGE>
    '*****************************************************************************/
    public class XMLMessage:XMLXFNode
    {
        XmlNode m_xmlParameter;     //node containing a message parameter
        XmlNode m_xmlParameters;    //node containing a collection of parameters
        XmlNode m_xmlErrors;        //node containing a collection of error messages
        XmlNode m_xmlError;         //node containing an error message
        Dictionary<pmType, string> sPMTypes;	//SIW485
        Dictionary<pmRespType, string> sPMRTypes;	//SIW485

        //initialize
        public XMLMessage()
        {
            xf = null;
            xmlThisNode = null;
            m_xmlParameter = null;
            m_xmlParameters = null;

            //Start SIW485
            //GlobalVariables.sPMRTypes = new Dictionary<pmRespType, string>();
            //GlobalVariables.sPMTypes = new Dictionary<pmType, string>();
            sPMRTypes = new Dictionary<pmRespType, string>();
            sPMTypes = new Dictionary<pmType, string>();

            //GlobalVariablessPMTypes.Add(pmType.pmmtRequest, Constants.sPMTypeRequest);
            //GlobalVariablessPMTypes.Add(pmType.pmmtResponse, Constants.sPMTypeResponse);
            //GlobalVariablessPMRTypes.Add(pmRespType.pmrtFile, Constants.sPMResponseTypeFile);
            //GlobalVariablessPMRTypes.Add(pmRespType.pmrtURL, Constants.sPMResponseTypeURL);
            //GlobalVariables.sPMRTypes.Add(pmRespType.pmrtText, Constants.sPMResponseTypeText);
            sPMTypes.Add(pmType.pmmtRequest, Constants.sPMTypeRequest);
            sPMTypes.Add(pmType.pmmtResponse, Constants.sPMTypeResponse);
            sPMRTypes.Add(pmRespType.pmrtFile, Constants.sPMResponseTypeFile);
            sPMRTypes.Add(pmRespType.pmrtURL, Constants.sPMResponseTypeURL);
            sPMRTypes.Add(pmRespType.pmrtText, Constants.sPMResponseTypeText);
            //End SIW485
        }

        //access to the COUNT attribute for the PARAMETERS node
        public int Count
        {
            get
            {
                int idata = 0;
                XmlNode xmlNode;

                xmlNode = ParametersNode;
                if (xmlNode != null)
                    idata = Int32.Parse(XML.XMLGetAttributeValue(xmlNode, Constants.sPMParametersCount));

                return idata;
            }
            set
            {
                XmlNode xmlNode;

                xmlNode = ParametersNode;
                if (xmlNode != null)
                    XML.XMLSetAttributeValue(xmlNode, Constants.sPMParametersCount, value + "");
            }
        }

        //access to the XML Functions object
        public override XML XML
        {
            get
            {
                if (xf == null)
                {
                    xf = new XML();
                    xf.Message = this;
                }
                return xf;
            }
            set
            {
                if ((xf != null) && (value != null) && (xf.guid == value.guid))
                    return;
                xf = null;
                xf = value;
                if (xf != null)
                    xf.Message = this;
            }
        }

        //access to the CODE attribute in the ERROR node
        public string ErrorCode
        {
            get
            {
                return XML.XMLGetAttributeValue(ErrorNode, Constants.sPMCode);
            }
            set
            {
                XML.XMLSetAttributeValue(putErrorNode(), Constants.sPMCode, value);
            }
        }

        //access to the COUNT attribute of the ERRORS node
        public int ErrorCount
        {
            get
            {
                return Int32.Parse(XML.XMLGetAttributeValue(ErrorsNode, Constants.sPMCount));
            }
            set
            {
                XML.XMLSetAttributeValue(ErrorsNode, Constants.sPMCount, value.ToString());
            }
        }

        //access to the data in the ERROR node
        public string ErrorDescription
        {
            get
            {
                XmlNode xmlNode;
                xmlNode = ErrorNode;
                return XML.XmlData;
            }
            set
            {
                putErrorNode();
                XML.XmlData = value;
            }
        }

        //access to the MNEMONIC attribute of the ERROR node
        public string ErrorMnemonic
        {
            get
            {
                return XML.XMLGetAttributeValue(ErrorNode, Constants.sPMMnemonic);
            }
            set
            {
                XML.XMLSetAttributeValue(putErrorNode(), Constants.sPMMnemonic, value);
            }
        }

        //access to an ERROR node
        public XmlNode ErrorNode
        {
            get
            {
                if (m_xmlError != null)
                    return GetError(biDirection.biCurr);
                else
                    return GetError(biDirection.biFirst);
            }
            set
            {
                m_xmlError = value;
                XML.CurrNode = value;
            }
        }

        //returns a list containing all existing ERROR nodes within the ERRORS node
        public XmlNodeList ErrorsNodeList
        {
            get
            {
                return XML.XMLGetNodes(ErrorsNode, Constants.sPMError);
            }
        }
        //access to the SOURCE attribute of the ERROR node
        public string ErrorSource
        {
            get
            {
                return XML.XMLGetAttributeValue(ErrorNode, Constants.sPMErrSource);
            }
            set
            {
                XML.XMLSetAttributeValue(putErrorNode(), Constants.sPMErrSource, value);
            }
        }
        // Start SIW452
        //access to the SEVERITY attribute of the ERROR node
        public string ErrorSeverity
        {
            get
            {
                return XML.XMLGetAttributeValue(ErrorNode, Constants.sPMErrSeverity);
            }
            set
            {
                XML.XMLSetAttributeValue(putErrorNode(), Constants.sPMErrSeverity, value);
            }
        }
        // End SIW452
        //access to the MESSAGE node under the RESPONSE_MESSAGE node
        public string Message
        {
            get
            {
                XmlNode xmlRspNode;
                string sdata = "";

                xmlRspNode = ResponseNode;
                if (xmlRspNode != null)
                    sdata = XML.XMLExtractElement(xmlRspNode, Constants.sPMResponseMessage);

                return sdata;
            }
            set
            {
                XmlNode xmlRspNode;
                XmlElement xmlDataNode;

                xmlRspNode = ResponseNode;

                if (xmlRspNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRspNode.SelectSingleNode(Constants.sPMResponseMessage);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMResponseMessage, value);
                            xmlRspNode.AppendChild(xmlDataNode);
                        }
                        else
                            xmlDataNode.InnerXml = value;
                            //xmlDataNode.Value = value;
                    else
                        if (xmlDataNode != null)
                            xmlRspNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the FUNCTION node
        public string MsgFunction
        {
            get
            {
                XmlNode xmlMsgnode;
                string sdata = "";

                xmlMsgnode = Node;
                if (xmlMsgnode != null)
                    sdata = XML.XMLExtractElement(xmlMsgnode, Constants.sPMFunction);

                return sdata;
            }
            set
            {
                XmlNode xmlMsgnode;
                XmlElement xmlDataNode;
                string sdata;

                xmlMsgnode = Node;
                sdata = value;

                if (xmlMsgnode != null)
                    xmlDataNode = (XmlElement)xmlMsgnode.SelectSingleNode(Constants.sPMFunction);
                else
                    xmlDataNode = null;

                if (xmlDataNode == null)
                {
                    xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMFunction, sdata);
                    xmlMsgnode.AppendChild(xmlDataNode);
                }
                else
                    xmlDataNode.InnerXml = sdata;
                    //xmlDataNode.Value = sdata;
            }
        }
        
        //access to the current node
        public override XmlNode Node
        {
            get
            {
                XmlDocument xmlDocument;
                XmlElement xmlDocElement;

                if (xmlThisNode == null)
                {
                    xmlDocument = Document;
                    xmlDocElement = xmlDocument.DocumentElement;
                    xmlThisNode = xmlDocElement.SelectSingleNode(Constants.sPM);
                }

                if (xmlThisNode == null)
                    Create(null, pmType.pmmtRequest, "NOFUNCTION");

                return xmlThisNode;
            }
        }

        //access to the current PARAMETER node
        public XmlNode Parameter
        {
            get
            {
                return m_xmlParameter;
            }
            set
            {
                XmlNode nn;

                nn = value;
                if (nn == null)
                {
                    m_xmlParameter = nn;
                    return;
                }else
                    if (nn.Name == "PARAMETER")
                    {
                        m_xmlParameter = nn;
                        return;
                    }
            }
        }

        //access to the NAME attribute of the PARAMETER node
        public string ParameterName
        {
            get
            {
                string sdata = "";
                string spn;
                spn = "XMLMessage.ParameterName(Get)";

                if (m_xmlParameter != null)
                    sdata = XML.XMLGetAttributeValue(m_xmlParameter, Constants.sPMParameterName);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);

                return sdata;
            }
            set
            {
                string spn;
                spn = "XMLMessage.ParameterName(Set)";
                if (m_xmlParameter != null)
                    XML.XMLSetAttributeValue(m_xmlParameter, Constants.sPMParameterName, value);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value);
            }
        }

        //access to the PARAMETERS node
        public XmlNode ParametersNode
        {
            get
            {
                XmlElement xmlMsgnode;
                XmlDocument xmlDocument;

                xmlDocument = Document;
                m_xmlParameters = null;
                xmlMsgnode = (XmlElement)Node;
                if (xmlMsgnode != null)
                {
                    m_xmlParameters = XML.XMLGetNode(xmlMsgnode, Constants.sPMParameters);
                    if(m_xmlParameters == null){
                        m_xmlParameters = xmlDocument.CreateElement(Constants.sPMParameters);
                        XML.XMLAddElement(m_xmlParameters,xmlMsgnode);
                        Count = 0;
                    }
                }

                return m_xmlParameters;
            }
        }

        //access to the TYPE attribute of the PARAMETER node
        public cxmlDataType ParameterType
        {
            get
            {
                string sdata = "";
                string spn;
                cxmlDataType dt;
                spn = "XMLMessage.ParameterType(Get)";

                if (m_xmlParameter != null)
                {
                    sdata = XML.XMLGetAttributeValue(m_xmlParameter, Constants.sPMParameterType);
                    dt = XML.getType(sdata);
                }
                else
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);
                    dt = cxmlDataType.dtUndefined;
                }
                return dt;
            }
            set
            {
                string spn;
                spn = "XMLMessage.ParameterType(Set)";
                if (m_xmlParameter != null)
                    XML.XMLSetAttributeValue(m_xmlParameter, Constants.sPMParameterType, XML.getType(value));
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value + "");
            }
        }

        //access to the APPLICATION node of the REQUESTING_DOC node
        public string RequestingDocApplication
        {
            get
            {
                XmlNode xmlRDocNode;
                string sdata = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                    sdata = XML.XMLExtractElement(xmlRDocNode, Constants.sPMRequestApp);

                return sdata;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestApp);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRequestApp, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            xmlDataNode.InnerXml = value;
                            //xmlDataNode.Value = value;
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the DOC_DATE node of the REQUESTING_DOC node
        public string RequestingDocDate
        {
            get
            {
                XmlNode xmlRDocNode;
                XmlNode xmlDataNode;
                string dDate = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                {
                    xmlDataNode = xmlRDocNode.SelectSingleNode(Constants.sPMRequestDate);
                    dDate = XML.XMLExtractDate_Date(xmlDataNode);
                }

                return dDate;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestDate);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementAsDate_Node(Constants.sPMRequestDate, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            XML.XMLSetDateAttributes(xmlDataNode, value);
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the NAME node of the REQUESTING_DOC node
        public string RequestingDocName
        {
            get
            {
                XmlNode xmlRDocNode;
                string sdata = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                    sdata = XML.XMLExtractElement(xmlRDocNode, Constants.sPMRequestName);

                return sdata;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestName);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRequestName, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            xmlDataNode.InnerXml = value;
                            //xmlDataNode.Value = value;
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
                
            }
        }

        //access to the REQUESTING_DOC node
        public XmlNode RequestingDocNode
        {
            get
            {
                XmlElement xmlRspNode;
                XmlNode xmlRDocNode;
                XmlDocument xmlDocument;

                xmlRDocNode = null;
                xmlRspNode = (XmlElement)ResponseNode;
                if (xmlRspNode != null)
                {
                    xmlRDocNode = XML.XMLGetNode(xmlRspNode, Constants.sPMRequestDoc);
                    if (xmlRDocNode == null)
                    {
                        xmlDocument = Document;
                        xmlRDocNode = xmlDocument.CreateElement(Constants.sPMRequestDoc);
                        XML.XMLAddElement(xmlRDocNode, xmlRspNode);
                    }
                }

                return xmlRDocNode;
            }
        }

        //acess to teh RqUID node of the REQUESTING_DOC node
        public string RequestingDocRqUID
        {
            get
            {
                XmlNode xmlRDocNode;
                string sdata = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                    sdata = XML.XMLExtractElement(xmlRDocNode, Constants.sPMRequestUID);

                return sdata;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestUID);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRequestUID, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            xmlDataNode.InnerXml = value;
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the SOURCEDB node of the REQUESTING_DOC node
        public string RequestingDocSourceDB
        {
            get
            {
                XmlNode xmlRDocNode;
                string sdata = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                    sdata = XML.XMLExtractElement(xmlRDocNode, Constants.sPMRequestSourceDB);

                return sdata;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestSourceDB);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRequestSourceDB, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            xmlDataNode.InnerXml = value;
                            //xmlDataNode.Value = value;
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the DOC_TIME node of the REQUESTING_DOC node
        public string RequestingDocTime
        {
            get
            {
                XmlNode xmlRDocNode;
                XmlNode xmlDataNode;
                string tTime = "";

                xmlRDocNode = RequestingDocNode;
                if (xmlRDocNode != null)
                {
                    xmlDataNode = xmlRDocNode.SelectSingleNode(Constants.sPMRequestTime);
                    tTime = XML.XMLExtractTime_Time(xmlDataNode);
                }

                return tTime;
            }
            set
            {
                XmlNode xmlRDocNode;
                XmlElement xmlDataNode;

                xmlRDocNode = RequestingDocNode;

                if (xmlRDocNode != null)
                {
                    xmlDataNode = (XmlElement)xmlRDocNode.SelectSingleNode(Constants.sPMRequestTime);
                    if (value != "" && value != null)
                        if (xmlDataNode == null)
                        {
                            xmlDataNode = XML.XMLElementAsTime_Node(Constants.sPMRequestTime, value);
                            xmlRDocNode.AppendChild(xmlDataNode);
                        }
                        else
                            XML.XMLSetTimeAttributes(xmlDataNode, value);
                    else
                        if (xmlDataNode != null)
                            xmlRDocNode.RemoveChild(xmlDataNode);
                }
            }
        }

        //access to the TIME_ZONE attribute of teh DOC_TIME node of the REQUESTING_DOC node
        public string RequestingDocTimeZone
        {
            get
            {
                XmlNode xmlRDNode;
                XmlNode xmlTmNode;
                string stimezone = "";

                xmlRDNode = RequestingDocNode;
                if (xmlRDNode != null)
                {
                    xmlTmNode = xmlRDNode.SelectSingleNode(Constants.sPMRequestTime);
                    stimezone = XML.XMLGetAttributeValue(xmlTmNode, Constants.sTimeTimeZone);
                }

                return stimezone;
            }
            set
            {
                XmlNode xmlRDNode;
                XmlElement xmlTmNode;

                xmlRDNode = RequestingDocNode;

                if (xmlRDNode != null)
                    xmlTmNode = (XmlElement)xmlRDNode.SelectSingleNode(Constants.sPMRequestTime);
                else
                    xmlTmNode = null;

                if (xmlTmNode == null)
                {
                    xmlTmNode = XML.XMLElementAsTime_Node(Constants.sPMRequestTime, "", value);
                    xmlRDNode.AppendChild(xmlTmNode);
                }
                else
                    XML.XMLSetAttributeValue(xmlTmNode, Constants.sTimeTimeZone, value);
            }
        }

        //access to teh RESPOND_BY attribute of the RESPOND_TO node
        public string RespondBy
        {
            get
            {
                XmlNode xmlMsgnode;
                string sdata = "";

                xmlMsgnode = Node;
                if (xmlMsgnode != null)
                {
                    sdata = XML.XMLExtractElement(xmlMsgnode, Constants.sPMRespondBy);
                }
                return sdata;
            }
            set
            {
                XmlNode xmlMsgnode;
                XmlNode xmlDataNode;

                xmlMsgnode = Node;
                if (xmlMsgnode != null)
                    xmlDataNode = xmlMsgnode.SelectSingleNode(Constants.sPMRespondBy);
                else
                    xmlDataNode = null;

                if (xmlDataNode == null)
                {
                    if (value != "" && value != null)
                    {
                        xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRespondBy, value);
                        xmlMsgnode.AppendChild(xmlDataNode);
                    }
                }
                else
                    if (value == "")
                        xmlMsgnode.RemoveChild(xmlDataNode);
                    else
                        xmlDataNode.InnerText = value;
            }
        }

        //access to the RESPOND_TO node
        public string RespondTo
        {
            get
            {
                XmlNode xmlMsgnode;
                string sdata = "";

                xmlMsgnode = Node;
                if (xmlMsgnode != null)
                    sdata = XML.XMLExtractElement(xmlMsgnode, Constants.sPMRespondTo);

                return sdata;
            }
            set
            {
                XmlNode xmlMsgnode;
                XmlElement xmlDataNode;

                xmlMsgnode = Node;

                if (xmlMsgnode != null)
                    xmlDataNode = (XmlElement)xmlMsgnode.SelectSingleNode(Constants.sPMRespondTo);
                else
                    xmlDataNode = null;

                if (xmlDataNode == null)
                {
                    if (value != "" && value != null)
                    {
                        xmlDataNode = XML.XMLElementWithData_Node(Constants.sPMRespondTo, value);
                        xmlMsgnode.AppendChild(xmlDataNode);
                    }
                }
                else
                {
                    if (value == "" || value == null)
                        xmlMsgnode.RemoveChild(xmlDataNode);
                    else
                        xmlDataNode.InnerXml = value;
                        //xmlDataNode.Value = value;
                }
            }
        }

        //access to the RESPONSE_MESSAGE node
        public XmlNode ResponseNode
        {
            get
            {
                XmlElement xmlMsgnode;
                XmlNode xmlDataNode;
                XmlDocument xmlDocument;

                xmlDocument = Document;
                xmlDataNode = null;
                xmlMsgnode = (XmlElement)Node;
                if (xmlMsgnode != null)
                {
                    xmlDataNode = XML.XMLGetNode(xmlMsgnode, Constants.sPMResponse);
                    if (xmlDataNode == null)
                    {
                        xmlDataNode = xmlDocument.CreateElement(Constants.sPMResponse);
                        XML.XMLAddElement(xmlDataNode, xmlMsgnode);
                    }
                }

                return xmlDataNode;
            }
        }

        //access to the RESPONSE_TYPE attribute of the RESPONSE_MESSAGE node
        public pmRespType ResponseType
        {
            get
            {
                XmlNode xmlRspNode;
                string sdata;
                pmRespType mtype = pmRespType.pmrtUndefined;

                sdata = "";
                xmlRspNode = ResponseNode;
                if (xmlRspNode != null)
                {
                    sdata = XML.XMLGetAttributeValue(xmlRspNode, Constants.sPMResponseType);
                    //SIW485 foreach (KeyValuePair<pmRespType, string> kvp in GlobalVariables.sPMRTypes)
                    foreach (KeyValuePair<pmRespType, string> kvp in sPMRTypes)	//SIW485
                        if (kvp.Value == sdata)
                            mtype = kvp.Key;
                }
                return mtype;
            }
            set
            {
                XmlNode xmlRspNode;
                string strpmtype;

                xmlRspNode = ResponseNode;
                if (xmlRspNode != null)
                {
                    if (value != pmRespType.pmrtUndefined)
                    {
                        //SIW485 GlobalVariables.sPMRTypes.TryGetValue(value, out strpmtype);
                        sPMRTypes.TryGetValue(value, out strpmtype);	//SIW485
                        XML.XMLSetAttributeValue(xmlRspNode, Constants.sPMType, strpmtype);
                    }
                }
            }
        }

        //access to the TYPE attribute of the PROCESSING_MESSAGE node
        public pmType TypeMsg
        {
            get
            {
                XmlNode xmlMsgnode;
                string sdata;
                pmType mtype = pmType.pmmtUndefined;

                xmlMsgnode = Node;
                if (xmlMsgnode != null)
                {
                    sdata = XML.XMLGetAttributeValue(xmlMsgnode, Constants.sPMType);
                    //SIW485 foreach (KeyValuePair<pmType, string> kvp in GlobalVariables.sPMTypes)
                    foreach (KeyValuePair<pmType, string> kvp in sPMTypes)	//SIW485
                        if (kvp.Value == sdata)
                            mtype = kvp.Key;
                }

                return mtype;
            }
            set
            {
                XmlNode xmlMsgnode;
                string strpmtype;

                xmlMsgnode = Node;

                if(xmlMsgnode != null)
                    if (value != pmType.pmmtUndefined)
                    {
                        //SIW485 GlobalVariables.sPMTypes.TryGetValue(value, out strpmtype);
                        sPMTypes.TryGetValue(value, out strpmtype);	//SIW485
                        XML.XMLSetAttributeValue(xmlMsgnode,Constants.sPMType,strpmtype);
                    }
            }
        }

        //translates the value in TYPE to a string
        public string TypeMsgName
        {
            get
            {
                pmType st;
                st = TypeMsg;
                string ret = "";
                switch (st)
                {
                    case pmType.pmmtRequest:
                        ret = Constants.sPMTypeRequest;
                        break;
                    case pmType.pmmtResponse:
                        ret = Constants.sPMTypeResponse;
                        break;
                    case pmType.pmmtUndefined:
                        ret = Constants.sUndefined;
                        break;
                }
                return ret;
            }
        }

        //adds an ERROR node to the ERRORS node
        public XmlNode AddError(string sErrCode, string sErrNumber, string sErrSource, string sErrMnemonic, string sErrDescription)
        {
            ErrorNode = XML.XMLNewElement(Constants.sPMError, true, null, ErrorsNode);

            ErrorCode = sErrCode;
            ErrorNumber = sErrNumber;
            ErrorSource = sErrSource;
            ErrorMnemonic = sErrMnemonic;
            ErrorDescription = sErrDescription;
            ErrorCount = ErrorCount + 1;

            return ErrorNode;
        }

        //adds a PARAMETER node to the PARAMETERS node
        public XmlNode AddParameter(string sParamName, cxmlDataType cPrmType, string sData1, string sData2)
        {
            XmlNode xmlPrmsNode;
            XmlDocument xmlDocument;

            xmlPrmsNode = ParametersNode;

            xmlDocument = Document;
            m_xmlParameter = xmlDocument.CreateElement(Constants.sPMParameter);

            xmlPrmsNode.AppendChild(m_xmlParameter);

            ParameterName = sParamName;
            ParameterType = cPrmType;
            ParameterData(pmParamDataDirection.pmPutData, ref sData1, ref sData2);

            Count = Count + 1;

            return m_xmlParameter;
        }

        //clears the current ERROR node from the ERRORS node
        public void ClearError()
        {
            XML.XMLRemoveNode(ErrorsNode, ErrorNode,"");
        }

        //clears all the ERROR nodes from the ERRORS node
        public void ClearErrors()
        {
            XML.XMLRemoveNode(ResponseNode, null, Constants.sPMErrors);
        }

        //Overridden for optional parameters
        public XmlNode Create(XmlDocument xDocument, pmType cType, string sFunctions)
        {
            return Create(xDocument, cType, sFunctions, "", "" + 0, pmRespType.pmrtText, "", "", "", "", "", "");
        }

        //sets up a new message
        public XmlNode Create(XmlDocument xDocument, pmType cType, string sFunctions, 
                              string sRespondTo, string sRespondBy, pmRespType cResponseType, 
                              string sResponse, string sReqApp, string sReqSourceDB, string sReqDocName, 
                              string sReqDocDate, string sReqDocTime)
        {
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;
            XmlDocument xmlDocument;

            if (xDocument != null)
                Document = xDocument;
            xmlDocument = Document;

            xmlThisNode = null;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = xmlDocument.CreateElement(Constants.sPM);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                TypeMsg = cType;
                MsgFunction = sFunctions;

                if (sRespondTo != "" && sRespondTo != null)
                {
                    RespondTo = sRespondTo;
                    RespondBy = sRespondBy;
                }

                if (sResponse != "" && sResponse != null)
                {
                    ResponseType = cResponseType;
                    Message = sResponse;
                    if (sReqDocName != "" && sReqDocName != null)
                    {
                        RequestingDocName = sReqDocName;
                        RequestingDocApplication = sReqApp;
                        RequestingDocSourceDB = sReqSourceDB;
                        RequestingDocDate = sReqDocDate;
                        RequestingDocTime = sReqDocTime;
                    }
                }

                InsertInDocument(null, null);
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLMessage.Create");
                return null;
            }
        }

        //returns the PARAMETER node with the given name
        public XmlNode FindParameter(string paramname)
        {
            XmlNode prmsnode;
            XmlNodeList prmlist;
            string prmname;

            prmsnode = ParametersNode;
            prmlist = prmsnode.SelectNodes(Constants.sPMParameter);

            m_xmlParameter = null;
            foreach (XmlNode node in prmlist)
            {
                prmname = XML.XMLGetAttributeValue(node, Constants.sPMParameterName);
                if (prmname == paramname)
                {
                    m_xmlParameter = node;
                    break;
                }
            }

            return m_xmlParameter;
        }

        //returns an ERROR node
        public XmlNode GetError(biDirection direction)
        {
            switch (direction)
            {
                case biDirection.biFirst:
                    m_xmlError = XML.XMLGetNode(ErrorsNode, Constants.sPMError);
                    break;
                case biDirection.biNext:
                    if (m_xmlError != null)
                        m_xmlError = m_xmlError.NextSibling;
                    else
                        m_xmlError = XML.XMLGetNode(ErrorsNode, Constants.sPMError);
                    break;
                case biDirection.biPrev:
                    if (m_xmlError != null)
                        m_xmlError = m_xmlError.PreviousSibling;
                    else
                        m_xmlError = ErrorsNode.LastChild;
                    break;
                case biDirection.biCurr:
                    break;
            }
            XML.CurrNode = m_xmlError;
            return m_xmlError;
        }

        //returns a PARAMETER node
        public XmlNode GetParameter(biDirection direction)
        {
            XmlNode prmsnode;

            prmsnode = ParametersNode;

            switch (direction)
            {
                case biDirection.biFirst:
                    m_xmlParameter = prmsnode.FirstChild;
                    //m_xmlParameter = XML.XMLGetNode(prmsnode, Constants.sPMParameter);
                    break;
                case biDirection.biNext:
                    if (m_xmlParameter != null)
                        m_xmlParameter = m_xmlParameter.NextSibling;
                    else
                        m_xmlParameter = prmsnode.FirstChild;
                        //m_xmlParameter = XML.XMLGetNode(prmsnode, Constants.sPMParameter);
                    break;
                case biDirection.biPrev:
                    if (m_xmlParameter != null)
                        m_xmlParameter = m_xmlParameter.PreviousSibling;
                    else
                        m_xmlParameter = prmsnode.LastChild;
                    break;
                case biDirection.biCurr:
                    break;
            }
            return m_xmlParameter;
        }

        //inserts this message into the current document
        public override void InsertInDocument(XmlDocument doc, XmlNode nde)
        {
            XmlNode xmlCurrNode;

            base.InsertInDocument(doc, nde);

            xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sPM);

            if (xmlCurrNode != null)
                xmlDocument.DocumentElement.ReplaceChild(xmlNewNode, xmlCurrNode);
            else
            {
                xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sHdrElement);
                if (xmlCurrNode != null)
                {
                    xmlCurrNode = xmlCurrNode.NextSibling;
                    if (xmlCurrNode != null)
                        xmlDocument.DocumentElement.InsertBefore(xmlNewNode, xmlCurrNode);
                    else
                        xmlDocument.DocumentElement.AppendChild(xmlNewNode);
                }
                else
                    xmlDocument.DocumentElement.AppendChild(xmlNewNode);
            }

        }

        //access to the data of the given named PARAMETER node
        public void NamedParameterData(string prmname, pmParamDataDirection getput, ref string dta1, ref string dta2)
        {
            FindParameter(prmname);
            ParameterData(getput, ref dta1, ref dta2);
        }

        //access to the data of the current PARAMETER
        public void ParameterData(pmParamDataDirection getput, ref string dta1, ref string dta2)
        {
            string sdata1;
            string sdata2;
            string spn;
            cxmlDataType dt;
            spn = "XMLMessage.ParameterData";

            sdata1 = dta1;
            sdata2 = dta2;

            if (m_xmlParameter != null)
            {
                XML.CurrNode = m_xmlParameter;
                dt = ParameterType;
                switch (dt)
                {
                    case cxmlDataType.dtUndefined:
                        if (getput == pmParamDataDirection.pmGetData)
                            sdata1 = XML.XmlData;
                        else
                            XML.XmlData = sdata1;
                        break;
                    case cxmlDataType.dtString:
                        if (getput == pmParamDataDirection.pmGetData)
                            sdata1 = XML.XmlData;
                        else
                            XML.XmlData = sdata1;
                        break;
                    case cxmlDataType.dtDate:
                        if (getput == pmParamDataDirection.pmGetData)
                        {
                            sdata1 = XML.XMLExtractDate_Date(null);
                            sdata2 = XML.XmlData;
                        }
                        else
                        {
                            XML.XMLSetDateAttributes(m_xmlParameter, sdata1);
                            if (GlobalVariables.m_formatDateSW)
                                XML.XMLSetDateValue(null, sdata1);
                        }
                        break;
                    case cxmlDataType.dtTime:
                        if (getput == pmParamDataDirection.pmGetData)
                        {
                            sdata1 = XML.XMLExtractTime_Time(null);
                            sdata2 = XML.XmlData;
                        }
                        else
                        {
                            XML.XMLSetTimeAttributes(m_xmlParameter, sdata1);
                            if (GlobalVariables.m_formatTimeSW)
                                XML.XMLSetTimeValue(null, sdata1);
                        }
                        break;
                    case cxmlDataType.dtNumber:
                        if (getput == pmParamDataDirection.pmGetData)
                            sdata1 = XML.XmlData;
                        else
                            XML.XmlData = sdata1;
                        break;
                    case cxmlDataType.dtCode:
                        if (getput == pmParamDataDirection.pmGetData)
                        {
                            sdata1 = XML.XMLExtractCodeAttribute(m_xmlParameter);
                            sdata2 = XML.XmlData;
                        }
                        else
                        {
                            XML.XMLInsertCode_Node(m_xmlParameter, sdata2, sdata1);
                        }
                        break;
                    case cxmlDataType.dtBoolean:
                        if (getput == pmParamDataDirection.pmGetData)
                            sdata1 = XML.XMLExtractIndicatorAttribute(m_xmlParameter,"").ToString();
                        else
                            XML.XMLInsertBoolean_Node(m_xmlParameter, "INDICATOR", sdata1);
                        break;
                    default:
                        sdata1 = "";
                        sdata2 = "";
                        break;
                }
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);
                sdata1 = "";
                sdata2 = "";
            }

            if (getput == pmParamDataDirection.pmGetData)
            {
                dta1 = sdata1;
                dta2 = sdata2;
            }
        }

        //creates a new error to work with
        public XmlNode putErrorNode()
        {
            if (m_xmlError == null)
                AddError("", "", "", "", "");
            return ErrorNode;
        }

        //access to the ERRORS node
        public XmlNode ErrorsNode
        {
            get
            {
                m_xmlErrors = XML.XMLGetNode(ResponseNode, Constants.sPMErrors);
                if (m_xmlErrors == null)
                {
                    m_xmlErrors = XML.XMLNewElement(Constants.sPMErrors, true, null, ResponseNode);
                    ErrorCount = 0;
                }
                return m_xmlErrors;
            }
        }

        //access to the NUMBER attribute of the ERROR node
        public string ErrorNumber
        {
            get
            {
                return XML.XMLGetAttributeValue(ErrorNode, Constants.sPMNumber);
            }
            set
            {
                XML.XMLSetAttributeValue(putErrorNode(), Constants.sPMNumber, value);
            }
        }

        //Start SIN204
        public void ExtractParameters(Parameters objParams)
        {
            string hNme, sNme, sVal = "", sCde = "";

            if (objParams == null)
                objParams = xf.Debug.Parameters;

            GetParameter(biDirection.biFirst);
            hNme = ParameterName;

            do
            {
                sNme = ParameterName;
                ParameterData(pmParamDataDirection.pmGetData, ref sVal, ref sCde);
                if (sNme != "" && sNme != null)
                {
                    if (sCde != "" && sCde != null)
                        objParams.Parameter(sNme, sCde);
                    else
                        objParams.Parameter(sNme, sVal);
                }
                GetParameter(biDirection.biNext);
            } while (ParameterName != hNme);
        }
        //End SIN204
    } //End Class XMLMessage
}
