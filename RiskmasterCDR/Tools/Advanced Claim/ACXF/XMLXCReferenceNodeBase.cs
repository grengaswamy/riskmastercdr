/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using System.Xml;
using System.Collections;

namespace CCP.XmlFormatting
{
    public abstract class XMLXCReferenceNodeBase:XMLXCNodeWithListBase
    {
        //Extended abstract class used in "Reference" nodes

        public virtual void Clear()
        {
            if (NodeList != null)
            {

                IEnumerator xmlNodeListEnum = xmlNodeList.GetEnumerator();
                xmlNodeListEnum.MoveNext();
                if (xmlNodeListEnum.MoveNext())
                    xmlThisNode = (XmlNode)xmlNodeListEnum.Current;
                else
                    xmlThisNode = null;
                while (xmlThisNode != null)
                    Remove();
            }
            xmlNodeList = null;
        }

        public void Remove()
        {
            Parent_Node.RemoveChild(xmlThisNode);
            getNode(false);
        }

        protected abstract void CheckEntry();

        protected void xEntry(string id)
        {
            xEntry("", "", id, 0);
        }

        protected void xEntry(string role, string roleCode, string id, int numchild)
        {
            int temp;
            Int32.TryParse(id, out temp);
            if (xmlThisNode != null)
                if (role == "" && roleCode == "" && temp == 0 && numchild == 0)
                    Remove();
        }
    }
}
