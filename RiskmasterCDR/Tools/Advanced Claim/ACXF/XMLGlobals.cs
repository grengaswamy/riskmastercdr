/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 03/04/2009 | SIW119  |    sw      | System Settings nodes
 * 05/17/2010 | SIW360  |   AP       | Techkey position reodered in .NET XML components similar to ordering in CCPXC components.
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
* 07/4/2011 | SIW7214    |   AV       |  Add medicare id       
 * 09/6/2011 | SIW7182   |   RG       |  Entity Account  
* 09/23/2011 | SIW7123 |    AV      | Add new IDs for limt and deductible nodes
 * 09/22/2011 | SIW7076   |   AV       |  Add contact id node to the contact node.  
* 01/17/2012 | SIW8067  |   SAS     | ID Number is not working for Converion ID Number type
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace CCP.XmlFormatting
{
    public class XMLGlobals
    {
        //SIW485 public static ArrayList sDocNodeOrder;
        //SIW485 public static ArrayList sNodeOrder;

        //SIW485 public static XMLControls gControl;

        public static int lClmId;
        public static int lPolId;
        public static int lUntId;
        public static int lInjId;
        public static int lInvId;
        public static int lItmId;
        public static int lLitId;
        public static int lSubId;
        public static int lArbId;
        public static int lLDId;
        public static int lDOId;
        public static int lTRGId;
        public static int lSLOGId;
        public static int lSearchId;
        public static int lSysId;       //SIW119
        public static int lPIId; //SIW360
        public static int lLSDId; //SIW360
        public static int lRSDId; //SIW360
        public static int lARID;       //SIW360
        public static int lAccId;       //SIW7182
        public static int lCRID;       //SIW360
        public static int lCmsId; //SIW7214
        public static int lLIMId;  ////SIW7123
        public static int lCONTACTId; //SIW7076
        public static int lDEDId; ////SIW7123
        public static int lEntIDNId; //SIW8067
    }
}
