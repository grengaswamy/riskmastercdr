/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace CCP.XmlFormatting
{
    public class XCGlobals
    {
        public static ArrayList sDocNodeOrder;
        public static ArrayList sNodeOrder;

        public static XMLControls gControl;

        public static int lClmId;
        public static int lPolId;
        public static int lUntId;
        public static int lInjId;
        public static int lInvId;
        public static int lItmId;
        public static int lLitId;
        public static int lSubId;
        public static int lArbId;
        public static int lLDId;
        public static int lDOId;
        public static int lTRGId;
        public static int lSLOGId;
        public static int lSearchId;
    }
}
