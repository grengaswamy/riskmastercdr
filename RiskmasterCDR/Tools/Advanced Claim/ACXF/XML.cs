/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/07/2008 | SI06468 |    ASM     | Add Code ID to code nodes   
 * 12/07/2008 | SI06499 |    NDB     | Fix to XMLAddElement to allow nodes from other Documents to be added
 * 01/27/2009 | SIW139  |    JTC     | Field blanking with <b>
 * 11/12/2009 | SIW211  |    AS      | Use InnerText instead of InnerXml to handle special characters like '&'
 * 11/12/2009 | SIN204  |    JTC     | Updates for ISO
 * 11/30/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 05/12/2010 | SIW321  |   AP       | Event location not saving on Event page.       
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected  
 * 05/07/2012 | SIW8421a|   MK       | Implementing Table Lookup
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using System.Windows.Forms;
using System.IO;
using System.Xml.Schema;

namespace CCP.XmlFormatting
{
    public class XML:XMLACNode
    {
        //Master variables
        private XmlDocument m_xmlDoc;
        private XmlNode m_xmlNode;
        private XmlNodeList m_xmlNodeList;
        private XMLHeader m_xmlHeader;
        private XMLMessage m_xmlMessage;
        private XMLBatch m_xmlBatch;
        private string m_GUID;
        private DialogResult response;
        private bool m_blanking;          //SIW139

        //Other variables
        private bool m_ReportErrors;            //Boolean flag used if errors exist
        private ArrayList sXMLNodeOrder;
        private Debug m_Debug;

        private Dictionary<cxmlDataType,string> DataType = new Dictionary<cxmlDataType,string>();

        //Constructor
        public XML()
        {
            m_Debug = null;
            m_xmlDoc = null;
            m_xmlNode = null;
            m_xmlNodeList = null;
            m_xmlHeader = null;
            m_xmlMessage = null;
            m_xmlBatch = null;
            m_ReportErrors = false;

            if (GlobalVariables.m_Instantiated != true)
            {
                DateFormatOption = false;
                TimeFormatOption = false;
                AutoEncrypt = true;
                GlobalVariables.m_Instantiated = true;
            }
            

            sXMLNodeOrder = new ArrayList(3);
            sXMLNodeOrder.Add(Constants.sHdrElement);
            sXMLNodeOrder.Add(Constants.sPM);
            sXMLNodeOrder.Add(Constants.sBatchElement);

            DataType.Add(cxmlDataType.dtDate, Constants.sdtDate);
            DataType.Add(cxmlDataType.dtTime, Constants.sdtTime);
            DataType.Add(cxmlDataType.dtNumber, Constants.sdtNumber);
            DataType.Add(cxmlDataType.dtString, Constants.sdtString);
            DataType.Add(cxmlDataType.dtCode, Constants.sdtCode);
            DataType.Add(cxmlDataType.dtBoolean, Constants.sdtBoolean);
            DataType.Add(cxmlDataType.dtUndefined, Constants.sUndefined);
            DataType.Add(cxmlDataType.dtDTTM, Constants.sdtDTTM);
            DataType.Add(cxmlDataType.dtEntityRef, Constants.sdtEntityRef);
            DataType.Add(cxmlDataType.dtDataRecord, Constants.sdtDataRecord);

            m_GUID = Guid.NewGuid().ToString();
        }

        //Property used to access the Debugger
        public Debug Debug
        {
            set
            {
                if ((value != null) && (m_Debug != null))
                {
                    if (value.DebugGUID == m_Debug.DebugGUID)
                        return;
                }
                if (value == null)
                {
                    m_Debug = new Debug();
                }
                else
                {
                    m_Debug = value;
                }
            }
            get
            {
                if (m_Debug == null)
                    m_Debug = new Debug();
                return m_Debug;
            }
        }

        public bool typeExists(string stype)
        {
            return DataType.ContainsValue(stype);
        }

        public bool typeExists(cxmlDataType dtype)
        {
            return DataType.ContainsKey(dtype);
        }

        public string getType(cxmlDataType dtype)
        {
            string ret;
            DataType.TryGetValue(dtype, out ret);
            return ret;
        }

        public cxmlDataType getType(string stype)
        {
            cxmlDataType ret = cxmlDataType.dtUndefined;
            foreach (KeyValuePair<cxmlDataType, string> kvp in DataType)
            {
                if(kvp.Value == stype)
                    ret = kvp.Key;
            }
            return ret;
        }

        //Start SIW139
        //If this property is set to true, "" will be converted to <b> when passed into the xml objects
        public bool Blanking
        {
            get { return m_blanking; }
            set { m_blanking = value; }
        }
        //End SIW139

        //Get/Set function for m_ReportErrors, If set to TRUE, reports all retrieval errors
        public bool ReportErrors 
        {
            get{return m_ReportErrors;} 
            set{m_ReportErrors = value;}
        }

        //Used to access the Xml Version
        public string XmlDocumentVersion
        {
            get{return Constants.sCCPXmlVersion;}
        }

        //Returns the name of the data type in string format
        public string XmlDataType(cxmlDataType dtDataType)
        {
            string sResult;

            if (!typeExists(dtDataType))
                sResult = cxmlDataType.dtUndefined.ToString();
            else
                sResult = getType(dtDataType);

            return sResult;
        }

        //Returns the open XML tag with given name attribute and element 
        //in the form '<sElementName sAttributeName="sAttributeCode">'
        public string XMLElementBeginTag(string sElementName,
                                         string sAttributeName,
                                         string sAttributeCode)
        {
            string sXMLline;

            sXMLline = "<" + sElementName;

            if (sAttributeCode == "" || sAttributeCode == null)
                sXMLline = sXMLline + " " + sAttributeName + "=" +
                        (char)34 + sAttributeCode + (char)34 + ">";
            else
                sXMLline = sXMLline + ">";

            return sXMLline;
        }

        //Returns the close XML tag with the given element
        //in the form '</sElementName>'
        public string XMLElementEndTag(string sElementName)
        {
            string sXMLline;

            sXMLline = "</" + sElementName + ">";

            return sXMLline;
        }

        //Returns an XML Element with the given name and data
        //i.e. <sElementName>sDataValue</sElementName>
        public XmlElement XMLElementWithData_Node(string sElementName,
                                              string sDataValue)
        {
            XmlElement xElement;
                    
            xElement = (XmlElement)XMLNewElement(sElementName,false,null,null);

            XmlData = sDataValue;
            
            return xElement;
        }
        //Start SI06468
        public void XMLInsertCode_Node(XmlNode xNode,
                                       string sElementDescription,
                                       string sAttributeCode)
        {
            XMLInsertCode_Node(xNode, sElementDescription, sAttributeCode, "");
        }
        //End SI06468
        //Inserts the CODE attribute with value sAttributeCode and changes the data
        //e.g. <nodeName CODE="sAttributeCode">sElementDescription</nodeName>
        /*public void XMLInsertCode_Node(XmlNode xNode,
                                       string sElementDescription,
                                       string sAttributeCode) SI06468*/
        public void XMLInsertCode_Node(XmlNode xNode,
                               string sElementDescription,
                               string sAttributeCode,
                               string sAttributeId)//SI06468
        {
            XMLInsertCode_Node(xNode, sElementDescription, sAttributeCode, "","");//SIW321
        }
        //public void XMLInsertCode_Node(XmlNode xNode,
        //                       string sElementDescription,
        //                       string sAttributeCode,
        //                       string sAttributeId)//SI06468
        //Start SIW321
        public void XMLInsertCode_Node(XmlNode xNode,
                                       string sElementDescription,
                                       string sAttributeCode,
                                       string sAttributeId //SI06468
                                       ,string IDName)     //SIW321
        {
            if (xNode != null)
                m_xmlNode = xNode;
            if (IDName.Trim() == "") { IDName = "ID"; }
            XMLSetAttributeValue(m_xmlNode, Constants.sdtCode, sAttributeCode);
            //Start SI06468 
            if (sAttributeId != "")
            {
                //XMLSetAttributeValue(m_xmlNode, "ID", sAttributeId);//SIW321
                XMLSetAttributeValue(m_xmlNode, IDName, sAttributeId);//SIW321
            }
            // End SI06468
            if (sElementDescription != "" && sElementDescription != null)
                XmlData = sElementDescription;
        }

        //End SIW321
        public string XmlData
        {
            get{return (string)XMLExtractElement(null,null);}
            set 
            {
                string sProcName;
                string sData;
                XmlNode xElement;

                sProcName = "XML.XmlData";

                if (m_xmlNode == null)
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, sProcName);
                }
                else
                {
                    if (m_xmlNode.NodeType != XmlNodeType.Element)
                    {
                        Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_NODE, sProcName, m_xmlNode.Name + m_xmlNode.NodeType);
                    }
                    else
                    {
                        xElement = m_xmlNode;
                        sData = EncryptData(value, cxmlDataType.dtString);
                        xElement.InnerText = sData;
                        //xElement.Value  = sData;
                    }
                }
            }


        }

        //Returns the value of the given element searching from a starting node
        public string XMLExtractElement(XmlNode xStartNode,
                                        string sElementName)
        {
            XmlElement xElement;
            XmlNode xNode;
            string sElementValue;
            string sProcName;

            sProcName = "XML.XMLExtractElement";

            if (xStartNode != null)
                xNode = xStartNode;
            else
                xNode = m_xmlNode;

            sElementValue = "";

            if (sElementName != "" && sElementName != null)
                XMLGetNode(xNode,sElementName);
            else
                m_xmlNode = xNode;

            if (m_xmlNode == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NODE_NOT_FOUND, sProcName, sElementName, m_xmlDoc, m_xmlNode, m_ReportErrors);
            }
            else
            {
                if (m_xmlNode.NodeType != XmlNodeType.Element)
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_NODE, sProcName, sElementName + m_xmlNode.NodeType, m_xmlDoc, m_xmlNode, m_ReportErrors);
                }
                else
                {
                    xElement = (XmlElement)m_xmlNode;
                    sElementValue = xElement.InnerText;
                    if(StringUtils.Left(sElementValue,1) == "~")
                    {
                        sElementValue = StringUtils.Decrypt(StringUtils.Mid(sElementValue, 2));
                    }
                }
            }

            return sElementValue;
        }

        //Returns the node with the name sNodeName from the current document starting from xStartNode if specified
        public XmlNode XMLGetNode(XmlNode xStartNode,
                                 string sNodeName)
        {
            XmlNode xNode;
            string sProcName;

            sProcName = "XML.XMLGetNode";
            if (xStartNode == null)
                xNode = m_xmlDoc.DocumentElement;
            else
                xNode = xStartNode;

            if (sNodeName != null && sNodeName != "")
                m_xmlNode = xNode.SelectSingleNode(sNodeName);
            else
            {
                m_xmlNode = null;
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sNodeName, m_xmlDoc, null, m_ReportErrors);
            }

            if (m_xmlNode == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NODE_NOT_FOUND, sProcName, sNodeName, m_xmlDoc, null, m_ReportErrors);
            }

            return m_xmlNode;
        }

        //Encrypts data
        private string EncryptData(string sData,
                                   cxmlDataType cDataType)
        {
            bool bData;
            bool bEncrypt;

            if (sData != "" && sData != null)
            {
                bEncrypt = false;

                switch (cDataType)
                {
                    case cxmlDataType.dtBoolean:
                        if (sData.Length >= 2 && StringUtils.Left(sData, 2) != "~~")
                        {
                            if (sData.Length >= 1 && StringUtils.Left(sData, 1) == "~")
                            {
                                bEncrypt = true;
                                bData = StringUtils.ConvertToBool(StringUtils.Mid(sData, 2));
                            }
                            else
                                bData = StringUtils.ConvertToBool(sData);

                            if (bData)
                                sData = "Yes";
                            else
                                sData = "No";
                        }
                        break;
                    case cxmlDataType.dtNumber:
                        break;
                    case cxmlDataType.dtString:
                        break;
                    case cxmlDataType.dtUndefined:
                        break;
                    case cxmlDataType.dtDate:
                        break;
                    case cxmlDataType.dtTime:
                        break;
                    default : 
                        break;
                }
                

                if (sData.Length >= 2 && StringUtils.Left(sData, 2) == "~~")
                    sData = StringUtils.Mid(sData,2);
                else if (sData.Length >= 1 && StringUtils.Left(sData, 1) == "~") 
                    sData = "~" + StringUtils.Encrypt(StringUtils.Mid(sData, 2));
                else if(bEncrypt)
                    sData = "~" + StringUtils.Encrypt(sData);
            }
            return sData;
        }

        //Sets the given attribute to the given value in the given node
        //e.g. <xNode sAttribute="sNewValue"/>
        public void XMLSetAttributeValue(XmlNode xNode,
                                         string sAttribute,
                                         string sNewValue)
        {
            XmlAttribute xCurrAttrib;

            if (xNode != null)
                m_xmlNode = xNode;

            if (m_xmlNode != null)
            {
                xCurrAttrib = (XmlAttribute)XMLGetAttribute_Node(m_xmlNode, sAttribute);
                if (xCurrAttrib != null)
                {
                    sNewValue = EncryptData(sNewValue, cxmlDataType.dtDefault);
                    //xCurrAttrib.InnerXml = sNewValue;//SIW211
                    xCurrAttrib.InnerText = sNewValue;//SIW211
                }
                else
                    XMLInsertAttribute_Node(m_xmlNode, sAttribute, sNewValue);
            }
        }

        public XmlNode XMLNewElement(string sElmName, bool bInsert)
        {
            return XMLNewElement(sElmName, bInsert, null, null);
        }

        //Creates a new element named sElmName and inserts it into the given Document xDoc if bInsert is true
        //with the given parent node xParentNode
        //e.g. <sElmName/>
        public XmlNode XMLNewElement(string sElmName,
                                    bool bInsert,
                                    XmlDocument xDoc,
                                    XmlNode xParentNode)
        {
            XmlElement xElement;
            string sProcName;

            sProcName = "XML.XMLNewElement";

            if (xDoc != null)
                m_xmlDoc = xDoc;

            if (xParentNode != null)
                m_xmlNode = xParentNode;
            
            if (m_xmlDoc == null )
            {
                if (m_xmlNode != null)
                    m_xmlDoc = (XmlDataDocument)m_xmlNode.OwnerDocument;
                else
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, sProcName, sElmName);
                }
            }

            if (m_xmlDoc != null)
            {
                xElement = m_xmlDoc.CreateElement(sElmName);
                if (bInsert)
                {
                    if (m_xmlNode == null)
                        m_xmlNode = m_xmlDoc.DocumentElement;

                    m_xmlNode.AppendChild(xElement);
                }
                m_xmlNode = xElement;
            }

            return m_xmlNode;
        }

        //Adds a node to the current document within a specified parent node if specified, otherwise
        //as a child to the last element
        //e.g. <xParentNode>
        //        <xNodeToAdd/>
        //     </xParentNode>
        public void XMLAddElement(XmlNode xNodeToAdd, 
                                  XmlNode xParentNode)
        {
            XmlDocument xDoc;
            XmlNode xNodeClone; // SI06499
            xDoc = Document;

            // Start SI06499
            if (xParentNode == null)
            {
                xNodeClone = xDoc.ImportNode(xNodeToAdd, true);
                xDoc.LastChild.AppendChild(xNodeClone);
            }
            else
            {
                xNodeClone = xParentNode.OwnerDocument.ImportNode(xNodeToAdd, true);
                xParentNode.AppendChild(xNodeClone);
            }

            m_xmlNode = xNodeClone;
            // End SI06499
        }

        //Adds a new node to the document with name sNodeName and
        public XmlNode XMLaddNode(XmlNode xNode, 
                                  string sNodeName, 
                                  ArrayList arrNodeList)
        {
            XmlNode xNewNode;
            xNewNode = (XmlNode)XMLNewElement(sNodeName, false, null, null);
            //xNewNode = (XmlNode)XMLNewElement(sNodeName, true, null,xNode);
            xNewNode = XMLInsertNodeInPlace(xNode, xNewNode, arrNodeList);
            return xNewNode;
        }

        private string XMLCheckSpecialChars(string sStringToCheck)
        {
            char sSplitChar;
            string sJoinChar;

            //Could use Convert.ToChar();
            sSplitChar = (char)38;
            sJoinChar = "&amp;";
            sStringToCheck = XMLReplaceSpecialChars(sStringToCheck, sSplitChar, sJoinChar);

            sSplitChar = (char)60;
            sJoinChar = "&lt;";
            sStringToCheck = XMLReplaceSpecialChars(sStringToCheck, sSplitChar, sJoinChar);

            sSplitChar = (char)62;
            sJoinChar = "&gt;";
            sStringToCheck = XMLReplaceSpecialChars(sStringToCheck, sSplitChar, sJoinChar);

            sSplitChar = (char)34;
            sJoinChar = "&quot;";
            sStringToCheck = XMLReplaceSpecialChars(sStringToCheck, sSplitChar, sJoinChar);

            sSplitChar = (char)39;
            sJoinChar = "&apos;";
            sStringToCheck = XMLReplaceSpecialChars(sStringToCheck, sSplitChar, sJoinChar);

            return sStringToCheck;
        }

        //Creates a new element node named sElementName holding the date passed in sAttributeDate as attributes
        //e.g. XMLElementAsDate_Node("DATE_ELEMENT","01/23/2001")
        // to  <DATE_ELEMENT YEAR="2001" MONTCH="01" DAY="23"/>
        public XmlElement XMLElementAsDate_Node(string sElementName, 
                                            string sAttributeDate)
        {
            XmlDocument xDoc;
            XmlElement xElement;
            string sProcName;

            sProcName = "XML.XMLElementAsDate_Node";

            xDoc = Document;

            if (sElementName == "" || sElementName == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sElementName);
                xElement = null;
            }
            else
            {
                xElement = xDoc.CreateElement(sElementName);
                XMLSetDateAttributes(xElement, sAttributeDate);
            }

            return xElement;
        }

        public XmlElement XMLElementAsTime_Node(string sElementName, string sAttributeTime)
        {
            return XMLElementAsTime_Node(sElementName, sAttributeTime, "EST");
        }

        //Creates a new element node named sElementName holding the time passed in sAttributeTime as attributes
        //in the given time zone sTimezone
        //e.g. XMLElementAsTime_Node("TIME_ELEMENT", "10:30:45", "EST" 
        // to  <TIME_ELEMENT HOUR="10" MINUTE="30" SECOND="45" TIME_ZONE="EST"
        public XmlElement XMLElementAsTime_Node(string sElementName,
                                            string sAttirbuteTime,
                                            string sTimezone)
        {
            XmlDocument xDoc;
            XmlElement xElement;
            string sProcName;

            sProcName = "XML.XMLElementAsTime_Node";

            xDoc = Document;

            if (sElementName == "" || sElementName == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sElementName);
                xElement = null;
            }
            else
            {
                xElement = xDoc.CreateElement(sElementName);
                XMLSetTimeAttributes(xElement, sAttirbuteTime, sTimezone);
            }

            return xElement;
        }

        //Creates a node with an address attribute
        //e.g. <sElementName ID/sAttribName="ADRsAttributeCode"/>
        public XmlNode XMLElementWithAddress_Node(string sElementName,
                                                 string sAttributeCode,
                                                 string sAttribName)
        {
            XmlNode xElement;

            if ((sAttribName == null) || (sAttribName == ""))
            {
                sAttribName = "ID";
            }

            if (StringUtils.Left(sAttributeCode, 1) == "~")
                sAttributeCode = "~" + Constants.sCCPXmlAddressIDPrefix + StringUtils.Mid(sAttributeCode, 2);
            else if (StringUtils.Left(sAttributeCode, 2) != "~~")
                sAttributeCode = Constants.sCCPXmlAddressIDPrefix + sAttributeCode;

            xElement = XMLNewElement(sElementName, false, null, null);

            XMLSetAttributeValue(xElement, sAttribName, sAttributeCode);

            return xElement;
        }
        
        //Start SI06468
        public XmlNode XMLElementWithCode_Node(string sElementName,
                                             string sElementDescription,
                                            string sAttributeCode)
        { 
        return XMLElementWithCode_Node(sElementName,sElementDescription,sAttributeCode,"");
        }
        //End SI06468

        //Creates a node with given info
        //e.g. <sElementName CODE="sAttributeCode">sElementDescription</sElementName>
        // SI06468 public XmlNode XMLElementWithCode_Node(string sElementName,
        //                                      string sElementDescription,
        //                                      string sAttributeCode)
        public XmlNode XMLElementWithCode_Node(string sElementName,
                                              string sElementDescription,
                                              string sAttributeCode,
                                              string sAttributeID) //SI06468
        {
            XmlNode xElement;

            xElement = XMLElementWithData_Node(sElementName, sElementDescription);
            XMLSetAttributeValue(xElement, Constants.sdtCode, sAttributeCode);
            //Start SI06468
            if (sAttributeCode != "")
            {
                XMLSetAttributeValue(xElement, "ID", sAttributeID);
            } // End SI06468
            return xElement;
        }

        public XmlNode XMLElementWithEntity_Node(string sElementName, string sAttributeCode)
        {
            return XMLElementWithEntity_Node(sElementName, sAttributeCode, "ID");
        }

        //Creates a node with an entity attribute
        //e.g. <sElementName ID/sAttribName="ENTsAttributeCode"/>
        public XmlNode XMLElementWithEntity_Node(string sElementName,
                                                string sAttributeCode,
                                                string sAttribName)
        {
            XmlNode xElement;

            xElement = XMLNewElement(sElementName, false, null, null);

            if ((sAttribName == null) || (sAttribName == ""))
            {
                sAttribName = "ID";
            }

            XMLSetAttributeValue(xElement, sAttribName, Constants.sCCPXmlEntityIDPrefix + sAttributeCode.Trim());

            return xElement;
        }

        //Creates a new node with an Indicator attribute
        //e.g. <sElementName INDICATOR="bAttributeBoolean"/>
        public XmlNode XMLElementWithIndicator_Node(string sElementName,
                                                   bool bAttributeBoolean)
        {
            XmlNode xElement;
            string sAttributeCode;

            if (bAttributeBoolean)
                sAttributeCode = "Yes";
            else
                sAttributeCode = "No";

            xElement = XMLNewElement(sElementName, false, null, null);
            XMLSetAttributeValue(xElement, Constants.sIndicator, sAttributeCode);

            return xElement;
        }

        public XmlNode XMLElementWithType_Node(string sElementName,
                                              string sElementDescription,
                                              string sAttributeCode)
        {
            XmlNode xElement;

            xElement = XMLElementWithCode_Node(sElementName, sElementDescription, sAttributeCode);
            XMLSetAttributeValue(xElement, Constants.sdtType, sAttributeCode);

            return xElement;
        }

        //Returns the value of the given attribute of the given node
        public string XMLExtractAddressAttribute(XmlNode xNode,
                                                 string sAttribName)
        {
            string sValue;
            string sID;

            if ((sAttribName == null) || (sAttribName == ""))
            {
                sAttribName = "ID";
            }

            sValue = XMLGetAttributeValue(xNode, sAttribName);

            if ((StringUtils.Left(sValue,3) == Constants.sCCPXmlAddressIDPrefix) && (sValue.Length >= 4))
                sID = StringUtils.Right(sValue,sValue.Length - 3);
            else
                sID = "";

            return sID;
        }

        //Returns the value of the given boolean type attribute
        public string XMLExtractBooleanAttribute(XmlNode xNode,
                                                 string sAttributeName)
        {
            bool bResults;

            bResults = XMLExtractIndicatorAttribute(xNode, sAttributeName);

            return bResults.ToString();
        }

        //Returns the value of the CODE attribute
        public string XMLExtractCodeAttribute(XmlNode xNode)
        {
            return XMLGetAttributeValue(xNode, Constants.sdtCode);
        }

        public string XMLExtractDTTM_Date(XmlNode dateNode)
        {
            return XMLExtractDate_Date(dateNode) + " " + XMLExtractTime_Time(dateNode);
        }

        //Returns the value of the date attributes
        public string XMLExtractDate_Date(XmlNode xDateNode)
        {
            string sYearAttrib;
            string sMonthAttrib;
            string sDayAttrib;
            DateTime oDate;
            string sDate = "";
            string sProcName;

            sProcName = "XML.XMLExtractDate_Date";
            Debug.PushProc(sProcName);

            try
            {

                    sMonthAttrib = XMLGetAttributeValue(xDateNode, Constants.sDateMonth);
                    sDayAttrib = XMLGetAttributeValue(xDateNode, Constants.sDateDay);
                    sYearAttrib = XMLGetAttributeValue(xDateNode, Constants.sDateYear);

                    sDate = sMonthAttrib + "/" + sDayAttrib + "/" + sYearAttrib;

                    if (DateTimeUtils.IsDate(sDate))
                    {
                        oDate = DateTime.Parse(sDate);
                        sDate = oDate.ToShortDateString();
                    }
                    else
                    {
                        response = Errors.ProcessAppError(ErrorGlobalConstants.ERRC_INVALID_DATE_VALUE, MessageBoxButtons.OK,
                                                          "Node processing is skipped", m_xmlNode.Name, sDate);
                        sDate = "";
                    }
                }
   
            catch
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, sProcName, sDate, m_xmlDoc, m_xmlNode);
                sDate = "";
            }
            Debug.PopProc();
            return sDate;
        }

        public string XMLExtractEntityAttribute(XmlNode xNode)
        {
            return XMLExtractEntityAttribute(xNode, "ID");
        }

        //Returns the value of the given Entity attribute
        public string XMLExtractEntityAttribute(XmlNode xNode,
                                                string sAttribName)
        {
            string sEntValue;
            string sEntID;

            if ((sAttribName == null) || (sAttribName == ""))
            {
                sAttribName = "ID";
            }

            sEntValue = XMLGetAttributeValue(xNode, sAttribName);

            if ((StringUtils.Left(sEntValue, 3) == Constants.sCCPXmlEntityIDPrefix) &&
                (sEntValue.Length >= 4))
                sEntID = StringUtils.Right(sEntValue, sEntValue.Length - 3);
            else
                sEntID = "";
            return sEntID;
        }

        //Returns the value of the Indicator or named attribute for the given node
        public bool XMLExtractIndicatorAttribute(XmlNode xNode,
                                                   string sAttribName)
        {
            string sInd;
            string sIndName;
            bool bInd;

            if (sAttribName == "" || sAttribName == null)
                sIndName = Constants.sIndicator;
            else
                sIndName = sAttribName;

            sInd = XMLGetAttributeValue(xNode,sIndName);

            sInd = sInd.ToUpper();

            bInd = false;

            switch (sInd)
            {
                case "TRUE":
                case "YES":
                case "ON":
                case "ONE":
                case "1":
                case "-1":
                    bInd = true;
                    break;

                case "FALSE":
                case "NO":
                case "OFF":
                case "ZERO":
                case "0":
                    bInd = false;
                    break;
            }

            //not needed?
            //sInd = XMLGetAttributeValue(xNode, sIndName);
            return bInd;
        }

        //Returns the values in the time attributes
        public string XMLExtractTime_Time(XmlNode xNode)
        {
            string sHourAttrib;
            string sMinAttrib;
            string sSecAttrib;
            string sTime = "";
            string sProcName;
            DateTime oTime;

            sProcName = "XML.XMLExtractTime_Time";
            Debug.PushProc(sProcName);

            try
            {
                sHourAttrib = XMLGetAttributeValue(xNode, Constants.sTimeHour);
                sMinAttrib = XMLGetAttributeValue(xNode, Constants.sTimeMinute);
                sSecAttrib = XMLGetAttributeValue(xNode, Constants.sTimeSecond);

                sTime = sHourAttrib + ":" + sMinAttrib + ":" + sSecAttrib;

                if (DateTimeUtils.IsDate(sTime))
                {
                    oTime = DateTime.Parse(sTime);
                    sTime = oTime.ToLongTimeString();
                }
                else
                {
                    response = Errors.ProcessAppError(ErrorGlobalConstants.ERRC_INVALID_TIME_VALUE, MessageBoxButtons.OK,
                                                      "Node processing is skipped", m_xmlNode.Name, sTime);
                    sTime = "";
                }
            }
            catch
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, sProcName, sTime, m_xmlDoc, m_xmlNode);
                sTime = "";
            }
            Debug.PopProc();
            return sTime;
        }
        //Start SI06468
        public string XMLExtractCodeIDAttribute(XmlNode cnode)
        {
            return XMLExtractCodeIDAttribute(cnode, "");//SIW321
        }
        //End SI06468
        //Start SIW321
        public string XMLExtractCodeIDAttribute(XmlNode cnode, string AttribName)
        {
            if (AttribName.Trim() == "") { AttribName = "ID"; }
            return XMLGetAttributeValue(cnode, AttribName);
        }
        //End SIW321

        //Returns the value in the TYPE attribute.
        public string XMLExtractTypeAttribute(XmlNode cnode)
        {
            return XMLGetAttributeValue(cnode, Constants.sdtType);
        }

        //Start SI06468
        public cxmlDataType XMLExtractVariableDataType(XmlNode xNode,
                                                       out string sData,
                                                       out string sCode)
        {
            string sid = "";
            return XMLExtractVariableDataType(xNode, out sData, out sCode, out sid);
        }
        //End SI06468

        //Extracts the values from a variable data dype node.
        //SI06468 public cxmlDataType XMLExtractVariableDataType(XmlNode xNode,
        //                                         out string sData,
        //                                         out string sCode)
        public cxmlDataType XMLExtractVariableDataType(XmlNode xNode,
                                                     out string sData,
                                                 out string sCode,
                                                 out string sID)    //SI06468
        {
            XmlNode xElement;
            string dtType;
            cxmlDataType sDataType = cxmlDataType.dtUndefined;
            bool bKeyExists;

            sData = "";
            sCode = "";
            sID = ""; //SI06468

            if (xNode == null)
                xElement = CurrNode;
            else
                xElement = xNode;

            dtType = XMLExtractTypeAttribute(xElement);

            bKeyExists = typeExists(dtType);
            if (bKeyExists)
                sDataType = getType(dtType);
            
            if (xElement != null)
            {
                switch ((cxmlDataType)sDataType)
                {
                    case cxmlDataType.dtUndefined:
                        sCode = (string)XMLExtractCodeAttribute(xElement);
                        sID = XMLExtractCodeIDAttribute(xElement);        //SI06468
                        sData = xElement.InnerText;
                        break;
                    case cxmlDataType.dtDate:
                        sData = XMLExtractDate_Date(xElement);
                        break;
                    case cxmlDataType.dtTime:
                        sData = XMLExtractTime_Time(xElement);
                        break;
                    case cxmlDataType.dtNumber:
                        sData = xElement.InnerText;
                        break;
                    case cxmlDataType.dtString:
                        sData = xElement.InnerText;
                        break;
                    case cxmlDataType.dtCode:
                        sCode = (string)XMLExtractCodeAttribute(xElement);
                        sID = XMLExtractCodeIDAttribute(xElement);  //SI06468   
                        sData = xElement.InnerText;
                        break;
                    case cxmlDataType.dtBoolean:
                        sData = (string)XMLExtractBooleanAttribute(xElement, null);
                        break;
                    //SIW8421a - start
                    case cxmlDataType.dtDataRecord:
                        sCode = XMLExtractCodeIDAttribute(xElement, "RECORDID");        
                        sData = xElement.InnerText;
                        break;
                    //SIW8421a - end  

                }
            }

            return sDataType;
        }

        //Formats an error string
        public string XMLFormatParseError(Exception e)
        {
            string sParseError;
            //XmlDocument xmlDoc;
            sParseError = "";

            if (e.GetType() == (new System.Xml.Schema.XmlSchemaValidationException()).GetType())
            {
                sParseError = sParseError + "Error Message:" + ((XmlSchemaValidationException)e).Message + "\r\n";
                sParseError = sParseError + "Error From:   " + ((XmlSchemaValidationException)e).TargetSite + "\r\n";
                sParseError = sParseError + "Line Number:  " + ((XmlSchemaValidationException)e).LineNumber + "\r\n";
                sParseError = sParseError + "Line Position:" + ((XmlSchemaValidationException)e).LinePosition + "\r\n";
                sParseError = sParseError + "Source:       " + ((XmlSchemaValidationException)e).SourceUri + "\r\n";
            }
            return sParseError;
        }

        //Returns the attribute named sAttribName from node xNode, or current node if not specified
        public XmlNode XMLGetAttribute_Node(XmlNode xNode, 
                                           string sAttribName)
        {
            XmlNode currAttrib;
            XmlAttributeCollection attNodeMap;
            string sProcName;

            sProcName = "XML.XMLGetAttribute_Node";

            if (xNode != null)
                m_xmlNode = xNode;

            currAttrib = null;

            if (m_xmlNode != null)
            {
                attNodeMap = m_xmlNode.Attributes;
                if (attNodeMap != null)
                    currAttrib = attNodeMap.GetNamedItem(sAttribName);
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, sProcName, sAttribName, null, null, m_ReportErrors);
            }

            if (currAttrib == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_ATTRIBUTE_NOT_FOUND, sProcName, sAttribName, null, null, m_ReportErrors);
            }

            return currAttrib;
        }

        //Returns the value of the given attribute name
        public string XMLGetAttributeValue(XmlNode xNode, string sAttName)
        {
            XmlNode xCurrAttrib;
            string sAttribValue;

            if (xNode != null)
                m_xmlNode = xNode;

            xCurrAttrib = XMLGetAttribute_Node(m_xmlNode, sAttName);

            if (xCurrAttrib == null)
                sAttribValue = "";
            else
                sAttribValue = xCurrAttrib.Value;

            if (StringUtils.Left(sAttribValue, 1) == "~")
                sAttribValue = StringUtils.Decrypt(StringUtils.Mid(sAttribValue, 2));

            return sAttribValue;
        }

        //Returns a collection of nodes with the given name from the starting node given
        public XmlNodeList XMLGetNodes(XmlNode xStartNode,
                                  string sNodeName)
        {
            XmlNode xmlStartNode;
            string sProcName;

            sProcName = "XML.XMLGetNodes";
            if(xStartNode == null)
                xmlStartNode = m_xmlDoc.DocumentElement;
            else
                xmlStartNode = xStartNode;

            if(sNodeName != "" && sNodeName != null)
                m_xmlNodeList = xmlStartNode.SelectNodes(sNodeName);
            else
            {
                m_xmlNodeList = null;
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sNodeName, m_xmlDoc, null, m_ReportErrors);
            }

            if(m_xmlNodeList == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NODE_NOT_FOUND, sProcName, sNodeName, m_xmlDoc, null, m_ReportErrors);
            }

            return m_xmlNodeList;
        }

        //Inserts an Address attribute to the given node.
        public void XMLInsertAddress_Node(XmlNode xNode,
                                          string sAttributeCode,
                                          string sAttributeName)
        {
            if ((sAttributeName == null) || (sAttributeName == ""))
            {
                sAttributeName = "ID";
            }

            XMLSetAttributeValue(xNode, sAttributeName, "ADR" + sAttributeCode.Trim());
        }

        //Adds an additional attribute to the given node.
        //e.g. <nodeName sAttributeName="sAttributeValue"/>
        public XmlAttribute XMLInsertAttribute_Node(XmlNode xNode,
                                              string sAttributeName,
                                              string sAttributeValue)
        {
            XmlAttribute xAttrib;
            XmlElement xElement;
            string sProcName;

            sProcName = "XML.XMLInsertAttribute_Node";

            if (xNode != null)
                m_xmlNode = xNode;

            xElement = (XmlElement)m_xmlNode;

            if (sAttributeName == "" || sAttributeName == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sAttributeName);
                xAttrib = null;
            }
            else
            {
                xAttrib = m_xmlDoc.CreateAttribute(sAttributeName);
                sAttributeValue = EncryptData(sAttributeValue, cxmlDataType.dtDefault);
                xAttrib.Value = sAttributeValue;
            }

            xElement.SetAttributeNode(xAttrib);

            return xAttrib;
        }

        //Inserts a boolean attribute into a node.
        //e.g. <nodeName sAttributeName="sAttributeBoolean"/>
        public void XMLInsertBoolean_Node(XmlNode oNode,
                                          string sAttributeName,
                                          string sAttributeBoolean)
        {
            string sAttributeCode;

            if (sAttributeName == "" || sAttributeName == null)
                sAttributeName = Constants.sIndicator;

            sAttributeCode = EncryptData(sAttributeBoolean, cxmlDataType.dtBoolean);

            if (StringUtils.Left(sAttributeCode, 2) != "~~")
            {
                if (StringUtils.Left(sAttributeCode, 1) == "~")
                    sAttributeCode = "~" + sAttributeCode;
            }

            XMLSetAttributeValue(oNode, sAttributeName, sAttributeCode);
        }

        public void XMLInsertEntity_Node(XmlNode xNode, string sAttributeCode)
        {
            XMLInsertEntity_Node(xNode, sAttributeCode, "ID");
        }

        //Inserts an Entity attribute to the given node.
        public void XMLInsertEntity_Node(XmlNode xNode,
                                         string sAttributeCode,
                                         string sAttribName)
        {

            if ((sAttribName == null) || (sAttribName == ""))
            {
                sAttribName = "ID";
            }

            XMLSetAttributeValue(xNode, sAttribName, "ENT" + sAttributeCode.Trim());
        }

        //Inserts the given node in the correct place within the parent node using the given ordering
        public XmlNode XMLInsertNodeInPlace(XmlNode xParentNode,
                                            XmlNode xNewNode,
                                            ArrayList arrNodeList)
        {
            XmlNode xmlPNode;
            XmlNode xmlNode;
            XmlNode xmlBefore;
            string sNodeName;
            int iIndex;
            int iIndex2;

            if (xParentNode == null)
                xmlPNode = m_xmlDoc.DocumentElement;
            else
                xmlPNode = xParentNode;

            if(xNewNode == null)
                xmlNode = CurrNode;
            else
                xmlNode = xNewNode;

            if((xmlNode == null) || (xmlPNode == null))
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, "XML.XMLInsertNodeinPlace",
                                 "Both Nodes Required", m_xmlDoc, xNewNode);
            }
            else
            {
                sNodeName = xmlNode.Name;
                for(iIndex=0; iIndex < arrNodeList.Count; iIndex++)
                {
                    if ((string)arrNodeList[iIndex] == sNodeName)
                        break;
                }

                xmlBefore = null;
                for (iIndex2 = iIndex + 1; iIndex2 < arrNodeList.Count; iIndex2++)
                {
                    if ((string)arrNodeList[iIndex2] != "" && arrNodeList[iIndex2] != null)
                    {
                        xmlBefore = xmlPNode.SelectSingleNode((string)arrNodeList[iIndex2]);
                        if (xmlBefore != null)
                            break;
                    }
                }
				//Start SIW183
				if (xmlNode.OwnerDocument != m_xmlDoc)
                {
                    xmlNode = m_xmlDoc.ImportNode(xmlNode, true);
                }
				//End SIW183
                xmlNode = xmlPNode.InsertBefore(xmlNode, xmlBefore);
            }
            
            return xmlNode;
        }

        //Inserts a TYPE attribute into the given node
        public void XMLInsertType_Node(XmlNode xNode,
                                       string sElementDescription,
                                       string sAttributeCode)
        {
            if(xNode != null)
                m_xmlNode = xNode;

            XMLSetAttributeValue(m_xmlNode,Constants.sdtType,sAttributeCode);
            if(sElementDescription != "" && sElementDescription != null)
                XmlData = sElementDescription;
        }

        //Inserts an attribute of the given data type into the given node with the given data
        //e.g. <nodeName dtDataType="sCode">sData</nodeName>
        public void XMLInsertVariable_Node(XmlNode xNode,
                                           cxmlDataType dtDataType,
                                           string sData,
                                           string sCode)
        {
            string sDataType;

            if (xNode != null)
                m_xmlNode = xNode;

            sDataType = getType(dtDataType);

            XMLInsertType_Node(null, "", sDataType);

            switch (dtDataType)
            {
                case cxmlDataType.dtUndefined:
                    if(sCode != "" && sCode != null)
                        XMLInsertCode_Node(null, sData, sCode);
                    else
                        XmlData = sData;

                    break;
                case cxmlDataType.dtDate:
                    XMLSetDateAttributes(null, sData);
                    XMLSetDateValue(null, sData);
                    break;
                case cxmlDataType.dtTime:
                    XMLSetTimeAttributes(null, sData,"");
                    XMLSetTimeValue(null, sData);
                    break;
                case cxmlDataType.dtDTTM:
                    XMLSetDTTMAttributes(null, sData);
                    XMLSetDTTMValue(null, sData);
                    break;
                case cxmlDataType.dtNumber:
                    XmlData = sData;
                    break;
                case cxmlDataType.dtString:
                    XmlData = sData;
                    break;
                case cxmlDataType.dtCode:
                    XMLInsertCode_Node(null, sData, sCode);
                    break;
                case cxmlDataType.dtBoolean:
                    XMLInsertBoolean_Node(null, "", sData);
                    break;
                case cxmlDataType.dtEntityRef:
                    XMLInsertEntity_Node(null, sData);
                    break;
                case cxmlDataType.dtDataRecord:
                    string[] sArr = sCode.Split(':');
                    if (sArr.GetUpperBound(0) >= 1)
                    {
                        XMLSetAttributeValue(null, Constants.sdtDataRecord, sArr[0]);
                        XMLSetAttributeValue(null, "RECORDID", sArr[1]);
                    }
                    XmlData = sData;
                    break;
            }
        }

        public XmlDocument XMLLoadDocument(string sfile)
        {
            XmlDocument xDoc;
            FileFunctions FleFnc;
            string sProcName;

            sProcName = "XML.XMLLoadDocument";
            Debug.PushProc(sProcName, sfile, null, null, null);

            xDoc = null;

            if (sfile == "" || sfile == null)
            {
                Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_NONAME, MessageBoxButtons.OKCancel,
                                       "", Errors.ErrorDescription(ErrorGlobalConstants.ERRC_OK_CANCEL));
                Debug.PopProc();
                return xDoc;
            }

            FleFnc = new FileFunctions();

            if (!FleFnc.FileExists("", sfile))
            {
                Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_NOTEXISTS, MessageBoxButtons.OKCancel,
                                       Errors.ErrorDescription(ErrorGlobalConstants.ERRC_OK_CANCEL), sfile);
                Debug.PopProc();
                return xDoc;
            }

            xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sfile);
                Document = xDoc;    //SIN204
                Debug.PopProc();
                return xDoc;
            }
            catch (XmlException e)
            {
                string sparseerror = XMLFormatParseError(e);
                Errors.ProcessAppError(ErrorGlobalConstants.ERRC_DOCUMENT_LOAD_FAILED, MessageBoxButtons.AbortRetryIgnore, null, sparseerror, sfile);
                Debug.PopProc();
                return xDoc;
            }
        }
        
        //Creates a new XMLDocument
        public XmlDocument XMLNewDoc(string sDocumentName)
        {
            XmlProcessingInstruction xmlPI;
            XmlDocument xmlDoc;
            XmlNode xmlNode;

            Errors.Clear();

            xmlDoc = null;
            xmlDoc = new XmlDocument();
            xmlNode = xmlDoc.CreateNode(XmlNodeType.Element, sDocumentName, "");
            xmlDoc.AppendChild(xmlNode);
            xmlPI = xmlDoc.CreateProcessingInstruction("xml", "version=" + (char)34 + "1.0" + (char)34);
            xmlDoc.InsertBefore(xmlPI, xmlDoc.FirstChild);

            m_xmlDoc = xmlDoc;
            return xmlDoc;
        }

        //Inserts a node with the given parent.  Deletes the node if xNode is null.
        public XmlNode XMLputData(XmlNode xParentNode,
                                  ArrayList arrNodeList,
                                  string sNodeName,
                                  XmlNode xNode)
        {
            XmlNode xmlNode;
            XmlElement xmlDataNode;
            XmlNode xmlNewNode;
            string sProcName;

            sProcName = "XML.XMLputData";
            xmlDataNode = null;

            xmlNode = xParentNode;
            if (xmlNode == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, sProcName, "Parent Node");
                return null;
            }

            xmlNewNode = (XmlNode)xNode;
            if((sNodeName == "" || sNodeName == null) && (xmlNewNode != null))
                sNodeName = xmlNewNode.Name;

            if(sNodeName == "" || sNodeName == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sNodeName);
            }
            else
            {
                xmlDataNode = (XmlElement)XMLGetNode(xmlNode, sNodeName);
                if(xmlDataNode != null)
                    xmlDataNode = (XmlElement)xmlNode.ReplaceChild(xmlNewNode,xmlDataNode);
                else if(xmlNewNode != null)
                    xmlDataNode = (XmlElement)XMLInsertNodeInPlace(xmlNode, xmlNewNode, arrNodeList);
            }

            return xmlDataNode;
        }

        //Inserts a node with the given parent.  Deletes the node if xNode is null.
        public XmlNode XMLputData(XmlNode xParentNode,
                                  ArrayList arrNodeList,
                                  string sNodeName,
                                  string sNewValue)
        {
            XmlNode xmlNode;
            XmlElement xmlDataNode;
            XmlNode xmlNewNode;
            string sData;
            string sProcName;

            sProcName = "XML.XMLputData";
            xmlDataNode = null;

            xmlNode = xParentNode;
            if (xmlNode == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, sProcName, "Parent Node");
                return null;
            }

            sData = sNewValue;
            if (sData != "" && sData != null)
                xmlNewNode = (XmlNode)XMLElementWithData_Node(sNodeName, sData);
            else
                xmlNewNode = null;

            if (sNodeName == "" || sNodeName == null)
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE_NAME, sProcName, sNodeName);
            }
            else
            {
                xmlDataNode = (XmlElement)XMLGetNode(xmlNode, sNodeName);
                if (xmlDataNode != null)
                    xmlDataNode = (XmlElement)xmlNode.ReplaceChild(xmlNewNode, xmlDataNode);
                else if (xmlNewNode != null)
                    xmlDataNode = (XmlElement)XMLInsertNodeInPlace(xmlNode, xmlNewNode, arrNodeList);
            }

            return xmlDataNode;
        }

        //Removes the named attribute from the given node
        public void XMLRemoveAttribute_Node(XmlNode xNode,
                                            string sAttribute)
        {
            XmlNamedNodeMap attNodeMap;
            string sProcName;

            sProcName = "XML.XMLRemoveAttribute_Node";

            if (xNode != null)
                m_xmlNode = xNode;

            if (m_xmlNode != null)
            {
                attNodeMap = m_xmlNode.Attributes;
                attNodeMap.RemoveNamedItem(sAttribute);
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, sProcName, sAttribute, null, null, m_ReportErrors);
            }
        }

        //Removes the named node
        public void XMLRemoveNode(XmlNode xParentNode,
                                  XmlNode xCurrNode,
                                  string sNodeName)
        {
            XmlNode xnode;

            if (xCurrNode != null)
                xnode = xCurrNode;
            else if (sNodeName != "" && sNodeName != null)
                xnode = (XmlNode)XMLGetNode(xParentNode, sNodeName);
            else
                xnode = m_xmlNode;

            if (xParentNode != null)
            {
                if (xnode != null)
                {
                    xParentNode.RemoveChild(xnode);
                    CurrNode = xParentNode;
                }
            }
        }

        private string XMLReplaceSpecialChars(string sStringToScrub, 
                                              char cSplitChar, 
                                              string sJoinString)
        {
            string[] strArray;

            strArray = sStringToScrub.Split(cSplitChar);
            if (strArray.GetUpperBound(0) != 0)
                sStringToScrub = string.Join(sJoinString, strArray);

            return sStringToScrub;
        }

        //Saves the current document
        public int XMLSaveCurrentDocument(string fName,cxmlFileOptions optn)
        {
            return XMLSaveDocument(null, fName, optn);
        }

        //Saves the given document
        public int XMLSaveDocument(XmlDocument xmlDoc,
                                   string sDestinationFileName,
                                   cxmlFileOptions optn)
        {
            FileFunctions fleFuncs;
            bool bWrite;
            DialogResult resp;
            XmlDocument xDoc;
            bool bhie;

            Debug.PushProc("XMLSaveDocument", sDestinationFileName, optn, null, null);

            if (xmlDoc == null)
                xDoc = m_xmlDoc;
            else
                xDoc = xmlDoc;

            fleFuncs = new FileFunctions();

            bWrite = true;
            if (fleFuncs.FileExists("",sDestinationFileName)){
                switch (optn){
                    case cxmlFileOptions.cxmlFOPrompt:
                        resp = Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_EXISTS, MessageBoxButtons.OKCancel, Errors.ErrorDescription(ErrorGlobalConstants.ERRC_OK_CANCEL), sDestinationFileName);
                        if (resp == DialogResult.Cancel)
                        {
                            bWrite = false;
                            Debug.LogEntry(Errors.ErrorDescription(ErrorGlobalConstants.ERRC_ABORTED));
                        }
                        break;
                    case cxmlFileOptions.cxmlFOBackup:
                        //try
                        //{
                        if (fleFuncs.FileExists("", sDestinationFileName + ".bak"))
                            fleFuncs.FileDelete("", sDestinationFileName + ".bak");
                        fleFuncs.FileCopy("", sDestinationFileName, "", sDestinationFileName + ".bak");
                        //}
                        //catch
                        //{
                        //    //ignore errors
                        //}
                        break;
                }
                //string sFullName = fleFuncs.GetFile("", sDestinationFileName);
                if (bWrite)
                {
                    if ((fleFuncs.GetFile("", sDestinationFileName).Attributes & FileAttributes.ReadOnly) != 0)
                    {
                        resp = Errors.ProcessAppError(ErrorGlobalConstants.ERRC_FILE_READ_ONLY,
                                                              MessageBoxButtons.OKCancel, 
                                                              Errors.ErrorDescription(ErrorGlobalConstants.ERRC_OK_CANCEL),
                                                              sDestinationFileName);
                        if (resp == DialogResult.Cancel)
                        {
                            bWrite = false;
                            Debug.LogEntry(Errors.ErrorDescription(ErrorGlobalConstants.ERRC_ABORTED));
                        }
                        else
                        {
                            fleFuncs.FileFile.Attributes = fleFuncs.FileFile.Attributes & ~FileAttributes.ReadOnly;
                        }
                    }
                }
            }
            if (bWrite)
            {
                xDoc.Save(sDestinationFileName);
                bhie = Debug.IgnoreErrors;
                Debug.IgnoreErrors = true;
                Errors.MessageBox(ErrorGlobalConstants.ERRC_FILE_SAVE_SUCCESSFUL,
                                          MessageBoxIcon.Information, MessageBoxButtons.OK,
                                          null, null, sDestinationFileName);
                Debug.IgnoreErrors = bhie;
                Debug.PopProc();
                return 0;
            }
            else
            {
                Debug.PopProc();
                return ErrorGlobalConstants.ERRC_FILE_WRITE_GENERAL;
            }

        }

        public void XMLSetDTTMAttributes(XmlNode xDTTMNode, string sDTTMValue)
        {
            DateTime dteDTTMIn = DateTime.Now;
            string sDateIn = "";
            string sTimeIn = "";

            if (DateTimeUtils.IsDate(sDTTMValue))
            {
                DateTime.TryParse(sDTTMValue, out dteDTTMIn);
                sDateIn = dteDTTMIn.ToString("yyyyMMdd");
                sTimeIn = dteDTTMIn.ToString("hhmmss");
            }
            else
            {
                sDTTMValue = sDTTMValue + "              ";
                sDateIn = StringUtils.Left(sDTTMValue, 8);
                sTimeIn = StringUtils.Mid(sDTTMValue, 9, 6);
            }

            XMLSetDateAttributes(xDTTMNode, sDateIn);
            XMLSetTimeAttributes(xDTTMNode, sTimeIn);
        }

        //Sets up the date attributes parsed out of sDateIn for the node xDateNode
        public void XMLSetDateAttributes(XmlNode xDateNode,
                                         string sDateIn)
        {
            string sAttributeDate;
            string sAttributeYear;
            string sAttributeMonth;
            string sAttributeDay;
            DateTime dteInDate = DateTime.Now;
            string sEncChar;

            sEncChar = "";

            //Start SIW139
            if (sDateIn == Constants.BLANK)
                XMLSetDateValue(xDateNode, sDateIn);
            else
            {
            //End SIW139
                if (sDateIn == "" || sDateIn == null)
                    sDateIn = DateTime.Now.Date.ToString();

                if (sDateIn.Substring(0, 2) != "~~")
                {
                    if (sDateIn.Substring(0, 1) == "~")
                    {
                        sEncChar = "~";
                        sDateIn = StringUtils.Mid(sDateIn, 2);
                    }

                    if (DateTimeUtils.IsDate(sDateIn))
                    {
                        DateTime.TryParse(sDateIn, out dteInDate);
                        sAttributeYear = dteInDate.ToString("yyyy");
                        //SI06264 sAttributeMonth = dteInDate.ToString("%M");
                        //SI06264 sAttributeDay = dteInDate.ToString("%d");
                        sAttributeMonth = dteInDate.ToString("MM"); //SI06264
                        sAttributeDay = dteInDate.ToString("dd");   //SI06264
                    }
                    else
                    {
                        sAttributeDate = sDateIn.PadRight(sDateIn.Length + 12);
                        sAttributeYear = sAttributeDate.Substring(0, 4);
                        sAttributeMonth = sAttributeDate.Substring(4, 2);
                        sAttributeDay = sAttributeDate.Substring(6, 2);
                    }

                    XMLSetAttributeValue(xDateNode, Constants.sDateYear, sEncChar + sAttributeYear);
                    XMLSetAttributeValue(xDateNode, Constants.sDateMonth, sEncChar + sAttributeMonth);
                    XMLSetAttributeValue(xDateNode, Constants.sDateDay, sEncChar + sAttributeDay);
                }

                if (GlobalVariables.m_formatDateSW)
                    XMLSetDateValue(xDateNode, sEncChar + dteInDate.ToString());
                //Start SIW139
                else
                    XMLSetDateValue(xDateNode, "");
            }
            //End SIW139
        }

        public string XMLSetDTTMValue(XmlNode xNode, string sDateValue)
        {
            if (xNode != null)
                CurrNode = xNode;

            XmlData = (string)Conversion.ConvertData(sDateValue, Conversion.cmnDataTypes.cdtDTTM);
            return XmlData;
        }

        //Sets the data to be a date value
        public string XMLSetDateValue(XmlNode xNode, 
                                      string sDateValue)
        {
            //SIW529 DateTime dtDate;

            if (xNode != null)
                CurrNode = xNode;

            XmlData = (string)Conversion.ConvertData(sDateValue, Conversion.cmnDataTypes.cdtDate);

            /*if (DateTimeUtils.IsDate(sDateValue))
            {
                DateTime.TryParse(sDateValue, out dtDate);
                XmlData = dtDate.ToString("M/d/yyyy");
            }
            else
                XmlData = sDateValue;*/

            return XmlData;
        }

        public void XMLSetTimeAttributes(XmlNode xTimeNode, string sTimeIn)
        {
            XMLSetTimeAttributes(xTimeNode, sTimeIn, "EST");
        }

        //Sets up the time attributes for the given node
        public void XMLSetTimeAttributes(XmlNode xTimeNode,
                                         string sTimeIn,
                                         string sTimeZone)
        {
            string sAttributeTime;
            string sAttributeHour;
            string sAttributeMinute;
            string sAttributeSecond;
            string sAttributeMSec;
            DateTime dteTimeIn = DateTime.Now;
            string sEncChar;


            sEncChar = "";

            if (sTimeIn == "" || sTimeIn == null)
                sTimeIn = DateTime.Now.ToString();

            //Start SIW139
            if (sTimeIn == Constants.BLANK)
                XMLSetTimeValue(xTimeNode, sTimeIn);
            else
            {
                //End SIW139
                if (sTimeIn.Substring(0, 2) != "~~")
                {
                    if (sTimeIn.Substring(0, 1) == "~")
                    {
                        sEncChar = "~";
                        sTimeIn = StringUtils.Mid(sTimeIn, 2);
                    }
                    if (DateTimeUtils.IsDate(sTimeIn))
                    {
                        DateTime.TryParse(sTimeIn, out dteTimeIn);
                        sAttributeHour = dteTimeIn.TimeOfDay.Hours.ToString();
                        sAttributeMinute = dteTimeIn.ToString("%m");
                        sAttributeSecond = dteTimeIn.ToString("%s");
                        sAttributeMSec = "";

                    }
                    else
                    {
                        sAttributeTime = sTimeIn.PadRight(sTimeIn.Length + 12);
                        sAttributeHour = sAttributeTime.Substring(0, 2);
                        sAttributeMinute = sAttributeTime.Substring(2, 2);
                        sAttributeSecond = sAttributeTime.Substring(4, 2);
                        sAttributeMSec = sAttributeTime.Substring(6, 4);
                    }

                    XMLSetAttributeValue(xTimeNode, Constants.sTimeHour, sEncChar + sAttributeHour);
                    XMLSetAttributeValue(xTimeNode, Constants.sTimeMinute, sEncChar + sAttributeMinute);
                    XMLSetAttributeValue(xTimeNode, Constants.sTimeSecond, sEncChar + sAttributeSecond);
                    XMLSetAttributeValue(xTimeNode, Constants.sTimeTimeZone, sEncChar + sTimeZone);
                }

                if (GlobalVariables.m_formatDateSW)
                    XMLSetTimeValue(xTimeNode, sEncChar + dteTimeIn.ToLongTimeString());
                //Start SIW139
                else
                    XMLSetTimeValue(xTimeNode, "");
            }
            //End SIW139
        }

        //Sets the data to the given time value
        public string XMLSetTimeValue(XmlNode xNode,
                                      string sDateValue)
        {
            //SIW529 DateTime dtDate;

            if (xNode != null)
                CurrNode = xNode;

            XmlData = (string)Conversion.ConvertData(sDateValue, Conversion.cmnDataTypes.cdtTime);

            /*if (DateTimeUtils.IsDate(sDateValue))
            {
                DateTime.TryParse(sDateValue, out dtDate);
                XmlData = dtDate.ToLongTimeString();
            }
            else
                XmlData = sDateValue;*/

            return XmlData;
        }

        //Ensures that the given document is Valid.
        public bool XMLValidateDocument(XmlDocument xDoc)
        {
            string sProcName;

            sProcName = "XML.XMLValidateDocument";
            Debug.PushProc(sProcName);

            if (xDoc != null)
                m_xmlDoc = (XmlDataDocument)xDoc;
            try
            {
                m_xmlDoc.Validate(null);
            }
            catch (XmlSchemaValidationException e)
            {
                string sparseerror = XMLFormatParseError(e);
                Errors.ProcessAppError(ErrorGlobalConstants.ERRC_INVALID_OPERATION, MessageBoxButtons.AbortRetryIgnore, "XML Validation Error:", sparseerror, null);
                Debug.PopProc();
                return false;
            }
            Debug.PopProc();
            return true;

        }

        //Gets/sets the global m_AutoEncrypt variable
        public bool AutoEncrypt
        {
            get{return GlobalVariables.m_AutoEncrypt;}
            set{GlobalVariables.m_AutoEncrypt = value;}
        }

        //gets/sets the XMLBatch object associated with this document
        public XMLBatch Batch
        {
            get
            {
                if (m_xmlBatch == null)
                {
                    m_xmlBatch = new XMLBatch();
                    m_xmlBatch.XML = this; 
                }
                return m_xmlBatch;
            }
            set
            {
                m_xmlBatch = (XMLBatch)value;
                if (value == null)
                {
                    value = Batch;
                }
                m_xmlBatch.XML = this;
            }
        }

        //ToDo: move convertdata and encryptdata over to a Functions file
        /*public object Functions
        {
            get { throw new System.NotImplementedException(); }
        }*/

        //gets/sets the current node
        public XmlNode CurrNode
        {
            get
            {
                if (m_xmlNode == null)
                    m_xmlNode = Document.DocumentElement;

                return m_xmlNode;
            }
            set{m_xmlNode = value;}
        }

        //gets/sets the global variable m_formatDateSW
        public Boolean DateFormatOption
        {
            get{return (bool)GlobalVariables.m_formatDateSW;}
            set{GlobalVariables.m_formatDateSW = value;}
        }

        //gets/sets the current document object
        //SIW529 public override XmlDocument Document
        public XmlDocument Document //SIW529
        {
            get
            {
                if (m_xmlDoc == null)
                    XMLNewDoc("NEWDOCUMENT");

                return m_xmlDoc;
            }
            set
            {
                m_xmlDoc = null;
                m_xmlDoc = value;
                if (m_xmlDoc == null)
                    XMLNewDoc("NEWDOCUMENT");

                m_xmlNode = m_xmlDoc.DocumentElement;
            }
        }

        public override Errors Errors
        {
            get { return Debug.Errors; }
        }

        //Gets/sets the header object associated with this document
        public XMLHeader Header
        {
            get
            {
                if (m_xmlHeader == null)
                {
                    m_xmlHeader = new XMLHeader();
                    m_xmlHeader.XML = this;
                }
                return m_xmlHeader;
            }
            set
            {
                m_xmlHeader = (XMLHeader)value;
                if (value == null)
                    value = Header;

                m_xmlHeader.XML = this;
            }
        }

        //Gets/sets the message object associated with this document
        public XMLMessage Message
        {
            get
            {
                if (m_xmlMessage == null)
                {
                    m_xmlMessage = new XMLMessage();
                    m_xmlMessage.XML = this;
                }
                return m_xmlMessage;
            }
            set
            {
                m_xmlMessage = (XMLMessage)value;
                if (value == null)
                    value = Message;

                m_xmlMessage.XML = this;
            }
        }

        public override XmlNode Node
        {
            get{return Document.DocumentElement;}
        }

        public override XmlNode putNode
        {
            get
            {
                return Node;
            }
        }

        //Gets/sets the global var m_formatDateSW
        public bool TimeFormatOption
        {
            get{return (bool)GlobalVariables.m_formatTimeSW;}
            set{GlobalVariables.m_formatTimeSW = value;}
        }

        //Returns the version of this application
        public string XmlApplicationVersion
        {
            get
            {
                return Application.ProductVersion;
            }
        }

        //Returns the value of the given attribute.
        public string XmlAttribute(string sAttName)
        {
            return XMLGetAttributeValue(null, sAttName);
        }

        //Sets the given attribute name to the given value
        //i.e. <curNodeName sAttName="sNewValue">curNodeData</curNodeName>
        //Note: Functionality has changed from VB version
        public void XmlAttribute(string sAttName, string sNewValue)
        {
            XMLSetAttributeValue(null, sAttName, sNewValue);
        }

        //gets/sets the day attribute in the current node
        public string XmlDay
        {
            get{return XMLGetAttributeValue(null, Constants.sDateDay);}
            set{XMLSetAttributeValue(null, Constants.sDateDay, value.ToString());}
        }

        //returns the name of the current document
        public string XmlDocName
        {
            get
            {
                if (m_xmlDoc != null)
                    return m_xmlDoc.DocumentElement.Name;
                else
                    return "";
            }
        }

        //gets/sets hour attribute
        public string XmlHour
        {
            get{return XMLGetAttributeValue(null, Constants.sTimeHour);}
            set{XMLSetAttributeValue(null, Constants.sTimeHour, value.ToString());}
        }

        //gets/sets minute attribute
        public string XmlMinute
        {
            get{return XMLGetAttributeValue(null, Constants.sTimeMinute);}
            set{XMLSetAttributeValue(null, Constants.sTimeMinute, value.ToString());}
        }

        //gets/sets month attribute
        public string XmlMonth
        {
            get{return XMLGetAttributeValue(null, Constants.sDateMonth);}
            set{XMLSetAttributeValue(null, Constants.sDateMonth, value.ToString());}
        }

        //gets/sets second attribute
        public string XmlSecond
        {
            get{return XMLGetAttributeValue(null, Constants.sTimeSecond);}
            set{XMLSetAttributeValue(null, Constants.sTimeSecond, value.ToString());}
        }

        //gets/sets time zone attribute
        public string XmlTimeZone
        {
            get{return XMLGetAttributeValue(null, Constants.sTimeTimeZone);}
            set{XMLSetAttributeValue(null, Constants.sTimeTimeZone, value.ToString());}
        }

        //gest/sets year attribute
        public string XmlYear
        {
            get{return XMLGetAttributeValue(null, Constants.sDateYear);}
            set{XMLSetAttributeValue(null, Constants.sDateYear, value.ToString());}
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return sXMLNodeOrder;
            }
            set
            {
                sXMLNodeOrder = value;
            }
        }

        public string guid
        {
            get { return m_GUID; }
        }
    
    } //End Class XML

} //End Namespace
