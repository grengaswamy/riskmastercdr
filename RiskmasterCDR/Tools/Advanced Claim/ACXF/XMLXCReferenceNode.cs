/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using System.Xml;
using System.Collections;

namespace CCP.XmlFormatting
{
    public abstract class XMLXCReferenceNode:XMLXCNodeWithList
    {
        //Extended abstract class used in "Reference" nodes

        public virtual void Clear()
        {
            if (NodeList != null)
            {

                IEnumerator xmlNodeListEnum = xmlNodeList.GetEnumerator();
                xmlNodeListEnum.MoveNext();
                if (xmlNodeListEnum.MoveNext())
                    xmlThisNode = (XmlNode)xmlNodeListEnum.Current;
                else
                    xmlThisNode = null;
                while (xmlThisNode != null)
                    Remove();
            }
            xmlNodeList = null;
        }

        public void Remove()
        {
            Parent_Node.RemoveChild(xmlThisNode);
            getNode(false);
        }

        protected abstract void CheckEntry();

        protected void xEntry(string id)
        {
            xEntry("", "", id, 0);
        }

        protected void xEntry(string role, string roleCode, string id, int numchild)
        {
            int temp;
            Int32.TryParse(id, out temp);
            if (xmlThisNode != null)
                if (role == "" && roleCode == "" && temp == 0 && numchild == 0)
                    Remove();
        }
    }
}
