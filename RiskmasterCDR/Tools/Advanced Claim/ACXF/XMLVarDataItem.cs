/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 12/04/2008 | SI06499 |    NDB     | Fix for pulling a Code from a Data Item
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 04/10/2012 | SIW8366 |    JTC     |  Blank dates in Supplemental fields are not getting saved.
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;
using CCP.Constants;
using System.Collections;

namespace CCP.XmlFormatting
{
    public class XMLVarDataItem: XMLXCNodeWithListBase  //SIW529
    {
        /*'<PARENT_NODE>
        '   <DATA_ITEM
        '      Name = "Data Item Name"
        '      TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '         [data attributes corresponding to data types above as follows:
        '           BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '           DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '           TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '           CODE:  CODE="shortcode"]>
        '         [Data corresponding to the data type as follows:
        '         STRING:  data value {string}
        '         NUMBER:  data value {decimal}
        '         DATE:  formatted Date
        '         TIME:  formatted TIME
        '   </DATA_ITEM>
        '</PARENT_NODE>*/

        //initialize
        //SIW529 public XMLVarDataItem() : this(true) {}

        //SIW529 public XMLVarDataItem(bool bUseSharedXML)
        public XMLVarDataItem() //SIW529
        {
            //SIW529 UseSharedXMLDocument = bUseSharedXML;
            //SIW529 XMLUtils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;
        }

        //Start SIW529
        public override XMLUtils Utils
        {
            get
            {
                if (m_xmlUtils == null)
                    m_xmlUtils = new XMLUtils();
                return m_xmlUtils;
            }
        }

        public override XMLUtils XMLUtils
        {
            get
            {
                return Utils;
            }
        }
        //End SIW529

        //resets the parent variables
        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
            }
        }

        //returns this node, ensuring that it is in the document under the parent node first
        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    //SIW529 Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
                    xmlThisNode = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);  //SIW529
                return Node;
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                //SIW485 return XMLGlobals.sNodeOrder;
                return Utils.sNodeOrder;	//SIW485
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNodeList DataItemList
        {
            get
            {
                return NodeList;
            }
        }

        public XmlNode getDataItem(bool reset)
        {
            return getNode(reset);
        }

        public void removeDataItem()
        {
            if (Node != null)
                Parent_Node.RemoveChild(Node);
            getDataItem(true);
        }

        public XmlNode addDataItem(string sName, string sData, string sCode, cxmlDataType cDataType)
        {
            Node = null;
            if (sName != "" && sName != null)
                findDataItembyName(sName);
            if (sName != "" && sName != null)
                Name = sName;
            if ((sData != "" && sData != null) || (sCode != "" && sCode != null))
                putDataItemValue(sData, sCode, cDataType);
            return Node;
        }

        public string Name
        {
            get
            {
                //SIW529 return XMLUtils.getAttribute(Node, "", Constants.sVarDataName);
                return XMLUtils.getAttribute(Node, "", Constants.sVarDataName); //SIW529
            }
            set
            {
                //SIW529 XMLUtils.putAttribute(putNode, "", Constants.sVarDataName, value, NodeOrder);
                XMLUtils.putAttribute(putNode, "", Constants.sVarDataName, value, NodeOrder);   //SIW529
            }
        }

        public void putDataItemValue(string sData, string sCode, cxmlDataType cDataType)
        {
            //Start SIW8366
            if (XML.Blanking)
            {
                if ((sData == "" || sData == null) && (sCode == "" || sCode == null))
                {
                    sData = Constants.BLANK;
                    sCode = Constants.BLANK;
                }
            }
            //End SIW8366
            XML.XMLInsertVariable_Node(putNode, (cxmlDataType)cDataType, sData, sCode);
        }

        public cxmlDataType getDataItemValue(out string sValue, out string sCode)
        {
            if (Node != null)
            {    //Start SIW8366
                //return (cxmlDataType)XML.XMLExtractVariableDataType(Node, out sValue, out sCode);
                cxmlDataType retval = (cxmlDataType)XML.XMLExtractVariableDataType(Node, out sValue, out sCode);
                XMLUtils.TranslateString(ref sValue);
                XMLUtils.TranslateString(ref sCode);
                return retval;
            }   //End SIW8366
            else
            {
                sValue = "";
                sCode = "";
                return cxmlDataType.dtUndefined;
            }
        }

        public XmlNode findDataItembyName(string sName)
        {
            if (Name != sName)
            {
                getFirst();
                while (Node != null)
                {
                    if (Name == sName)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public string DataItemValue(string sName)
        {
            string sV, sC;
            findDataItembyName(sName);
            getDataItemValue(out sV, out sC);
            return sV;
        }

        public string DataItemCode(string sName)
        {
            string sV, sC;
            findDataItembyName(sName);
            getDataItemValue(out sV, out sC);
            return sC; // SI06499
        }

        protected override string DefaultNodeName
        {
            get {return Constants.sVarDataItem;}
        }
    }
}
