/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;

namespace CCP.XmlFormatting
{
    public abstract class XMLXFNode:XMLACNodeBase
    {
        //provides common functionality to XMLBatch, XMLHeader and XMLMessage

        protected XML xf;
        protected XmlDocument xmlDocument;
        protected XmlNode xmlNewNode;

        //access to the XML Functions object
        public abstract XML XML
        {
            get;
            set;
        }

        //access to the current document
        //SIW529 public override XmlDocument Document
        public virtual XmlDocument Document //SIW529
        {
            get
            {
                return XML.Document;
            }
            protected set
            {
                XML.Document = value;
            }
        }

        //sets the current document to the given document and clears the message
        public XmlNode Load(XmlDocument doc)
        {
            if (doc != null)
                Document = doc;

            xmlThisNode = null;
            return Node;
        }

        //inserts this message into the current document
        public virtual void InsertInDocument(XmlDocument doc, XmlNode nde)
        {
            if (doc == null)
                xmlDocument = Document;
            else
                xmlDocument = doc;
            if (nde == null)
                xmlNewNode = Node;
            else
                xmlNewNode = nde;
        }

        //access to the Errors object
        public override Errors Errors
        {
            get { return XML.Errors; }
        }
    }
}
