/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Constants;

namespace CCP.XmlFormatting
{
    public static class GlobalVariables
    {
        //Start SIW485
        //public static Dictionary<pmType, string> sPMTypes;
        //public static Dictionary<pmRespType, string> sPMRTypes;
        //End SIW485

        public static bool m_formatDateSW;
        public static bool m_formatTimeSW;
        public static bool m_AutoEncrypt;
        public static bool m_Instantiated;
    }
}
