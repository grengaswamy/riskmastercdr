/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;

namespace CCP.XmlFormatting
{
    public abstract class XMLACNodeBase
    {
        //The top-level abstract class for all nodes used in AC with functionality common in all nodes

        protected XmlNode xmlThisNode;

        //Start SIW529
        //access to the current document
        //public abstract XmlDocument Document
        //{
        //    get;
        //    set;
        //}
        //End SIW529

        public virtual XmlNode Node
        {
            get
            {
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
            }
        }

        public abstract Errors Errors
        {
            get;
        }
    }
}
