/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;

namespace CCP.XmlFormatting
{
    public abstract class XMLACNode:XMLACNodeBase
    {
        //Abstract class sitting above XML and all nodes in XmlComponents and XmlSupport providing common functionality

        protected XMLACNode m_Parent;
        protected ArrayList sThisNodeOrder;

        public virtual XmlNode putNode
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public virtual ArrayList NodeOrder
        {
            get { return null; }
            set { }
        }

        public virtual XMLACNode Parent
        {
            get
            {
                return null;
            }
            set
            {
                m_Parent = value;
            }
        }
    }
}
