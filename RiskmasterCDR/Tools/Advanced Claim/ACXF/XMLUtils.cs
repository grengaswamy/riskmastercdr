/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/06/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 11/07/2008 | SI06468 |    ASM     | Code IDs
 * 01/27/2009 | SIW139  |    JTC     | Field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 11/12/2009 | SIW211  |    AS      | Use InnerText instead of InnerXml to handle special characters like '&'                                   
 * 05/12/2010 | SIW321  |   AP       | Event location not saving on Event page.
 * 05/17/2010 | SIW360  |   AP       | Techkey position reodered in .NET XML components similar to ordering in CCPXC components.
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/24/2011 | SIW529  |    AS      | General fix
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.XmlFormatting;
using System.Collections;
using System.Xml;
using CCP.Common;

namespace CCP.XmlFormatting
{
    public class XMLUtils
    {
        public XMLControls gControl;	//SIW485
        public ArrayList sDocNodeOrder;	//SIW485
        public ArrayList sNodeOrder;	//SIW485

        //SIW529 public void subInitializeGlobals(XML xmlLocal)	//SIW485 Removed Static
        public XMLUtils()   //SIW529 Turned into constructor, as it's an operation that needs to occur for every object that uses this.
        {
            if (gControl == null)	//SIW485 Removed XMLGlobals
            {
                gControl = new XMLControls();	//SIW485 Removed XMLGlobals
            }
            if (sDocNodeOrder == null)	//SIW485 Removed XMLGlobals
            {
                sDocNodeOrder = new ArrayList();	//SIW485 Removed XMLGlobals
            }
            if (sNodeOrder == null)	//SIW485 Removed XMLGlobals
            {
                sNodeOrder = new ArrayList(1);	//SIW485 Removed XMLGlobals
                sNodeOrder.Add("");	//SIW485 Removed XMLGlobals
            }
            subSetupDocumentNodeOrder();            
            //SIW529 if (null == xmlLocal)
            //SIW529 {
            gControl.XML.NodeOrder = sDocNodeOrder;	//SIW485 Removed XMLGlobals
            //SIW529 }
            //SIW529 else
            //SIW529 {
            //SIW529     xmlLocal.NodeOrder = sDocNodeOrder;	//SIW485 Removed XMLGlobals
            //SIW529 }            
        }

        // public static XML XML {...} // SIW183

        //Start SIW529
        public XML XML
        {
            get
            {
                return gControl.XML;
            }
            set
            {
                gControl.XML = value;
            }
        }
        //End SIW529
        
        public Debug Debug	//SIW485 Removed Static
        {
            get
            {
                return gControl.Debug;	//SIW485 Removed XMLGlobals
            }
        }

        public void subSetupDocumentNodeOrder()	//SIW485 Removed Static
        {
            //if (XMLGlobals.sDocNodeOrder.Count < 13)          //SI06333
            if (sDocNodeOrder.Count < 16)            //SI06333	//SIW485 Removed XMLGlobals
            {
                //XMLGlobals.sDocNodeOrder.Capacity = 14;       //SI06333
                sDocNodeOrder.Capacity = 17;         //SI06333	//SIW485 Removed XMLGlobals
                setUpArray(sDocNodeOrder);	//SIW485 Removed XMLGlobals
            }
            if ((string)sDocNodeOrder[0] == "" || sDocNodeOrder[0] == null)	//SIW485 Removed XMLGlobals
            {
				//Start SIW485 Removed XMLGlobals
                sDocNodeOrder[0] = Constants.sHdrElement;
                sDocNodeOrder[1] = Constants.sPM;
                sDocNodeOrder[2] = Constants.sBatchElement;
                sDocNodeOrder[3] = Constants.sEvtNode;
                sDocNodeOrder[4] = Constants.sDiaCollection;
                sDocNodeOrder[5] = Constants.sClmNode;
                sDocNodeOrder[6] = Constants.sVCHGroup;
                sDocNodeOrder[7] = Constants.sAddNode;
                sDocNodeOrder[8] = Constants.sOrgHierNode;
                sDocNodeOrder[9] = Constants.sEntCollection;
                sDocNodeOrder[10] = Constants.sAdrCollection;
                sDocNodeOrder[11] = Constants.sCmtCollection;
                sDocNodeOrder[12] = Constants.sCDGlossary;
                sDocNodeOrder[13] = Constants.sBLNode;
                sDocNodeOrder[14] = Constants.sSrchNode;             //SI06333
                sDocNodeOrder[15] = Constants.sSrchFields;           //SI06333
                sDocNodeOrder[16] = Constants.sSrchDataRows;         //SI06333
				//End SIW485 Removed XMLGLobals
            }
        }

        private void setUpArray(ArrayList arr)	//SIW485 Removed Static
        {
            for (int i = arr.Count; i < arr.Capacity; i++)
            {
                arr.Add(null);
            }
        }

        //SIW529 public XmlNode putAttribute(XML xmlLocal, XmlNode xmlPNode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putAttribute(XmlNode xmlPNode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder)	//SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if(XML.Blanking)
                if (sCode == "" || sCode == null)
                    sCode = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529
                    XML.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);    //SIW529
                }
            }
            else
            {
                if (sCode != "" && sCode != null)
                    //SIW529 xmlLocal.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);
                    XML.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);    //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, AttribName);
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);   //SIW529
            }
            return xmlNode;
        }

        //SIW529 public string getAttribute(XML xmlLocal, XmlNode pnode, string NodeName, string attname)	//SIW485 Removed Static
        public string getAttribute(XmlNode pnode, string NodeName, string attname)
        {
            string strdata;
            XmlNode xmlNode;
            strdata = "";
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLGetAttributeValue(xmlNode, attname);
                    strdata = XML.XMLGetAttributeValue(xmlNode, attname);   //SIW529
            }
            TranslateString(ref strdata);   //SIW139
            return strdata;
        }

        //SIW529 public XmlNode getNode(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public XmlNode getNode(XmlNode pnode, string NodeName)  //SIW529
        {
            XmlNode xmlENde = null;
            if (pnode != null)
                if (NodeName != "" && NodeName != null)
                    //SIW529 xmlENde = xmlLocal.XMLGetNode(pnode, NodeName);
                    xmlENde = XML.XMLGetNode(pnode, NodeName);  //SIW529
                else
                {
                    xmlENde = pnode;
                    //SIW529 xmlLocal.CurrNode = pnode;
                    XML.CurrNode = pnode;   //SIW529
                }
            return xmlENde;
        }

        //SIW529 public string getData(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getData(XmlNode pnode, string NodeName)   //SIW529
        {
            //Start SIW139
            //return xmlLocal.XMLExtractElement(xmlLocal, pnode, NodeName);
            string strdata = "";//SIW529
            //SIW529 strdata = xmlLocal.XMLExtractElement(pnode, NodeName);
            if (pnode != null)//SIW529
                strdata = XML.XMLExtractElement(pnode, NodeName);   //SIW529
            TranslateString(ref strdata);
            return strdata;
            //End SIW139
        }

        //SIW529 public string getDate(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getDate(XmlNode pnode, string NodeName)
        {
            XmlNode xmlNode;
            string strdata = "";
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                //SIW529 strdata = xmlLocal.XMLExtractDate_Date(xmlNode);
                xmlNode = getNode(pnode, NodeName); //SIW529
                strdata = XML.XMLExtractDate_Date(xmlNode); //SIW529
                //Start SIW139
                if (strdata == "")
                    //SIW529 strdata = xmlLocal.XMLExtractElement(xmlNode, "");
                    strdata = XML.XMLExtractElement(xmlNode, "");   //SIW529
                //End SIW139
            }
            TranslateString(ref strdata);   //SIW139
            return strdata;
        }

        //SIW529 public XmlNode putData(XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putData(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
                if (sNewValue == "" || sNewValue == null)
                    sNewValue = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                    //SIW529 xmlNode = xmlLocal.XMLputData(xmlPNode, arrnodeorder, NodeName, sNewValue);
                    xmlNode = XML.XMLputData(xmlPNode, arrnodeorder, NodeName, sNewValue);  //SIW529

            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    xmlNode.InnerText = sNewValue;//SIW211
                    //xmlNode.InnerXml = sNewValue;//SIW211
            }
            return xmlNode;
        }

        //SIW529 public XmlNode putTypeItem(XML xmlLocal, XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putTypeItem(XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder)  //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null))
                {
                    sDesc = Constants.BLANK;
                    sCode = Constants.BLANK;
                }
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if ((sDesc != "" && sDesc != null) || (sCode != "" && sCode != null))
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLInsertType_Node(xmlNode, sDesc, sCode);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529
                    XML.XMLInsertType_Node(xmlNode, sDesc, sCode);  //SIW529
                }
            }
            else
            {
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null))
                {
                    if (NodeName != null && NodeName != "")
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                {
                    //SIW529 xmlLocal.XMLInsertType_Node(xmlNode, sDesc, sCode);
                    XML.XMLInsertType_Node(xmlNode, sDesc, sCode);  //SIW529
                }
            }
            return xmlNode;
        }

        //SIW529 public string getType(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getType(XmlNode pnode, string NodeName)   //SIW529
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractTypeAttribute(xmlNode);
                    strdata = XML.XMLExtractTypeAttribute(xmlNode); //SIW529
            }
            TranslateString(ref strdata);   //SIW139
            return strdata;
        }

        //SIW529 public XmlNode putType(XML xmlLocal, XmlNode pnode, string NodeName, string sType, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putType(XmlNode pnode, string NodeName, string sType, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
                if (sType == "" || sType == null)
                    sType = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sType != "" && sType != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);    //SIW529
                }
            }
            else
            {
                if (sType != "" && sType != null)
                    //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);    //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, Constants.sdtType);
                    XML.XMLRemoveAttribute_Node(xmlNode, Constants.sdtType);    //SIW529
            }
            return xmlNode;
        }

        // Start SIW360
        //SIW529 public string getEntityRef(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getEntityRef(XmlNode pnode, string NodeName)
        { 
            //SIW529 return getEntityRef(xmlLocal, pnode, NodeName, "ID");
            return getEntityRef(pnode, NodeName, "ID"); //SIW529
        }
        //SIW529 public string getEntityRef(XML xmlLocal, XmlNode pnode, string NodeName, string AttribName)	//SIW485 Removed Static
        public string getEntityRef(XmlNode pnode, string NodeName, string AttribName)   //SIW529
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractEntityAttribute(xmlNode, AttribName);
                    strdata = XML.XMLExtractEntityAttribute(xmlNode, AttribName);   //SIW529
            }
            return strdata;
        }
        //SIW529 public XmlNode putEntityRef(XML xmlLocal, XmlNode pnode, string NodeName, string sEID, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putEntityRef(XmlNode pnode, string NodeName, string sEID, ArrayList arrnodeorder)    //SIW529
        {
            //SIW529 return putEntityRef(xmlLocal, pnode, NodeName, sEID, arrnodeorder, "ID");
            return putEntityRef(pnode, NodeName, sEID, arrnodeorder, "ID"); //SIW529
        }
        //SIW529 public XmlNode putEntityRef(XML xmlLocal, XmlNode pnode, string NodeName, string sEID, ArrayList arrnodeorder, string AttribName)	//SIW485 Removed Static
        public XmlNode putEntityRef(XmlNode pnode, string NodeName, string sEID, ArrayList arrnodeorder, string AttribName) //SIW529
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sEID != "" && sEID != null && sEID != "0")
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLInsertEntity_Node(xmlNode, sEID, AttribName);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529
                    XML.XMLInsertEntity_Node(xmlNode, sEID, AttribName);    //SIW529
                }
            }
            else
            {
                if (sEID != "" && sEID != null && sEID != "0")
                    //SIW529 xmlLocal.XMLInsertEntity_Node(xmlNode, sEID, AttribName);
                    XML.XMLInsertEntity_Node(xmlNode, sEID, AttribName);    //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, AttribName);
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);   //SIW529
            }
            return xmlNode;
        }
        // End SIW360
        //SIW529 public XmlNode putDate(XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putDate(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)
                if (sNewValue == "" || sNewValue == null)
                    sNewValue = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLElementAsDate_Node(NodeName, sNewValue);
                    //SIW529 xmlLocal.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);
                    xmlNode = XML.XMLElementAsDate_Node(NodeName, sNewValue);
                    XML.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);
                }
            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    //SIW529 xmlLocal.XMLSetDateAttributes(xmlNode, sNewValue);
                    XML.XMLSetDateAttributes(xmlNode, sNewValue);   //SIW529
            }
            return xmlNode;
        }

        //SIW529 public string getTime(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getTime(XmlNode pnode, string NodeName)   //SIW529
        {
            XmlNode xmlNode;
            string strdata = "";
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractTime_Time(xmlNode);
                    strdata = XML.XMLExtractTime_Time(xmlNode);
                //Start SIW139
                if (strdata == "")
                    //SIW529 strdata = xmlLocal.XMLExtractElement(xmlNode, "");
                    strdata = XML.XMLExtractElement(xmlNode, "");   //SIW529
                //End SIW139
            }
            TranslateString(ref strdata);   //SIW139
            return strdata;
        }

        //SIW529 public XmlNode putTime(XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putTime(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
                if (sNewValue == "" || sNewValue == null)
                    sNewValue = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLElementAsTime_Node(NodeName, sNewValue);
                    //SIW529 xmlLocal.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);
                    xmlNode = XML.XMLElementAsTime_Node(NodeName, sNewValue);   //SIW529
                    XML.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);  //SIW529
                }
            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    //SIW529 xmlLocal.XMLSetTimeAttributes(xmlNode, sNewValue);
                    XML.XMLSetTimeAttributes(xmlNode, sNewValue);   //SIW529
            }
            return xmlNode;
        }
        //Start SI06468
        //SIW529 public XmlNode putCodeItem(XML xmlLocal, XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder) 	//SIW485 Removed Static
        public XmlNode putCodeItem(XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder)  //SIW529
        {
        return putCodeItem(pnode,NodeName,sDesc,sCode,arrnodeorder,""); //SIW529
        }
        //End SI06468
        //public static XmlNode putCodeItem(XML xmlLocal, XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder) //SI06468
        //SIW529 public XmlNode putCodeItem(XML xmlLocal, XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder, string sId) //SI06468	//SIW485 Removed Static
        public XmlNode putCodeItem(XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder, string sId)  //SIW529 
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null) && (sId == "" || sId == null))
                {
                    sId = "0";
                    sDesc = Constants.BLANK;
                    sCode = Constants.BLANK;
                }
            //End SIW139

            //SIW139 sId = ""; //SI06468
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529 
            if (xmlNode == null)
            {
                if ((sDesc != "" && sDesc != null) || (sCode != "" && sCode != null) || (sId != "" && sId != null)) //SIW139
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //xmlLocal.XMLInsertCode_Node(xmlNode, sDesc, sCode);//SI06468
                    //SIW529 xmlLocal.XMLInsertCode_Node(xmlNode, sDesc, sCode, sId);//SI06468
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529 
                    XML.XMLInsertCode_Node(xmlNode, sDesc, sCode, sId); //SIW529
                }
            }
            else
            {
                //if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null))//SI06468
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null) && (sId == "" || sId == null))//SI06468
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    //xmlLocal.XMLInsertCode_Node(xmlNode, sDesc, sCode);//SI06468
                    //SIW529 xmlLocal.XMLInsertCode_Node(xmlNode, sDesc, sCode, sId);//SI06468
                    XML.XMLInsertCode_Node(xmlNode, sDesc, sCode, sId); //SIW529
            }
            return xmlNode;
        }

        //SIW529 public string getCode(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getCode(XmlNode pnode, string NodeName)   //SIW529
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractCodeAttribute(xmlNode);
                    strdata = XML.XMLExtractCodeAttribute(xmlNode); //SIW529
            }
            TranslateString(ref strdata);   //SIW139
            return strdata;
        }
       
        //Start SI06468
        //SIW529 public string getCodeID(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getCodeID(XmlNode pnode, string NodeName) //SIW529
        {
            //Start SIW321
            //string strdata;
            //XmlNode xmlNode;
            //strdata = "";
            //if (pnode != null)
            //{
            //    xmlNode = getNode(xmlLocal, pnode, NodeName);
            //    if (xmlNode != null)
            //        strdata = xmlLocal.XMLExtractCodeIDAttribute(xmlNode);
            //}
            ////Start SIW139
            //if (strdata != "")
            //{
            //    strdata = strdata.Replace(Constants.BLANK, "0");
            //    strdata = strdata.Replace("[BLANK]", "0");
            //}
            ////End SIW139
            //return strdata;
            //SIW529 return getCodeID(xmlLocal, pnode, NodeName, "");
            return getCodeID(pnode, NodeName, "");  //SIW529
            //Ends SIW321
        }
        //Start SIW321
        //SIW529 public string getCodeID(XML xmlLocal, XmlNode pnode, string NodeName, string AttribName)	//SIW485 Removed Static
        public string getCodeID(XmlNode pnode, string NodeName, string AttribName)  //SIW529
        {
            string strdata;
            XmlNode xmlNode;
            strdata = "";
            AttribName = "Intel";
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractCodeIDAttribute(xmlNode, AttribName);
                    strdata = XML.XMLExtractCodeIDAttribute(xmlNode, AttribName);   //SIW529
            }
            //Start SIW139
            if (strdata != "")
            {
                strdata = strdata.Replace(Constants.BLANK, "0");
                strdata = strdata.Replace("[BLANK]", "0");
            }
            //End SIW139
            return strdata;
        }
        //End SIW321

        //SIW529 public XmlNode putCode(XML xmlLocal, XmlNode pnode, string NodeName, string sCode, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putCode(XmlNode pnode, string NodeName, string sCode, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)
                if (sCode == "" || sCode == null)
                    sCode = Constants.BLANK;
            //End SIW139

            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529 
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, Constants.sdtCode, sCode);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529 
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtCode, sCode);    //SIW529
                }
            }
            else
            {
                if (sCode != "" && sCode != null)
                    //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, Constants.sdtCode, sCode);
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtCode, sCode);    //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, Constants.sdtCode);
                    XML.XMLRemoveAttribute_Node(xmlNode, Constants.sdtCode);    //SIW529
            }
            return xmlNode;
        }
        //Start SI06468
        //SIW529 public XmlNode putCodeID(XML xmlLocal, XmlNode pNode, string NodeName, string sId, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putCodeID(XmlNode pNode, string NodeName, string sId, ArrayList arrnodeorder)    //SIW529
        {
            //Start SIW321
            ////Start SIW139
            //if (xmlLocal.Blanking)
            //    if (sId == "" || sId == null)
            //        sId = "0";
            ////End SIW139

            //XmlNode xmlNode;
            //XmlNode xmlPNode;
            //xmlPNode = pNode;
            //xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            ////SIW139 if (sId == "<B>")
            ////SIW139    sId = "";
            //if (xmlNode == null)
            //    if (sId != "")
            //    {
            //        xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
            //        xmlLocal.XMLSetAttributeValue(xmlNode, "ID", sId);
            //    }
            //    else
            //    {
            //        if (sId != "")
            //            xmlLocal.XMLSetAttributeValue(xmlNode, "ID", sId);
            //        else
            //            xmlLocal.XMLRemoveAttribute_Node(xmlNode, "ID");
            //    }
            //return xmlNode;
            //SIW529 return putCodeID(xmlLocal, pNode, NodeName, sId, arrnodeorder, "");
            return putCodeID(pNode, NodeName, sId, arrnodeorder, "");   //SIW529
            //End SIW321
        }
        //Start SIW321
        //SIW529 public XmlNode putCodeID(XML xmlLocal, XmlNode pNode, string NodeName, string sId, ArrayList arrnodeorder, string AttribName)	//SIW485 Removed Static
        public XmlNode putCodeID(XmlNode pNode, string NodeName, string sId, ArrayList arrnodeorder, string AttribName) //SIW529
        {
            //Start SIW139
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529 
                if (sId == "" || sId == null)
                    sId = "0";
            //End SIW139
            if (AttribName.Trim() == "") { AttribName = "ID"; }
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pNode;
            //SIW529 xmlNode = getNode(xmlLocal, xmlPNode, NodeName);
            xmlNode = getNode(xmlPNode, NodeName);  //SIW529
            //SIW139 if (sId == "<B>")
            //SIW139    sId = "";
            if (xmlNode == null)
                if (sId != "")
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, AttribName, sId);
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder); //SIW529 
                    XML.XMLSetAttributeValue(xmlNode, AttribName, sId); //SIW529
                }
                else
                {
                    if (sId != "")
                        //SIW529 xmlLocal.XMLSetAttributeValue(xmlNode, AttribName, sId);
                        XML.XMLSetAttributeValue(xmlNode, AttribName, sId); //SIW529
                    else
                        //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, AttribName);
                        XML.XMLRemoveAttribute_Node(xmlNode, AttribName);   //SIW529
                }
            return xmlNode;
        }
        //End SIW321
        //SIW529 public string getAddressRef(XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed Static
        public string getAddressRef(XmlNode pnode, string NodeName) //SIW529 
        {
            //Start SIW360
            //string strdata = "";
            //XmlNode xmlNode;
            //if (pnode != null)
            //{
            //    xmlNode = getNode(xmlLocal, pnode, NodeName);
            //    if (xmlNode != null)
            //        strdata = xmlLocal.XMLExtractAddressAttribute(xmlNode, null);
            //}
            //return strdata;
            //SIW529 return getAddressRef(xmlLocal, pnode, NodeName, "");
            return getAddressRef(pnode, NodeName, "");  //SIW529
            //End SIW360
        }
        //Start SIW360
        //SIW529 public string getAddressRef(XML xmlLocal, XmlNode pnode, string NodeName, string AttribName)	//SIW485 Removed Static
        public string getAddressRef(XmlNode pnode, string NodeName, string AttribName)  //SIW529
        {
            string strdata = "";
            XmlNode xmlNode;
            if (AttribName.Trim()==""){AttribName=null;}
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractAddressAttribute(xmlNode, AttribName);
                    strdata = XML.XMLExtractAddressAttribute(xmlNode, AttribName);  //SIW529
            }
            return strdata;
        }
        //SIW529 public XmlNode putAddressRef(XML xmlLocal, XmlNode pnode, string NodeName, string sID, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putAddressRef(XmlNode pnode, string NodeName, string sID, ArrayList arrnodeorder)    //SIW529
        {
            //SIW529 return putAddressRef(xmlLocal, pnode, NodeName, sID, arrnodeorder,"ID");
            return putAddressRef(pnode, NodeName, sID, arrnodeorder, "ID"); //SIW529
        }
        //SIW529 public XmlNode putAddressRef(XML xmlLocal, XmlNode pnode, string NodeName, string sID, ArrayList arrnodeorder, string AttribName)	//SIW485 Removed Static
        public XmlNode putAddressRef(XmlNode pnode, string NodeName, string sID, ArrayList arrnodeorder, string AttribName) //SIW529
        {
            XmlNode xmlNode;
            //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
            xmlNode = getNode(pnode, NodeName); //SIW529
            if (xmlNode == null)
            {
                if (sID != "" && sID != null && sID != "0")
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(pnode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLInsertAddress_Node(xmlNode, sID, AttribName);
                    xmlNode = XML.XMLaddNode(pnode, NodeName, arrnodeorder);    //SIW529
                    XML.XMLInsertAddress_Node(xmlNode, sID, AttribName);    //SIW529
                }
            }
            else
                if (sID != "" && sID != null && sID != "0")
                    //SIW529 xmlLocal.XMLInsertAddress_Node(xmlNode, sID, AttribName);
                    XML.XMLInsertAddress_Node(xmlNode, sID, AttribName);    //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, AttribName);
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);   //SIW529
            return xmlNode;
        }
        // End SIW360
        //SIW529 public string getBool(XML xmlLocal, XmlNode pnode, string NodeName, string AttribName)	//SIW485 Removed Static
        public string getBool(XmlNode pnode, string NodeName, string AttribName)    //SIW529
        {
            string strdata;
            XmlNode xmlNode;
            strdata = "";
            if (pnode != null)
            {
                //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
                xmlNode = getNode(pnode, NodeName); //SIW529
                if (xmlNode != null)
                    //SIW529 strdata = xmlLocal.XMLExtractBooleanAttribute(xmlNode, AttribName);
                    strdata = XML.XMLExtractBooleanAttribute(xmlNode, AttribName);  //SIW529
            }
            return strdata;
        }

        //SIW529 public XmlNode putBool(XML xmlLocal, XmlNode pnode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder)	//SIW485 Removed Static
        public XmlNode putBool(XmlNode pnode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder) //SIW529
        {
            XmlNode xmlNode;
            //SIW529 xmlNode = getNode(xmlLocal, pnode, NodeName);
            xmlNode = getNode(pnode, NodeName); //SIW529
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    //SIW529 xmlNode = xmlLocal.XMLaddNode(pnode, NodeName, arrnodeorder);
                    //SIW529 xmlLocal.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);
                    xmlNode = XML.XMLaddNode(pnode, NodeName, arrnodeorder);    //SIW529
                    XML.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);  //SIW529
                }
            }
            else
            {
                if (sCode != "" && sCode != null)
                    //SIW529 xmlLocal.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);
                    XML.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);  //SIW529
                else
                    //SIW529 xmlLocal.XMLRemoveAttribute_Node(xmlNode, AttribName);
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);   //SIW529
            }
            return xmlNode;
        }

        //Start SIW139
        public void TranslateString(ref string sValue)	//SIW485 Removed Static
        {
            if (sValue != "" && sValue != null)
            {
                sValue = sValue.Replace(Constants.BLANK, " ");
                sValue = sValue.Replace("[BLANK]", " ");
            }
        }
        //End SIW139
    }
}
