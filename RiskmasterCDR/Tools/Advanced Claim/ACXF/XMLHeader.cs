/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/22/2009 | SIW180  |   JTC      | Prevent saving of stale data
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 02/27/2012 | SIW8194 |   rdorsey5 | Added SSO code.
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using System.Windows.Forms;

namespace CCP.XmlFormatting
{
    /*****************************************************************************
    ' The XMLHeader class provides the functionality to support and manage an
    ' Advanced Claims XML Document Header
    '*****************************************************************************
    ' <HEADER DOCVERSION="2002.1"
    '         SOURCEAPP="vAppName"
    '         SOURCEDB="vUserDSN"
    '         TARGETDB="vTargetDatabase"
    '         APPVERSION="vAppVersion"
    '         USERID="vUserId"
    '         PASSWORD="vPassword" 
    '         UseSSO="vUseSSO">
    '         SSOCert="vSSOCert">
    '   <DOC_NAME>vDocName</DOC_NAME>
    '   <RqUID>uuid</RqUID>
    '   <DESCRIPTION>vAppDescription</DESCRIPTION>
    '   <DATE_CREATED YEAR="CurrentYear"
    '                 MONTH="CurrentMonth"
    '                 DAY="CurrentDay"/>
    '   <TIME_CREATED TIME_ZONE="CurrentTimeZone"
    '                 HOUR="CurrentHour"
    '                 MINUTE="CurrentMinute"
    '                 SECOND="CurrentSecond"/>
    'Start SIW180
    '   <DATE_EXTRACTED YEAR="CurrentYear"
    '                   MONTH="CurrentMonth"
    '                   DAY="CurrentDay"/>
    '   <TIME_EXTRACTED TIME_ZONE="CurrentTimeZone"
    '                   HOUR="CurrentHour"
    '                   MINUTE="CurrentMinute"
    '                   SECOND="CurrentSecond"/>
    'End SIW180
    ' </HEADER>
    '*****************************************************************************/
    public class XMLHeader:XMLXFNode
    {

        public XMLHeader()
        {
            xmlThisNode = null;
            xf = null;
        }

        //gets/sets the APPVERSION attribute
        public string AppVersion
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrAppVersion);

                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (nval == "" || nval == null)
                    nval = Application.ProductVersion;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrAppVersion, nval);
            }
        }

        //gets/sets the DATE_CREATED node
        public string CreateDate
        {
            get
            {
                XmlNode xmlHdrNode;
                XmlNode xmlCrDtNode;
                string sDate;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                {
                    xmlCrDtNode = xmlHdrNode.SelectSingleNode(Constants.sHdrDate);
                    sDate = XML.XMLExtractDate_Date(xmlCrDtNode);
                }
                else
                    sDate = "";
                return sDate;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlNode xmlDateNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlDateNode = xmlHdrNode.SelectSingleNode(Constants.sHdrDate);
                else
                    xmlDateNode = null;

                if (xmlDateNode == null)
                {
                    xmlDateNode = XML.XMLElementAsDate_Node(Constants.sHdrDate, value);
                    xmlHdrNode.AppendChild(xmlDateNode);
                }
                else
                    XML.XMLSetDateAttributes(xmlDateNode, value);
            }
        }

        //gets/sets the TIME_CREATED node
        public string CreateTime
        {
            get
            {
                XmlNode xmlHdrNode;
                XmlNode xmlCrTmNode;
                string sTime;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                {
                    xmlCrTmNode = xmlHdrNode.SelectSingleNode(Constants.sHdrTime);
                    sTime = XML.XMLExtractTime_Time(xmlCrTmNode);
                }
                else
                    sTime = "";

                return sTime;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlTimeNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlTimeNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrTime);
                else
                    xmlTimeNode = null;

                if (xmlTimeNode == null)
                {
                    xmlTimeNode = XML.XMLElementAsTime_Node(Constants.sHdrTime, value);
                    xmlHdrNode.AppendChild(xmlTimeNode);
                }
                else
                    XML.XMLSetTimeAttributes(xmlTimeNode, value);
            }
        }
        //gets/sets the TIME_ZONE attribute in the TIME_CREATED node
        public string CreateTimeZone
        {
            get
            {
                XmlNode xmlHdrNode;
                XmlNode xmlCrTmNode;
                string stimezone;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                {
                    xmlCrTmNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrTime);
                    stimezone = XML.XMLGetAttributeValue(xmlCrTmNode, Constants.sTimeTimeZone);
                }
                else
                    stimezone = "";

                return stimezone;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlTimeNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlTimeNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrTime);
                else
                    xmlTimeNode = null;

                if (xmlTimeNode == null)
                {
                    xmlTimeNode = XML.XMLElementAsTime_Node(Constants.sHdrTime, "", value);
                    xmlHdrNode.AppendChild(xmlTimeNode);
                }
                else
                    XML.XMLSetAttributeValue(xmlTimeNode, Constants.sTimeTimeZone, value);
            }
        }

        //Start SIW180
        //gets/sets the DATE_EXTRACTED node
        public string ExtractDate
        {
            get
            {
                XmlNode xmlHdrNode;
                XmlNode xmlDtNode;
                string sDate;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                {
                    xmlDtNode = xmlHdrNode.SelectSingleNode(Constants.sHdrDateExtract);
                    if (xmlDtNode != null)
                        sDate = XML.XMLExtractDate_Date(xmlDtNode);
                    else
                        sDate = "";
                }
                else
                    sDate = "";
                return sDate;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlNode xmlDateNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlDateNode = xmlHdrNode.SelectSingleNode(Constants.sHdrDateExtract);
                else
                    xmlDateNode = null;

                if (xmlDateNode == null)
                {
                    if (value != "" && value != null)
                    {
                        xmlDateNode = XML.XMLElementAsDate_Node(Constants.sHdrDateExtract, value);
                        xmlHdrNode.AppendChild(xmlDateNode);
                    }
                }
                else
                {
                    if (value == "" || value == null)
                    {
                        xmlHdrNode.RemoveChild(xmlDateNode);
                    }
                    else
                        XML.XMLSetDateAttributes(xmlDateNode, value);
                }
            }
        }
        //End SIW180

        //Start SIW180
        //gets/sets the TIME_EXTRACTED node
        public string ExtractTime
        {
            get
            {
                XmlNode xmlHdrNode;
                XmlNode xmlTmNode;
                string sTime;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                {
                    xmlTmNode = xmlHdrNode.SelectSingleNode(Constants.sHdrTimeExtract);
                    if (xmlTmNode != null)
                        sTime = XML.XMLExtractTime_Time(xmlTmNode);
                    else
                        sTime = "";
                }
                else
                    sTime = "";

                return sTime;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlTimeNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlTimeNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrTimeExtract);
                else
                    xmlTimeNode = null;

                if (xmlTimeNode == null)
                {
                    if (value != "" && value != null)
                    {
                        xmlTimeNode = XML.XMLElementAsTime_Node(Constants.sHdrTimeExtract, value);
                        xmlHdrNode.AppendChild(xmlTimeNode);
                    }
                }
                else
                {
                    if (value == "" || value == null)
                    {
                        xmlHdrNode.RemoveChild(xmlTimeNode);
                    }
                    else
                        XML.XMLSetTimeAttributes(xmlTimeNode, value);
                }
            }
        }
        //End SIW180

        //gets/sets the XML functions object
        public override XML XML
        {
            get
            {
                if (xf == null)
                {
                    xf = new XML();
                    xf.Header = this;
                }
                return xf;
            }
            set
            {
                if (xf != null)
                    if (value != null)
                        if (xf.guid == value.guid)
                            return;
                xf = null;
                xf = value;
                if (xf != null)
                    xf.Header = this;
            }
        }

        //gets/sets the DESCRIPTION node
        public string Description
        {
            get
            {
                XmlNode xmlHdrNode;
                string strDesc;

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    strDesc = XML.XMLExtractElement(xmlHdrNode, Constants.sHdrDescription);
                else
                    strDesc = "";

                return strDesc;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlDescNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlDescNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrDescription);
                else
                    xmlDescNode = null;

                if (xmlDescNode == null)
                {
                    xmlDescNode = XML.XMLElementWithData_Node(Constants.sHdrDescription, value);
                    xmlHdrNode.AppendChild(xmlDescNode);
                }
                else
                {
                    xmlDescNode.InnerXml = value;
                    //xmlHdrNode.ReplaceChild(xmlHdrNode.SelectSingleNode(Constants.sHdrDescription), xmlDescNode);
                    //xmlDescNode.Value = value;
                }
            }
        }

        //gets/sets the DOCVERSION attribute
        public string DocumentVersion
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrDocVersion);

                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (nval == "" || nval == null)
                    nval = Constants.sCCPXmlVersion;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrDocVersion, nval);
            }
        }

        //gets/sets the DOC_NAME node
        public string Name
        {
            get
            {
                XmlNode xmlHdrNode;
                string strName = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    strName = XML.XMLExtractElement(xmlHdrNode, Constants.sHdrName);

                return strName;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlNameNode;
                
                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    xmlNameNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrName);
                else
                    xmlNameNode = null;

                if (xmlNameNode == null)
                {
                    xmlNameNode = XML.XMLElementWithData_Node(Constants.sHdrName, value);
                    xmlHdrNode.AppendChild(xmlNameNode);
                }
                else
                    xmlNameNode.InnerXml = value;
                    //xmlNameNode.Value = value;
            }
        }

        //gets/sets the header node for the current document
        public override XmlNode Node
        {
            get
            {
                XmlElement xmlDocElement;
                XmlDocument xmlDocument;

                if (xmlThisNode == null)
                {
                    xmlDocument = Document;
                    xmlDocElement = xmlDocument.DocumentElement;
                    xmlThisNode = xmlDocElement.SelectSingleNode(Constants.sHdrElement);
                }

                if (xmlThisNode == null)
                    Create(null, "CCP.XMLFormatting");

                return xmlThisNode;
            }
        }

        //gets/sets the PASSWORD attribute
        public string Password
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata;

                xmlHdrNode = Node;
                sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrPassword);
                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (GlobalVariables.m_AutoEncrypt)
                    if (StringUtils.Left(nval, 1) != "~")
                        nval = "~" + nval;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrPassword, nval);
            }
        }

        //gets/sets the UseSSO attribute SIW8194
        public string UseSSO
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata;

                xmlHdrNode = Node;
                sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrUseSSO);
                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (GlobalVariables.m_AutoEncrypt)
                    if (StringUtils.Left(nval, 1) != "~")
                        nval = "~" + nval;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrUseSSO, nval);
            }
        }

        //gets/sets the RqUID node
        public string RqUID
        {
            get
            {
                XmlNode xmlHdrNode;
                string strUUID = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    strUUID = XML.XMLExtractElement(xmlHdrNode, Constants.sHdrRqUID);

                return strUUID;
            }
            set
            {
                XmlNode xmlHdrNode;
                XmlElement xmlDataNode;
                string strData;

                xmlHdrNode = Node;
                strData = value;
                if (strData == "" || strData == null)
                    strData = Guid.NewGuid().ToString();

                if (xmlHdrNode != null)
                    xmlDataNode = (XmlElement)xmlHdrNode.SelectSingleNode(Constants.sHdrRqUID);
                else
                    xmlDataNode = null;

                if (xmlDataNode == null)
                {
                    xmlDataNode = XML.XMLElementWithData_Node(Constants.sHdrRqUID, "{" + strData.ToUpper() + "}");
                    xmlHdrNode.AppendChild(xmlDataNode);
                }
                else
                    xmlDataNode.InnerXml = "{" + strData.ToUpper() + "}";
            }
        }

        //gets/sets the SOURCEAPP attribute
        public string SourceApplication
        {
            get
            {
                XmlNode xmlHdrNode;
                string sourceapp = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    sourceapp = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrSourceApp);

                return sourceapp;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (nval == "" || nval == null)
                    nval = Application.ProductName;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrSourceApp, nval);
            }
        }

        //Begin SIW485
        //gets/sets the SESSIONID attribute
        public string SessionID
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrSessionID);
                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrSessionID, value);
            }
        }
        //End SIW485

        //gets/sets the SOURCEDB attribute
        public string SourceDB
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata = "";

                xmlHdrNode = Node;
                if (xmlHdrNode != null)
                    sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrSourceDB);
                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrSourceDB, value);
            }
        }

        //gets/sets the TARGETDB attribute
        public string TargetDB
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata = "";

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrTargetDB);
                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;

                xmlHdrNode = Node;

                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrTargetDB, value);
            }
        }

        //gets/sets the USERID attribute
        public string UserID
        {
            get
            {
                XmlNode xmlHdrNode;
                string sdata;

                xmlHdrNode = Node;
                sdata = XML.XMLGetAttributeValue(xmlHdrNode, Constants.sHdrUserID);

                return sdata;
            }
            set
            {
                XmlNode xmlHdrNode;
                string nval;

                xmlHdrNode = Node;
                nval = value;

                if (GlobalVariables.m_AutoEncrypt)
                    if (StringUtils.Left(nval, 1) != "~")
                        nval = "~" + nval;
                if (xmlHdrNode != null)
                    XML.XMLSetAttributeValue(xmlHdrNode, Constants.sHdrUserID, nval);
            }
        }

        //Creates a new HEADER node with the given attributes and children
        public XmlNode Create(XmlDocument xDocument, string sDocName, string sAppName, string sAppDescription, string sUserID, string sUserDSN, string sAppVersion, string sTargetDSN, string sPassword)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;

            if (xDocument != null)
            {
                Document = xDocument;
            }

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(Constants.sHdrElement);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                DocumentVersion = Constants.sCCPXmlVersion;
                if (sAppName != "" && sAppName != null)
                    SourceApplication = sAppName;
                if (sUserDSN != "" && sUserDSN != null)
                    SourceDB = sUserDSN;
                if (sTargetDSN != "" && sTargetDSN != null)
                    TargetDB = sTargetDSN;
                if (sAppVersion != "" && sAppVersion != null)
                    AppVersion = sAppVersion;
                if (sUserID != "" && sUserID != null)
                    UserID = sUserID;
                if (sPassword != "" && sPassword != null)
                    Password = sPassword;
                if (sDocName != "" && sDocName != null)
                    Name = sDocName;
                if (sAppDescription != "" && sAppDescription != null)
                    Description = sAppDescription;
                RqUID = "";
                CreateDate = "";
                CreateTime = "";

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLHeader.Create");
                return null;
            }
        }

        //Overridden for optional parameters
        public XmlNode Create(XmlDocument xDocument, string sDocName)
        {
            return Create(xDocument, sDocName, "", "", "", "", "", "", "");
        }

        //Places the given node into the begining of the current document as header
        public override void InsertInDocument(XmlDocument doc, XmlNode nde)
        {
            XmlNode xmlCurrNode;

            base.InsertInDocument(doc, nde);

            xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sHdrElement);

            if (xmlCurrNode != null)
                xmlDocument.DocumentElement.ReplaceChild(xmlNewNode, xmlCurrNode);
            else
                if (xmlDocument.DocumentElement.FirstChild == null)
                    xmlDocument.DocumentElement.AppendChild(xmlNewNode);
                else
                    xmlDocument.InsertBefore(xmlNewNode, xmlDocument.DocumentElement.FirstChild);
        }
    } //End Class XMLHeader
}
