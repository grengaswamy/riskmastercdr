/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.XmlFormatting;
using System.Collections;
using System.Xml;
using CCP.Common;

namespace CCP.XmlFormatting
{
    public class XCUtils
    {
        public static void subInitializeGlobals()
        {
            if (XCGlobals.gControl == null)
            {
                XCGlobals.gControl = new XMLControls();
            }
            if (XCGlobals.sDocNodeOrder == null)
            {
                XCGlobals.sDocNodeOrder = new ArrayList();
            }
            if (XCGlobals.sNodeOrder == null)
            {
                XCGlobals.sNodeOrder = new ArrayList(1);
                XCGlobals.sNodeOrder.Add("");
            }
            subSetupDocumentNodeOrder();
            XML.NodeOrder = XCGlobals.sDocNodeOrder;
        }

        public static XML XML
        {
            get
            {
                return XCGlobals.gControl.XML;
            }
        }

        public static Debug Debug
        {
            get
            {
                return XCGlobals.gControl.Debug;
            }
        }

        public static void subSetupDocumentNodeOrder()
        {
            if (XCGlobals.sDocNodeOrder.Count < 13)
            {
                XCGlobals.sDocNodeOrder.Capacity = 14;
                setUpArray(XCGlobals.sDocNodeOrder);
            }
            if ((string)XCGlobals.sDocNodeOrder[0] == "" || XCGlobals.sDocNodeOrder[0] == null)
            {
                XCGlobals.sDocNodeOrder[0] = Constants.sHdrElement;
                XCGlobals.sDocNodeOrder[1] = Constants.sPM;
                XCGlobals.sDocNodeOrder[2] = Constants.sBatchElement;
                XCGlobals.sDocNodeOrder[3] = Constants.sEvtNode;
                XCGlobals.sDocNodeOrder[4] = Constants.sDiaCollection;
                XCGlobals.sDocNodeOrder[5] = Constants.sClmNode;
                XCGlobals.sDocNodeOrder[6] = Constants.sVCHGroup;
                XCGlobals.sDocNodeOrder[7] = Constants.sAddNode;
                XCGlobals.sDocNodeOrder[8] = Constants.sOrgHierNode;
                XCGlobals.sDocNodeOrder[9] = Constants.sEntCollection;
                XCGlobals.sDocNodeOrder[10] = Constants.sAdrCollection;
                XCGlobals.sDocNodeOrder[11] = Constants.sCmtCollection;
                XCGlobals.sDocNodeOrder[12] = Constants.sCDGlossary;
                XCGlobals.sDocNodeOrder[13] = Constants.sBLNode;
            }
        }

        private static void setUpArray(ArrayList arr)
        {
            for (int i = arr.Count; i < arr.Capacity; i++)
            {
                arr.Add(null);
            }
        }

        public static XmlNode putAttribute(XmlNode xmlPNode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);
                }
            }
            else
            {
                if (sCode != "" && sCode != null)
                    XML.XMLInsertAttribute_Node(xmlNode, AttribName, sCode);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);
            }
            return xmlNode;
        }

        public static string getAttribute(XmlNode pnode, string NodeName, string attname)
        {
            string strdata;
            XmlNode xmlNode;
            strdata = "";
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLGetAttributeValue(xmlNode, attname);
            }
            return strdata;
        }

        public static XmlNode getNode(XmlNode pnode, string NodeName)
        {
            XmlNode xmlENde = null;
            if (pnode != null)
                if (NodeName != "" && NodeName != null)
                    xmlENde = XML.XMLGetNode(pnode, NodeName);
                else
                {
                    xmlENde = pnode;
                    XML.CurrNode = pnode;
                }
            return xmlENde;
        }

        public static string getData(XmlNode pnode, string NodeName)
        {
            return XML.XMLExtractElement(pnode, NodeName);
        }

        public static string getDate(XmlNode pnode, string NodeName)
        {
            XmlNode xmlNode;
            string strdata = "";
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractDate_Date(xmlNode);
            }
            return strdata;
        }

        public static XmlNode putData(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                    xmlNode = XML.XMLputData(xmlPNode, arrnodeorder, NodeName, sNewValue);

            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    xmlNode.InnerXml = sNewValue;
            }
            return xmlNode;
        }

        public static XmlNode putTypeItem(XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if ((sDesc != "" && sDesc != null) || (sCode != "" && sCode != null))
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLInsertType_Node(xmlNode, sDesc, sCode);
                }
            }
            else
            {
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null))
                {
                    if (NodeName != null && NodeName != "")
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                {
                    XML.XMLInsertType_Node(xmlNode, sDesc, sCode);
                }
            }
            return xmlNode;
        }

        public static string getType(XmlNode pnode, string NodeName)
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractTypeAttribute(xmlNode);
            }
            return strdata;
        }

        public static XmlNode putType(XmlNode pnode, string NodeName, string sType, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sType != "" && sType != null)
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);
                }
            }
            else
            {
                if (sType != "" && sType != null)
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtType, sType);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode, Constants.sdtType);
            }
            return xmlNode;
        }

        public static string getEntityRef(XmlNode pnode, string NodeName)
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractEntityAttribute(xmlNode, null);
            }
            return strdata;
        }

        public static XmlNode putEntityRef(XmlNode pnode, string NodeName, string sEID, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sEID != "" && sEID != null && sEID != "0")
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLInsertEntity_Node(xmlNode, sEID, null);
                }
            }
            else
            {
                if (sEID != "" && sEID != null && sEID != "0")
                    XML.XMLInsertEntity_Node(xmlNode, sEID, null);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode, "ID");
            }
            return xmlNode;
        }

        public static XmlNode putDate(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                {
                    xmlNode = XML.XMLElementAsDate_Node(NodeName, sNewValue);
                    XML.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);
                }
            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    XML.XMLSetDateAttributes(xmlNode, sNewValue);
            }
            return xmlNode;
        }

        public static string getTime(XmlNode pnode, string NodeName)
        {
            XmlNode xmlNode;
            string strdata = "";
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractTime_Time(xmlNode);
            }
            return strdata;
        }

        public static XmlNode putTime(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sNewValue != "" && sNewValue != null)
                {
                    xmlNode = XML.XMLElementAsTime_Node(NodeName, sNewValue);
                    XML.XMLInsertNodeInPlace(xmlPNode, xmlNode, arrnodeorder);
                }
            }
            else
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    XML.XMLSetTimeAttributes(xmlNode, sNewValue);
            }
            return xmlNode;
        }

        public static XmlNode putCodeItem(XmlNode pnode, string NodeName, string sDesc, string sCode, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if ((sDesc != "" && sDesc != null) || (sCode != "" && sCode != null))
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLInsertCode_Node(xmlNode, sDesc, sCode);
                }
            }
            else
            {
                if ((sDesc == "" || sDesc == null) && (sCode == "" || sCode == null))
                {
                    if (NodeName != "" && NodeName != null)
                        xmlPNode.RemoveChild(xmlNode);
                    else
                        xmlNode.ParentNode.RemoveChild(xmlNode);
                    xmlNode = null;
                }
                else
                    XML.XMLInsertCode_Node(xmlNode, sDesc, sCode);
            }
            return xmlNode;
        }

        public static string getCode(XmlNode pnode, string NodeName)
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractCodeAttribute(xmlNode);
            }
            return strdata;
        }

        public static XmlNode putCode(XmlNode pnode, string NodeName, string sCode, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            XmlNode xmlPNode;
            xmlPNode = pnode;
            xmlNode = getNode(xmlPNode, NodeName);
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    xmlNode = XML.XMLaddNode(xmlPNode, NodeName, arrnodeorder);
                    XML.XMLSetAttributeValue(xmlNode, Constants.sdtCode, sCode);
                }
            }else{
                if(sCode != "" && sCode != null)
                    XML.XMLSetAttributeValue(xmlNode,Constants.sdtCode,sCode);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode,Constants.sdtCode);
            }
            return xmlNode;
        }

        public static string getAddressRef(XmlNode pnode, string NodeName)
        {
            string strdata = "";
            XmlNode xmlNode;
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractAddressAttribute(xmlNode, null);
            }
            return strdata;
        }

        public static XmlNode putAddressRef(XmlNode pnode, string NodeName, string sID, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            xmlNode = getNode(pnode, NodeName);
            if (xmlNode == null)
            {
                if (sID != "" && sID != null && sID != "0")
                {
                    xmlNode = XML.XMLaddNode(pnode, NodeName, arrnodeorder);
                    XML.XMLInsertAddress_Node(xmlNode, sID, null);
                }
            }
            else
                if (sID != "" && sID != null && sID != "0")
                    XML.XMLInsertAddress_Node(xmlNode, sID,null);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode, "ID");
            return xmlNode;
        }

        public static string getBool(XmlNode pnode, string NodeName, string AttribName)
        {
            string strdata;
            XmlNode xmlNode;
            strdata = "";
            if (pnode != null)
            {
                xmlNode = getNode(pnode, NodeName);
                if (xmlNode != null)
                    strdata = XML.XMLExtractBooleanAttribute(xmlNode, AttribName);
            }
            return strdata;
        }

        public static XmlNode putBool(XmlNode pnode, string NodeName, string AttribName, string sCode, ArrayList arrnodeorder)
        {
            XmlNode xmlNode;
            xmlNode = getNode(pnode, NodeName);
            if (xmlNode == null)
            {
                if (sCode != "" && sCode != null)
                {
                    xmlNode = XML.XMLaddNode(pnode, NodeName, arrnodeorder);
                    XML.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);
                }
            }
            else
            {
                if (sCode != "" && sCode != null)
                    XML.XMLInsertBoolean_Node(xmlNode, AttribName, sCode);
                else
                    XML.XMLRemoveAttribute_Node(xmlNode, AttribName);
            }
            return xmlNode;
        }
    }
}
