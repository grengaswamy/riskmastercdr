/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 07/24/2009 | SIW191  |   JTC      | Updates for security with 2008.3
 * 05/19/2010 | SIW451  |   AS       | Updates for security for reserves
 * 07/16/2010 | SIW344  |   AP       | Add security constants for Liability Loss.
 * 05/24/2011 | SIW7087 |   MS       | Diary reject implemetation
 * 08/09/2011 | SIN7343 |   MK       | Security settings for Entity W9-Info
 * 10/12/2011 | SIW7650 |   LAu      | Add Privacy View
 * 12/26/2011 | SIW7928 |   Vineet   | Implementing security permission for bank account combo in payment/collection
 * 01/10/2011 | SIW8039 |   SW       | Security for Diary Attachments
 * 01/13/2012 | SIW7938 |   SAS      | Assign security ID for Individual type
 * 05/03/2012 | SIN8201 |    MS      | Correcting Query designer Security Definitions
 * 05/03/2012 | SIW8460 |   PV       |Supplementals are not working as per the settings in SMS in web
 ***********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace CCP.Constants
{
    public class SecDefs
    {
    public const int RME_OK = 0;
    public const int RME_NOVIEW = 1;                     // lacks view permission
    public const int RME_NOCREATE = 2;                   // lacks create new record permission

    public const int RMO_ACCESS = 0;
    public const int RMO_VIEW = 1;
    public const int RMO_UPDATE = 2;
    public const int RMO_CREATE = 3;
    public const int RMO_DELETE = 4;
    public const int RMO_COPY = 5;      //SIW191
    public const int RMO_IMPORT = 5;
    public const int RMO_ATTACHACCESS = 6;
    public const int RMO_COMMENTS = 11;      //SIW191
    public const int RMO_PARTIES_INVOLVED = 12;      //SIW191
    public const int RMO_PAY = 6;
    public const int RMO_PROPERTIES = 7;
    public const int RMO_REVIEW = 7;
    public const int RMO_APPROVE = 8;
    public const int RMO_VOID = 9;
    public const int RMO_ASSIGN_EXPIRED = 9;
    public const int RMO_PURGE = 10;      //SIW191

    public const string SEC_VIOLATION = "SecurityViolationMsg.";
    public const string SEC_VIOLATION_CAPTION = "0001";
    public const string SEC_VIOLATION_MODULE = "0002";
    public const string SEC_VIOLATION_APPL = "0003";
    public const string SEC_VIOLATION_PEEK = "0004";
    public const string SEC_VIOLATION_FUNCTION = "0005";
    public const string SEC_VIOLATION_CREATE = "0006";
    public const string SEC_VIOLATION_ACCESS = "0007";
    public const string SEC_VIOLATION_DELETE = "0008";
    public const string SEC_VIOLATION_TRAILER = "0009";
    public const string SEC_VIOLATION_LAUNCH = "0010";
    public const string SEC_VIOLATION_UPDATE = "0011";
    public const string SEC_VIOLATION_VIEW = "0012";

    public const int RMA_RISKMASTER = 1;
    public const int RMA_UTILITIES = 100000;
    //SIW191 public const int RMA_QUICKENTRY = 150000;
    public const int RMA_EXEC_SUMMARY = 350000;
    public const int RMA_SCRIPT_EDITOR = 120000;
    public const int RMA_DATABASE_TOOLS = 122000;
    public const int RMA_EVENTSCHEDULER = 250000;

    public const int RMA_DB_UPGRADES = 121000;
    public const int RMA_DB_UPGRADE_FORMS = 10;
    public const int RMA_DB_UPGRADE_CODES = 20;
    public const int RMA_DB_UPGRADE_SUPPL = 30;
    public const int RMA_DB_UPGRADE_SCRIPTS = 40;
    public const int RMA_DB_UPGRADE_CODEREL = 50;      //SIW191

    public const int RMB_CC = 30000;              //Carrier Claims
    public const int RMB_CC_PL = 30100;           //CC Property Loss
    public const int RMB_CC_PI = 30050;           //CC Person Involved
    public const int RMB_CC_VL = 30150;           //CC Vehicle Loss
    public const int RMB_CC_IL = 30200;           //CC Injury Loss
    public const int RMB_CC_WC = 30250;           //CC Workers Comp
    public const int RMB_CC_LI = 30300;           //CC Litigation
    public const int RMB_CC_SB = 30350;           //CC Subrogation
    public const int RMB_CC_EV = 30400;           //CC Event
    public const int RMB_CC_AR = 30450;           //CC Arbitration
    public const int RMB_CC_LP = 30500;           //CC Legal Inv Parties

    public const int SUPP_OFFSET = 50;
    public const int JUR_OFFSET = 100;
    public const int ADM_OFFSET = 70;      //SIW191

    public const int RMB_NA = 0;
    public const int RMB_NOT_SECURED = 0;                // used for all unsecured forms

    public const int RMB_USERDOC = 50;
    public const int MCP_MEDICARE = 31780;//SIW8460
    public const int MCP_APPLICATION = 31000;
    public const int MCP_ALLOW_MULTI_DB_ACCESS = 31010;
    public const int MCP_USERDOC = 31050;
    public const int MCP_PARTYINVOLVED = 31070;
    public const int MCP_ASSIGN_EXPIRED = RMO_ASSIGN_EXPIRED;
    public const int MCP_POLICY = 31180;
    public const int MCP_POLICY_DISALLOW_PAYMENTS = 5;      //SIW191
    public const int MCP_CARRIER_CLAIMS = 31100;
    public const int MCP_CLOSE_CLAIM = 30;
    public const int MCP_REOPEN_CLAIM = 31;
    public const int MCP_UPDATE_CLOSED_CLAIM = 32;
    public const int MCP_CHANGE_CLAIM_DOL = 33;
    public const int MCP_FREEZE_CLAIM_PAYMENTS = 34;
    public const int MCP_CHANGE_DOL_OUTSIDE_POL = 35;
    public const int MCP_CHANGE_DOL_INSIDE_POL = 36;
    public const int MCP_ASSIGN_NUMBER = 37;      //SIW191

    public const int MCP_INJ_LOSS = 31200;
    public const int MCP_INJURY_WORKLOSS = 38000;
    public const int MCP_PROPERTY_LOSS = 31200;
    public const int MCP_VEHICLE_LOSS = 31300;
    public const int MCP_SALVAGE = 31700;      //SIW191
    public const int MCP_INJURY_LOSS = 31400;
    public const int MCP_WCOMP = 31500;
    public const int MCP_WC_WORKLOSS = 31800;
    public const int MCP_WC_RESTRICT = 31900;
    public const int MCP_LITIGATION = 32000;
    public const int MCP_SUBROGATION = 32100;
    public const int MCP_ARBITRATION = 32200;
    public const int MCP_LIAB_LOSS = 32400; //SIW344

    public const int MCP_ENTITY_W9= 34020; //SIN7343
    public const int MCP_ENTITY_W9_VW = 1; //SIN7343
    public const int MCP_ENTITY_W9_UPDT = 2; //SIN7343
    public const int MCP_ENTITY_FINANCIALS = 34015; //SIN7343
    public const int MCP_ENTITY_DISALLOW_PAY = 1; //SIN7343
    public const int MCP_ENTITY = 34000;
    public const int MCP_ENTITY_UPDATE_FINANCIAL_INFO = 15;      //SIW191
    public const int MCP_ENTITY_DISALLOW_PAYMENTS = 16;      //SIW191
    public const int MCP_ENTITY_PRIVACY_VIEW = 17;      //SIW7650
    //public const int MCP_ENTITY_CHANGE_TYPE = 12;     //SIW7938
    //public const int MCP_ENTITY_ASSIGN_SEC_ID = 19;      //SIW191//SIW7938
    public const int MCP_ENTITY_CHANGE_TYPE = 19;          //SIW7938
    public const int MCP_ENTITY_ASSIGN_SEC_ID = 18;        //SIW7938
    public const int MCP_ENTITY_MEDICAL_DATA = 34040;
    public const int MCP_ENTITY_ID_NBR = 39200;
    public const int MCP_CHANGE_PROTECTED_ENTITY_ID = 30;
    public const int MCP_ENTITY_W9_VIEW = 34021;
    public const int MCP_ENTITY_W9_UPDATE = 34022;
    public const int MCP_EVENT = 34100;
    public const int MCP_ADMIN_TRACKING = 34200;
    //SIW191 public const int MCP_CODES = 34500;
    public const int MCP_TABLES = 134600;
    public const int MCP_TABLES_SYSTEM = 10;      //SIW191
    public const int MCP_TABLES_SYSCODE = 20;      //SIW191
    public const int MCP_TABLES_USRCODE = 30;      //SIW191
    public const int MCP_TABLES_ADMIN_TRACKING = 70;      //SIW191
    public const int MCP_TABLES_SUPPLEMENTALS = 50;      //SIW191
    public const int MCP_TABLES_JURISDICATIONALS = 100;      //SIW191
    public const int MCP_STATES = 134800;      //SIW191
    public const int MCP_ORG = 34700;
    public const int MCP_ORG_SEC_OVERRIDE = 34720;      //SIW191
    //SIW191 public const int MCP_STATES = 34800;
    public const int MCP_DIARY = 35000;
    public const int MCP_DIARYEXPENSES = 35100;
    public const int MCP_DIARYCREATE = RMO_CREATE;
    public const int MCP_DIARYCOMPL = RMO_UPDATE;
    public const int MCP_DIARYROLL = 32;
    public const int MCP_DIARYVOID = 33;
    public const int MCP_DIARYROUTE = 34;
    public const int MCP_DIARYPEEK = 35;
    public const int MCP_DIARYPEEKALL = 36;      //SIW191           //Allow peeking to all users
    public const int MCP_DIARYREINSTATE = 37;       //SIW191        //Reinstate closed diaries
    public const int MCP_DIARYMULTI = 10;       //SIW191            //Allows Multiple Processing
    public const int MCP_DIARYALL = 20;         //SIW191            //Override Not Routable/Rollable
    //public const int MCP_DIARYREJECT = 6;       // SIW7087    //SIW8039
    public const int MCP_DIARYREJECT = 38;        // SIW8039

    public const int MCP_DIARYHIST = 36000;
    public const int MCP_MESSAGES = 37000;
    public const int MCP_TREATMENT = 38100;
    public const int MCP_PRE_CERTIFICATION = 38200;
    public const int MCP_IME = 38300;
    public const int MCP_PEER_REVIEW = 38400;
    public const int MCP_BILL_ENTRY = 39000;
    public const int MCP_COMMENT = 39100;
    public const int MCP_APPEND_COMMENT = 5;
    public const int MCP_VIEW_PRIVATE_COMMENT = 30;
    public const int MCP_UPDATE_PUB_VIEW_ONLY_COMMENT = 31;
    public const int MCP_UPDATE_PAST_DURATION_COMMENT = 32;
    //SIW191 public const int MCP_ENTITY_ASSIGN_SEC_ID = 11;
    public const int MCP_WORK_COMP_JUR = 31600;
    public const int MCP_SCHEDULED_ACTIVITY = 39300;
    public const int MCP_DELETE_SCHED_ITEM_WITH_ACTUAL_DT = 30;
    public const int MCP_DEMAND_OFFER = 39400;
    public const int MCP_DELETE_DEMAND_OFFER_ITEM_WITH_DT = 30;

    public const int MCP_JUR_OFFSET = 500;

    public const int MCP_CUSTOM_REPORTS = 400000;
    public const int MCP_CUSTOM_REPORTS_STATE = 100;
    public const int MCP_CUSTOM_REPORTS_ADJUSTER = 200;
    public const int MCP_CUSTOM_REPORTS_EXEC_REPORTS = 500;

    //Table Security Definition format = 1ggtttfffp gg = Glossary Type,
    //                                              ttt = Table ID,
    //                                              fff = field ID
    //                                              p = permission
    //This provides 999 tables by type and 999 tables by field.  Should be plenty
    public const int MCP_TABLE_SECURITY_OFFSET = 1000000000;
    public const int MCP_TABLE_SECURITY_TYPE_MULTIPLIER = 10000000;
    public const int MCP_TABLE_SECURITY_MULTIPLIER = 10000;
    public const int MCP_TABLE_SECURITY_FIELD_MULTIPLIER = 10;

    public const int MCP_XML_IMPORT = 200100;

    public const int MCP_FUNDS = 32300;
    public const int MCP_FUNDS_TRANSACT = 32400;
    public const int MCP_PRINT_CHK = 30;
    public const int MCP_EDIT_PAYEE = 31;
    public const int MCP_ALLOW_MANUAL_CHECKS = 34;
    public const int MCP_ALLOW_EDIT_CTL_NUMBER = 35;
    public const int MCP_ALLOW_EDIT_CHK_NUMBER = 36;

    public const int MCP_FUNDS_BNKACCT = 32500;
    public const int MCP_FUNDS_BNKACCT_CHKSTOCK = 32600;
    public const int MCP_FUNDS_PRINTCHK = 32700;
    public const int MCP_ALLOW_OVRRIDE_LOCK = 30;
    public const int MCP_ALLOW_CHANGE_CHECK_NUMBER = 31;

    public const int MCP_FUNDS_CLEARCHK = 32800;
    public const int MCP_FUNDS_VOIDCHK = 32900;
    public const int MCP_FUNDS_VOID_PRINTED_CHECK = 30;
    public const int MCP_FUNDS_BNKACCT_BALANCE = 32950;
    public const int MCP_FUNDS_BNKACCT_SUBACT = 33000;
    public const int MCP_FUNDS_SUP_APPROVE_PAYMENTS = 33050;
    public const int MCP_FUNDS_DEPOSIT = 33100;
    public const int MCP_FUNDS_COMB_PAYMENTS = 33200;
    public const int MCP_FUNDS_COMB_PAYMENTS_OFFSET = 33210;
    public const int MCP_FUNDS_IMPORT_PAYMENTS = 33250;
    public const int MCP_FUNDS_IMPORT_PAYMENTS_EDIT = 33251;
    public const int MCP_FUNDS_IMPORT_PAYMENTS_SETTINGS = 33250;
    public const int MCP_FUNDS_HONORED_TO_PRINTED = 33310;
    public const int MCP_FUNDS_PRINTED_TO_RELEASED = 33320;
    public const int MCP_FUNDS_BACKUP_WITHHOLDING = 33400;
    public const int MCP_FUNDS_PRINTED_TO_UNCLAIMED = 33330;
    public const int MCP_FUNDS_UNCLAIMED_TO_ESCHEAT = 33340;

    public const int MCP_MANUAL_POLICY = 34900;
    //SIW191 public const int MCP_PAYMENT_ENTRY = 39700;

    public const int MCP_NA = RMB_NA;
    public const int MCP_NOT_SECURED = RMB_NOT_SECURED;
    public const int MCP_OK = RME_OK;
    public const int MCP_ACCESS = RMO_ACCESS;
    public const int MCP_VIEW = RMO_VIEW;
    public const int MCP_UPDATE = RMO_UPDATE;
    public const int MCP_CREATE = RMO_CREATE;
    public const int MCP_DELETE = RMO_DELETE;
    public const int MCP_IMPORT = RMO_IMPORT;
    public const int MCP_COPY = RMO_COPY;      //SIW191
    public const int MCP_COMMENTS = RMO_COMMENTS;      //SIW191
    public const int MCP_PARTIES_INVOLVED = RMO_PARTIES_INVOLVED;      //SIW191
    public const int MCP_ATTACHACCESS = RMO_ATTACHACCESS;
    public const int MCP_REVIEW = RMO_REVIEW;
    public const int MCP_APPROVE = RMO_APPROVE;
    public const int MCP_VOID = RMO_VOID;
    public const int MCP_PAY = RMO_PAY;
    public const int MCP_PURGE = RMO_PURGE;      //SIW191

    public const int MCP_SUPP_OFFSET = SUPP_OFFSET;

    public const int MCP_UTILITIES = RMA_UTILITIES;
    public const int MCP_UTILITIES_DESIGNER = 4000;//SIN8201
    public const int MCP_UTIL_USER_PRIV = 101000;
    public const int MCP_UTIL_USER_PRIV_ENABLE_LIMITS = 101050;
    public const int MCP_UTIL_USER_PRIV_ASN_VAL = 10;
    public const int MCP_UTIL_USER_PRIV_CLM_LIM = 101100;
    public const int MCP_UTIL_USER_PRIV_INC_LIM = 101200;
    public const int MCP_UTIL_USER_PRIV_INC_ASN_OVR_AUTH = 101220;
    public const int MCP_UTIL_USER_PRIV_INC_SET_FRZ_LVL = 101230;
    public const int MCP_UTIL_USER_PRIV_PAY_LIM = 101300;
    public const int MCP_UTIL_USER_PRIV_RSRV_LIM = 101400;
    public const int MCP_UTIL_USER_PRIV_PAY_DET_LIM = 101500;
    public const int MCP_UTIL_USER_PRIV_PRT_CHK_LIM = 101600;
    public const int MCP_UTIL_USER_PRIV_EVNT_LIM = 101700;
    public const int MCP_UTIL_FIELDS = 101800;
    public const int MCP_UTIL_FIELD_SETUP = 1;
    public const int MCP_UTIL_FIELD_CAPTION = 2;
    public const int MCP_UTIL_OPTIONS = 101810;
    public const int MCP_UTIL_URL = 101820;
    public const int MCP_UTIL_URL_GLOBAL = 1;
    public const int MCP_UTIL_URL_PRIVATE = 2;
    public const int MCP_UTIL_PAY_PARA_SETUP = 102000;
    public const int MCP_UTIL_PAY_PARA_ASN_VAL = 10;
    public const int MCP_UTIL_CHK_OPTS_SETUP = 102100;
    public const int MCP_UTIL_FUTURE_PAY_SETUP = 102200;
    public const int MCP_UTIL_SUPER_APP_SETUP = 102300;
    public const int MCP_UTIL_CODE_RELATE = 103000;
    //SIW191 public const int MCP_UTIL_CODE_RELATE_UPDATE = 103100;
    public const int MCP_UTIL_ORG_SEC = 134750;      //SIW191
    public const int MCP_UTIL_ORG_SEC_ENT = 134760;      //SIW191
    //SIW191 public const int MCP_UTIL_CODE_MAINT = 104000;
    //SIW191 public const int MCP_UTIL_CODE_MAINT_SYSTBLE_UPDATE = 9;

    //Start SIN8201 - commmented the below line to correct the security definition
    //public const int MCP_UTIL_QUERY_DESIGNER = 104010;      //SIW191
    public const int MCP_UTIL_DESIGNER_QUERY_DESIGNER = 10;
    //End SIN8201

    public const int MCP_UTIL_MAIL_MERGE = 104020;      //SIW191
    public const int MCP_UTIL_WPA_DESIGNER = 104030;      //SIW191
    public const int MCP_UTIL_CLAIM_NUMBER_OPTIONS = 105010;      //SIW191
    public const int MCP_UTIL_CLAIM_TYPE_OPTIONS = 105020;      //SIW191
    public const int MCP_UTIL_AUTOTASK_SETUP = 105030;      //SIW191
    public const int MCP_UTIL_DEDCOPAY_SETUP = 105040;      //SIW191
    public const int MCP_UTIL_BRS_SETUP = 105050;      //SIW191
    public const int MCP_UTIL_DIARY_UTILITIES = 106000;      //SIW191
    public const int MCP_UTIL_TECHNICAL = 108000;      //SIW191

    //SIW191 public const int MCP_QUICKENTRY = RMA_QUICKENTRY;
    public const int MCP_EXEC_SUMMARY = RMA_EXEC_SUMMARY;

    public const int RMB_GC = 150;
    public const int RMO_CLAIM_CHANGE_DEPARTMENT = 33;

    public const int RMB_GC_PI = 300;
    public const int RMB_GC_PI_DEPEND = 450;
    public const int RMB_GC_PI_RESTRICT = 600;
    public const int RMB_GC_PI_WRKLOSS = 750;
    public const int RMB_GC_PI_PROC = 900;
    public const int RMB_GC_ADJ = 1050;
    public const int RMB_GC_LIT = 1350;
    public const int RMB_GC_LIT_EXPERT = 1500;
    public const int RMB_GC_DEFEND = 1650;
    public const int RMB_GC_CLMNTS = 1800;
    public const int RMB_GC_CLMNTS_RSRV = 1950;
    public const int RMB_GC_CLMNTS_RSRV_PAYHIST = 2100;
    public const int RMB_GC_RSRV = 2250;
    public const int RMB_GC_RSRV_PAYHIST = 2400;
    public const int RMB_WC = 3000;
    public const int RMO_OVERRIDE_MCO_SELECTION = 34;
    public const int RMB_WC_PI = 3150;
    public const int RMB_WC_PI_DEPEND = 3300;
    public const int RMB_WC_PI_RESTRICT = 3450;
    public const int RMB_WC_PI_WRKLOSS = 3600;
    public const int RMB_WC_PI_PROC = 3750;
    public const int RMB_WC_ADJ = 3900;
    public const int RMB_WC_ADJ_TEXT = 4050;
    public const int RMB_WC_LIT = 4200;
    public const int RMB_WC_LIT_EXPERT = 4350;
    public const int RMB_WC_DEFEND = 4500;
    public const int RMB_WC_OSHA = 4650;
    public const int RMO_OSHA_PRINT101 = 30;             // applies to WC and EVENT OSHA
    public const int RMB_WC_EMP = 4800;
    public const int RMB_WC_EMP_DEPEND = 4950;
    public const int RMB_WC_EMP_RESTRICT = 5100;
    public const int RMB_WC_EMP_WRKLOSS = 5250;
    public const int RMB_WC_RSRV = 5400;
    public const int RMB_WC_RSRV_PAYHIST = 5550;
    public const int RMB_WC_BENEF = 5600;
    public const int RMB_VA = 6000;
    public const int RMB_VA_PI = 6150;
    public const int RMB_VA_PI_DEPEND = 6300;
    public const int RMB_VA_PI_RESTRICT = 6450;
    public const int RMB_VA_PI_WRKLOSS = 6600;
    public const int RMB_VA_PI_PROC = 6750;
    public const int RMB_VA_ADJ = 6900;
    public const int RMB_VA_ADJ_TEXT = 7050;
    public const int RMB_VA_LIT = 7200;
    public const int RMB_VA_LIT_EXPERT = 7350;
    public const int RMB_VA_DEFEND = 7500;
    public const int RMB_VA_CLMNTS = 7650;
    public const int RMB_VA_CLMNTS_RSRV = 7800;
    public const int RMB_VA_CLMNTS_RSRV_PAYHIST = 7950;
    public const int RMB_VA_UNIT = 8100;
    public const int RMB_VA_UNIT_RSRV = 8250;
    public const int RMB_VA_UNIT_RSRV_PAYHIST = 8400;
    public const int RMB_VA_RSRV = 8550;
    public const int RMB_VA_RSRV_PAYHIST = 8700;
    public const int RMB_POLMGT = 9000;
    public const int RMB_POLMGT_COV = 9150;

    public const int RMB_FUNDS = 9500;
    public const int RMB_FUNDS_TRANSACT = 9650;
    public const int RMO_PRINT_CHK = 30;
    public const int RMO_EDIT_PAYEE = 31;
    public const int RMO_ALLOW_UNVOID = 32;
    public const int RMO_ALLOW_UNCLEAR = 33;
    public const int RMO_ALLOW_MANUAL_CHECKS = 34;
    public const int RMO_ALLOW_EDIT_CTL_NUMBER = 35;
    public const int RMO_ALLOW_EDIT_CHK_NUMBER = 36;

    public const int RMB_FUNDS_BNKACCT = 9800;
    public const int RMB_FUNDS_BNKACCT_CHKSTOCK = 9950;

    public const int RMB_FUNDS_PRINTCHK = 10100;
    public const int RMO_ALLOW_CHANGE_CHECK_NUMBER = 31;

    public const int RMB_FUNDS_CLEARCHK = 10200;
    public const int RMB_FUNDS_VOIDCHK = 10250;
    public const int RMO_FUNDS_VOID_PRINTED_CHECK = 30;

    public const int RMB_FUNDS_BNKACCT_BALANCE = 10300;

    public const int RMB_FUNDS_AUTOCHK = 10400;
    public const int RMO_AUTOCHK_ALLOW_EDIT_CTL_NUMBER = 32;

    public const int RMB_FUNDS_APPPAYCOV = 10500;
    public const int RMB_FUNDS_BNKACCT_SUBACT = 10550;
    public const int RMB_FUNDS_SUP_APPROVE_PAYMENTS = 10600;
    public const int RMB_FUNDS_DEPOSIT = 10650;

    public const int RMB_EV = 11000;
    public const int RMO_EVENT_CHANGE_DEPARTMENT = 31;

    public const int RMB_EV_PI = 11150;
    public const int RMB_EV_PI_DEPEND = 11300;
    public const int RMB_EV_PI_RESTRICT = 11450;
    public const int RMB_EV_PI_WRKLOSS = 11600;
    public const int RMB_EV_PI_PROC = 11750;
    public const int RMB_EV_FALL = 11900;
    public const int RMB_EV_MEDW = 12050;
    public const int RMO_PRINTMEDW = 30;
    public const int RMB_EV_MEDW_TEST = 12200;
    public const int RMB_EV_MEDW_CONCOM = 12350;
    public const int RMB_EV_OSHA = 12500;
    public const int RMB_EV_TEXT = 12650;
    public const int RMB_AT = 13000;
    public const int RMB_VEHMAINT = 13500;
    public const int RMB_PEOPMAINT = 14000;
    public const int RMB_EMPMAINT = 14500;
    public const int RMB_EMPMAINT_DEPEND = 14650;
    public const int RMB_QUICKDISP = 15000;
    public const int RMB_CODES = 16000;
    public const int RMB_ENTITIES = 16500;
    public const int RMB_ORG = 17000;
    public const int RMB_ORG_EXPO = 17150;
    public const int RMO_EXPO_ROLLUP = 30;
    public const int RMB_STATES = 18000;
    public const int RMB_DIARY = 19000;
    public const int RMB_DIARYHIST = 19500;
    public const int RMB_MESSAGES = 20000;

    public const int RMO_SALARY = 30;
    public const int RMO_CLOSE_CLAIM = 30;
    public const int RMO_REOPEN_CLAIM = 31;
    public const int RMO_UPDATE_CLOSED_CLAIM = 32;
    public const int RMO_PI_EMPLOYEE = 31;

    public const int RMO_HOSPITAL = 35;
    public const int RMO_PHYSICIAN = 36;
    public const int RMO_DIAGNOSIS = 37;
    public const int RMO_TREATMENT = 38;

    //SIW191 public const int MCP_FIN_VIEW = 10;
    //SIW191 public const int MCP_FIN_CREATE = 20;
    //SIW191 public const int MCP_FIN_UPDATE = 30;
    //SIW191 public const int MCP_FIN_CLOSE = 40;
    //SIW191 public const int MCP_FIN_SUP_FLDS = 50;
    //SIW191 public const int MCP_FINANCIALS = 40000;
    //SIW191 public const int MCP_FIN_RESERVES = 40100;
    //SIW191 public const int MCP_FIN_RSRV_KEY = 60;
    //SIW191 public const int MCP_FIN_PAYMENTS = 40200;
    //SIW191 public const int MCP_FIN_OFFSET = 40;
    //SIW191 public const int MCP_FIN_OFFSET_PRINTED = 45;
    //SIW191 public const int MCP_FIN_PAYS_MANUAL = 70;
    //SIW191 public const int MCP_FIN_PAYS_ADJUST = 80;
    public const int MCP_FIN_VIEW = MCP_VIEW;      //SIW191
    public const int MCP_FIN_CREATE = MCP_CREATE;      //SIW191
    public const int MCP_FIN_UPDATE = MCP_UPDATE;      //SIW191
    public const int MCP_FIN_CLOSE = MCP_DELETE;      //SIW191
    public const int MCP_FIN_OFFSET = MCP_DELETE;      //SIW191
    public const int MCP_FIN_RESTRICT_PAYMENT = 9;      //SIW191
    public const int MCP_FIN_OFFSET_PRINTED = 6;      //SIW191
    public const int MCP_FIN_RSRV_KEY = 6;      //SIW191
    public const int MCP_FIN_EDIT_PAYTO = 9;      //SIW191
    public const int MCP_FIN_PAYS_MANUAL = 7;      //SIW191
    public const int MCP_FIN_PAYS_ADJUST = 8;      //SIW191
    public const int MCP_FIN_SUP_FLDS = SUPP_OFFSET;      //SIW191
    public const int MCP_FINANCIALS = 40000;      //SIW191
    public const int MCP_FIN_RESERVES = 40100;      //SIW191
    public const int MCP_FIN_PAYMENTS = 40200;      //SIW191
    public const int MCP_FIN_COLLECTIONS = 40300;
    public const int MCP_FIN_SCHED_PAYS = 40400;
    public const int MCP_FIN_NOTICE = 40500;
    public const int MCP_FIN_MOVE = 40600;
    //SIW191 public const int MCP_FIN_EDIT_PAYTO = 90;
    public const int MCP_FIN_OVERRIDE_TRANS_DATE = 40700;
    public const int MCP_FIN_RESERVE_CHANGE_AMOUNT_OFFSET =2;//SIW451
    public const int MCP_FIN_RESERVE_CANCEL_OFFSET = 4;//SIW451
    public const int MCP_FIN_BANK_ACCT_UPD = 13;  //SIW7928
    }
}
