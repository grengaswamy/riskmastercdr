/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace CCP.Constants
{
    public static class CommonGlobalConstants
    {
        public const int UC_cdtString = 0;
        public const int UC_cdtInteger = 1;
        public const int UC_cdtLong = 2;
        public const int UC_cdtDecimal = 3;
        public const int UC_cdtDouble = 4;
        public const int UC_cdtCurrency = 5;
        public const int UC_cdtDate = 6;
        public const int UC_cdtBool = 7;
        public const int UC_cdtByte = 8;
        public const int UC_cdtSingle = 9;
        public const int UC_cdtTime = 10;

        public const string DB_ACCESS = "DBMS_IS_ACCESS";
        public const string DB_SQLSRVR = "DBMS_IS_SQLSRVR";
        public const string DB_SYBASE = "DBMS_IS_SYBASE";
        public const string DB_INFORMIX = "DBMS_IS_INFORMIX";
        public const string DB_ORACLE = "DBMS_IS_ORACLE";
        public const string DB_ODBC = "DBMS_IS_ODBC";
        public const string DB_DB2 = "DBMS_IS_DB2";
        public const string CHECK_BOX = "checkbox";
        public const string CODE = "code";
        public const string CODE_LIST = "codelist";
        public const string ORGH = "orgh";
        public const string STATE = "state";
        public const string MULTI_STATE = "multistate";
        public const string ENTITY_LIST = "entitylist";
        public const string ENTITY = "entity";
        public const string CURRENCY = "currency";
        public const string NUMERIC = "numeric";
        public const string TEXT = "text";
        public const string SSN = "ssn";
        public const string DATE = "date";
        public const string TIME = "time";
        public const string TABLE_LIST = "tablelist";
        public const string TEXTML = "textml";

        public enum NavDir : int
        {
            // No Navigation
            NavigationNone = 0,

            // Navigate to first record
            NavigationFirst = 1,

            // Navigate to previous record
            NavigationPrev = 2,

            // Navigate to next record
            NavigationNext = 3,

            // Navigate to last record
            NavigationLast = 4,

            // Navigate to a specific record by specifying the id
            NavigationGoTo = 99
        }

        public enum FileAccessTypes
        {
            ForReading = 0,
            ForWriting = 1,
            ForAppending = 2
        }

    }
}
