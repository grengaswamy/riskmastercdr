﻿/**********************************************************************************************
 *   Date     |    SI        | Programmer | Description                                            *
 **********************************************************************************************
 *  06/15/2011 |   SIW7214    |    AV     | Created - to add Medicare nodes for the claimant
 *  09/10/2011 |   SIW7496    |    SW     | Multiple diagnosis node 
 *  10/26/2011 |   SIW7214    |   KCB     | Medicare Eligible
 *  12/20/2011 |   SIW7941    |   SV      | FSIT 140555(Add CommentReference node for the Medicare XML)
 *  04/10/2012 |   SIN8355    |    SW     | PutPartyInvoved corrected.
 *********************************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

//Start SIW7214
namespace CCP.XmlComponents
{
    public class XMLMedicare : XMLXCNodeWithList
    {

        /*************************************************************
          <CLAIM>
              <MEDICARE ID="CMSxxx" ENTITY_ID="ENTxxx">
                 <!-- Multiple Medicare entries Allowed -->
                 <TECH_KEY>AC Technical key for the element </TECH_KEY>
                 <TREE_LABEL>component label</TREE_LABEL>
                 <ELIGIBLE INDICATOR="TRUE|FALSE"/>
                 <HICN_ID>HICN ID Number</HICN_ID>
                 <EFFECTIVE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
                 <CAUSE_OF_INJURY CODE="99">Default</CAUSE_OF_INJURY>
                 <DIAGNOSIS>
                       <SPECIFIC CODE="ICD-9 Code {string} [ICD9]">Diagnosis Translated {string}</SPECIFIC>
                 </DIAGNOSIS>
                 <!-- Multiple Diagonsis Codes Allowed -->
                 <NF_LIMIT>Amount</NF_LIMIT>
                 <NF_EXHAUST_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
                 <PLAN SEQ="x">
                        <PLAN_INS_CODE CODE="Standard CPT Treatment Code {string} [CMS_INS_PLAN]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">                       description
                        </PLAN_INS_CODE>
                        <ORM_IND INDICATOR="TRUE|FALSE"/>
                        <ORM_TERM_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
                        <TPOC SEQ="x">
                           <TPOC_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
                           <TPOC_AMOUNT>Amount</NF_LIMIT>
                           <TPOC_DELAY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
                        </TPOC>
                        <!--Multiple TPOC allowed-->
                  </PLAN>
                  <!--Multiple Plans allowed-->
                  <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">
                      Relationship Translated {string}
                  </PARTY_INVOLVED>
                      <!--Multiple Parties Involved-->
                 <DATA_ITEM>                                
                      <!-- multiple entries --> 
            <COMMENT_REFERENCE ID="CMTxx"/>//SIW7941
                       <!--Muliple nodes allowed -->
              </MEDICARE>
       </CLAIM>
         * **************************************************************************/

        private XmlNode m_xmlMedicare;
        private XmlNodeList m_xmlMedicareList;
        private XmlNode xmlDiagnosis;
        private XmlNodeList xmlDiagnosisList;
        private XmlNode xmlPlan;
        private XmlNodeList xmlPlanList;
        private XmlNode xmlPlanTPOC;
        private XmlNodeList xmlPlanTPOCList;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLClaim m_Claim;
        private XMLVarDataItem m_DataItem;
        private ArrayList sMedicareNodeOrder;
        private ArrayList sDiagNodeOrder;
        private ArrayList sPlanNodeOrder;
        private ArrayList sTPOCNodeOrder;
        private int lEvtId;
        private XMLCommentReference m_Comment;//SIW7941 


        public XMLMedicare()
        {
            Node = null;

            sMedicareNodeOrder = new ArrayList(11);//SIW7941 
            sMedicareNodeOrder.Add(Constants.sStdTechKey);
            sMedicareNodeOrder.Add(Constants.sStdTreeLabel);
            sMedicareNodeOrder.Add(Constants.sCMSEligible);
            sMedicareNodeOrder.Add(Constants.sCMSEffectiveDt);
            sMedicareNodeOrder.Add(Constants.sCMSHICN);
            sMedicareNodeOrder.Add(Constants.sCMSCauseOfInjury);
            sMedicareNodeOrder.Add(Constants.sCMSDiagnosis);
            sMedicareNodeOrder.Add(Constants.sCMSNFLimit);
            sMedicareNodeOrder.Add(Constants.sCMSNFExhaustDt);
            sMedicareNodeOrder.Add(Constants.sCMSPlanNode);
            sMedicareNodeOrder.Add(Constants.sClmComment);//SIW7941 
 
            sPlanNodeOrder = new ArrayList(4);
            sPlanNodeOrder.Add(Constants.sCMSPlanIns);
            sPlanNodeOrder.Add(Constants.sCMSPlanORMInd);
            sPlanNodeOrder.Add(Constants.sCMSPlanORMTermDt);
            sPlanNodeOrder.Add(Constants.sCMSPlanTPOCNode);

            sDiagNodeOrder = new ArrayList(1);
            sDiagNodeOrder.Add(Constants.sInjDiagSpecific);

            sTPOCNodeOrder = new ArrayList(3);
            sTPOCNodeOrder.Add(Constants.sCMSPlanTPOCDt);
            sTPOCNodeOrder.Add(Constants.sCMSPlanTPOCAmt);
            sTPOCNodeOrder.Add(Constants.sCMSPlanTPOCDelayDt);

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "","");
        }


        public XmlNode Create(string sMedicareID,string sMedicareEntityID,string sEligibleCode,string sEffectiveDate,string sHICN,string sNFLimit,string sNFExhaustDate, string sCauseOfInjury,string sCauseOfInjuryCode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

             xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
               
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                MedicareID =sMedicareID;
                EntityID=sMedicareEntityID;

                if (sEligibleCode != "")
                {
                    MedicareEligible = sEligibleCode;

                }
                if (sEffectiveDate != "")
                {
                    EffectiveDate = sEffectiveDate;
                }

                if (sNFLimit != "")
                {
                    NFLimit = sNFLimit;
                }
                if (sHICN != "")
                {
                    HICN_ID = sHICN;
                }

                if (sNFExhaustDate != "")
                {
                    NFExhaustDate = sNFExhaustDate;
                }
                
                InsertInDocument(null, null);

                XML.Blanking = bblanking; 
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLMedicare.Create");
                return null;
            }
        }

        public string EntityID
        {
           get
           {
               string strdata = XML.XMLGetAttributeValue(Node, Constants.sCMSEntId);
               string sEId = "";

               if (strdata != "" && strdata != null)
                   if (StringUtils.Left(strdata, 3) == Constants.sCCPXmlEntityIDPrefix && strdata.Length >= 4)
                       sEId = StringUtils.Right(strdata, strdata.Length - 3);
               return sEId;
           }
            set
            {
                string lid;
                if (value == "" || value == null || value == "0")
                {
                    lEvtId = lEvtId + 1;
                    lid = lEvtId + "";
                }
                else
                    lid = value;
                XML.XMLSetAttributeValue(putNode, Constants.sCMSEntId, Constants.sCCPXmlEntityIDPrefix + lid);
            }

        }
        public XMLEntity ClaimantEntity
        {
            get
            {
                XMLEntity xmlEntity; 
                xmlEntity=new XMLEntity();
                LinkXMLObjects(xmlEntity);
                if(EntityID !="")
                {
                    xmlEntity.getEntitybyID(EntityID);
                }
               
                return xmlEntity;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        } 

        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, sMedicareNodeOrder);
            }
        }

        public string IDPrefix
        {
            get
            {
             return Utils.getData(Node,Constants.sCMSIDPfx);
            }
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sClmMedicare; }
        }
        public string MedicareID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sCMSID);
                string lid = "";

                if (StringUtils.Left(strdata, 3) == Constants.sCMSIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lCmsId = Globals.lCmsId + 1;
                    lid = Globals.lCmsId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sCMSID, Constants.sCMSIDPfx + lid);
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }
        
        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        private void subClearPointers()
        {           
            xmlPlan=null;
            xmlPlanList=null;
            xmlDiagnosisList=null;
            xmlPlanTPOC=null;
            xmlPlanTPOCList=null;
            CommentReference = null;//SIW7941
         }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null)
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);
            }
        }

        //Begin W7214
        //******* Start ******** Medicare Eligible ****************//
        public XmlNode putEntMedEligible(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCMSEligible, sDesc, sCode, sMedicareNodeOrder, scodeid);
        }
        public string EntMedEligible
        {
            get
            {
                return Utils.getData(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSEligible, value, sMedicareNodeOrder);
            }
        }
        public string EntMedEligible_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCMSEligible, value, sMedicareNodeOrder);
            }
        }
        public string EntMedEligible_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCMSEligible, value, sMedicareNodeOrder);
            }
        }
        //******* End ******** Medicare Eligible ****************//
        //End W7214
        
        //** NOTE: DO NOT USE MEDICAREELIGIBLE!!!! USE ENTMEDELIGIBLE
        public string MedicareEligible
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sCMSEligible, "INDICATOR");
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sCMSEligible, "INDICATOR", value, sMedicareNodeOrder);
            }
        }
        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCMSEffectiveDt);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCMSEffectiveDt, value, sMedicareNodeOrder);
            }
        }
        public string NFLimit
        {
            get
            {
                return Utils.getData(Node, Constants.sCMSNFLimit);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSNFLimit, value, sMedicareNodeOrder);
            }
        }
        public string HICN_ID
        {
            get
            {
                return Utils.getData(Node, Constants.sCMSHICN);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSHICN, value, sMedicareNodeOrder);
            }
        }
        public string NFExhaustDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCMSNFExhaustDt);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCMSNFExhaustDt, value, sMedicareNodeOrder);
            }
        }

        //******* Start ******** Cause of Injury ****************//

        public XmlNode putCauseOfInjury(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCMSCauseOfInjury, sDesc, sCode, sMedicareNodeOrder, scodeid);
        }
        public string CauseOfInjury
        {
            get
            {
                 return Utils.getData(Node, Constants.sCMSCauseOfInjury);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSCauseOfInjury, value, sMedicareNodeOrder);
            }          
        }
        public string CauseOfInjury_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCMSCauseOfInjury);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCMSCauseOfInjury, value, sMedicareNodeOrder);
            }
        }
        public string CauseOfInjury_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCMSCauseOfInjury);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCMSCauseOfInjury, value, sMedicareNodeOrder);    
            }
        }
        //******* End ******** Cause of Injury ****************//

        //******* Start ******** Diagnosis ****************//
        public long DiagnosisCount
        {
            get
            {
                return DiagnosisList.Count;
            }
        }
        public XmlNodeList DiagnosisList
        {
            
            get
            {
                if (xmlDiagnosisList == null)
                {

                    return getNodeList(Constants.sCMSDiagnosis, ref xmlDiagnosisList, Node);

                }
                else
                {
                    return xmlDiagnosisList;
                }
            }
        }
        public XmlNode getDiagnosis(bool reset)
        {
            if (null != DiagnosisList)
            {
                return getNode(reset, ref xmlDiagnosisList, ref xmlDiagnosis);
            }
            else
            {
                return null;
            }
        }
        public XmlNode removeDiagnosis()
        {
            if(DiagnosisNode!=null)
            {
                Node.RemoveChild(DiagnosisNode);
            }
            DiagnosisNode=null;
            return DiagnosisNode;
        }
        private XmlNode putDiagnosisNode
        {
            get
            {
                if (DiagnosisNode == null)  
                {
                    DiagnosisNode = XML.XMLaddNode(putNode, Constants.sCMSDiagnosis, sMedicareNodeOrder);
                }
                return DiagnosisNode;
            }
        }
        private XmlNode DiagnosisNode
        {
            get
            {
                //if (xmlDiagnosis == null) //SIW7496
                //xmlDiagnosis = XML.XMLGetNode(Node, Constants.sCMSDiagnosis); //SIW7496
                    return xmlDiagnosis;
             }
            set
            {
                //xmlDiagnosis = null;//SIW7496
                 xmlDiagnosis = value;
            }
        }
        public XmlNode addDiagnosis(string sDiagnosis,string sDiagnosisCode,string scodeid)
    {
        DiagnosisNode =null;
        putDiagnosis(sDiagnosis,sDiagnosisCode,scodeid);
        return DiagnosisNode;

    }
        public XmlNode putDiagnosis(string sDiagnosis,string sDiagnosisCode,string scodeid)
        {
            return Utils.putCodeItem(putDiagnosisNode,Constants.sCMSDiagSpecific,sDiagnosis,sDiagnosisCode,sDiagNodeOrder,scodeid);
        }
        public XmlNode putDiagnosis(string sDiagnosis, string sDiagnosisCode)
        {
            return putDiagnosis(sDiagnosis, sDiagnosisCode, "");
        }
        public string Diagnosis
        {
            get
            {
                return Utils.getData(DiagnosisNode,Constants.sCMSDiagSpecific);
            }
            set
            {
                Utils.putData(putDiagnosisNode, Constants.sCMSDiagSpecific, value, sDiagNodeOrder);
            }
        }
        public string Diagnosis_Code
        {
            get
            {
                return Utils.getCode(DiagnosisNode, Constants.sCMSDiagSpecific);
            }
            set
            {
                Utils.putCode(putDiagnosisNode, Constants.sCMSDiagSpecific, value, sDiagNodeOrder);
            }
        }
        public string Diagnosis_CodeID
        {
            get
            {
                return Utils.getCodeID(DiagnosisNode, Constants.sCMSDiagSpecific);
            }
            set
            {
                Utils.putCodeID(putDiagnosisNode, Constants.sCMSDiagSpecific, value, sDiagNodeOrder);
            }
        }
        //******* End   ******** Diagnosis ****************//
        
        //******* Start ******** Plan *********************//
        public long PlanCount
        {
            get
            {
                return PlanList.Count;
            }
        }
        public XmlNodeList PlanList
        {
            get
            {
                if (xmlPlanList == null)
                {
                  return getNodeList(Constants.sCMSPlanNode, ref xmlPlanList, Node);   
                }
                else
                {
                    return xmlPlanList;
                }
            }
        }
        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }
        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create();
                return xmlThisNode;
            }
        }
        public XmlNode getPlan(bool reset)
        {
            if (null != PlanList)
            {
                return getNode(reset, ref xmlPlanList, ref xmlPlan); 
            }
            else
            {
                return null;
            }
        }
        public XmlNode removePlan()
        {
            if(PlanNode!=null)
            {
                Node.RemoveChild(PlanNode);
                xmlPlanTPOC=null;
            }
            PlanNode=null;
            return PlanNode;
        }
        private XmlNode putPlanNode
        {
            get
            {
            if(PlanNode==null)
            {
                PlanNode=Utils.XML.XMLaddNode(putNode,Constants.sCMSPlanNode,sMedicareNodeOrder);
                xmlPlanTPOC =null;
            }
            return PlanNode;
            }
        }
        private XmlNode PlanNode
        {
            get
            {
                return xmlPlan;
            }
            set
            {
                xmlPlan =null;
                xmlPlanTPOC=null;
                xmlPlan=value;
            } 
        }
        public string PlanSeq
        {
            get
            {
                string strdata;
                strdata = Utils.XML.XMLGetAttributeValue(PlanNode, Constants.sCMSPlanSeq);
             
                return strdata;
            }
            set
            {
                 Utils.XML.XMLSetAttributeValue(putPlanNode, Constants.sCMSPlanSeq, value);
            }
        }
        public XmlNode addPlan()
        {
            PlanNode =null;
            return PlanNode;
        }
        public XmlNode putPlan()
        {
           return putPlanNode;
        }
        public XmlNode putPlanInsType(string sDesc,string sCode,string scodeid)
        {
            return Utils.putCodeItem(putPlanNode ,Constants.sCMSPlanIns,sDesc,sCode,sPlanNodeOrder,scodeid);
        }
        public string PlanInsType
        {
            get
            {
                return Utils.getData(PlanNode,Constants.sCMSPlanIns);
            }
            set
            {
                Utils.putData(putPlanNode,Constants.sCMSPlanIns,value,sPlanNodeOrder);
            }
        }
        public string PlanInsType_Code
        {
            get
            {
                return Utils.getCode(PlanNode,Constants.sCMSPlanIns);
            }
            set
            {
                Utils.putCode(putPlanNode,Constants.sCMSPlanIns,value,sPlanNodeOrder);
            }
        }
        public string PlanInsType_CodeID
        {
            get
            {
                return Utils.getCodeID(PlanNode,Constants.sCMSPlanIns);
            }
            set
            {
                Utils.putCodeID(putPlanNode,Constants.sCMSPlanIns,value,sPlanNodeOrder);
            }
        }
        public string PlanORMInd
        {
           get
           {
               return Utils.getAttribute(PlanNode, Constants.sCMSPlanORMInd, "INDICATOR");
           }
            set
            {
                Utils.putAttribute(putPlanNode, Constants.sCMSPlanORMInd, "INDICATOR", value, sPlanNodeOrder);
            }
        }
        public string PlanORMTermDt
        {
            get
            {
                return Utils.getDate(PlanNode,Constants.sCMSPlanORMTermDt);
            }
            set
            {
                Utils.putDate(putPlanNode,Constants.sCMSPlanORMTermDt,value,sPlanNodeOrder);
            }
        }
    //******* End ********** Plan *********************//

    //******* Start ******* TPOC **********************//
      
        public long TPOCCount
        {
            get
            {
                return TPOCList.Count;
            }
          
        }
        public XmlNodeList TPOCList
        {
            get
            {
                if (xmlPlanTPOCList == null)
                {
                    return getNodeList(Constants.sCMSPlanTPOCNode, ref xmlPlanTPOCList, PlanNode);
                }
                else
                {
                    return xmlPlanTPOCList;
                }
            }
        }
        public XmlNode getTPOC(Boolean reset)
        {
            if (null != TPOCList)
            {
                return getNode(reset, ref xmlPlanTPOCList, ref xmlPlanTPOC);
            }
            else
            {
                return null;
            }
          
        }
        public XmlNode removeTPOC()
        {
            if(TPOCNode!=null)
            {
                Node.RemoveChild(TPOCNode);
                xmlPlanTPOC = null;
            }
            TPOCNode=null;
            return TPOCNode;
        }
        private XmlNode putTPOCNode
        {
            get
            {
                if (TPOCNode == null)
                {
                    TPOCNode = Utils.XML.XMLaddNode(putPlanNode, Constants.sCMSPlanTPOCNode, sPlanNodeOrder);
                }
                return TPOCNode;
            }
        }
        private XmlNode TPOCNode
        {
            get
            {
                return xmlPlanTPOC;
            }
            set
            {
                xmlPlanTPOC=null;
                xmlPlanTPOC=value;
            }
        }
        public string TPOCSeq
        {
           get
           {
               string strdata;
               strdata=Utils.XML.XMLGetAttributeValue(TPOCNode,Constants.sCMSPlanTPOCSeq);
               return strdata;
           }
            set
            {
               Utils.XML.XMLSetAttributeValue(putTPOCNode, Constants.sCMSPlanTPOCSeq, value);
            }
        }
        public XmlNode addTPOC()
        {
            TPOCNode =null;
            return TPOCNode;
        }
        public XmlNode putTPOC()
        {
            return putTPOCNode;
        }
        public string TPOCDt
        {
            get
            {
                return Utils.getDate(TPOCNode,Constants.sCMSPlanTPOCDt);
            }
            set
            {
                Utils.putDate(putTPOCNode,Constants.sCMSPlanTPOCDt,value,sTPOCNodeOrder);
            }
        }
        public string TPOCAmount
        {
            get
            {
                return Utils.getData(TPOCNode,Constants.sCMSPlanTPOCAmt);
            }
            set
            {
                Utils.putData(putTPOCNode,Constants.sCMSPlanTPOCAmt,value,sTPOCNodeOrder);
            }
        }
        public string TPOCDelayDt
        {
            get
            {
                return Utils.getDate(TPOCNode,Constants.sCMSPlanTPOCDelayDt);
            }
            set
            {
                Utils.putDate(putTPOCNode,Constants.sCMSPlanTPOCDelayDt,value,sTPOCNodeOrder);
            }
        }

    //******* End   ******* TPOC **********************//
      public XMLVarDataItem DataItem
        {
            get
            {
                if(m_DataItem ==null)
                {
                    m_DataItem=new  XMLVarDataItem();
                    m_DataItem.Parent=this;
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem=value;
            }
        }
      //Start SIN8355
      //public XMLPartyInvolved PartyInvolved
      //  {
      //      set
      //      {
      //          m_PartyInvolved =null;
      //          m_PartyInvolved=value;
      //          if(m_PartyInvolved!=null)
      //          {
      //          ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder); 
      //          }
      //      }
      //  }
      public XMLPartyInvolved PartyInvolved
      {
          get
          {
              if (m_PartyInvolved == null)
              {
                  m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);
                  m_PartyInvolved.getFirst();
              }
              return m_PartyInvolved;
          }
          set
          {
              m_PartyInvolved = value;
          }
      }

      //End SIN8355
      public override XmlNode Parent_Node
      {
          get
          {
              XmlNode XMLNode;
              XMLNode=m_xmlMedicare;
              m_xmlMedicare =XMLNode;
              XMLNode =null;
              return Parent.putNode;
          }
      }


      public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
      public override ArrayList  NodeOrder
       {
	    get 
	    { 
            return Parent.NodeOrder;
        }
       }
      public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
      public XmlNode getMedicare(bool reset)
       {
        return getNode(reset);
       }     
      public XmlNode  getMedicarebyID(string sMedicareID)
        {
            Boolean rst;
            rst=true;
            while(getMedicare(rst)!=null)
            {
                rst=false;
                if(MedicareID==sMedicareID)
                {
                    break;
                }
            }
           return  m_xmlMedicare;
        }
      public XmlNode getMedicareByEntity(string sEntID)
        {
           bool rst=true;
           while(getMedicare(rst)!=null)
            {
                rst=false;
               if(EntityID==sEntID)
                   break;
            }
           return Node;
       }


      //Start SIW7941
      public XMLCommentReference CommentReference
      {
          get
          {
              if (m_Comment == null)
              {
                  m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);
                  m_Comment.getFirst();
              }
              return m_Comment;
          }
          set
          {
              m_Comment = value;
          }
      }
        //End SIW7941
    }
}

//End SIW7214
