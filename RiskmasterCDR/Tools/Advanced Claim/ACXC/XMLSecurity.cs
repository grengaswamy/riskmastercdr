/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 04/16/2008 | SI06321 | JTC        | Added user list request for diary
 * 06/06/2008 | SI06320   |    NAB     | Add Support DB Connection String
 * 07/24/2008 | SI06321 | JTC        | Added user id
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 03/16/2011 | SIW7049  |   AS      | To assign a diary module to a module security group. 
 * 08/08/2011 | SIW7332 |   Vineet   | Diary Peek
 * 08/10/2011 | SIW7341  |   AL      | Ability to attach Reserve/Payment to Diary.
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLSecurity : XMLXCNodeWithList
    {
        /******************************************************************************************
        ' The XMLSecurity class provides the functionality to support and manage a Security Node
        '******************************************************************************************
        '<SECURITY>
	    '   <GROUPS COUNT=nn NEXTID= nn>
		'       <GROUP ID=xx />	'xx represents the actual groupID value
		'       ...GROUP will repeat as the user may belong to several groups (future dev)
        '       <SUPP_DB_CONN_STRING>Support DB Connection string (encrypted)</SUPP_DB_CONN_STRING>  'SI06320
	    '   </GROUPS>
        '</SECURITY>
        '
        '<SECURITY>
	    '   <DSNS COUNT=nn>
		'       <DSN NAME=xx />	'xx represents the actual DSN name
		'       ...DSN will repeat as there may be multiple DSNs
	    '   </DSNS>
        '</SECURITY>
        '
        'Start SI06321
        '<SECURITY>
        '   <USERS COUNT=nn>
        '       <USER LOGIN="..." ID="...">
        '           <NAME FIRST="..." LAST="..." />
        '       </USER>
        '       ...USER repeats for every user for the current db
        '   </USERS>
        '</SECURITY>
        'End SI06321
        '******************************************************************************************/

        private XmlNode m_xmlGroup;
        private XmlNode m_xmlGroups;
        private XmlNodeList m_xmlGroupList;
        private ArrayList sGroupNodeOrder;
        private XmlNode m_xmlSptConnString;  //SI06320

        private XmlNode m_xmlDSN;
        private XmlNode m_xmlDSNs;
        private XmlNodeList m_xmlDSNList;
        //SIW529 private ArrayList sDSNNodeOrder;

        //Start SI06321
        private XmlNode m_xmlUser;
        private XmlNode m_xmlUsers;
        private XmlNodeList m_xmlUserList;
        //End SI06321
        
        public XMLSecurity()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sSecGroups);
            sThisNodeOrder.Add(Constants.sSecDSNs);     //SI06321
            sThisNodeOrder.Add(Constants.sSecUsers);    //SI06321

            sGroupNodeOrder = new ArrayList();
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sSecSecurity; }
        }

        public override XmlNode Create()
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                //SI06321 Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDiary.Create");
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSecurity.Create");      //SI06321
                return null;
            }
        }

        public string GroupIDPrefix
        {
            get
            {
                return Constants.sSecGroupIDPfx;
            }
        }

        public XmlNode addGroup()
        {
            GroupNode = null;
            GroupCount = GroupCount + 1;
            return putGroupNode();
        }

        public XmlNode addGroup(string sGroupID)
        {
            GroupNode = null;
            GroupCount = GroupCount + 1;
            return putGroupNode(sGroupID);
        }

        //Begin SIW7049
        public XmlNode addGroup(string sGroupID, string sGroupName)
        {
            GroupNode = null;
            GroupCount = GroupCount + 1;
            return putGroupNode(sGroupID,sGroupName);
        }
        //End SIW7049
        public XmlNode addDSN(string sDSNID)
        {
            DSNNode = null;
            DSNCount = DSNCount + 1;
            return putDSNNode(sDSNID);
        }
        
        public XmlNode putGroupsNode
        {
            get
            {
                if (GroupsNode == null)
                {
                    GroupsNode = XML.XMLaddNode(putNode, Constants.sSecGroups, NodeOrder);
                    NextGroupID = "1";
                    GroupCount = 0;
                }
                return GroupsNode;
            }
        }

        public XmlNode putDSNsNode
        {
            get
            {
                if (DSNsNode == null)
                {
                    DSNsNode = XML.XMLaddNode(putNode, Constants.sSecDSNs, NodeOrder);
                    //NextDSNID = "1";
                    DSNCount = 0;
                }
                return DSNsNode;
            }
        }

        public XmlNode GroupsNode
        {
            get
            {
                if (m_xmlGroups == null) //SIW529
                    m_xmlGroups = XML.XMLGetNode(Node, Constants.sSecGroups);   //SIW529
                return m_xmlGroups;
            }
            set
            {
                m_xmlGroups = value;
            }
        }

        public XmlNode DSNsNode
        {
            get
            {
                if (m_xmlDSNs == null)  //SIW529
                    m_xmlDSNs = XML.XMLGetNode(Node, Constants.sSecDSNs);   //SIW529
                return m_xmlDSNs;
            }
            set
            {
                m_xmlDSNs = value;
            }
        }

        public XmlNodeList GroupList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlGroupList)
                {
                    //SIW7049 return getNodeList(Constants.sSecGroup, ref m_xmlGroupList, m_xmlGroups);
                    return getNodeList(Constants.sSecGroup, ref m_xmlGroupList, GroupsNode);    //SIW7049
                }
                else
                {
                    return m_xmlGroupList;
                }
                //End SIW163
            }
        }

        public XmlNodeList DSNList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlDSNList)
                {
                    //SIW529 return getNodeList(Constants.sSecDSN, ref m_xmlDSNList, m_xmlDSNs);
                    return getNodeList(Constants.sSecDSN, ref m_xmlDSNList, DSNsNode);  //SIW529 
                }
                else
                {
                    return m_xmlDSNList;
                }
                //End SIW163
            }
        }

        //Start SIW529
        public XmlNode getDSN(bool reset)
        {
            object o = DSNList;
            return getNode(reset, ref m_xmlDSNList, ref m_xmlDSN);
        }
        //End SIW529

        public XmlNode putGroupNode()
        {
            if (GroupNode == null)
                //SIW485 GroupNode = XML.XMLaddNode(putGroupsNode, Constants.sSecGroup, Globals.sNodeOrder);
                GroupNode = XML.XMLaddNode(putGroupsNode, Constants.sSecGroup, Utils.sNodeOrder);	//SIW485
            return GroupNode;

        }

        public XmlNode putGroupNode(string sGroupId)
        {
            if (GroupNode == null)
                {
                //SIW485 GroupNode = XML.XMLaddNode(putGroupsNode, Constants.sSecGroup, Globals.sNodeOrder);
                GroupNode = XML.XMLaddNode(putGroupsNode, Constants.sSecGroup, Utils.sNodeOrder);	//SIW485
                GroupID = sGroupId;
                }
            return GroupNode;
        }
        //Begin SIW7049
        public XmlNode putGroupNode(string sGroupId, string sGroupName)
        {
            if (GroupNode == null)
            {
                GroupNode = XML.XMLaddNode(putGroupsNode, Constants.sSecGroup, Utils.sNodeOrder);
                GroupID = sGroupId;
                GroupName = sGroupName;
            }
            return GroupNode;
        }
        //End SIW7049
        public XmlNode putDSNNode()
        {
            if (DSNNode == null)
                //SIW485 DSNNode = XML.XMLaddNode(putDSNsNode, Constants.sSecDSN, Globals.sNodeOrder);
                DSNNode = XML.XMLaddNode(putDSNsNode, Constants.sSecDSN, Utils.sNodeOrder);	//SIW485
            return DSNNode;

        }

        public XmlNode putDSNNode(string sDSNId)
        {
            if (DSNNode == null)
            {
                //SIW485 DSNNode = XML.XMLaddNode(putDSNsNode, Constants.sSecDSN, Globals.sNodeOrder);
                DSNNode = XML.XMLaddNode(putDSNsNode, Constants.sSecDSN, Utils.sNodeOrder);	//SIW485
                //DSNID = sDSNId;
                DSNName = sDSNId;
            }
            return DSNNode;
        }        

        public XmlNode GroupNode
        {
            get
            {
                return m_xmlGroup;
            }
            set
            {
                m_xmlGroup = value;
            }
        }

        public XmlNode DSNNode
        {
            get
            {
                return m_xmlDSN;
            }
            set
            {
                m_xmlDSN = value;
            }
        }

        public int GroupCount
        {
            get
            {
                string strdata = Utils.getAttribute(GroupsNode, "", Constants.sSecGroupCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putGroupsNode, "", Constants.sSecGroupCount, value + "", sGroupNodeOrder);
            }
        }

        public int DSNCount
        {
            get
            {
                string strdata = Utils.getAttribute(DSNsNode, "", Constants.sSecDSNCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                //SIW529 Utils.putAttribute(putDSNsNode, "", Constants.sSecDSNCount, value + "", sDSNNodeOrder);
                Utils.putAttribute(putDSNsNode, "", Constants.sSecDSNCount, value + "", Utils.sNodeOrder); //SIW529
            }
        }

        public string NextGroupID
        {
            get
            {
                return Utils.getAttribute(GroupsNode, "", Constants.sSecGroupNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putGroupsNode, "", Constants.sSecGroupNextID, value, sGroupNodeOrder);
            }
        }

        public string extGroupID(string strdata)
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sSecGroupIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string GroupID
        {
            get
            {
                string strdata;
                string lid = "";
                if (GroupNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(GroupNode, Constants.sSecGroupID);
                    lid = extGroupID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = getNextGroupID(null);
                XML.XMLSetAttributeValue(putGroupNode(), Constants.sSecGroupID, Constants.sSecGroupIDPfx + value);
            }
        }
        //Begin SIW7049
        public string GroupName
        {
            get
            {
                string strdata;
                string sname = "";
                if (GroupNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(GroupNode, Constants.sSecGroupName);
                    sname = strdata;
                }
                return sname;
            }
            set
            {
                XML.XMLSetAttributeValue(putGroupNode(), Constants.sSecGroupName, value);
            }
        }
        //End SIW7049

        public string DSNName
        {
            get
            {
                return XML.XMLGetAttributeValue(DSNNode, Constants.sSecDSNName);
            }
            set
            {
                XML.XMLSetAttributeValue(putDSNNode(), Constants.sSecDSNName, value);
            }
        }

        public string getNextGroupID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextGroupID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextGroupID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        //Start SI06321
        public XmlNode addUser()
        {
            UserNode = null;
            UserCount = UserCount + 1;
            return putUserNode;
        }

        public XmlNode putUsersNode
        {
            get
            {
                if (UsersNode == null)
                {
                    UsersNode = XML.XMLaddNode(putNode, Constants.sSecUsers, NodeOrder);
                    UserCount = 0;
                }
                return UsersNode;
            }
        }

        public XmlNode UsersNode
        {
            get
            {
                if (m_xmlUsers == null)
                    m_xmlUsers = XML.XMLGetNode(Node, Constants.sSecUsers);
                return m_xmlUsers;
            }
            set
            {
                m_xmlUsers = value;
            }
        }

        public XmlNodeList UserList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlUserList)
                {
                    return getNodeList(Constants.sSecUser, ref m_xmlUserList, UsersNode);
                }
                else
                {
                    return m_xmlUserList;
                }
                //Wnd SIW163
            }
        }

        public XmlNode putUserNode
        {
            get
            {
                if (UserNode == null)
                    //SIW485 UserNode = XML.XMLaddNode(putUsersNode, Constants.sSecUser, Globals.sNodeOrder);
                    UserNode = XML.XMLaddNode(putUsersNode, Constants.sSecUser, Utils.sNodeOrder);	//SIW485
                return UserNode;
            }
        }

        public XmlNode UserNode
        {
            get
            {
                return m_xmlUser;
            }
            set
            {
                m_xmlUser = value;
            }
        }

        public int UserCount
        {
            get
            {
                string strdata = Utils.getAttribute(UsersNode, "", Constants.sSecUserCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                //SIW485 Utils.putAttribute(putUsersNode, "", Constants.sSecUserCount, value + "", Globals.sNodeOrder);
                Utils.putAttribute(putUsersNode, "", Constants.sSecUserCount, value + "", Utils.sNodeOrder);	//SIW485
            }
        }

        public XmlNode putUser(string sLogin, string sFirst, string sLast, string sID)
        {
            if (sLogin != "" && sLogin != null)
                UserLogin = sLogin;
            if (sFirst != "" && sFirst != null)
                UserNameFirst = sFirst;
            if (sLast != "" && sLast != null)
                UserNameLast = sLast;
            if (sID != "" && sID != null)
                UserID = sID;
            return UserNode;
        }
		//Start SIW7332 // Start SIW7341
		public XmlNode putUser(string sLogin, string sFirst, string sLast, string sID,string sManagerID, string sEmailAddr)
        {
            if (sLogin != "" && sLogin != null)
                UserLogin = sLogin;
            if (sFirst != "" && sFirst != null)
                UserNameFirst = sFirst;
            if (sLast != "" && sLast != null)
                UserNameLast = sLast;
			if (sEmailAddr != "" && sEmailAddr != null)
				UserEmailAddr = sEmailAddr;
            if (sID != "" && sID != null)
                UserID = sID;
            if (sManagerID != "" && sManagerID != null)
                ManagerID = sManagerID;
            return UserNode;
        }

        public string ManagerID
        {
            get
            {
                return Utils.getAttribute(UserNode, "", Constants.sSecManagerID);
            }
            set
            {
                
                Utils.putAttribute(putUserNode, "", Constants.sSecManagerID, value, Utils.sNodeOrder);	//SIW485
            }
        }

		//End SIW7332 // End SIW7341
        public string UserLogin
        {
            get
            {
                return Utils.getAttribute(UserNode, "", Constants.sSecUserLogin);
            }
            set
            {
                //SIW485 Utils.putAttribute(putUserNode, "", Constants.sSecUserLogin, value, Globals.sNodeOrder);
                Utils.putAttribute(putUserNode, "", Constants.sSecUserLogin, value, Utils.sNodeOrder);	//SIW485
            }
        }

        public string UserID
        {
            get
            {
                return Utils.getAttribute(UserNode, "", Constants.sSecUserID);
            }
            set
            {
                //SIW485 Utils.putAttribute(putUserNode, "", Constants.sSecUserID, value, Globals.sNodeOrder);
                Utils.putAttribute(putUserNode, "", Constants.sSecUserID, value, Utils.sNodeOrder);	//SIW485
            }
        }

        public string UserNameFirst
        {
            get
            {
                return Utils.getAttribute(UserNode, Constants.sSecUserName, Constants.sSecUserNameFirst);
            }
            set
            {
                //SIW485 Utils.putAttribute(putUserNode, Constants.sSecUserName, Constants.sSecUserNameFirst, value, Globals.sNodeOrder);
                Utils.putAttribute(putUserNode, Constants.sSecUserName, Constants.sSecUserNameFirst, value, Utils.sNodeOrder);	//SIW485
            }
        }

        public string UserNameLast
        {
            get
            {
                return Utils.getAttribute(UserNode, Constants.sSecUserName, Constants.sSecUserNameLast);
            }
            set
            {
                //SIW485 Utils.putAttribute(putUserNode, Constants.sSecUserName, Constants.sSecUserNameLast, value, Globals.sNodeOrder);
                Utils.putAttribute(putUserNode, Constants.sSecUserName, Constants.sSecUserNameLast, value, Utils.sNodeOrder);	//SIW485
            }
        }
        // Start SIW7341 
        public string UserEmailAddr
        {
            get
            {
                return Utils.getAttribute(UserNode, "", Constants.sSecUserEmailAddr);
            }
            set
            {
                Utils.putAttribute(putUserNode, "", Constants.sSecUserEmailAddr, value, Utils.sNodeOrder);	
            }
        }
        // End SIW7341 

        //Begin SIW7049
        public XmlNode getGroup(bool reset)
        {
            object o = GroupList;
            return getNode(reset, ref m_xmlGroupList, ref m_xmlGroup);
        }
        //End SIW7049
        public XmlNode getUser(bool reset)
        {
            object o = UserList;
            return getNode(reset, ref m_xmlUserList, ref m_xmlUser);
        }

        public XmlNode getUserbyName(string sFirst, string sLast)
        {
            if (UserNameFirst != sFirst && UserNameLast != sLast)
            {
                getUser(true);
                while (UserNode != null)
                {
                    if (UserNameFirst == sFirst && UserNameLast == sLast)
                        break;
                    getUser(false);
                }
            }
            return UserNode;
        }

        public XmlNode getUserbyID(string sID)
        {
            if (UserID != sID)
            {
                getUser(true);
                while (UserNode != null)
                {
                    if (UserID == sID)
                        break;
                    getUser(false);
                }
            }
            return UserNode;
        }

        private void subClearPointers()
        {
            m_xmlDSN = null;
            m_xmlDSNList = null;
            m_xmlDSNs = null;
            m_xmlGroup = null;
            m_xmlGroupList = null;
            m_xmlGroups = null;
            m_xmlUser = null;
            m_xmlUserList = null;
            m_xmlUsers = null;
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLGetNode(Parent.Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
                ResetParent();
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
        //End SI06321
        //Start SI06320
        public XmlNode addSuppDBConnString(string sConnString)
        {
            SuppDBConnStrNode = null;
            return putSuppDBConnStringNode(sConnString);
        }
        public XmlNode putSuppDBConnStringNode(string sConnString)
        {
            if (SuppDBConnStrNode == null)
            {
                //SIW485 SuppDBConnStrNode = XML.XMLaddNode(putGroupsNode, Constants.sSecSuppDBConnString, Globals.sNodeOrder);
                SuppDBConnStrNode = XML.XMLaddNode(putGroupsNode, Constants.sSecSuppDBConnString, Utils.sNodeOrder);	//SIW485
                SuppDBConnStrValue = sConnString;
            }
            return SuppDBConnStrNode;
        }
        public XmlNode putSuppDBConnStringNode()
        {
            if (SuppDBConnStrNode == null)
                //SIW485 SuppDBConnStrNode = XML.XMLaddNode(putGroupsNode, Constants.sSecSuppDBConnString, Globals.sNodeOrder);
                SuppDBConnStrNode = XML.XMLaddNode(putGroupsNode, Constants.sSecSuppDBConnString, Utils.sNodeOrder);	//SIW485
            return SuppDBConnStrNode;

        }
        public XmlNode SuppDBConnStrNode           
        {
            get
            {
                return m_xmlSptConnString;
            }
            set
            {
                m_xmlSptConnString = value;
            }
        }
        public string SuppDBConnStrValue
        {
            get
            {
                return XML.XMLGetAttributeValue(SuppDBConnStrNode, Constants.sSecSuppDBConnStringValue);
            }
            set
            {
                XML.XMLSetAttributeValue(putSuppDBConnStringNode(), Constants.sSecSuppDBConnStringValue, value);
            }
        }
        //End SI06320
    }
}
