/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 05/13/2011 | SIW7123 |    AS      | Policy Limits and Deductibles
 * 06/11/2011 | SIW7206 |   Lau      | Add Legacy Key
 * 05/24/2012 | SIN8529 |    PV      | Implemented Organization Hierarchy
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLPolicy : XMLXCNodeWithList, XMLXCIntEndorsement, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        ' The cXMLPolicy class provides the functionality to support and manage a
        ' ClaimsPro Policy Node
        '*****************************************************************************
        
        '<CLAIM>
        '   <POLICY POLICY_ID="POL143" VERIFIED="boolean" PRIMARY_POLICY="boolean">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>       '(SI06023 - Implemented in SI06333) 'SIW360
        '      <TREE_LABEL>Tree Description</TREE_LABEL>
        '      <POLICY_NUMBER LOCATION="00" MODULE="01" SYMBOL="WCV">0003290</POLICY_NUMBER>
        '      <MASTER_COMPANY CODE="05">MYND</MASTER_COMPANY>
        '      <EFFECTIVE_DATE YEAR="2001" MONTH="12" DAY="01" />
        '      <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <RETRO_DATE YEAR="2001" MONTH="12" DAY="01" />                 'SI04652
        '      <TAIL_DATE YEAR="2001" MONTH="12" DAY="01" />                  'SI04652
        '      <CANCELLATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <LINE_OF_BUSINESS CODE="WCV">Workers Comp Voluntary           </LINE_OF_BUSINESS>
        '      <BRANCH CODE="00"></BRANCH>
        '      <SUB-AGENT>dskdj</SUB_AGENT>
        '      <REINSURANCE ASSIGNED="boolean"/>
        '      <LEGACY_KEY>Policy Key from the host system</LEGACY_KEY> 'SIN7206
        '      <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '      <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->        
        '      <INSURED_UNIT> ....  </INSURED_UNIT>
        '        <!-- Multiple Units -->
        '      <ENDORSEMENT> ... <ENDORSEMENT>
        '        <!-- Multiple Endorsements -->
        '      <LIMIT_DEDUCTIBLE> ...... </LIMIT_DEDUCTIBLE>                    'SIW7123
        '      <!-- Multiple Policy limits and Policy Deductibles -->
        '      <PARTY_INVOLVED ENTITY_ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE COMMENT_ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DATA_ITEM> ... </DATA_ITEM>                   'SI04920        
        '      <ISSUE_SYSTEM_ID CODE="xx">Issuing System</ISSUE_SYSTEM_ID>  '(SI06232 - Implemented in SI-6333)
        '      <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '   </POLICY>
        '</CLAIM>*/

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLClaim m_Claim;
        private XMLInsUnit m_Unit;
        private XMLLimit m_Limit;
        private XMLDeductible m_Deductible;
        private XMLEndorsement m_Endorsement;
        private XMLPolLimDed m_PolLimDed;       //SIW7123
        private XMLVarDataItem m_DataItem;
        
        public XMLPolicy()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(19);               //(SI06023,SI06232 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(21);                 //(SI06023,SI06232 - Implemented in SI06333)
            //SIW7123      sThisNodeOrder = new ArrayList(22);  //SIW490
            //sThisNodeOrder = new ArrayList(24);     //SIN7206 //SIW7123//SIN8529
            sThisNodeOrder = new ArrayList(29);  //SIN8529
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);//SIW490
            sThisNodeOrder.Add(Constants.sPolNumber);
            sThisNodeOrder.Add(Constants.sPolMasterCo);
            sThisNodeOrder.Add(Constants.sPolEffDate);
            sThisNodeOrder.Add(Constants.sPolExpDate);
            sThisNodeOrder.Add(Constants.sPolRetroDate);
            sThisNodeOrder.Add(Constants.sPolTailDate);
            sThisNodeOrder.Add(Constants.sPolCanDate);
            sThisNodeOrder.Add(Constants.sPolLOB);
            sThisNodeOrder.Add(Constants.sPolBranch);
            sThisNodeOrder.Add(Constants.sPolAgentNumber);
            sThisNodeOrder.Add(Constants.sPolSubAgentNumber);
            sThisNodeOrder.Add(Constants.sPolReinsurance);
            sThisNodeOrder.Add(Constants.sPolLegacyKey);        //SIN7206
            sThisNodeOrder.Add(Constants.sPolLimit);
            sThisNodeOrder.Add(Constants.sPolDeductible);
            sThisNodeOrder.Add(Constants.sPolUnit);
            sThisNodeOrder.Add(Constants.sPolEndorsement);
            sThisNodeOrder.Add(Constants.sPolLimDed);               //SIW7123
            sThisNodeOrder.Add(Constants.sPolPartyInvolved);
            sThisNodeOrder.Add(Constants.sPolComment);
            sThisNodeOrder.Add(Constants.sPolDataItem);
            //sThisNodeOrder.Add(Constants.sPolTechKey);              //(SI06023 - Implemented in SI06333) //SIW360
            sThisNodeOrder.Add(Constants.sPolIssueSystemId);        //(SI06232 - Implemented in SI06333)
            sThisNodeOrder.Add(Constants.sPolOrgDept);  //SIN8529

            sThisNodeOrder.Add(Constants.sPolDttmAdded);
            sThisNodeOrder.Add(Constants.sPolDttmUpdated);
            sThisNodeOrder.Add(Constants.sPolPreparedByUser);
            sThisNodeOrder.Add(Constants.sPolUpdatedByUser);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sPolNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sID, string spolnumber, string smodule, string ssymbol, string sLocation,
                              string smco, string smcocode, string sLOB, string sLOBCode, string seffdte,
                              string sexpdte, string sbranch, string sbranchcode, string sagentnumber)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                PolicyID = sID;
                PolicyNumber = spolnumber;
                Symbol = ssymbol;
                Location = sLocation;
                Module = smodule;
                EffectiveDate = seffdte;
                ExpirationDate = sexpdte;
                putLOB(sLOB, sLOBCode);
                putMasterCompany(smco, smcocode);
                putBranch(sbranch, sbranchcode);
                AgentNumber = sagentnumber;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLPolicy.Create");
                return null;
            }
        }

        public string PolicyNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sPolNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolNumber, value, sThisNodeOrder);
            }
        }

        public string Symbol
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sPolNumber, Constants.sPolSymbol);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sPolNumber, Constants.sPolSymbol, value, sThisNodeOrder);
            }
        }

        public string Module
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sPolNumber, Constants.sPolModule);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sPolNumber, Constants.sPolModule, value, sThisNodeOrder);
            }
        }

        public string Location
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sPolNumber, Constants.sPolLocation);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sPolNumber, Constants.sPolLocation, value, sThisNodeOrder);
            }
        }

        public string AgentNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sPolAgentNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolAgentNumber, value, sThisNodeOrder);
            }
        }

        public string SubAgentNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sPolSubAgentNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolSubAgentNumber, value, sThisNodeOrder);
            }
        }

        public string ReinsuranceAssigned
        {
            get
            {
                return Utils.getBool(Node, Constants.sPolReinsurance, Constants.sPolReinAssigned);
            }
            set
            {
                Utils.putBool(putNode, Constants.sPolReinsurance, Constants.sPolReinAssigned, value, sThisNodeOrder);
            }
        }
        //SIN7206 Start 
        public string LegacyKey
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLegacyKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolLegacyKey, value, sThisNodeOrder);
            }
        }
        //SIN7206 End 
        //Start SIN8529
        public string OrgDeptEID
        {
            get
            {

                return Utils.getEntityRef(Node, Constants.sPolOrgDept);
            }
            set
            {

                Utils.putEntityRef(putNode, Constants.sPolOrgDept, value, NodeOrder);
            }
        }
        public string OrgDeptEntDisplayName
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sPolOrgDept, Constants.sEntDisplayName);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sPolOrgDept, Constants.sEntDisplayName, value, NodeOrder);
            }
        }
        //End SIN8529
        public string Verified
        {
            get
            {
                return XML.XMLExtractBooleanAttribute(Node, Constants.sPolVerified);
            }
            set
            {
                XML.XMLInsertBoolean_Node(putNode, Constants.sPolVerified, value);
            }
        }

        public string PrimaryPolicyInd
        {
            get
            {
                return XML.XMLExtractBooleanAttribute(Node, Constants.sPolPrimary);
            }
            set
            {
                XML.XMLInsertBoolean_Node(putNode, Constants.sPolPrimary, value);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }
        // Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        // End SIW490
        //Start SIW139
        public XmlNode putIssueSystemID(string sDesc, string sCode)
        {
            return putIssueSystemID(sDesc, sCode, "");
        }

        public XmlNode putIssueSystemID(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolIssueSystemId, sDesc, sCode, sThisNodeOrder, scodeid);
        }
        public string IssueSystemID
        {
            get
            {
                return Utils.getData(Node, Constants.sPolIssueSystemId);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolIssueSystemId, value, NodeOrder);
            }
        }

        public string IssueSystemID_Code 
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolIssueSystemId);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolIssueSystemId, value, sThisNodeOrder);
            }
        }//End (SI06232 - Implemented in SI06333)

        public string IssueSystemID_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolIssueSystemId);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolIssueSystemId, value, sThisNodeOrder);
            }
        }
        //End SIW139

        public string IDPrefix
        {
            get
            {
                return Constants.sPolIDPfx;
            }
        }

        public string PolicyID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sPolID);
                string lid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sPolIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lPolId = Globals.lPolId + 1;
                    lid = Globals.lPolId + "";
                }
                if (Node != null)
                    XML.XMLSetAttributeValue(putNode, Constants.sPolID, Constants.sPolIDPfx + (lid + ""));
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.Policy = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null) //SIW529
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Claim != null)
                m_Claim.Policy = this;
            Node = null;
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            Unit = null;
            Limit = null;
            Deductible = null;
            Endorsement = null;
            DataItem = null;
            PolLimDed = null; //SIW7123
        }

        //Start SIW139
        public XmlNode putMasterCompany(string sDesc, string sCode)
        {
            return putMasterCompany(sDesc, sCode, "");
        }

        public XmlNode putMasterCompany(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolMasterCo, sDesc, sCode, sThisNodeOrder, scodeid);
        }

        public string MasterCompany
        {
            get
            {
                return Utils.getData(Node, Constants.sPolMasterCo);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolMasterCo, value, sThisNodeOrder);
            }
        }

        public string MasterCompany_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolMasterCo);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolMasterCo, value, sThisNodeOrder);
            }
        }

        public string MasterCompany_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolMasterCo);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolMasterCo, value, sThisNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putLOB(string sDesc, string sCode)
        {
            return putLOB(sDesc, sCode, "");
        }



        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sPolPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sPolPreparedByUser, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sPolUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sPolUpdatedByUser, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sPolDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sPolDttmUpdated, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sPolDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sPolDttmAdded, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }




        public XmlNode putLOB(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolLOB, sDesc, sCode, sThisNodeOrder, scodeid);
        }

        public string LOB
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLOB);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolLOB, value, sThisNodeOrder);
            }
        }

        public string LOB_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolLOB);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolLOB, value, sThisNodeOrder);
            }
        }

        public string LOB_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolLOB);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolLOB, value, sThisNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putBranch(string sDesc, string sCode)
        {
            return putBranch(sDesc, sCode, "");
        }

        public XmlNode putBranch(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolBranch, sDesc, sCode, sThisNodeOrder, scodeid);
        }

        public string Branch
        {
            get
            {
                return Utils.getData(Node, Constants.sPolBranch);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolBranch, value, sThisNodeOrder);
            }
        }

        public string Branch_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolBranch);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolBranch, value, sThisNodeOrder);
            }
        }

        public string Branch_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolBranch);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolBranch, value, sThisNodeOrder);
            }
        }
        //End SIW139

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPolEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPolEffDate, value, sThisNodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPolExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPolExpDate, value, sThisNodeOrder);
            }
        }

        public string CancellationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPolCanDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPolCanDate, value, sThisNodeOrder);
            }
        }

        public string RetroDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPolRetroDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPolRetroDate, value, sThisNodeOrder);
            }
        }

        public string TailDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPolTailDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPolTailDate, value, sThisNodeOrder);
            }
        }

        public XMLInsUnit Unit
        {
            get
            {
                if (m_Unit == null)
                {
                    m_Unit = new XMLInsUnit();
                    m_Unit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Unit);
                    m_Unit.getInsUnit(Constants.xcGetFirst);    //SI06420
                }
                return m_Unit;
            }
            set
            {
                m_Unit = value;
            }
        }

        public XMLEndorsement Endorsement
        {
            get
            {
                if (m_Endorsement == null)
                {
                    m_Endorsement = new XMLEndorsement();
                    m_Endorsement.Parent = this;
                    //SIW529 LinkXMLObjects(m_Endorsement);
                    m_Endorsement.getEndorsement(Constants.xcGetFirst);     //SI06420
                }
                return m_Endorsement;
            }
            set
            {
                m_Endorsement = value;
            }
        }

        //START SIW7123
        public XMLPolLimDed PolLimDed
        {
            get
            {
                if (m_PolLimDed == null)
                {
                    m_PolLimDed = new XMLPolLimDed();
                    m_PolLimDed.Parent = this;
                    m_PolLimDed.getPolLimDed(Constants.xcGetFirst);
                }
                return m_PolLimDed;
            }
            set
            {
                m_PolLimDed=value;
            }
        }


        //END SIW7123

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, putNode, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_Comment.getFirst();       //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, putNode, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getPolicybyID(string sPolID)
        {
            if (PolicyID != sPolID)
            {
                getFirst();
                while (Node != null)
                {
                    if (PolicyID == sPolID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getPolicybyNumber(string sPolNum)
        {
            if (PolicyNumber != sPolNum)
            {
                getFirst();
                while (Node != null)
                {
                    if (PolicyNumber == sPolNum)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getPolicy(bool reset)
        {
            return getNode(reset);
        }
        
        public XMLLimit Limit
        {
            get
            {
                if (m_Limit == null)
                {
                    m_Limit = new XMLLimit();
                    m_Limit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Limit);
                    m_Limit.getFirst();     //SI06420
                }
                return m_Limit;
            }
            set
            {
                m_Limit = value;
            }
        }

        public XMLDeductible Deductible
        {
            get
            {
                if (m_Deductible == null)
                {
                    m_Deductible = new XMLDeductible();
                    m_Deductible.Parent = this;
                    //SIW529 LinkXMLObjects(m_Deductible);
                    m_Deductible.getFirst();    //SI06420
                }
                return m_Deductible;
            }
            set
            {
                m_Deductible = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
    }
}
