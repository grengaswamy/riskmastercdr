/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/01/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 05/28/2008 | SI06354 |    SW      | Add DISPLAY_ADDRESS tag to Address XML. 
 * 07/24/2008 | SI06249 |    sw      | Add Effective start date and End date to Address/Contact XML
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 11/17/2008 | SI06420 |    ASM     | Child Objects
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 10/05/2009 | SIW209  |    JTC     | Fixes from retrofits
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLAddress : XMLXCNodeWithList
    {
        /*'*****************************************************************************
        ' The cXMLAddress class provides the functionality to support and manage a
        ' ClaimsPro Address Node
        '*****************************************************************************
        '  <ADDRESS ADDRESS_ID="ADR2">
        '     <TECH_KEY>AC Technical key for the element </TECH_KEY>  'SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                'SIW490
        '     <ADDRESS_LINE>AddressLine</ADDRESS_LINE>
        '       <!-- multiple address lines allowed -->
        '     <CITY>Dallas</CITY>
        '     <COUNTY CODE="code">CountName</COUNTY>
        '     <STATE CODE="TX">Texas</STATE>
        '     <COUNTRY CODE="code">Country Name</COUNTRY>
        '     <POSTAL_CODE>75252</POSTAL_CODE>
        '     <COMMENT_REFERENCE ID="CMTxx"/>
        '       <!-- multiple entries -->
        '     <EFFECTIVE_START_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>     'SI06249
        '     <EFFECTIVE_END_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>       'Si06249
        '     <DISPLAY_ADDRESS>Address to be displayed in the tree </DISPLAY_ADDRESS>  'SI06345 - Implemented in SI06354
        '  </ADDRESS>
        '*****************************************************************************/

        private XmlNode m_xmlAdrLine;
        private XmlNodeList m_xmlAdrLineList;
        //private IEnumerator m_xmlAdrLineListEnum;
        private XMLCommentReference m_Comment;
        private XMLVarDataItem m_DataItem;  //SI06420

        public XMLAddress()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlNodeList = null;
            m_xmlAdrLine = null;
            m_xmlAdrLineList = null;

            //sThisNodeOrder = new ArrayList(7);            //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(8);            //(SI06345 - Implemented in SI06354)
            //sThisNodeOrder = new ArrayList(9);            //(SI06345 - Implemented in SI06354)
            //sThisNodeOrder = new ArrayList(11);             //SI06249 //SIW490
            sThisNodeOrder = new ArrayList(12);             //SIW490
            sThisNodeOrder.Add(Constants.sStdTechKey);         //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);    //SIW490
            sThisNodeOrder.Add(Constants.sAdrLine);
            sThisNodeOrder.Add(Constants.sAdrCity);
            sThisNodeOrder.Add(Constants.sAdrCounty);
            sThisNodeOrder.Add(Constants.sAdrState);
            sThisNodeOrder.Add(Constants.sAdrCountry);
            sThisNodeOrder.Add(Constants.sAdrPostCode);
            sThisNodeOrder.Add(Constants.sAdrComment);
            //sThisNodeOrder.Add(Constants.sAddTechKey);    //(SI06023 - Implemented in SI06333)//SIW360
            sThisNodeOrder.Add(Constants.sAdrDisplAddr);    //(SI06345 - Implemented in SI06354)
            sThisNodeOrder.Add(Constants.sAdrEffStartDt);   //SI06249   
            sThisNodeOrder.Add(Constants.sAdrEffEndDt);     //SI06249
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sAdrNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sadrid, string scity, string sCountyCode, string scounty, string sstatecode,
                              string sstate, string sCountryCode, string scountry, string sPostCode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLElementWithAddress_Node(NodeName, "0", "");
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                AddressID = sadrid;
                if (scity != "" && scity != null)
                    City = scity;
                if ((scounty != "" && scounty != null) || (sCountyCode != "" && sCountyCode != null))
                    putCounty(scounty, sCountyCode);
                if ((sstate != "" && sstate != null) || (sstatecode != "" && sstatecode != null))
                    putState(sstate, sstatecode);
                if ((scountry != "" && scountry != null) || (sCountryCode != "" && sCountryCode != null))
                    putCountry(scountry, sCountryCode);
                if (sPostCode != "" && sPostCode != null)
                    PostalCode = sPostCode;
                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XML.Address.Create");
                return null;
            }
        }

        public int AddressLineCount
        {
            get
            {
                return AddressLineList.Count;
            }
        }

        public XmlNodeList AddressLineList
        {
            get
            {
                if (m_xmlAdrLineList == null)
                {
                    m_xmlAdrLineList = XML.XMLGetNodes(Node, Constants.sAdrLine);
                }
                return m_xmlAdrLineList;
            }
        }

        public void removeAddressLine()
        {
            if (m_xmlAdrLine != null)
                Node.RemoveChild(m_xmlAdrLine);
            m_xmlAdrLine = null;
        }

        public XmlNode getAddressLine(bool reset)
        {
            //Start SIW163
            if (null != AddressLineList)
            {
                return getNode(reset, ref m_xmlAdrLineList, ref m_xmlAdrLine);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode findAddressLine(string sLine)
        {
            bool rst = true;
            while (getAddressLine(rst) != null)
            {
                rst = false;
                if (AddressLine == sLine)
                    break;
            }
            return m_xmlAdrLine;
        }

        public XmlNode addAddressLine(string sNewValue)
        {
            m_xmlAdrLine = null;
            AddressLine = sNewValue;
            return m_xmlAdrLine;
        }

        public string AddressLine
        {
            get
            {
                return Utils.getData(m_xmlAdrLine, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                if (findAddressLine(value) == null)
                    m_xmlAdrLine = XML.XMLaddNode(putNode, Constants.sAdrLine, NodeOrder);
                m_xmlAdrLine = Utils.putData(m_xmlAdrLine, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string PostalCode
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrPostCode);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAdrPostCode, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string City
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrCity);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAdrCity, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Start SIW139
        public XmlNode putState(string sstate, string sstatecode)
        {
            return putState(sstate, sstatecode, "");
        }

        public XmlNode putState(string sstate, string sstatecode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAdrState, sstate, sstatecode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string State
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAdrState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string State_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAdrState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sAdrState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string State_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAdrState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAdrState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCounty(string scounty, string scountycode)
        {
            return putCounty(scounty, scountycode, "");
        }

        public XmlNode putCounty(string scounty, string scountycode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAdrCounty, scounty, scountycode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string County
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrCounty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAdrCounty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string County_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAdrCounty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sAdrCounty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string County_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAdrCounty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAdrCounty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCountry(string scountry, string sCountryCode)
        {
            return putCountry(scountry, sCountryCode, "");
        }

        public XmlNode putCountry(string scountry, string sCountryCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAdrCountry, scountry, sCountryCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string Country
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrCountry);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAdrCountry, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Country_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAdrCountry);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sAdrCountry, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Country_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAdrCountry);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAdrCountry, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //SIW139

        public string IDPrefix
        {
            get
            {
                return Constants.sCCPXmlAddressIDPrefix;
            }
        }

        public string AddressID
        {
            get
            {
                return Utils.getAddressRef(Node, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null || strdata == "0")
                    strdata = ((XMLAddresses)Parent).getNextAdrID(null);
                Utils.putAddressRef(putNode, "", strdata, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //Start (SI06023 - Implemented in SI06333)
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360    //SIW529 Remove local XML parameter
            }
        }
        //End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW490

        public string DisplayAddress //Start SI06345 - Implemented in SI06354)
        {
            get
            {
                return Utils.getData(Node, Constants.sAdrDisplAddr);    //SIW529 Remove local XML parameter
            }
            
        }//End SI06345  - Implemented in SI06354)

        //Start SI06249
        public string EffectiveStartDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sAdrEffStartDt);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAdrEffStartDt, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string EffectiveEndDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sAdrEffEndDt);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAdrEffEndDt, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SI06249

        public string FullAddress()
        {
            return FullAddress(" ");
        }

        public string FullAddress(string sSeperator)
        {
            string strAdd, strTmp;
            bool rst;
            strAdd = "";
            rst = true;
            while (getAddressLine(rst) != null)
            {
                rst = false;
                strAdd = strAdd + AddressLine + sSeperator;
            }
            strTmp = (State_Code != "" && State_Code != null) ? State_Code : State;
            if (City != "" && City != null)
                strAdd = strAdd + ((strTmp != "" && strTmp != null) ? ", " : " ");
            if (strTmp != "" && strTmp != null)
                strAdd = strAdd + strTmp + " ";
            if (PostalCode != "" && PostalCode != null)
                strAdd = strAdd + PostalCode + sSeperator;
            strTmp = (Country_Code != "" && Country_Code != null) ? Country_Code : Country;
            if (strTmp != "" && strTmp != null)
                strAdd = strAdd + strTmp + sSeperator;
            return strAdd;
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            //SIW209 ((XMLAddresses)Parent).Address = this;
            ((XMLAddresses)Parent).Count = ((XMLAddresses)Parent).Count + 1;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getAddress(bool reset)
        {
            return getNode(reset);
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLAddresses Parent
        {
            get
            {
                if (m_Parent == null)
                {   //SIW209
                    m_Parent = new XMLAddresses();
                    ((XMLAddresses)m_Parent).Address = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW209
                }   //SIW209
                return (XMLAddresses)m_Parent;
            }
            set
            {
                m_Parent = value;
                //SIW529 if (m_Parent != null)
                    //SI06420((XMLAddresses)m_Parent).Address = this;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        //Begin SI06420
        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    if (Node != null)
                    {
                        m_DataItem.getFirst();
                    }
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        //End SI06420

        private void subClearPointers()
        {
            m_xmlAdrLine = null;
            m_xmlAdrLineList = null;
            CommentReference = null;
        }

        public XmlNode getAddressbyID(string sadrid)
        {
            if (AddressID != sadrid)
            {
                bool rst = true;
                while (getAddress(rst) != null)
                {
                    rst = false;
                    if (AddressID == sadrid)
                        break;
                }
            }
            return Node;
        }

        public XmlNode getAddressbyAddress(ArrayList sadrline, string scity, string sstate, string sstatecd,
                                           string scounty, string scountycd, string spstcode,
                                           string scountry, string scountrycd)
        {
            bool bMatch, rst;

            getFirst();
            while (Node != null)
            {
                bMatch = true;
                rst = true;
                for (int idx = 0; idx < sadrline.Count; idx++)
                {
                    while (getAddressLine(rst) != null)
                    {
                        if ((string)sadrline[idx] != "" && sadrline[idx] != null && (string)sadrline[idx] != AddressLine)
                            bMatch = false;
                        rst = false;
                    }
                }

                if ((scity != "" && scity != null && scity != City) ||
                   (sstate != "" && sstate != null && sstate != State) ||
                   (sstatecd != "" && sstatecd != null && sstatecd != State_Code) ||
                   (scounty != "" && scounty != null && scounty != County) ||
                   (scountycd != "" && scountycd != null && scountycd != County_Code) ||
                   (scountry != "" && scountry != null && scountry != Country) ||
                   (scountrycd != "" && scountrycd != null && scountrycd != Country_Code) ||
                   (spstcode != "" && spstcode != null && spstcode != PostalCode))
                    bMatch = false;

                if (bMatch)
                    break;
                getNext();
            }
            return Node;
        }
    }

    public class XMLAddresses:XMLXCNode
    {
        /*'*****************************************************************************
        ' The cXMLAddresses class provides the functionality to support and manage a
        ' ClaimsPro Addressess Collection Node
        '*****************************************************************************
        '<ADDRESSES NEXT_ID="0" COUNT=xx">
        '  <ADDRESS></ADDRESS>
        '</ADDRESSES>
        '*****************************************************************************/

        XMLAddress m_Address;

        public XMLAddresses()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_Address = null;
            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sAdrNode);

            Node = Utils.getNode(Parent.Node, NodeName);    //SIW529 Remove local XML parameter
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                ResetParent(); //SI06420
            }
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sAdrCollection; }
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc)
        {
            return Create(xmlDoc, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextAdrID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextAdrID = sNextAdrID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLAddresses.Create");
                return null;
            }
        }

        public string getNextAdrID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextAdrID;
            if (nid == "" || nid == null || nid == "0")
                nid = "1";
            NextAdrID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextAdrID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sAdrNextID);    //SIW529 Remove local XML parameter
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null)
                    strdata = "1";
                Utils.putAttribute(putNode, "", Constants.sAdrNextID, strdata, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public int Count
        {
            get
            {
                string strdata;
                strdata = Utils.getAttribute(Node, "", Constants.sAdrCount);    //SIW529 Remove local XML parameter
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sAdrCount, value + "", NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public override XmlNode Node
        {
            get//Start SI06420
            {
                if (xmlThisNode == null)
                    ResetParent();
                return xmlThisNode; 
            }//End SI06420
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            //Start SI06420
            //Address = null;
            if (m_Address != null)
                //SIW529 m_Address.Node = null;
                m_Address.Parent = this;    //SIW529  Resets the Address object
            //End SI06420
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = (XML)value;
                ResetParent();
            }
        }

        public XmlNode AddAddress()
        {
            return Address.Create();
        }

        public XMLAddress Address
        {
            get
            {
                if (m_Address == null)
                {
                    m_Address = new XMLAddress();
                    m_Address.Parent = this;
                    //SIW529 LinkXMLObjects(m_Address);
                    //m_Address.getAddress(Constants.xcGetFirst);     //SI06420
                }
                if (Node != null)//Start SI06420
                { 
                  if(m_Address.Node == null)
                      m_Address.getAddress(Constants.xcGetFirst);
                }//End SI06420
                return m_Address;
            }
            set
            {
                m_Address = value;
                if (m_Address != null)       //SI06420
                {
                    m_Address.Parent = this; //SI06420
                    //SIW529 LinkXMLObjects(m_Address);
                }
            }
        }

        public XmlNode getAddress(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Address.getAddress(reset);
        }

        public XmlNode getAddressbyID(XmlDocument xdoc, string sadrid)
        {
            if (xdoc != null)
                Document = xdoc;
            return Address.getAddressbyID(sadrid);
        }

        public XmlNode getAddressbyAddress(XmlDocument xdoc, ArrayList sadrline, string scity, string sstate,
                                           string sstatecd, string scounty, string scountycd, string spstcode,
                                           string scountry, string scountrycd)
        {
            if (xdoc != null)
                Document = xdoc;
            return Address.getAddressbyAddress(sadrline, scity, sstate, sstatecd, scounty, scountycd, spstcode, scountry, scountrycd);
        }
    }
}
