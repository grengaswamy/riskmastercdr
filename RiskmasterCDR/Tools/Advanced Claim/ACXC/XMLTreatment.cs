/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/31/2008 | SI06467 |   ASM      |  Create
 *            |         |            | Move Treatement info from cXMLinjury to this one
 *            |         |            | Add IME and Pre-Cert Nodes
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/23/2010 | SIW254  |    AS      | HTML fields added to treatment xmlobject
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLTreatment : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
       '<CLAIM>
       '   <INJURY ID="INJxxx">
       '      <TREATMENT>
       '         <TECH_KEY>AC Technical key for the element </TECH_KEY>
       '         <SPECIFIC CODE="Standard CPT Treatment Code {string} [TREATMENT_CODE]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">description</SPECIFIC>
       '         <STARTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '         <ENDED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '         <TREATED_BY ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</TREATED_BY>
       '         <DATE_INITIAL_VISIT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '         <PHYSICIAN_SPECIALITY CODE ="DOC">description</PHYSICIAN_SPECIALITY>
       '         <PRE_CERTIFICATION>
       '            <TECH_KEY>AC Technical key for the element </TECH_KEY>
       '            <CLASS_DESCRIPTION CODE="Class Description Code {string} [PRECERT_CLASS_TYPE]" TYPE="Code Type (DEC,NON, PRE) {string} [DEC,NON,PRE]">description</CLASS_DESCRIPTION>
       '            <DATE_RECEIVED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <TIME_OF_EXAM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <TYPE_REQUEST CODE="Type Request Desc Code {string} [PRECERT_REQUEST_TY]" TYPE="Code Type (TES,TRE) {string} [TES,TRE]">description</TYPE_REQUEST>
       '            <PROCEDURE_DESCRIPTION>Procedure description </PROCEDURE_DESCRIPTION>
       '            <NUMBER_OF_VISITS_REQ> Number of Visits Required</NUMBER_OF_VISITS_REQ>
       '            <DECISION_DESC CODE="Decision Desc Code {string} [PRECERT_DEC_TYPE]" TYPE="Code Type (APP,DEN,MOD) {string} [APP,DEN,MOD]">description</DECISION_DESC>
       '            <DATE_OF_DECISION YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <REASON_DECISION_DESC CODE="Reason Decision Desc Code {string} [PRECERT_REAS_DEC_T]" TYPE="Code Type (A,B,C,D1, etc.) {string} [A,B]">description</REASON_DECISION_DESC>
       '            <NEXT_REVIEW YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <NUMBER_OF_VISITS> Number of Actual Visits</NUMBER_OF_VISITS>
       '            <VISIT_FROM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <VISIT_TO YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <PERCENT_COPAY INDICATOR="Yes/No" />
       '            <APPEAL_DECISION CODE="Appeal Decision Code {string} [PRECERT_APPEAL_DEC]" TYPE="Code Type (APP,DEN,MOD) {string} [APP,DEN,MOD]">description</APPEAL_DECISION>
       '            <APPEAL_RECEIVED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <APPEAL_RECEIVED_DECISION YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <NEXT_APPEAL YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <PRECERT_RESULTS></PRECERT_RESULTS>
       '         </PRE_CERTIFICATION>
       '         <!--Multiple Pre-Certification allowed-->
       '         <IME>
       '             <TECH_KEY>AC Technical key for the element </TECH_KEY>
       '            <EXAM_SCHEDULED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <TIME_OF_EXAM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <IME_REQUESTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <EXAM_COMPLETED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
       '            <IME_RESULTS></IME_RESULTS>
       '            <IME_RESULTS_HTML></IME_RESULTS_HTML>   'SIW254
       '         </IME>
       '         <!--Multiple Indepedent Medical Exams (IME) allowed-->
       '      </TREATMENT>
       '         <!--Multiple treatments allowed-->
       '   </INJURY>
       '</CLAIM>*/


        private XmlNode m_xmlTreatment;
        private XmlNodeList m_xmlTreatmentList;
        private XmlNode m_xmlPreCert;
        private XmlNodeList m_xmlPreCertList;
        private XmlNode m_xmlIME;
        private XmlNodeList m_xmlIMEList;
        //SIW529 private XMLClaim m_Claim; 
        private XMLInjury m_Injury;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        //SIW529 private XMLAddressReference m_AddressReference;
        private XMLVarDataItem m_DataItem;
        private ArrayList sTreatNodeOrder;
        private ArrayList sPreCertNodeOrder;
        private ArrayList sIMENodeOrder;
        
        protected override string DefaultNodeName
        {
            get { return Constants.sInjTreatment; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sTreatment,
                             string sTreatmentCode,
                             string sStartDate,
                             string sEndDate,
                             string sTreatedBy,
                             string sDateInitialVisit,
                             string sPhysicianSpeciality,
                             string sPhysicianSpecialityCode)
        {

            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;
            xmlThisNode = null;
            Errors.Clear();
            xmlDocument = Document;
            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                if (sTreatment != "")
                {
                    putTreatment(sTreatment, sTreatmentCode);
                }
                if (sStartDate != "")
                {
                    TreatmentStartDate = sStartDate;
                }
                if (sEndDate != "")
                {
                    TreatmentEndDate = sEndDate;
                }
                if (sTreatedBy != "")
                {
                    TreatedBy = sTreatedBy;
                }
                if (sDateInitialVisit != "")
                {
                    DateInitialVisit = sDateInitialVisit;
                }
                if (sPhysicianSpeciality != "")
                {
                    putPhysicianSpeciality(sPhysicianSpeciality, sPhysicianSpecialityCode);
                }
                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return m_xmlTreatment;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLTreatment.Create");
                return null;
            }

        }
        public XMLTreatment()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            sTreatNodeOrder = new ArrayList(7);
            sTreatNodeOrder.Add(Constants.sStdTechKey); //SIW360
            sTreatNodeOrder.Add(Constants.sInjTreatSpecific);
            sTreatNodeOrder.Add(Constants.sInjTreatStarted);
            sTreatNodeOrder.Add(Constants.sInjTreatEnded);
            sTreatNodeOrder.Add(Constants.sInjTreatedBy);
            sTreatNodeOrder.Add(Constants.sInjDateInitialVisit);
            sTreatNodeOrder.Add(Constants.sInjPhysicianSpeciality);

            sPreCertNodeOrder = new ArrayList(20);
            sPreCertNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sPreCertNodeOrder.Add(Constants.sTreatPCClassDesc);
            sPreCertNodeOrder.Add(Constants.sTreatPCDateReceived);
            sPreCertNodeOrder.Add(Constants.sTreatPCTimeOfExam);
            sPreCertNodeOrder.Add(Constants.sTreatPCTypeRequest);
            sPreCertNodeOrder.Add(Constants.sTreatPCProcedureDesc);
            sPreCertNodeOrder.Add(Constants.sTreatPCNumOfVisitReq);
            sPreCertNodeOrder.Add(Constants.sTreatPCDecisionDesc);
            sPreCertNodeOrder.Add(Constants.sTreatPCDecisionDate);
            sPreCertNodeOrder.Add(Constants.sTreatPCReasonDecisionDesc);
            sPreCertNodeOrder.Add(Constants.sTreatPCNextReview);
            sPreCertNodeOrder.Add(Constants.sTreatPCNumOfVisits);
            sPreCertNodeOrder.Add(Constants.sTreatPCVisitFromYear);
            sPreCertNodeOrder.Add(Constants.sTreatPCVisitToYear);
            sPreCertNodeOrder.Add(Constants.sTreatPCPercentCoPay);
            sPreCertNodeOrder.Add(Constants.sTreatPCAppealDescision);
            sPreCertNodeOrder.Add(Constants.sTreatPCAppealReceived);
            sPreCertNodeOrder.Add(Constants.sTreatPCAppealReceivedDecision);
            sPreCertNodeOrder.Add(Constants.sTreatPCNextAppeal);
            sPreCertNodeOrder.Add(Constants.sTreatPCResults);

            //sIMENodeOrder = new ArrayList(6);//SIW254
            sIMENodeOrder = new ArrayList(7);//SIW254

            sIMENodeOrder.Add(Constants.sStdTechKey); //SIW360
            sIMENodeOrder.Add(Constants.sTreatIMEExamSched);
            sIMENodeOrder.Add(Constants.sTreatIMETimeOfExam);
            sIMENodeOrder.Add(Constants.sTreatIMERequested);
            sIMENodeOrder.Add(Constants.sTreatIMECompleted);
            sIMENodeOrder.Add(Constants.sTreatIMEResults);
            sIMENodeOrder.Add(Constants.sTreatIMEResultsHTML); //SIW254


        }
        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }
        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }
        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }
        public new XMLInjury Parent
        {
            get
            {
                if (m_Injury == null)
                {
                    m_Injury = new XMLInjury();
                    //SIW122 m_Injury = this.m_Injury;
                    LinkXMLObjects((XMLXCNodeBase)m_Injury);   //SIW529
                    ResetParent();
                }
                return m_Injury;
            }
            set
            {
                m_Injury = value;
                ResetParent();
                if (m_Injury != null) //SIW529
                    ((XMLXCNodeBase)m_Injury).LinkXMLObjects(this);    //SIW529
            }
        }

       

        public XMLPartyInvolved PartyInvolved
        {

            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();

                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }


        }
        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }
        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }



        private void subClearPointers()
        {
        }
        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
        //'*********** Treatments ****************
        public int TreatmentCount
        {
            get
            {
                return TreatmentList.Count;
            }
        }

        public XmlNodeList TreatmentList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlTreatmentList)
                {
                    return getNodeList(NodeName, ref m_xmlTreatmentList, Node);
                }
                else
                {
                    return m_xmlTreatmentList;
                }
                //End SIW163
            }
        }
        public XmlNode getTreatment(bool reset)
        {
            //Start SIW163
            if (null != TreatmentList)
            {
                return getNode(reset, ref m_xmlTreatmentList, ref m_xmlTreatment);
            }
            else
            {
                return null;
            }
            //End SIW163
        }


        public void removeTreatment()
        {
            if (TreatmentNode != null)
                Node.RemoveChild(TreatmentNode);
            TreatmentNode = null;
        }
        private XmlNode putTreatmentNode
        {
            get
            {
                if (TreatmentNode == null)
                    TreatmentNode = XML.XMLaddNode(putNode, NodeName, NodeOrder);
                return TreatmentNode;
            }
        }
        private XmlNode TreatmentNode
        {
            get
            {
                return m_xmlTreatment;
            }
            set
            {
                m_xmlTreatment = value;
            }
        }

        public XmlNode putTreatment(string sTreatment, string sTreatmentCode)
        {
            Treatment = sTreatment;
            Treatment_Code = sTreatmentCode;
            return TreatmentNode;
        }

        public string Treatment
        {
            get
            {
                return Utils.getData(TreatmentNode, Constants.sInjTreatSpecific);
            }
            set
            {
                Utils.putData(putTreatmentNode, Constants.sInjTreatSpecific, value, sTreatNodeOrder);
            }
        }
        public string Treatment_Code
        {
            get
            {
                return Utils.getCode(TreatmentNode, Constants.sInjTreatSpecific);
            }
            set
            {
                Utils.putCode(putTreatmentNode, Constants.sInjTreatSpecific, value, sTreatNodeOrder);
            }
        }

        public string TreatmentStartDate
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjTreatStarted);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjTreatStarted, value, sTreatNodeOrder);
            }
        }

        public string TreatmentEndDate
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjTreatEnded);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjTreatEnded, value, sTreatNodeOrder);
            }
        }

        public string TreatedBy
        {
            get
            {
                return Utils.getEntityRef(TreatmentNode, Constants.sInjTreatedBy);
            }
            set
            {
                Utils.putEntityRef(putTreatmentNode, Constants.sInjTreatedBy, value, sTreatNodeOrder);
            }
        }

        public string DateInitialVisit
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjDateInitialVisit);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjDateInitialVisit, value, sTreatNodeOrder);
            }
        }

        public XmlNode putPhysicianSpeciality(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putTreatmentNode, Constants.sInjPhysicianSpeciality, sDesc, sCode, sTreatNodeOrder);
        }

        public string PhysicianSpeciality
        {
            get
            {
                return Utils.getData(TreatmentNode, Constants.sInjPhysicianSpeciality);
            }
            set
            {
                Utils.putData(putTreatmentNode, Constants.sInjPhysicianSpeciality, value, sTreatNodeOrder);
            }
        }

        public string PhysicianSpeciality_Code
        {
            get
            {
                return Utils.getCode(TreatmentNode, Constants.sInjPhysicianSpeciality);
            }
            set
            {
                Utils.putCode(putTreatmentNode, Constants.sInjPhysicianSpeciality, value, sTreatNodeOrder);
            }
        }
        public string TreatTechkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }
        /* ******************************************
       *********** Pre-Certification ****************
       **********************************************/

        public XmlNodeList PreCertList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlPreCertList)
                {
                    return getNodeList(Constants.sTreatPreCertNode, ref m_xmlPreCertList, PreCertNode);
                }
                else
                {
                    return m_xmlPreCertList;
                }
                //End SIW163
            }
        }

        public XmlNode getPreCert(bool reset)
        {
            //Start SIW163
            if (null != PreCertList)
            {
                return getNode(reset, ref m_xmlPreCertList, ref m_xmlPreCert);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removePreCert()
        {
            if (PreCertNode != null)
                Node.RemoveChild(PreCertNode);
            PreCertNode = null;
        }
        private XmlNode putPreCertNode
        {
            get
            {
                if (PreCertNode == null)
                    PreCertNode = XML.XMLaddNode(putNode, Constants.sTreatPreCertNode, NodeOrder);
                return PreCertNode;
            }
        }
        //private XmlNode PreCertNode   //SI06467
        public XmlNode PreCertNode
        {
            get
            {
                return m_xmlPreCert;
            }
            set
            {
                m_xmlPreCert = value;
            }
        }
        public XmlNode addPreCert(string sClassDescription,
                            string sClassDescriptionCode,
                            string sReceivedDate,
                            string sTimeOfExam,
                            string sTypeRequest,
                            string sTypeRequestCode,
                            string sProcedureDescription,
                            string sNumOfVisitsReq,
                            string sDecisionDescription,
                            string sDecisionDescriptionCode,
                            string sDecisionDate,
                            string sReasonDecision,
                            string sReasonDecisionCode,
                            string sNextReviewDate,
                            string sNumberOfVisits,
                            string sVisitFromDate,
                            string sVisitToDate,
                            string sCoPayInd,
                            string sAppealDecision,
                            string sAppealDecisionCode,
                            string sAppealReceivedDate,
                            string sAppealReceivedDecisionDate,
                            string sNextAppealDate,
                            string sResults)
        {


            PreCertNode = null;


            if (sClassDescription != null)
            {
                putClassDescription(sClassDescription, sClassDescriptionCode);
            }

            if (sReceivedDate != null)
            {
                PreCertReceivedDate = sReceivedDate;
            }


            if (sTimeOfExam != null)
            {
                PreCertTimeOfExam = sTimeOfExam;
            }

            if (sTypeRequest != null)
            {
                putTypeRequest(sTypeRequest, sTypeRequestCode);
            }

            if (sProcedureDescription != null)
            {
                PreCertProcedureDescription = sProcedureDescription;
            }

            if (sNumOfVisitsReq != null)
            {
                PreCertNumOfVisitsReq = sNumOfVisitsReq;
            }

            if (sDecisionDescription != null)
            {
                putDecisionDescription(sDecisionDescription, sDecisionDescriptionCode);
            }

            if (sDecisionDate != null)
            {
                PreCertDecisionDate = sDecisionDate;
            }

            if (sReasonDecision != null)
            {
                putReasonDecision(sReasonDecision, sReasonDecisionCode);
            }

            if (sNextReviewDate != null)
            {
                PreCertNextReviewDate = sNextReviewDate;
            }

            if (sNumberOfVisits != null)
            {
                PreCertNumOfVisits = sNumberOfVisits;
            }

            if (sVisitFromDate != null)
            {
                PreCertVisitFromDate = sVisitFromDate;
            }

            if (sVisitToDate != null)
            {
                PreCertVisitFromDate = sVisitFromDate;
            }

            if (sCoPayInd != null)
            {
                PreCertCoPayInd = sCoPayInd;
            }

            if (sAppealDecision != null)
            {
                putAppealDecision(sAppealDecision, sAppealDecisionCode);
            }

            if (sAppealReceivedDate != null)
            {
                PreCertAppealReceivedDate = sAppealReceivedDate;
            }

            if (sAppealReceivedDecisionDate != null)
            {
                PreCertAppealReceivedDecisionDate = sAppealReceivedDecisionDate;
            }

            if (sNextAppealDate != null)
            {
                PreCertNextAppealDate = sNextAppealDate;
            }

            if (sResults != null)
            {
                PreCertResults = sResults;
            }
            return PreCertNode;
        }
        public string PreCertTechkey
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sStdTechKey, value, sPreCertNodeOrder);//SIW360
            }
        }

        public XmlNode putClassDescription(string sClassDescription, string sClassDescriptionCode)
        {
            return Utils.putCodeItem(putPreCertNode, Constants.sTreatPCClassDesc, sClassDescription, sClassDescriptionCode, sPreCertNodeOrder);
        }
        public string PreCertClassDescription
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCClassDesc);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCClassDesc, value, sPreCertNodeOrder);
            }
        }
        public string PreCertClassDescription_Code
        {
            get
            {
                return Utils.getCode(PreCertNode, Constants.sTreatPCClassDesc);
            }
            set
            {
                Utils.putCode(putPreCertNode, Constants.sTreatPCClassDesc, value, sPreCertNodeOrder);
            }
        }
        public string PreCertReceivedDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCDateReceived);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCDateReceived, value, sPreCertNodeOrder);
            }
        }

        public string PreCertTimeOfExam
        {
            get
            {
                return Utils.getTime(PreCertNode, Constants.sTreatPCTimeOfExam);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCTimeOfExam, value, sPreCertNodeOrder);
            }
        }

        public XmlNode putTypeRequest(string sTypeRequest, string sTypeRequestCode)
        {
            return Utils.putCodeItem(putPreCertNode, Constants.sTreatPCTypeRequest, sTypeRequest, sTypeRequestCode, sPreCertNodeOrder);
        }
        public string TypeRequest
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCTypeRequest);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCTypeRequest, value, sPreCertNodeOrder);
            }
        }
        public string TypeRequest_Code
        {
            get
            {
                return Utils.getCode(PreCertNode, Constants.sTreatPCTypeRequest);
            }
            set
            {
                Utils.putCode(putPreCertNode, Constants.sTreatPCTypeRequest, value, sPreCertNodeOrder);
            }
        }
        public string PreCertProcedureDescription
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCProcedureDesc);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCProcedureDesc, value, sPreCertNodeOrder);
            }
        }

        public string PreCertNumOfVisitsReq
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCNumOfVisitReq);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCNumOfVisitReq, value, sPreCertNodeOrder);
            }
        }

        public XmlNode putDecisionDescription(string sDecisionDescription, string sDecisionDescriptionCode)
        {
            return Utils.putCodeItem(putPreCertNode, Constants.sTreatPCDecisionDesc, sDecisionDescription, sDecisionDescriptionCode, sPreCertNodeOrder);
        }
        public string PreCertDecisionDescription
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCDecisionDesc);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCDecisionDesc, value, sPreCertNodeOrder);
            }
        }
        public string PreCertDecisionDescription_Code
        {
            get
            {
                return Utils.getCode(PreCertNode, Constants.sTreatPCDecisionDesc);
            }
            set
            {
                Utils.putCode(putPreCertNode, Constants.sTreatPCDecisionDesc, value, sPreCertNodeOrder);
            }
        }

        public string PreCertDecisionDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCDecisionDate);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCDecisionDate, value, sPreCertNodeOrder);
            }
        }

        public XmlNode putReasonDecision(string sReasonDecision, string sReasonDecisionCode)
        {
            return Utils.putCodeItem(putPreCertNode, Constants.sTreatPCReasonDecisionDesc, sReasonDecision, sReasonDecisionCode, sPreCertNodeOrder);
        }
        public string PreCertReasonDecision
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCReasonDecisionDesc);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCReasonDecisionDesc, value, sPreCertNodeOrder);
            }
        }
        public string PreCertReasonDecision_Code
        {
            get
            {
                return Utils.getCode(PreCertNode, Constants.sTreatPCReasonDecisionDesc);
            }
            set
            {
                Utils.putCode(putPreCertNode, Constants.sTreatPCReasonDecisionDesc, value, sPreCertNodeOrder);
            }
        }

        public string PreCertNextReviewDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCNextReview);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCNextReview, value, sPreCertNodeOrder);
            }
        }
        public string PreCertNumOfVisits
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCNumOfVisitReq);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCNumOfVisitReq, value, sPreCertNodeOrder);
            }
        }
        public string PreCertVisitFromDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCVisitFromYear);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCVisitFromYear, value, sPreCertNodeOrder);
            }
        }
        public string PreCertVisitToDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCVisitToYear);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCVisitToYear, value, sPreCertNodeOrder);
            }
        }
        public string PreCertCoPayInd
        {
            get
            {
                return Utils.getBool(PreCertNode, Constants.sTreatPCPercentCoPay, "");
            }
            set
            {
                Utils.putBool(putPreCertNode, Constants.sTreatPCPercentCoPay, "", value, sPreCertNodeOrder);
            }
        }

        public XmlNode putAppealDecision(string sAppealDecision, string sAppealDecisionCode)
        {
            return Utils.putCodeItem(putPreCertNode, Constants.sTreatPCAppealDescision, sAppealDecision, sAppealDecisionCode, sPreCertNodeOrder);
        }
        public string PreCertAppealDecision
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCAppealDescision);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCAppealDescision, value, sPreCertNodeOrder);
            }
        }
        public string PreCertAppealDecision_Code
        {
            get
            {
                return Utils.getCode(PreCertNode, Constants.sTreatPCAppealDescision);
            }
            set
            {
                Utils.putCode(putPreCertNode, Constants.sTreatPCAppealDescision, value, sPreCertNodeOrder);
            }
        }

        public string PreCertAppealReceivedDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCAppealReceived);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCAppealReceived, value, sPreCertNodeOrder);
            }
        }

        public string PreCertAppealReceivedDecisionDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCAppealReceivedDecision);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCAppealReceivedDecision, value, sPreCertNodeOrder);
            }
        }

        public string PreCertNextAppealDate
        {
            get
            {
                return Utils.getDate(PreCertNode, Constants.sTreatPCNextAppeal);
            }
            set
            {
                Utils.putDate(putPreCertNode, Constants.sTreatPCNextAppeal, value, sPreCertNodeOrder);
            }
        }

        public string PreCertResults
        {
            get
            {
                return Utils.getData(PreCertNode, Constants.sTreatPCResults);
            }
            set
            {
                Utils.putData(putPreCertNode, Constants.sTreatPCResults, value, sPreCertNodeOrder);
            }
        }

        /********************************
        *********** IME ****************
        ********************************/


        public XmlNodeList IMEList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlIMEList)
                {
                    return getNodeList(Constants.sTreatIMENode, ref m_xmlIMEList, PreCertNode);
                }
                else
                {
                    return m_xmlIMEList;
                }
                //End SIW163
            }
        }

        public XmlNode getIME(bool reset)
        {
            //Start SIW163
            if (null != IMEList)
            {
                return getNode(reset, ref m_xmlIMEList, ref m_xmlIME);
            }
            else
            {
                return null;
            }
            //End SIW163
        }


        public void remove()
        {
            if (IMENode != null)
                Node.RemoveChild(IMENode);
            IMENode = null;
        }

        private XmlNode putIMENode
        {
            get
            {
                if (IMENode == null)
                    IMENode = XML.XMLaddNode(putNode, Constants.sTreatIMENode, NodeOrder);
                return IMENode;
            }
        }

        //private XmlNode IMENode   //SI06467
        public XmlNode IMENode    
        {
            get
            {
                return m_xmlIME;
            }
            set
            {
                m_xmlIME = value;
            }
        }

        public XmlNode addIME(string sExamSchedDate,
                          string sTimeOfExam,
                          string sRequestedDate,
                          string sExamCompletedDate,
                          string sResults)
        {
            IMENode = null;
            

            if (sExamSchedDate != null)
            {
                IMEExamSchedDate = sExamSchedDate;
            }

            if (sTimeOfExam != null)
            {
                IMETimeOfExam = sTimeOfExam;
            }
            
            if (sRequestedDate != null)
            {
                IMERequestedDate = sRequestedDate;
            }

            if (sExamCompletedDate != null)
            {
                IMEExamCompletedDate = sExamCompletedDate;
            }

            if (sResults != null)
            {
                IMEResults = sResults;
            }
            return IMENode;
        }
        public string IMETechkey
        {
            get
            {
                return Utils.getData(IMENode, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putIMENode, Constants.sStdTechKey, value, sIMENodeOrder);//SIW360
            }
        }
        public string IMEExamSchedDate
        {
            get
            {
                return Utils.getDate(IMENode, Constants.sTreatIMEExamSched);
            }
            set
            {
                Utils.putDate(putIMENode, Constants.sTreatIMEExamSched, value, sIMENodeOrder);
            }
        }

        public string IMETimeOfExam
        {
            get
            {
                return Utils.getDate(IMENode, Constants.sTreatIMETimeOfExam);
            }
            set
            {
                Utils.putDate(putIMENode, Constants.sTreatIMETimeOfExam, value, sIMENodeOrder);
            }
        }


        public string IMERequestedDate
        {
            get
            {
                return Utils.getDate(IMENode, Constants.sTreatIMERequested);
            }
            set
            {
                Utils.putDate(putIMENode, Constants.sTreatIMERequested, value, sIMENodeOrder);
            }
        }


        public string IMEExamCompletedDate
        {
            get
            {
                return Utils.getDate(IMENode, Constants.sTreatIMECompleted);
            }
            set
            {
                Utils.putDate(putIMENode, Constants.sTreatIMECompleted, value, sIMENodeOrder);
            }
        }

        public string IMEResults
        {
            get
            {
                return Utils.getData(IMENode, Constants.sTreatIMEResults);
            }
            set
            {
                Utils.putData(putIMENode, Constants.sTreatIMEResults, value, sIMENodeOrder);
            }
        }
        //Start SIW254
        public string IMEResultsHTML
        {
            get
            {
                return Utils.getData(IMENode, Constants.sTreatIMEResultsHTML);
            }
            set
            {
                Utils.putData(putIMENode, Constants.sTreatIMEResultsHTML, value, sIMENodeOrder);
            }
        }
        //End SIW254

    }


}
