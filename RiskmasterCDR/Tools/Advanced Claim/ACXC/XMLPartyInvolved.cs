/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 01/13/2010 | SI06650 |    NDB     | Fix for GetPartyInvolvedByRoleCode     
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 05/04/2010 | SIW403  |   ASM      | Expire Party Involved Role
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label    
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList  
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/24/2011 | SIW529  |    AS      | Updated getPartyByRole
 * 02/04/2011 | SIW529  |    AS      | Reference to Entity property fixed
 * 11/03/2011 | SIN7750 | MK         | Insured Person name for Dairy List
 * 02/22/2011 | SIW8170 |  PV        |Information for Parties not showing on the explorer tree in AC WEB
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLPartyInvolved : XMLXCReferenceNode, XMLXCIntPartyInvolved  //SIW122
    {
        /*<PARENT_NODE>
        '   <PARTY_INVOLVED ID="PIxxx" ENTITY_ID="ENTxxx" CODE="Role"> // SI06650 
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '      <TYPE>Role Description</TYPE>
        '      <EXPIRY_DATE YEAR="year" MONTH="month" DAY="day"/>       'SIW403
        '      <DATA_ITEM/>
        '      <PARTY_INVOLVED ID="PIxxx" ENTITYIDREF="ENTxxx" CODE="Role"> // SI06650
        '        ...
        '      </PARTY_INVOLVED>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '   </PARTY_INVOLVED>
        '     <!-- Multiple Occurrences -->        
        '</PARENT_NODE>*/

        private XMLVarDataItem m_DataItem;
        private XMLCommentReference m_Comment;
        private XMLEntity m_Entity;
        private XMLEntities m_Entities; //SIW529
        private XMLPartyInvolved m_PartyInvolved;
        
        public XMLPartyInvolved()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            m_DataItem = null;
            m_Comment = null;
            m_Entity = null;
            m_Entities = null; //SIW529
            m_PartyInvolved = null;

            //sThisNodeOrder = new ArrayList(2);            //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(3);              //(SI06023 - Implemented in SI06333) //SIW403
            //sThisNodeOrder = new ArrayList(4);               //SIW403  //SIW490
            //sThisNodeOrder = new ArrayList(5);               //SIW490
            sThisNodeOrder = new ArrayList(6);              //SIN7750
            sThisNodeOrder.Add(Constants.sStdTechKey);       //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW490
            sThisNodeOrder.Add("TYPE");
            sThisNodeOrder.Add("DATA_ITEM");
            sThisNodeOrder.Add("COMMENT_REFERENCE");
            //sThisNodeOrder.Add(Constants.sPITechKey);       //(SI06023 - Implemented in SI06333) //SIW360
            sThisNodeOrder.Add(Constants.sPIExpDate);      //SIW403
            sThisNodeOrder.Add(Constants.sInsuredName);     //SIN7750

        }

        public XMLPartyInvolved CloneObject()
        {
            return (XMLPartyInvolved) this.MemberwiseClone();
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sPartyInvolvedNode; }
        }

        private void CheckPIEntry()
        {
            CheckEntry();
        }

        protected override void CheckEntry()
        {
            //xEntry(Role, RoleCode, EntityID, xmlThisNode.ChildNodes.Count);
        }

        // SI06650 public XmlNode putPartyInvolved(string entid, string pirole, string pirolecode)
        public XmlNode putPartyInvolved(string PIid, string entid, string pirole, string pirolecode)
        {
            Node = null;
            // SI06659 if (entid != "" && entid != null && pirolecode != "" && pirolecode != null)
            // SI06650    findPartyInvolved(entid, pirolecode);
            // Start SI06650
            if (!string.IsNullOrEmpty(PIid))
            {
                findPartyInvolved(PIid);
            }
            else if (!string.IsNullOrEmpty(entid) && !string.IsNullOrEmpty(pirolecode))
            {
                findPartyInvolvedByEntity(entid, pirolecode);
            }
            // End SI06650

            if (Node == null)
                Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            if (entid != "" && entid != null)
                EntityID = entid;
            if (pirolecode != "" && pirolecode != null)
                RoleCode = pirolecode;
            if (pirole != "" && pirole != null)
                Role = pirole;
            return xmlThisNode;
        }


        public XmlNode getPartyInvolved(bool reset)
        {
            return getNode(reset);
        }

        public string Role
        {
            get
            {
                return Utils.getData(Node, Constants.sPIType);
            }
            set
            {
                //SIW485 Utils.putData(putNode, Constants.sPIType, value, Globals.sNodeOrder);
                Utils.putData(putNode, Constants.sPIType, value, Utils.sNodeOrder);	//SIW485
                if (value == "")
                    CheckPIEntry();
            }
        }
        // Start SIW490
        public string DisplayRole
        {
            get
            {
                string sDisplayRole = "";
                //string sClaimantNumber = ""; //SIW8170
                 
                sDisplayRole = this.Role;
                // Start SIW8170
                //if(this.RoleCode.Equals("CLT"))
                if (this.RoleCode.Equals("CLT") || this.RoleCode.Equals("ADJ"))
                {
                    //sClaimantNumber = this.DataItem.DataItemValue("CLAIMANT_NUMBER");
                    //sDisplayRole += " (" + sClaimantNumber + ")";
                    sDisplayRole = this.DataItem.DataItemValue("TREE_LABEL");               
                }
                // End SIW8170
                return sDisplayRole;
            }
        }
        // End SIW490

        public string RoleCode
        {
            get
            {
                if (Node != null)
                    return XML.XMLExtractCodeAttribute(xmlThisNode);
                else
                    return "";
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sdtCode, value);
                if (value == "")
                    CheckPIEntry();
            }
        }

        public string IDPrefix
        {
            get
            {
                // SI06650 return Constants.sCCPXmlEntityIDPrefix;
                return Constants.sPIIDPfx;
            }
        }

        public string EntityID
        {
            get
            {
                if (Node != null)
                {
                    // SI06650 return XML.XMLExtractEntityAttribute(xmlThisNode);
                    // Start SI06650
                    string sEntID;
                    sEntID = Constants.sPIEntId;
                    sEntID = XML.XMLExtractEntityAttribute(xmlThisNode, sEntID);
                    if (string.IsNullOrEmpty(sEntID))
                    {
                        sEntID = XML.XMLExtractEntityAttribute(xmlThisNode);
                    }
                    //Start SIW493
                    if (sEntID == "")
                        sEntID=PartyInvolvedID;
                    //End SIW493
                    return sEntID;

                    // End SI06650
                }
                else
                {
                    return "";
                }
            }
            set
            {
                // SI06650 XML.XMLInsertEntity_Node(putNode, value);
                XML.XMLInsertEntity_Node(putNode, value, Constants.sPIEntId); // SI06650
                if (value == "" || value == null || value == "0")
                    CheckPIEntry();
            }
        }

        // Start SI06650

        public string PartyInvolvedID
        {
            get
            {// Start SIW360
                string sPIId = string.Empty;
                string sStrData = string.Empty; 

                if (Node != null)
                {
                    sPIId = "";
                    sStrData = XML.XMLGetAttributeValue(xmlThisNode, Constants.sPIID); //SIW360
                    if (!string.IsNullOrEmpty(sStrData))
                    {
                        if ((StringUtils.Left(sStrData, 2) == Constants.sPIIDPfx) &&
                        (sStrData.Length >= 3))
                            sPIId = StringUtils.Right(sStrData, sStrData.Length - 2);
                       //Start SIW493
                        else  //For backward compatibility
                            if(StringUtils.Left(sStrData,2) ==Constants.sCCPXmlEntityIDPrefix && sStrData.Length >=4)
                                sPIId = StringUtils.Right(sStrData, sStrData.Length - 3);
                       //End SIW493
                    }
                }
                return sPIId;
            }//End SIW360
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sPIID, value);
                if (value == "")
                {
                    CheckPIEntry();
                }
            }
        }

        // End SI06650

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        protected override void ResetParent()
        {
            Node = null;
            xmlNodeList = null;
        }

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        protected override bool AllowNodeNameOverride
        {
            get { return true; }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNodeList List
        {
            get { return NodeList; }
        }

        //Start SIN7750
        public string InsuredName
        {
            get
            {
                return Utils.getData(Node, Constants.sInsuredName);
            }
            set
            {

                Utils.putData(putNode, Constants.sInsuredName, value, Utils.sNodeOrder);
            }
        }
        //End SIN7750

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                m_DataItem = null;
                m_Comment = null;
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    // SI06650 putPartyInvolved("", "", "");
                    putPartyInvolved("", "", "", ""); // SI06650 
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                m_DataItem = null;
                m_Comment = null;
                m_PartyInvolved = null;
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    //SIW529 m_DataItem.Document = Document;
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
                if (m_DataItem == null)
                {
                    //SIW529 m_DataItem.Document = Document;
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                }
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //START SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        public XMLEntity Entity
        {
            get
            {
                //Start SIW529
                if (m_Entities== null)
                {
                    m_Entities = new XMLEntities();
                    LinkXMLObjects(m_Entities);
                }
                if (m_Entity == null)
                {
                    m_Entity = new XMLEntity();
                    //SIW529 m_Entity.Document = Document;
                    //LinkXMLObjects(m_Entity);
                    m_Entity = m_Entities.Entity;
                }
                // End SIW529
                m_Entity.getEntitybyID(EntityID);
                return m_Entity;
            }
        }

        // SI06650 public XmlNode findPartyInvolved(string sID, string srole)
        public XmlNode findPartyInvolvedByEntity(string sID, string srole) // SI06650
        {
            if (sID != EntityID || RoleCode != srole)
            {
                getFirst();
                while (Node != null)
                {
                    if (sID == EntityID && RoleCode == srole)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getPartyInvolvedbyRole(string srole, bool reset)
        {
            if (reset)
                getFirst();
            else           //SIW529
                getNext(); //SIW529
            while (Node != null)
            {
                // SI06650 if (RoleCode == Role)
                if (RoleCode == srole) // SI06650
                    break;
                getNext();
            }
            return Node;
        }

        public XmlNode findpartyInvolvedbyRole(string srole)
        {
            if (RoleCode != srole)
            {
                getFirst();
                while (Node != null)
                {
                    if (RoleCode == srole)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        // SI06650 public XmlNode findPartyInvolvedbyID(string sID)
        public XmlNode findPartyInvolved(string sID) // SI06650
        {
            // SI06650 if (sID != EntityID)
            if (sID != PartyInvolvedID) // SI06650
            {
                getFirst();
                while (Node != null)
                {
                    // SI06650 if (sID == EntityID)
                    if (sID == PartyInvolvedID) // SI06650
                        break;
                    getNext();
                }
            }
            return Node;
        }
        //Start SIW403
        public string ExpiryDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sPIExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sPIExpDate, value, NodeOrder);
            }
        }
        //End SIW403
    }
}
