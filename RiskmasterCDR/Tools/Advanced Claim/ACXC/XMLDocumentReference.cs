/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLDocumentReference : XMLXCReferenceNode
    {
        /*' <!-- Multiple Levels -->
        '  <DOCUMENT_REFERENCE DOCUMENT_ID="DOCxxx">
        '     <!-- Multiple Occurrences -->*/

        private XMLDocument m_Document;
        
        public XMLDocumentReference()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_Document = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add("");
        }

        protected override void CheckEntry()
        {
            xEntry(DocumentID);
        }

 
        protected override string DefaultNodeName
        {
            get { return Constants.sDocReference; }
        }

        public XmlNode putDocument(string sID)
        {
            Node = null;
            if (sID != "" && sID != null)
                findDocumentRefbyId(sID);
            if (Node == null)
                Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            DocumentID = sID;
            return Node;
        }

        public XmlNode getDocument(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode findDocumentRefbyId(string sID)
        {
            if (sID != DocumentID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sID == DocumentID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public string DocumentID
        {
            get
            {
                string strdata = "";
                if (xmlThisNode != null)
                    strdata = XML.XMLGetAttributeValue(xmlThisNode, Constants.sDocID);
                return ((Utils)Utils).extDocumentID(strdata);
            }
            set
            {
                if (xmlThisNode == null)
                    putDocument(value);
                else
                    XML.XMLSetAttributeValue(xmlThisNode, Constants.sDocID, Constants.sDocIDPfx + value.Trim());
                if (value == "" || value == null || value == "0")
                    CheckEntry();
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    putDocument("");
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                base.Node = value;
                m_Document = null;
            }
        }

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
        }

        public XMLDocument Doc
        {
            get
            {
                if (m_Document == null)
                {   //SIW529
                    m_Document = new XMLDocument();
                    LinkXMLObjects(m_Document);
                }   //SIW529
                m_Document.getDocumentbyID(DocumentID);
                return m_Document;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
    }
}
