/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors 
 * 11/07/2008 | SI06423a|   KCB      | Update to previous field Mapping
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label        
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList               
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 08/18/2011 | SIW7419  |    RG     | Demand Offer added  
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLVehLoss : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*<VEHICLE_LOSS>
        '    <UNIT_REFERENCE ID="UNTxxx"/>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '    <LOSS_DESCRIPTION>description of loss</LOSS_DESCRIPTION>
        '    <DRIVER_RELATION_TO_OWNER CODE="rltn>relationship</DRIVER_RELATION_TO_OWNER>
        '    <USED_WITH_PERMISSION INDICATOR="YES|NO"/>
        '    <DAMAGE_DESCRIPTION>description of damage</DAMAGE_DESCRIPTION>
        '    <DAMAGE_DESC_HTML>description of damage in HTML format</DAMAGE_DESC_HTML>   'SI06369
        '    <ESTIMATED_DAMAGE_AMOUNT>$$$$$$$<ESTIMATED_DAMAGE_AMOUNT>
        '    <ACTUAL_DAMAGE_AMOUNT>$$$$$$$<ACTUAL_DAMAGE_AMOUNT>
        '    <DRIVEABLE INDICATOR="YES|NO"/>
        '    <NON_VEH_PROP_DAMAGE INDICATOR="YES|NO">                       'SI06423a
        '    <RENTAL>
        '       <DAILY_CHARGE>daily rental charge</DAILY_CHARGE>
        '       <START_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <END_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <VENDOR ID="ENTxxxx>vendor name</VENDOR>
        '    </RENTAL>
        '    <CAN_BE_SEEN>
        '       <WHERE ID="ADRxxx">where is property for viewing</WHERE>
        '       <DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <AFTER_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '       <BEFORE_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '    </CAN_BE_SEEN>
        '    <CHARGES TYPE=[TOWING, CLEANING, REPAIRS, RENTAL, OTHER]>$$$$$</CHARGES>
        '       <!-- Multiple Charges Allowed --->
        '    <OTHER_INSURANCE>
        '      <POLICY_NUMBER>policynumber</POLICY_NUMBER
        '      <CARRIER>carriername</CARRIER>
        '      <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <COVERAGE>
        '          <COVERAGE_TYPE CODE="xx">Coverage Type</COVERAGE_TYPE>
        '          <COVERAGE_LIMIT CODE="xx" AMOUNT="amount">limit type</COVERAGE_LIMIT>
        '             <!-- Multiple Limits Allowed -->
        '          <COVERAGE_DEDUCTIBLE CODE="xx" AMOUNT="xx">deductible type</COVERAGE_DEDUCTIBLE>
        '             <!-- Multiple Deductibles Allowed -->
        '      </COVERAGE>
        '             <!-- Multiple Coverages Allowed -->
        '    </OTHER_INSURANCE>
        '    <REPAIR_ESTIMATE_DETAILS>
        '       <REPAIR_ESTIMATE>
        '          <REPAIR_ITEM CODE="xx">itemname</REPAIR_ITEM>
        '          <ITEM_UCR CODE="itemucrcode" SOURCE="ucrsourcecatalog"/>
        '          <COST>itemcost</COST>
        '          <LABOR HOURS="hours" RATE="rate">laborcost</LABOR>
        '          <TYPE_OF_REPAIR CODE="REPLACE|REPAIR|NEW|REBUILT">
        '          <VENDOR ID="ENTxxxx/>
        '       </REPAIR_ESTIMATE>
        '          <!-- Multiple Items allowed -->
        '    </REPAIR_ESTIMATE_DETAILS>
        '    <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '      <!--Muliple Party Involved nodes allowed -->
        '      <!Multiple Demand Offers Allowed>              //SIW7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '    <COMMENT_REFERENCE ID="CMTxx"/>
        '      <!-- multiple entries -->        
        '</VEHICLE_LOSS>*/

        private XMLLossInfo m_LossInfo;
        private XMLDemandOffer m_DemandOffer;     //SIW7419
        
        public XMLVehLoss()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;

            //sThisNodeOrder = new ArrayList(4);                //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(5);                  //(SI06023 - Implemented in SI06333) //SIW490
            sThisNodeOrder = new ArrayList(8);                  //SIW490   //SIW7419
            sThisNodeOrder.Add(Constants.sVLUnitReference);
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);        //SIW490
            sThisNodeOrder.Add(Constants.sVLDriverRltnToOwner);
            sThisNodeOrder.Add(Constants.sVLDriveable);
            sThisNodeOrder.Add(Constants.sVLNonVehPropDamage);  //SI06423a
            sThisNodeOrder.Add(Constants.sVLLossInfo);
            sThisNodeOrder.Add(Constants.sVLDemandOffer);        //SIW7419
            //sThisNodeOrder.Add(Constants.sVLTechKey);           //(SI06023 - Implemented in SI06333) //SIW360
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sVLNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "");
        }

        public XmlNode Create(string sUnitReference, string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                UnitReferenceID = sUnitReference;
                LossDescription = sdescription;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLVehLoss.Create");
                return null;
            }
        }

        public string UnitReferenceID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, Constants.sVLUnitReference, Constants.sVLUnitReferenceID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sUntIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sVLUnitReference, Constants.sVLUnitReferenceID,
                                   Constants.sUntIDPfx + value, NodeOrder);
            }
        }

        public XMLUnit Unit
        {
            get
            {
                XMLUnit xmlUnit = new XMLUnit();
                LinkXMLObjects(xmlUnit); //SIW529
                xmlUnit.Parent.Parent = Parent;
                if (UnitReferenceID != "" && UnitReferenceID != null)
                    xmlUnit.getUnitbyID(UnitReferenceID);
                return xmlUnit;
            }
        }

        //Start SIW139
        public XmlNode putDriverRltnToOwner(string sDesc, string sCode)
        {
            return putDriverRltnToOwner(sDesc, sCode, "");
        }

        public XmlNode putDriverRltnToOwner(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sVLDriverRltnToOwner, sDesc, sCode, NodeOrder, scodeid);
        }

        public string DriverRltnToOwner
        {
            get
            {
                return Utils.getData(Node, Constants.sVLDriverRltnToOwner);
            }
            set
            {
                Utils.putData(putNode, Constants.sVLDriverRltnToOwner, value, NodeOrder);
            }
        }

        public string DriverRltnToOwner_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sVLDriverRltnToOwner);
            }
            set
            {
                Utils.putCode(putNode, Constants.sVLDriverRltnToOwner, value, NodeOrder);
            }
        }

        public string DriverRltnToOwner_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sVLDriverRltnToOwner);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sVLDriverRltnToOwner, value, NodeOrder);
            }
        }
        //End SIW139

        public string Driveable
        {
            get
            {
                return Utils.getBool(Node, Constants.sVLDriveable, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sVLDriveable, "", value, NodeOrder);
            }
        }

        //Begin SI06423a
        public string NonVehiclePropertyDamageFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sVLNonVehPropDamage, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sVLNonVehPropDamage, "", value, NodeOrder);
            }
        }
        //End SI06423a

        public XMLLossInfo LossInfo
        {
            get
            {
                if (m_LossInfo == null)
                {
                    m_LossInfo = new XMLLossInfo();
                    m_LossInfo.Parent = this;
                    //SIW529 LinkXMLObjects(m_LossInfo);
                }
                return m_LossInfo;
            }
            set
            {
                m_LossInfo = value;
            }
        }

        public string LossDescription
        {
            get
            {
                return LossInfo.LossDescription;
            }
            set
            {
                LossInfo.LossDescription = value;
            }
        }

        public string UsedWithPermission
        {
            get
            {
                return LossInfo.UsedWithPermission;
            }
            set
            {
                LossInfo.UsedWithPermission = value;
            }
        }

        public string DamageDescription
        {
            get
            {
                return LossInfo.DamageDescription;
            }
            set
            {
                LossInfo.DamageDescription = value;
            }
        }
        
        //Start SI06369
        public string DamageDescriptionHTML
        {
            get
            {
                return LossInfo.DamageDescriptionHTML;
            }
            set
            {
                LossInfo.DamageDescriptionHTML = value;
            }
        }
        //End SI06369

        public string EstDamageAmount
        {
            get
            {
                return LossInfo.EstDamageAmount;
            }
            set
            {
                LossInfo.EstDamageAmount = value;
            }
        }

        public string ActDamageAmount
        {
            get
            {
                return LossInfo.ActDamageAmount;
            }
            set
            {
                LossInfo.ActDamageAmount = value;
            }
        }

        public string RentalCharge
        {
            get
            {
                return LossInfo.RentalCharge;
            }
            set
            {
                LossInfo.RentalCharge = value;
            }
        }

        public string RentalStartDate
        {
            get
            {
                return LossInfo.RentalStartDate;
            }
            set
            {
                LossInfo.RentalStartDate = value;
            }
        }

        public string RentalEndDate
        {
            get
            {
                return LossInfo.RentalEndDate;
            }
            set
            {
                LossInfo.RentalEndDate = value;
            }
        }

        public string RentalVendor
        {
            get
            {
                return LossInfo.RentalVendor;
            }
            set
            {
                LossInfo.RentalVendor = value;
            }
        }

        public string RentalVendorID
        {
            get
            {
                return LossInfo.RentalVendorID;
            }
            set
            {
                LossInfo.RentalVendorID = value;
            }
        }

        public XMLEntity RentalVendorEntity
        {
            get
            {
                return LossInfo.RentalVendorEntity;
            }
        }

        public XmlNode addRprEst()
        {
            return LossInfo.addRprEst();
        }

        public XmlNode getRprEst(bool reset)
        {
            return LossInfo.getRprEst(reset);
        }

        public XmlNode putRprEstItem(string sdesc, string scode)
        {
            return LossInfo.putRprEstItem(sdesc, scode);
        }

        public string RprEstItem
        {
            get
            {
                return LossInfo.RprEstItem;
            }
            set
            {
                LossInfo.RprEstItem = value;
            }
        }

        public string RprEstItem_Code
        {
            get
            {
                return LossInfo.RprEstItem_Code;
            }
            set
            {
                LossInfo.RprEstItem_Code = value;
            }
        }

        public string RprEstCost
        {
            get
            {
                return LossInfo.RprEstCost;
            }
            set
            {
                LossInfo.RprEstCost = value;
            }
        }

        public string RprEstLaborCost
        {
            get
            {
                return LossInfo.RprEstLaborCost;
            }
            set
            {
                LossInfo.RprEstLaborCost = value;
            }
        }

        public string RprEstLaborHours
        {
            get
            {
                return LossInfo.RprEstLaborHours;
            }
            set
            {
                LossInfo.RprEstLaborHours = value;
            }
        }

        public string RprEstLaborRate
        {
            get
            {
                return LossInfo.RprEstLaborHours;
            }
            set
            {
                LossInfo.RprEstLaborHours = value;
            }
        }

        public string RprEstUCR
        {
            get
            {
                return LossInfo.RprEstUCR;
            }
            set
            {
                LossInfo.RprEstUCR = value;
            }
        }

        public string RprEstUCRSource
        {
            get
            {
                return LossInfo.RprEstUCRSource;
            }
            set
            {
                LossInfo.RprEstUCRSource = value;
            }
        }

        public xcRepairType RprEstItemTypeRepair
        {
            get
            {
                return LossInfo.RprEstItemTypeRepair;
            }
            set
            {
                LossInfo.RprEstItemTypeRepair = value;
            }
        }

        public string RprEstItemTypeRepair_Code
        {
            get
            {
                return LossInfo.RprEstItemTypeRepair_Code;
            }
            set
            {
                LossInfo.RprEstItemTypeRepair_Code = value;
            }
        }


        public string RprEstItemVendor
        {
            get
            {
                return LossInfo.RprEstItemVendor;
            }
            set
            {
                LossInfo.RprEstItemVendor = value;
            }
        }

        public string RprEstItemVendorID
        {
            get
            {
                return LossInfo.RprEstItemVendorID;
            }
            set
            {
                LossInfo.RprEstItemVendorID = value;
            }
        }

        public XMLEntity RprEstItemVendorEntity
        {
            get
            {
                return LossInfo.RprEstItemVendorEntity;
            }
        }

        public string CanBeSeenWhere
        {
            get
            {
                return LossInfo.CanBeSeenWhere;
            }
            set
            {
                LossInfo.CanBeSeenWhere = value;
            }
        }

        public string CanBeSeenWhereID
        {
            get
            {
                return LossInfo.CanBeSeenWhereID;
            }
            set
            {
                LossInfo.CanBeSeenWhereID = value;
            }
        }

        public XMLAddress CanBeSeenAddress
        {
            get
            {
                return LossInfo.CanBeSeenAddress;
            }
        }

        public string CanBeSeenDate
        {
            get
            {
                return LossInfo.CanBeSeenDate;
            }
            set
            {
                LossInfo.CanBeSeenDate = value;
            }
        }

        public string CanBeSeenAfterTime
        {
            get
            {
                return LossInfo.CanBeSeenAfterTime;
            }
            set
            {
                LossInfo.CanBeSeenAfterTime = value;
            }
        }

        public string CanBeSeenBeforeTime
        {
            get
            {
                return LossInfo.CanBeSeenBeforeTime;
            }
            set
            {
                LossInfo.CanBeSeenBeforeTime = value;
            }
        }

        public int ChargeCount
        {
            get
            {
                return LossInfo.ChargeCount;
            }
        }

        public XmlNode addCharge()
        {
            return LossInfo.addCharge();
        }

        public XmlNode getCharge(bool reset)
        {
            return LossInfo.getCharge(reset);
        }

        public string ChargeAmount
        {
            get
            {
                return LossInfo.ChargeAmount;
            }
            set
            {
                LossInfo.ChargeAmount = value;
            }
        }

        public string ChargeType
        {
            get
            {
                return LossInfo.ChargeType;
            }
            set
            {
                LossInfo.ChargeType = value;
            }
        }

        public string OthrInsPolicy
        {
            get
            {
                return LossInfo.OthrInsPolicy;
            }
            set
            {
                LossInfo.OthrInsPolicy = value;
            }
        }

        public string OthrInsCarrier
        {
            get
            {
                return LossInfo.OthrInsCarrier;
            }
            set
            {
                LossInfo.OthrInsCarrier = value;
            }
        }

        public string OthrInsCarrierID
        {
            get
            {
                return LossInfo.OthrInsCarrierID;
            }
            set
            {
                LossInfo.OthrInsCarrierID = value;
            }
        }

        public XMLEntity OthrInsCarrierEntity
        {
            get
            {
                return LossInfo.OthrInsCarrierEntity;
            }
        }

        public string OthrInsEffectiveDate
        {
            get
            {
                return LossInfo.OthrInsEffectiveDate;
            }
            set
            {
                LossInfo.OthrInsEffectiveDate = value;
            }
        }

        public string OthrInsExpirationDate
        {
            get
            {
                return LossInfo.OthrInsExpirationDate;
            }
            set
            {
                LossInfo.OthrInsExpirationDate = value;
            }
        }

        public int OthrInsCoverageCount
        {
            get
            {
                return LossInfo.OthrInsCoverageCount;
            }
        }

        public XmlNode addOtherInsCoverage()
        {
            return LossInfo.addOthrInsCoverage();
        }

        public XmlNode getOthrInsCoverage(bool reset)
        {
            return LossInfo.getOthrInsCoverage(reset);
        }

        public XmlNode putOthrInsCovType(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovType(sdesc, scode);
        }

        public string OthrInsCovType
        {
            get
            {
                return LossInfo.OthrInsCovType;
            }
            set
            {
                LossInfo.OthrInsCovType = value;
            }
        }

        public string OthrInsCovType_Code
        {
            get
            {
                return LossInfo.OthrInsCovType_Code;
            }
            set
            {
                LossInfo.OthrInsCovType_Code = value;
            }
        }

        public XmlNode addOthrInsCovLimit()
        {
            return LossInfo.addOthrInsCovLimit();
        }

        public int OthrInsCovLimitCount
        {
            get
            {
                return LossInfo.OthrInsCovLimitCount;
            }
        }

        public XmlNode getOthrInsCovLimit(bool reset)
        {
            return LossInfo.getOthrInsCovLimit(reset);
        }

        public XmlNode putOthrInsCovLimit(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovLimit(sdesc, scode);
        }

        public string OthrInsCovLimit
        {
            get
            {
                return LossInfo.OthrInsCovLimit;
            }
            set
            {
                LossInfo.OthrInsCovLimit = value;
            }
        }

        public string OthrInsCovLimit_Code
        {
            get
            {
                return LossInfo.OthrInsCovLimit_Code;
            }
            set
            {
                LossInfo.OthrInsCovLimit_Code = value;
            }
        }

        public string OthrInsCovLimitAmount
        {
            get
            {
                return LossInfo.OthrInsCovLimitAmount;
            }
            set
            {
                LossInfo.OthrInsCovLimitAmount = value;
            }
        }

        public XmlNode addOthrInsCovDeductible()
        {
            return LossInfo.addOthrInsCovDeductible();
        }

        public int OthrInsCovDeductibleCount
        {
            get
            {
                return LossInfo.OthrInsCovDeductibleCount;
            }
        }

        public XmlNode getOthrInsCovDeductible(bool reset)
        {
            return LossInfo.getOthrInsCovDeductible(reset);
        }

        public XmlNode putOthrInsCovDeductible(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovDeductible(sdesc, scode);
        }

        public string OthrInsCovDeductible
        {
            get
            {
                return LossInfo.OthrInsCovDeductible;
            }
            set
            {
                LossInfo.OthrInsCovDeductible = value;
            }
        }

        public string OthrInsCovDeductible_Code
        {
            get
            {
                return LossInfo.OthrInsCovDeductible_Code;
            }
            set
            {
                LossInfo.OthrInsCovDeductible_Code = value;
            }
        }

        public string OthrInsCovDeductibleAmount
        {
            get
            {
                return LossInfo.OthrInsCovDeductibleAmount;
            }
            set
            {
                LossInfo.OthrInsCovDeductibleAmount = value;
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //START SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490
      
        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        private void subClearPointers()
        {
            LossInfo = null;
            DemandOffer = null;            //SIW7419
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                return LossInfo.PartyInvolved;
            }
            set
            {
                LossInfo.PartyInvolved = null;
            }
        }

        //Start SIW7419
        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }
        //SIW7419

        public XMLCommentReference CommentReference
        {
            get
            {
                return LossInfo.CommentReference;
            }
            set
            {
                LossInfo.CommentReference = value;
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).VehicleLoss = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW529
                }
                return (XMLClaim)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)           //SIW7419
                    DemandOffer.getFirst();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
        
        public XmlNode getVehLoss(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getVehLossByReference(string sref)
        {
            if (sref != UnitReferenceID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sref == UnitReferenceID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
        //Start SIW493
        public XmlNode getVehLossByTechkey(string stc)
        {
            bool rst = false;
            if (stc != Techkey)
            {
                rst = true;
                while (getVehLoss(rst) != null)
                {
                    rst = false;//SIW529
                    if (Techkey == stc)
                        break;
                }
            }
            return Node;
        }
        //End SIW493
    }
}
