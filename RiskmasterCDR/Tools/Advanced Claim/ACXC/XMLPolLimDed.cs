﻿

/**********************************************************************************************
 *   Date     |    SI        | Programmer | Description                                            *
 **********************************************************************************************
 * 11/05/2011 |   SIW7123    |    AV     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLPolLimDed : XMLXCNodeWithList
    {


        /*****************************************************************************
        '<POLICY>
        '   <LIMIT_DEDUCTIBLE>
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '           <TYPE CODE="">Type</TYPE>
        '           <COVERAGE CODE="">Coverage</COVERAGE>
        '           <APPLICATION CODE="">Applicatioin</APPLICATION>
        '           <AMOUNT>2000</AMOUNT>
        '           <USED_AMOUNT>1000</USED_AMOUNT>
        '           <PART_OF>50</PART_OF>
        '           <AGGRIGATE></AGGRIGATE>
        '           <DESCRIPTION>Test</DESCRIPTION>
        '   </LIMIT_DEDUCTIBLE>
        '</POLICY>*/


        public XMLPolLimDed()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlNodeList = null;
            xmlThisNode = null;
            m_Parent = null;                        

            sThisNodeOrder = new ArrayList(9);
            sThisNodeOrder.Add(Constants.sStdTechKey);
            sThisNodeOrder.Add(Constants.sPolLimDedType);
            sThisNodeOrder.Add(Constants.sPolLimDedCoverage);
            sThisNodeOrder.Add(Constants.sPolLimDedApp);
            sThisNodeOrder.Add(Constants.sPolLimDedAmount);
            sThisNodeOrder.Add(Constants.sPolLimDedUsedAmount);
            sThisNodeOrder.Add(Constants.sPolLimDedPartOf);
            sThisNodeOrder.Add(Constants.sPolLimDedAggregate);
            sThisNodeOrder.Add(Constants.sPolLimDedDescription);
        }


        protected override string DefaultNodeName
        {
            get { return Constants.sPolLimDedNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "","");
        }

        public XmlNode Create(string sType, string sTypeCode, string sCoverage, string sCoverageCode, string sApp, string sAppCode, string sAmount,string sDesc)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);

                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                LimDedType = sType;
                LimDedType_Code = sTypeCode;
                CoverageType = sCoverage;
                CoverageType_Code = sCoverageCode;
                ApplicationType = sApp;
                ApplicationType_Code = sAppCode;
                PolLimDedAmount = sAmount;
                PolLimDedDescription = sDesc;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLPolLimDed.Create");
                return null;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Parent != null)
                ((XMLPolicy)m_Parent).PolLimDed = this;
            Node = null;
        }

        private void subClearPointers()
        {           
           
        }
        public string PolLimDed_ID
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sPolLimDedID);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sPolLimDedID, value);
            }
        }
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }
        // Code for Combo Type Code

        public XmlNode putLimDedType(string sDesc, string sCode)
        {
            return putLimDedType(sDesc, sCode, "");
        }
        public XmlNode putLimDedType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolLimDedType, sDesc, sCode, NodeOrder, scodeid);
        }
        public string LimDedType
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLimDedType);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolLimDedType, value, NodeOrder);
            }
        }
        public string LimDedType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolLimDedType);  
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolLimDedType, value, NodeOrder);    
            }
        }
        public string LimDedType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolLimDedType);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolLimDedType, value, NodeOrder);    
            }
        }

        // Code for Combo Coverage Type
        public XmlNode putCoverageType(string sDesc, string sCode)
        {
            return putCoverageType(sDesc, sCode, "");
        }
        public XmlNode putCoverageType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolLimDedCoverage, sDesc, sCode, NodeOrder, scodeid);
        }
        public string CoverageType
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLimDedCoverage);
            }
            set
            {
                Utils.putData(putNode,Constants.sPolLimDedCoverage,value, NodeOrder);
            }
        }
        public string CoverageType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPolLimDedCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolLimDedCoverage, value, NodeOrder);   
            }
        }
        public string CoverageType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolLimDedCoverage);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolLimDedCoverage, value, NodeOrder);
            }
        }

        //Code for Combo Application
        public XmlNode putApplicationType(string sDesc, string sCode)
        {
            return putApplicationType(sDesc, sCode, "");
        }
        public XmlNode putApplicationType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPolLimDedApp, sDesc, sCode, NodeOrder, scodeid);
        }
        public string ApplicationType
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLimDedApp);
            }
            set
            {
                Utils.putData(putNode, Constants.sPolLimDedApp, value, NodeOrder);
            }
        }
        public string ApplicationType_Code
        {
            get
            {
                return Utils.getData(Node, Constants.sPolLimDedApp);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPolLimDedApp, value, NodeOrder);
            }
        }
        public string ApplicationType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPolLimDedApp);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPolLimDedApp, value, NodeOrder);
            }
        } 
         public string PolLimDedAmount
         {
             get
             {
                 return Utils.getData(Node, Constants.sPolLimDedAmount);
             }
             set
             {
                 Utils.putData(putNode, Constants.sPolLimDedAmount, value, NodeOrder);
             }
         }

         public string PolLimDedUsedAmount
         {
             get
             {
                 return Utils.getData(Node, Constants.sPolLimDedUsedAmount);
             }
             set
             {
                 Utils.putData(putNode, Constants.sPolLimDedUsedAmount, value, NodeOrder);
             }
         }

         public string PolLimDedPartOf
         {
             get
             {
                 return Utils.getData(Node, Constants.sPolLimDedPartOf);
             }
             set
             {
                 Utils.putData(putNode, Constants.sPolLimDedPartOf, value, NodeOrder);
             }
         }
         public string PolLimDedAggregate
         {
             get
             {
                 return Utils.getData(Node, Constants.sPolLimDedAggregate);
             }
             set
             {
                 Utils.putData(putNode, Constants.sPolLimDedAggregate, value, NodeOrder);
             }
         }
         public string PolLimDedDescription
         {
             get
             {
                 return Utils.getData(Node, Constants.sPolLimDedDescription);
             }
             set
             {
                 Utils.putData(putNode, Constants.sPolLimDedDescription, value, NodeOrder);
             }
         }         

        public XmlNode getLimitDeductible(bool reset)
        {
            return getNode(reset);
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLPolicy();
                    ((XMLPolicy)m_Parent).PolLimDed = this;   //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getPolLimDed(bool reset)
        {
            return getNode(reset);
        }
    }


}
