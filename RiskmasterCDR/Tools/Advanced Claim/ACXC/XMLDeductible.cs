/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 09/23/2011| SIW7123 |    AV      | Add new child nodes to the deductibles
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLDeductible : XMLXCNodeWithList
    {
        /*****************************************************************************
        ' The cXMLDeductible class Creates and Retrieves Deductible nodes within a
        ' defined parent node
        '*****************************************************************************
        //Start SIW7123
        ' <PARENT_NODE>
        '   <DEDUCTIBLE ID="DEDxxx">
        '     <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '     <TYPE CODE="DED">Deductible</TYPE>
        '     <SPECIFIED CODE="OCCUR">Deductible description</SPECIFIED>
        '     <COVERAGE CODE="">coverage type</COVERAGE>
        '     <AMOUNT>2135</AMOUNT>
        '     <USED_AMOUNT>...</USED_AMOUNT>
        '     <PART_OF>...</PART_OF>
        '     <AGGREGATE>...</AGGREGATE>
        '     <DESCRIPTION>text</DESCRIPTION>
        '     <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
        '     <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
        '   </Deductible>
        '   <!-- Multiple Deductible Lines Allowed -->
        ' </PARENT_NODE>*/
        //End SIW7123
        
        public XMLDeductible()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;

            // SIW7123 sThisNodeOrder = new ArrayList(4);            //(SI06023 - Implemented in SI06333)
            //(SI06023 - Implemented in SI06333)//SIW360
            sThisNodeOrder = new ArrayList(11);              //SIW7123
            sThisNodeOrder.Add(Constants.sStdTechKey);      //SIN7147
            sThisNodeOrder.Add(Constants.sDedSpecified);
            sThisNodeOrder.Add(Constants.sDedAmt);
            sThisNodeOrder.Add(Constants.sDedEffDate);
            sThisNodeOrder.Add(Constants.sDedExpDate);

            //Start SIW7123
            sThisNodeOrder.Add(Constants.sStdTreeLabel);
            sThisNodeOrder.Add(Constants.sDedType);
            sThisNodeOrder.Add(Constants.sDedUsedAmount);
            sThisNodeOrder.Add(Constants.sDedCoverage);
            sThisNodeOrder.Add(Constants.sDedPartOf);
            sThisNodeOrder.Add(Constants.sDedAggregate);
            sThisNodeOrder.Add(Constants.sDedDescription);
            //End SIW7123
        }

        public override XmlNode Create()
        {
            return AddDeductible("", "", "", "", "");
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sDedNode; }
        }

        public XmlNode AddDeductible(string sDesc, string sCode, string sAmount, string sEffDate, string sExpDate)
        {
            xmlThisNode = XML.XMLaddNode(Parent_Node, NodeName, sThisNodeOrder);

            if (sDesc != "" && sDesc != null)
                Description = sDesc;
            if (sCode != "" && sCode != null)
                Deductible_Code = sCode;
            if (sAmount != "" && sAmount != null)
                Deductible = sAmount;
            if (sEffDate != "" && sEffDate != null)
                EffectiveDate = sEffDate;
            if (sExpDate != "" && sExpDate != null)
                ExpirationDate = sExpDate;

            return xmlThisNode;
        }

        public string Description
        {
            get
            {
                // SIW7123 return Utils.getData(Node, Constants.sDedSpecified);
                return Utils.getData(Node, Constants.sDedDescription); //SIW7123
            }
            set
            {
               // SIW7123 Utils.putData(putNode, Constants.sDedSpecified, value, sThisNodeOrder);
                Utils.putData(putNode, Constants.sDedDescription, value, sThisNodeOrder); //SIW7123
            }
        }

        public string Deductible_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDedSpecified);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDedSpecified, value, sThisNodeOrder);
            }
        }

        //Start SIW139
        public string Deductible_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDedSpecified);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDedSpecified, value, sThisNodeOrder);
            }
        }

        public XmlNode putDeductible(string sdesc, string scode)
        {
            return putDeductible(sdesc, scode, "");
        }

        public XmlNode putDeductible(string sdesc, string scode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDedSpecified, sdesc, scode, sThisNodeOrder, scodeid);
        }
        //End SIW139

        //Start SIW7123
        public XmlNode putCoverageType(string sDesc, string sCode)
        {
            return putCoverageType(sDesc, sCode, "");
        }
        public XmlNode putCoverageType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDedCoverage, sDesc, sCode, sThisNodeOrder, scodeid);
        }
          public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sDedID);
                string lid = "";
                if(StringUtils.Left(strdata,3)==Constants.sDedIDPFX && strdata.Length>=4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lDEDId = Globals.lDEDId + 1;
                    lid = Globals.lDEDId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sDedID, Constants.sDedIDPFX + lid);

            }
        }

        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, sThisNodeOrder);
            }
        }

        public string CoverageType
        {
            get
            {
                return Utils.getData(Node, Constants.sDedCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedCoverage, value, sThisNodeOrder);
            }
        }
        public string CoverageType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDedCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDedCoverage, value, sThisNodeOrder);
            }
        }
        public string CoverageType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDedCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDedCoverage, value, sThisNodeOrder);
            }
        }

        public string DedutibleDescription
        {
            get
            {
              return Utils.getData(Node, Constants.sDedSpecified);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedSpecified, value, sThisNodeOrder);
            }
        }

        public string Type_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDedType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDedType, value, sThisNodeOrder);
            }
        }

        public XmlNode putType(string sDesc, string sCode)
        {
            return putType(sDesc, sCode, "");
        }
        public XmlNode putType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDedType, sDesc, scodeid, sThisNodeOrder, scodeid);
        }

        public string Type_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDedType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDedType, value, sThisNodeOrder);
            }
        }

        public string TypeDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sDedType);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedType, value, sThisNodeOrder);
            }
        }
        public string UsedAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sDedUsedAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedUsedAmount, value, sThisNodeOrder);
            }
        }

        public string PartOf
        {
            get
            {
                return Utils.getData(Node, Constants.sDedPartOf);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedPartOf, value, sThisNodeOrder);
            }
        }

        public string Aggregate
        {
            get
            {
                return Utils.getData(Node, Constants.sDedAggregate);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedAggregate, value, sThisNodeOrder);
            }
        }

        //End SIW7123
        public string Deductible
        {
            get
            {
                return Utils.getData(Node, Constants.sDedAmt);
            }
            set
            {
                Utils.putData(putNode, Constants.sDedAmt, value, sThisNodeOrder);
            }
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDedEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDedEffDate, value, sThisNodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDedExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDedExpDate, value, sThisNodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return List;
            }
        }

        public XmlNodeList List
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getDeductible(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode Remove()
        {
            if (Node != null)
            {
                Parent_Node.RemoveChild(Node);
                getFirst();
            }
            return Node;
        }
    }
}
