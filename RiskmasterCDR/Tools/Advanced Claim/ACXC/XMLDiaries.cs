/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/22/2007 |         |    JTC     | Created
 * 03/27/2008 | SI06267 |    JTC     | Updated to reflect current .COM version
 * 04/15/2008 | SI06267 |    JTC     | Fixed access to subnodes
 * 05/05/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 07/21/2008 | SI06267 |    JTC     | Updated for HTML fields
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 11/18/2008 | SI06420 |   ASM      | Get first child
 * 11/20/2008 | SI06472 |   JTC      | Retrofit date split
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 03/09/2009 | SIW111  |    JTC     | Updates for Diary Tree
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 04/09/2010 | SIW404  |    JTC     | Restrict max number of extracted diaries
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label      
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/12/2010 | SIW529  |    KCB     | Fixes for removing static functions 
 * 03/22/2011 | SIW7038  |   BS      | To distinguish diary items from  tasks and to add policy number,
                                     |  insured name and document number on diary screen as read only fields.
 * 08/22/2011 | SIW7351 |     ubora  | Process Payment WPA activity
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLDiary : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /******************************************************************************************
        ' The cXMLDiary class provides the functionality to support and manage a Diary Node
        '******************************************************************************************
        '<DIARY ID="DIA#">
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '   <DATE_CREATED YEAR="####" MONTH="##" DAY="##"/>
        '   <TIME_CREATED HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '   <DATE_AFTER YEAR="####" MONTH="##" DAY="##"/>          'SI06267
        '   <DATE_COMPLETE YEAR="####" MONTH="##" DAY="##"/>
        '   <TIME_COMPLETE HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '   <AUTO_ID>#</AUTO_ID>
        '   <DELETED INDICATOR="TRUE|FALSE"/>
        '   <VOID INDICATOR="TRUE|FALSE"/>
        '   <ESTIMATED_TIME>#</ESTIMATED_TIME>
        '   <NOTIFY_FLAG INDICATOR="TRUE|FALSE"/>
        '   <ROUTE_FLAG INDICATOR="TRUE|FALSE"/>
        '   <ENTRY ID="##" NAME="(string)">Notes (string)</ENTRY>
        '   <ENTRY_NOTES_RTF>string</ENTRY_NOTES_RTF>
        '   <ENTRY_NOTES_HTML>string</ENTRY_NOTES_HTML>      'SI06267
        '   <PRIORITY>#</PRIORITY>
        '   <STATUS_OPEN INDICATOR="TRUE|FALSE"/>
        '   <AUTO_CONFIRM INDICATOR="TRUE|FALSE"/>
        '   <RESPONSE_DATE YEAR="####" MONTH="##" DAY="##"/>                          'SI06472
        '   <RESPONSE_TIME HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>        'SI06472
        '   <RESPONSE>Response (string)</RESPONSE> 
        '   <RESPONSE_RTF>string</RESPONSE_RTF>
        '   <RESPONSE_HTML>string</RESPONSE_HTML>            'SI06267
        '   <ASSIGNED_USER>username</ASSIGNED_USER>
        '   <ASSIGNING_USER>username</ASSIGNING_USER>
        '   <ASSIGNED_GROUP>Group Name (string)</ASSIGNED_GROUP>
        '   <IS_ATTACHED INDICATOR="TRUE|FALSE"/>
        '   <ATTACH_TABLE>TableID</ATTACH_TABLE>
        '   <ATTACH_RECORD_ID>##</ATTACH_RECORD_ID>
        '   <REGARDING>string</REGARDING>
        '   <CLAIMANT>#</CLAIMANT>
        '   <CLAIM_ID>Claim ID</CLAIM_ID>
        '   <EVENT_ID>Event ID</EVENT_ID>
        '   <ROUTABLE INDICATOR="TRUE|FALSE"/>
        '   <ROLLABLE INDICATOR="TRUE|FALSE"/>
        '   <ROUTED_TO_ID>user id</ROUTED_TO_ID>
        '   <ROUTED_FROM_ID>user id</ROUTED_FROM_ID>
        '   <AUTO_LATE_NOTIFY INDICATOR="TRUE|FALSE"/>
        '   <SERIES_ID>#</SERIES_ID>
        '   <RESERVE_ID>#</RESERVE_ID>
        '   <PAY_POST_FLAG>#</PAY_POST_FLAG>
        '   <TE_TRACKED INDICATOR="TRUE|FALSE"/>
        '   <ACTIVITIES NEXTID="#" COUNT="#">
        '      <ACTIVITY ID="#">
        '         <PARENT_ENTRY_ID>#</PARENT_ENTRY_ID>
        '         <ACTIVITY_CODE CODE="...">...</ACTIVITY_CODE>
        '         <ENTRY>3750</ENTRY>
        '      </ACTIVITY>
        '   </ACTIVITIES>
        '   <EXPENSES NEXTID="#" COUNT="#">
        '      <EXPENSE ID="#">
        '         <ENTRY_ID>#</ENTRY_ID>
        '         <LINE_NUMBER>#</LINE_NUMBER>
        '         <START_DATE YEAR="####" MONTH="##" DAY="##"/>
        '         <START_TIME HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '         <END_DATE YEAR="####" MONTH="##" DAY="##"/>
        '         <END_TIME Hour="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '         <DESCRIPTION CODE="...">...</DESCRIPTION>
        '         <UNIT CODE="...">...</UNIT>
        '         <REASON CODE="...">...</REASON>
        '         <STATUS CODE="...">...</STATUS>
        '         <DISAPPROVAL_REASON CODE="...">...</DISAPPROVAL_REASON>
        '         <APPROVED_BY>user id</APPROVED_BY>
        '         <APPROVED_DATE YEAR="####" MONTH="##" DAY="##"/>
        '         <TRANSACTION_TYPE CODE="...">...</TRANSACTION_TYPE>
        '         <UNITS>#</UNITS>
        '         <AMOUNT_PER_UNIT>#</AMOUNT_PER_UNIT>
        '         <AMOUNT_NET>#</AMOUNT_NET>
        '         <MANUAL_NET INDICATOR="TRUE|FALSE"/>
        '         <AMOUNT_APPROVED>#</AMOUNT_APPROVED>
        '         <AMOUNT_PAID>#</AMOUNT_PAID>
        '         <DELETED INDIACATOR="TRUE|FALSE"/>
        '         <BILLABLE INDICATOR="TRUE|FALSE"/>\
        '         <CRC>#</CRC>
        '      </EXPENSE>
        '   </EXPENSES>
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '     <!-- multiple entries -->
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '     <!--Muliple Party Involved nodes allowed -->        
        '</DIARY>
        '******************************************************************************************/

        private XmlNode m_xmlAct;
        private XmlNode m_xmlActs;
        private XmlNodeList m_xmlActList;
        private XmlNode m_xmlExp;
        private XmlNode m_xmlExps;
        private XmlNodeList m_xmlExpList;
        private XMLDiaries m_Diaries;
        private XMLCommentReference m_Comment;
        private XMLPartyInvolved m_PartyInvolved;
        private Dictionary<object, object> dctExpPP;
        private ArrayList sActNodeOrder;
        private ArrayList sExpNodeOrder;
        
        public XMLDiary()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            //Start SI06472
            //xmlThisNode = null;     
            //SIW529 Parent = null;
            //SIW529 ResetParent();
            //End SI06472
            dctExpPP = new Dictionary<object, object>();
            dctExpPP.Add(Constants.iDiaExpPPUnspecified, Constants.sDiaExpPPUnspecified);
            dctExpPP.Add(Constants.sDiaExpPPUnspecified, Constants.iDiaExpPPUnspecified);
            dctExpPP.Add(Constants.iDiaExpPPPost, Constants.sDiaExpPPPost);
            dctExpPP.Add(Constants.sDiaExpPPPost, Constants.iDiaExpPPPost);
            dctExpPP.Add(Constants.iDiaExpPPPay, Constants.sDiaExpPPPay);
            dctExpPP.Add(Constants.sDiaExpPPPay, Constants.iDiaExpPPPay);

            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SI06472 //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);        //SIW490
            sThisNodeOrder.Add(Constants.sDiaEntry);            //SI06472
            sThisNodeOrder.Add(Constants.sDiaDateCreated);
            sThisNodeOrder.Add(Constants.sDiaTimeCreated);
            sThisNodeOrder.Add(Constants.sDiaDateAfter);        //SI06267
            //SI06472 sThisNodeOrder.Add(Constants.sDiaDateComp);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaTimeComp);
            sThisNodeOrder.Add(Constants.sDiaDateDue);          //SI06472
            sThisNodeOrder.Add(Constants.sDiaTimeDue);          //SI06472
            sThisNodeOrder.Add(Constants.sDiaAutoID);
            sThisNodeOrder.Add(Constants.sDiaDeleted);
            sThisNodeOrder.Add(Constants.sDiaVoid);
            sThisNodeOrder.Add(Constants.sDiaEstTime);
            sThisNodeOrder.Add(Constants.sDiaNotify);
            sThisNodeOrder.Add(Constants.sDiaRoute);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaEntry);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaEntryNotesRTF);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaEntryNotesHTML);   //SI06267
            sThisNodeOrder.Add(Constants.sDiaPriority);
            sThisNodeOrder.Add(Constants.sDiaStatus);
            sThisNodeOrder.Add(Constants.sDiaAutoConfirm);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaResp);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaRespRTF);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaRespHTML);         //SI06267
            sThisNodeOrder.Add(Constants.sDiaRespDate);         //SI06472
            sThisNodeOrder.Add(Constants.sDiaRespTime);         //SI06472
            sThisNodeOrder.Add(Constants.sDiaAssignedUser);
            sThisNodeOrder.Add(Constants.sDiaAssigningUser);
            sThisNodeOrder.Add(Constants.sDiaAssignedGroup);
            sThisNodeOrder.Add(Constants.sDiaIsAttached);
            sThisNodeOrder.Add(Constants.sDiaAttachTable);
            sThisNodeOrder.Add(Constants.sDiaAttachRecord);
            sThisNodeOrder.Add(Constants.sDiaRegarding);
            sThisNodeOrder.Add(Constants.sDiaClaimant);
            sThisNodeOrder.Add(Constants.sDiaClaimID);
            sThisNodeOrder.Add(Constants.sDiaEventID);
            sThisNodeOrder.Add(Constants.sDiaRoutable);
            sThisNodeOrder.Add(Constants.sDiaRollable);
            sThisNodeOrder.Add(Constants.sDiaRoutedTo);
            sThisNodeOrder.Add(Constants.sDiaRoutedFrom);
            sThisNodeOrder.Add(Constants.sDiaAutoLateNotify);
            sThisNodeOrder.Add(Constants.sDiaSeriesID);
            sThisNodeOrder.Add(Constants.sDiaReserveID);
            sThisNodeOrder.Add(Constants.sDiaPayPost);
            sThisNodeOrder.Add(Constants.sDiaTETracked);
            sThisNodeOrder.Add(Constants.sDiaEntryNotes);       //SI06472
            sThisNodeOrder.Add(Constants.sDiaEntryNotesRTF);    //SI06472
            sThisNodeOrder.Add(Constants.sDiaEntryNotesHTML);   //SI06472
            sThisNodeOrder.Add(Constants.sDiaResp);             //SI06472
            sThisNodeOrder.Add(Constants.sDiaRespRTF);          //SI06472
            sThisNodeOrder.Add(Constants.sDiaRespHTML);         //SI06472
            sThisNodeOrder.Add(Constants.sWPAStatus);           //SIW7038
            sThisNodeOrder.Add(Constants.sDiaDocumentNumber);   //SIW7038
            sThisNodeOrder.Add(Constants.sDiaActivities);
            sThisNodeOrder.Add(Constants.sDiaExpenses);
            sThisNodeOrder.Add(Constants.sDiaComment);
            sThisNodeOrder.Add(Constants.sDiaPartyInvolved);
            //SI06472 sThisNodeOrder.Add(Constants.sDiaTechKey);              //(SI06023 - Implemented in SI06333)

            sActNodeOrder = new ArrayList();
            sActNodeOrder.Add(Constants.sDiaActPEID);
            sActNodeOrder.Add(Constants.sDiaActCode);
            sActNodeOrder.Add(Constants.sDiaActEntry);

            sExpNodeOrder = new ArrayList();
            sExpNodeOrder.Add(Constants.sDiaExpEntryID);
            sExpNodeOrder.Add(Constants.sDiaExpLineNum);
            sExpNodeOrder.Add(Constants.sDiaExpStartDate);
            sExpNodeOrder.Add(Constants.sDiaExpStartTime);
            sExpNodeOrder.Add(Constants.sDiaExpEndDate);
            sExpNodeOrder.Add(Constants.sDiaExpEndTime);
            sExpNodeOrder.Add(Constants.sDiaExpDesc);
            sExpNodeOrder.Add(Constants.sDiaExpUnit);
            sExpNodeOrder.Add(Constants.sDiaExpReason);
            sExpNodeOrder.Add(Constants.sDiaExpStatus);
            sExpNodeOrder.Add(Constants.sDiaExpDisReason);
            sExpNodeOrder.Add(Constants.sDiaExpApprovedBy);
            sExpNodeOrder.Add(Constants.sDiaExpApprovedDate);
            sExpNodeOrder.Add(Constants.sDiaExpTransType);
            sExpNodeOrder.Add(Constants.sDiaExpUnits);
            sExpNodeOrder.Add(Constants.sDiaExpAmtPerUnit);
            sExpNodeOrder.Add(Constants.sDiaExpAmtNet);
            sExpNodeOrder.Add(Constants.sDiaExpManNet);
            sExpNodeOrder.Add(Constants.sDiaExpAmtApp);
            sExpNodeOrder.Add(Constants.sDiaExpAmtPaid);
            sExpNodeOrder.Add(Constants.sDiaExpDeleted);
            sExpNodeOrder.Add(Constants.sDiaExpBill);
            sExpNodeOrder.Add(Constants.sDiaExpCRC);
        }

        public override XmlNode Create()
        {
            //SI06472 return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","","");  //SI06472 //SIW7038
        }

        /*Start SI06472
        //Start SI06267
        public XmlNode Create(string sID, string sName, string sNotes, string sCreateDate, string sPriority,
                              string sStatus, string sAssignedUser, string sAssigningUser, string sNotesRTF,
                              string sAutoConf, string sCompleteTime, string sEstimateTime, string sVoid,
                              string sDeleted, string sRout, string sRoll, string sAutoLate, string sClaimant,
                              string sAttached, string sClaimID, string sAttachTable,
                              string sAttachRecord, string sRegarding, string sEventID)
        {
            return Create(sID, sName, sNotes, sCreateDate, sPriority, sStatus, sAssignedUser, sAssigningUser,
                          sNotesRTF, sAutoConf, sCompleteTime, sEstimateTime, sVoid, sDeleted, sRout, sRoll,
                          sAutoLate, sClaimant, sAttached, sClaimID, sAttachTable, sAttachRecord, sRegarding, sEventID, "");
        }
        //End SI06267
        //End SI06472 */

        protected override string DefaultNodeName
        {
            get { return Constants.sDiaNode; }
        }

        //SI06267 public XmlNode Create(string sID, string sName, string sNotes, string sCreateDate, string sPriority,
        //                      string sStatus, string sAssignedUser, string sAssigningUser, string sNotesRTF,
        //                      string sAutoConf, string sCompleteTime, string sEstimateTime, string sVoid,
        //                      string sDeleted, string sRout, string sRoll, string sAutoLate, string sClaimant,
        //                      string sAttached, string sClaimID, string sAttachTable, 
        //                      string sAttachRecord, string sRegarding, string sEventID)
        //SI06472 public XmlNode Create(string sID, string sName, string sNotes, string sCreateDate, string sPriority,
        //                      string sStatus, string sAssignedUser, string sAssigningUser, string sNotesRTF,
        //                      string sAutoConf, string sCompleteTime, string sEstimateTime, string sVoid,
        //                      string sDeleted, string sRout, string sRoll, string sAutoLate, string sClaimant,
        //                      string sAttached, string sClaimID, string sAttachTable, 
        //                      string sAttachRecord, string sRegarding, string sEventID, string sNotesHTML)  //SI06267
        public XmlNode Create(string sID, string sName, string sNotes, string sCreateDate, string sCreateTime, 
                          string sPriority, string sStatus, string sAssignedUser, string sAssigningUser,
                          string sAutoConf, string sVoid, string sDeleted, string sRout, string sRoll, 
                          string sAutoLate, string sClaimant, string sAttached, string sClaimID, string sAttachTable, 
                          string sAttachRecord, string sRegarding, string sEventID, string sWPAStatusType,string sWPAStatusCode) //SI06472 //SIW7038
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);    
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                DiaryID = sID;
                EntryName = sName;
                EntryNotes = sNotes;
                CreatedDate = sCreateDate;
                CreatedTime = sCreateTime;  //SI06472
                Priority = sPriority;
                StatusOpen = sStatus;
                AssignedUser = sAssignedUser;
                AssigningUser = sAssigningUser;
                putWPAStatus(sWPAStatusType, sWPAStatusCode); //SIW7038

                /*Start SI06472
                if (sNotesRTF != "" && sNotesRTF != null)
                    EntryNotesRTF = sNotesRTF;
                if (sNotesHTML != "" && sNotesHTML != null)
                    EntryNotesHTML = sNotesHTML;
                //End SI06472 */
                if (sAutoConf != "" && sAutoConf != null)
                    AutoConfirm = sAutoConf;
                /*Start SI06472
                if (sCompleteTime != "" && sCompleteTime != null)
                    CompletedTime = sCompleteTime;
                if (sEstimateTime != "" && sEstimateTime != null)
                    EstimatedTime = sEstimateTime;
                //End SI06472 */
                if (sVoid != "" && sVoid != null)
                    Voided = sVoid;
                if (sDeleted != "" && sDeleted != null)
                    Deleted = sDeleted;
                if (sRout != "" && sRout != null)
                    Routable = sRout;
                if (sRoll != "" && sRoll != null)
                    Rollable = sRoll;
                if (sAutoLate != "" && sAutoLate != null)
                    AutoLate = sAutoLate;
                if (sClaimant != "" && sClaimant != null)
                    Claimant = sClaimant;
                if (sAttached != "" && sAttached != null)
                    IsAttached = sAttached;
                if (sClaimID != "" && sClaimID != null)
                    ClaimID = sClaimID;
                if (sAttachTable != "" && sAttachTable != null)
                    AttachTable = sAttachTable;
                if (sAttachRecord != "" && sAttachRecord != null)
                    AttachRecordID = sAttachRecord;
                if (sRegarding != "" && sRegarding != null)
                    Regarding = sRegarding;
                if (sEventID != "" && sEventID != null)
                    EventID = sEventID;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDiary.Create");
                return null;
            }
        }

        public string CreatedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDiaDateCreated);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDiaDateCreated, value, NodeOrder);
            }
        }

        public string CreatedTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDiaTimeCreated);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDiaTimeCreated, value, NodeOrder);
            }
        }

        //Start SI06267
        public string AfterDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDiaDateAfter);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDiaDateAfter, value, NodeOrder);
            }
        }
        //End SI06267

        /*Start SI06472
        public string CompletedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDiaDateComp);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDiaDateComp, value, NodeOrder);
            }
        }

        public string CompletedTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDiaTimeComp);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDiaTimeComp, value, NodeOrder);
            }
        }*/

        public string DueDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDiaDateDue);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDiaDateDue, value, NodeOrder);
            }
        }

        public string DueTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDiaTimeDue);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDiaTimeDue, value, NodeOrder);
            }
        }
        //End SI06472

        public string AutoID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAutoID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAutoID, value, NodeOrder);
            }
        }

        public string Deleted
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaDeleted, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaDeleted, "", value, NodeOrder);
            }
        }

        public string Voided
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaVoid, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaVoid, "", value, NodeOrder);
            }
        }

        public string EstimatedTime
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaEstTime);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaEstTime, value, NodeOrder);
            }
        }

        public string NotifyFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaNotify, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaNotify, "", value, NodeOrder);
            }
        }

        public string RouteFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaRoute, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaRoute, "", value, NodeOrder);
            }
        }

        /*Start SI06472
        public string EntryID
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sDiaEntry, Constants.sDiaEntryID);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sDiaEntry, Constants.sDiaEntryID, value, NodeOrder);
            }
        }*/

        public XmlNode putEntryName(string vname, string vnameCode)
        {
            return putEntryName(vname, vnameCode, "");
        }

        public XmlNode putEntryName(string vname, string vnameCode, string vnameCodeID)
        {
            return Utils.putCodeItem(putNode, Constants.sDiaEntry, vname, vnameCode, NodeOrder, vnameCodeID);
        }

        public string EntryNameCodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDiaEntry);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDiaEntry, value, NodeOrder);
            }
        }

        public string EntryName
        {
            get
            {
                //return Utils.getAttribute(Node, Constants.sDiaEntry, Constants.sDiaEntryName);
                return Utils.getData(Node, Constants.sDiaEntry);
            }
            set
            {
                //Utils.putAttribute(putNode, Constants.sDiaEntry, Constants.sDiaEntryName, value, NodeOrder);
                Utils.putData(putNode, Constants.sDiaEntry, value, NodeOrder);
            }
        }

        public string EntryNotes
        {
            get
            {
                //return Utils.getData(Node, Constants.sDiaEntry);
                return Utils.getData(Node, Constants.sDiaEntryNotes);
            }
            set
            {
                //Utils.putData(putNode, Constants.sDiaEntry, value, NodeOrder);
                Utils.putData(putNode, Constants.sDiaEntryNotes, value, NodeOrder);
            }
        }
        //End SI06472

        public string EntryNotesRTF
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaEntryNotesRTF);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaEntryNotesRTF, value, NodeOrder);
            }
        }

        //Start SI06267
        public string EntryNotesHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaEntryNotesHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaEntryNotesHTML, value, NodeOrder);
            }
        }
        //End SI06267

        public string Priority
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaPriority);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaPriority, value, NodeOrder);
            }
        }

        public string StatusOpen
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaStatus, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaStatus, "", value, NodeOrder);
            }
        }
        //start SIW7038
        public string DocumentNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaDocumentNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaDocumentNumber, value, NodeOrder);
            }
        }

        public XmlNode putWPAStatus(string sDesc, string sCode)
        {
            return putWPAStatus(sDesc, sCode, "");
        }

        public XmlNode putWPAStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sWPAStatus, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string WPAStatus
        {
            get
            {
                return Utils.getData(Node, Constants.sWPAStatus);   
            }
            set
            {
                Utils.putData(putNode, Constants.sWPAStatus, value, NodeOrder);    
            }
        }

        public string WPAStatus_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sWPAStatus);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sWPAStatus, value, NodeOrder);    
            }
        }

        public string WPAStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sWPAStatus);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sWPAStatus, value, NodeOrder);    
            }
        }
        //End SIW7038

        public string AutoConfirm
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaAutoConfirm, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaAutoConfirm, "", value, NodeOrder);
            }
        }

        //Start SI06472
        public string ResponseDate
        {
            get
            {
                //return Utils.getDate(Node, Constants.sDiaResp);
                return Utils.getDate(Node, Constants.sDiaRespDate);
            }
            set
            {
                //Utils.putDate(putNode, Constants.sDiaResp, value, NodeOrder);
                Utils.putDate(putNode, Constants.sDiaRespDate, value, NodeOrder);
            }
        }

        public string ResponseTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDiaRespTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDiaRespTime, value, NodeOrder);
            }
        }
        //End SI06472

        public string Response
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaResp);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaResp, value, NodeOrder);
            }
        }

        public string ResponseRTF
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaRespRTF);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaRespRTF, value, NodeOrder);
            }
        }

        //Start SI06267
        public string ResponseHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaRespHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaRespHTML, value, NodeOrder);
            }
        }
        //End SI06267

        public string AssignedUser
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAssignedUser);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAssignedUser, value, NodeOrder);
            }
        }

        //Start SIW111
        public string AssignedUserID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sDiaAssignedUser);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sDiaAssignedUser, value, NodeOrder);
            }
        }
        //End SIW111

        public string AssigningUser
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAssigningUser);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAssigningUser, value, NodeOrder);
            }
        }

        //Start SIW111
        public string AssigningUserID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sDiaAssigningUser);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sDiaAssigningUser, value, NodeOrder);
            }
        }
        //End SIW111

        public string AssignedGroup
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAssignedGroup);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAssignedGroup, value, NodeOrder);
            }
        }

        public string IsAttached
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaIsAttached, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaIsAttached, "", value, NodeOrder);
            }
        }

        public string AttachTable
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAttachTable);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAttachTable, value, NodeOrder);
            }
        }

        public string AttachRecordID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaAttachRecord);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaAttachRecord, value, NodeOrder);
            }
        }

        public string Regarding
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaRegarding);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaRegarding, value, NodeOrder);
            }
        }

        public string Claimant
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaClaimant);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaClaimant, value, NodeOrder);
            }
        }

        public string ClaimID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaClaimID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaClaimID, value, NodeOrder);
            }
        }

        public string EventID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaEventID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaEventID, value, NodeOrder);
            }
        }

        public string Routable
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaRoutable, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaRoutable, "", value, NodeOrder);
            }
        }

        public string Rollable
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaRollable, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaRollable, "", value, NodeOrder);
            }
        }

        public string RoutedTo
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaRoutedTo);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaRoutedTo, value, NodeOrder);
            }
        }

        public string RoutedFrom
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaRoutedFrom);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaRoutedFrom, value, NodeOrder);
            }
        }

        public string AutoLate
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaAutoLateNotify, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaAutoLateNotify, "", value, NodeOrder);
            }
        }

        public string SeriesID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaSeriesID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaSeriesID, value, NodeOrder);
            }
        }

        public string ReserveID
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaReserveID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaReserveID, value, NodeOrder);
            }
        }

        //Start (SI06023 - Implemented in SI06333)
        public string Techkey 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }
        
        //START SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        //public string PayPostTranslate(string intype)
        //{
        //    object ret;
        //    if (dctExpPP.TryGetValue(intype, out ret))
        //        return ret + "";
        //    else
        //        return "";
        //}
        
        public string PayPostTranslate(diaExpPP intype)
        {
            object ret;
            if (!dctExpPP.TryGetValue((int)intype, out ret))
                ret = "";
            return (string)ret;
        }

        public diaExpPP PayPostTranslate(string intype)
        {
            object ret;
            if (!dctExpPP.TryGetValue(intype, out ret))
                ret = diaExpPP.expUnspecified;
            return (diaExpPP)ret;
        }//End (SI06023 - Implemented in SI06333)
  
        public string PayPostFlag
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaPayPost);
            }
            set
            {
                object odata;
                int iType;
                string sdata = value;
                bool i = false;

                if (Int32.TryParse(value, out iType))
                {
                    i = true;
                    odata = iType;
                }
                else
                    odata = value;
                if (dctExpPP.ContainsKey(odata))
                {
                    if (i)
                    {
                        dctExpPP.TryGetValue(iType, out odata);
                    }
                    sdata = (string)odata;
                    Utils.putData(putNode, Constants.sDiaPayPost, sdata, NodeOrder);
                }else
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, "XMLDiary.PayPostFlag",
                                     value + "", Document, Node);
            }
        }

        public string TETracked
        {
            get
            {
                return Utils.getBool(Node, Constants.sDiaTETracked, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sDiaTETracked, "", value, NodeOrder);
            }
        }

        public XmlNodeList ActivityList
        {
            get
            {
                //SI06267 return getNodeList(Constants.sDiaActivity, ref m_xmlActList, m_xmlActs);
                // Start SIW163
                if (null == m_xmlActList)
                {
                    return getNodeList(Constants.sDiaActivity, ref m_xmlActList, ActivitiesNode);   //SI06267
                }
                else
                {
                    return m_xmlActList;
                }
                //End SIW163
            }
        }

        public XmlNode ActivitiesNode
        {
            get
            {
                //start SI06267
                if (m_xmlActs == null)
                    m_xmlActs = XML.XMLGetNode(Node, Constants.sDiaActivities);
                //end SI06267
                return m_xmlActs;
            }
            set
            {
                m_xmlActs = value;
            }
        }

        public XmlNode putActivitiesNode
        {
            get
            {
                if (ActivitiesNode == null)
                {
                    ActivitiesNode = XML.XMLaddNode(putNode, Constants.sDiaActivities, NodeOrder);
                    NextActivityID = "1";
                    ActivityCount = 0;
                }
                return ActivitiesNode;
            }
        }

        public int ActivityCount
        {
            get
            {
                string strdata = Utils.getAttribute(ActivitiesNode, "", Constants.sDiaActCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putActivitiesNode, "", Constants.sDiaActCount, value + "", sActNodeOrder);
            }
        }

        public string NextActivityID
        {
            get
            {
                return Utils.getAttribute(ActivitiesNode, "", Constants.sDiaActNextID);
            }
            set
            {
                if(value == "")
                    value = "1";
                Utils.putAttribute(putActivitiesNode, "", Constants.sDiaActNextID, value, sActNodeOrder);
            }
        }

        public string getNextActivityID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextActivityID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextActivityID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public XmlNode getActivity(bool reset)
        {
            // Start SIW7351
            //object o = ActivityList;
            if (null != ActivityList)
                return getNode(reset, ref m_xmlActList, ref m_xmlAct);
            else
                return null;
            // End SIW7351
        }

        public XmlNode getActivitybyID(string sID)
        {
            if (ActivityID != sID)
            {
                getActivity(true);
                while (ActivityNode != null)
                {
                    if (ActivityID == sID)
                        break;
                    getActivity(false);
                }
            }
            return ActivityNode;
        }

        public XmlNode putActivityNode
        {
            get
            {
                if (ActivityNode == null)
                    //SIW485 ActivityNode = XML.XMLaddNode(putActivitiesNode, Constants.sDiaActivity, Globals.sNodeOrder);
                    ActivityNode = XML.XMLaddNode(putActivitiesNode, Constants.sDiaActivity, Utils.sNodeOrder);	//SIW485
                return ActivityNode;
            }
        }

        public XmlNode ActivityNode
        {
            get
            {
                return m_xmlAct;
            }
            set
            {
                m_xmlAct = value;
            }
        }

        public XmlNode addActivity()
        {
            ActivityNode = null;
            ActivityCount = ActivityCount + 1;
            return putActivityNode;
        }

        public XmlNode putActivity(string sActID, string sParEntryID, string sActivity,
                                   string sActivityCode, string sEntryID)
        {
            if (sActID != "" && sActID != null)
                ActivityID = sActID;
            if (sParEntryID != "" && sParEntryID != null)
                ParentEntryID = sParEntryID;
            if ((sActivity != "" && sActivity != null) || (sActivityCode != "" && sActivityCode != null))
                putActivityCode(sActivity, sActivityCode);
            if (sEntryID != "" && sEntryID != null)
                ActivityEntryID = sEntryID;
            return ActivityNode;
        }

        public string ActIDPrefix
        {
            get
            {
                return Constants.sDiaActIDPfx;
            }
        }

        public string extActivityID(string strdata)
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sDiaActIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string ActivityID
        {
            get
            {
                string strdata;
                string lid = "";
                if (ActivityNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(ActivityNode, Constants.sDiaActID);
                    lid = extActivityID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = getNextActivityID(null);
                XML.XMLSetAttributeValue(putActivityNode, Constants.sDiaActID, Constants.sDiaActIDPfx + value);
            }
        }

        public string ParentEntryID
        {
            get
            {
                return Utils.getData(ActivityNode, Constants.sDiaActPEID);
            }
            set
            {
                Utils.putData(putActivityNode, Constants.sDiaActPEID, value, sActNodeOrder);
            }
        }

        //Start SI06472
        public XmlNode putActivityCode(string sDesc, string sCode)
        {
            return putActivityCode(sDesc, sCode, "");
        }

        //public XmlNode putActivityCode(string sDesc, string sCode)
        public XmlNode putActivityCode(string sDesc, string sCode, string sCodeID)
        {
            return Utils.putCodeItem(putActivityNode, Constants.sDiaActCode, sDesc, sCode, sActNodeOrder, sCodeID);
        }
        //End SI06472

        public string Activity
        {
            get
            {
                return Utils.getData(ActivityNode, Constants.sDiaActCode);
            }
            set
            {
                Utils.putData(putActivityNode, Constants.sDiaActCode, value, sActNodeOrder);
            }
        }

        public string Activity_Code
        {
            get
            {
                return Utils.getCode(ActivityNode, Constants.sDiaActCode);
            }
            set
            {
                Utils.putCode(putActivityNode, Constants.sDiaActCode, value, sActNodeOrder);
            }
        }

        //Start SI06472
        public string Activity_CodeID
        {
            get
            {
                return Utils.getCodeID(ActivityNode, Constants.sDiaActCode);
            }
            set
            {
                Utils.putCodeID(putActivityNode, Constants.sDiaActCode, value, sActNodeOrder);
            }
        }
        //End SI06472

        public string ActivityEntryID
        {
            get
            {
                return Utils.getData(ActivityNode, Constants.sDiaActEntry);
            }
            set
            {
                Utils.putData(putActivityNode, Constants.sDiaActEntry, value, sActNodeOrder);
            }
        }

        public XmlNodeList ExpenseList
        {
            get
            {
                //SI06267 return getNodeList(Constants.sDiaExpense, ref m_xmlExpList, m_xmlExps);
                // Start SIW163
                if (null == m_xmlExpList)
                {
                    return getNodeList(Constants.sDiaExpense, ref m_xmlExpList, ExpensesNode);     //SI06267
                }
                else
                {
                    return m_xmlExpList;
                }
                //End SIW163
            }
        }

        public XmlNode ExpensesNode
        {
            get
            {
                //Start SI06267
                if (m_xmlExps == null)
                    m_xmlExps = XML.XMLGetNode(Node, Constants.sDiaExpenses);
                //End SI06267
                return m_xmlExps;
            }
            set
            {
                m_xmlExps = value;
            }
        }

        public XmlNode putExpensesNode
        {
            get
            {
                if (ExpensesNode == null)
                {
                    ExpensesNode = XML.XMLaddNode(putNode, Constants.sDiaExpenses, NodeOrder);
                    NextExpenseID = "1";
                    ExpenseCount = 0;
                }
                return ExpensesNode;
            }
        }

        public int ExpenseCount
        {
            get
            {
                string strdata = Utils.getAttribute(ExpensesNode, "", Constants.sDiaExpCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putExpensesNode, "", Constants.sDiaExpCount, value + "", sExpNodeOrder);
            }
        }

        public string NextExpenseID
        {
            get
            {
                return Utils.getAttribute(ExpensesNode, "", Constants.sDiaExpNextID);
            }
            set
            {
                if(value == "")
                    value = "1";
                Utils.putAttribute(putExpensesNode, "", Constants.sDiaExpNextID, value, sExpNodeOrder);
            }
        }

        public String getNextExpenseID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextExpenseID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextExpenseID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public XmlNode getExpense(bool reset)
        {
            object o = ExpenseList;
            return getNode(reset, ref m_xmlExpList, ref m_xmlExp);
        }

        public XmlNode getExpensebyID(string sID)
        {
            if (ExpenseID != sID)
            {
                getExpense(true);
                while (ExpenseNode != null)
                {
                    if (ExpenseID == sID)
                        break;
                    getExpense(false);
                }
            }
            return ExpenseNode;
        }

        public XmlNode putExpenseNode
        {
            get
            {
                if (ExpenseNode == null)
                    //SIW485 ExpenseNode = XML.XMLaddNode(putExpensesNode, Constants.sDiaExpense, Globals.sNodeOrder);
                    ExpenseNode = XML.XMLaddNode(putExpensesNode, Constants.sDiaExpense, Utils.sNodeOrder);	//SIW485
                return ExpenseNode;
            }
        }

        public XmlNode ExpenseNode
        {
            get
            {
                return m_xmlExp;
            }
            set
            {
                m_xmlExp = value;
            }
        }

        public XmlNode addExpense()
        {
            ExpenseNode = null;
            ExpenseCount = ExpenseCount + 1;
            return putExpenseNode;
        }

        public XmlNode putExpense(string sExpID)
        {
            if (sExpID != "" && sExpID != null)
                ExpenseID = sExpID;
            return ExpenseNode;
        }

        public string ExpenseID
        {
            get
            {
                return Utils.getAttribute(ExpenseNode, Constants.sDiaExpense, Constants.sDiaExpID);
            }
            set
            {
                Utils.putAttribute(putExpenseNode, Constants.sDiaExpense, Constants.sDiaExpID, value, sExpNodeOrder);
            }
        }

        public string ExpenseEntryID
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpEntryID);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpEntryID, value, sExpNodeOrder);
            }
        }

        public string ExpenseLineNumber
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpLineNum);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpLineNum, value, sExpNodeOrder);
            }
        }

        public string ExpenseStartDate
        {
            get
            {
                return Utils.getDate(ExpenseNode, Constants.sDiaExpStartDate);
            }
            set
            {
                Utils.putDate(putExpenseNode, Constants.sDiaExpStartDate, value, sExpNodeOrder);
            }
        }

        public string ExpenseStartTime
        {
            get
            {
                return Utils.getTime(ExpenseNode, Constants.sDiaExpStartTime);
            }
            set
            {
                Utils.putTime(putExpenseNode, Constants.sDiaExpStartTime, value, sExpNodeOrder);
            }
        }

        public string ExpenseEndDate
        {
            get
            {
                return Utils.getDate(ExpenseNode, Constants.sDiaExpEndDate);
            }
            set
            {
                Utils.putDate(putExpenseNode, Constants.sDiaExpEndDate, value, sExpNodeOrder);
            }
        }

        public string ExpenseEndTime
        {
            get
            {
                return Utils.getTime(ExpenseNode, Constants.sDiaExpEndTime);
            }
            set
            {
                Utils.putTime(putExpenseNode, Constants.sDiaExpEndTime, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public XmlNode putExpenseDescription(string sDesc, string sCode)
        {
            return putExpenseDescription(sDesc, sCode, "");
        }

        public XmlNode putExpenseDescription(string sDesc, string sCode, string sCodeID)
        {
            //return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpDesc, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpDesc, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseDescription
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpDesc);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpDesc, value, sExpNodeOrder);
            }
        }

        public string ExpenseDescription_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpDesc);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpDesc, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseDescription_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpDesc);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpDesc, value, sExpNodeOrder);
            }
        }

        public XmlNode putExpenseUnit(string sDesc, string sCode)
        {
            return putExpenseUnit(sDesc, sCode, "");
        }

        public XmlNode putExpenseUnit(string sDesc, string sCode, string sCodeID)
        {
            //return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpUnit, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpUnit, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseUnit
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpUnit);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpUnit, value, sExpNodeOrder);
            }
        }

        public string ExpenseUnit_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpUnit);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpUnit, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseUnit_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpUnit);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpUnit, value, sExpNodeOrder);
            }
        }

        public XmlNode putExpenseReason(string sDesc, string sCode)
        {
            return putExpenseReason(sDesc, sCode, "");
        }

        public XmlNode putExpenseReason(string sDesc, string sCode, string sCodeID)
        {
            //return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpReason, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpReason, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseReason
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpReason);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpReason, value, sExpNodeOrder);
            }
        }

        public string ExpenseReason_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpReason);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpReason, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseReason_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpReason);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpReason, value, sExpNodeOrder);
            }
        }

        public XmlNode putExpenseStatus(string sDesc, string sCode)
        {
            return putExpenseStatus(sDesc, sCode, "");
        }

        public XmlNode putExpenseStatus(string sDesc, string sCode, string sCodeID)
        {
            //return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpStatus, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpStatus, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseStatus
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpStatus);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpStatus, value, sExpNodeOrder);
            }
        }

        public string ExpenseStatus_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpStatus);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpStatus, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpStatus);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpStatus, value, sExpNodeOrder);
            }
        }

        public XmlNode putExpenseDisapproval(string sDesc, string sCode)
        {
            return putExpenseDisapproval(sDesc, sCode, "");
        }

        public XmlNode putExpenseDisapproval(string sDesc, string sCode, string sCodeID)
        {
            //return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpDisReason, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpDisReason, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseDisapproval
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpDisReason);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpDisReason, value, sExpNodeOrder);
            }
        }

        public string ExpenseDisapproval_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpDisReason);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpDisReason, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseDisapproval_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpDisReason);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpDisReason, value, sExpNodeOrder);
            }
        }

        public XmlNode putExpenseTransactionType(string sDesc, string sCode)
        {
            return putExpenseTransactionType(sDesc, sCode, "");
        }

        public XmlNode putExpenseTransactionType(string sDesc, string sCode, string sCodeID)
        {
            //SI06472 return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpTransType, sDesc, sCode, sExpNodeOrder);
            return Utils.putCodeItem(putExpenseNode, Constants.sDiaExpTransType, sDesc, sCode, sExpNodeOrder, sCodeID);
        }
        //End SI06472

        public string ExpenseTransactionType
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpTransType);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpTransType, value, sExpNodeOrder);
            }
        }

        public string ExpenseTransactionType_Code
        {
            get
            {
                return Utils.getCode(ExpenseNode, Constants.sDiaExpTransType);
            }
            set
            {
                Utils.putCode(putExpenseNode, Constants.sDiaExpTransType, value, sExpNodeOrder);
            }
        }

        //Start SI06472
        public string ExpenseTransactionType_CodeID
        {
            get
            {
                return Utils.getCodeID(ExpenseNode, Constants.sDiaExpTransType);
            }
            set
            {
                Utils.putCodeID(putExpenseNode, Constants.sDiaExpTransType, value, sExpNodeOrder);
            }
        }
        //End SI06472

        public string ExpenseApprovedBy
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpApprovedBy);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpApprovedBy, value, sExpNodeOrder);
            }
        }

        public string ExpenseApprovedDate
        {
            get
            {
                return Utils.getDate(ExpenseNode, Constants.sDiaExpApprovedDate);
            }
            set
            {
                Utils.putDate(putExpenseNode, Constants.sDiaExpApprovedDate, value, sExpNodeOrder);
            }
        }

        public string ExpenseUnits
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpUnits);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpUnits, value, sExpNodeOrder);
            }
        }

        public string ExpenseAmountPerUnit
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpAmtPerUnit);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpAmtPerUnit, value, sExpNodeOrder);
            }
        }

        public string ExpenseAmountNet
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpAmtNet);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpAmtNet, value, sExpNodeOrder);
            }
        }

        public string ExpenseManNetInd
        {
            get
            {
                return Utils.getBool(ExpenseNode, Constants.sDiaExpManNet, "");
            }
            set
            {
                Utils.putBool(putExpenseNode, Constants.sDiaExpManNet, "", value, sExpNodeOrder);
            }
        }

        public string ExpenseAmountApproved
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpAmtApp);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpAmtApp, value, sExpNodeOrder);
            }
        }

        public string ExpenseAmountPaid
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpAmtPaid);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpAmtPaid, value, sExpNodeOrder);
            }
        }

        public string ExpenseDeleted
        {
            get
            {
                return Utils.getBool(ExpenseNode, Constants.sDiaExpDeleted, "");
            }
            set
            {
                Utils.putBool(putExpenseNode, Constants.sDiaExpDeleted, "", value, sExpNodeOrder);
            }
        }

        public string ExpenseBillable
        {
            get
            {
                return Utils.getBool(ExpenseNode, Constants.sDiaExpBill, "");
            }
            set
            {
                Utils.putBool(putExpenseNode, Constants.sDiaExpBill, "", value, sExpNodeOrder);
            }
        }

        public string ExpenseCRC
        {
            get
            {
                return Utils.getData(ExpenseNode, Constants.sDiaExpCRC);
            }
            set
            {
                Utils.putData(putExpenseNode, Constants.sDiaExpCRC, value, sExpNodeOrder);
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sDiaIDPfx;
            }
        }

        public string DiaryID
        {
            get
            {
                string strdata;
                string lid = "";
                if(Node != null){
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sDiaID);
                    lid = ((Utils)Utils).extDiaryID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = Parent.getNextID(null);
                XML.XMLSetAttributeValue(putNode, Constants.sDiaID, Constants.sDiaIDPfx + value);
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            Parent.Count = Parent.Count + 1;
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLDiaries Parent
        {
            get
            {
                if (m_Diaries == null)
                {
                    m_Diaries = new XMLDiaries();
                    m_Diaries.Diary = this;
                    //SIW529 LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Diaries);   //SIW529
                    ResetParent();  //SI06420
                }
                return m_Diaries;
            }
            set
            {
                m_Diaries = value;
                ResetParent();
                //if (m_Parent != null)                                         //SIW529
                if (m_Diaries != null)                                          //SIW529
                    //SIW529 ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
                    ((XMLXCNodeBase)m_Diaries).LinkXMLObjects(this);            //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                //Start SI06472
                XmlNode node = xmlThisNode;
                //SIW122 return Parent.putNode;
                XmlNode pNode = Parent.putNode;     //SIW122
                xmlThisNode = node;
                return pNode;       //SIW122
                //End SI06472
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            m_xmlAct = null;
            m_xmlActs = null;
            m_xmlActList = null;
            m_xmlExps = null;
            m_xmlExp = null;
            m_xmlExpList = null;
            PartyInvolved = null;
            CommentReference = null;
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XmlNode getDiary(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getDiarybyID(string sID)
        {
            if (DiaryID != sID)
            {
                getDiary(true);
                while (Node != null)
                {
                    if (DiaryID == sID)
                        break;
                    getDiary(false);
                }
            }
            return Node;
        }
    }

    public class XMLDiaries : XMLXCNode
    {
        /*******************************************************************************
        ' The cXMLDiaries class provides the functionality to support and manage
        ' an AC Diaries Collection Node
        '*******************************************************************************
        '<DIARIES NEXT_ID="#" COUNT="#">
        'Start SIW404
        '   <GROUP_START>#</GROUP_START>
        '   <GROUP_END>#</GROUP_END>
        '   <NAVIGATION>NEXT|PREVIOUS</NAVIGATION>
        'End SIW404
        '   <DIARY>
        '      ...
        '   </DIARY>
        '</DIARIES>
        '*******************************************************************************/

        private XMLDiary m_Diary;
        
        public XMLDiaries()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;

            sThisNodeOrder = new ArrayList();
            //Start SIW404
            sThisNodeOrder.Add(Constants.sDiaGroupStart);
            sThisNodeOrder.Add(Constants.sDiaGroupEnd);
            sThisNodeOrder.Add(Constants.sDiaNavigation);
            //End SIW404
            sThisNodeOrder.Add(Constants.sDiaNode);

            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sDiaCollection; }
        }

        private void subClearPointers()
        {
            //Start SI06420
            //Diary = null;
            if (m_Diary != null)
                //SIW529 m_Diary.Node = null;
                m_Diary.Parent = this;  //SIW529 Resets the Diary object
            //End SI06420
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();
            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                //SIW529 xmlElement = xmlDocument.CreateElement(NodeName);
                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);    //SIW529
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextID = sNextID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDiaries.Create");
                return null;
            }
        }

        public string getNextID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sDiaNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putNode, "", Constants.sDiaNextID, value, NodeOrder);
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sDiaCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sDiaCount, value + "", NodeOrder);
            }
        }

        //Start SIW404
        public string GroupStart
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaGroupStart);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaGroupStart, value, NodeOrder);
            }
        }

        public string GroupEnd
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaGroupEnd);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaGroupEnd, value, NodeOrder);
            }
        }

        public string Navigation
        {
            get
            {
                return Utils.getData(Node, Constants.sDiaNavigation);
            }
            set
            {
                Utils.putData(putNode, Constants.sDiaNavigation, value, NodeOrder);
            }
        }
        //End SIW404

        public override XmlNode Node
        {
            get //Start SI06420
            {
                if (xmlThisNode == null)
                    ResetParent();
                return xmlThisNode;
            }//End SI06420
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
                ResetParent();
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.Node;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XMLDiary Diary
        {
            get
            {
                if (m_Diary == null)
                {
                    m_Diary = new XMLDiary();
                    m_Diary.Parent = this;
                    //SIW529 LinkXMLObjects(m_Diary);
                    //m_Diary.getDiary(Constants.xcGetFirst); //SI06420
                }
                if (Node != null) //Start SI06420
                { 
                  if(m_Diary.Node == null)
                      m_Diary.getDiary(Constants.xcGetFirst); 
                
                }//End SI06420
                return m_Diary;
            }
            set
            {
                m_Diary = value;
            }
        }

        public void AddDiary()
        {
            Diary.Create();
        }

        public XmlNode getDiary(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Diary.getDiary(reset);
        }

        public XmlNode getDiarybyID(XmlDocument xdoc, string sID)
        {
            if (xdoc != null)
                Document = xdoc;
            return Diary.getDiarybyID(sID);
        }
    }
}
