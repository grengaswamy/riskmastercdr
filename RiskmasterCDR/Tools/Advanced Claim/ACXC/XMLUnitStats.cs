/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 04/21/2010 | SIW423  |    SW      |  ClaimDenied corrected     
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment                                 
 * 08/13/2010 | SIW490 |    ASM     | Tree Label          
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Common;
using CCP.Constants;

namespace CCP.XmlComponents
{
    public class XMLUnitStats : XMLXCNode, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '   ....
        '   <UNIT_STAT_DCI>
        '      <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '      <ANNUITY_PURCHASE_AMOUNT>0</ANNUITY_PURCHASE_AMOUNT>
        '      <DEDUCTIBLE REIMBURSEMENT="No" CODE="pgmcode">Deductible Program<DEDUCTIBLE/>
        '      <DISPUTED_CASE INDICATOR="No" />
        '      <POSSIBLE_FRAUD CODE="02">Fraudulent Claim Indicator</POSSIBLE_FRAUD>
        '      <LUMP_SUM INDICATOR="No" />
        '      <JURISDICTION_STATE CODE="NC">North Carolina</JURISDICTION_STATE>
        '      <WORKERS_COMP_CAT>0</WORKERS_COMP_CAT>
        '      <BENEFIT_TYPE CODE="05">Temporary Total or Temporary Partial Disability</BENEFIT_TYPE>
        '      <DATE_EMPLOYER_NOTIFIED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <TIME_EMPLOYER_NOTIFIED HOUR="17" MINUTE="35" SECOND="04" />
        '      <NCCI_WAGE_RANGE CODE="xxx">wage range</NCCI_WAGE_RANGE>
        '      <CATASTROPHE_NUMBER>catastrophe</CATASTROPHE_NUMBER>
        '      <LOSS_ACT CODE='lossactcode'>Loss Act</LOSS_ACT>
        '      <SETTLEMENT_METHOD CODE="code">settlement method</SETTLEMENT_METHOD>
        '      <SETTLEMENT_TYPE CODE="code">settlement Type</SETTLEMENT_TYPE>
        '      <TYPE_RECOVERY CODE="code">type recovery</TYPE_RECOVERY>
        '      <TYPE_RESERVE CODE="code">reserve typee</TYPE_RESERVE>
        '      <TYPE_COVERAGE CODE="code">coverage type</TYPE_COVERAGE>
        '      <TYPE_LOSS CODE="code">loss code</TYPE_LOSS>
        '      <LOSS_COVERAGE CODE="code">loss coverage</LOSS_COVERAGE>
        '      <CLAIM_DENIED INDICATOR="No" />                              'SI05300
        '      <EMPLOYEE_INFORMATION>
        '          ....
        '      </EMPLOYEE_INFORMATION>
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>         'SI04637
        '         <!--Multiple Parties Involved at the UnitStats level such as Employee, Employer, etc.-->
        '   </UNIT_STAT_DCI>
        '</CLAIM>*/

        private XMLClaim m_Claim;
        private XMLEmployee m_Employee;
        private XMLPartyInvolved m_PartyInvolved;
        
        public XMLUnitStats()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            //sThisNodeOrder = new ArrayList(24);  //SIW490
            sThisNodeOrder = new ArrayList(25);   //SIW490
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sUSAnnPurAmt);
            sThisNodeOrder.Add(Constants.sUSTotGrossIncurred);
            sThisNodeOrder.Add(Constants.sUSDeductible);
            sThisNodeOrder.Add(Constants.sUSDisputedCase);
            sThisNodeOrder.Add(Constants.sUSPossibleFraud);
            sThisNodeOrder.Add(Constants.sUSLumpSum);
            sThisNodeOrder.Add(Constants.sUSJurisdictionState);
            sThisNodeOrder.Add(Constants.sUSWrkCompCategory);
            sThisNodeOrder.Add(Constants.sUSBenefitType);
            sThisNodeOrder.Add(Constants.sUSEmployerNotifiedDate);
            sThisNodeOrder.Add(Constants.sUSEmployerNotifiedTime);
            sThisNodeOrder.Add(Constants.sUSAttyFormRcptDate);
            sThisNodeOrder.Add(Constants.sUSEmployerWageRange);
            sThisNodeOrder.Add(Constants.sUSCatastrophe);
            sThisNodeOrder.Add(Constants.sUSLossAct);
            sThisNodeOrder.Add(Constants.sUSSettleMethod);
            sThisNodeOrder.Add(Constants.sUSSettleType);
            sThisNodeOrder.Add(Constants.sUSTypeRecovery);
            sThisNodeOrder.Add(Constants.sUSTypeReserve);
            sThisNodeOrder.Add(Constants.sUSTypeCoverage);
            sThisNodeOrder.Add(Constants.sUSTypeLoss);
            sThisNodeOrder.Add(Constants.sUSLossCoverage);
            sThisNodeOrder.Add(Constants.sUSClaimDenied);
            sThisNodeOrder.Add(Constants.sUSEmployee);
            sThisNodeOrder.Add(Constants.sUSPartyInvolved);

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sUSNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sAnnuityPurchaseAmt, string sDeductibleReimbursement, string sDisputedCase,
                              string sLumpSum, string sJurisdictionState, string sJurisdictionStatecode,
                              string sWorkersCompCategory, string sBenefitType, string sBenefitTypecode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                AnnuityPurchaseAmt = sAnnuityPurchaseAmt;
                DeductibleReimbursement = sDeductibleReimbursement;
                DisputedCase = sDisputedCase;
                LumpSum = sLumpSum;
                putJurisdictionState(sJurisdictionState, sJurisdictionStatecode);
                WorkersCompCategory = sWorkersCompCategory;
                putBenefitType(sBenefitType, sBenefitTypecode);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLUnitStats.Create");
                return null;
            }
        }

        public string AnnuityPurchaseAmt
        {
            get
            {
                return Utils.getData(Node, Constants.sUSAnnPurAmt);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSAnnPurAmt, value, NodeOrder);
            }
        }

        //START SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        public string CatastropheNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sUSCatastrophe);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSCatastrophe, value, NodeOrder);
            }
        }

        public string TotalGrossIncurred
        {
            get
            {
                return Utils.getData(Node, Constants.sUSTotGrossIncurred);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSTotGrossIncurred, value, NodeOrder);
            }
        }

        public string WorkersCompCategory
        {
            get
            {
                return Utils.getData(Node, Constants.sUSWrkCompCategory);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSWrkCompCategory, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putDeductible(string sDesc, string sCode)
        {
            return putDeductible(sDesc, sCode, "");
        }

        public XmlNode putDeductible(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSDeductible, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Deductible
        {
            get
            {
                return Utils.getData(Node, Constants.sUSDeductible);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSDeductible, value, NodeOrder);
            }
        }

        public string Deductible_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSDeductible);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSDeductible, value, NodeOrder);
            }
        }

        public string Deductible_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSDeductible);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSDeductible, value, NodeOrder);
            }
        }
        //End SIW139

        public string DeductibleReimbursement
        {
            get
            {
                return Utils.getBool(Node, Constants.sUSDeductible, Constants.sUSDeductibleReimbursement);
            }
            set
            {
                Utils.putBool(putNode, Constants.sUSDeductible, Constants.sUSDeductibleReimbursement, value, NodeOrder);
            }
        }

        public string DisputedCase
        {
            get
            {
                return Utils.getBool(Node, Constants.sUSDisputedCase, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUSDisputedCase, "", value, NodeOrder);
            }
        }

        public string PossibleFraud
        {
            get
            {
                return Utils.getBool(Node, Constants.sUSPossibleFraud, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUSPossibleFraud, "", value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putFraudulentClaimInd(string sDesc, string sCode)
        {
            return putFraudulentClaimInd(sDesc, sCode, "");
        }

        public XmlNode putFraudulentClaimInd(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSPossibleFraud, sDesc, sCode, NodeOrder, scodeid);
        }

        public string FraudulentClaimInd
        {
            get
            {
                return Utils.getData(Node, Constants.sUSPossibleFraud);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSPossibleFraud, value, NodeOrder);
            }
        }

        public string FraudulentClaimInd_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSPossibleFraud);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSPossibleFraud, value, NodeOrder);
            }
        }

        public string FraudulentClaimInd_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSPossibleFraud);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSPossibleFraud, value, NodeOrder);
            }
        }
        //End SIW139

        public string LumpSum
        {
            get
            {
                return Utils.getBool(Node, Constants.sUSLumpSum, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUSLumpSum, "", value, NodeOrder);
            }
        }

        public string ClaimDenied
        {
            get
            {
                //return Utils.getBool(Node, "", Constants.sUSClaimDenied); //SIW423
                return Utils.getBool(Node, Constants.sUSClaimDenied, "" );  //SIW423 
            }
            set
            {
                //Utils.putBool(putNode, "", Constants.sUSClaimDenied, value, NodeOrder); //SIW423
                Utils.putBool(putNode, Constants.sUSClaimDenied, "", value, NodeOrder);   //SIW423 
            }
        }

        //Start SIW139
        public XmlNode putTypeRecovery(string sDesc, string sCode)
        {
            return putTypeRecovery(sDesc, sCode, "");
        }

        public XmlNode putTypeRecovery(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSTypeRecovery, sDesc, sCode, NodeOrder, scodeid);
        }

        public string TypeRecovery
        {
            get
            {
                return Utils.getData(Node, Constants.sUSTypeRecovery);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSTypeRecovery, value, NodeOrder);
            }
        }

        public string TypeRecovery_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSTypeRecovery);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSTypeRecovery, value, NodeOrder);
            }
        }

        public string TypeRecovery_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSTypeRecovery);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSTypeRecovery, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putTypeReserve(string sDesc, string sCode)
        {
            return putTypeReserve(sDesc, sCode, "");
        }

        public XmlNode putTypeReserve(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSTypeReserve, sDesc, sCode, NodeOrder, scodeid);
        }

        public string TypeReserve
        {
            get
            {
                return Utils.getData(Node, Constants.sUSTypeReserve);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSTypeReserve, value, NodeOrder);
            }
        }

        public string TypeReserve_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSTypeReserve);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSTypeReserve, value, NodeOrder);
            }
        }

        public string TypeReserve_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSTypeReserve);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSTypeReserve, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putTypeCoverage(string sDesc, string sCode)
        {
            return putTypeCoverage(sDesc, sCode, "");
        }

        public XmlNode putTypeCoverage(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSTypeCoverage, sDesc, sCode, NodeOrder, scodeid);
        }

        public string TypeCoverage
        {
            get
            {
                return Utils.getData(Node, Constants.sUSTypeCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSTypeCoverage, value, NodeOrder);
            }
        }

        public string TypeCoverage_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSTypeCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSTypeCoverage, value, NodeOrder);
            }
        }

        public string TypeCoverage_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSTypeCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSTypeCoverage, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putTypeLoss(string sDesc, string sCode)
        {
            return putTypeLoss(sDesc, sCode, "");
        }

        public XmlNode putTypeLoss(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSTypeLoss, sDesc, sCode, NodeOrder, scodeid);
        }

        public string TypeLoss
        {
            get
            {
                return Utils.getData(Node, Constants.sUSTypeLoss);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSTypeLoss, value, NodeOrder);
            }
        }

        public string TypeLoss_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSTypeLoss);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSTypeLoss, value, NodeOrder);
            }
        }

        public string TypeLoss_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSTypeLoss);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSTypeLoss, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putLossCoverage(string sDesc, string sCode)
        {
            return putLossCoverage(sDesc, sCode, "");
        }

        public XmlNode putLossCoverage(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSLossCoverage, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LossCoverage
        {
            get
            {
                return Utils.getData(Node, Constants.sUSLossCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSLossCoverage, value, NodeOrder);
            }
        }

        public string LossCoverage_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSLossCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSLossCoverage, value, NodeOrder);
            }
        }

        public string LossCoverage_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSLossCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSLossCoverage, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putSettlementType(string sDesc, string sCode)
        {
            return putSettlementType(sDesc, sCode, "");
        }

        public XmlNode putSettlementType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSSettleType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SettlementType
        {
            get
            {
                return Utils.getData(Node, Constants.sUSSettleType);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSSettleType, value, NodeOrder);
            }
        }

        public string SettlementType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSSettleType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSSettleType, value, NodeOrder);
            }
        }

        public string SettlementType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSSettleType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSSettleType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putSettlementMethod(string sDesc, string sCode)
        {
            return putSettlementMethod(sDesc, sCode, "");
        }

        public XmlNode putSettlementMethod(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSSettleMethod, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SettlementMethod
        {
            get
            {
                return Utils.getData(Node, Constants.sUSSettleMethod);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSSettleMethod, value, NodeOrder);
            }
        }

        public string SettlementMethod_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSSettleMethod);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSSettleMethod, value, NodeOrder);
            }
        }

        public string SettlementMethod_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSSettleMethod);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSSettleMethod, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putBenefitType(string sDesc, string sCode)
        {
            return putBenefitType(sDesc, sCode, "");
        }

        public XmlNode putBenefitType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSBenefitType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string BenefitType
        {
            get
            {
                return Utils.getData(Node, Constants.sUSBenefitType);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSBenefitType, value, NodeOrder);
            }
        }

        public string BenefitType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSBenefitType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSBenefitType, value, NodeOrder);
            }
        }

        public string BenefitType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSBenefitType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSBenefitType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putLossAct(string sDesc, string sCode)
        {
            return putLossAct(sDesc, sCode, "");
        }

        public XmlNode putLossAct(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSLossAct, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LossAct
        {
            get
            {
                return Utils.getData(Node, Constants.sUSLossAct);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSLossAct, value, NodeOrder);
            }
        }

        public string LossAct_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSLossAct);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSLossAct, value, NodeOrder);
            }
        }

        public string LossAct_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSLossAct);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSLossAct, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putJurisdictionState(string sDesc, string sCode)
        {
            return putJurisdictionState(sDesc, sCode, "");
        }

        public XmlNode putJurisdictionState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSJurisdictionState, sDesc, sCode, NodeOrder, scodeid);
        }

        public string JurisdictionState
        {
            get
            {
                return Utils.getData(Node, Constants.sUSJurisdictionState);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSJurisdictionState, value, NodeOrder);
            }
        }

        public string JurisdictionState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSJurisdictionState);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSJurisdictionState, value, NodeOrder);
            }
        }

        public string JurisdictionState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSJurisdictionState);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSJurisdictionState, value, NodeOrder);
            }
        }
        //End SIW139

        public string AttyFormRcptDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sUSAttyFormRcptDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sUSAttyFormRcptDate, value, NodeOrder);
            }
        }

        public string EmployerNotifiedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sUSEmployerNotifiedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sUSEmployerNotifiedDate, value, NodeOrder);
            }
        }

        public string EmployerNotfiedTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sUSEmployerNotifiedTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sUSEmployerNotifiedTime, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putEmployerWageRange(string sDesc, string sCode)
        {
            return putEmployerWageRange(sDesc, sCode, "");
        }

        public XmlNode putEmployerWageRange(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUSEmployerWageRange, sDesc, sCode, NodeOrder, scodeid);
        }

        public string EmployerWageRange
        {
            get
            {
                return Utils.getData(Node, Constants.sUSEmployerWageRange);
            }
            set
            {
                Utils.putData(putNode, Constants.sUSEmployerWageRange, value, NodeOrder);
            }
        }

        public string EmployerWageRange_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUSEmployerWageRange);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUSEmployerWageRange, value, NodeOrder);
            }
        }

        public string EmployerWageRange_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUSEmployerWageRange);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUSEmployerWageRange, value, NodeOrder);
            }
        }
        //End SIW139

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.UnitStats = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null) //SIW529
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        protected override void ResetParent()
        {
            Node = Utils.getNode(Parent.Node, NodeName);
        }

        private void subClearPointers()
        {
            Employee = null;
            PartyInvolved = null;
        }

        public XMLEmployee Employee
        {
            get
            {
                if (m_Employee == null)
                {
                    m_Employee = new XMLEmployee();
                    m_Employee.Parent = this;
                    //SIW529 LinkXMLObjects(m_Employee);

                }
                return m_Employee;
            }
            set
            {
                m_Employee = value;
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }
    }
}
