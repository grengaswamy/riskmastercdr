/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/20/2010 | SIW341  |    SW      | Subrogation's Status Desc field not saving
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLSubrogation : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '  <SUBROGATION ID="SUBxxx">
        '     <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '     <TYPE CODE="xx">type</TYPE>
        '     <ADVERSE_POLICY_NUM>policynumber</ADVERSE_POLICY_NUM>
        '     <ADVERSE_CLAIM_NUM>claimnumber</ADVERSE_CLAIM_NUM>
        '     <FIRST_NOTICE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <NEXT_NOTICE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COUNTERCLAIM>counterclaim</COUNTERCLAIM>
        '     <LITIGATION INDICATOR="TRUE|FALSE"/>
        '     <AWARD_AMOUNT>amount</AWARD_AMOUNT>
        '     <COMPARATIVE_NEGLIGENCE INDICATOR="TRUE|FALSE"/>
        '     <ADDITIONAL_ADVERSE>???</ADDITIONAL_ADVERSE>
        '     <STATUS CODE="xx">status</STATUS>
        '     <STATUS_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <STATUS_DESCRIPTION CODE="XXX">description</STATUS_DESCRIPTION>  'SIW341
        '     <NUMBER_OF_YEARS>years</NUMBER_OF_YEARS>
        '     <STATUTE_LIMITATION_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COVERAGE CODE="xx">coverage</COVERAGE>
        '     <ADV_PARTIES_COV_LIMIT>limit</ADV_PARTIES_COV_LIMIT>
        '     <AMOUNT_PAID>paid amount</AMOUNT_PAID>
        '     <COV_DED_AMOUNT>deductible</COV_DED_AMOUNT>
        '     <SETTLEMENT_PERCENT>percent</SETTLEMENT_PERCENT>
        '     <TOTAL_TO_BE_RECOVERED>totalamount</TOTAL_TO_BE_RECOVERED>
        '     <POTENTIAL_PAYOUT_AMT>amount</POTENTIAL_PAYOUT_AMT>
        '     <POTENTIAL_DED_AMT>amount</POTENTIAL_DED_AMT>
        '     <!Multiples Allowed>                                     '(SI04744 - Implemented in SI06333)
        '     <RELATED_ARBITRATION ID="ARBxxx"/>
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>        
        '  </SUBROGATION>
        '</CLAIM>*/

        private XMLClaim m_Claim;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLVarDataItem m_DataItem;
        private XMLActivityHistory m_ActivityHistory;
        private XMLDemandOffer m_DemandOffer;
        private XMLArbitration m_Arbitration;                   //(SI06023 - Implemented in SI06333) 
        
        public XMLSubrogation()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(28);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(29);                 //(SI06023 - Implemented in SI06333) //SIW490
            sThisNodeOrder = new ArrayList(34);                 //SIW490
            sThisNodeOrder.Add(Constants.sStdTechKey);              //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW490
            sThisNodeOrder.Add(Constants.sSUBType);
            sThisNodeOrder.Add(Constants.sSUBAdvPolicy);
            sThisNodeOrder.Add(Constants.sSUBAdvClaim);
            sThisNodeOrder.Add(Constants.sSUBFirstNoticeDate);
            sThisNodeOrder.Add(Constants.sSUBNextNoticeDate);
            sThisNodeOrder.Add(Constants.sSUBCounterClaim);
            sThisNodeOrder.Add(Constants.sSUBLitigation);
            sThisNodeOrder.Add(Constants.sSUBAwardAmount);
            sThisNodeOrder.Add(Constants.sSUBCompNegligence);
            sThisNodeOrder.Add(Constants.sSUBAddAdverse);
            sThisNodeOrder.Add(Constants.sSUBStatus);
            sThisNodeOrder.Add(Constants.sSUBStatusDate);
            sThisNodeOrder.Add(Constants.sSUBStatusDesc);
            sThisNodeOrder.Add(Constants.sSUBNbrOfYears);
            sThisNodeOrder.Add(Constants.sSUBStatLimDate);
            sThisNodeOrder.Add(Constants.sSUBCoverage);
            sThisNodeOrder.Add(Constants.sSUBAdvPartyCovLimit);
            sThisNodeOrder.Add(Constants.sSUBAmtPaid);
            sThisNodeOrder.Add(Constants.sSUBDedAmount);
            sThisNodeOrder.Add(Constants.sSUBSettlementPrc);
            sThisNodeOrder.Add(Constants.sSUBTotalTBRecovered);
            sThisNodeOrder.Add(Constants.sSUBPotPayoutAmt);
            sThisNodeOrder.Add(Constants.sSUBPotDedAmt);
            //sThisNodeOrder.Add(Constants.sSUBRelatedArb);         //SI06333 - (SI No not found)
            sThisNodeOrder.Add(Constants.sARBNode);                 //SI06333 - (SI No not found)
            sThisNodeOrder.Add(Constants.sSUBPartyInvolved);
            sThisNodeOrder.Add(Constants.sSUBActivityHistory);
            sThisNodeOrder.Add(Constants.sSUBCommentReference);
            sThisNodeOrder.Add(Constants.sSUBDataItem);
            //sThisNodeOrder.Add(Constants.sSUBTechKey);              //(SI06023 - Implemented in SI06333)//SIW360


            sThisNodeOrder.Add(Constants.sSubroDttmAdded);
            sThisNodeOrder.Add(Constants.sSubroDttmUpdated);
            sThisNodeOrder.Add(Constants.sSubroPreparedByUser);
            sThisNodeOrder.Add(Constants.sSubroUpdatedByUser);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sSUBNode; }
        }

        public override XmlNode Create()
        {
            return Create("","","","","","","");
        }

        public XmlNode Create(string sID, string sSubrogationType, string sSubrogationTypeCode, string sAdvPolicy,
                              string sAdvClaim, string sFNDate, string sNNDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ID = sID;
                putSubrogationType(sSubrogationType, sSubrogationTypeCode);
                AdversePolicyNumber = sAdvPolicy;
                AdverseClaimNumber = sAdvClaim;
                FirstNoticeDate = sFNDate;
                NextNoticeDate = sNNDate;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSubrogation.Create");
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sSUBIDPfx;
            }
        }

        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sSUBID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sSUBIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                    else
                        lid = strdata;
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lSubId = Globals.lSubId + 1;
                    lid = Globals.lSubId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sSUBID, Constants.sSUBIDPfx + (lid + ""));
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                Node = null;
                getFirst();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.Subrogation = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null) //SIW529
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DataItem = null;
            ActivityHistory = null;
        }

        //Start SIW139
        public XmlNode putSubrogationType(string sDesc, string sCode)
        {
            return putSubrogationType(sDesc, sCode, "");
        }

        public XmlNode putSubrogationType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSUBType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SubrogationType
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBType);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBType, value, NodeOrder);
            }
        }

        public string SubrogationType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSUBType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSUBType, value, NodeOrder);
            }
        }

        public string SubrogationType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSUBType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSUBType, value, NodeOrder);
            }
        }
        //End SIW139


        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sSubroPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sSubroPreparedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sSubroUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sSubroUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sSubroDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sSubroDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sSubroDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sSubroDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }




        public string AdversePolicyNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAdvPolicy);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAdvPolicy, value, NodeOrder);
            }
        }

        public string AdverseClaimNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAdvClaim);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAdvClaim, value, NodeOrder);
            }
        }

        public string AdditionalAdverse
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAddAdverse);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAddAdverse, value, NodeOrder);
            }
        }

        public string FirstNoticeDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSUBFirstNoticeDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSUBFirstNoticeDate, value, NodeOrder);
            }
        }

        public string NextNoticeDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSUBNextNoticeDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSUBNextNoticeDate, value, NodeOrder);
            }
        }

        public string CounterClaim
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBCounterClaim);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBCounterClaim, value, NodeOrder);
            }
        }

        public string Litigation
        {
            get
            {
                return Utils.getBool(Node, Constants.sSUBLitigation, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sSUBLitigation, "", value, NodeOrder);
            }
        }

        public string AwardAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAwardAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAwardAmount, value, NodeOrder);
            }
        }

        public string ComparativeNegligence
        {
            get
            {
                return Utils.getBool(Node, Constants.sSUBCompNegligence, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sSUBCompNegligence, "", value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSUBStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBStatus, value, NodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSUBStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSUBStatus, value, NodeOrder);
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSUBStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSUBStatus, value, NodeOrder);
            }
        }
        //End SIW139

        public string StatusDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSUBStatusDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSUBStatusDate, value, NodeOrder);
            }
        }

        public string StatuteLimitationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSUBStatLimDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSUBStatLimDate, value, NodeOrder);
            }
        }
        //Start SIW341
        //public string StatusDescription
        //{
        //    get
        //    {
        //        return Utils.getData(Node, Constants.sSUBStatusDesc);
        //    }
        //    set
        //    {
        //        Utils.putData(putNode, Constants.sSUBStatusDesc, value, NodeOrder);
        //    }
        //}
        public string StatusDesc
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBStatusDesc);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBStatusDesc, value, NodeOrder);
            }
        }

        public string StatusDesc_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSUBStatusDesc);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSUBStatusDesc, value, NodeOrder);
            }
        }

        public string StatusDesc_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSUBStatusDesc);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSUBStatusDesc, value, NodeOrder);
            }
        }
        //End SIW341

        public string NumberOfYears
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBNbrOfYears);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBNbrOfYears, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putCoverage(string sDesc, string sCode)
        {
            return putCoverage(sDesc, sCode, "");
        }

        public XmlNode putCoverage(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSUBCoverage, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Coverage
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBCoverage, value, NodeOrder);
            }
        }

        public string Coverage_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSUBCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSUBCoverage, value, NodeOrder);
            }
        }

        public string Coverage_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSUBCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSUBCoverage, value, NodeOrder);
            }
        }
        //End SIW139

        public string AdvPartyCovLimit
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAdvPartyCovLimit);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAdvPartyCovLimit, value, NodeOrder);
            }
        }

        public string AmountPaid
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBAmtPaid);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBAmtPaid, value, NodeOrder);
            }
        }

        public string DeductibleAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBDedAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBDedAmount, value, NodeOrder);
            }
        }

        public string SettlementPercent
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBSettlementPrc);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBSettlementPrc, value, NodeOrder);
            }
        }

        public string TotalTBRecovered
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBTotalTBRecovered);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBTotalTBRecovered, value, NodeOrder);
            }
        }

        public string PotentialPayoutAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBPotPayoutAmt);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBPotPayoutAmt, value, NodeOrder);
            }
        }

        public string PotentialDeductibleAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sSUBPotDedAmt);
            }
            set
            {
                Utils.putData(putNode, Constants.sSUBPotDedAmt, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490

        //Start(SI04744 - Implemented in SI06333)
        //public string RelatedArbitrationID
        //{
        //    get
        //    {
        //        string strdata = XML.XMLGetAttributeValue(Node, Constants.sSUBRelatedArb);
        //        string lid = "";
        //        if (strdata != "" && strdata != null)
        //            if (StringUtils.Left(strdata, 3) == Constants.sARBIDPfx && strdata.Length >= 4)
        //                lid = StringUtils.Right(strdata, strdata.Length - 3);
        //            else
        //                lid = strdata;
        //        return lid;
        //    }
        //    set
        //    {
        //        XML.XMLSetAttributeValue(putNode, Constants.sSUBRelatedArb, Constants.sARBIDPfx + value);
        //    }
        //}

        public XMLArbitration Arbitration
        {
            get
            {
                m_Arbitration = new XMLArbitration();
                m_Arbitration.Parent = this;
                //SIW529 LinkXMLObjects(m_Arbitration);
                m_Arbitration.getFirst();
                return m_Arbitration;
            }
            set
            {
                m_Arbitration = value;
            }

        }
        //End (SI04744 - Implemented in SI06333)

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLActivityHistory ActivityHistory
        {
            get
            {
                if (m_ActivityHistory == null)
                {
                    m_ActivityHistory = new XMLActivityHistory();
                    m_ActivityHistory.Parent = this;
                    //SIW529 LinkXMLObjects(m_ActivityHistory);
                    m_ActivityHistory.getFirst();
                }
                return m_ActivityHistory;
            }
            set
            {
                m_ActivityHistory = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {

            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getSubrogation(bool reset)
        {
            return getNode(reset);
        }
        
        public XmlNode FindByID(string sID)
        {
            if (ID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (ID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }
}
