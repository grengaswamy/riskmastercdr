/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors 
 * 08/06/2008 | SI06369 |    sw      | CommentHTML Tag fix
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/11/2010 | SIW353  |    AS      | XMLComment updated with Public/Private flags
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/21/2010 | SIW367  |    AS      | ParentCommentID added to comment xmlobject
 * 06/11/2010 | SIW367  |   SW       | Initial, Next and Previous CommentID.
 * 06/01/2010 | SIW481  |   SW       | Comments performance Issue.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 05/12/2012 | SI08479 | m.baum     | Author name import
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;

namespace CCP.XmlComponents
{
    public class XMLComment : XMLXCNodeWithList
    {
        /*'*****************************************************************************
        ' The cXMLComment class provides the functionality to support and manage a
        ' ClaimsPro Comment Node
        '*****************************************************************************
        '   <COMMENT ID="CMTxxx" PARENT_ID="CMTxxxparentcomment" NEXT_ID="CMTxxxNextCommentInList" PREV_ID="CMTxxxPreviousCommentInList"> 'SIW367
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <PARENT_COMMENT_ID>Comment ID of parent when appending another comment</PARENT_COMMENT_ID> 'SIW367
        '      <CATEGORY TYPE="xxxxx">Category Description</CATEGORY>
        '      <SUBJECT>Comment Subject</SUBJECT>
        '      <DATE YEAR="2002" MONTH="03" DAY="01" />
        '      <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '      <AUTHOR USER_ID="userid" ENTITY_ID="ENTxxx">                 //SI08479
        '          <NAME>authorfullname</NAME>                              //SI08479
        '          <FIRST_NAME>authorfullname</FULL_NAME>                   //SI08479        
        '          <LAST_NAME>authorfullname</LAST_NAME>                    //SI08479
        '      </AUTHOR>                                                    //SI08479
//SAI08479'      <AUTHOR USER_ID="userid" ENTITY_ID="ENTxxx"/>author name</AUTHOR>
        '      <TEXT>This is the text of the comment</TEXT>
        '      <TEXT_HTML>This is the text of the comment in HTML format</TEXT_HTML>   'SI06369        
        '      <PRIVATE_FLAG INDICATOR = "True|False"/>                     'SIW353
        '      <PUBLIC_FLAG INDICATOR = "True|False"/>                      'SIW353
        '      <PUBLICVIEWONLY_FLAG INDICATOR = "True|False"/>              'SIW353
        '      <UPDATE_ALLOWED INDICATOR = "True|False"/>                   'SIW353
        '      <APPEND_ALLOWED INDICATOR = "True|False"/>                   'SIW353
        '   </COMMENT>
        '*****************************************************************************/

        //XmlNode m_xmlCmtLine;
        //XmlNodeList m_xmlCmtLineList;

        private ArrayList sAuthNodeOrder;                                   //SI08479
        
        public XMLComment()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            //m_xmlCmtLine = null;
            //m_xmlCmtLineList = null;

            //sThisNodeOrder = new ArrayList(7);//(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(8);  //SI06369 //SIW353
            sThisNodeOrder = new ArrayList(15);  //SIW353
            sThisNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sThisNodeOrder.Add(Constants.sCmtParentID);//SIW367
            sThisNodeOrder.Add(Constants.sCmtCategory);
            sThisNodeOrder.Add(Constants.sCmtSubject);
            sThisNodeOrder.Add(Constants.sCmtDate);
            sThisNodeOrder.Add(Constants.sCmtTime);
            sThisNodeOrder.Add(Constants.sCmtAuthor);
            sThisNodeOrder.Add(Constants.sCmtText);
            //sThisNodeOrder.Add(Constants.sCmtTechKey);//(SI06023 - Implemented in SI06333)//SIW360
            sThisNodeOrder.Add(Constants.sCmtHTML);   //SI06369
            sThisNodeOrder.Add(Constants.sCmtPrivateFlag);   //SIW353
            sThisNodeOrder.Add(Constants.sCmtPublicFlag);   //SIW353
            sThisNodeOrder.Add(Constants.sCmtPublicViewOnlyFlag);   //SIW353
            sThisNodeOrder.Add(Constants.sCmtUpdateAllowed);   //SIW353
            sThisNodeOrder.Add(Constants.sCmtAppendOnlyAllowed);   //SIW353
            sThisNodeOrder.Add("DATELASTUPDATED");
            sThisNodeOrder.Add("TIMELASTUPDATED");   

            sAuthNodeOrder = new ArrayList(3);                              //SI08479
            sAuthNodeOrder.Add(Constants.sCmtAuthorName);                   //SI08479
            sAuthNodeOrder.Add(Constants.sCmtAuthorLastName);               //SI08479
            sAuthNodeOrder.Add(Constants.sCmtAuthorFirstName);              //SI08479
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
            }
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sCmtNode; }
        }

        //Start SI06369
        //public override XmlNode Create()
        //{
        //    return Create("", "", "", "", "", "", "");
        //}
        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "","","");                                                            //SI08479
//SI08479   return Create("", "", "", "", "", "", "", "");
        }
       
        //public XmlNode Create(int iCmtid, string sDate, string sTime, string sAuthor, string sUserId,
        //                      int iEntityID, string stext)
        //{
        //    return Create(iCmtid + "", sDate, sTime, sAuthor, sUserId, iEntityID + "", stext);
        //}
        public XmlNode Create(int iCmtid, string sDate, string sTime, string sAuthor, string sUserId,
                              int iEntityID, string stext, string stextHTML)                         
        {
            return Create(iCmtid + "", sDate, sTime, sAuthor, sUserId, iEntityID + "", stext, stextHTML,"","");             //SI08479
//SI08479   return Create(iCmtid + "", sDate, sTime, sAuthor, sUserId, iEntityID + "", stext, stextHTML, "", "");
        }
        
        //public XmlNode Create(string sCmtid, string sDate, string sTime, string sAuthor, string sUserId,
        //                      string sEntityID, string stext)                                             

        //SI08479public XmlNode Create(string sCmtid, string sDate, string sTime, string sAuthor, string sUserId,
        //SI08479                      string sEntityID, string stext, string stextHTML) //End SI06369                             
        public XmlNode Create(string sCmtid, string sDate, string sTime, string sAuthor, string sUserId,
                              string sEntityID, string stext, string stextHTML, string sAuthorFirstName,string sAuthorLastName) //SI08479
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false, null, null);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                CommentID = sCmtid;
                CmtDate = sDate;
                CmtTime = sTime;
                AuthorName = sAuthor;
                AuthorUserID = sUserId;
                AuthorEntityID = sEntityID;
                Text = stext;
                CommentHTML = stextHTML; //SI06369
                AuthorFirstName = sAuthorFirstName;                 //SI08479
                AuthorLastName = sAuthorLastName;                   //SI08479

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLComment.Create");
                return null;
            }
        }




        public string CmtDateLastUpdated
        {
            get
            {
                return Utils.getDate(Node, "DATELASTUPDATED");
            }
            set
            {
                Utils.putDate(putNode, "DATELASTUPDATED", value, NodeOrder);
            }
        }


        public string CmtTimeLastUpdated
        {
            get
            {
                return Utils.getTime(Node, "TIMELASTUPDATED");
            }
            set
            {
                Utils.putTime(putNode, "TIMELASTUPDATED", value, NodeOrder);
            }
        }



        public string Text
        {
            get
            {
                return Utils.getData(Node, Constants.sCmtText);
            }
            set
            {
                Utils.putData(putNode, Constants.sCmtText, value, NodeOrder);
            }
        }

        //Start SI06369
        public string CommentHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sCmtHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sCmtHTML, value, NodeOrder);
            }
        }
        //End SI06369

        public XmlNode putCommentType(string sType, string stypecode)
        {
            return Utils.putTypeItem(putNode, Constants.sCmtCategory, sType, stypecode, NodeOrder);
        }

        public string CommentType
        {
            get
            {
                return Utils.getData(Node, Constants.sCmtCategory);
            }
            set
            {
                Utils.putData(putNode, Constants.sCmtCategory, value, NodeOrder);
            }
        }

        public string CommentType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCmtCategory);
            }
            set
            {
                Utils.putType(putNode, Constants.sCmtCategory, value, NodeOrder);
            }
        }

        public string Subject
        {
            get
            {
                return Utils.getData(Node, Constants.sCmtSubject);
            }
            set
            {
                Utils.putData(putNode, Constants.sCmtSubject, value, NodeOrder);
            }
        }

//Start SI08479
        private XmlNode AuthorNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sCmtAuthor);    
            }
        }

        private XmlNode putAuthorNode
        {
            get
            {
                if (AuthorNode == null)
                    XML.XMLaddNode(putNode, Constants.sCmtAuthor, NodeOrder);
                return AuthorNode;
            }
        }

        public XmlNode putAuthorName(string UserID, string EntityIDRef, string FirstName, string LastName, string DisplayName)
        {
            if (UserID != "")
                AuthorUserID = UserID;
            if (EntityIDRef != "")
                AuthorEntityID = EntityIDRef;
            if (FirstName != "")
                AuthorFirstName = FirstName;
            if (LastName != "")
                AuthorLastName = LastName;
            if (DisplayName != "")
                AuthorName = DisplayName;
            return AuthorNode;
        }

        public string AuthorName
        {
            get
            {
                return Utils.getData(AuthorNode, Constants.sCmtAuthorName);
                //return Utils.getData(Node, Constants.AuthorNode);
            }
            set
            {
                Utils.putData(putAuthorNode, Constants.sCmtAuthorName, value, sAuthNodeOrder);
                //Utils.putData(putNode, Constants.sCmtAuthor, value, NodeOrder);
            }
        }

        public string AuthorFirstName
        {
            get
            {
                return Utils.getData(AuthorNode, Constants.sCmtAuthorFirstName);
            }
            set
            {
                Utils.putData(putAuthorNode, Constants.sCmtAuthorFirstName, value, sAuthNodeOrder);
            }
        }

        public string AuthorLastName
        {
            get
            {
                return Utils.getData(AuthorNode, Constants.sCmtAuthorLastName);
            }
            set
            {
                Utils.putData(putAuthorNode, Constants.sCmtAuthorLastName, value, sAuthNodeOrder);
            }
        }

        public string AuthorUserID
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sCmtAuthor, Constants.sCmtUserID);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sCmtAuthor, Constants.sCmtUserID, value, NodeOrder);
            }
        }

        public string AuthorEntityID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sCmtAuthor);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sCmtAuthor, value, NodeOrder);
            }
        }
//End SI08479

        public string CmtDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCmtDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCmtDate, value, NodeOrder);
            }
        }

        public string CmtTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sCmtTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sCmtTime, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder); //SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //Start SIW367
        public string InitialCommentID
        {
            get
            {
                string strdata;
                string lid = "";
                if (Node != null)
                {
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sCmtInitialID);
                    lid = ((Utils)Utils).extCommentID(strdata);
                }
                return lid;
            }
            set
            {
                string strdata = value;
                XML.XMLSetAttributeValue(putNode, Constants.sCmtInitialID, Constants.sCmtIDPfx + strdata.Trim());
            }
        }
        public XmlNode InitialComment()
        {
            string sID = "";
            sID = InitialCommentID;
            if (sID == "" || sID == "0")
                return null;
            else
                return getCommentbyID(sID);
        }
        public string NextCommentID
        {
            get
            {
                string strdata;
                string lid = "";
                if (Node != null)
                {
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sCmtNextID);
                    lid = ((Utils)Utils).extCommentID(strdata);
                }
                return lid;
            }
            set
            {
                string strdata = value;
                XML.XMLSetAttributeValue(putNode, Constants.sCmtNextID, Constants.sCmtIDPfx + strdata.Trim());
            }
        }
        public XmlNode NextComment()
        {
            string sID = "";
            sID = NextCommentID;
            if (sID == "" || sID == "0")
                return null;
            else
                return getCommentbyID(sID);
        }
        public string PrevCommentID
        {
            get
            {
                string strdata;
                string lid = "";
                if (Node != null)
                {
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sCmtPrevID);
                    lid = ((Utils)Utils).extCommentID(strdata);
                }
                return lid;
            }
            set
            {
                string strdata = value;
                XML.XMLSetAttributeValue(putNode, Constants.sCmtPrevID, Constants.sCmtIDPfx + strdata.Trim());
            }
        }        
        public XmlNode PrevComment()
        {
            string sID = "";
            sID = PrevCommentID;
            if (sID == "" || sID == "0")
                return null;
            else
                return getCommentbyID(sID); 
        } 
        //public string ParentCommentID //Start SIW367
        //{
        //    get
        //    {
        //        return Utils.getData(Node, Constants.sCmtParentID);
        //    }
        //    set
        //    {
        //        Utils.putData(putNode, Constants.sCmtParentID, value, NodeOrder); 
        //    }
        //}
        //End SIW367
       
        public string IDPrefix()
        {
            return Constants.sCmtIDPfx;
        }

        public string CommentID
        {
            get
            {
                string strdata;
                string lid = "";
                if (Node != null)
                {
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sCmtID);
                    lid = ((Utils)Utils).extCommentID(strdata);
                }
                return lid;
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null || strdata == "0")
                    strdata = ((XMLComments)Parent).getNextCmtID(null);
                XML.XMLSetAttributeValue(putNode, Constants.sCmtID, Constants.sCmtIDPfx + strdata.Trim());
            }
        }

        //Begin SIW353
        public string PrivateFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sCmtPrivateFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sCmtPrivateFlag, "", value, NodeOrder);
            }
        }
        public string PublicFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sCmtPublicFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sCmtPublicFlag, "", value, NodeOrder);
            }
        }
        public string PublicViewOnlyFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sCmtPublicViewOnlyFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sCmtPublicViewOnlyFlag, "", value, NodeOrder);
            }
        }
        public string UpdateAllowed
        {
            get
            {
                return Utils.getBool(Node, Constants.sCmtUpdateAllowed, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sCmtUpdateAllowed, "", value, NodeOrder);
            }
        }
        public string AppendOnlyAllowed
        {
            get
            {
                return Utils.getBool(Node, Constants.sCmtAppendOnlyAllowed, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sCmtAppendOnlyAllowed, "", value, NodeOrder);
            }
        }
        //End SIW353

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            Parent.Comment = this;
            Parent.Count = Parent.Count + 1;
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getComment(bool rst)
        {
            return getNode(rst);
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLComments Parent
        {
            get
            {
                if (m_Parent == null)
                {   //SIW529
                    m_Parent = new XMLComments();
                    ((XMLComments)m_Parent).Comment = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW529
                }   //SIW529
                return (XMLComments)m_Parent;
            }
            set
            {
                m_Parent = value;
                if (m_Parent != null)
                    ((XMLComments)m_Parent).Comment = this;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
        //Start SIW481
        public XmlNode FindComment()
        {
            return FindComment("", ""); 
        }
        public XmlNode FindComment(string sTechID, string sCommentID)
        {
            bool rst = true;
            if ((sTechID != "" && sTechID == Techkey) || (sCommentID != "" && sCommentID == CommentID))
                return Node;
            while (getComment(rst) != null)
            {
                rst = false;
                if (sTechID != "")
                    if (sTechID == Techkey)
                        break;
                if (sCommentID != "")
                    if (sCommentID == CommentID)
                        break;
            }
            return Node;
        }
        public XmlNode getCommentbyTechKey(string sTechID)
        {
            bool rst = true;
            if (sTechID != Techkey)
                while (getComment(rst) != null)
                {
                    rst = false;
                    if (sTechID == Techkey)
                        break;
                }
            return Node;
        }
        //End SIW481
        public XmlNode getCommentbyID(string sCmtid)
        {
            if (sCmtid != CommentID)
            {
                getFirst();
                while (Node != null)
                {
                    if (CommentID == sCmtid)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }

    public class XMLComments:XMLXCNode
    {
        /*'*****************************************************************************
        ' The cXMLComments class provides the functionality to support and manage a
        ' ClaimsPro Comments Collection Node
        '*****************************************************************************
        '<COMMENTS COUNT="xx" NEXT_ID="xx"
        '   <COMMENT COMMENT_ID="CMTxxx">
        '   </COMMENT>
        '</COMMENTS>
        '*****************************************************************************/

        XMLComment m_Comment;
        
        public XMLComments()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sCmtNode);
            m_Comment = null;
            Utils.subSetupDocumentNodeOrder();
            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public XMLComment Comment
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = new XMLComment();
                    m_Comment.Parent = this;
                    //SIW529 LinkXMLObjects(m_Comment);
                    m_Comment.getComment(Constants.xcGetFirst);     //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sCmtCollection; }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
            }
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc)
        {
            return Create(xmlDoc, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextCmtID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = (XmlNode)xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextCmtID = sNextCmtID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLComments.Create");
                return null;
            }
        }

        public string getNextCmtID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextCmtID;
            if (nid == "" || nid == null)
                nid = "1";
            NextCmtID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextCmtID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sCmtNextID);
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null)
                    strdata = "1";
                Utils.putAttribute(putNode, "", Constants.sCmtNextID, strdata, NodeOrder);
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sCmtCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sCmtCount, value + "", NodeOrder);
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = (XML)value;
                ResetParent();
            }
        }
        //Start SI06369
        //public XmlNode AddComment(string sCmtid, string sDate, string sTime, string sAuthor, string sUserID,
        //                          string sEntityID, string stext)
        //{
        //    return Comment.Create(sCmtid, sDate, sTime, sAuthor, sUserID, sEntityID, stext);
        //}
        public XmlNode AddComment(string sCmtid, string sDate, string sTime, string sAuthor, string sUserID,
                                  string sEntityID, string stext, string stextHTML)
        {
            return Comment.Create(sCmtid, sDate, sTime, sAuthor, sUserID, sEntityID, stext, stextHTML, "", "");             //SI08479
//SI08479   return Comment.Create(sCmtid, sDate, sTime, sAuthor, sUserID, sEntityID, stext, stextHTML);
        }
        //End SI06369
    
        public XmlNode getComment(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Comment.getComment(reset);
        }

        public XmlNode getCommentbyID(XmlDocument xdoc, string sCmtid)
        {
            if (xdoc != null)
                Document = xdoc;
            return Comment.getCommentbyID(sCmtid);
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }
    }
}
