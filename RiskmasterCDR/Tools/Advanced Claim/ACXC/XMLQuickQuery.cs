﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using CCP.Constants;
using CCP.XmlFormatting;
using System.Collections;

namespace CCP.XmlComponents
{
    public class XMLQuickQuery : XMLXCNodeWithList
    {

        /*
         * This class provides the ability to extract data from Main Database.
         * <QUICKQUERY COMPONENT="" ESCAPE="True|False" NOTIFICATION_ID="" PREV_NOTIFICATION_ID="">  
         *      <SCRIPT_ACTION></SCRIPT_ACTION>
                <DISPLAY_TYPE OK_CLICK"" CANCEL_CLICK="">TYPE OF DISPLAY(HARD PROMPT, ALERT, CONFIRMATION, RESELECTION, RESELECTION-DUPLICATE)</DISPLAY_TYPE>
         *      <DISPLAY_MSG>The custom string to be displayed to the user</DISPLAY_MSG>
         *      <USER_ACTION></USER_ACTION>
                <QUERYDATA>
                    <!--Custom Data to  be displayed, in Serialized format-->
                </QUERYDATA>
                <RESELECT>
                    <ASSOCIATED_COMP TYPE="" PROPERTY="" VALUE=""/> <!-- [0 ...n occurences] -->
                </RESELECT>		
            </QUICKQUERY>         
         */
        #region Private Fields
        ArrayList sAssoCompNodeOrder;
        XmlNode m_ReSelectNode;
        XmlNodeList m_AsscCompList;
        #endregion

        public XMLQuickQuery()
        {
            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sQQQuickQuery);
            sThisNodeOrder.Add(Constants.SQQScriptAction);
            sThisNodeOrder.Add(Constants.sQQDisplayType);
            sThisNodeOrder.Add(Constants.sQQDisplayMessage);
            sThisNodeOrder.Add(Constants.SQQAction);     
            sThisNodeOrder.Add(Constants.SQQQueryData);
            sThisNodeOrder.Add(Constants.SQQUserReSelect);
            sAssoCompNodeOrder = new ArrayList();
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sQQQuickQuery; }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLGetNode(Parent.Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        public XmlNode QueryDataNode { get; set; }        

        public XmlNode UserActionNode { get; set; }

        public XmlNode DisplayTypeNode { get; set; }

        public XmlNode AssoComponentNode { get; set; }

        public XmlNode DisplayMsgnode { get; set; }

        public XmlNode ScriptActionNode { get; set; }

        public string QueryDataValue
        {
            get
            {
                return Utils.getData(Node, Constants.SQQQueryData);
            }
            set
            {
                Utils.putData(putNode, Constants.SQQQueryData, value, NodeOrder);
            }
        }

        public string UserActionType
        {
            get
            {
                return Utils.getData(Node, Constants.SQQAction);
            }
            set
            {
                
                Utils.putData(putNode, Constants.SQQAction, value, NodeOrder);
            }
        }

        public string Component
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sSysComponent);
            }
            set
            {
                XML.XMLSetAttributeValue(Node, Constants.sSysComponent, value);
            }
        }

        public string Escape
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sQQQueryEscape);
            }
            set
            {
                XML.XMLSetAttributeValue(Node, Constants.sQQQueryEscape, value);
            }
        }

        public string NotificationId
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sQQNotificationId);
            }
            set
            {
                XML.XMLSetAttributeValue(Node, Constants.sQQNotificationId, value);
            }
        }

        public string PreviousNotificationId
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sQQPrevNotificationId);
            }
            set
            {
                XML.XMLSetAttributeValue(Node, Constants.sQQPrevNotificationId, value);
            }
        }

        public string ScriptAction
        {
            get
            {
                return Utils.getData(Node, Constants.SQQScriptAction);
            }
            set
            {

                Utils.putData(putNode, Constants.SQQScriptAction, value, NodeOrder);
            }
        }


        public string DisplayType
        {
            get
            {
                return Utils.getData(Node, Constants.sQQDisplayType);
            }
            set
            {

                Utils.putData(putNode, Constants.sQQDisplayType, value, NodeOrder);
            }
        }

        public XmlNode putDisplayTypeNode()
        {
            if (DisplayTypeNode == null)
                DisplayTypeNode = XML.XMLaddNode(putNode, Constants.sQQDisplayType, Utils.sNodeOrder);
            return DisplayTypeNode;
        }

        public XmlNode putDisplayTypeNode(string sProceedClick, string sCancelClick)
        {
            if (DisplayTypeNode == null)
            {
                DisplayTypeNode = XML.XMLaddNode(putNode, Constants.sQQDisplayType, Utils.sNodeOrder);
                ProceedClick = sProceedClick;
                CancelClick = sCancelClick;
            }
            return DisplayTypeNode;
        }


        public string ProceedClick
        {
            get
            {
                return XML.XMLGetAttributeValue(DisplayTypeNode, Constants.sQQProceedClick);                
            }
            set
            {
                XML.XMLSetAttributeValue(putDisplayTypeNode(), Constants.sQQProceedClick, value);
            }
        }

        public string CancelClick
        {
            get
            {
                return XML.XMLGetAttributeValue(DisplayTypeNode, Constants.sQQCancelClick);
            }
            set
            {
                XML.XMLSetAttributeValue(putDisplayTypeNode(), Constants.sQQCancelClick, value);
            }
        }

        public string DisplayMessage
        {
            get { return Utils.getData(Node, Constants.sQQDisplayMessage); }
            set { Utils.putData(putNode, Constants.sQQDisplayMessage, value, NodeOrder); }
        }

        public XmlNode ReSelectNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.SQQUserReSelect);
            }
            set
            {
                m_ReSelectNode = value;
            }
        }

        public XmlNode putReSelectNode
        {
            get
            {
                if (ReSelectNode == null)
                {
                    ReSelectNode = XML.XMLaddNode(putNode, Constants.SQQUserReSelect, NodeOrder);                    
                }
                return ReSelectNode;
            }
        }

        public string AssociatedComponentType
        {
            get
            {
                string sData = string.Empty;
                if (AssoComponentNode != null)
                {
                    sData = XML.XMLGetAttributeValue(AssoComponentNode, Constants.sSysType);                    
                }
                return sData;
            }
            set
            {
                XML.XMLSetAttributeValue(putAssoCompNode(), Constants.sSysType, value);
            }
        }

        public string AssociatedProperty
        {
            get
            {
                string sData = string.Empty;
                if (AssoComponentNode != null)
                {
                    sData = XML.XMLGetAttributeValue(AssoComponentNode, Constants.sQQProperty);
                }
                return sData;
            }
            set
            {
                XML.XMLSetAttributeValue(putAssoCompNode(), Constants.sQQProperty, value);
            }
        }

        public string AssociatedReselectValue
        {
            get
            {
                string sData = string.Empty;
                if (AssoComponentNode != null)
                {
                    sData = XML.XMLGetAttributeValue(AssoComponentNode, Constants.sSysValue);
                }
                return sData;
            }
            set
            {
                XML.XMLSetAttributeValue(putAssoCompNode(), Constants.sSysValue, value);
            }
        }

        public XmlNode putAssoCompNode()
        {
            if (AssoComponentNode == null)                
                AssoComponentNode = XML.XMLaddNode(putReSelectNode, Constants.sQQAssociatedComp, Utils.sNodeOrder);
            return AssoComponentNode;
        }

        public XmlNode putAsscCompNode(string sComponentType, string sPropName, string sReselectValue)
        {
            if (AssoComponentNode == null)
            {
                AssoComponentNode = XML.XMLaddNode(putReSelectNode, Constants.sQQAssociatedComp, Utils.sNodeOrder);
                AssociatedComponentType = sComponentType;
                AssociatedProperty = sPropName;
                AssociatedReselectValue = sReselectValue;
            }
            return AssoComponentNode;
        }

        public XmlNode putQueryDataNode(string sQueryData)
        {
            if (QueryDataNode == null)
            {
                QueryDataNode = XML.XMLaddNode(putNode, Constants.SQQQueryData, Utils.sNodeOrder);
                QueryDataValue = sQueryData;
            }                                   
            return QueryDataNode;
        }

        public XmlNodeList AssociatedComponentList
        {
            get
            {                
                if (m_AsscCompList == null)
                {                    
                    return getNodeList(Constants.sQQAssociatedComp, ref m_AsscCompList, ReSelectNode);    //SIW7049
                    
                }
                else
                {
                    return m_AsscCompList;
                }                
            }
        }

        public XmlNode getAssociatedComponent(bool reset)
        {
            object o = AssociatedComponentList;
            XmlNode objAssociatedNode = this.AssoComponentNode;
            return getNode(reset, ref m_AsscCompList, ref objAssociatedNode);
        }

        public override XmlNode Create()
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XmlQuickQuery.Create");
                return null;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
                ResetParent();
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public T GetQueryData<T>()
        {
            T objQueryData = default(T);
            QueryDataNode = XML.XMLGetNode(putNode, Constants.SQQQueryData);
            if (QueryDataNode != null)
            {                
                using (StringReader sr = new StringReader(QueryDataValue))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(T));
                    objQueryData = (T)xs.Deserialize(sr);
                    sr.Close();
                }                
            }
            return objQueryData;
        }

        private void subClearPointers()
        {
            m_ReSelectNode = null;
            m_AsscCompList = null;

            QueryDataNode = null;
            UserActionNode = null;
            DisplayTypeNode = null;
            DisplayMsgnode = null;
            ScriptActionNode = null;
            AssoComponentNode = null;
        }
    }
}
