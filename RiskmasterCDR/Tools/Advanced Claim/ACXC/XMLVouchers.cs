/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 01/13/2010 | SI06650 |    NDB     | Fix for GetPartyInvolvedByRoleCode  
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 05/12/2010 | SIW451  |    AS      | TechKey added to target and originating banks
*' SIW489 7/15/2010 hfw - changes for WEB based Combined Pay
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/31/2011 | SIW529  |    AS      | Fix for non-static vouchers object
 '*SIN7178 5/4/2011 hfw -add future print date for payments
 * 01/31/2012 | SIW7928 |    PB      | Bank Account combo box processing on Payment Panel.
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;

namespace CCP.XmlComponents
{
    public class XMLVoucher : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*'<VOUCHERS COUNT=count>
        '   <VOUCHER ID="VCHxxx" MANUAL_IND="TRUE" TYPE="[CHECK|DRAFT|RECEIPT]>
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 Implemented in SI06333)
        '      <VOUCHER_AMOUNT>100</VOUCHER_AMOUNT>
        '      <CONTROL_NUMBER>0000311</CONTROL_NUMBER>
        '      <ORIGINATING_BANK ID="ENTxx" CODE="banknumber">
        '         <ACCOUNT_NAME>AccountName<ACCOUNT_NAME>'SIW7928
        '         <ROUTING_NUMBER>Routing</ROUTING_NUMBER>
        '         <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '         <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '      </ORIGINATING_BANK>
        '      <TARGET_BANK ID="ENTxx" CODE="banknumber">
        '         <ACCOUNT_NAME>AccountName<ACCOUNT_NAME>'SIW7928
        '         <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '         <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '         <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '      </TARGET_BANK>
        '      <VOUCHER_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <RECEIPT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <POST_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <PRINT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <HONORED_DATE YEAR="2002" MONTH="03" DAY="07" />               'SI04589
        '      <VOID_DATE YEAR="2002" MONTH="03" DAY="07" />                  'SI04669
        '      <DATE_OF_VOUCHER YEAR="2002" MONTH="03" DAY="07" />
        '      <VOUCHER_BATCH_NUMBER>0</VOUCHER_BATCH_NUMBER>
        '      <1099_REPORTABLE CODE="**" />
        '      <VOUCHER_STATUS CODE="O">Open</VOUCHER_STATUS>
        '      <MEMO>memo phrase</MEMO>
        '      <PAYEE_VERBIAGE MANUAL_IND="YES|NO">payee verbiage block</PAYEE_VERBIAGE> 'SI04679
        '      <PREPARED_BY_USER>userid</PREPARED_BY_USER>
        '      <DISTRIBUTION_TYPE CODE="x" CODE_ID="id">Distribution Type</DISTRIBUTION_TYPE>  'SIW7928
// Begin SIW489
        '      <COMB_PAY CODE="CBP">Combined Payment>
        '         <TECH_KEY>COMB_PAY_ENTITY_ID</TECH_KEY>
        '      </COMB_PAY>
// End SIW489
        '      <!Multiple Invoices Allowed>
        '      <INVOICE>
        '         <NUMBER>invoice number</NUMBER>
        '         <DATE YEAR="2002" MONTH="03" DAY="07" />
        '         <INVOICED_BY>invoiced by</INVOICED_BY>
        '         <PO_NUMBER>PO Number</PO_NUMBER>
        '         <SERVICE_FROM_DATE YEAR="2002" MONTH="03" DAY="20" />
        '         <SERVICE_TO_DATE YEAR="2002" MONTH="03" DAY="20" />
        '         <AMOUNT>0</INVOICE_AMOUNT>
        '      </INVOICE>
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... payees, banks, others>
        '         <TYPE>Payee</TYPE>
        '         <DATA_ITEM NAME="PayeePhrase" TYPE="String">payee phrase</DATA_ITEM>
        '      </PARTY_INVOLVED>
        '      <ADDRESS_REFERENCE ID="addressID" CODE="Relationship Code">Type of</ADDRESS>
        '      <COMMENT_REFERENCE> ... </COMMENT>        
        '   </VOUCHER>
        '<VOUCHERS>*/

        private XmlNode m_xmlInvoice;
        private XmlNodeList m_xmlInvoiceList;
        //private IEnumerator m_xmlInvoiceListEnum;
        //private XMLVouchers m_Vouchers; //SIW529
        private XmlNode m_xmlTargetBank;
        private XmlNode m_xmlOriginBank;
        private ArrayList sGRPNodeOrder;
        private ArrayList sBNKNodeOrder;
        private ArrayList sInvNodeOrder;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLAddressReference m_AddressReference;
        private Dictionary<object, object> dctVCHType;
        private XmlNode m_xmlCombPay; // SIW489
        private ArrayList sCombPayNodeOrder; // SIW489

        public string MailToCode;
        public string PayeeCode;
        
        public XMLVoucher()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;
            ResetMailToCode();
            ResetPayeeCode();

            sGRPNodeOrder = new ArrayList(1);
            sGRPNodeOrder.Add(Constants.sVCHVoucher);

            //sThisNodeOrder = new ArrayList(19);               //(SI06023 Implemented in SI06333)
            // SIW489 sThisNodeOrder = new ArrayList(20);                 //(SI06023 Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(21); // SIW489 //SIW7928    
            sThisNodeOrder = new ArrayList(29);   //SIW7928                
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sVCHAmount);
            sThisNodeOrder.Add(Constants.sVCHControlNumber);
            sThisNodeOrder.Add(Constants.sVCHTargetBank);
            sThisNodeOrder.Add(Constants.sVCHOriginatingBank);
            sThisNodeOrder.Add(Constants.sVCHDocumentNumber);
            sThisNodeOrder.Add(Constants.sVCHDate);
            sThisNodeOrder.Add(Constants.sVCHReceiptDate);
            sThisNodeOrder.Add(Constants.sVCHPostDate);
            sThisNodeOrder.Add(Constants.sVCHPrintDate);
            sThisNodeOrder.Add(Constants.sVCHDateToPrint); //  SIN7178
            sThisNodeOrder.Add(Constants.sVCHHonoredDate);
            sThisNodeOrder.Add(Constants.sVCHVoidDate);
            sThisNodeOrder.Add(Constants.sVCHBatchNumber);
            sThisNodeOrder.Add(Constants.sVCHStatus);
            sThisNodeOrder.Add(Constants.sVCHMemo);
            sThisNodeOrder.Add(Constants.sVCHPayeeVerbiage);
            sThisNodeOrder.Add(Constants.sVCHCombPayCode);
            sThisNodeOrder.Add(Constants.sVCHPreparedByUser);
            sThisNodeOrder.Add(Constants.sDistributionType);        // SIW7928
            sThisNodeOrder.Add(Constants.sVCHPartyInvolved);
            sThisNodeOrder.Add(Constants.sVCHAddressReference);
            sThisNodeOrder.Add(Constants.sVCHCommentReference);


            sThisNodeOrder.Add(Constants.sFiled1099Flag);
            sThisNodeOrder.Add(Constants.sfundsUpdatedByUser);
            sThisNodeOrder.Add(Constants.sFundsDttmAdded);
            sThisNodeOrder.Add(Constants.sFundsDttmUpdated);

            sThisNodeOrder.Add(Constants.sFundsApproveUser);
            sThisNodeOrder.Add(Constants.sFundsDttmApproval);

            //sThisNodeOrder.Add(Constants.sVCHTechKey);          //(SI06023 Implemented in SI06333) //SIW360

            //sBNKNodeOrder = new ArrayList(3);//SIW451
            //sBNKNodeOrder = new ArrayList(4);//SIW451 //SIW7928
            sBNKNodeOrder = new ArrayList(5);//SIW7928
            sBNKNodeOrder.Add(Constants.sStdTechKey);//SIW451
            sBNKNodeOrder.Add(Constants.sVCHAccountName); //SIW7928
            sBNKNodeOrder.Add(Constants.sVCHRoutingNumber);
            sBNKNodeOrder.Add(Constants.sVCHAccountNumber);
            sBNKNodeOrder.Add(Constants.sVCHFractionalNumber);

            sCombPayNodeOrder = new ArrayList(1); // SIW489
            sCombPayNodeOrder.Add(Constants.sStdTechKey); // SIW489

            sInvNodeOrder = new ArrayList(7);
            sInvNodeOrder.Add(Constants.sVCHInvNbr);
            sInvNodeOrder.Add(Constants.sVCHInvDate);
            sInvNodeOrder.Add(Constants.sVCHInvBy);
            sInvNodeOrder.Add(Constants.sVCHPONbr);
            sInvNodeOrder.Add(Constants.sVCHSvcFromDate);
            sInvNodeOrder.Add(Constants.sVCHSvcToDate);
            sInvNodeOrder.Add(Constants.sVCHInvAmount);

            dctVCHType = new Dictionary<object, object>();
            dctVCHType.Add(Constants.sUndefined, Constants.iUndefined);
            dctVCHType.Add(Constants.iUndefined, Constants.sUndefined);
            dctVCHType.Add(Constants.sVCHTypeCheck, Constants.iVCHTypeCheck);
            dctVCHType.Add(Constants.iVCHTypeCheck, Constants.sVCHTypeCheck);
            dctVCHType.Add(Constants.sVCHTypeDraft, Constants.iVCHTypeDraft);
            dctVCHType.Add(Constants.iVCHTypeDraft, Constants.sVCHTypeDraft);
            dctVCHType.Add(Constants.sVCHTypeReceipt, Constants.iVCHTypeReceipt);
            dctVCHType.Add(Constants.iVCHTypeReceipt, Constants.sVCHTypeReceipt);

            Utils.subSetupDocumentNodeOrder();
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sVCHVoucher; }
        }

        public override XmlNode Create()
        {
//  SIN7178            return Create("", enVCHType.enVCHUnSpecified, "", "", "", "", "", "", "", "", "", "", "", "", "");
            return Create("", enVCHType.enVCHUnSpecified, "", "", "", "", "", "", "", "", "", "", "", "", "",""); //  SIN7178
        }

 //' Begin SIN7178
        //public XmlNode Create(string sID, enVCHType cType, string sManInd, string sDocNumber, string sCntrlNumber,
        //                      string sOrigAcctNum, string sOrigBankNum, string sAmount, string sDate, string sStatus,
        //                      string sStatusCd, string sPrintDate, string sPostDate, string sReceiptDate, string sBatchNumber)
        public XmlNode Create(string sID, enVCHType cType, string sManInd, string sDocNumber, string sCntrlNumber,
                              string sOrigAcctNum, string sOrigBankNum, string sAmount, string sDate, string sStatus,
                              string sStatusCd, string sPrintDate, string sPostDate, string sReceiptDate, string sBatchNumber,
                              string sDateToPrint)
// End SIN7178
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Debug.PushProc("XMLVoucher.Create");
            Debug.DebugTrace(sID, cType, sDocNumber, sCntrlNumber, sAmount);

            xmlThisNode = null;

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                VoucherID = sID;
                VoucherType = cType;
                ManualInd = sManInd;
                DocumentNumber = sDocNumber;
                OriginatingAccountNumber = sOrigAcctNum;
                OriginatingBankNumber = sOrigBankNum;
                ControlNumber = sCntrlNumber;
                Amount = sAmount;
                VoucherDate = sDate;
                PrintDate = sPrintDate;
                DateToPrint = sDateToPrint; //  SIN7178
                PostDate = sPostDate;
                ReceiptDate = sReceiptDate;
                BatchNumber = sBatchNumber;
                putStatus(sStatus, sStatusCd);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                Debug.PopProc();
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLVoucher.Create");
                Debug.PopProc();
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sVCHIDPfx;
            }
        }

        public string VoucherID
        {
            get
            {
                return stripID(Utils.getAttribute(Node, "", Constants.sVCHID)); 
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                    lid = Parent.getNextVchID(null);
                Utils.putAttribute(putNode, "", Constants.sVCHID, Constants.sVCHIDPfx + lid, NodeOrder);
            }
        }

        public string VoucherType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sVCHType);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sVCHType, value, NodeOrder);
            }
        }

        public enVCHType VoucherType
        {
            get
            {
                object etype;
                string sdata = Utils.getAttribute(Node, "", Constants.sVCHType);
                if (!dctVCHType.TryGetValue(sdata, out etype))
                    etype = enVCHType.enVCHUnSpecified;
                return (enVCHType)etype;
            }
            set
            {
                object sdata;
                if(!dctVCHType.TryGetValue((int)value, out sdata))
                    sdata = value;
                Utils.putAttribute(putNode, "", Constants.sVCHType, (string)sdata, NodeOrder); 
            }
        }

        public string ManualInd
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sVCHManualInd);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sVCHManualInd, value, NodeOrder);
            }
        }

        public string ApproveUser
        {
            get
            {
                return Utils.getData(Node, Constants.sFundsApproveUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFundsApproveUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DttmApproval
        {
            get
            {
                return Utils.getData(Node, Constants.sFundsDttmApproval);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFundsDttmApproval, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }


       
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sfundsUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sfundsUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sFundsDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFundsDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sFundsDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFundsDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }



        public string Amount
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHAmount, value, NodeOrder);
            }
        }

        public string ControlNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHControlNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHControlNumber, value, NodeOrder);
            }
        }

        public string DocumentNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHDocumentNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHDocumentNumber, value, NodeOrder);
            }
        }

        public string VoucherDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHDate, value, NodeOrder);
            }
        }

        public string SERDTETO
        {
            get
            {
                return Utils.getDate(Node, "SERDTETO");
            }
            set
            {
                Utils.putDate(putNode, "SERDTETO", value, NodeOrder);
            }
        }

        public string SERDTEFROM
        {
            get
            {
                return Utils.getDate(Node, "SERDTEFROM");
            }
            set
            {
                Utils.putDate(putNode, "SERDTEFROM", value, NodeOrder);
            }
        }

        public string ReceiptDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHReceiptDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHReceiptDate, value, NodeOrder);
            }
        }

        public string PostDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHPostDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHPostDate, value, NodeOrder);
            }
        }

        public string PrintDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHPrintDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHPrintDate, value, NodeOrder);
            }
        }


        // Begin SIN7178
        public string DateToPrint
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHDateToPrint);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHDateToPrint, value, NodeOrder);
            }
        }
// End SIN7178

        public string HonoredDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHHonoredDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHHonoredDate, value, NodeOrder);
            }
        }

        public string VoidDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sVCHVoidDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sVCHVoidDate, value, NodeOrder);
            }
        }

        public string BatchNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHBatchNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHBatchNumber, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sVCHStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHStatus, value, NodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sVCHStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sVCHStatus, value, NodeOrder);
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sVCHStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sVCHStatus, value, NodeOrder);
            }
        }
        //End SIW139

        public string Memo
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHMemo);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHMemo, value, NodeOrder);
            }
        }

        public void putPayeeVerbiage(string sVerbiage, bool bManual)
        {
            PayeeVerbiage = sVerbiage;
            PayeeVerbiageManual = bManual.ToString();
        }

        public string PayeeVerbiage
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHPayeeVerbiage);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHPayeeVerbiage, value, NodeOrder);
            }
        }

        public string PayeeVerbiageManual
        {
            get
            {
                return Utils.getBool(Node, Constants.sVCHPayeeVerbiage, Constants.sVCHPayeeVerbiageManual);
            }
            set
            {
                Utils.putBool(putNode, Constants.sVCHPayeeVerbiage, Constants.sVCHPayeeVerbiageManual, value, NodeOrder);
            }
        }

        public string Filed1099Flag
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sFiled1099Flag);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sFiled1099Flag, value, NodeOrder);
            }
        }

        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sVCHPreparedByUser);
            }
            set
            {
                Utils.putData(putNode, Constants.sVCHPreparedByUser, value, NodeOrder);
            }
        }

        public void ResetMailToCode()
        {
            MailToCode = Constants.MAILTO_CODE;
        }

        public void ResetPayeeCode()
        {
            PayeeCode = Constants.PAYEE_CODE;
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

// Begin SIW489
        public XmlNode addCombPay(string sCombPayEntityRowID, string sCombPayCode)
        {
            CombPayTechKey = sCombPayEntityRowID;
            CombPayCode_CodeID = sCombPayCode;
            return m_xmlCombPay;
        }

        public XmlNode putCombPayNode
        {
            get
            {
                if (CombPayNode == null)
                    m_xmlCombPay = XML.XMLaddNode(Node, Constants.sVCHCombPay, NodeOrder);
                return m_xmlCombPay;
            }
        }

        public XmlNode CombPayNode
        {
            get
            {
                if (m_xmlCombPay == null)
                    m_xmlCombPay = XML.XMLGetNode(Node, Constants.sVCHCombPay);
                return m_xmlCombPay;
            }
            set
            {
                m_xmlCombPay = value;
            }
        }
        public string CombPayTechKey
        {
            get
            {
                return Utils.getData(CombPayNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putCombPayNode, Constants.sStdTechKey, value, sCombPayNodeOrder);
            }
        }
        public string CombPayEntityId 
        {
            get
            {
                return CombPayTechKey;
            }
            set
            {
                Utils.putData(putCombPayNode, Constants.sStdTechKey, value, sCombPayNodeOrder);
            }
        }

        public string CombPayCode
        {
            get
            {
                return Utils.getData(CombPayNode, Constants.sVCHCombPayCode);
            }
            set
            {
                Utils.putData(putCombPayNode, Constants.sVCHCombPayCode, value, sCombPayNodeOrder);
            }
        }

        public string CombPayCode_Code
        {
            get
            {
                return Utils.getCode(CombPayNode, Constants.sVCHCombPayCode);
            }
            set
            {
                Utils.putCode(putCombPayNode, Constants.sVCHCombPayCode, value, sCombPayNodeOrder);
            }
        }

        public string CombPayCode_CodeID
        {
            get
            {
                return Utils.getCodeID(CombPayNode, Constants.sVCHCombPayCode);
            }
            set
            {
                Utils.putCodeID(putCombPayNode, Constants.sVCHCombPayCode, value, sCombPayNodeOrder);
            }
        }
// End SIW489

        private void subClearPointers()
        {
            m_xmlTargetBank = null;
            m_xmlOriginBank = null;
            m_xmlCombPay = null; // SIW489

            PartyInvolved = null;
            AddressReference = null;
            CommentReference = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLVouchers Parent
        {
            get
            {//Start SIW529
                if (m_Parent == null)
                {
                    m_Parent = new XMLVouchers();
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ((XMLVouchers)m_Parent).Voucher = this;                    
                    ResetParent();
                }
                return (XMLVouchers)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }//End SIW529
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public string PayeePhrase
        {
            get
            {
                return PartyInvolved.DataItem.DataItemValue(Constants.sVCHPayeePhrase);
            }
            set
            {
                PartyInvolved.DataItem.addDataItem(Constants.sVCHPayeePhrase, value, "", cxmlDataType.dtString);
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();       //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                {
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_AddressReference.getAddressReference(Constants.xcGetFirst);   //SI06420
                }
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public void removeVoucher()
        {
            if (xmlThisNode != null)
                Node.RemoveChild(xmlThisNode);
            xmlThisNode = null;
        }

        public string stripID(string sID)
        {
            if (StringUtils.Left(sID, 3) == Constants.sVCHIDPfx && sID.Length >= 4)
                return StringUtils.Right(sID, sID.Length - 3);
            else
                return sID;
        }

        public string formatID(string sID)
        {
            if (StringUtils.Left(sID, 3) != Constants.sVCHIDPfx)
                return Constants.sVCHIDPfx + sID;
            else
                return sID;
        }

        public XmlNode addOriginatingBank(string sEntityID, string sBankNumber, string sRouteNumber, string sAccountNumber,
                                          string sFractionalNumber)
        {
            OriginatingBankEntityID = sEntityID;
            OriginatingBankNumber = sBankNumber;
            OriginatingAccountNumber = sAccountNumber;
            OriginatingRoutingNumber = sRouteNumber;
            OriginatingFractionalNumber = sFractionalNumber;
            return m_xmlOriginBank;
        }

        public XmlNode putOriginatingBankNode
        {
            get
            {
                if (OriginatingBankNode == null)
                    m_xmlOriginBank = XML.XMLaddNode(Node, Constants.sVCHOriginatingBank, NodeOrder);
                return m_xmlOriginBank;
            }
        }

        public XmlNode OriginatingBankNode
        {
            get
            {
                if (m_xmlOriginBank == null)
                    m_xmlOriginBank = XML.XMLGetNode(Node, Constants.sVCHOriginatingBank);
                return m_xmlOriginBank;
            }
            set
            {
                m_xmlOriginBank = value;
            }
        }

        public XmlNode OriginatingBankEntity
        {
            get
            {
                XMLEntities xml_entities = new XMLEntities();
                XmlNode xml_entity = xml_entities.getEntitybyID(Document, OriginatingBankEntityID);
                return xml_entity;
            }
        }

        public string OriginatingBankEntityID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sVCHOriginatingBank);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sVCHOriginatingBank, value, NodeOrder);
            }
        }

        public string OriginatingBankNumber
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sVCHOriginatingBank, Constants.sVCHBankNumber);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sVCHOriginatingBank, Constants.sVCHBankNumber, value, NodeOrder);
            }
        }
        //Start SIW7928
        public string OriginatingAccountName
        {
            get
            {
                return Utils.getData(OriginatingBankNode, Constants.sVCHAccountName);
            }
            set
            {
                Utils.putData(putOriginatingBankNode, Constants.sVCHAccountName, value, sBNKNodeOrder);
            }
        }
        //End SIW7928
        public string OriginatingRoutingNumber
        {
            get
            {
                return Utils.getData(OriginatingBankNode, Constants.sVCHRoutingNumber);
            }
            set
            {
                Utils.putData(putOriginatingBankNode, Constants.sVCHRoutingNumber, value, sBNKNodeOrder);
            }
        }

        public string OriginatingFractionalNumber
        {
            get
            {
                return Utils.getData(OriginatingBankNode, Constants.sVCHFractionalNumber);
            }
            set
            {
                Utils.putData(putOriginatingBankNode, Constants.sVCHFractionalNumber, value, sBNKNodeOrder);
            }
        }

        public string OriginatingAccountNumber
        {
            get
            {
                return Utils.getData(OriginatingBankNode, Constants.sVCHAccountNumber);
            }
            set
            {
                Utils.putData(putOriginatingBankNode, Constants.sVCHAccountNumber, value, sBNKNodeOrder);
            }
        }
        // Start SIW451
        public string OriginatingBankTechKey
        {
            get
            {
                return Utils.getData(OriginatingBankNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putOriginatingBankNode, Constants.sStdTechKey, value, sBNKNodeOrder);
            }
        }
        // End SIW451
        public XmlNode addTargetBank(string sEntityID, string sBankNumber, string sRouteNumber, string sAccountNumber,
                                     string sFractionalNumber)
        {
            TargetBankNumber = sBankNumber;
            TargetAccountNumber = sAccountNumber;
            TargetRoutingNumber = sRouteNumber;
            TargetFractionalNumber = sFractionalNumber;
            return m_xmlTargetBank;
        }

        public XmlNode putTargetBankNode
        {
            get
            {
                if (TargetBankNode == null)
                    m_xmlTargetBank = XML.XMLaddNode(Node, Constants.sVCHTargetBank, NodeOrder);
                return m_xmlTargetBank;
            }
        }

        public XmlNode TargetBankNode
        {
            get
            {
                if (m_xmlTargetBank == null)
                    m_xmlTargetBank = XML.XMLGetNode(Node, Constants.sVCHTargetBank);
                return m_xmlTargetBank;
            }
            set
            {
                m_xmlTargetBank = value;
            }
        }

        public XmlNode TargerBankEntity
        {
            get
            {
                XMLEntities xml_entities = new XMLEntities();
                XmlNode xml_entity = xml_entities.getEntitybyID(Document, TargetBankEntityID);
                return xml_entity;
            }
        }

        public string TargetBankEntityID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sVCHTargetBank);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sVCHTargetBank, value, NodeOrder);
            }
        }

        public string TargetBankNumber
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sVCHTargetBank, Constants.sVCHBankNumber);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sVCHTargetBank, Constants.sVCHBankNumber, value, NodeOrder);
            }
        }
        //Start SIW7928
        public string TargetAccountName
        {
            get
            {
                return Utils.getData(TargetBankNode, Constants.sVCHAccountName);
            }
            set
            {
                Utils.putData(putTargetBankNode, Constants.sVCHAccountName, value, sBNKNodeOrder);
            }
        }
        //End SIW7928
        public string TargetRoutingNumber
        {
            get
            {
                return Utils.getData(TargetBankNode, Constants.sVCHRoutingNumber);
            }
            set
            {
                Utils.putData(putTargetBankNode, Constants.sVCHRoutingNumber, value, sBNKNodeOrder);
            }
        }

        public string TargetFractionalNumber
        {
            get
            {
                return Utils.getData(TargetBankNode, Constants.sVCHFractionalNumber);
            }
            set
            {
                Utils.putData(putTargetBankNode, Constants.sVCHFractionalNumber, value, sBNKNodeOrder);
            }
        }

        public string TargetAccountNumber
        {
            get
            {
                return Utils.getData(TargetBankNode, Constants.sVCHAccountNumber);
            }
            set
            {
                Utils.putData(putTargetBankNode, Constants.sVCHAccountNumber, value, sBNKNodeOrder);
            }
        }
        // Start SIW451
        public string TargetBankTechKey
        {
            get
            {
                return Utils.getData(TargetBankNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putTargetBankNode, Constants.sStdTechKey, value, sBNKNodeOrder);
            }
        }
        // End SIW451

        // SI06650 public XmlNode addPayee(string sEID)
        public XmlNode addPayee(string sPIId, string sEID) // SI06650
        {
            // SI06650 return PartyInvolved.putPartyInvolved(sEID, "", PayeeCode);
            return PartyInvolved.putPartyInvolved(sPIId, sEID, "", PayeeCode); // SI06650
        }

        public XmlNode getPayee(bool reset)
        {
            XmlNode xmlNode = PartyInvolved.getPartyInvolved(reset);
            while (xmlNode != null && PartyInvolved.RoleCode != PayeeCode)
                xmlNode = PartyInvolved.getPartyInvolved(false);
            return xmlNode;
        }

        // SI06650 public XmlNode addMailToEntity(string sEID)
        public XmlNode addMailToEntity(string sPIId, string sEID) // SI06650
        {
            // SI06650 return PartyInvolved.putPartyInvolved(sEID, "", MailToCode);
            return PartyInvolved.putPartyInvolved(sPIId, sEID, "", MailToCode); // SI06650
        }

        public XmlNode getMailToEntity(bool reset)
        {
            XmlNode xmlNode = PartyInvolved.getPartyInvolved(reset);
            while (xmlNode != null && PartyInvolved.RoleCode != MailToCode)
                xmlNode = PartyInvolved.getPartyInvolved(false);
            return xmlNode;
        }

        public XmlNode addMailTo(string sAID)
        {
            return AddressReference.putAddressReference(sAID, "", MailToCode,"","");
        }

        public XmlNode getMailTo(bool reset)
        {
            XmlNode xmlNode = AddressReference.getAddressReference(reset);
            while (xmlNode != null && AddressReference.RoleCode != MailToCode)
                xmlNode = AddressReference.getAddressReference(false);
            return xmlNode;
        }

        public XmlNode getVoucher(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getVoucherByAccount(string sAccountNumber, string sDocumentNumber)
        {
            if (OriginatingAccountNumber != sAccountNumber || DocumentNumber != sDocumentNumber)
            {
                getFirst();
                while (Node != null)
                {
                    if (OriginatingAccountNumber == sAccountNumber && DocumentNumber == sDocumentNumber)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getVoucherByBank(string sBankNumber, string sDocumentNumber)
        {
            if (OriginatingBankNumber != sBankNumber || DocumentNumber != sDocumentNumber)
            {
                getFirst();
                while (Node != null)
                {
                    if (OriginatingBankNumber == sBankNumber && DocumentNumber == sDocumentNumber)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getVoucherByControl(string sControlNumber)
        {
            if (ControlNumber != sControlNumber)
            {
                getFirst();
                while (Node != null)
                {
                    if (ControlNumber == sControlNumber)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getVoucherbyID(string sID)
        {
            if (VoucherID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (VoucherID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public int InvoiceCount
        {
            get
            {
                return InvoiceList.Count;
            }
        }

        public XmlNodeList InvoiceList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlInvoiceList)
                {
                    return getNodeList(Constants.sVCHInvoice, ref m_xmlInvoiceList, Node);
                }
                else
                {
                    return m_xmlInvoiceList;
                }
                //End SIW163
            }
        }

        public XmlNode getInvoice(bool reset)
        {
            //Start SIW163
            if (null != InvoiceList)
            {
                return getNode(reset, ref m_xmlInvoiceList, ref m_xmlInvoice);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeInvoice()
        {
            if (InvoiceNode != null)
                Node.RemoveChild(InvoiceNode);
            InvoiceNode = null;
        }

        private XmlNode putInvoiceNode
        {
            get
            {
                if (InvoiceNode == null)
                    InvoiceNode = XML.XMLaddNode(putNode, Constants.sVCHInvoice, NodeOrder);
                return InvoiceNode;
            }
        }

        public XmlNode InvoiceNode
        {
            get
            {
                return m_xmlInvoice;
            }
            set
            {
                m_xmlInvoice = value;
            }
        }

        public XmlNode addInvoice(string snumber, string sDate, string sAmount, string sPONumber, string sSvcFromDate,
                                  string sSvcToDate)
        {
            InvoiceNode = null;
            InvoiceNumber = snumber;
            InvoiceDate = sDate;
            InvoiceAmount = sAmount;
            PONumber = sPONumber;
            ServiceFromDate = sSvcFromDate;
            ServiceToDate = sSvcToDate;
            return InvoiceNode;
        }

        public string InvoiceNumber
        {
            get
            {
                return Utils.getData(InvoiceNode, Constants.sVCHInvNbr);
            }
            set
            {
                Utils.putData(putInvoiceNode, Constants.sVCHInvNbr, value, sInvNodeOrder);
            }
        }

        public string InvoiceDate
        {
            get
            {
                return Utils.getDate(InvoiceNode, Constants.sVCHInvDate);
            }
            set
            {
                Utils.putDate(putInvoiceNode, Constants.sVCHInvDate, value, sInvNodeOrder);
            }
        }

        public string InvoicedBy
        {
            get
            {
                return Utils.getData(InvoiceNode, Constants.sVCHInvBy);
            }
            set
            {
                Utils.putData(putInvoiceNode, Constants.sVCHInvBy, value, sInvNodeOrder);
            }
        }

        public string PONumber
        {
            get
            {
                return Utils.getData(InvoiceNode, Constants.sVCHPONbr);
            }
            set
            {
                Utils.putData(putInvoiceNode, Constants.sVCHPONbr, value, sInvNodeOrder);
            }
        }

        public string ServiceFromDate
        {
            get
            {
                return Utils.getDate(InvoiceNode, Constants.sVCHSvcFromDate);
            }
            set
            {
                Utils.putDate(putInvoiceNode, Constants.sVCHSvcFromDate, value, sInvNodeOrder);
            }
        }

        public string ServiceToDate
        {
            get
            {
                return Utils.getDate(InvoiceNode, Constants.sVCHSvcToDate);
            }
            set
            {
                Utils.putDate(putInvoiceNode, Constants.sVCHSvcToDate, value, sInvNodeOrder);
            }
        }

        public string InvoiceAmount
        {
            get
            {
                return Utils.getData(InvoiceNode, Constants.sVCHInvAmount);
            }
            set
            {
                Utils.putData(putInvoiceNode, Constants.sVCHInvAmount, value, sInvNodeOrder);
            }
        }

        public XmlNode getInvoiceByNumber(string snumber)
        {
            if (InvoiceNumber != snumber)
            {
                getInvoice(true);
                while (InvoiceNode != null)
                {
                    if (InvoiceNumber == snumber)
                        break;
                    getInvoice(false);
                }
            }
            return InvoiceNode;
        }

        public XmlNode getInvoiceByPO(string snumber)
        {
            if (PONumber != snumber)
            {
                getInvoice(true);
                while (InvoiceNode != null)
                {
                    if (PONumber == snumber)
                        break;
                    getInvoice(false);
                }
            }
            return InvoiceNode;
        }

        public XmlNode getInvoiceByDate(string sDate)
        {
            if (InvoiceDate != sDate)
            {
                getInvoice(true);
                while (InvoiceNode != null)
                {
                    if (InvoiceDate == sDate)
                        break;
                    getInvoice(false);
                }
            }
            return InvoiceNode;
        }
        //Start SIW7928 - Properties are defined for displaying the value for Distribution Type code and code id in XML
        public XmlNode putDistributionType(string sDesc, string sCode)
        {
            return putDistributionType(sDesc, sCode, "");
        }

        public XmlNode putDistributionType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDistributionType, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string DistributionType
        {
            get
            {
                return Utils.getData(Node, Constants.sDistributionType);    
            }
            set
            {
                Utils.putData(putNode, Constants.sDistributionType, value, NodeOrder);   
            }
        }

        public string DistributionType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDistributionType);   
            }
            set
            {
                Utils.putCode(putNode, Constants.sDistributionType, value, NodeOrder);   
            }
        }

        public string DistributionType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDistributionType);   
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDistributionType, value, NodeOrder);    
            }
        }
        //End SIW7928
    }

    public class XMLVouchers : XMLXCNode
    {
        /*****************************************************************************
        '<VOUCHERS NEXT_ID="0" COUNT=xx">
        '  <VOUCHER></VOUCHER>
        '</VOUCHERS>
        '*****************************************************************************/

        private XMLVoucher m_Voucher;

        public XMLVouchers()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_Voucher = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sVCHVoucher);

            Utils.subSetupDocumentNodeOrder();

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public override XmlDocument Document
        {
            get
            {
                return Parent.Document;
            }
            protected set
            {
                Parent.Document = value;
            }
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sVCHGroup; }
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextVchID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextVchID = sNextVchID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLVouchers.Create");
                return null;
            }
        }

        public string getNextVchID(XmlNode xnode)
        {
            if (xnode != null)
                Node = xnode;
            string s = NextVchID;
            NextVchID = (Int32.Parse(NextVchID) + 1) + "";
            return s;
        }

        public string NextVchID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sVCHNextID);
                if (strdata == "")
                    strdata = "1";
                return strdata;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = "1";
                Utils.putAttribute(putNode, "", Constants.sVCHNextID, value, NodeOrder);
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sVCHCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sVCHCount, value + "", NodeOrder);
            }
        }

        public override XmlNode Node
        {
            get//Start SIW529
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLGetNode(Parent.Node, NodeName);
                return xmlThisNode;
            }//End SIW529
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XmlNode Addvoucher()
        {
            return Voucher.Create();
        }

        private void subClearPointers()
        {
            Voucher = null;
        }

        public XmlNode getVoucher(XmlDocument Doc, bool reset)
        {
            if (Doc != null)
                Document = Doc;
            return Voucher.getVoucher(reset);
        }

        public XmlNode getVoucherByAccount(XmlDocument Doc, string sAccountNumber, string sDocumentNumber)
        {
            if (Doc != null)
                Document = Doc;
            return Voucher.getVoucherByAccount(sAccountNumber, sDocumentNumber);
        }

        public XmlNode getVoucherByControl(XmlDocument Doc, string sControlNumber)
        {
            if (Doc != null)
                Document = Doc;
            return Voucher.getVoucherByControl(sControlNumber);
        }

        public XmlNode getVoucherbyID(XmlDocument Doc, string sVchID)
        {
            if (Doc != null)
                Document = Doc;
            return Voucher.getVoucherbyID(sVchID);
        }

        public XMLVoucher Voucher
        {
            get
            {
                if (m_Voucher == null)
                {
                    m_Voucher = new XMLVoucher();
                    m_Voucher.Parent = this;
                    //SIW529 LinkXMLObjects(m_Voucher);
                    m_Voucher.getVoucher(Constants.xcGetFirst);     //SI06420
                }
                return m_Voucher;
            }
            set
            {
                m_Voucher = value;
            }
        }
    }
}
