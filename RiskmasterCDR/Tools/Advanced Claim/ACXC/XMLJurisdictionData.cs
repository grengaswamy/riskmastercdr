/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLJurisdictionData : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*<JURISDICTIONAL_DATA>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) ' SIW360
        '   <JURISDICTION CODE="MA">Massachusetts</STATE>
        '   <DATA_ITEM NAME="INDUSTRY_CODE" TYPE="CODE" CODE="xx">Description</DATA_ITEM>
        '        <!-- Multiple Data Items Allowed -->
        '   <PARTY_INVOLVED ENTITY_ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '        <!--Multiple Parties Involved-->
        '   <COMMENT_REFERENCE COMMENT_ID="CMTxx"/>
        '        <!-- multiple entries -->        
        '</JURISDICTIONAL_DATA>*/

        XmlNode xmlDataItem;
        XMLPartyInvolved m_PartyInvolved;
        XMLCommentReference m_Comment;
        XMLClaim m_Claim;
        
        public XMLJurisdictionData()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;

            //sThisNodeOrder = new ArrayList(4);                //(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(5);                  //(SI06023 - Implemented in SI06333)
            sThisNodeOrder.Add(Constants.sStdTechKey);      //SIW360
            sThisNodeOrder.Add(Constants.sJurJurisdiction);
            sThisNodeOrder.Add(Constants.sJurDataItem);
            sThisNodeOrder.Add(Constants.sJurPartyInvolved);
            sThisNodeOrder.Add(Constants.sJurCommentReference);
            //sThisNodeOrder.Add(Constants.sJurTechKey);          //(SI06023 - Implemented in SI06333)//SIW360
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sJurNode; }
        }
        
        public override XmlNode Create()
        {
            return Create("", "", "", "", "", cxmlDataType.dtString);
        }

        public XmlNode Create(string sJurisdiction, string sJurisdictionCode, string sDataName, string sData,
                              string sCode, cxmlDataType cDataType)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                putJurisdiction(sJurisdiction, sJurisdictionCode);
                putDataItem(sDataName, sData, sCode, cDataType);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLJurisdiction.Create");
                return null;
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.JurisdictionalData = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null) //SIW529
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            xmlDataItem = null;
            xmlNodeList = null;

            PartyInvolved = null;
            CommentReference = null;
        }

        //Start SIW139
        public XmlNode putJurisdiction(string sDesc, string sCode)
        {
            return putJurisdiction(sDesc, sCode, "");
        }

        public XmlNode putJurisdiction(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sJurJurisdiction, sDesc, sCode, NodeOrder,scodeid);
        }

        public string Jurisdiction
        {
            get
            {
                return Utils.getData(Node, Constants.sJurJurisdiction);
            }
            set
            {
                Utils.putData(putNode, Constants.sJurJurisdiction, value, NodeOrder);
            }
        }

        public string Jurisdiction_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sJurJurisdiction);
            }
            set
            {
                Utils.putCode(putNode, Constants.sJurJurisdiction, value, NodeOrder);
            }
        }

        public string Jurisdiction_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sJurJurisdiction);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sJurJurisdiction, value, NodeOrder);
            }
        }
        //End SIW139

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public int DataItemCount
        {
            get
            {
                return DataItemList.Count;
            }
        }

        public XmlNodeList DataItemList
        {
            get
            {
                //Start SIW163
                if (null == xmlNodeList)
                {
                    return getNodeList(Constants.sJurDataItem, ref xmlNodeList, Node);
                }
                else
                {
                    return xmlNodeList;
                }
                //End SIW163
            }
        }

        private XmlNode DataItemNode
        {
            get
            {
                if (xmlDataItem == null)
                    xmlDataItem = XML.XMLaddNode(putNode, Constants.sJurDataItem, NodeOrder);
                return xmlDataItem;
            }
        }

        public XmlNode getDataItem(bool reset)
        {
            object o = DataItemList;
            return getNode(reset, ref xmlNodeList, ref xmlDataItem);
        }

        public void removeDataItem()
        {
            if (xmlDataItem != null)
                Node.RemoveChild(xmlDataItem);
            xmlDataItem = null;
        }

        public XmlNode addDataItem(string sName, string sData, string sCode, cxmlDataType cDataType)
        {
            xmlDataItem = null;
            return putDataItem(sName, sData, sCode, cDataType);
        }

        public XmlNode putDataItem(string sName, string sData, string sCode, cxmlDataType cDataType)
        {
            DataItemName = sName;
            XML.XMLInsertVariable_Node(DataItemNode, cDataType, sData, sCode);
            return xmlDataItem;
        }

        public string DataItemName
        {
            get
            {
                string strdata = "";
                if (xmlDataItem != null)
                    strdata = XML.XMLGetAttributeValue(xmlDataItem, Constants.sJurDataName);
                return strdata;
            }
            set
            {
                if (value != "" && value != null)
                    XML.XMLSetAttributeValue(DataItemNode, Constants.sJurDataName, value);
            }
        }

        public cxmlDataType DataItemType
        {
            get
            {
                string stype = XML.XMLGetAttributeValue(xmlDataItem, Constants.sJurDataType);
                return XML.getType(stype);
            }
            set
            {
                string stype = XML.XmlDataType(value);
                DataItemType_Code = stype;
            }
        }

        public string DataItemType_Code
        {
            get
            {
                return XML.XMLGetAttributeValue(xmlDataItem, Constants.sJurDataType);
            }
            set
            {
                XML.XMLSetAttributeValue(DataItemNode, Constants.sJurDataType, value);
            }
        }

        public cxmlDataType DataItemValues(out string sValue, out string sCode)
        {
            return XML.XMLExtractVariableDataType(xmlDataItem, out sValue, out sCode);
        }
    }
}
