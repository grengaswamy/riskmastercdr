/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/15/2008 | SI06420 |    CB      | Get first child
 * 10/30/2008 | SI06420 |    ASM     | Corrected data types
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 01/18/2010 | SI06650 |    NDB     | Party Involved / Medicare / Add'l Data enhancements
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 12/11/2011 | SIW7882 |    RG      |  Group Association for Supplementals
 * 04/10/2012 | SIW8366 |    JTC     |  Blank dates in Supplemental fields are not getting saved.
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLAdditionalData : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<ADDITIONAL_DATA>
        '   <!-These are supplemental fields defined within a group associated with a particular data element -- >
        '   <!-Additional data groups and items may be defined.  Appropriate fields must be defined within the ClaimsPro Database. -- >
        '   <DATA_GROUP
        '         COMPONENT_ID="[yyyy] document unique ID that corresponds to a data component within the body of this document"
        '   Important:     When referencing the UNIT_STAT_DCI group, always use the unique Id of the associated with
        '         COMPONENT_NAME="Corresponds to a group data tag name in this document"
        '         SUPP_TABLE="Corresponds to a specific supplemental table in ClaimsPro">
        '      <DATA_ITEM>
        '         NAME="Data Item Name"
        '         SUPP_FIELD="Corresponds to a supplemental table field within the table above"
        '         TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '            [data attributes corresponding to data types above as follows:
        '            BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '            DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '            TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '            CODE:  CODE="shortcode"]>
        '            [Data corresponding to the data type as follows:
        '            STRING:  data value {string}
        '            NUMBER:  data value {decimal}
        '            Date:  formatted Date
        '            TIME:  formatted time
        '       CODE_TABLE_NAME="Name of code table"
        '       LOCKED="YES, if field is locked"
        '       DISABLED="YES, if field is disabled"
        '       REQUIRED="YES, if field is Required"
        '       LOOKUP="YES, if field is a look up"
        '       PATTERN="mask"
        '       FIELD_SIZE="integer, length of field size"
        '       FIELD_TYPE="integer, matches with supplemental enumeration"
        '      </DATA_ITEM>
        '      <PARTY_INVOLVED ENTITY_ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '          <!--Multiple Parties Involved-->
        '      <ADDRESS_REFERENCE ADDRESS_ID="ADRxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</ADDRESS_REFERENCE>
        '          <!--Multiple Parties Involved-->
        '      <COMMENT_REFERENCE COMMENT_ID="CMTxx"/>
        '          <!-- multiple entries -->
        '   </DATA_GROUP>
        '</ADDITIONAL_DATA>*/

        private XmlNode xmlAddGroup;
        private XmlNodeList xmlAddGroupList;
        private XmlNode xmlDataItem;
        private XmlNodeList xmlDataItemList;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLAddressReference m_AddressReference;

        public XMLAdditionalData()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlAddGroup = null;
            xmlAddGroupList = null;
            xmlDataItem = null;
            xmlDataItemList = null;

            sThisNodeOrder = new ArrayList(4);
            sThisNodeOrder.Add(Constants.sAddDataItem);
            sThisNodeOrder.Add(Constants.sAddPartyInvolved);
            sThisNodeOrder.Add(Constants.sAddAddressReference);
            sThisNodeOrder.Add(Constants.sAddCommentReference);
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sAddNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", cxmlDataType.dtString);
        }

        public XmlNode Create(string sComponentID, string sComponentName, string sSuppTable, string sDataName,
                              string sSuppField, string sData, string scode, cxmlDataType cDataType)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                if ((sComponentID != "" && sComponentID != null) ||
                    (sComponentName != "" && sComponentName != null) ||
                    (sSuppTable != "" && sSuppTable != null))
                {
                    ComponentID = sComponentID;
                    ComponentName = sComponentName;
                    SuppTable = sSuppTable;
                }

                if ((sDataName != "" && sDataName != null) ||
                    (sSuppField != "" && sSuppField != null))
                    putDataItem(sDataName, sSuppField, sData, scode, cDataType);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLAddData.Create");
                return null;
            }
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            xmlAddGroup = null;
            xmlAddGroupList = null;
            xmlDataItem = null;
            xmlDataItemList = null;

            PartyInvolved = null;
            CommentReference = null;
            AddressReference = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, GroupNode, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = null;  //SI06420
                m_PartyInvolved = value;
            }
        }

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                {
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, GroupNode, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_AddressReference.getAddressReference(Constants.xcGetFirst);   //SI06420
                }
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, GroupNode, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Node = Parent_Node.SelectSingleNode(NodeName);
                if (Node == null)
                    Create();
                return Node;
            }
        }

        public override XmlNode Node
        {
            get
            {
                xmlThisNode = Parent.Node.SelectSingleNode(NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                xmlAddGroupList = null;
                GroupNode = null;
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                //SIW485 return Globals.sNodeOrder;
                return Utils.sNodeOrder;	//SIW485
            }
        }

        public int GroupCount
        {
            get
            {
                return GroupList.Count;
            }
        }

        public XmlNodeList GroupList
        {
            get
            {
                //Start SIW163
                if (null == xmlAddGroupList)
                {
                    return getNodeList(Constants.sAddGroup, ref xmlAddGroupList, Node);
                }
                else
                {
                    return xmlAddGroupList;
                }
                //End SIW163
            }
        }

        private XmlNode putGroupNode
        {
            get
            {
                if (GroupNode == null)
                    GroupNode = XML.XMLaddNode(putNode, Constants.sAddGroup, NodeOrder);
                return GroupNode;
            }
        }

        public XmlNode GroupNode
        {
            get
            {
                return xmlAddGroup;
            }
            set
            {
                xmlAddGroup = value;
                xmlDataItemList = null;
                DataItemNode = null;
                PartyInvolved = null;
                CommentReference = null;
                AddressReference = null;
            }
        }

        public XmlNode getGroup(bool reset)
        {
            object o = GroupList;
            XmlNode xmlNode = getNode(reset, ref xmlAddGroupList, ref xmlAddGroup);
            xmlDataItemList = null;
            DataItemNode = null;
            PartyInvolved = null;
            CommentReference = null;
            AddressReference = null;
            return xmlNode;
        }

        public void RemoveGroup()
        {
            if (GroupNode != null)
                Node.RemoveChild(GroupNode);
            GroupNode = null;
        }

        public XmlNode addGroup(string sComponentName, string sComponentID, string sSuppTable)
        {
            GroupNode = null;
            return putGroup(sComponentName, sComponentID, sSuppTable);
        }

        public XmlNode putGroup(string sComponentName, string sComponentID, string sSuppTable)
        {
            // Start SI06650
            if (!string.IsNullOrEmpty(sComponentName) && !string.IsNullOrEmpty(sComponentID))
            {
                findDataGroup(sComponentName, sComponentID);
            } 
            // End SI06650
            
            ComponentName = sComponentName;
            ComponentID = sComponentID;
            SuppTable = sSuppTable;            
            return GroupNode;
        }

        public string ComponentID
        {
            get
            {
                return Utils.getAttribute(GroupNode, "", Constants.sAddComponentID);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putGroupNode, "", Constants.sAddComponentID, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ComponentName
        {
            get
            {
                return Utils.getAttribute(GroupNode, "", Constants.sAddComponentName);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putGroupNode, "", Constants.sAddComponentName, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string SuppTable
        {
            get
            {
                return Utils.getAttribute(GroupNode, "", Constants.sAddSuppTable);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putGroupNode, "", Constants.sAddSuppTable, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public int DataItemCount
        {
            get
            {
                return DataItemList.Count;
            }
        }

        public XmlNodeList DataItemList
        {
            get
            {
                //Start SIW163
                if (null == xmlDataItemList)
                {
                    return getNodeList(Constants.sAddDataItem, ref xmlDataItemList, GroupNode);
                }
                else
                {
                    return xmlDataItemList;
                }
                //End SIW163
            }
        }

        private XmlNode putDataItemNode
        {
            get
            {
                if (DataItemNode == null)
                    DataItemNode = XML.XMLaddNode(putGroupNode, Constants.sAddDataItem, sThisNodeOrder);
                return DataItemNode;
            }
        }

        private XmlNode DataItemNode
        {
            get
            {
                return xmlDataItem;
            }
            set
            {
                xmlDataItem = value;
            }
        }

        public XmlNode getDataItem(bool reset)
        {
            object o = DataItemList;
            return getNode(reset, ref xmlDataItemList, ref xmlDataItem);
        }

        public void removeDataItem()
        {
            if (DataItemNode != null)
                GroupNode.RemoveChild(DataItemNode);
            DataItemNode = null;
        }

        public XmlNode addDataItem(string sName, string sSuppField, string sData, string scode, cxmlDataType cDataType)
        {
            DataItemNode = null;
            return putDataItem(sName, sSuppField, sData, scode, cDataType);
        }

        public XmlNode putDataItem(string sName, string sSuppField, string sData, string scode, cxmlDataType cDataType)
        {
            // Start SI06650
            if (!string.IsNullOrEmpty(sSuppField))
            {
                findDataItembyField(sSuppField, "", "");
            } 
            else if (!string.IsNullOrEmpty(sName))
            {
                findDataItem(sName, "", "");
            } 
            // End SI06605

            DataItemName = sName;
            DataItemSuppField = sSuppField;
            ValueDataItem(sData, scode, cDataType);
            return DataItemNode;
        }

        public void ValueDataItem(string sData, string scode, cxmlDataType cDataType)
        {
            //Start SIW8366
            if (XML.Blanking)
            {
                if ((sData == "" || sData == null) && (scode == "" || scode == null))
                {
                    sData = Constants.BLANK;
                    scode = Constants.BLANK;
                }
            }
            //End SIW8366
            XML.XMLInsertVariable_Node(putDataItemNode, cDataType, sData, scode);
        }

        public string DataItemName
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddDataName);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddDataName, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DataItemSuppField
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddSuppField);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddSuppField, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }




        
        public string SuppCodeTable
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", "SUPP_CODE_TABLE");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", "SUPP_CODE_TABLE", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DataItemLookUp
        {
            get
            {
                return Utils.getBool(DataItemNode, "", Constants.sAddLookUp);
            }
            set
            {
                Utils.putBool(putDataItemNode, "", Constants.sAddLookUp, value, NodeOrder);
            }
        }

        public string DataItemRequired
        {
            get
            {
                return Utils.getBool(DataItemNode, "", Constants.sAddRequired);
            }
            set
            {
                Utils.putBool(putDataItemNode, "", Constants.sAddRequired, value, NodeOrder);
            }
        }

        public string DataItemDisabled
        {
            get
            {
                return Utils.getBool(DataItemNode, "", Constants.sAddDisabled);
            }
            set
            {
                Utils.putBool(putDataItemNode, "", Constants.sAddDisabled, value, NodeOrder);
            }
        }

        public string DataItemLocked
        {
            get
            {
                return Utils.getBool(DataItemNode, "", Constants.sAddLocked);
            }
            set
            {
                Utils.putBool(putDataItemNode, "", Constants.sAddLocked, value, NodeOrder);
            }
        }

        public string DataItemFieldSize
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddFieldSize);  
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddFieldSize, value, NodeOrder);   
            }
        }

        public string DataItemFieldType
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddFieldType);   
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddFieldType, value, NodeOrder);  
            }
        }

        public string DataItemCodeTable
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddCodeFileId);
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddCodeFileId, value, NodeOrder);
            }
        }

        public string DataItemPattern
        {
            get
            {
                return Utils.getAttribute(DataItemNode, "", Constants.sAddPattern);
            }
            set
            {
                Utils.putAttribute(putDataItemNode, "", Constants.sAddPattern, value, NodeOrder);
            }
        }

        public bool IsPattern
        {
            get
            {
                if (DataItemPattern.ToUpper().Equals("YES") || DataItemPattern.ToUpper().Equals("TRUE"))
                    return true;
                else
                    return false;
            }            
        }
        //SIW7882 - End

        public cxmlDataType DataItemValues(out string sValue, out string scode)
        {
            //Start SIW8366
            //return XML.XMLExtractVariableDataType(DataItemNode, out sValue, out scode);         
            cxmlDataType retval = XML.XMLExtractVariableDataType(DataItemNode, out sValue, out scode);
            XMLUtils.TranslateString(ref sValue);
            XMLUtils.TranslateString(ref scode);
            return retval;
            //End SIW8366
        }

        public XmlNode findDataGroup(string sCompName, string sCompID)
        {
            bool rst = true;
            if (ComponentName != sCompName || ComponentID != sCompID)
            {
                while (getGroup(rst) != null)
                {
                    rst = false;
                    if (ComponentName == sCompName && ComponentID == sCompID)
                        break;
                }
            }
            return xmlAddGroup;
        }

        public XmlNode findDataGroupSpecified(string sCompID, string sCompName, string sSuppTable)
        {
            bool rst = true;
            bool fnd = true;
            while (getGroup(rst) != null)
            {
                rst = false;
                
                if ((sCompID != "" && sCompID != null && sCompID != ComponentID) ||
                    (sCompName != "" && sCompName != null && sCompName != ComponentName) ||
                    (sSuppTable != "" && sSuppTable != null && sSuppTable != SuppTable))
                    fnd = false;
                else
                    fnd = true;

                if (fnd)
                    break;
            }
            return GroupNode;
        }

        public XmlNode findDataGroupbyID(string sDGID)
        {
            bool rst = true;
            if (ComponentID != sDGID)
            {
                while (getGroup(rst) != null)
                {
                    rst = false;
                    if (ComponentID == sDGID)
                        break;
                }
            }
            return GroupNode;
        }

        public XmlNode findDataItem(string sItemName, string sCompName, string sCompID)
        {
            bool rst = true;
            if ((sCompName != "" && sCompName != null) || (sCompID != "" && sCompID != null))
                findDataGroupSpecified(sCompID, sCompName, "");
            if (GroupNode != null)
            {
                while (getDataItem(rst) != null)
                {
                    rst = false;
                    if (DataItemName == sItemName)
                        break;
                }
            }
            return DataItemNode;
        }

        public XmlNode findDataItembyField(string sField, string sCompName, string sCompID)
        {
            bool rst = true;
            if ((sCompName != "" && sCompName != null) || (sCompID != "" && sCompID != null))
                findDataGroupSpecified(sCompID, sCompName, ""); // SI06650 NDB
            if (GroupNode != null) // SI06650 NDB
            {
                while (getDataItem(rst) != null)
                {
                    rst = false;
                    if (DataItemSuppField == sField)
                        break;
                }
            }
            return DataItemNode;
        }
    }
}
