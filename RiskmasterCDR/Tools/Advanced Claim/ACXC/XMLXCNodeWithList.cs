﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public abstract class XMLXCNodeWithList:XMLXCNodeWithListBase
    {
        public override XMLUtils Utils
        {
            get
            {
                if (m_xmlUtils == null)
                    m_xmlUtils = new Utils();
                return m_xmlUtils;
            }
        }

        public override XMLUtils XMLUtils
        {
            get
            {
                return Utils;
            }
        }
    }
}
