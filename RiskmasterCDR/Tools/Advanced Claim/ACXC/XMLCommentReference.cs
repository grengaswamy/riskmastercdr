/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 05/17/2010 | SIW360  |   AP       | Techkey position reodered in .NET XML components similar to ordering in CCPXC components.
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 05/21/2010 | SIW321  |    SW      | Effective date and Expiration date added.
 * 06/01/2010 | SIW481  |   SW       | Comments performance Issue.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 09/08/2010 | SIW493  |    AS      | Fix in xc object constructor
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLCommentReference:XMLXCReferenceNode
    {
        /*' <!-- Multiple Levels -->
        '  <COMMENT_REFERENCE ID="CRxx" COMMENT_ID="CMTxxx">
        '     <TECH_KEY>tehnical key</TECH_KEY>
        '     <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day" />  'SIW321
        '     <EXPIRY_DATE YEAR="year" MONTH="month" DAY="day" />     'SIW321
        '  </COMMENT_REFERENCE>
        '     <!-- Multiple Occurrences -->*/

        private XMLComment m_Comment;
        //SIW529 private string sCRNodeName; //SIW360
        
        public XMLCommentReference()
        {
            //SIW529 Utils.subInitializeGlobals(XML); //SIW493
            xmlThisNode = null;
            xmlNodeList = null;
            m_Comment = null;
            //SIW529 sCRNodeName = Constants.sCmtReference; //SIW360

            sThisNodeOrder = new ArrayList(2);//SIW360
            sThisNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sThisNodeOrder.Add(Constants.sCmtEffDate);//SIW360
            sThisNodeOrder.Add(Constants.sCmtExpDate);//SIW360
        }

        protected override void CheckEntry()
        {
            xEntry(CommentID);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sCmtReference; }
        }

        public XmlNode putComment(string sID)
        {
            //Node = null;
            //if (sID != "" && sID != null)
            //    findCommentRefbyId(sID);
            //if (Node == null)
            //    Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            //CommentID = sID;
            //return Node;
            return putComment(sID, "", "");
            //End SIW360
        }

        //Start SIW360 
        public XmlNode putComment(string sID, string sRefID, string sTechKey)
        {
            Node = null;
            //Start SIW481
            //if (sID != "" && sID != null)
            //    findCommentRefbyId(sID);
            //SIW529 if (sID != "" || sRefID != "" || sTechKey != "")
            if (!string.IsNullOrEmpty(sID) || !string.IsNullOrEmpty(sRefID) || !string.IsNullOrEmpty(sTechKey)) //SIW529
                findCommentRef(sRefID, sTechKey, sID);
            if (Node == null)
            {
                //SIW529 Node = XML.XMLaddNode(Parent_Node, sCRNodeName, Parent_NodeOrder); //SIW360
                Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);  //SIW529
            }
            if (CommentRefID != "")
                CommentRefID = sRefID;
            if (sTechKey != "")
                Techkey = sTechKey;
            CommentID = sID;
            return Node;
        }
        //End SIW481
        //End SIW360

        public XmlNode getComment(bool reset)
        {
            return getNode(reset);
        }
        //Start SIW481
        public XmlNode findCommentRef()
        {
            return findCommentRef("", "", ""); 
        }
        public XmlNode findCommentRef(string RefID, string RefTK, string CmntID)
        {
            bool rst = true;
            if ((RefID != "" && RefID == CommentRefID) || (RefTK != "" && RefTK == Techkey) || (CmntID != "" && CmntID == CommentID))
                return Node;
            while (getComment(rst) != null)
            {
                rst = false;
                if (RefID != "")
                    if (RefID == CommentRefID)
                        break;
                if (RefTK != "")
                    if (RefTK == Techkey)
                        break;
                if(CmntID != "")
                    if(CmntID == CommentID)
                        break;
            }
            return Node;
        }
        //End SIW481
        public XmlNode findCommentRefbyId(string sID)
        {
            if (sID != CommentID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sID == CommentID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public string CommentID
        {
            get
            {
                string strdata = "";
                if (xmlThisNode != null)
                    if (xmlThisNode.Attributes["COMMENT_ID"] != null) //ABAHL3
                        strdata = XML.XMLGetAttributeValue(xmlThisNode, Constants.sCmtIDRef);//SIW360
                    else
                        strdata = XML.XMLGetAttributeValue(xmlThisNode, Constants.sCmtID);
                return ((Utils)Utils).extCommentID(strdata);
            }
            set
            {
                if (xmlThisNode == null)
                    putComment(value);
                else
                    XML.XMLSetAttributeValue(xmlThisNode, Constants.sCmtIDRef, Constants.sCmtIDPfx + value.Trim()); //SIW360
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    putComment("");
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                base.Node = value;
                m_Comment = null;
            }
        }
        //start SIW529 
        //public string NodeName //SIW360
        //{
        //    get
        //    {
        //        return sCRNodeName;
        //    }
        //    set
        //    {
        //        sCRNodeName = value;
        //    }
        //}
        //End SIW529
        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                ResetParent();  //SIW529
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            Node = null;
        }

        public XMLComment Comment
        {
            get
            {
                if (m_Comment == null)
                {   //SIW529
                    m_Comment = new XMLComment();
                    LinkXMLObjects(m_Comment);  //SIW529
                }   //SIW529
                m_Comment.getCommentbyID(CommentID);
                return m_Comment;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                //SIW529 return getNodeList(sCRNodeName);//SIW360
                return getNodeList(NodeName);    //SIW529 
            }
        }

        //SIW360 Starts
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                //SIW485 Utils.putData(putNode, Constants.sStdTechKey, value, Globals.sNodeOrder); //SIW321
                Utils.putData(putNode, Constants.sStdTechKey, value, Utils.sNodeOrder); //SIW485
            }
        }
        //Start SIW321
        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCmtEffDate);
            }
            set
            {
                //SIW485 Utils.putDate(putNode, Constants.sCmtEffDate, value, Globals.sNodeOrder);
                Utils.putDate(putNode, Constants.sCmtEffDate, value, Utils.sNodeOrder);	//SIW485
            }
        }
        public string ExpiryDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCmtExpDate);
            }
            set
            {
                //SIW485 Utils.putDate(putNode, Constants.sCmtExpDate, value, Globals.sNodeOrder);
                Utils.putDate(putNode, Constants.sCmtExpDate, value, Utils.sNodeOrder);	//SIW485
            }
        }
        //End SIW321
        public string CommentRefID
        {
            get
            {
                string sID, strdata;
                sID = "";
                strdata = XML.XMLGetAttributeValue(Node, Constants.sCmtRefID);
                if (strdata.Trim() != "")
                {
                    if (strdata.Substring(0, 2) == Constants.sCmtRefIDPFX && strdata.Length >= 3)
                    {
                        sID = StringUtils.Right(strdata, (strdata.Length - 2));
                    }
                }
                return sID;
            }
            set
            {
                string lid = string.Empty;
                if (value == null || value == "" || value == "0")
                {
                    XMLGlobals.lCRID = XMLGlobals.lCRID + 1;
                    lid = XMLGlobals.lCRID.ToString();
                }
                else
                {
                    lid = value;
                }
                XML.XMLSetAttributeValue(putNode, Constants.sCmtRefID, Constants.sCmtRefIDPFX + lid.Trim());
            }
        }
        //SIW360 Ends
    }
}
