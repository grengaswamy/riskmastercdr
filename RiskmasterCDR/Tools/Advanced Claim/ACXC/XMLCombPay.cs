/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *  08/21/2011 | SIW7240 |   VM      | Comb Payment web enhancement
 *  09/27/2011 | SIN7582 |   SS      | Changes in  Stop Flag property(Related to SIW7492)
 *  10/05/2011 | SIW7601 |   SW      | EntityAccountID added to combpay
 *  05/25/2012 | SIN8537 |   SAS     | Fixed check box mapping function.
 ***********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;

namespace CCP.XmlComponents
{
    public class XMLCombPay : XMLXCNodeWithList
    {
        /*****************************************************************************
        ' The cXMLCombPay class provides the functionality to support and manage an
        ' Advanced Claims Combined Pay (Entity) Node
        ' Which is a child of the Entity node
        '*****************************************************************************
        '      <COMBINED_PAY ID="CPAYxxx">
        '         <!--Mulitiple combined pay entries allowed per entity-->
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>  
        '           <ADDRESS_REFERENCE ID="RELATIONID"
        '                      ADDRESS_ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Relationship Code">Type of</ADDRESS>
        '                     <TECH_KEY>technical key value for relationship</TECH_KEY>
        '           </ADDRESS_REFERENCE >
        '           <ACCOUNT CODE="banknumber">
        '               <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '               <NAME>AccountName</NAME>
        '               <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '               <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '               <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '           </ACCOUNT>
        '           <NEXT_PRINT_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      </COMBINED_PAY>
        */
        private XMLEntity m_Entity;
        private XMLAddressReference m_AddressReference;
        private XmlNodeList xmlAccountList;
        private XmlNode xmlAccount;
        private ArrayList sAccountNodeOrder;

         public XMLCombPay()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;

            //SIW7601 sThisNodeOrder = new ArrayList(14);    //SI7420 
            sThisNodeOrder = new ArrayList(15);              //SIW7601
            sThisNodeOrder.Add(Constants.sStdTechKey);          
            sThisNodeOrder.Add(Constants.sEntCombPayNextPrtDt);
            sThisNodeOrder.Add(Constants.sAddressReferenceNode);
            sThisNodeOrder.Add(Constants.sEntCombPayAcctNode);
            //SI7420- START
            sThisNodeOrder.Add(Constants.sEntCombPayFrequency);
            sThisNodeOrder.Add(Constants.sEntCombPayWeekMonthCount);
            sThisNodeOrder.Add(Constants.sEntCombPayStopDate);
            sThisNodeOrder.Add(Constants.sEntCombPayNextSchedDate);
            sThisNodeOrder.Add(Constants.sEntCombPayCurrBatch);
            sThisNodeOrder.Add(Constants.sEntCombPayCurrBatchNumPays);
            sThisNodeOrder.Add(Constants.sEntCombPayEFTEffDate);
            sThisNodeOrder.Add(Constants.sEntCombPayEFTExpDate);
            sThisNodeOrder.Add(Constants.sEntCombPayEFTRoute);
            sThisNodeOrder.Add(Constants.sEntCombPayEFTAcct);
            //SI7420- END
            sThisNodeOrder.Add(Constants.sEntCombPayEntAccountID);  //SIW7601  
            sAccountNodeOrder = new ArrayList(5);
            sAccountNodeOrder.Add(Constants.sStdTechKey);
            sAccountNodeOrder.Add(Constants.sEntCombPayAccountName);
            sAccountNodeOrder.Add(Constants.sEntCombPayRoutingNumber);
            sAccountNodeOrder.Add(Constants.sEntCombPayAccountNumber);
            sAccountNodeOrder.Add(Constants.sEntCombPayFractionalNumber);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sEntCombPay; }
        }

        public override XmlNode Create()
        {
            //SIW7240 - start
            //return Create("", "");
            return Create("", "","", "","", "","", "","", "","","", "","");
            //SIW7240 -end
        }

        //SIW7240 - start
        //public XmlNode Create(string sID, string snextprintdate)
        public XmlNode Create(string sID, string snextprintdate, string sFrequency, string sFrequencyCode, string sWeekMonthCount, string sStopDate, string sStopFlag, 
                              string sNextSchedDate, string sCurrBatch, string sCurrBatchNumPays, string sEFTAcct, string sEFTRoute, 
                              string sEFTEffDate, string sEFTExpDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                CombPayId = sID;
                NextPrintDate = snextprintdate;
                //SIW7240 -START
                WeekMonthCount = sWeekMonthCount;
                StopDate = sStopDate;
                StopFlag = sStopFlag;
                NextSchedDate = sNextSchedDate;
                CurrBatch = sCurrBatch;
                CurrBatchNumPays = sCurrBatchNumPays;
                EFTAcct = sEFTAcct;
                EFTRoute = sEFTRoute;
                EFTEffDate = sEFTEffDate;
                EFTExpDate = sEFTExpDate;

                putFrequency(sFrequency, sFrequencyCode);
                //SIW7240 - END
              
                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLPolicy.Create");
                return null;
            }   
        }
        //SIW7240- start
        public string CombPayId
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sEntCombPayId);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 4) == "CPAY" && strdata.Length >= 5)
                        lid = StringUtils.Right(strdata, strdata.Length - 4);
                return lid;

                //return Utils.getAttribute(Node, Constants.sEntCombPay, Constants.sEntCombPayId);
            }
            set
            {
                //Utils.putAttribute(putNode, Constants.sEntCombPay, Constants.sEntCombPayId, value, NodeOrder);
                XML.XMLSetAttributeValue(putNode, Constants.sEntCombPayId, "CPAY" + value);
                //Techkey = value;
            }
        }

        public string Techkey 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }

        //Start SIW7601
        public string EntityAccountID
        {
            get
            {               
                string strdata = Utils.getData(Node, Constants.sEntCombPayEntAccountID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sACCIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEntAccountID, Constants.sACCIDPfx + value, NodeOrder);
            }
        }
        //End SIW7601

        public string NextPrintDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayNextPrtDt);//SIW489
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayNextPrtDt, value, NodeOrder);//SIW489
            }
        }

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                {
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                }
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public int AccountCount
        {
            get
            {
                return AccountList.Count;
            }
        }

        public XmlNodeList AccountList
        {
            get
            {
                //Start SIW163
                if (null == xmlAccountList)
                {
                    return getNodeList(Constants.sEntCombPayAcctNode, ref xmlAccountList, Node);
                }
                else
                {
                    return xmlAccountList;
                }
                //End SIW163
            }
        }

        public XmlNode getAccount(bool reset)
        {
            //Start SIW163
            if (null != AccountList)
            {
                return getNode(reset, ref xmlAccountList, ref xmlAccount);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode addAccount()
        {
            AccountNode = null;
            return putAccountNode;
        }

        public XmlNode putAccountNode
        {
            get
            {
                if (AccountNode == null)
                    AccountNode = XML.XMLaddNode(Node, Constants.sEntCombPayAcctNode, NodeOrder);
                return AccountNode;
            }
        }

        public XmlNode AccountNode
        {
            get
            {
                return xmlAccount;
            }
            set
            {
                xmlAccount = value;
            }
        }

        public XmlNode putAccount(string sAccountId, string sBankNumber, string sRoutingNumber,
                                  string sAccountNumber, string sFractionalNumber, string sAccountName)
        {
            if (sAccountId != "" && sAccountId != null)
                AccountTechkey = sAccountId;
            if (sAccountName != "" && sAccountName != null)
                AccountName = sAccountName;
            if (sBankNumber != "" && sBankNumber != null)
                BankNumber = sBankNumber;
            if (sRoutingNumber != "" && sRoutingNumber != null)
                RoutingNumber = sRoutingNumber;
            if (sAccountNumber != "" && sAccountNumber != null)
                AccountNumber = sAccountNumber;
            if (sFractionalNumber != "" && sFractionalNumber != null)
                FractionalNumber = sFractionalNumber;
            return AccountNode;
        }

        public string BankNumber
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sEntCombPayAcctNode, Constants.sEntCombPayBankNumber);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sEntCombPayAcctNode, Constants.sEntCombPayBankNumber, value, sAccountNodeOrder);
            }
        }

        public string AccountTechkey
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sStdTechKey, value, sAccountNodeOrder);
            }
        }

        public string RoutingNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayRoutingNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayRoutingNumber, value, sAccountNodeOrder);
            }
        }

        public string AccountName
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayAccountName);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayAccountName, value, sAccountNodeOrder);
            }
        }

        public string AccountNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayAccountNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayAccountNumber, value, sAccountNodeOrder);
            }
        }

        public string FractionalNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayFractionalNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayFractionalNumber, value, sAccountNodeOrder);
            }
        }
        //SIW7240 -START
        public string Frequency
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);    
            }
        }

        public string Frequency_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putCode(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);   
            }
        }

        public string Frequency_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);    
            }
        }

        public string WeekMonthCount
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayWeekMonthCount);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayWeekMonthCount, value, NodeOrder);
            }
        }


        public string StopDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayStopDate);    
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayStopDate, value, NodeOrder);    
            }
        }

        public string StopFlag
        {
           

           get
           {
            //return Utils.getBool(Node, Constants.sEntCombPayAcctNode, Constants.sEntCombPayStopFlag);//SIN7582
            //return Utils.getBool(Node, Constants.sEntCombPayStopFlag, "");//SIN7582 //SIN8537
            return XML.XMLGetAttributeValue(Node, Constants.sEntCombPayStopFlag); //SIN8537
           }
           set
           {
             // Utils.putBool(putNode, Constants.sEntCombPayAcctNode, Constants.sEntCombPayStopFlag, value, NodeOrder);//SIN7582
             // Utils.putBool(putNode, Constants.sEntCombPayStopFlag, "", value, NodeOrder);//SIN7582 SIN8537
            XML.XMLSetAttributeValue(putNode, Constants.sEntCombPayStopFlag, "" + value);  //SIN8537
           }           
        }


        public string LastDOM
        {
            get
            {
                return Utils.getBool(Node, Constants.sEntCombPayLastDOM,"");
            }
            set
            {
                Utils.putBool(putNode,  Constants.sEntCombPayLastDOM,"", value, NodeOrder);
            }
        }

        public string LastSchedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayLastSchedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayLastSchedDate, value, NodeOrder);
            }
        }

        public string NextSchedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayNextSchedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayNextSchedDate, value, NodeOrder);
            }
        }


        public string CurrBatch
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayCurrBatch);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayCurrBatch, value, NodeOrder);
            }
        }
        public string CurrBatchNumPays 
        {
            get
            {
                return Utils.getData(Node,Constants.sEntCombPayCurrBatchNumPays);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayCurrBatchNumPays, value, NodeOrder);
            }
        }

        public string EFTAcct
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayEFTAcct);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEFTAcct, value, NodeOrder);
            }
        }


        public string EFTRoute
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayEFTRoute);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEFTRoute, value, NodeOrder);
            }
        }

        public string EFTEffDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayEFTEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayEFTEffDate, value, NodeOrder);
            }
        }

        public string EFTExpDate
        {
            get
            {
                return Utils.getDate(Node,  Constants.sEntCombPayEFTExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayEFTExpDate, value, NodeOrder);
            }
        }

        public XmlNode putFrequency(string sDesc, string sCode)
        {
            return putFrequency(sDesc, sCode, "");
        }

        public XmlNode putFrequency(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEntCombPayAcctNode, sDesc, sCode, NodeOrder, scodeid);    
        }

        public new XMLEntity Parent
        {
            get
            {
                if (m_Entity == null)
                {
                    m_Entity = new XMLEntity();
                    m_Entity.CombPay = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Entity);   //SIW529
                    ResetParent();
                }
                return m_Entity;
            }
            set
            {
                m_Entity = value;
                ResetParent();
                if (m_Entity != null) //SIW529
                    ((XMLXCNodeBase)m_Entity).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Entity != null)
                m_Entity.CombPay = this;
            Node = null;
            //LinkXMLObjects((XMLXCNodeBase)m_Entity);   //SIW529
        }

        private void subClearPointers()
        {
            m_AddressReference=null;
            xmlAccountList = null;
            xmlAccount =null;
       }
       public override XmlNodeList NodeList
       {
           get
           {
               return getNodeList(NodeName);
           }
       }

       public XmlNode getCombPay(bool reset)
       {
           return getNode(reset);
       }
        //SIW7240- start
       public XmlNode getCombPayID(string sCombPayID)
       {
           getFirst();
           while (Node != null)
           {
               if (CombPayId  == sCombPayID)
                   break;
               getNext();
           }
           return Node;
       }
        //SIW7240- End
   }
}
