/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 02/24/2010 |  SIW288 |    ASM     | Update for DemandOffer save    
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 12/02/2011 |SIN7884  |  SS      | Resolved - Demand/Offer 'Activity' in WEB is not displaying after a Demand / Offer is saved                                  
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLDemandOffer : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<parent>
        '  Multiple nodes allowed
        '  <DEMAND_OFFER>
        '  <DEMAND_OFFER ID="DOxxx" TYPE="DEMAND | OFFER | APPEAL"/>            'SI05003
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        ''    <ACTIVITY CODE="xx"></ACTIVITY> <SIN7884>
        '     <AMOUNT>amount</AMOUNT>
        '     <DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '     <RESULT CODE="xx">result</RESULT>
        '     <DECISION CODE="xx">descision</DECISION>
        '     <REMARKS>remarks text</REMARKS>                                   'SI05003
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <DATA_ITEM> ... </DATA_ITEM>        
        '  </DEMAND_OFFER>
        '</parent>*/

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLVarDataItem m_DataItem;

        private Dictionary<object, object> dctDOType;
        
        public XMLDemandOffer()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(10);               //(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(12);  //SIN7884               //(SI06023 - Implemented in SI06333)
            sThisNodeOrder.Add(Constants.sStdTechKey);           //SIW360
            sThisNodeOrder.Add(Constants.sDOActivity);//SIN7884
            sThisNodeOrder.Add(Constants.sDOType);
            sThisNodeOrder.Add(Constants.sDOAmount);
            sThisNodeOrder.Add(Constants.sDODate);
            sThisNodeOrder.Add(Constants.sDOTime);
            sThisNodeOrder.Add(Constants.sDOResult);
            sThisNodeOrder.Add(Constants.sDODecision);
            sThisNodeOrder.Add(Constants.sDORemarks);
            sThisNodeOrder.Add(Constants.sDOPartyInvolved);
            sThisNodeOrder.Add(Constants.sDOCommentReference);
            sThisNodeOrder.Add(Constants.sDODataItem);
          
           
            //sThisNodeOrder.Add(Constants.sDOTechKey);           //(SI06023 - Implemented in SI06333) //SIW360


            dctDOType = new Dictionary<object, object>();
            dctDOType.Add(doType.doTypeUndefined, Constants.sUndefined);
            dctDOType.Add(Constants.sUndefined, doType.doTypeUndefined);
            dctDOType.Add(doType.doTypeDemand, Constants.sDOTypeDemand);
            dctDOType.Add(Constants.sDOTypeDemand, doType.doTypeDemand);
            dctDOType.Add(doType.doTypeOffer, Constants.sDOTypeOffer);
            dctDOType.Add(Constants.sDOTypeOffer, doType.doTypeOffer);
            dctDOType.Add(doType.doTypeAppeal, Constants.sDOTypeAppeal);
            dctDOType.Add(Constants.sDOTypeAppeal, doType.doTypeAppeal);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sDONode; }
        }

        public override XmlNode Create()
        {
            return Create("", doType.doTypeUndefined, "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sID, doType sType, string sAmount, string sDate, string sTime, string sResult,
                              string sResultCode, string sDecision, string sDecisionCode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ID = sID;
                ItemType = sType;
                Amount = sAmount;
                DODate = sDate;
                DOTime = sTime;
                putResult(sResult, sResultCode);
                putDecision(sDecision, sDecisionCode);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDemandOffer.Create");
                return null;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLLitigation();
                    ((XMLLitigation)m_Parent).DemandOffer = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DataItem = null;
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sDOIDPfx;
            }
        }

        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sDOID);
                string sID = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, IDPrefix.Length) == Constants.sDOIDPfx && strdata.Length >= (IDPrefix.Length + 1))
                        sID = StringUtils.Right(strdata, strdata.Length - IDPrefix.Length);
                return sID;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lDOId = Globals.lDOId + 1;
                    lid = Globals.lDOId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sDOID, Constants.sDOIDPfx + (lid + ""));
            }
        }

        public string ItemTypeTranslate(doType intype)
        {
            object etype;
            if (!dctDOType.TryGetValue((int)intype, out etype))
                etype = "";
            return (string)etype;
        }

        public doType ItemTypeTranslate(string intype)
        {
            object etype;
            if (!dctDOType.TryGetValue(intype, out etype))
                etype = doType.doTypeUndefined;
            return (doType)etype;
        }
        
        public string ItemType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sDOType);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sDOType, value, NodeOrder);
            }
        }

        public doType ItemType
        {
            get
            {
                string sdata = ItemType_Code;
                object etype;
                if (!dctDOType.TryGetValue(sdata, out etype))
                    etype = doType.doTypeUndefined;
                return (doType)etype;
            }
            set
            {
                object sdata; //string;
                //dctDOType.TryGetValue((int)value, out sdata); //SIW288
                dctDOType.TryGetValue(value, out sdata); //SIW288
                ItemType_Code = (string)sdata;
            }
        }

        //Start SIN7884


        public XmlNode putActivity(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDOActivity, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Activity
        {
            get
            {
                return Utils.getData(Node, Constants.sDOActivity);
            }
            set
            {
                Utils.putData(putNode, Constants.sDOActivity, value, NodeOrder);
            }
        }

        public string Activity_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDOActivity);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDOActivity, value, NodeOrder);
            }
        }

        public string Activity_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDOActivity);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDOActivity, value, NodeOrder);
            }
        }
        //End SIN7884
        public string Amount
        {
            get
            {
                return Utils.getData(Node, Constants.sDOAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sDOAmount, value, NodeOrder);
            }
        }

        public string Remarks
        {
            get
            {
                return Utils.getData(Node, Constants.sDORemarks);
            }
            set
            {
                Utils.putData(putNode, Constants.sDORemarks, value, NodeOrder);
            }
        }

        public string DODate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDODate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDODate, value, NodeOrder);
            }
        }

        public string DOTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDOTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDOTime, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putResult(string sDesc, string sCode)
        {
            return putResult(sDesc, sCode, "");
        }

        public XmlNode putResult(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDOResult, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Result
        {
            get
            {
                return Utils.getData(Node, Constants.sDOResult);
            }
            set
            {
                Utils.putData(putNode, Constants.sDOResult, value, NodeOrder);
            }
        }

        public string Result_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDOResult);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDOResult, value, NodeOrder);
            }
        }

        public string Result_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDOResult);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDOResult, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putDecision(string sDesc, string sCode)
        {
            return putDecision(sDesc, sCode, "");
        }

        public XmlNode putDecision(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDODecision, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Decision
        {
            get
            {
                return Utils.getData(Node, Constants.sDODecision);
            }
            set
            {
                Utils.putData(putNode, Constants.sDODecision, value, NodeOrder);
            }
        }

        public string Decision_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDODecision);
            }
            set
            {
                Utils.putCode(putNode, Constants.sDODecision, value, NodeOrder);
            }
        }

        public string Decision_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDODecision);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDODecision, value, NodeOrder);
            }
        }
        //End SIW139

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getDemandOffer(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode FindByID(string sID)
        {
            getFirst();
            while (Node != null)
            {
                if (ID == sID)
                    break;
                getNext();
            }
            return Node;
        }

        public XmlNode Remove()
        {
            if (Node != null)
            {
                Parent_Node.RemoveChild(Node);
                getFirst();
            }
            return Node;
        }
    }
}
