/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors 
 * 08/06/2008 | SI06249 |    sw      | CanBeSeen Date format fix
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 09/10/2008 | SI06423 |    CB      | Field mapping
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 ********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLLossInfo : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*<LOSS_INFORMATION>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333)'SIW360
        '    <LOSS_DESCRIPTION>description of loss</LOSS_DESCRIPTION>
        '    <USED_WITH_PERMISSION INDICATOR="YES|NO"/>
        '    <DAMAGE_DESCRIPTION>description of damage</DAMAGE_DESCRIPTION>
        '    <DAMAGE_DESC_HTML>description of damage in HTML format</DAMAGE_DESC_HTML>    //SI06369
        '    <ESTIMATED_DAMAGE_AMOUNT>$$$$$$$<ESTIMATED_DAMAGE_AMOUNT>
        '    <ACTUAL_DAMAGE_AMOUNT>$$$$$$$<ACTUAL_DAMAGE_AMOUNT>
        '    <RENTAL>
        '       <DAILY_CHARGE>daily rental charge</DAILY_CHARGE>
        '       <START_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <END_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <VENDOR ID="ENTxxxx>vendor name</VENDOR>
        '    </RENTAL>
        '    <CAN_BE_SEEN>
        '       <WHERE ID="ADRxxx">where is property for viewing</WHERE>
        '       <DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <AFTER_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '       <BEFORE_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '    </CAN_BE_SEEN>
        '    <CHARGES TYPE=[TOWING, CLEANING, REPAIRS, RENTAL, OTHER]>$$$$$</CHARGES>
        '       <!-- Multiple Charges Allowed --->
        '    <OTHER_INSURANCE>
        '      <POLICY_NUMBER>policynumber</POLICY_NUMBER
        '      <CARRIER>carriername</CARRIER>
        '      <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <COVERAGE>
        '          <COVERAGE_TYPE CODE="xx">Coverage Type</COVERAGE_TYPE>
        '          <COVERAGE_LIMIT CODE="xx" AMOUNT="amount">limit type</COVERAGE_LIMIT>
        '             <!-- Multiple Limits Allowed -->
        '          <COVERAGE_DEDUCTIBLE CODE="xx" AMOUNT="xx">deductible type</COVERAGE_DEDUCTIBLE>
        '             <!-- Multiple Deductibles Allowed -->
        '      </COVERAGE>
        '             <!-- Multiple Coverages Allowed -->
        '    </OTHER_INSURANCE>
        '    <REPAIR_ESTIMATE_DETAILS>
        '       <REPAIR_ESTIMATE>
        '          <REPAIR_ITEM CODE="xx">itemname</REPAIR_ITEM>
        '          <ITEM_UCR CODE="itemucrcode" SOURCE="ucrsourcecatalog"/>
        '          <COST>itemcost</COST>
        '          <LABOR HOURS="hours" RATE="rate">laborcost</LABOR>
        '          <TYPE_OF_REPAIR CODE="REPLACE|REPAIR|NEW|REBUILT">
        '          <VENDOR ID="ENTxxxx/>
        '       </REPAIR_ESTIMATE>
        '          <!-- Multiple Items allowed -->
        '    </REPAIR_ESTIMATE_DETAILS>
        '    <SALVAGE> ... </SALVAGE>
        '    <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '      <!--Muliple Party Involved nodes allowed -->
        '    <COMMENT_REFERENCE ID="CMTxx"/>
        '      <!-- multiple entries -->
        '    <DATA_ITEM> ... </DATA_ITEM>
        '      <!-- multiple data items -->        
        '</VEHICLE_LOSS>*/

        private XmlNode m_xmlCharge;
        private XmlNodeList m_xmlChargeList;
        private XmlNode m_xmlRprEst;
        private XmlNodeList m_xmlRprEstList;
        private XmlNode m_xmlOINCov;
        private XmlNodeList m_xmlOINCovList;
        private XmlNode m_xmlOINCovLmt;
        private XmlNodeList m_xmlOINCovLmtList;
        private XmlNode m_xmlOINCovDed;
        private XmlNodeList m_xmlOINCovDedList;

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLSalvage m_Salvage;
        private XMLVarDataItem m_DataItem;

        private ArrayList sRntNodeOrder;
        private ArrayList sRprNodeOrder;
        private ArrayList sCBSNodeOrder;
        private ArrayList sOINNodeOrder;
        private ArrayList sCovNodeOrder;

        private Dictionary<object, object> dctRPRType;
        
        public XMLLossInfo()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            m_Parent = null;

            //sThisNodeOrder = new ArrayList(15);                   //(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(16);                     //SI06369
            sThisNodeOrder.Add(Constants.sStdTechKey);               //SIW360
            sThisNodeOrder.Add(Constants.sLIDescription);
            sThisNodeOrder.Add(Constants.sLIUsedWithPermission);
            sThisNodeOrder.Add(Constants.sLIDamageDescription);
            sThisNodeOrder.Add(Constants.sLIDamageDescriptionHTML); //SI06369
            sThisNodeOrder.Add(Constants.sLIEstDamageAmount);
            sThisNodeOrder.Add(Constants.sLIActualDamageAmount);
            sThisNodeOrder.Add(Constants.sLIRental);
            sThisNodeOrder.Add(Constants.sLISeen);
            sThisNodeOrder.Add(Constants.sLICharges);
            sThisNodeOrder.Add(Constants.sLIOthrIns);
            sThisNodeOrder.Add(Constants.sLIRepairEstDetails);
            sThisNodeOrder.Add(Constants.sLISalvage);
            sThisNodeOrder.Add(Constants.sLIPartyInvolved);
            sThisNodeOrder.Add(Constants.sLICommentReference);
            sThisNodeOrder.Add(Constants.sLIDataItem);
            //sThisNodeOrder.Add(Constants.sLITechKey);               //(SI06023 - Implemented in SI06333) //SIW360

            sRprNodeOrder = new ArrayList(6);
            sRprNodeOrder.Add(Constants.sLIRepairEstItem);
            sRprNodeOrder.Add(Constants.sLIRepairEstItemUCR);
            sRprNodeOrder.Add(Constants.sLIRepairEstItemCost);
            sRprNodeOrder.Add(Constants.sLIRepairEstItemLabor);
            sRprNodeOrder.Add(Constants.sLIRepairEstItemTypeRepair);
            sRprNodeOrder.Add(Constants.sLIRepairEstItemVendor);

            sRntNodeOrder = new ArrayList(4);
            sRntNodeOrder.Add(Constants.sLIRentalCharge);
            sRntNodeOrder.Add(Constants.sLIRentalStartDate);
            sRntNodeOrder.Add(Constants.sLIRentalEndDate);
            sRntNodeOrder.Add(Constants.sLIRentalVendor);

            sCBSNodeOrder = new ArrayList(4);
            sCBSNodeOrder.Add(Constants.sLISeenWhere);
            sCBSNodeOrder.Add(Constants.sLISeenDate);
            sCBSNodeOrder.Add(Constants.sLISeenAfterTime);
            sCBSNodeOrder.Add(Constants.sLISeenBeforeTime);

            sOINNodeOrder = new ArrayList(5);
            sOINNodeOrder.Add(Constants.sLIOthrInsPolicy);
            sOINNodeOrder.Add(Constants.sLIOthrInsCarrier);
            sOINNodeOrder.Add(Constants.sLIOthrInsEffDate);
            sOINNodeOrder.Add(Constants.sLIOthrInsExpDate);
            sOINNodeOrder.Add(Constants.sLIOthrInsCoverage);

            sCovNodeOrder = new ArrayList(3);
            sCovNodeOrder.Add(Constants.sLIOthrInsCoverageType);
            sCovNodeOrder.Add(Constants.sLIOthrInsCoverageLimit);
            sCovNodeOrder.Add(Constants.sLIOthrInsCoverageDeductible);

            dctRPRType = new Dictionary<object, object>();
            dctRPRType.Add(Constants.iUndefined, Constants.sUndefined);
            dctRPRType.Add(Constants.sUndefined, Constants.iUndefined);
            dctRPRType.Add(Constants.iRepairTypeReplace, Constants.sRepairTypeReplace);
            dctRPRType.Add(Constants.sRepairTypeReplace, Constants.iRepairTypeReplace);
            dctRPRType.Add(Constants.iRepairTypeRepair, Constants.sRepairTypeRepair);
            dctRPRType.Add(Constants.sRepairTypeRepair, Constants.iRepairTypeRepair);
            dctRPRType.Add(Constants.iRepairTypeNew, Constants.sRepairTypeNew);
            dctRPRType.Add(Constants.sRepairTypeNew, Constants.iRepairTypeNew);
            dctRPRType.Add(Constants.iRepairTypeRebuilt, Constants.sRepairTypeRebuilt);
            dctRPRType.Add(Constants.sRepairTypeRebuilt, Constants.iRepairTypeRebuilt);
        }

        public override XmlNode Create()
        {
            return Create("");
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sLINode; }
        }

        public XmlNode Create(string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                LossDescription = sdescription;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLInfoLoss.Create");
                return null;
            }
        }

        public string LossDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sLIDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sLIDescription, value, NodeOrder);
            }
        }

        public string UsedWithPermission
        {
            get
            {
                return Utils.getBool(Node, Constants.sLIUsedWithPermission, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sLIUsedWithPermission, "", value, NodeOrder);
            }
        }

        public string DamageDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sLIDamageDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sLIDamageDescription, value, NodeOrder);
            }
        }

        //Start SI06369
        public string DamageDescriptionHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLIDamageDescriptionHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLIDamageDescriptionHTML, value, NodeOrder);
            }
        }
        //End SI06369

        public string EstDamageAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sLIEstDamageAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sLIEstDamageAmount, value, NodeOrder);
            }
        }

        public string ActDamageAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sLIActualDamageAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sLIActualDamageAmount, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public XmlNode putRentalNode
        {
            get
            {
                if (RentalNode == null)
                    XML.XMLaddNode(putNode, Constants.sLIRental, NodeOrder);
                return RentalNode;
            }
        }

        public XmlNode RentalNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sLIRental);
            }
        }

        public string RentalCharge
        {
            get
            {
                return Utils.getData(RentalNode, Constants.sLIRentalCharge);
            }
            set
            {
                Utils.putData(putRentalNode, Constants.sLIRentalCharge, value, sRntNodeOrder);
            }
        }

        public string RentalStartDate
        {
            get
            {
                return Utils.getDate(RentalNode, Constants.sLIRentalStartDate);
            }
            set
            {
                Utils.putDate(putRentalNode, Constants.sLIRentalStartDate, value, sRntNodeOrder);
            }
        }

        public string RentalEndDate
        {
            get
            {
                return Utils.getDate(RentalNode, Constants.sLIRentalEndDate);
            }
            set
            {
                Utils.putDate(putRentalNode, Constants.sLIRentalEndDate, value, sRntNodeOrder);
            }
        }

        public string RentalVendor
        {
            get
            {
                return Utils.getData(RentalNode, Constants.sLIRentalVendor);
            }
            set
            {
                Utils.putData(putRentalNode, Constants.sLIRentalVendor, value, sRntNodeOrder);
            }
        }

        public string RentalVendorID
        {
            get
            {
                return Utils.getEntityRef(RentalNode, Constants.sLIRentalVendor);
            }
            set
            {
                Utils.putEntityRef(putRentalNode, Constants.sLIRentalVendor, value, sRntNodeOrder);
            }
        }

        public XMLEntity RentalVendorEntity
        {
            get
            {
                XMLEntity m_xmlEntity = new XMLEntity();
                if (RentalVendorID != "" && RentalVendorID != null)
                    m_xmlEntity.getEntitybyID(RentalVendorID);
                return m_xmlEntity;
            }
        }

        public XmlNode putRprEstDetailsNode
        {
            get
            {
                if (RprEstDetailsNode == null)
                    XML.XMLaddNode(putNode, Constants.sLIRepairEstDetails, NodeOrder);
                return RprEstDetailsNode;
            }
        }

        public XmlNode RprEstDetailsNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sLIRepairEstDetails);
            }
        }

        public int RprEstCount
        {
            get
            {
                return RprEstNodeList.Count;
            }
        }

        public XmlNode addRprEst()
        {
            RprEstNode = null;
            return putRprEstNode;
        }

        public XmlNode putRprEstNode
        {
            get
            {
                if (RprEstNode == null)
                    //SIW485 RprEstNode = XML.XMLaddNode(putRprEstDetailsNode, Constants.sLIRepairEst, Globals.sNodeOrder);
                    RprEstNode = XML.XMLaddNode(putRprEstDetailsNode, Constants.sLIRepairEst, Utils.sNodeOrder);	//SIW485
                return RprEstNode;
            }
        }

        public XmlNode RprEstNode
        {
            get
            {
                return m_xmlRprEst;
            }
            set
            {
                m_xmlRprEst = value;
            }
        }

        public XmlNodeList RprEstNodeList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlRprEstList)
                {
                    return getNodeList(Constants.sLIRepairEst, ref m_xmlRprEstList, RprEstDetailsNode);
                }
                else
                {
                    return m_xmlRprEstList;
                }
                //End SIW163
            }
        }

        public XmlNode getRprEst(bool reset)
        {
            //Start SIW163
            if (null != RprEstNodeList)
            {
                return getNode(reset, ref m_xmlRprEstList, ref m_xmlRprEst);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        //Start SIW139
        public XmlNode putRprEstItem(string sDesc, string sCode)
        {
            return putRprEstItem(sDesc, sCode, "");
        }

        public XmlNode putRprEstItem(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putRprEstNode, Constants.sLIRepairEstItem, sDesc, sCode, sRprNodeOrder, scodeid);
        }

        public string RprEstItem
        {
            get
            {
                return Utils.getData(RprEstNode, Constants.sLIRepairEstItem);
            }
            set
            {
                Utils.putData(putRprEstNode, Constants.sLIRepairEstItem, value, sRprNodeOrder);
            }
        }

        public string RprEstItem_Code
        {
            get
            {
                return Utils.getCode(RprEstNode, Constants.sLIRepairEstItem);
            }
            set
            {
                Utils.putCode(putRprEstNode, Constants.sLIRepairEstItem, value, sRprNodeOrder);
            }
        }

        public string RprEstItem_CodeID
        {
            get
            {
                return Utils.getCodeID(RprEstNode, Constants.sLIRepairEstItem);
            }
            set
            {
                Utils.putCodeID(putRprEstNode, Constants.sLIRepairEstItem, value, sRprNodeOrder);
            }
        }
        //End SIW139

        public string RprEstCost
        {
            get
            {
                return Utils.getData(RprEstNode, Constants.sLIRepairEstItemCost);
            }
            set
            {
                Utils.putData(putRprEstNode, Constants.sLIRepairEstItemCost, value, sRprNodeOrder);
            }
        }

        public string RprEstLaborCost
        {
            get
            {
                return Utils.getData(RprEstNode, Constants.sLIRepairEstItemLabor);
            }
            set
            {
                Utils.putData(putRprEstNode, Constants.sLIRepairEstItemLabor, value, sRprNodeOrder);
            }
        }

        public string RprEstLaborHours
        {
            get
            {
                return Utils.getAttribute(RprEstNode, Constants.sLIRepairEstItemLabor, Constants.sLIRepairEstItemLaborHours);
            }
            set
            {
                Utils.putAttribute(putRprEstNode, Constants.sLIRepairEstItemLabor, Constants.sLIRepairEstItemLaborHours, value, sRprNodeOrder);
            }
        }

        public string RprEstLaborRate
        {
            get
            {
                return Utils.getAttribute(RprEstNode, Constants.sLIRepairEstItemLabor, Constants.sLIRepairEstItemLaborRate);
            }
            set
            {
                Utils.putAttribute(putRprEstNode, Constants.sLIRepairEstItemLabor, Constants.sLIRepairEstItemLaborRate, value, sRprNodeOrder);
            }
        }

        public string RprEstUCR
        {
            get
            {
                return Utils.getCode(RprEstNode, Constants.sLIRepairEstItemUCR);
            }
            set
            {
                Utils.putCode(putRprEstNode, Constants.sLIRepairEstItemUCR, value, sRprNodeOrder);
            }
        }

        public string RprEstUCRSource
        {
            get
            {
                return Utils.getAttribute(RprEstNode, Constants.sLIRepairEstItemUCR, Constants.sLIRepairEstItemUCRSource);
            }
            set
            {
                Utils.putAttribute(putRprEstNode, Constants.sLIRepairEstItemUCR, Constants.sLIRepairEstItemUCRSource, value, sRprNodeOrder);
            }
        }

        public xcRepairType RprEstItemTypeRepair
        {
            get
            {
                object etype;
                string sdata = RprEstItemTypeRepair_Code;
                if (!dctRPRType.TryGetValue(sdata, out etype))
                    etype = xcRepairType.xcRTUndefined;
                return (xcRepairType)etype;
            }
            set
            {
                object stype;
                dctRPRType.TryGetValue((int)value, out stype);
                RprEstItemTypeRepair_Code = (string)stype;
            }
        }

        public string RprEstItemTypeRepair_Code
        {
            get
            {
                return Utils.getCode(RprEstNode, Constants.sLIRepairEstItemTypeRepair);
            }
            set
            {
                Utils.putCode(putRprEstNode, Constants.sLIRepairEstItemTypeRepair, value, sRprNodeOrder);
            }
        }

        public string RprEstItemVendor
        {
            get
            {
                return Utils.getData(RprEstNode, Constants.sLIRepairEstItemVendor);
            }
            set
            {
                Utils.putData(putRprEstNode, Constants.sLIRepairEstItemVendor, value, sRprNodeOrder);
            }
        }

        public string RprEstItemVendorID
        {
            get
            {
                return Utils.getEntityRef(RprEstNode, Constants.sLIRepairEstItemVendor);
            }
            set
            {
                Utils.putEntityRef(putRprEstNode, Constants.sLIRepairEstItemVendor, value, sRprNodeOrder);
            }
        }

        public XMLEntity RprEstItemVendorEntity
        {
            get
            {
                XMLEntity m_xmlEntity = new XMLEntity();
                if (RprEstItemVendorID != "" && RprEstItemVendorID != null)
                    m_xmlEntity.getEntitybyID(RprEstItemVendorID);
                return m_xmlEntity;
            }
        }

        public XmlNode CanBeSeenNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sLISeen);
            }
        }

        public XmlNode putCanBeSeenNode
        {
            get
            {
                if (CanBeSeenNode == null)
                    XML.XMLaddNode(putNode, Constants.sLISeen, NodeOrder);

                return CanBeSeenNode;
            }
        }

        public string CanBeSeenWhere
        {
            get
            {
                return Utils.getData(CanBeSeenNode, Constants.sLISeenWhere);
            }
            set
            {
                Utils.putData(putCanBeSeenNode, Constants.sLISeenWhere, value, sCBSNodeOrder);
            }
        }

        public string CanBeSeenWhereID
        {
            get
            {
                return Utils.getAddressRef(CanBeSeenNode, Constants.sLISeenWhere);
            }
            set
            {
                Utils.putAddressRef(putCanBeSeenNode, Constants.sLISeenWhere, value, sCBSNodeOrder);
            }
        }

        public XMLAddress CanBeSeenAddress
        {
            get
            {
                XMLAddress xmlAddress = new XMLAddress();
                if (CanBeSeenWhereID != "" && CanBeSeenWhereID != null)
                    xmlAddress.getAddressbyID(CanBeSeenWhereID);
                return xmlAddress;
            }
        }

        public string CanBeSeenDate
        {
            get
            {
                //return Utils.getData(CanBeSeenNode, Constants.sLISeenDate);  //SI06249
                return Utils.getDate(CanBeSeenNode, Constants.sLISeenDate);
            }
            set
            {
                //Utils.putData(putCanBeSeenNode, Constants.sLISeenDate, value, sCBSNodeOrder); //SI06249
                Utils.putDate(putCanBeSeenNode, Constants.sLISeenDate, value, sCBSNodeOrder);
            }
        }

        public string CanBeSeenAfterTime
        {
            get
            {
                return Utils.getTime(CanBeSeenNode, Constants.sLISeenAfterTime);
            }
            set
            {
                Utils.putTime(putCanBeSeenNode, Constants.sLISeenAfterTime, value, sCBSNodeOrder);
            }
        }

        public string CanBeSeenBeforeTime
        {
            get
            {
                return Utils.getTime(CanBeSeenNode, Constants.sLISeenBeforeTime);
            }
            set
            {
                Utils.putTime(putCanBeSeenNode, Constants.sLISeenBeforeTime, value, sCBSNodeOrder);
            }
        }

        public int ChargeCount
        {
            get
            {
                return ChargeNodeList.Count;
            }
        }

        public XmlNode addCharge()
        {
            ChargeNode = null;
            return putChargeNode;
        }

        public XmlNode putChargeNode
        {
            get
            {
                if (ChargeNode == null)
                    ChargeNode = XML.XMLaddNode(putNode, Constants.sLICharges, NodeOrder);
                return ChargeNode;
            }
        }

        public XmlNode ChargeNode
        {
            get
            {
                return m_xmlCharge;
            }
            set
            {
                m_xmlCharge = value;
            }
        }

        public XmlNodeList ChargeNodeList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlChargeList)
                {
                    return getNodeList(Constants.sLICharges, ref m_xmlChargeList, Node);
                }
                else
                {
                    return m_xmlChargeList;
                }
                //End SIW163
            }
        }

        public XmlNode getCharge(bool reset)
        {
            //Start SIW163
            if (null != ChargeNodeList)
            {
                return getNode(reset, ref m_xmlChargeList, ref m_xmlCharge);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public string ChargeAmount
        {
            get
            {
                return Utils.getData(ChargeNode, "");
            }
            set
            {
                Utils.putData(putChargeNode, "", value, NodeOrder);
            }
        }

        public string ChargeType
        {
            get
            {
                return Utils.getAttribute(ChargeNode, "", Constants.sLIChargesType);
            }
            set
            {
                Utils.putAttribute(putChargeNode, "", Constants.sLIChargesType, value, NodeOrder);
            }
        }

        public XmlNode putOthrInsNode
        {
            get
            {
                if (OthrInsNode == null)
                    XML.XMLaddNode(putNode, Constants.sLIOthrIns, NodeOrder);
                return OthrInsNode;
            }
        }

        public XmlNode OthrInsNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sLIOthrIns);
            }
        }

        public string OthrInsPolicy
        {
            get
            {
                return Utils.getData(OthrInsNode, Constants.sLIOthrInsPolicy);
            }
            set
            {
                Utils.putData(putOthrInsNode, Constants.sLIOthrInsPolicy, value, sOINNodeOrder);
            }
        }

        public string OthrInsCarrier
        {
            get
            {
                return Utils.getData(OthrInsNode, Constants.sLIOthrInsCarrier);
            }
            set
            {
                Utils.putData(putOthrInsNode, Constants.sLIOthrInsCarrier, value, sOINNodeOrder);
            }
        }

        public string OthrInsCarrierID
        {
            get
            {
                return Utils.getEntityRef(OthrInsNode, Constants.sLIOthrInsCarrier);
            }
            set
            {
                Utils.putEntityRef(putOthrInsNode, Constants.sLIOthrInsCarrier, value, sOINNodeOrder);
            }
        }

        public XMLEntity OthrInsCarrierEntity
        {
            get
            {
                XMLEntity m_xmlEntity = new XMLEntity();
                if (OthrInsCarrierID != "" && OthrInsCarrierID != null)
                    m_xmlEntity.getEntitybyID(OthrInsCarrierID);
                return m_xmlEntity;
            }
        }

        public string OthrInsEffectiveDate
        {
            get
            {
                return Utils.getDate(OthrInsNode, Constants.sLIOthrInsEffDate);
            }
            set
            {
                Utils.putDate(putOthrInsNode, Constants.sLIOthrInsEffDate, value, sOINNodeOrder);
            }
        }

        public string OthrInsExpirationDate
        {
            get
            {
                return Utils.getDate(OthrInsNode, Constants.sLIOthrInsExpDate);
            }
            set
            {
                Utils.putDate(putOthrInsNode, Constants.sLIOthrInsExpDate, value, sOINNodeOrder);
            }
        }

        public int OthrInsCoverageCount
        {
            get
            {
                return OthrInsCoverageNodeList.Count;
            }
        }

        //Start SIW139 removing SI06423
        public XmlNode addOthrInsCoverage()
        {
            OthrInsCoverageNode = null;
            return putOthrInsCoverageNode;
        }

        public XmlNode putOthrInsCoverageNode
        {
            get
            {
                if (OthrInsCoverageNode == null)
                    OthrInsCoverageNode = XML.XMLaddNode(putOthrInsNode, Constants.sLIOthrInsCoverage, sOINNodeOrder);
                return OthrInsCoverageNode;
            }
        }

        public XmlNode OthrInsCoverageNode
        {
            get
            {
                return m_xmlOINCov;
            }
            set
            {
                m_xmlOINCov = value;
                m_xmlOINCovLmt = null;
                m_xmlOINCovLmtList = null;
                m_xmlOINCovDed = null;
                m_xmlOINCovDedList = null;
            }
        }
        //End SIW139

        public XmlNodeList OthrInsCoverageNodeList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlOINCovList)
                {
                    return getNodeList(Constants.sLIOthrInsCoverage, ref m_xmlOINCovList, OthrInsNode);
                }
                else
                {
                    return m_xmlOINCovList;
                }
                //End SIW163
            }
        }

        public XmlNode getOthrInsCoverage(bool reset)
        {
            //Start SIW163
            XmlNode xmlNode = null;
            if (null != OthrInsCoverageNodeList)
            {
                xmlNode = getNode(reset, ref m_xmlOINCovList, ref m_xmlOINCov);
            }       
            //End SIW163     
            m_xmlOINCovLmt = null;
            m_xmlOINCovLmtList = null;
            m_xmlOINCovDed = null;
            m_xmlOINCovDedList = null;
            return xmlNode;
        }

        //Start SIW139
        public XmlNode putOthrInsCovType(string sDesc, string sCode)
        {
            return putOthrInsCovType(sDesc, sCode, "");
        }

        public XmlNode putOthrInsCovType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageType, sDesc, sCode, sCovNodeOrder, scodeid);
        }

        public string OthrInsCovType
        {
            get
            {
                return Utils.getData(OthrInsCoverageNode, Constants.sLIOthrInsCoverageType);
            }
            set
            {
                Utils.putData(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageType, value, sCovNodeOrder);
            }
        }

        public string OthrInsCovType_Code
        {
            get
            {
                return Utils.getCode(OthrInsCoverageNode, Constants.sLIOthrInsCoverageType);
            }
            set
            {
                Utils.putCode(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageType, value, sCovNodeOrder);
            }
        }

        public string OthrInsCovType_CodeID
        {
            get
            {
                return Utils.getCodeID(OthrInsCoverageNode, Constants.sLIOthrInsCoverageType);
            }
            set
            {
                Utils.putCodeID(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageType, value, sCovNodeOrder);
            }
        }
        //End SIW139

        public XmlNode addOthrInsCovLimit()
        {
                OthrInsCovLimitNode = null;
                return putOthrInsCovLimitNode;
        }

        public XmlNode putOthrInsCovLimitNode
        {
            get
            {
                if (OthrInsCovLimitNode == null)
                    OthrInsCovLimitNode = XML.XMLaddNode(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageLimit, sCovNodeOrder);
                return OthrInsCovLimitNode;
            }
        }

        public XmlNode OthrInsCovLimitNode
        {
            get
            {
                return m_xmlOINCovLmt;
            }
            set
            {
                m_xmlOINCovLmt = value;
            }
        }

        public int OthrInsCovLimitCount
        {
            get
            {
                return OthrInsCovLimitNodeList.Count;
            }
        }

        public XmlNodeList OthrInsCovLimitNodeList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlOINCovLmtList)
                {
                    if (null == OthrInsCoverageNode)
                    {
                        getOthrInsCoverage(true);
                    }
                    return getNodeList(Constants.sLIOthrInsCoverageLimit, ref m_xmlOINCovLmtList, OthrInsCoverageNode);
                }
                else
                {
                    return m_xmlOINCovLmtList;
                }
                //End SIW163
            }
        }

        public XmlNode getOthrInsCovLimit(bool reset)
        {
            //Start SIW163
            if (null != OthrInsCovLimitNodeList)
            {
                return getNode(reset, ref m_xmlOINCovLmtList, ref m_xmlOINCovLmt);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        //Start SIW139 (also removed SI06423)
        public XmlNode putOthrInsCovLimit(string sDesc, string sCode)
        {
            return putOthrInsCovLimit(sDesc, sCode, "");
        }

        public XmlNode putOthrInsCovLimit(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putOthrInsCovLimitNode, "", sDesc, sCode, sCovNodeOrder, scodeid);
        }
        
        public string OthrInsCovLimit
        {
            get
            {
                return Utils.getData(OthrInsCovLimitNode, "");
            }
            set
            {
                Utils.putData(putOthrInsCovLimitNode, "", value, sCovNodeOrder);     //SI06423
            }
        }

        public string OthrInsCovLimit_Code
        {
            get
            {
                return Utils.getCode(OthrInsCovLimitNode, "");
            }
            set
            {
                Utils.putCode(putOthrInsCovLimitNode, "", value, sCovNodeOrder);
            }
        }

        public string OthrInsCovLimit_CodeID
        {
            get
            {
                return Utils.getCodeID(OthrInsCovLimitNode, "");
            }
            set
            {
                Utils.putCodeID(putOthrInsCovLimitNode, "", value, sCovNodeOrder);
            }
        }
        //End SIW139

        public string OthrInsCovLimitAmount
        {
            get
            {
                return Utils.getAttribute(OthrInsCovLimitNode, "", Constants.sLIOthrInsCoverageLimitAmount);
            }
            set
            {
                Utils.putAttribute(putOthrInsCovLimitNode, "", Constants.sLIOthrInsCoverageLimitAmount, value, sCovNodeOrder);
            }
        }

        public XmlNode addOthrInsCovDeductible()
        {
            OthrInsCovDeductibleNode = null;
            return putOthrInsCovDeductibleNode;
        }

        public XmlNode putOthrInsCovDeductibleNode
        {
            get
            {
                if (OthrInsCovDeductibleNode == null)
                    OthrInsCovDeductibleNode = XML.XMLaddNode(putOthrInsCoverageNode, Constants.sLIOthrInsCoverageDeductible, sCovNodeOrder);
                return OthrInsCovDeductibleNode;
            }
        }

        public XmlNode OthrInsCovDeductibleNode
        {
            get
            {
                return m_xmlOINCovDed;
            }
            set
            {
                m_xmlOINCovDed = value;
            }
        }

        public int OthrInsCovDeductibleCount
        {
            get
            {
                return OthrInsCovDeductibleNodeList.Count;
            }
        }

        public XmlNodeList OthrInsCovDeductibleNodeList
        {
            get
            {   
                //Start SIW163
                if (null == m_xmlOINCovDedList)
                {
                    if (null == OthrInsCoverageNode)
                    {
                        getOthrInsCoverage(true);
                    }
                    return getNodeList(Constants.sLIOthrInsCoverageDeductible, ref m_xmlOINCovDedList, OthrInsCoverageNode);
                }
                else
                {
                    return m_xmlOINCovDedList;
                }
                //End SIW163
            }
        }

        public XmlNode getOthrInsCovDeductible(bool reset)
        {
            //Start SIW163
            if (null != OthrInsCovDeductibleNodeList)
            {
                return getNode(reset, ref m_xmlOINCovDedList, ref m_xmlOINCovDed);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        //Start SIW139
        public XmlNode putOthrInsCovDeductible(string sDesc, string sCode)
        {
            return putOthrInsCovDeductible(sDesc, sCode, "");
        }

        public XmlNode putOthrInsCovDeductible(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putOthrInsCovDeductibleNode, "", sDesc, sCode, sCovNodeOrder, scodeid);
        }

        public string OthrInsCovDeductible
        {
            get
            {
                return Utils.getData(OthrInsCovDeductibleNode, "");
            }
            set
            {
                Utils.putData(putOthrInsCovDeductibleNode, "", value, sCovNodeOrder);
            }
        }

        public string OthrInsCovDeductible_Code
        {
            get
            {
                return Utils.getCode(OthrInsCovDeductibleNode, "");
            }
            set
            {
                Utils.putCode(putOthrInsCovDeductibleNode, "", value, sCovNodeOrder);
            }
        }

        public string OthrInsCovDeductible_CodeID
        {
            get
            {
                return Utils.getCodeID(OthrInsCovDeductibleNode, "");
            }
            set
            {
                Utils.putCodeID(putOthrInsCovDeductibleNode, "", value, sCovNodeOrder);
            }
        }
        //End SIW139

        public string OthrInsCovDeductibleAmount
        {
            get
            {
                return Utils.getAttribute(OthrInsCovDeductibleNode, "", Constants.sLIOthrInsCoverageDeductibleAmount);
            }
            set
            {
                Utils.putAttribute(putOthrInsCovDeductibleNode, "", Constants.sLIOthrInsCoverageDeductibleAmount, value, sCovNodeOrder);
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        private void subClearPointers()
        {
            m_xmlCharge = null;
            m_xmlChargeList = null;
            m_xmlRprEst = null;
            m_xmlRprEstList = null;
            m_xmlOINCov = null;
            m_xmlOINCovList = null;
            m_xmlOINCovLmt = null;
            m_xmlOINCovLmtList = null;
            m_xmlOINCovDed = null;
            m_xmlOINCovDedList = null;
            PartyInvolved = null;
            CommentReference = null;
            Salvage = null;
            DataItem = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLSalvage Salvage
        {
            get
            {
                if (m_Salvage == null)
                {
                    m_Salvage = new XMLSalvage();
                    m_Salvage.Parent = this;
                    //SIW529 LinkXMLObjects(m_Salvage);
                }
                return m_Salvage;
            }
            set
            {
                m_Salvage = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public new XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLVehLoss();
                    ((XMLVehLoss)m_Parent).LossInfo = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = Utils.getNode(Parent_Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }
    }
}
