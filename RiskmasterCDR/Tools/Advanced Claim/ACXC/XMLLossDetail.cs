/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/17/2010 | SIW169  |    AS      | Financial Processing
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/24/2010 | SI06650 |    KCB     | FindByID loop
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList
 * 09/13/2010 | SIW493  |   AS       | Claimant Display Name removed. Tree label used in UI
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/12/2010 | SIW452  |    AS      | Add percent disability and number of weeks to loss detail object
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 02/10/2011 | SIW529  |    AS      | Fixes for removing static functions
 * 03/21/2011 | SIW7036 |   SW       | Incurred limit in Web.
 * 09/23/2011 | SIW7509 |   Vineet   | Load payment screen from payment history screen
 * 10/13/2011 | SIW7647 | Vineet     | Display payment/collection history for frozen reserve
 * 11/14/2011 | SIW7537 | ubora      | ReserveLess payment / collection in ACWeb
 * 01/05/2012 | SIW8030 | AS         | Can not value the Reserve_Balance node in a Reserve Transaction in the AC XML
 * 04/17/2012 | SIN8386 |   MK       | ReserveAmount and ChangeAmount not getting updated on reserve change.
 * 04/17/2012 | SIW8393 |    ACH     | Added Invoice Date Field
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLLossDetail : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        ' The cXMLLoss Detail class provides the functionality to support and manage a
        ' ClaimsPro Loss Detail Node
        '*****************************************************************************
        
        '<CLAIM>
        '   <!-- Multiple Loss Details Allowed -->
        '   <LOSS_DETAIL ID="LDxxx" RESERVE_CLAIMANT_SEQ="1" OFFSET_ONSET="OFFSET">
        '     <TECH_KEY>AC Technical key for the element </TECH_KEY>  'SI06023 ' SI06333 'SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '     <LOSS_CLAIMANT CLAIMANT_NUMBER="1" ID="ENT249" />
        '     <RESERVE_TYPE CODE="MED">Medical</RESERVE_TYPE>
        '     <LOSS_MEMBER CODE="**" />
        '     <LOSS_DISABILITY CODE="DM" BUREAU_CODE="xx">Defense Medical Evaluation</LOSS_DISABILITY>
        '     <PERCENT_OF_DISABILITY>percent</PERCENT_OF_DISABILITY> 'SIW452
        '     <NUMBER_OF_WEEK>Number of Week</NUMBER_OF_WEEK>        'SIW452
        '     <RESERVE_CATEGORY CODE="**" />
        '     <CAUSE_OF_LOSS CODE="07" BUREAU_CODE="xx">WM</CAUSE_OF_LOSS>
        '     <CURRENT_STATUS CODE="O">Open</CURRENT_STATUS>                  'SI05005
        '     <BALANCE>balanceamount</BALANCE>                                'SI05005
        '     <INCURRED>incurred</INCURRED>                                   'SI05005
        '     <TOTAL_PAID>paidtotal</TOTAL_PAID>                              'SI05005
        '     <TOTAL_COLLECTION>collectionTotal</TOTAL_COLLECTION>            'SI05005
        '     <FINANCIAL_ACTIVITY ISFROZEN='True|False' ISONHOLD="True|False" ISPAYMENT='True|False' ISSCHEDULE='True|False' VIEWPAYMENT='True|False'/> 'SIW7036 'SIW7647
        '     <POLICY_COVERAGE POLICY_ID="POL143" LEGACY_COVERAGE_KEY="00001,1,2">
        '        <TECH_KEY>AC Technical key for PMPOLICYCOV</TECH_KEY>                    'SIW360
        '        <COV_EFF_DATE YEAR="2001" MONTH="12" DAY="01" />
        '        <INSURANCE_LINE CODE="WC">Workers Compensation</INSURANCE_LINE>
        '        <LOCATION>1</LOCATION>
        '        <SUB_LOCATION>0</SUB_LOCATION>
        '        <RISK_UNIT_GROUP />
        '        <RISK_UNIT>Workers Compensation - Voluntary</RISK_UNIT>
        '        <PERIL_OF_COVERAGE CODE="WC">Workers Compensation</PERIL_OF_COVERAGE>
        '        <COVERAGE_SEQUENCE>2</COVERAGE_SEQUENCE>
        '     </POLICY_COVERAGE>
        '     <FINANCIAL_TRANSACTION>
        '        <ACTIVITY_SEQUENCE>613</ACTIVITY_SEQUENCE>
        '        <ACTIVITY_TYPE CODE="PO">Offset No Reserve Adjustment</ACTIVITY_TYPE>
        '        <ADDED_BY_USER>ps</ADDED_BY_USER>
        '        <DATE_OF_TRANSACTION YEAR="2002" MONTH="03" DAY="20" />
        '        <TIME_OF_TRANSACTION HOUR="17" MINUTE="35" SECOND="04" />
        '        <TRANSACTION_TYPE CODE="**" />
        '        <RESERVE_STATUS CODE="O">Open</RESERVE_STATUS>
        '        <PAYMENT_TRANSACTION>
        '        <TECH_KEY>FUNDS_TRANS_SPLIT->SPLIT_ROW_ID</TECH_KEY>  'SIW7509
        '           <CONTROL_NUMBER>control number</CONTROL_NUMBER>
        '           <PAYMENT_AMOUNT>100</PAYMENT_AMOUNT>
        '           <REPORTABLE CODE="**" />
        '           <VOUCHER_REFERENCE ID="VCHxxx"/>
        '           <EXPENSE_TYPE CODE="xx">expense type</EXPENSE_TYPE>
        '           <INVOICE_NUMBER>invoice number</INVOICE_NUMBER>
        '           <INVOICE_AMOUNT>0</INVOICE_AMOUNT>
        '           <INVOICE_DATE YEAR="2002" MONTH="03" DAY="20" />
        '           <INVOICED_BY>invoiced by</INVOICED_BY>
        '           <PO_NUMBER>PO Number</PO_NUMBER>
        '           <SERVICE_FROM_DATE YEAR="2002" MONTH="03" DAY="20" />
        '           <SERVICE_TO_DATE YEAR="2002" MONTH="03" DAY="20" />
        '            <ISFINAL INDICATOR="True|False"/>       'SIW7509
        '           <ISFIRSTFINAL INDICATOR="True|False"/>       'SIW7537
        '           <ISSUPPLEMENTAL INDICATOR="True|False"/>       'SIW7537
        '        </PAYMENT_TRANSACTION>
        '        <RESERVE_TRANSACTION>
        '           <BALANCE_AMOUNT>0</BALANCE_AMOUNT>
        '           <CHANGE_AMOUNT>0</CHANGE_AMOUNT>
        '           <REASON_FOR_CHANGE CODE="xx">Change Reason</REASON_FOR_CHANGE>
        '        </RESERVE_TRANSACTION>
        '      <!-- Payment or Reserve, not both-->
        '     </FINANCIAL_TRANSACTION>        
        '   </LOSS_DETAIL>
        '</CLAIM>*/

        private XmlNode m_xmlFinTrans;
        private XmlNodeList m_xmlFinTransList;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private new XMLClaim m_Parent;
        private XMLVoucher m_Voucher;
        private XMLPolicy m_Policy;
        private ArrayList sPolCovNodeOrder;
        private ArrayList sFintransNodeOrder;
        private ArrayList sPayTransNodeOrder;
        private ArrayList sRsvTransNodeOrder;
        private XMLCoverage m_Coverage;//SIW490

        private Dictionary<object, object> DctOffSet;
        
        public XMLLossDetail()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_xmlFinTrans = null;
            m_xmlFinTransList = null;
            m_PartyInvolved = null;
            m_Comment = null;
            m_Parent = null;
            m_Coverage = null;//SIW490

            //sThisNodeOrder = new ArrayList(15);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(16);                 //(SI06023 - Implemented in SI06333)  //SIW490
            sThisNodeOrder = new ArrayList(22); //Payal
            sThisNodeOrder.Add(Constants.sStdTechKey);           //SI06023 // SI06333 //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW490
            sThisNodeOrder.Add(Constants.sLDClaimant);
            sThisNodeOrder.Add(Constants.sLDUnitNumber); //Payal
            sThisNodeOrder.Add(Constants.sLDRiskType); //Payal
            sThisNodeOrder.Add(Constants.sLDRsvType);
            sThisNodeOrder.Add(Constants.sLDLossMember);
            sThisNodeOrder.Add(Constants.sLDLossDisability);
            sThisNodeOrder.Add(Constants.sLDPercentDisability);  //SIW452
            sThisNodeOrder.Add(Constants.sLDNumberOfWeek);       //SIW452
            sThisNodeOrder.Add(Constants.sLDRsvCategory);
            sThisNodeOrder.Add(Constants.sLDCauseOfLoss);
            sThisNodeOrder.Add(Constants.sLDCurrentStatus);
            sThisNodeOrder.Add(Constants.sLDBalance);
            sThisNodeOrder.Add(Constants.sLDIncurred);
            sThisNodeOrder.Add(Constants.sLDTotalPaid);
            sThisNodeOrder.Add(Constants.sLDTotalCollection);
            sThisNodeOrder.Add(Constants.sStdFinancialActivity);  //SIW7036  
            sThisNodeOrder.Add(Constants.sLDPolCoverageNode);
            sThisNodeOrder.Add(Constants.sLDFTNode);
            sThisNodeOrder.Add(Constants.sLDPartyInvolved);
            sThisNodeOrder.Add(Constants.sLDCommentRef);


            sThisNodeOrder.Add(Constants.sRsvDttmAdded);
            sThisNodeOrder.Add(Constants.sRsvDttmUpdated);
            sThisNodeOrder.Add(Constants.sRsvPreparedByUser);
            sThisNodeOrder.Add(Constants.sRsvUpdatedByUser);

            
            //sThisNodeOrder.Add(Constants.sLDTechKey);           //SI06023 // SI06333 //SIW360

            //sPolCovNodeOrder = new ArrayList(8);              //SIW169
            sPolCovNodeOrder = new ArrayList(9);                //SIW169
            sPolCovNodeOrder.Add(Constants.sStdTechKey);         //SIW169
            sPolCovNodeOrder.Add(Constants.sLDPolCovEffDate);
            sPolCovNodeOrder.Add(Constants.sLDPolCovInsLine);
            sPolCovNodeOrder.Add(Constants.sLDPolCovLocationNbr);
            sPolCovNodeOrder.Add(Constants.sLDPolCovSubLocationNbr);
            sPolCovNodeOrder.Add(Constants.sLDPolCovRiskUnitGrp);
            sPolCovNodeOrder.Add(Constants.sLDPolCovRiskUnit);
            sPolCovNodeOrder.Add(Constants.sLDPolCovPeril);
            sPolCovNodeOrder.Add(Constants.sLDPolCovSeq);

            sFintransNodeOrder = new ArrayList(14);
            sFintransNodeOrder.Add(Constants.sLDFTActSequence);
            sFintransNodeOrder.Add(Constants.sLDFTActType);
            sFintransNodeOrder.Add(Constants.sLDFTAddedByUser);
            sFintransNodeOrder.Add(Constants.sLDFTTransDate);
            sFintransNodeOrder.Add(Constants.sLDFTTransTime);
            sFintransNodeOrder.Add(Constants.sLDFTTransType);
            sFintransNodeOrder.Add(Constants.sLDFTRsvStatus);
            sFintransNodeOrder.Add(Constants.sLDPTNode);
            sFintransNodeOrder.Add(Constants.sLDRTNode);
            sFintransNodeOrder.Add(Constants.sFtsDttmAdded);
            sFintransNodeOrder.Add(Constants.sFtsDttmUpdated);
            sFintransNodeOrder.Add(Constants.sFtsUpdatedByUser);
            sFintransNodeOrder.Add(Constants.sFtsPreparedByUser);
          //  sPayTransNodeOrder = new ArrayList(13);  //SIW7509            
            //sPayTransNodeOrder = new ArrayList(15);  //SIW7509 // SIW7537
            sPayTransNodeOrder = new ArrayList(17);  //SIW7537
            sPayTransNodeOrder.Add(Constants.sStdTechKey); //SIW7509
            sPayTransNodeOrder.Add(Constants.sLDPTControlNumber);
            sPayTransNodeOrder.Add(Constants.sLDPTAmount);
            sPayTransNodeOrder.Add(Constants.sLDPTReportableType);
            sPayTransNodeOrder.Add(Constants.sLDPTPaymentOffsetInd);
            sPayTransNodeOrder.Add(Constants.sLDPTExpType);
            sPayTransNodeOrder.Add(Constants.sLDPTVoucherReference);
            sPayTransNodeOrder.Add(Constants.sLDPTInvNbr);
            sPayTransNodeOrder.Add(Constants.sLDPTInvDate);
            sPayTransNodeOrder.Add(Constants.sLDPTInvAmount);
            sPayTransNodeOrder.Add(Constants.sLDPTPONbr);
            sPayTransNodeOrder.Add(Constants.sLDPTInvBy);
            sPayTransNodeOrder.Add(Constants.sLDPTSvcFromDate);
            sPayTransNodeOrder.Add(Constants.sLDPTSvcToDate);
            sPayTransNodeOrder.Add(Constants.sLDPTIsFinal);   //SIW7509
            sPayTransNodeOrder.Add(Constants.sLDPTIsFirstFinal);   //SIW7537
            sPayTransNodeOrder.Add(Constants.sLDPTIsSupplemental);   //SIW7537

            //SIN8386 sRsvTransNodeOrder = new ArrayList(4);
            sRsvTransNodeOrder = new ArrayList(5); //SIN8386
            sRsvTransNodeOrder.Add(Constants.sLDRTBalance);
            sRsvTransNodeOrder.Add(Constants.sLDRTChange);
            sRsvTransNodeOrder.Add(Constants.sLDRTChangeReason);
            sRsvTransNodeOrder.Add(Constants.sLDRTEntryOperator);
            sRsvTransNodeOrder.Add(Constants.sLDRTAmount); //SIN8386
            DctOffSet = new Dictionary<object, object>();
            DctOffSet.Add(Constants.sLDOffSet, Constants.iLDOffSet);
            DctOffSet.Add(Constants.iLDOffSet, Constants.sLDOffSet);
            DctOffSet.Add(Constants.sLDOnSet, Constants.iLDOnSet);
            DctOffSet.Add(Constants.iLDOnSet, Constants.sLDOnSet);
            DctOffSet.Add("UNSPECIFIED", 0);
            DctOffSet.Add(0, "UNSPECIFIED");
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sLDNode; }
        }

        public override XmlNode Create()
        {
            return Create("","", LDTOffOnSet.LDTOOSUnspecified,"","","","","","","","","","","","","","","");//Payal
        }

        public XmlNode Create(string sID, string sClaimantSeq, LDTOffOnSet cOffOnSet, string sClaimantNumber,
                              string sClaimandEntityID, string sReserveType, string sReserveTypeCode, string sLossMember,
                              string sLossMemberCode, string sLossDisability, string sLossDisabilityCode,
                              string sReserveCatg, string sReserveCatgCode, string sCauseOfLoss, string sCauseOfLossCode, string sUnitNumber, string sRiskType, string sRiskTypeCode) //Payal
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;

            xmlDocument = Document;
            Errors.Clear();

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                LossDetailID = sID;
                ClaimantSeq = sClaimantSeq;
                OffOnSet = cOffOnSet;
                ClaimantNumber = sClaimantNumber;
                ClaimantEntityID = sClaimandEntityID;
                putReserveType(sReserveType, sReserveTypeCode);
                putLossMember(sLossMember, sLossMemberCode);
                putLossDisability(sLossDisability, sLossDisabilityCode);
                putReserveCategory(sReserveCatg, sReserveCatgCode);
                putCauseOfLoss(sCauseOfLoss, sCauseOfLossCode);
                TotalUnitNumber = sUnitNumber;//Payal
                putRiskType(sRiskType, sRiskTypeCode);//Payal

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLLossDetail.Create");
                return null;
            }
        }

        public string ClaimantSeq
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sLDRsvClmntSeq);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sLDRsvClmntSeq, value);
            }
        }

        public LDTOffOnSet OffOnSet
        {
            get
            {
                string sdata = OffOnSet_Code;
                object idata;
                if (!DctOffSet.TryGetValue(sdata, out idata))
                    idata = LDTOffOnSet.LDTOOSUnspecified;
                return (LDTOffOnSet)idata;
            }
            set
            {
                object sdata;
                if (value == LDTOffOnSet.LDTOOSUnspecified)
                    XML.XMLRemoveAttribute_Node(Node, Constants.sLDOffOnSet);
                else
                {
                    DctOffSet.TryGetValue((int)value, out sdata);
                    OffOnSet_Code = (string)sdata;
                }
            }
        }

        public string OffOnSet_Code
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sLDOffOnSet);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sLDOffOnSet, value);
            }
        }

        public string ClaimantNumber
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sLDClaimant, Constants.sLDClaimantNumber);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sLDClaimant, Constants.sLDClaimantNumber, value, NodeOrder);
            }
        }
        public string ReserveAdjusterExaminerCd
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, "RESERVE_EXAMINER_CODE");
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, "RESERVE_EXAMINER_CODE", value);
            }
        }
            
        public string ClaimantEntityID
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sLDClaimant);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sLDClaimant, value, NodeOrder);
            }
        }
        // Start SIW493
        //public string ClaimantDisplayName
        //{
        //    get
        //    {
        //        XMLEntities xmlEntities = new XMLEntities();
        //        XMLEntity xmlEntity = new XMLEntity();
        //        xmlEntities.Document = Document;
        //        xmlEntity = xmlEntities.Entity;
        //        xmlEntity.getEntitybyID(this.ClaimantEntityID);
        //        return xmlEntity.DisplayName;
        //    }
        //}
        // End SIW493
        //Start SIW139
        public XmlNode putLossDisability(string sDesc, string sCode)
        {
            return putLossDisability(sDesc, sCode, "");
        }

        public XmlNode putLossDisability(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDLossDisability, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LossDisability
        {
            get
            {
                return Utils.getData(Node, Constants.sLDLossDisability);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDLossDisability, value, NodeOrder);
            }
        }

        public string LossDisability_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDLossDisability);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDLossDisability, value, NodeOrder);
            }
        }

        public string LossDisability_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDLossDisability);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDLossDisability, value, NodeOrder);
            }
        }
        //End SIW139
        
        //Start SIW452
        public string PercentDisability
        {
            get
            {
                return Utils.getData(Node, Constants.sLDPercentDisability);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDPercentDisability, value, NodeOrder);
            }
        }

        public string NumberOfWeek
        {
            get
            {
                return Utils.getData(Node, Constants.sLDNumberOfWeek);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDNumberOfWeek, value, NodeOrder);
            }
        }
        //End SIW452
        public string LossDisabilityBureau_Code
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sLDLossDisability, Constants.sLDBureauCode);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sLDLossDisability, Constants.sLDBureauCode, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putReserveType(string sDesc, string sCode)
        {
            return putReserveType(sDesc, sCode, "");
        }

        public XmlNode putReserveType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDRsvType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ReserveType
        {
            get
            {
                return Utils.getData(Node, Constants.sLDRsvType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDRsvType, value, NodeOrder);
            }
        }

        public string ReserveType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDRsvType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDRsvType, value, NodeOrder);
            }
        }

        public string ReserveType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDRsvType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDRsvType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putReserveCategory(string sDesc, string sCode)
        {
            return putReserveCategory(sDesc, sCode, "");
        }

        public XmlNode putReserveCategory(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDRsvCategory, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ReserveCategory
        {
            get
            {
                return Utils.getData(Node, Constants.sLDRsvCategory);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDRsvCategory, value, NodeOrder);
            }
        }

        public string ReserveCategory_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDRsvCategory);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDRsvCategory, value, NodeOrder);
            }
        }

        public string ReserveCategory_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDRsvCategory);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDRsvCategory, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCurrentStatus(string sDesc, string sCode)
        {
            return putCurrentStatus(sDesc, sCode, "");
        }

        public XmlNode putCurrentStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDCurrentStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CurrentStatus
        {
            get
            {
                return Utils.getData(Node, Constants.sLDCurrentStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDCurrentStatus, value, NodeOrder);
            }
        }

        public string CurrentStatus_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDCurrentStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDCurrentStatus, value, NodeOrder);
            }
        }

        public string CurrentStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDCurrentStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDCurrentStatus, value, NodeOrder);
            }
        }
        //End SIW139

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder); //SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel); 
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder); 
            }
        }
        //End SIW490

        public string Balance
        {
            get
            {
                return Utils.getData(Node, Constants.sLDBalance);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDBalance, value, NodeOrder);
            }
        }

        public string Incurred
        {
            get
            {
                return Utils.getData(Node, Constants.sLDIncurred);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDIncurred, value, NodeOrder);
            }
        }

        public string TotalPaid
        {
            get
            {
                return Utils.getData(Node, Constants.sLDTotalPaid);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDTotalPaid, value, NodeOrder);
            }
        }

        public string TotalCollection
        {
            get
            {
                return Utils.getData(Node, Constants.sLDTotalCollection);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDTotalCollection, value, NodeOrder);
            }
        }
        //Start SIW7036
        //<FINANCIAL_ACTIVITY ISFROZEN='True|False' ISONHOLD="True|False" ISPAYMENT='True|False' ISSCHEDULE='True|False'/> 'SIW7036
        public string IsFrozen
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsFrozen);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsFrozen, value, NodeOrder);
            }
        }
        public string IsOnHold
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsOnHold);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsOnHold, value, NodeOrder);
            }
        }
        public string IsPayment
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsPayment);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsPayment, value, NodeOrder);
            }
        }
        public string IsSchedule
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsSchedule);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsSchedule, value, NodeOrder);
            }
        }
        //End SIW7036
        //Start SIW7647
        public string ViewPayment
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdViewPayment);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdViewPayment, value, NodeOrder);
            }
        }
        //End SIW7647

        //Start SIW139
        public XmlNode putCauseOfLoss(string sDesc, string sCode)
        {
            return putCauseOfLoss(sDesc, sCode, "");
        }

        public XmlNode putCauseOfLoss(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDCauseOfLoss, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CauseOfLoss
        {
            get
            {
                return Utils.getData(Node, Constants.sLDCauseOfLoss);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDCauseOfLoss, value, NodeOrder);
            }
        }

        public string CauseOfLoss_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDCauseOfLoss);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDCauseOfLoss, value, NodeOrder);
            }
        }

        public string CauseOfLoss_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDCauseOfLoss);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDCauseOfLoss, value, NodeOrder);
            }
        }
        //End SIW139

        public string CauseOfLossBureau_Code
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sLDCauseOfLoss, Constants.sLDBureauCode);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sLDCauseOfLoss, Constants.sLDBureauCode, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putLossMember(string sDesc, string sCode)
        {
            return putLossMember(sDesc, sCode, "");
        }

        public XmlNode putLossMember(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDLossMember, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LossMember
        {
            get
            {
                return Utils.getData(Node, Constants.sLDLossMember);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDLossMember, value, NodeOrder);
            }
        }

        public string LossMember_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDLossMember);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDLossMember, value, NodeOrder);
            }
        }

        public string LossMember_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDLossMember);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDLossMember, value, NodeOrder);
            }
        }
        //End SIW139

        public XMLPolicy Policy
        {
            get
            {
                if (m_Policy == null)
                {
                    m_Policy = new XMLPolicy();
                    m_Policy.Parent = Parent;
                    m_Policy.getPolicybyID(PolicyID);
                }
                return m_Policy;
            }
        }

        public string PolicyID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(PolCovNode, Constants.sLDPolCovPolicyID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sLDPolCovPolIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                if (Node != null)
                    XML.XMLSetAttributeValue(putPolCovNode, Constants.sLDPolCovPolicyID, Constants.sLDPolCovPolIDPfx + value);
            }
        }

        public string PolicyCoverageLegacyKey
        {
            get
            {
                return XML.XMLGetAttributeValue(PolCovNode, Constants.sLDPolCovLegacyCovKey);
            }
            set
            {
                XML.XMLSetAttributeValue(putPolCovNode, Constants.sLDPolCovLegacyCovKey, value);
            }
        }

        public string CoverageEffDate
        {
            get
            {
                return Utils.getDate(PolCovNode, Constants.sLDPolCovEffDate);
            }
            set
            {
                Utils.putDate(putPolCovNode, Constants.sLDPolCovEffDate, value, sPolCovNodeOrder);
            }
        }
        // Start SIW169        
        public string CoverageTechkey
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sStdTechKey);//SIw169
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sStdTechKey, value, sPolCovNodeOrder);//SIW169
            }
        }
        // End SIW169
        //Start SIW139
        public XmlNode putCoverageInsLine(string sDesc, string sCode)
        {
            return putCoverageInsLine(sDesc, sCode, "");
        }

        public XmlNode putCoverageInsLine(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPolCovNode, Constants.sLDPolCovInsLine, sDesc, sCode, sPolCovNodeOrder, scodeid);
        }

        public string CoverageInsLine
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovInsLine);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovInsLine, value, sPolCovNodeOrder);
            }
        }

        public string CoverageInsLine_Code
        {
            get
            {
                return Utils.getCode(PolCovNode, Constants.sLDPolCovInsLine);
            }
            set
            {
                Utils.putCode(putPolCovNode, Constants.sLDPolCovInsLine, value, sPolCovNodeOrder);
            }
        }

        public string CoverageInsLine_CodeID
        {
            get
            {
                return Utils.getCodeID(PolCovNode, Constants.sLDPolCovInsLine);
            }
            set
            {
                Utils.putCodeID(putPolCovNode, Constants.sLDPolCovInsLine, value, sPolCovNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCoverageRiskUnit(string sDesc, string sCode)
        {
            return putCoverageRiskUnit(sDesc, sCode, "");
        }

        public XmlNode putCoverageRiskUnit(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPolCovNode, Constants.sLDPolCovRiskUnit, sDesc, sCode, sPolCovNodeOrder, scodeid);
        }

        public string CoverageRiskUnit
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovRiskUnit);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovRiskUnit, value, sPolCovNodeOrder);
            }
        }

        public string CoverageRiskUnit_Code
        {
            get
            {
                return Utils.getCode(PolCovNode, Constants.sLDPolCovRiskUnit);
            }
            set
            {
                Utils.putCode(putPolCovNode, Constants.sLDPolCovRiskUnit, value, sPolCovNodeOrder);
            }
        }

        public string CoverageRiskUnit_CodeID
        {
            get
            {
                return Utils.getCodeID(PolCovNode, Constants.sLDPolCovRiskUnit);
            }
            set
            {
                Utils.putCodeID(putPolCovNode, Constants.sLDPolCovRiskUnit, value, sPolCovNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCoveragePeril(string sDesc, string sCode)
        {
            return putCoveragePeril(sDesc, sCode, "");
        }

        public XmlNode putCoveragePeril(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPolCovNode, Constants.sLDPolCovPeril, sDesc, sCode, sPolCovNodeOrder, scodeid);
        }

        public string CoveragePeril
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovPeril);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovPeril, value, sPolCovNodeOrder);
            }
        }

        public string CoveragePeril_Code
        {
            get
            {
                return Utils.getCode(PolCovNode, Constants.sLDPolCovPeril);
            }
            set
            {
                Utils.putCode(putPolCovNode, Constants.sLDPolCovPeril, value, sPolCovNodeOrder);
            }
        }

        public string CoveragePeril_CodeID
        {
            get
            {
                return Utils.getCodeID(PolCovNode, Constants.sLDPolCovPeril);
            }
            set
            {
                Utils.putCodeID(putPolCovNode, Constants.sLDPolCovPeril, value, sPolCovNodeOrder);
            }
        }
        //End SIW139

        public string CoverageLocation
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovLocationNbr);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovLocationNbr, value, sPolCovNodeOrder);
            }
        }

        public string CoverageSubLocation
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovSubLocationNbr);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovSubLocationNbr, value, sPolCovNodeOrder);
            }
        }

        public string CoverageRiskUnitGrp
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovRiskUnitGrp);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovRiskUnitGrp, value, sPolCovNodeOrder);
            }
        }

        public string CoverageSeq
        {
            get
            {
                return Utils.getData(PolCovNode, Constants.sLDPolCovSeq);
            }
            set
            {
                Utils.putData(putPolCovNode, Constants.sLDPolCovSeq, value, sPolCovNodeOrder);
            }
        }

        public string ActivitySeq
        {
            get
            {
                return Utils.getData(FinTransNode, Constants.sLDFTActSequence);
            }
            set
            {
                Utils.putData(putFinTransNode, Constants.sLDFTActSequence, value, sFintransNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putActivityType(string sDesc, string sCode)
        {
            return putActivityType(sDesc, sCode, "");
        }

        public XmlNode putActivityType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putFinTransNode, Constants.sLDFTActType, sDesc, sCode, sFintransNodeOrder, scodeid);
        }

        public string ActivityType
        {
            get
            {
                return Utils.getData(FinTransNode, Constants.sLDFTActType);
            }
            set
            {
                Utils.putData(putFinTransNode, Constants.sLDFTActType, value, sFintransNodeOrder);
            }
        }

        public string ActivityType_Code
        {
            get
            {
                return Utils.getCode(FinTransNode, Constants.sLDFTActType);
            }
            set
            {
                Utils.putCode(putFinTransNode, Constants.sLDFTActType, value, sFintransNodeOrder);
            }
        }

        public string ActivityType_CodeID
        {
            get
            {
                return Utils.getCodeID(FinTransNode, Constants.sLDFTActType);
            }
            set
            {
                Utils.putCodeID(putFinTransNode, Constants.sLDFTActType, value, sFintransNodeOrder);
            }
        }
        //End SIW139

        public string AddedByUser
        {
            get
            {
                return Utils.getData(FinTransNode, Constants.sLDFTAddedByUser);
            }
            set
            {
                Utils.putData(putFinTransNode, Constants.sLDFTAddedByUser, value, sFintransNodeOrder);
            }
        }

        public string TransactionDate
        {
            get
            {
                return Utils.getDate(FinTransNode, Constants.sLDFTTransDate);
            }
            set
            {
                Utils.putDate(putFinTransNode, Constants.sLDFTTransDate, value, sFintransNodeOrder);
            }
        }

        public string TransactionTime
        {
            get
            {
                return Utils.getTime(FinTransNode, Constants.sLDFTTransTime);
            }
            set
            {
                Utils.putTime(putFinTransNode, Constants.sLDFTTransTime, value, sFintransNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putTransactionType(string sDesc, string sCode)
        {
            return putTransactionType(sDesc, sCode, "");
        }

        public XmlNode putTransactionType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putFinTransNode, Constants.sLDFTTransType, sDesc, sCode, sFintransNodeOrder, scodeid);
        }

        public string TransactionType
        {
            get
            {
                return Utils.getData(FinTransNode, Constants.sLDFTTransType);
            }
            set
            {
                Utils.putData(putFinTransNode, Constants.sLDFTTransType, value, sFintransNodeOrder);
            }
        }

        public string TransactionType_Code
        {
            get
            {
                return Utils.getCode(FinTransNode, Constants.sLDFTTransType);
            }
            set
            {
                Utils.putCode(putFinTransNode, Constants.sLDFTTransType, value, sFintransNodeOrder);
            }
        }

        public string TransactionType_CodeID
        {
            get
            {
                return Utils.getCodeID(FinTransNode, Constants.sLDFTTransType);
            }
            set
            {
                Utils.putCodeID(putFinTransNode, Constants.sLDFTTransType, value, sFintransNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putReserveStatus(string sDesc, string sCode)
        {
            return putReserveStatus(sDesc, sCode, "");
        }

        public XmlNode putReserveStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putFinTransNode, Constants.sLDFTRsvStatus, sDesc, sCode, sFintransNodeOrder, scodeid);
        }

        public string ReserveStatus
        {
            get
            {
                return Utils.getData(FinTransNode, Constants.sLDFTRsvStatus);
            }
            set
            {
                Utils.putData(putFinTransNode, Constants.sLDFTRsvStatus, value, sFintransNodeOrder);
            }
        }

        public string ReserveStatus_Code
        {
            get
            {
                return Utils.getCode(FinTransNode, Constants.sLDFTRsvStatus);
            }
            set
            {
                Utils.putCode(putFinTransNode, Constants.sLDFTRsvStatus, value, sFintransNodeOrder);
            }
        }

        public string ReserveStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(FinTransNode, Constants.sLDFTRsvStatus);
            }
            set
            {
                Utils.putCodeID(putFinTransNode, Constants.sLDFTRsvStatus, value, sFintransNodeOrder);
            }
        }
        //End SIW139
       
        public string ReserveBalance
        {
            get
            {
                return Utils.getData(RsvTransNode, Constants.sLDRTBalance);
            }
            set
            {
                Utils.putData(putRsvTransNode, Constants.sLDRTBalance, value, sRsvTransNodeOrder);
            }
        }

        public string ReserveChange
        {
            get
            {
                return Utils.getData(RsvTransNode, Constants.sLDRTChange);
            }
            set
            {
                Utils.putData(putRsvTransNode, Constants.sLDRTChange, value, sRsvTransNodeOrder);
            }
        }
        //SIN8386
        public string ReserveAmount
        {
            get
            {
                return Utils.getData(RsvTransNode, Constants.sLDRTAmount);
            }
            set
            {
                Utils.putData(putRsvTransNode, Constants.sLDRTAmount, value, sRsvTransNodeOrder);
            }
        }
        //SIN8386

        //Start SIW139
        public XmlNode putReserveChangeReason(string sDesc, string sCode)
        {
            return putReserveChangeReason(sDesc, sCode, "");
        }

        public XmlNode putReserveChangeReason(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putRsvTransNode, Constants.sLDRTChangeReason, sDesc, sCode, sRsvTransNodeOrder, scodeid);
        }

        public string ReserveChangeReason
        {
            get
            {
                return Utils.getData(RsvTransNode, Constants.sLDRTChangeReason);
            }
            set
            {
                Utils.putData(putRsvTransNode, Constants.sLDRTChangeReason, value, sRsvTransNodeOrder);
            }
        }

        public string ReserveChangeReason_Code
        {
            get
            {
                return Utils.getCode(RsvTransNode, Constants.sLDRTChangeReason);
            }
            set
            {
                Utils.putCode(putRsvTransNode, Constants.sLDRTChangeReason, value, sRsvTransNodeOrder);
            }
        }

        public string ReserveChangeReason_CodeID
        {
            get
            {
                return Utils.getCodeID(RsvTransNode, Constants.sLDRTChangeReason);
            }
            set
            {
                Utils.putCodeID(putRsvTransNode, Constants.sLDRTChangeReason, value, sRsvTransNodeOrder);
            }
        }
        //End SIW139

        public string ReserveEntryOperator
        {
            get
            {
                return Utils.getData(RsvTransNode, Constants.sLDRTEntryOperator);
            }
            set
            {
                Utils.putData(putRsvTransNode, Constants.sLDRTEntryOperator, value, sRsvTransNodeOrder);
            }
        }

        public string PayControlNumber
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTControlNumber);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTControlNumber, value, sPayTransNodeOrder);
            }
        }

        public string PayAmount
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTAmount);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTAmount, value, sPayTransNodeOrder);
            }
        }

        public string InvoiceAmount
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTInvAmount);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTInvAmount, value, sPayTransNodeOrder);
            }
        }

        public string InvoiceNumber
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTInvNbr);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTInvNbr, value, sPayTransNodeOrder);
            }
        }

        public string InvoiceBy
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTInvBy);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTInvBy, value, sPayTransNodeOrder);
            }
        }

        //Start SIW8393
        public string InvoiceDate
        {
            get
            {
                return Utils.getDate(PayTransNode, Constants.sLDPTInvDate);
            }
            set
            {
                Utils.putDate(putPayTransNode, Constants.sLDPTInvDate, value, sPayTransNodeOrder);
            }
        }
        //End SIW8393

        public string PONumber
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTPONbr);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTPONbr, value, sPayTransNodeOrder);
            }
        }

        public string ServiceFromDate
        {
            get
            {
                return Utils.getDate(PayTransNode, Constants.sLDPTSvcFromDate);
            }
            set
            {
                Utils.putDate(putPayTransNode, Constants.sLDPTSvcFromDate, value, sPayTransNodeOrder);
            }
        }

        public string ServiceToDate
        {
            get
            {
                return Utils.getDate(PayTransNode, Constants.sLDPTSvcToDate);
            }
            set
            {
                Utils.putDate(putPayTransNode, Constants.sLDPTSvcToDate, value, sPayTransNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putExpenseType(string sDesc, string sCode)
        {
            return putExpenseType(sDesc, sCode, "");
        }

        public XmlNode putExpenseType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPayTransNode, Constants.sLDPTExpType, sDesc, sCode, sPayTransNodeOrder, scodeid);
        }

        public string ExpenseType
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTExpType);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTExpType, value, sPayTransNodeOrder);
            }
        }

        public string ExpenseType_Code
        {
            get
            {
                return Utils.getCode(PayTransNode, Constants.sLDPTExpType);
            }
            set
            {
                Utils.putCode(putPayTransNode, Constants.sLDPTExpType, value, sPayTransNodeOrder);
            }
        }

        public string ExpenseType_CodeID
        {
            get
            {
                return Utils.getCodeID(PayTransNode, Constants.sLDPTExpType);
            }
            set
            {
                Utils.putCodeID(putPayTransNode, Constants.sLDPTExpType, value, sPayTransNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putReportableType(string sDesc, string sCode)
        {
            return putReportableType(sDesc, sCode, "");
        }

        public XmlNode putReportableType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPayTransNode, Constants.sLDPTReportableType, sDesc, sCode, sPayTransNodeOrder, scodeid);
        }

        public string ReportableType
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sLDPTReportableType);
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sLDPTReportableType, value, sPayTransNodeOrder);
            }
        }

        public string ReportableType_Code
        {
            get
            {
                return Utils.getCode(PayTransNode, Constants.sLDPTReportableType);
            }
            set
            {
                Utils.putCode(putPayTransNode, Constants.sLDPTReportableType, value, sPayTransNodeOrder);
            }
        }

        public string ReportableType_CodeID
        {
            get
            {
                return Utils.getCodeID(PayTransNode, Constants.sLDPTReportableType);
            }
            set
            {
                Utils.putCodeID(putPayTransNode, Constants.sLDPTReportableType, value, sPayTransNodeOrder);
            }
        }
        //End SIW139

        public string PaymentOffset
        {
            get
            {
                return Utils.getBool(PayTransNode, Constants.sLDPTPaymentOffsetInd, "");
            }
            set
            {
                Utils.putBool(putPayTransNode, Constants.sLDPTPaymentOffsetInd, "", value, sPayTransNodeOrder);
            }
        }

        public XMLVoucher Voucher
        {
            get
            {
                if (m_Voucher == null)
                {   //SIW529
                    m_Voucher = new XMLVoucher();
                    LinkXMLObjects(m_Voucher);  //SIW529                    
                }   //SIW529
                m_Voucher.getVoucherbyID(VoucherReference);
                return m_Voucher;
            }
        }

        public string VoucherReference
        {
            get
            {
                string strdata = Utils.getAttribute(PayTransNode, Constants.sLDPTVoucherReference, Constants.sLDPTVoucherRefID);
                if (StringUtils.Left(strdata, 3) == Constants.sVCHIDPfx && strdata.Length >= 4)
                    strdata = StringUtils.Mid(strdata, 4);
                return strdata;
            }
            set
            {
                Utils.putAttribute(putPayTransNode, Constants.sLDPTVoucherReference, Constants.sLDPTVoucherRefID,
                                   Constants.sVCHIDPfx + value, sPayTransNodeOrder);
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        private void subClearPointers()
        {
            m_xmlFinTrans = null;
            m_xmlFinTransList = null;
            m_Voucher = null;
            m_Policy = null;

            PartyInvolved = null;
            CommentReference = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();      //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sLDIDPfx;
            }
        }

        public string LossDetailID
        {
            get
            {
                return ID;
            }
            set
            {
                ID = value;
            }
        }

        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sLDID);
                string lid = "";

                if (strdata != "")
                    if (StringUtils.Left(strdata, 2) == Constants.sLDIDPfx && strdata.Length >= 3)
                        lid = StringUtils.Right(strdata, strdata.Length - 2);
                    else
                        lid = strdata;
                return lid;
            }
            set
            {
                string lid;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lLDId = Globals.lLDId + 1;
                    lid = Globals.lLDId + "";
                }
                else
                    lid = value;
                XML.XMLSetAttributeValue(putNode, Constants.sLDID, Constants.sLDIDPfx + lid);
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    m_Parent.LossDetail = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW529
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)
                    getFirstFinTrans();
            }
        }

        public void AddLossDetail()
        {
            InsertInDocument(null, null);
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getLossDetail(XmlDocument xdoc, bool reset)
        {
            return getLDNode(xdoc, reset);
        }

        // Start SIW490
        public XMLCoverage Coverage
        {
            get
            {
                // SIW529
                return m_Coverage;
            }
            set
            {
                m_Coverage = value;
            }
        }
        public XmlNode getCovLossDetail(bool reset) //SIW490
        {           
            // Start SIW529
            while(getNode(reset) != null)
            {
                if (PolicyCoverageLegacyKey == Coverage.LegacyKey && PolicyID == Coverage.PolicyID)
                    break;
                reset = false;
                //getNext();      //SI06650
            }
            return Node;
            // End SIW529
        }
        // End SIW490
        public XmlNode getLDNode(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return getNode(reset);
        }

        public XmlNode FindByID(string sID)
        {
            if (ID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (ID == sID)
                        break;
                    getNext();      //SI06650
                }
            }
            return Node;
        }

        public XmlNode PolCovNode
        {
            get
            {
                return Utils.getNode(Node, Constants.sLDPolCoverageNode);
            }
        }

        public XmlNode putPolCovNode
        {
            get
            {
                XmlNode xmlNode;
                xmlNode = PolCovNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sLDPolCoverageNode, sThisNodeOrder);
                return xmlNode;
            }
        }

        public XmlNode getFinTrans(bool reset)
        {
            //Start SIW7509
            //return getFinTransNode(reset);
            if (m_xmlFinTransList != null)
            {
                return getNode(reset, ref m_xmlFinTransList, ref m_xmlFinTrans);
                
            }
            else
            {
                return null;
            }
            //End SIW7509
        }

        public XmlNode getFinTransNode(bool reset)
        {
            if (reset)
            {
                m_xmlFinTransList = null;
            }
            if (FinTransList != null)
            {
                IEnumerator m_xmlFinTransListEnum = m_xmlFinTransList.GetEnumerator();
                if (!reset)
                {
                    m_xmlFinTransListEnum.MoveNext();
                    while (m_xmlFinTransListEnum.Current != FinTransNode && m_xmlFinTransListEnum.Current != null)
                        m_xmlFinTransListEnum.MoveNext();
                }
                if (m_xmlFinTransListEnum.MoveNext())
                    FinTransNode = (XmlNode)m_xmlFinTransListEnum.Current;
                else
                    FinTransNode = null;
                if (FinTransNode == null)
                {
                    m_xmlFinTransList = null;
                }
                else                          //SIW493
                    FinTransNode = null;      //SIW493
            }
            return FinTransNode;
        }

        public XmlNode getFirstFinTrans()
        {
            return getFinTransNode(true);
        }

        public XmlNode getNextFinTrans()
        {
            return getFinTransNode(false);
        }

        public XmlNode Remove()
        {
            if (Node != null)
            {
                Parent_Node.RemoveChild(Node);
                getFirst();
            }
            return Node;
        }

        public void RemoveFinTrans()
        {
            if (m_xmlFinTrans != null)
                FinTransNode.RemoveChild(m_xmlFinTrans);
            m_xmlFinTrans = null;
        }

        public int FinTransCount
        {
            get
            {
                return FinTransList.Count;
            }
        }

        public XmlNodeList FinTransList
        {
            get
            {
                return getNodeList(Constants.sLDFTNode, ref m_xmlFinTransList, Node);
            }
        }

        public XmlNode FinTransNode
        {
            get
            {
                if (m_xmlFinTrans == null)
                    FinTransNode = Utils.getNode(Node, Constants.sLDFTNode);   //SIW139
                return m_xmlFinTrans;
            }
            set
            {
                m_xmlFinTrans = value;
            }
        }

        public XmlNode putFinTransNode
        {
            get
            {
                if (m_xmlFinTrans == null)
                    FinTransNode = XML.XMLaddNode(putNode, Constants.sLDFTNode, sThisNodeOrder);
                return m_xmlFinTrans;
            }
        }



        public string ResPreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sRsvPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sRsvPreparedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string ResUpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sRsvUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sRsvUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string ResDttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sRsvDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sRsvDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string ResDttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sRsvDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sRsvDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }




        public string FtsPreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sFtsPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFtsPreparedByUser, value, sFintransNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string FtsUpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sFtsUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFtsUpdatedByUser, value, sFintransNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string FtsDttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sFtsDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFtsDttmUpdated, value, sFintransNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string FtsDttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sFtsDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sFtsDttmAdded, value, sFintransNodeOrder);    //SIW529 Remove local XML parameter
            }
        }





        public XmlNode addFinTrans(string sActSeq, string sActType, string sActTypeCode, string sAddedByUser, string sTransDate,
                                   string sTransType, string sTransTypeCode, string sRsvStatus, string sRsvStatusCode)
        {
            m_xmlFinTrans = null;

            if (sActSeq != "" && sActSeq != null)
                ActivitySeq = sActSeq;
            if ((sActType != "" && sActType != null) || (sActTypeCode != "" && sActTypeCode != null))
                putActivityType(sActType, sActTypeCode);
            if (sAddedByUser != "" && sAddedByUser != null)
                AddedByUser = sAddedByUser;
            if (sTransDate != "" && sTransDate != null)
                TransactionDate = sTransDate;
            if ((sTransType != "" && sTransType != null) || (sTransTypeCode != "" && sTransTypeCode != null))
                putTransactionType(sTransType, sTransTypeCode);
            if ((sRsvStatus != "" && sRsvStatus != null) || (sRsvStatusCode != "" && sRsvStatusCode != null))
                putReserveStatus(sRsvStatus, sRsvStatusCode);

            return m_xmlFinTrans;
        }

        public XmlNode PayTransNode
        {
            get
            {
                return Utils.getNode(FinTransNode, Constants.sLDPTNode);
            }
        }

        public XmlNode putPayTransNode
        {
            get
            {
                XmlNode xmlNode = PayTransNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putFinTransNode, Constants.sLDPTNode, sThisNodeOrder);
                return xmlNode;
            }
        }

        public XmlNode RsvTransNode
        {
            get
            {
                return Utils.getNode(FinTransNode, Constants.sLDRTNode);
            }
        }

        public XmlNode putRsvTransNode
        {
            get
            {
                XmlNode xmlNode = RsvTransNode;
                if (xmlNode == null)
                    //SIW7080 XML.XMLaddNode(putFinTransNode, Constants.sLDRTNode, sThisNodeOrder); 
                    xmlNode = XML.XMLaddNode(putFinTransNode, Constants.sLDRTNode, sThisNodeOrder); //SIW7080
                return xmlNode;
            }
        }

        public void RemovePayTrans()
        {
            XmlNode xmlNode = PayTransNode;
            if (xmlNode != null)
                FinTransNode.RemoveChild(xmlNode);
        }

        public void RemoveRsvTrans()
        {
            XmlNode xmlNode = RsvTransNode;
            if (xmlNode != null)
                FinTransNode.RemoveChild(xmlNode);
        }
        //Start SIW7509
        public string PayTransTechkey
        {
            get
            {
                return Utils.getData(PayTransNode, Constants.sStdTechKey); 
            }
            set
            {
                Utils.putData(putPayTransNode, Constants.sStdTechKey, value, sPayTransNodeOrder); 
            }
        }
        public string IsFinal
        {
           
            get
            {
                return Utils.getBool(PayTransNode, Constants.sLDPTIsFinal, ""); 
            }
            set
            {
                Utils.putBool(putPayTransNode, Constants.sLDPTIsFinal, "", value, sPayTransNodeOrder);    
            }
        }
        //End SIW7509
        //Start SIW7537
        public string IsFirstFinal
        {

            get
            {
                return Utils.getBool(PayTransNode, Constants.sLDPTIsFirstFinal, "");
            }
            set
            {
                Utils.putBool(putPayTransNode, Constants.sLDPTIsFirstFinal, "", value, sPayTransNodeOrder);
            }
        }

        public string IsSupplemental
        {

            get
            {
                return Utils.getBool(PayTransNode, Constants.sLDPTIsSupplemental, "");
            }
            set
            {
                Utils.putBool(putPayTransNode, Constants.sLDPTIsSupplemental, "", value, sPayTransNodeOrder);
            }
        }
        //End SIW7537
        //Payal starts
        public string TotalUnitNumber 
        {
            get
            {
                return Utils.getData(Node, Constants.sLDUnitNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDUnitNumber, value, NodeOrder);
            }
        }

        public XmlNode putRiskType(string sDesc, string sCode)
        {
            return putRiskType(sDesc, sCode, "");
        }

        public XmlNode putRiskType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLDRiskType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string RiskType
        {
            get
            {
                return Utils.getData(Node, Constants.sLDRiskType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLDRiskType, value, NodeOrder);
            }
        }

        public string RiskType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLDRiskType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLDRiskType, value, NodeOrder);
            }
        }

        public string RiskType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLDRiskType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLDRiskType, value, NodeOrder);
            }
        }
       
        
        //Payal Ends
    }
}
