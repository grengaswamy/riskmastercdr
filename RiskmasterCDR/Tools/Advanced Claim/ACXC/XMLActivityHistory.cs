/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *  09/17/2011 | SIW7368 |    ND      |  Scheduled activity added to AC Web
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLActivityHistory : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<parent>
        '  <ACTIVITY_HISTORY LINE_NUMBER="x" PREDECESSOR_LINE_NUMBER="x" ITEM_TYPE="ACTIVITY | STATUS">
        '     <ACTIVITY_TYPE CODE="xx">Activity</ACTIVITY_CODE>
        '     <DESCRIPTION>description</DESCRIPTION>
        '     <STATUS CODE="xx">status</STATUS>
        '     <DATE_CREATED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_DUE YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_SCHEDULED_START YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_COMPLETE YEAR="2002" MONTH="03" DAY="07" />
        '     <NOTES>ActivityNotes</NOTES>                                            SI04744
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '      <OWNER>XX</OWNER>                                                        SIW7368
        '  </ACTIVITY_HISTORY>
        '</parent>*/

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLVarDataItem m_DataItem;

        private Dictionary<object, object> dctAHType;

        public XMLActivityHistory()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(11); //SIW7368
            sThisNodeOrder = new ArrayList(12); // SIW7368
            sThisNodeOrder.Add(Constants.sAHActivityType);
            sThisNodeOrder.Add(Constants.sAHDescription);
            sThisNodeOrder.Add(Constants.sAHStatus);
            sThisNodeOrder.Add(Constants.sAHDateCreated);
            sThisNodeOrder.Add(Constants.sAHDateDue);
            sThisNodeOrder.Add(Constants.sAHDateSchedStart);
            sThisNodeOrder.Add(Constants.sAHDateComplete);
            sThisNodeOrder.Add(Constants.sAHNotes);
            sThisNodeOrder.Add(Constants.sAHPartyInvolved);
            sThisNodeOrder.Add(Constants.sAHCommentReference);
            sThisNodeOrder.Add(Constants.sAHDataItem);
            sThisNodeOrder.Add(Constants.sAHOwner); //SIW7368

            dctAHType = new Dictionary<object, object>();
            dctAHType.Add(ahType.ahTypeUndefined, Constants.sUndefined);
            dctAHType.Add(Constants.sUndefined, ahType.ahTypeUndefined);
            dctAHType.Add(ahType.ahTypeActivity, Constants.sAHItemTypeActivity);
            dctAHType.Add(Constants.sAHItemTypeActivity, ahType.ahTypeActivity);
            dctAHType.Add(ahType.ahTypeStatus, Constants.sAHItemTypeStatus);
            dctAHType.Add(Constants.sAHItemTypeStatus, ahType.ahTypeStatus);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sAHNode; }
        }

        public override XmlNode Create()
        {
            return Create(ahType.ahTypeUndefined, "", "", "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(ahType sType, string sLineNbr, string sPredLineNbr, string sActivity, string sActivityCode,
                              string sCreateDate, string sDueDate, string sSchedStartDate, string sCompleteDate,
                              string sStatus, string sStatusCode, string sdescription, string sNotes)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ItemType = sType;
                LineNumber = sLineNbr;
                PredecesorLineNumber = sPredLineNbr;
                putActivityType(sActivity, sActivityCode);
                DateCreated = sCreateDate;
                DateDue = sDueDate;
                DateScheduledStart = sSchedStartDate;
                DateComplete = sCompleteDate;
                putStatus(sStatus, sStatusCode);
                Description = sdescription;
                Notes = sNotes;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLActivityHistory.Create");
                return null;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLLitigation();
                    ((XMLLitigation)m_Parent).ActivityHistory = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DataItem = null;
        }

        public string LineNumber
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sAHLineNbr);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sAHLineNbr, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string PredecesorLineNumber
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sAHPredLineNbr);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sAHPredLineNbr, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public ahType ItemTypeTranslate(string intype)
        {
            object ret;
            if (!dctAHType.TryGetValue(intype, out ret))
                ret = ahType.ahTypeUndefined;
            return (ahType)ret;
        }

        public string ItemTypeTranslate(ahType intype)
        {
            object ret;
            if (!dctAHType.TryGetValue((int)intype, out ret))
                ret = "";
            return (string)ret;
        }

        public string ItemType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sAHItemType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sAHItemType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public ahType ItemType
        {
            get
            {
                string sdata = ItemType_Code;
                object etype;
                if (!dctAHType.TryGetValue(sdata, out etype))
                    etype = ahType.ahTypeUndefined;
                return (ahType)etype;
            }
            set
            {
                object sdata; //string;
                dctAHType.TryGetValue((int)value, out sdata);
                ItemType_Code = (string)sdata;
            }
        }

        //Start SIW139
        public XmlNode putActivityType(string sDesc, string sCode)
        {
            return putActivityType(sDesc, sCode, "");
        }

        public XmlNode putActivityType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAHActivityType, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string ActivityType
        {
            get
            {
                return Utils.getData(Node, Constants.sAHActivityType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAHActivityType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ActivityType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAHActivityType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sAHActivityType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ActivityType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAHActivityType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAHActivityType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sAHDescription);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAHDescription, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Owner // SIW7368
        {
            get
            {
                return Utils.getData(Node, Constants.sAHOwner);
            }
            set
            {
                Utils.putData(putNode, Constants.sAHOwner, value, NodeOrder);
            }
        }
        public string Notes
        {
            get
            {
                return Utils.getData(Node, Constants.sAHNotes);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAHNotes, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //Start (SIW7368)
        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sAHID);
                string sID = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, Constants.sAHIDPfx.Length) == Constants.sAHIDPfx && strdata.Length >= (Constants.sAHIDPfx.Length + 1))
                    {
                        sID = StringUtils.Right(strdata, strdata.Length - Constants.sAHIDPfx.Length);
                    }
                return sID;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lDOId = Globals.lDOId + 1;
                    lid = Globals.lDOId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sAHID, Constants.sAHIDPfx + (lid + ""));
            }
        }
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); 
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }//End (SIW7368)

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }
        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAHStatus, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sAHStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sAHStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAHStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sAHStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAHStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAHStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string DateCreated
        {
            get
            {
                return Utils.getDate(Node, Constants.sAHDateCreated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAHDateCreated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateDue
        {
            get
            {
                return Utils.getDate(Node, Constants.sAHDateDue);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAHDateDue, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateScheduledStart
        {
            get
            {
                return Utils.getDate(Node, Constants.sAHDateSchedStart);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAHDateSchedStart, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateComplete
        {
            get
            {
                return Utils.getDate(Node, Constants.sAHDateComplete);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sAHDateComplete, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getActivityHistory(bool reset)
        {
            return getNode(reset);
        }
    }
}
