/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public interface XMLXCIntSItem
    {
        //Simple interface to be implemented in classes that are parent nodes to XMLSchedItem

        XMLSchedItem ScheduledItem
        {
            get;
            set;
        }
    }
}
