/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLInvoice : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '   <INVOICES INVOICE_COUNT="invoicecount">
        '     <TOTAL_BILLED>Billed Amount</TOTAL_BILLED>
        '     <TOTAL_REDUCED>Total reductions</TOTAL_REDUCED>
        '     <TOTAL_ALLOWED>Total Allowed</TOTAL_ALLOWED>
        '     <TOTAL_FEES>total fees</TOTAL_FEES>
        '   </INVOICES>
        '   <INVOICE ID="INVxx" LINE_COUNT="line count">
        '        <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <BILLED>Billed Amount</BILLED>
        '      <REDUCED>Total invoice reduction</REDUCED>
        '      <ALLOWED>Total invoice allowed</ALLOWED>
        '      <FEES>fees</FEES>
        '      <NUMBER>BER.6</NUMBER>
        '      <PO_NUMBER>NA</PO_NUMBER>
        '      <REFERENCE>BER.5</REFERENCE>
        '      <INVOICE_DATE YEAR="" MONTH="" DAY="">BER.53</DATE>
        '      <DUE_DATE YEAR="" MONTH="" DAY="">BER.58</DUE_DATE>
        '      <REVIEW_DATE YEAR="" MONTH="" DAY="">BER.56</REVIEW_DATE>
        '      <DATE_RECEIVED YEAR="year" MONTH="month" DAY="day">BER.53</DATE_RECEIVED>
        '      <FROM_DATE YEAR="" MONTH="" DAY="">
        '      <TO_DATE YEAR="" MONTH="" DAY="">
        '      <RELATED_DOCUMENTATION>                                  'SI05215
        '          <TYPE CODE="">documenttype</TYPE>                    'SI05215
        '          <FILENAME>filename</FILENAME>                        'SI05215
        '          <DESCRIPTION>description</DESCRIPTION>               'SI05215
        '      </RELATED_DOCUMENTATION>
        '          <! -- Multiple entries allowed -->
        '      <PAYMENT_ASSIGNMENT PAY_CODE="BER.59,61 : P=Pay, H=Hold, D=Deny, R=Review">
        '          <TYPE_OF_PAYMENT CODE="TOPCode:  Expense, Medical, etc.  Tied to receiving
        '            System ">Type of Payment</TYPE_OF_PAYMENT>"
        '          <RESERVE CODE="ReserveCode:  Defined in Setup.  Need specfic one.">Reserve to
        '            be Assigned</RESERVE>
        '          <PAY_TRANSACTION_TYPE CODE="transtypecode">transactiontype</PAY_TRANSACTION_TYPE>
        '      </PAYMENT_ASSIGNMENT>
        '      <PARTY_INVOLVED ENTITY_ID="EntityID: References Entity Below"
        '      <ADDRESS_REFERENCE ADDRESS_ID="addressID" RELATION_CODE="Relationship Code">Type of</ADDRESS>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <FEE>
        '         <PAYMENT_ASSIGNMENT PAY_CODE=" P=Pay, H=Hold, D=Deny, R=Review">
        '            <TYPE_OF_PAYMENT CODE="TOPCode:  Expense, Medical, etc.  Tied to
        '             receiving system">Type of Payment</TYPE_OF_PAYMENT>
        '            <RESERVE CODE="ReserveCode:  Defined in Setup.  Need specific
        '             one.">Reserve to be Assigned</RESERVE>
        '         </PAYMENT_ASSIGNMENT>
        '         <PAYEE ENTITY_ID="ENTid:  Identify Payee for the fee.  References an Entity
        '          below" CODE="BER.14, 20, 25, 30, 35:  Payee Code">Type of Payee / Fee
        '          (RVWR, PPN, PPAY, HOSPRVW, UR)</PAYEE>
        '         <AMOUNT>BER.16, 22, 27, 32, 37    : Fee</AMOUNT>
        '         <CONTROL_NUMBER>BER.17, 23, 28, 33, 38</CONTROL_NUMBER>
        '      </FEE>
        '      <DIAGNOSIS>
        '         <SPECIFIC CODE="ICD-9 Code {string} [ICD9]">Diagnosis Translated {string}</SPECIFIC>
        '         <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      </DIAGNOSIS>
        '          <!-- Multiple Diagonsis Codes Allowed -->
        '      <EOB CODE="BER.62,63 remark code">
        '          <SHORT_REMARK>RMCR.5</SHORT_REMARK>
        '          <LONG_REMARK>RMCR.6 + RMCR.7</LONG_REMARK>
        '      </EOB>
        '      <STATE_ADJUDICATED>
        '          <STATE CODE="">statename</STATE>
        '          <DISCLAIMER>SD.4 : All Sequences Combined for Single Disclaimer Line</DISCLAIMER>
        '      </STATE_ADJUDICATED>
        '      <PROCESS_ERROR CODE="Error Code">Error message</PROCESS_ERROR>
        '         <!-- Multiple lines allowed -->
        '      <INVOICE_DETAILS>
        '        <DETAIL_LINE>
        '           <PLACE_OF_SVC CODE="LIR.2">Translated</PLACE_OF_SVC>
        '           <TYPE_OF_SVC CODE="LIR.7">Translated</TYPE_OF_SVC>
        '           <SVC_START_DATE YEAR="year" MONTH="month" DAY="day">LIR.4</START_DATE>
        '           <SVC_END_DATE YEAR="year" MONTH="month" DAY="day">LIR.5</END_DATE>
        '           <UNITS TYPE="Type of Unit (Defaults to Each)">unitsbilled</UNITS>
        '           <BILLED>LIR.15 billed amount</BILLED>
        '           <DISALLOWED>LIR.16 disallowed by review</DISALLOWED>
        '           <HOSP_DISALLOWED>LIR.17 hospital disallowed</HOSP_DISALLOWED>
        '           <UR_DISALLOWED>LIR.18 UR disallowed</UR_DISALLOWED>
        '           <DISCOUNT>LIR.19 Negotiated Discount</DISCOUNT>
        '           <PPNREDUCED>LIR.20 PPN Reduction</PPNREDUCED>
        '           <PAYED>LIR.21 (Previously Paid Amounts)</PAYED>
        '           <ALLOWED>BILLED - DISALLOWED - HOSP_DISALLOWED - UR_DISALLOWED - DISCOUNT -
        '            PPNREDUCED - PAYED</ALLOWED>
        '           <SAVED>DISALLOWED + HOSP_DISALLOWED + UR_DISALLOWED + DISCOUNT + PPNREDUCED</SAVED>
        '           <CONTRACT>
        '              <NUMBER>contractnumber</NUMBER>
        '              <DISCOUNT TYPE="discounttype">discountamount</DISCOUNT>
        '           </CONTRACT>
        '           <EOB CODE="LIR.22,23,24 eobcode">
        '              <!--Comments retrieved from RMCR record where PMCR.2="L" and PMCR.4 = CODE-->
        '              <!--Multiple Remarks allowed-->
        '              <SHORT_REMARK>RMCR.5</SHORT_REMARK>
        '              <LONG_REMARK>RMCR.6 + RMCR.7</LONG_REMARK>
        '           </EOB>
        '           <PROCEDURE>
        '              <USE>use</USE>
        '              <STATE CODE="statecode">state</STATE>
        '              <SPECIFIED CODE="ccc">PCR.5</SPECIFIED>
        '              <TYPE CODE="code">type</TYPE>
        '              <TOOTH>number</TOOTH>
        '           </PROCEDURE>
        '           <DIAGNOIS>number 1 through 5, ordinal to diagnosees listed above</DIAGNOSIS>
        '              <!-- Multiple Diagnosees -->
        '        </DETAIL_LINE>        
        '      </INVOICE_DETAILS>
        '   </INVOICE>
        '</CLAIM>*/

        private XmlNode m_xmlSummary;
        private XmlNode xmlDiagnosis;
        private XmlNodeList xmlDiagnosisList;
        //private IEnumerator xmlDiagnosisListEnum;
        private XmlNode xmlFee;
        private XmlNodeList xmlFeeList;
        //private IEnumerator xmlFeeListEnum;
        private XmlNode xmlProcessError;
        private XmlNodeList xmlProcessErrorList;
        //private IEnumerator xmlProcessErrorListEnum;
        private XmlNode xmlRelatedDoc;
        private XmlNodeList xmlRelatedDocList;
        //private IEnumerator xmlRelatedDocListEnum;
        private XmlNode xmlDetail;
        private XmlNodeList xmlDetailList;
        //private IEnumerator xmlDetailListEnum;
        private XmlNode xmlEOB;
        private XmlNodeList xmlEOBList;
        //private IEnumerator xmlEOBListEnum;
        private XmlNode xmlDetailEOB;
        private XmlNodeList xmlDetailEOBList;
        //private IEnumerator xmlDetailEOBListEnum;
        private XmlNode xmlDetailDiag;
        private XmlNodeList xmlDetailDiagList;
        //private IEnumerator xmlDetailDiagListEnum;
        private XmlNode xmlProcedure;
        private XmlNodeList xmlProcedureList;
        //private IEnumerator xmlProcedureListEnum;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLAddressReference m_AddressReference;
        private XMLCommentReference m_Comment;
        private XMLClaim m_Claim;
        private ArrayList sInvSumNodeOrder;
        private ArrayList sDocNodeOrder;
        private ArrayList sPayNodeOrder;
        private ArrayList sDtlNodeOrder;
        private ArrayList sFeeNodeOrder;
        private ArrayList sDiagNodeOrder;
        private ArrayList sProcNodeOrder;
        private ArrayList sParentNodeOrder;
        private ArrayList sEOBNodeOrder;
        private ArrayList sStANodeOrder;
        private ArrayList sContNodeOrder;
        
        public XMLInvoice()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_xmlSummary = null;

            sInvSumNodeOrder = new ArrayList(4);
            sInvSumNodeOrder.Add(Constants.sInvTotalBilled);
            sInvSumNodeOrder.Add(Constants.sInvTotalReduced);
            sInvSumNodeOrder.Add(Constants.sInvTotalAllowed);
            sInvSumNodeOrder.Add(Constants.sInvTotalFees);

            //sThisNodeOrder = new ArrayList(24);               //(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(25);                 //(SI06023 - Implemented in SI06333)
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sInvBilled);
            sThisNodeOrder.Add(Constants.sInvReduced);
            sThisNodeOrder.Add(Constants.sInvAllowed);
            sThisNodeOrder.Add(Constants.sInvFees);
            sThisNodeOrder.Add(Constants.sInvNumber);
            sThisNodeOrder.Add(Constants.sInvPONumber);
            sThisNodeOrder.Add(Constants.sInvReference);
            sThisNodeOrder.Add(Constants.sInvDate);
            sThisNodeOrder.Add(Constants.sInvDueDate);
            sThisNodeOrder.Add(Constants.sInvReviewDate);
            sThisNodeOrder.Add(Constants.sInvReceivedDate);
            sThisNodeOrder.Add(Constants.sInvFromDate);
            sThisNodeOrder.Add(Constants.sInvToDate);
            sThisNodeOrder.Add(Constants.sInvRltdDocument);
            sThisNodeOrder.Add(Constants.sInvPayAssignment);
            sThisNodeOrder.Add(Constants.sInvPartyInvolved);
            sThisNodeOrder.Add(Constants.sInvAddressRef);
            sThisNodeOrder.Add(Constants.sInvCommentRef);
            sThisNodeOrder.Add(Constants.sInvFee);
            sThisNodeOrder.Add(Constants.sInvDiagnosis);
            sThisNodeOrder.Add(Constants.sInvEOB);
            sThisNodeOrder.Add(Constants.sInvStateAdjudicated);
            sThisNodeOrder.Add(Constants.sInvProcessError);
            sThisNodeOrder.Add(Constants.sInvDetails);
            //sThisNodeOrder.Add(Constants.sInvTechKey);          //(SI06023 - Implemented in SI06333) //SIW360

            sDocNodeOrder = new ArrayList(3);
            sDocNodeOrder.Add(Constants.sInvRltdDocType);
            sDocNodeOrder.Add(Constants.sInvRltdDocFileName);
            sDocNodeOrder.Add(Constants.sInvRltdDocDescription);

            sPayNodeOrder = new ArrayList(4);
            sPayNodeOrder.Add(Constants.sInvPayAssignmentCode);
            sPayNodeOrder.Add(Constants.sInvPayAssignmentTypeofPayment);
            sPayNodeOrder.Add(Constants.sInvPayAssignmentReserve);
            sPayNodeOrder.Add(Constants.sInvPayAssignmentTransactionType);

            sFeeNodeOrder = new ArrayList(4);
            sFeeNodeOrder.Add(Constants.sInvPayAssignment);
            sFeeNodeOrder.Add(Constants.sInvFeePayee);
            sFeeNodeOrder.Add(Constants.sInvFeeAmount);
            sFeeNodeOrder.Add(Constants.sInvFeeControlNumber);

            sDiagNodeOrder = new ArrayList(2);
            sDiagNodeOrder.Add(Constants.sInvDiagSpecific);
            sDiagNodeOrder.Add(Constants.sInvDiagDate);

            sEOBNodeOrder = new ArrayList(2);
            sEOBNodeOrder.Add(Constants.sInvEOBShortRemark);
            sEOBNodeOrder.Add(Constants.sInvEOBLongRemark);

            sStANodeOrder = new ArrayList(2);
            sStANodeOrder.Add(Constants.sInvStateAdjudicatedState);
            sStANodeOrder.Add(Constants.sInvStateAdjudicatedDisclaimer);

            sDtlNodeOrder = new ArrayList(18);
            sDtlNodeOrder.Add(Constants.sInvDetailPlaceOfSvc);
            sDtlNodeOrder.Add(Constants.sInvDetailTypeOfSvc);
            sDtlNodeOrder.Add(Constants.sInvDetailSvcStartDate);
            sDtlNodeOrder.Add(Constants.sInvDetailSvcEndDate);
            sDtlNodeOrder.Add(Constants.sInvDetailUnits);
            sDtlNodeOrder.Add(Constants.sInvDetailBilled);
            sDtlNodeOrder.Add(Constants.sInvDetailDisallowed);
            sDtlNodeOrder.Add(Constants.sInvDetailHospDisallowed);
            sDtlNodeOrder.Add(Constants.sInvDetailURDisallowed);
            sDtlNodeOrder.Add(Constants.sInvDetailDiscount);
            sDtlNodeOrder.Add(Constants.sInvDetailPPNReduced);
            sDtlNodeOrder.Add(Constants.sInvDetailPayed);
            sDtlNodeOrder.Add(Constants.sInvDetailAllowed);
            sDtlNodeOrder.Add(Constants.sInvDetailSaved);
            sDtlNodeOrder.Add(Constants.sInvDetailContract);
            sDtlNodeOrder.Add(Constants.sInvEOB);
            sDtlNodeOrder.Add(Constants.sInvDetailProcedure);
            sDtlNodeOrder.Add(Constants.sInvDetailDiagnosis);

            sContNodeOrder = new ArrayList(2);
            sContNodeOrder.Add(Constants.sInvDetailContractNumber);
            sContNodeOrder.Add(Constants.sInvDetailContractDiscType);

            sProcNodeOrder = new ArrayList(5);
            sProcNodeOrder.Add(Constants.sInvDetailProcedureSpecified);
            sProcNodeOrder.Add(Constants.sInvDetailProcedureType);
            sProcNodeOrder.Add(Constants.sInvDetailProcedureState);
            sProcNodeOrder.Add(Constants.sInvDetailProcedureUse);
            sProcNodeOrder.Add(Constants.sInvDetailProcedureTooth);

            //SIW485 sParentNodeOrder = Globals.sNodeOrder;
            sParentNodeOrder = Utils.sNodeOrder;	//SIW485
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sInvoiceNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sInvoiceId, string sBilled, string sReduced, string sAllowed, string sFees,
                              string sPONumber, string sReference, string sInvDate, string sDueDate,
                              string sRvwDate, string sRcvDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                InvoiceID = sInvoiceId;
                InvoiceBilledAmt = sBilled;
                InvoiceReducedAmt = sReduced;
                InvoiceAllowedAmt = sAllowed;
                InvoiceFeesAmt = sFees;
                PONumber = sPONumber;
                Reference = sReference;
                InvoiceDate = sInvDate;
                DueDate = sDueDate;
                ReviewDate = sRvwDate;
                ReceivedDate = sRcvDate;

                LineCount = "0";

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLInvoice.Create");
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sInvoiceIDPFX;
            }
        }

        public string InvoiceID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sInvoiceID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sInvoiceIDPFX && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lInvId = Globals.lInvId + 1;
                    lid = Globals.lInvId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sInvoiceID, Constants.sInvoiceIDPFX + (lid + ""));
            }
        }

        public string LineCount
        {
            get
            {
                if (xmlThisNode != null)
                    return XML.XMLGetAttributeValue(Node, Constants.sInvLineCount);
                else
                    return "";
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sInvLineCount, value);
            }
        }

        public string InvoiceBilledAmt
        {
            get
            {
                return Utils.getData(Node, Constants.sInvBilled);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvBilled, value, NodeOrder);
            }
        }

        public string InvoiceReducedAmt
        {
            get
            {
                return Utils.getData(Node, Constants.sInvReduced);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvReduced, value, NodeOrder);
            }
        }

        public string InvoiceAllowedAmt
        {
            get
            {
                return Utils.getData(Node, Constants.sInvAllowed);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvAllowed, value, NodeOrder);
            }
        }

        public string InvoiceFeesAmt
        {
            get
            {
                return Utils.getData(Node, Constants.sInvFees);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvFees, value, NodeOrder);
            }
        }

        public string Number
        {
            get
            {
                return Utils.getData(Node, Constants.sInvNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvNumber, value, NodeOrder);
            }
        }

        public string PONumber
        {
            get
            {
                return Utils.getData(Node, Constants.sInvPONumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvPONumber, value, NodeOrder);
            }
        }

        public string Reference
        {
            get
            {
                return Utils.getData(Node, Constants.sInvReference);
            }
            set
            {
                Utils.putData(putNode, Constants.sInvReference, value, NodeOrder);
            }
        }

        public string InvoiceDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvDate, value, NodeOrder);
            }
        }

        public string DueDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvDueDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvDueDate, value, NodeOrder);
            }
        }

        public string ReviewDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvReviewDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvReviewDate, value, NodeOrder);
            }
        }

        public string ReceivedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvReceivedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvReceivedDate, value, NodeOrder);
            }
        }

        public string FromDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvFromDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvFromDate, value, NodeOrder);
            }
        }

        public string ToDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInvToDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInvToDate, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public XmlNode addProcessErrorNode()
        {
            xmlProcessError = XML.XMLaddNode(putNode, Constants.sInvProcessError, NodeOrder);
            return xmlProcessError;
        }

        public XmlNode ProcessErrorNode
        {
            get
            {
                if (xmlProcessError == null)
                    addProcessErrorNode();
                return xmlProcessError;
            }
        }

        public XmlNode putProcessError(string sDesc, string sCode)
        {
            XML.XMLInsertCode_Node(addProcessErrorNode(), sDesc, sCode);
            return xmlProcessError;
        }

        public XmlNodeList ProcessErrorList
        {
            get
            {
                // Start SIW163
                if (null == xmlProcessErrorList)
                {
                    return getNodeList(Constants.sInvProcessError, ref xmlProcessErrorList, Node);
                }
                else
                {
                    return xmlProcessErrorList;
                }
                // End SIW163
            }
        }

        public XmlNode getProcessError(bool reset)
        {
            //Start SIW163
            if (null != ProcessErrorList)
            {
                return getNode(reset, ref xmlProcessErrorList, ref xmlProcessError);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public string ProcessError
        {
            get
            {
                if (xmlProcessError == null)
                    return "";
                else
                    return xmlProcessError.InnerText;
            }
            set
            {
                ProcessErrorNode.InnerText = value;
            }
        }

        public string ProcessError_Code
        {
            get
            {
                if (xmlProcessError == null)
                    return "";
                else
                    return XML.XMLExtractCodeAttribute(xmlProcessError);
            }
            set
            {
                XML.XMLSetAttributeValue(ProcessErrorNode, Constants.sdtCode, value);
            }
        }

        public XmlNode addRelatedDocumentNode()
        {
            xmlRelatedDoc = XML.XMLaddNode(putNode, Constants.sInvRltdDocument, NodeOrder);
            return xmlRelatedDoc;
        }

        public XmlNode RelatedDocumentNode
        {
            get
            {
                if (xmlRelatedDoc == null)
                    addRelatedDocumentNode();
                return xmlRelatedDoc;
            }
        }

        public XmlNode putRelatedDocument(string sDesc, string sCode)
        {
            addRelatedDocumentNode();
            putRelatedDocumentType(sDesc, sCode);
            return xmlRelatedDoc;
        }

        public XmlNodeList RelatedDocumentList
        {
            get
            {
                // Start SIW163
                if (null == xmlRelatedDocList)
                {
                    return getNodeList(Constants.sInvRltdDocument, ref xmlRelatedDocList, Node);
                }
                else
                {
                    return xmlRelatedDocList;
                }
                // End SIW163
            }
        }

        public XmlNode getRelatedDocument(bool reset)
        {
            // Start SIW163
            if (null != RelatedDocumentList)
            {
                return getNode(reset, ref xmlRelatedDocList, ref xmlRelatedDoc);
            }
            else
            {
                return null;
            }
            // End SIW163
        }

        public string RelatedDocumentDescription
        {
            get
            {
                return Utils.getData(xmlRelatedDoc, Constants.sInvRltdDocDescription);
            }
            set
            {
                Utils.putData(RelatedDocumentNode, Constants.sInvRltdDocDescription, value, sDocNodeOrder);
            }
        }

        public string RelatedDocumentFilename
        {
            get
            {
                return Utils.getData(xmlRelatedDoc, Constants.sInvRltdDocFileName);
            }
            set
            {
                Utils.putData(RelatedDocumentNode, Constants.sInvRltdDocFileName, value, sDocNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putRelatedDocumentType(string sDesc, string sCode)
        {
            return putRelatedDocumentType(sDesc, sCode, "");
        }

        public XmlNode putRelatedDocumentType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(RelatedDocumentNode, Constants.sInvRltdDocType, sDesc, sCode, sDocNodeOrder, scodeid);
        }

        public string RelatedDocumentType
        {
            get
            {
                return Utils.getData(xmlRelatedDoc, Constants.sInvRltdDocType);
            }
            set
            {
                Utils.putData(RelatedDocumentNode, Constants.sInvRltdDocType, value, sDocNodeOrder);
            }
        }

        public string RelatedDocumentType_Code
        {
            get
            {
                return Utils.getCode(xmlRelatedDoc, Constants.sInvRltdDocType);
            }
            set
            {
                Utils.putCode(RelatedDocumentNode, Constants.sInvRltdDocType, value, sDocNodeOrder);
            }
        }

        public string RelatedDocumentType_CodeID
        {
            get
            {
                return Utils.getCodeID(xmlRelatedDoc, Constants.sInvRltdDocType);
            }
            set
            {
                Utils.putCodeID(RelatedDocumentNode, Constants.sInvRltdDocType, value, sDocNodeOrder);
            }
        }
        //End SIW139

        public XmlNode StateAdjudicatedNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.sInvStateAdjudicated);
            }
        }

        public XmlNode putStateAdjudicatedNode
        {
            get
            {
                XmlNode xmlNode = StateAdjudicatedNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sInvStateAdjudicated, NodeOrder);
                return xmlNode;
            }
        }

        public string StateAdjudicatedDisclaimer
        {
            get
            {
                return Utils.getData(StateAdjudicatedNode, Constants.sInvStateAdjudicatedDisclaimer);
            }
            set
            {
                Utils.putData(putStateAdjudicatedNode, Constants.sInvStateAdjudicatedDisclaimer, value, sStANodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putStateAdjudicatedState(string sDesc, string sCode)
        {
            return putStateAdjudicatedState(sDesc, sCode, "");
        }

        public XmlNode putStateAdjudicatedState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putStateAdjudicatedNode, Constants.sInvStateAdjudicatedState, sDesc, sCode, sStANodeOrder, scodeid);
        }

        public string StateAdjudicatedState
        {
            get
            {
                return Utils.getData(StateAdjudicatedNode, Constants.sInvStateAdjudicatedState);
            }
            set
            {
                Utils.putData(putStateAdjudicatedNode, Constants.sInvStateAdjudicatedState, value, sStANodeOrder);
            }
        }

        public string StateAdjudicatedState_Code
        {
            get
            {
                return Utils.getCode(StateAdjudicatedNode, Constants.sInvStateAdjudicatedState);
            }
            set
            {
                Utils.putCode(putStateAdjudicatedNode, Constants.sInvStateAdjudicatedState, value, sStANodeOrder);
            }
        }

        public string StateAdjudicatedState_CodeID
        {
            get
            {
                return Utils.getCodeID(StateAdjudicatedNode, Constants.sInvStateAdjudicatedState);
            }
            set
            {
                Utils.putCodeID(putStateAdjudicatedNode, Constants.sInvStateAdjudicatedState, value, sStANodeOrder);
            }
        }
        //End SIW139

        public void PaymentAssignment(string sPayCode, string sPayType, string sPayTypeCode, string sReserve, string sReserveCode)
        {
            if (sPayCode != "" && sPayCode != null)
                PaymentAssignmentPayCode = sPayCode;
            if ((sPayType != "" && sPayType != null) || (sPayTypeCode != "" && sPayTypeCode != null))
                putPaymentAssignmentTypeofPayment(sPayType, sPayTypeCode);
            if ((sReserve != "" && sReserve != null) || (sReserveCode != "" && sReserveCode != null))
                putPaymentAssignmentReserve(sReserve, sReserveCode);
        }

        public XmlNode PaymentAssignmentNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.sInvPayAssignment);
            }
        }

        public XmlNode putPaymentAssignmentNode
        {
            get
            {
                XmlNode xmlNode = PaymentAssignmentNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sInvPayAssignment, NodeOrder);
                return xmlNode;
            }
        }

        public string PaymentAssignmentPayCode
        {
            get
            {
                return XML.XMLGetAttributeValue(PaymentAssignmentNode, Constants.sInvPayAssignmentCode);
            }
            set
            {
                XML.XMLSetAttributeValue(putPaymentAssignmentNode, Constants.sInvPayAssignmentCode, value);
            }
        }

        //Start SIW139
        public XmlNode putPaymentAssignmentTypeofPayment(string sDesc, string sCode)
        {
            return putPaymentAssignmentTypeofPayment(sDesc, sCode, "");
        }

        public XmlNode putPaymentAssignmentTypeofPayment(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, sDesc, sCode, sPayNodeOrder, scodeid);
        }

        public string PaymentAssignmentTypeofPayment
        {
            get
            {
                return Utils.getData(PaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putData(putPaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentTypeofPayment_Code
        {
            get
            {
                return Utils.getCode(PaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putCode(putPaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentTypeofPayment_CodeID
        {
            get
            {
                return Utils.getCodeID(PaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putCodeID(putPaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putPaymentAssignmentReserve(string sDesc, string sCode)
        {
            return putPaymentAssignmentReserve(sDesc, sCode, "");
        }

        public XmlNode putPaymentAssignmentReserve(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPaymentAssignmentNode, Constants.sInvPayAssignmentReserve, sDesc, sCode, sPayNodeOrder, scodeid);
        }

        public string PaymentAssignmentReserve
        {
            get
            {
                return Utils.getData(PaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putData(putPaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentReserve_Code
        {
            get
            {
                return Utils.getCode(PaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putCode(putPaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentReserve_CodeID
        {
            get
            {
                return Utils.getCodeID(PaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putCodeID(putPaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putPaymentAssignmentTransactionType(string sDesc, string sCode)
        {
            return putPaymentAssignmentTransactionType(sDesc, sCode, "");
        }

        public XmlNode putPaymentAssignmentTransactionType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType, sDesc, sCode, sPayNodeOrder, scodeid);
        }

        public string PaymentAssignmentTransactionType
        {
            get
            {
                return Utils.getData(PaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType);
            }
            set
            {
                Utils.putData(putPaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentTransactionType_Code
        {
            get
            {
                return Utils.getCode(PaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType);
            }
            set
            {
                Utils.putCode(putPaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType, value, sPayNodeOrder);
            }
        }

        public string PaymentAssignmentTransactionType_CodeID
        {
            get
            {
                return Utils.getCodeID(PaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType);
            }
            set
            {
                Utils.putCodeID(putPaymentAssignmentNode, Constants.sInvPayAssignmentTransactionType, value, sPayNodeOrder);
            }
        }
        //End SIW139

        public int FeeCount
        {
            get
            {
                object o = FeeList;
                return xmlFeeList.Count;
            }
        }

        public XmlNodeList FeeList
        {
            get
            {
                //Start SIW163
                if (null == xmlFeeList)
                {
                    return getNodeList(Constants.sInvFee, ref xmlFeeList, Node);
                }
                else
                {
                    return xmlFeeList;
                }
                //End SIw163
            }
        }

        public XmlNode getFee(bool reset)
        {
            //Start SIW163
            if (null != FeeList)
            {
                return getNode(reset, ref xmlFeeList, ref xmlFee);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeFee()
        {
            if (xmlFee != null)
                Node.RemoveChild(xmlFee);
            xmlFee = null;
        }

        private XmlNode FeeNode
        {
            get
            {
                if (xmlFee == null)
                    xmlFee = XML.XMLGetNode(Node, Constants.sInvFee);
                return xmlFee;
            }
        }

        public XmlNode addFee(string sFee, string sPayeeID, string sPayeeType, string sPayeeCode, string sContrlNumber)
        {
            xmlFee = XML.XMLaddNode(putNode, Constants.sInvFee, NodeOrder);
            if (sFee != "" && sFee != null)
                Fee = sFee;
            putFeePayee(sPayeeID, sPayeeType, sPayeeCode);
            if (sContrlNumber != "" && sContrlNumber != null)
                FeeControlNumber = sContrlNumber;
            return xmlFee;
        }

        public XmlNode putFeeNode
        {
            get
            {
                if (FeeNode == null)
                    addFee("", "", "", "", "");
                return xmlFee;
            }
        }

        public void FeePaymentAssignment(string sPayCode, string sPayType, string sPayTypeCode, string sReserve, string sReserveCode)
        {
            if (sPayCode != "" && sPayCode != null)
                FeePaymentAssignmentPayCode = sPayCode;
            if ((sPayType != "" && sPayType != null) || (sPayTypeCode != "" && sPayTypeCode != null))
                putFeePaymentAssignmentTypeofPayment(sPayType, sPayTypeCode);
            if ((sReserve != "" && sReserve != null) || (sReserveCode != "" && sReserveCode != null))
                putFeePaymentAssignmentReserve(sReserve, sReserveCode);
        }

        public XmlNode FeePaymentAssignmentNode
        {
            get
            {
                return XML.XMLGetNode(FeeNode, Constants.sInvPayAssignment);
            }
        }

        public XmlNode putFeePaymentAssignmentNode
        {
            get
            {
                XmlNode xmlNode = FeePaymentAssignmentNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putFeeNode, Constants.sInvPayAssignment, sFeeNodeOrder);
                return xmlNode;
            }
        }

        public string FeePaymentAssignmentPayCode
        {
            get
            {
                return XML.XMLGetAttributeValue(FeePaymentAssignmentNode, Constants.sInvPayAssignmentCode);
            }
            set
            {
                XML.XMLSetAttributeValue(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentCode, value);
            }
        }

        //Start SIW139
        public XmlNode putFeePaymentAssignmentTypeofPayment(string sDesc, string sCode)
        {
            return putFeePaymentAssignmentTypeofPayment(sDesc, sCode, "");
        }

        public XmlNode putFeePaymentAssignmentTypeofPayment(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, sDesc, sCode, sPayNodeOrder, scodeid);
        }

        public string FeePaymentAssignmentTypeofPayment
        {
            get
            {
                return Utils.getData(FeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putData(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }

        public string FeePaymentAssignmentTypeofPayment_Code
        {
            get
            {
                return Utils.getCode(FeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putCode(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }

        public string FeePaymentAssignmentTypeofPayment_CodeID
        {
            get
            {
                return Utils.getCodeID(FeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment);
            }
            set
            {
                Utils.putCodeID(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentTypeofPayment, value, sPayNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putFeePaymentAssignmentReserve(string sDesc, string sCode)
        {
            return putFeePaymentAssignmentReserve(sDesc, sCode, "");
        }

        public XmlNode putFeePaymentAssignmentReserve(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve, sDesc, sCode, sPayNodeOrder, scodeid);
        }

        public string FeePaymentAssignmentReserve
        {
            get
            {
                return Utils.getData(FeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putData(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }

        public string FeePaymentAssignmentReserve_Code
        {
            get
            {
                return Utils.getCode(FeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putCode(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }

        public string FeePaymentAssignmentReserve_CodeID
        {
            get
            {
                return Utils.getCodeID(FeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve);
            }
            set
            {
                Utils.putCodeID(putFeePaymentAssignmentNode, Constants.sInvPayAssignmentReserve, value, sPayNodeOrder);
            }
        }
        //End SIW139

        public string Fee
        {
            get
            {
                return Utils.getData(FeeNode, Constants.sInvFeeAmount);
            }
            set
            {
                Utils.putData(putFeeNode, Constants.sInvFeeAmount, value, sFeeNodeOrder);
            }
        }

        public string FeeControlNumber
        {
            get
            {
                return Utils.getData(FeeNode, Constants.sInvFeeControlNumber);
            }
            set
            {
                Utils.putData(putFeeNode, Constants.sInvFeeControlNumber, value, sFeeNodeOrder);
            }
        }

        //Start SIW139
        public void putFeePayee(string sID, string sType, string stypecode)
        {
            putFeePayee(sID, sType, stypecode, "");
        }

        public void putFeePayee(string sID, string sType, string stypecode, string scodeid)
        {
            if (sID != "" && sID != null)
                FeePayeeID = sID;
            if (sType != "" && sType != null)
                FeePayeeType = sType;
            if (stypecode != "" && stypecode != null)
                FeePayeeType_Code = stypecode;
            if (scodeid != "" && scodeid != null)
                FeePayeeType_CodeID = scodeid;
        }

        public string FeePayeeID
        {
            get
            {
                return Utils.getEntityRef(FeeNode, Constants.sInvFeePayee);
            }
            set
            {
                Utils.putEntityRef(putFeeNode, Constants.sInvFeePayee, value, sFeeNodeOrder);
            }
        }

        public string FeePayeeType
        {
            get
            {
                return Utils.getData(FeeNode, Constants.sInvFeePayee);
            }
            set
            {
                Utils.putData(putFeeNode, Constants.sInvFeePayee, value, sFeeNodeOrder);
            }
        }

        public string FeePayeeType_Code
        {
            get
            {
                return Utils.getCode(FeeNode, Constants.sInvFeePayee);
            }
            set
            {
                Utils.putCode(putFeeNode, Constants.sInvFeePayee, value, sFeeNodeOrder);
            }
        }

        public string FeePayeeType_CodeID
        {
            get
            {
                return Utils.getCodeID(FeeNode, Constants.sInvFeePayee);
            }
            set
            {
                Utils.putCodeID(putFeeNode, Constants.sInvFeePayee, value, sFeeNodeOrder);
            }
        }
        //End SIW139

        public int DiagnosisCount
        {
            get
            {
                object o = DiagnosisList;
                return xmlDiagnosisList.Count;
            }
        }

        public XmlNodeList DiagnosisList
        {
            get
            {
                //Start SIW163
                if (null == xmlDiagnosisList)
                {
                    return getNodeList(Constants.sInvDiagnosis, ref xmlDiagnosisList, Node);
                }
                else
                {
                    return xmlDiagnosisList;
                }
                //End SIW163
            }
        }

        public XmlNode getDiagnosis(bool reset)
        {
            //Start SIW163
            if (null != DiagnosisList)
            {
                return getNode(reset, ref xmlDiagnosisList, ref xmlDiagnosis);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeDiagnosis()
        {
            if (xmlDiagnosis != null)
                Node.RemoveChild(xmlDiagnosis);
            xmlDiagnosis = null;
        }

        private XmlNode DiagnosisNode
        {
            get
            {
                if (xmlDiagnosis == null)
                    xmlDiagnosis = XML.XMLaddNode(putNode, Constants.sInvDiagnosis, NodeOrder);
                return xmlDiagnosis;
            }
        }

        public XmlNode addDiagnosis(string sDiagnosis, string sDiagnosisCode, string sDate)
        {
            xmlDiagnosis = null;
            object o = DiagnosisNode;
            putDiagnosis(sDiagnosis, sDiagnosisCode, sDate);
            return xmlDiagnosis;
        }
        
        //Start SIW139
        public XmlNode putDiagnosis(string sDiagnosis, string sDiagnosisCode, string sDate)
        {
            return putDiagnosis(sDiagnosis, sDiagnosisCode, sDate, "");
        }

        public XmlNode putDiagnosis(string sDiagnosis, string sDiagnosisCode, string sDate, string scodeid)
        {
            Diagnosis = sDiagnosis;
            Diagnosis_Code = sDiagnosisCode;
            DiagnosisDate = sDate;
            Diagnosis_CodeID = scodeid;
            return xmlDiagnosis;
        }

        public string Diagnosis
        {
            get
            {
                return Utils.getData(DiagnosisNode, Constants.sInvDiagSpecific);
            }
            set
            {
                Utils.putData(DiagnosisNode, Constants.sInvDiagSpecific, value, sDiagNodeOrder);
            }
        }

        public string Diagnosis_Code
        {
            get
            {
                return Utils.getCode(DiagnosisNode, Constants.sInvDiagSpecific);
            }
            set
            {
                Utils.putCode(DiagnosisNode, Constants.sInvDiagSpecific, value, sDiagNodeOrder);
            }
        }

        public string Diagnosis_CodeID
        {
            get
            {
                return Utils.getCodeID(DiagnosisNode, Constants.sInvDiagSpecific);
            }
            set
            {
                Utils.putCodeID(DiagnosisNode, Constants.sInvDiagSpecific, value, sDiagNodeOrder);
            }
        }
        //End SIW139

        public string DiagnosisDate
        {
            get
            {
                return Utils.getDate(DiagnosisNode, Constants.sInvDiagDate);
            }
            set
            {
                Utils.putDate(DiagnosisNode, Constants.sInvDiagDate, value, sDiagNodeOrder);
            }
        }

        public int EOBCount
        {
            get
            {
                object o = EOBList;
                return xmlEOBList.Count;
            }
        }

        public XmlNodeList EOBList
        {
            get
            {
                //Start SIW163
                if (null == xmlEOBList)
                {
                    return getNodeList(Constants.sInvEOB, ref xmlEOBList, Node);
                }
                else
                {
                    return xmlEOBList;
                }
                //End SIW163
            }
        }

        public XmlNode getEOB(bool reset)
        {
            //Start SIW163
            if (null != EOBList)
            {
                return getNode(reset, ref xmlEOBList, ref xmlEOB);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeEOB()
        {
            if (xmlEOB != null)
                Node.RemoveChild(xmlEOB);
            xmlEOB = null;
        }

        private XmlNode EOBNode
        {
            get
            {
                if (xmlEOB == null)
                    xmlEOB = XML.XMLGetNode(putNode, Constants.sInvEOB);
                return xmlEOB;
            }
        }

        public XmlNode addEOB(string sEOBCode, string sShortRem, string sLongRem)
        {
            xmlEOB = XML.XMLaddNode(putNode, Constants.sInvEOB, NodeOrder);
            if (sEOBCode != "" && sEOBCode != null)
                EOB_Code = sEOBCode;
            if (sShortRem != "" && sShortRem != null)
                EOBShortRemark = sShortRem;
            if (sLongRem != "" && sLongRem != null)
                EOBLongRemark = sLongRem;
            return xmlEOB;
        }

        public XmlNode putEOBNode
        {
            get
            {
                if (EOBNode == null)
                    addEOB("", "", "");
                return xmlEOB;
            }
        }

        public string EOB_Code
        {
            get
            {
                if (EOBNode == null)
                    return "";
                else
                    return XML.XMLExtractCodeAttribute(xmlEOB);
            }
            set
            {
                XML.XMLSetAttributeValue(putEOBNode, Constants.sdtCode, value);
            }
        }

        public string EOBShortRemark
        {
            get
            {
                return Utils.getData(EOBNode, Constants.sInvEOBShortRemark);
            }
            set
            {
                Utils.putData(putEOBNode, Constants.sInvEOBShortRemark, value, sEOBNodeOrder);
            }
        }

        public string EOBLongRemark
        {
            get
            {
                return Utils.getData(EOBNode, Constants.sInvEOBLongRemark);
            }
            set
            {
                Utils.putData(putEOBNode, Constants.sInvEOBLongRemark, value, sEOBNodeOrder);
            }
        }

        public XmlNode InvoiceDetailsNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.sInvDetails);
            }
        }

        public XmlNode putInvoiceDetailsNode
        {
            get
            {
                XmlNode xmlNode = InvoiceDetailsNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sInvDetails, NodeOrder);
                return xmlNode;
            }
        }

        public int DetailCount
        {
            get
            {
                object o = DetailList;
                return xmlDetailList.Count;
            }
        }

        public XmlNodeList DetailList
        {
            get
            {
                //Start SIW163
                if (null == xmlDetailList)
                {
                    return getNodeList(Constants.sInvDetail, ref xmlDetailList, InvoiceDetailsNode);
                }
                else
                {
                    return xmlDetailList;
                }
                //End SIW163
            }
        }

        public XmlNode getDetail(bool reset)
        {
            //Start SIW163
            if (null != DetailList)
            {
                return getNode(reset, ref xmlDetailList, ref xmlDetail);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeDetail()
        {
            if (xmlDetail != null)
                InvoiceDetailsNode.RemoveChild(xmlDetail);
            xmlDetail = null;
        }

        private XmlNode DetailNode
        {
            get
            {
                if (xmlDetail == null)
                    xmlDetail = XML.XMLGetNode(InvoiceDetailsNode, Constants.sInvDetail);
                return xmlDetail;
            }
            set
            {
                xmlDetail = value;
                xmlDetailEOB = null;
                xmlDetailEOBList = null;
                xmlDetailDiag = null;
                xmlDetailDiagList = null;
            }
        }

        private XmlNode putDetailNode
        {
            get
            {
                if (DetailNode == null)
                    addDetail("", "", "", "", "", "", "0", "", "", "", "", "", "", "", "", "");
                return xmlDetail;
            }
        }

        public XmlNode addDetail(string sPOS, string sPOSCode, string sTOS, string sTOSCode, string sSvcStartDate,
                                 string sSvcEndDate, string sUnits, string sBilled, string sDisallowed,
                                 string sHOSPDisallowed, string sURDisallowed, string sDiscount, string sPPNReduced,
                                 string sPayed, string sAllowed, string sSaved)
        {
            //SIW485 xmlDetail = XML.XMLaddNode(putInvoiceDetailsNode, Constants.sInvDetail, Globals.sNodeOrder);
            xmlDetail = XML.XMLaddNode(putInvoiceDetailsNode, Constants.sInvDetail, Utils.sNodeOrder);	//SIW485

            if ((sPOS != "" && sPOS != null) || (sPOSCode != "" && sPOSCode != null))
                putDetailPlaceOfService(sPOS, sPOSCode);
            if ((sTOS != "" && sTOS != null) || (sTOSCode != "" && sTOSCode != null))
                putDetailTypeOfService(sTOS, sTOSCode);
            if (sSvcStartDate != "" && sSvcStartDate != null)
                ServiceStartDate = sSvcStartDate;
            if (sSvcEndDate != "" && sSvcEndDate != null)
                ServiceEndDate = sSvcEndDate;
            if (sUnits != "" && sUnits != null)
                DetailUnits = sUnits;
            if (sBilled != "" && sBilled != null)
                DetailBilled = sBilled;
            if (sDisallowed != "" && sDisallowed != null)
                DetailDisallowed = sDisallowed;
            if (sHOSPDisallowed != "" && sHOSPDisallowed != null)
                DetailHospDisallowed = sHOSPDisallowed;
            if (sURDisallowed != "" && sURDisallowed != null)
                DetailURDisallowed = sURDisallowed;
            if (sDiscount != "" && sDiscount != null)
                DetailDiscount = sDiscount;
            if (sPPNReduced != "" && sPPNReduced != null)
                DetailPPNReduced = sPPNReduced;
            if (sPayed != "" && sPayed != null)
                DetailPayed = sPayed;
            if (sAllowed != "" && sAllowed != null)
                DetailAllowed = sAllowed;
            if (sSaved != "" && sSaved != null)
                DetailSaved = sSaved;
            return xmlDetail;
        }

        //Start SIW139
        public XmlNode putDetailPlaceOfService(string sDesc, string sCode)
        {
            return putDetailPlaceOfService(sDesc, sCode, "");
        }

        public XmlNode putDetailPlaceOfService(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putDetailNode, Constants.sInvDetailPlaceOfSvc, sDesc, sCode, sDtlNodeOrder, scodeid);
        }

        public string DetailPlaceOfService
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailPlaceOfSvc);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailPlaceOfSvc, value, sDtlNodeOrder);
            }
        }

        public string DetailPlaceOfService_Code
        {
            get
            {
                return Utils.getCode(DetailNode, Constants.sInvDetailPlaceOfSvc);
            }
            set
            {
                Utils.putCode(putDetailNode, Constants.sInvDetailPlaceOfSvc, value, sDtlNodeOrder);
            }
        }

        public string DetailPlaceOfService_CodeID
        {
            get
            {
                return Utils.getCodeID(DetailNode, Constants.sInvDetailPlaceOfSvc);
            }
            set
            {
                Utils.putCodeID(putDetailNode, Constants.sInvDetailPlaceOfSvc, value, sDtlNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putDetailTypeOfService(string sDesc, string sCode)
        {
            return putDetailTypeOfService(sDesc, sCode, "");
        }

        public XmlNode putDetailTypeOfService(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putDetailNode, Constants.sInvDetailTypeOfSvc, sDesc, sCode, sDtlNodeOrder, scodeid);
        }

        public string DetailTypeOfService
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailTypeOfSvc);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailTypeOfSvc, value, sDtlNodeOrder);
            }
        }

        public string DetailTypeOfService_Code
        {
            get
            {
                return Utils.getCode(DetailNode, Constants.sInvDetailTypeOfSvc);
            }
            set
            {
                Utils.putCode(putDetailNode, Constants.sInvDetailTypeOfSvc, value, sDtlNodeOrder);
            }
        }

        public string DetailTypeOfService_CodeID
        {
            get
            {
                return Utils.getCodeID(DetailNode, Constants.sInvDetailTypeOfSvc);
            }
            set
            {
                Utils.putCodeID(putDetailNode, Constants.sInvDetailTypeOfSvc, value, sDtlNodeOrder);
            }
        }
        //End SIW139

        public string ServiceStartDate
        {
            get
            {
                return Utils.getDate(DetailNode, Constants.sInvDetailSvcStartDate);
            }
            set
            {
                Utils.putDate(putDetailNode, Constants.sInvDetailSvcStartDate, value, sDtlNodeOrder);
            }
        }

        public string ServiceEndDate
        {
            get
            {
                return Utils.getDate(DetailNode, Constants.sInvDetailSvcEndDate);
            }
            set
            {
                Utils.putDate(putDetailNode, Constants.sInvDetailSvcEndDate, value, sDtlNodeOrder);
            }
        }

        public XmlNode putUnits(string sDesc, string sCode)
        {
            return Utils.putTypeItem(putDetailNode, Constants.sInvDetailUnits, sDesc, sCode, sDtlNodeOrder);
        }

        public string DetailUnits
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailUnits);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailUnits, value, sDtlNodeOrder);
            }
        }

        public string DetailUnits_Type
        {
            get
            {
                return Utils.getType(DetailNode, Constants.sInvDetailUnits);
            }
            set
            {
                Utils.putType(putDetailNode, Constants.sInvDetailUnits, value, sDtlNodeOrder);
            }
        }

        public string DetailBilled
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailBilled);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailBilled, value, sDtlNodeOrder);
            }
        }

        public string DetailDisallowed
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailDisallowed);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailDisallowed, value, sDtlNodeOrder);
            }
        }

        public string DetailHospDisallowed
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailHospDisallowed);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailHospDisallowed, value, sDtlNodeOrder);
            }
        }

        public string DetailURDisallowed
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailURDisallowed);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailURDisallowed, value, sDtlNodeOrder);
            }
        }

        public string DetailDiscount
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailDiscount);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailDiscount, value, sDtlNodeOrder);
            }
        }

        public string DetailPPNReduced
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailPPNReduced);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailPPNReduced, value, sDtlNodeOrder);
            }
        }

        public string DetailPayed
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailPayed);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailPayed, value, sDtlNodeOrder);
            }
        }

        public string DetailAllowed
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailAllowed);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailAllowed, value, sDtlNodeOrder);
            }
        }

        public string DetailSaved
        {
            get
            {
                return Utils.getData(DetailNode, Constants.sInvDetailSaved);
            }
            set
            {
                Utils.putData(putDetailNode, Constants.sInvDetailSaved, value, sDtlNodeOrder);
            }
        }

        public int DetailDiagnosisCount
        {
            get
            {
                object o = DetailDiagnosisList;
                return xmlDetailDiagList.Count;
            }
        }

        public XmlNodeList DetailDiagnosisList
        {
            get
            {
                //Start SIW163
                if (null == xmlDetailDiagList)
                {
                    if (null == DetailNode)
                    {
                        getDetail(true);
                    }
                    return getNodeList(Constants.sInvDetailDiagnosis, ref xmlDetailDiagList, DetailNode);
                }
                else
                {
                    return xmlDetailDiagList;
                }
                //End SIW163
            }
        }

        public XmlNode getDetailDiagnosis(bool reset)
        {
            //Start SIW163
            if (null != DetailDiagnosisList)
            {                
                return getNode(reset, ref xmlDetailDiagList, ref xmlDetailDiag);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeDetailDiagnosis()
        {
            if (xmlDetailDiag != null)
                DetailNode.RemoveChild(xmlDetailDiag);
            xmlDetailDiag = null;
        }

        private XmlNode DetailDiagnosisNode
        {
            get
            {
                if (xmlDetailDiag == null)
                    xmlDetailDiag = XML.XMLGetNode(DetailNode, Constants.sInvDiagnosis);
                return xmlDetailDiag;
            }
        }

        public XmlNode addDetailDiagnosis(string sDetailDiagnosis)
        {
            xmlDetailDiag = XML.XMLaddNode(putDetailNode, Constants.sInvDiagnosis, sDtlNodeOrder);
            if (sDetailDiagnosis != "" && sDetailDiagnosis != null)
                DetailDiagnosis = sDetailDiagnosis;
            return xmlDetailDiag;
        }

        public XmlNode putDetailDiagnosisNode
        {
            get
            {
                if (DetailDiagnosisNode == null)
                    addDetailDiagnosis("");
                return xmlDetailDiag;
            }
        }

        public string DetailDiagnosis
        {
            get
            {
                if (DetailDiagnosisNode == null)
                    return "";
                else
                    return DetailDiagnosisNode.InnerText;
            }
            set
            {
                putDetailDiagnosisNode.InnerText = value;
            }
        }

        public int DetailEOBCount
        {
            get
            {
                object o = DetailEOBList;
                return xmlDetailEOBList.Count;
            }
        }

        public XmlNodeList DetailEOBList
        {
            get
            {
                //Start SIW163
                if (null == xmlDetailEOBList)
                {
                    if (null == DetailNode)
                    {
                        getDetail(true);
                    }
                    return getNodeList(Constants.sInvEOB, ref xmlDetailEOBList, DetailNode);
                }
                else
                {
                    return xmlDetailEOBList;
                }
                //End SIW163
            }
        }

        public XmlNode getDetailEOB(bool reset)
        {
            //Start SIW163
            if (null != DetailEOBList)
            {
                return getNode(reset, ref xmlDetailEOBList, ref xmlDetailEOB);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeDetailEOB()
        {
            if (xmlDetailEOB != null)
                DetailNode.RemoveChild(xmlDetailEOB);
            xmlDetailEOB = null;
        }

        private XmlNode DetailEOBNode
        {
            get
            {
                if (xmlDetailEOB == null)
                    xmlDetailEOB = XML.XMLGetNode(DetailNode, Constants.sInvEOB);
                return xmlDetailEOB;
            }
        }

        public XmlNode addDetailEOB(string sDetailEOBCode, string sShortRem, string sLongRem)
        {
            xmlDetailEOB = XML.XMLaddNode(putDetailNode, Constants.sInvEOB, sDtlNodeOrder);
            if (sDetailEOBCode != "" && sDetailEOBCode != null)
                DetailEOB_Code = sDetailEOBCode;
            if (sShortRem != "" && sShortRem != null)
                DetailEOBShortRemark = sShortRem;
            if (sLongRem != "" && sLongRem != null)
                DetailEOBLongRemark = sLongRem;
            return xmlDetailEOB;
        }

        public XmlNode putDetailEOBNode
        {
            get
            {
                if (DetailEOBNode == null)
                    addDetailEOB("", "", "");
                return xmlDetailEOB;
            }
        }

        public string DetailEOB_Code
        {
            get
            {
                if (DetailEOBNode == null)
                    return "";
                else
                    return XML.XMLExtractCodeAttribute(xmlDetailEOB);
            }
            set
            {
                XML.XMLSetAttributeValue(putDetailEOBNode, Constants.sdtCode, value);
            }
        }

        public string DetailEOBShortRemark
        {
            get
            {
                return Utils.getData(DetailEOBNode, Constants.sInvEOBShortRemark);
            }
            set
            {
                Utils.putData(putDetailEOBNode, Constants.sInvEOBShortRemark, value, sEOBNodeOrder);
            }
        }

        public string DetailEOBLongRemark
        {
            get
            {
                return Utils.getData(DetailEOBNode, Constants.sInvEOBLongRemark);
            }
            set
            {
                Utils.putData(putDetailEOBNode, Constants.sInvEOBLongRemark, value, sEOBNodeOrder);
            }
        }

        public int ProcedureCount
        {
            get
            {
                return ProcedureList.Count;
            }
        }

        public XmlNodeList ProcedureList
        {
            get
            {
                //Start SIW163
                if (null == xmlProcedureList)
                {
                    return getNodeList(Constants.sInvDetailProcedure, ref xmlProcedureList, DetailNode);
                }
                else
                {
                    return xmlProcedureList;
                }
                //End SIW163
            }
        }

        public XmlNode getProcedure(bool reset)
        {
            //Start SIW163
            if (null != ProcedureList)
            {
                return getNode(reset, ref xmlProcedureList, ref xmlProcedure);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        private XmlNode ProcedureNode
        {
            get
            {
                if (xmlProcedure == null)
                    xmlProcedure = XML.XMLGetNode(DetailNode, Constants.sInvDetailProcedure);
                return xmlProcedure;
            }
        }

        private XmlNode putProcedureNode
        {
            get
            {
                if (ProcedureNode == null)
                    addProcedure("", "", "", "", "", "", "", "");
                return xmlProcedure;
            }
        }

        public void addProcedure(string sProcedure, string sProcedureCode, string sstate, string sstatecode,
                                 string sType, string stypecode, string sUse, string sTooth)
        {
            xmlProcedure = XML.XMLaddNode(putDetailNode, Constants.sInvDetailProcedure, sDtlNodeOrder);
            if ((sProcedure != "" && sProcedure != null) || (sProcedureCode != "" && sProcedureCode != null))
                putProcedureCode(sProcedure, sProcedureCode);
            if ((sstate != "" && sstate != null) || (sstatecode != "" && sstatecode != null))
                putProcedureState(sstate, sstatecode);
            if ((sType != "" && sType != null) || (stypecode != "" && stypecode != null))
                putProcedureType(sType, stypecode);
            if (sUse != "" && sUse != null)
                ProcedureUse = sUse;
            if (sTooth != "" && sTooth != null)
                ProcedureTooth = sTooth;
        }

        public string ProcedureUse
        {
            get
            {
                return Utils.getData(ProcedureNode, Constants.sInvDetailProcedureUse);
            }
            set
            {
                Utils.putData(putProcedureNode, Constants.sInvDetailProcedureUse, value, sProcNodeOrder);
            }
        }

        public string ProcedureTooth
        {
            get
            {
                return Utils.getData(ProcedureNode, Constants.sInvDetailProcedureTooth);
            }
            set
            {
                Utils.putData(putProcedureNode, Constants.sInvDetailProcedureTooth, value, sProcNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putProcedureCode(string sDesc, string sCode)
        {
            return putProcedureCode(sDesc, sCode, "");
        }

        public XmlNode putProcedureCode(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putProcedureNode, Constants.sInvDetailProcedureSpecified, sDesc, sCode, sProcNodeOrder, scodeid);
        }

        public string ProcedureCode
        {
            get
            {
                return Utils.getData(ProcedureNode, Constants.sInvDetailProcedureSpecified);
            }
            set
            {
                Utils.putData(putProcedureNode, Constants.sInvDetailProcedureSpecified, value, sProcNodeOrder);
            }
        }

        public string ProcedureCode_Code
        {
            get
            {
                return Utils.getCode(ProcedureNode, Constants.sInvDetailProcedureSpecified);
            }
            set
            {
                Utils.putCode(putProcedureNode, Constants.sInvDetailProcedureSpecified, value, sProcNodeOrder);
            }
        }

        public string ProcedureCode_CodeID
        {
            get
            {
                return Utils.getCodeID(ProcedureNode, Constants.sInvDetailProcedureSpecified);
            }
            set
            {
                Utils.putCodeID(putProcedureNode, Constants.sInvDetailProcedureSpecified, value, sProcNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putProcedureState(string sDesc, string sCode)
        {
            return putProcedureState(sDesc, sCode, "");
        }

        public XmlNode putProcedureState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putProcedureNode, Constants.sInvDetailProcedureState, sDesc, sCode, sProcNodeOrder, scodeid);
        }

        public string ProcedureState
        {
            get
            {
                return Utils.getData(ProcedureNode, Constants.sInvDetailProcedureState);
            }
            set
            {
                Utils.putData(putProcedureNode, Constants.sInvDetailProcedureState, value, sProcNodeOrder);
            }
        }

        public string ProcedureState_Code
        {
            get
            {
                return Utils.getCode(ProcedureNode, Constants.sInvDetailProcedureState);
            }
            set
            {
                Utils.putCode(putProcedureNode, Constants.sInvDetailProcedureState, value, sProcNodeOrder);
            }
        }

        public string ProcedureState_CodeID
        {
            get
            {
                return Utils.getCodeID(ProcedureNode, Constants.sInvDetailProcedureState);
            }
            set
            {
                Utils.putCodeID(putProcedureNode, Constants.sInvDetailProcedureState, value, sProcNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putProcedureType(string sDesc, string sCode)
        {
            return putProcedureType(sDesc, sCode, "");
        }

        public XmlNode putProcedureType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putProcedureNode, Constants.sInvDetailProcedureType, sDesc, sCode, sProcNodeOrder, scodeid);
        }

        public string ProcedureType
        {
            get
            {
                return Utils.getData(ProcedureNode, Constants.sInvDetailProcedureType);
            }
            set
            {
                Utils.putData(putProcedureNode, Constants.sInvDetailProcedureType, value, sProcNodeOrder);
            }
        }

        public string ProcedureType_Code
        {
            get
            {
                return Utils.getCode(ProcedureNode, Constants.sInvDetailProcedureType);
            }
            set
            {
                Utils.putCode(putProcedureNode, Constants.sInvDetailProcedureType, value, sProcNodeOrder);
            }
        }

        public string ProcedureType_CodeID
        {
            get
            {
                return Utils.getCodeID(ProcedureNode, Constants.sInvDetailProcedureType);
            }
            set
            {
                Utils.putCodeID(putProcedureNode, Constants.sInvDetailProcedureType, value, sProcNodeOrder);
            }
        }
        //End SIW139

        public XmlNode ContractNode
        {
            get
            {
                return XML.XMLGetNode(DetailNode, Constants.sInvDetailContract);
            }
        }

        public XmlNode putContractNode
        {
            get
            {
                XmlNode xmlNode = ContractNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putDetailNode, Constants.sInvDetailContract, sDtlNodeOrder);
                return xmlNode;
            }
        }

        public string ContractNumber
        {
            get
            {
                return Utils.getData(ContractNode, Constants.sInvDetailContractNumber);
            }
            set
            {
                Utils.putData(putContractNode, Constants.sInvDetailContractNumber, value, sContNodeOrder);
            }
        }

        public XmlNode putContractDiscount(string sDesc, string sCode)
        {
            return Utils.putTypeItem(putContractNode, Constants.sInvDetailContractDiscType, sDesc, sCode, sContNodeOrder);
        }

        public string ContractDiscount
        {
            get
            {
                return Utils.getData(ContractNode, Constants.sInvDetailContractDiscType);
            }
            set
            {
                Utils.putData(putContractNode, Constants.sInvDetailContractDiscType, value, sContNodeOrder);
            }
        }

        public string ContractDiscount_Type
        {
            get
            {
                return Utils.getType(ContractNode, Constants.sInvDetailContractDiscType);
            }
            set
            {
                Utils.putType(putContractNode, Constants.sInvDetailContractDiscType, value, sContNodeOrder);
            }
        }

        private void subClearPointers()
        {
            xmlDiagnosis = null;
            xmlDiagnosisList = null;
            xmlRelatedDoc = null;
            xmlRelatedDocList = null;
            xmlProcessError = null;
            xmlProcessErrorList = null;
            xmlFee = null;
            xmlFeeList = null;
            xmlEOB = null;
            xmlEOBList = null;
            xmlDetail = null;
            xmlDetailList = null;
            xmlDetailEOB = null;
            xmlDetailEOBList = null;
            xmlDetailDiag = null;
            xmlDetailDiagList = null;
            xmlProcedure = null;
            xmlProcedureList = null;

            PartyInvolved = null;
            CommentReference = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                {
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_AddressReference.getAddressReference(true);
                }
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.Invoice = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getInvoice(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode putSummary(XmlDocument xdoc, string scount, string sBilled, string sReduction, 
                                  string sAllowed, string sFees)
        {
            if (xdoc != null)
                Document = xdoc;
            if (scount != "" && scount != null)
                SummaryInvCount = scount;
            if (sBilled != "" && sBilled != null)
                SummaryBilledAmt = sBilled;
            if (sReduction != "" && sReduction != null)
                SummaryReducedAmt = sReduction;
            if (sAllowed != "" && sAllowed != null)
                SummaryAllowedAmt = sAllowed;
            if (sFees != "" && sFees != null)
                SummaryFeesAmt = sFees;
            return m_xmlSummary;
        }

        public XmlNode putSummaryNode
        {
            get
            {
                if (SummaryNode == null)
                    m_xmlSummary = XML.XMLaddNode(Parent.Node, Constants.sInvoices, Parent_NodeOrder);
                return m_xmlSummary;
            }
        }

        public XmlNode SummaryNode
        {
            get
            {
                if (m_xmlSummary == null)
                    m_xmlSummary = XML.XMLGetNode(Parent_Node, Constants.sInvoices);
                return m_xmlSummary;
            }
            set
            {
                m_xmlSummary = value;
            }
        }

        public string SummaryBilledAmt
        {
            get
            {
                return Utils.getData(SummaryNode, Constants.sInvTotalBilled);
            }
            set
            {
                Utils.putData(putSummaryNode, Constants.sInvTotalBilled, value, sInvSumNodeOrder);
            }
        }

        public string SummaryReducedAmt
        {
            get
            {
                return Utils.getData(SummaryNode, Constants.sInvTotalReduced);
            }
            set
            {
                Utils.putData(putSummaryNode, Constants.sInvTotalReduced, value, sInvSumNodeOrder);
            }
        }

        public string SummaryAllowedAmt
        {
            get
            {
                return Utils.getData(SummaryNode, Constants.sInvTotalAllowed);
            }
            set
            {
                Utils.putData(putSummaryNode, Constants.sInvTotalAllowed, value, sInvSumNodeOrder);
            }
        }

        public string SummaryFeesAmt
        {
            get
            {
                return Utils.getData(SummaryNode, Constants.sInvTotalFees);
            }
            set
            {
                Utils.putData(putSummaryNode, Constants.sInvTotalFees, value, sInvSumNodeOrder);
            }
        }

        public string SummaryInvCount
        {
            get
            {
                if (m_xmlSummary != null)
                    return Utils.getAttribute(SummaryNode, "", Constants.sInvCount);
                else
                    return "";
            }
            set
            {
                Utils.putAttribute(putSummaryNode, "", Constants.sInvCount, value, sInvSumNodeOrder);
            }
        }
    }
}
