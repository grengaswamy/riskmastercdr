﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface (Created)
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public interface XMLXCIntPartyInvolved
    {
        XMLPartyInvolved PartyInvolved
        {
            get;
            set;
        }

        XML XML
        {
            get;
            set;
        }
    }
}
