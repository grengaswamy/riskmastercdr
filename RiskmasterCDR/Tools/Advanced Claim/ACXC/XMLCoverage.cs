/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 02/10/2011 | SIW529  |    AS      | Fixes for removing static functions
 * 03/21/2011 | SIW7036 |   SW       | Incurred limit in Web.
 **********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLCoverage : XMLXCNodeWithList, XMLXCIntEndorsement, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        ' The cXMLCoverage class provides the functionality to support and manage a
        ' ClaimsPro Coverage Node
        '*****************************************************************************
        
        '<UNIT>
        '   <COVERAGE_DATA ID="COVxxx" LEGACY_KEY="00001,1,1" COV_SEQ_NO="1" ITEM_SEQ_NO="1">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <COVERAGE CODE="032">Workers Compensation</COVERAGE>
        '      <TREE_LABEL>Tree Description</TREE_LABEL>
        '      <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '      <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '      <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->
        '      <RISK_GROUP CODE="011">Workers Compensation - Voluntary</RISK_GROUP>
        '      <CLASSIFICATION CODE="8017">Store: Retail Noc</CLASSIFICATION>
        '      <STATE CODE="TX">Texas</COV_STATE>
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DATA_ITEM> ... </DATA_ITEM>                   'SI04920
        '     <FINANCIAL_ACTIVITY ISFROZEN='True|False' ISPAYMENT='True|False' ISCOLLECTION='True|False'/> 'SIW7036
        '   </COVERAGE_DATA>
        '</UNIT>*/

        private XMLLimit m_Limit;
        private XMLDeductible m_Deductible;
        private XMLCommentReference m_Comment;
        private XMLPartyInvolved m_PartyInvolved;
        private new XMLInsUnit m_Parent;
        private XMLEndorsement m_Endorsement;
        private XMLVarDataItem m_DataItem;
        private XMLLossDetail m_LossDetail; //SIw490
        private int lCovID;
        
        public XMLCoverage()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;
            m_LossDetail = null;//SIw490

            lCovID = 0;

            //sThisNodeOrder = new ArrayList(12); //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(13);//(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(15);//SIW490 //SIW7036
            sThisNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sThisNodeOrder.Add(Constants.sCovCoverage);
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sCovEffDate);
            sThisNodeOrder.Add(Constants.sCovExpDate);
            sThisNodeOrder.Add(Constants.sCovRiskGroup);
            sThisNodeOrder.Add(Constants.sCovClassification);
            sThisNodeOrder.Add(Constants.sCovState);
            sThisNodeOrder.Add(Constants.sCovLimit);
            sThisNodeOrder.Add(Constants.sCovDeductible);
            sThisNodeOrder.Add(Constants.sCovEndorsement);
            sThisNodeOrder.Add(Constants.sCovPartyInvolved);
            sThisNodeOrder.Add(Constants.sCovComment);
            sThisNodeOrder.Add(Constants.sCovDataItem);
            sThisNodeOrder.Add(Constants.sStdFinancialActivity); //SIW7036    
            //sThisNodeOrder.Add(Constants.sCovTechKey);//(SI06023 - Implemented in SI06333)//sIW360
        }

        public override XmlNode Create()
        {
            return Create("","","","","","","","","","","","","","");
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sCovNode; }
        }

        public XmlNode Create(string scovid, string scoverage, string scoveragecode, string slkey, string scovseq,
                              string sitmseq, string seffdte, string sexpdte, string sstate, string sstatecode,
                              string sClass, string sclasscode, string srgroup, string srgroupcode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                CoverageID = scovid;
                putCoverage(scoverage, scoveragecode);
                LegacyKey = slkey;
                CovSequence = scovseq;
                CovItemSequence = sitmseq;
                putCoverageState(sstate, sstatecode);
                putClassification(sClass, sclasscode);
                putRiskGroup(srgroup, srgroupcode);
                EffectiveDate = seffdte;
                ExpirationDate = sexpdte;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLCoverage.Create");
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sCovIDPfx;
            }
        }

        public string CoverageID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sCovID);
                string lid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sCovIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value; ;
                if (value == null || value == "" || value == "0")
                {
                    lCovID = lCovID + 1;
                    lid = lCovID + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sCovID, Constants.sCovIDPfx + lid);
            }
        }

        public string LegacyKey
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sCovLegacyKey);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sCovLegacyKey, value);
            }
        }

        public string CovSequence
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sCovSeqNo);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sCovSeqNo, value);
            }
        }

        public string CovItemSequence
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sCovItemSeqNo);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sCovItemSeqNo, value);
            }
        }

        //Start SIW139
        public XmlNode putCoverageState(string sDesc, string sCode)
        {
            return putCoverageState(sDesc, sCode, "");
        }

        public XmlNode putCoverageState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCovState, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CoverageState
        {
            get
            {
                return Utils.getData(Node, Constants.sCovState);
            }
            set
            {
                Utils.putData(putNode, Constants.sCovState, value, NodeOrder);
            }
        }

        public string CoverageState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCovState);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCovState, value, NodeOrder);
            }
        }

        public string CoverageState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCovState);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCovState, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCoverage(string sDesc, string sCode)
        {
            return putCoverage(sDesc, sCode, "");
        }

        public XmlNode putCoverage(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCovCoverage, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Coverage
        {
            get
            {
                return Utils.getData(Node, Constants.sCovCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sCovCoverage, value, NodeOrder);
            }
        }
        public string CovDescription
        {
            get
            {
                return Utils.getData(Node, "COV_DESCRIPTION");
            }
            set
            {
                Utils.putData(putNode, "COV_DESCRIPTION", value, NodeOrder);
            }
        }

          

        public string Coverage_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCovCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCovCoverage, value, NodeOrder);
            }
        }

        public string Coverage_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCovCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCovCoverage, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putClassification(string sDesc, string sCode)
        {
            return putClassification(sDesc, sCode, "");
        }

        public XmlNode putClassification(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCovClassification, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Classification
        {
            get
            {
                return Utils.getData(Node, Constants.sCovClassification);
            }
            set
            {
                Utils.putData(putNode, Constants.sCovClassification, value, NodeOrder);
            }
        }

        public string Classification_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCovClassification);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCovClassification, value, NodeOrder);
            }
        }

        public string Classification_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCovClassification);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCovClassification, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putRiskGroup(string sDesc, string sCode)
        {
            return putRiskGroup(sDesc, sCode, "");
        }

        public XmlNode putRiskGroup(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCovRiskGroup, sDesc, sCode, NodeOrder, scodeid);
        }

        public string RiskGroup
        {
            get
            {
                return Utils.getData(Node, Constants.sCovRiskGroup);
            }
            set
            {
                Utils.putData(putNode, Constants.sCovRiskGroup, value, NodeOrder);
            }
        }

        public string RiskGroupCode
        {
            get
            {
                return Utils.getCode(Node, Constants.sCovRiskGroup);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCovRiskGroup, value, NodeOrder);
            }
        }

        public string RiskGroupCodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCovRiskGroup);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCovRiskGroup, value, NodeOrder);
            }
        }
        //End SIW139

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        // Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        // End SIW490
        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Parent != null)
                m_Parent.Coverage = this;
            Node = null;
        }

        private void subClearPointers()
        {
            Limit = null;
            Deductible = null;
            CommentReference = null;
            PartyInvolved = null;
            DataItem = null;
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCovEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCovEffDate, value, NodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCovExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCovExpDate, value, NodeOrder);
            }
        }
        //Start SIW7036
        //<FINANCIAL_ACTIVITY ISFROZEN='True|False' ISPAYMENT='True|False' ISCOLLECTION='True|False'/> 'SIW7036
        public string IsFrozen
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsFrozen);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsFrozen, value, NodeOrder);
            }
        }
        public string IsPayment
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsPayment);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsPayment, value, NodeOrder);
            }
        }
        public string IsCollection
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsCollection);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsCollection, value, NodeOrder);
            }
        }
        //End SIW7036
        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public new XMLInsUnit Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLInsUnit();
                    m_Parent.Coverage = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }
        // Start SIW529
        public XMLLossDetail LossDetail
        {
            get
            {
                bool bCovHaveLD = false;
                if (m_LossDetail == null)
                {
                    m_LossDetail = new XMLLossDetail();
                    m_LossDetail.Parent = this.Parent.Parent.Parent;
                    LinkXMLObjects(m_LossDetail);
                }
                bool reset = true;
                while (m_LossDetail.getLossDetail(null, reset) != null)//SIW490
                {
                    reset = false;
                    if (m_LossDetail.PolicyID == PolicyID && m_LossDetail.PolicyCoverageLegacyKey == LegacyKey)
                    {
                        bCovHaveLD = true;
                        m_LossDetail.Coverage = this;
                        break;
                    }
                }
                if (!bCovHaveLD)
                    return null;

                return m_LossDetail;
            }
        }
        // End SIW529        
        public string PolicyID
        {
            get
            {
                // Start SIW529
                XMLPolicy m_pol = null;
                XMLInsUnit m_iUnt = null;
                XMLCoverage m_Cov = null; 
                m_pol = this.Parent.Parent; 
                LinkXMLObjects(m_pol);
                bool bPolicyReset = true;
                while (m_pol.getPolicy(bPolicyReset) != null)
                {
                    bPolicyReset = false;
                    m_iUnt = m_pol.Unit;
                    bool bInsUnitReset = true;
                    while (m_iUnt.getInsUnit(bInsUnitReset) != null)
                    {
                        bInsUnitReset = false;
                        m_Cov = m_iUnt.Coverage;
                        bool bCovReset = true;
                        while (m_Cov.getCoverage(bCovReset) != null)
                        {
                            bCovReset = false;
                            if (m_Cov.CoverageID == this.CoverageID)
                                return m_pol.PolicyID;
                        }
                    }
                }// End SIW529
                return "";
            }
        }
        // End SIW490
        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public int CoverageCount
        {
            get
            {
                return NodeList.Count;
            }
        }

        public XmlNode getCoverage(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getCoveragebyID(string sID)
        {
            if (CoverageID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (CoverageID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getCoveragebyLegacyKey(string sID)
        {
            if (LegacyKey != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (LegacyKey == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XMLLimit Limit
        {
            get
            {
                if (m_Limit == null)
                {
                    m_Limit = new XMLLimit();
                    m_Limit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Limit);
                    m_Limit.getFirst();     //SI06420
                }
                return m_Limit;
            }
            set
            {
                m_Limit = value;
            }
        }

        public XMLDeductible Deductible
        {
            get
            {
                if (m_Deductible == null)
                {
                    m_Deductible = new XMLDeductible();
                    m_Deductible.Parent = this;
                    //SIW529 LinkXMLObjects(m_Deductible);
                    m_Deductible.getFirst();    //SI06420
                }
                return m_Deductible;
            }
            set
            {
                m_Deductible = value;
            }
        }

        public XMLEndorsement Endorsement
        {
            get
            {
                if (m_Endorsement == null)
                {
                    m_Endorsement = new XMLEndorsement();
                    m_Endorsement.Parent = this;
                    //SIW529 LinkXMLObjects(m_Endorsement);
                    m_Endorsement.getEndorsement(Constants.xcGetFirst); //SI06420
                }
                return m_Endorsement;
            }
            set
            {
                m_Endorsement = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();      //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
    }
}
