﻿/*****************************************************************************
'  AdminTrackings Collection Node
'-----------------------------------------------------------------------
' SINumber |  date   | Programmer | Description
'-----------------------------------------------------------------------
'  SIN7278 | 04/07/11 | Vineet    | Create
'-----------------------------------------------------------------------
*****************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLAdminTableData : XMLXCNodeWithList
    {
        /*'*****************************************************************************
        
        '*****************************************************************************
            '<ADMIN_TRACKING>
        '   <ADMIN_TABLE NAME="xxxxxx">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '      <DATA_ITEM>
        '         NAME="Data Item Name"
        '         SUPP_FIELD="Corresponds to a supplemental table field within the table above"
        '         TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '            [data attributes corresponding to data types above as follows:
        '            BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '            DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '            TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '            CODE:  CODE="shortcode"]>
        '            [Data corresponding to the data type as follows:
        '            STRING:  data value {string}
        '            NUMBER:  data value {decimal}
        '            Date:  formatted Date
        '            TIME:  formatted time
        '      </DATA_ITEM>
        '        <!-- Multiple Data Items Allowed -->
        '      <PARTY_INVOLVED ENTITY_ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '        <!--Multiple Parties Involved-->
        '   </ADMIN_TABLE>
        '</ADMIN_TRACKING>
        '*****************************************************************************/



       // private XmlNode m_AdminTable;
        private XmlNode m_xmlAdminTable;
        private XmlNodeList m_xmlAdmTblLst; 
        private XmlNode xmlDataItem;
        private XmlNodeList xmlDataItemList;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLAdminTracking m_AdminTracking;
       

        public XMLAdminTableData()
        {

            m_xmlAdminTable = null;
            sThisNodeOrder = new ArrayList(4);             
            sThisNodeOrder.Add(Constants.sStdTechKey);
            sThisNodeOrder.Add(Constants.sAdmAdminTable);
            sThisNodeOrder.Add(Constants.sAddDataItem);
            sThisNodeOrder.Add(Constants.sPartyInvolvedNode);
         
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sAdmAdminTable; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", cxmlDataType.dtString);
        }

        public XmlNode Create(string sAdminTableName, string sDataName, string sData, string scode, cxmlDataType cDataType)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

              
                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                putAdminTable(sAdminTableName);
                putDataItem(sDataName, sData, scode, cDataType);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XML.AdminTableData.Create");
                return null;
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = null;
        }

        public new XMLAdminTracking Parent
        {
            get
            {
                if (m_AdminTracking == null)
                {
                    m_AdminTracking = new XMLAdminTracking();
                    m_AdminTracking.AdminTableData = this;
                    LinkXMLObjects((XMLXCNodeBase)m_AdminTracking);   
                    ResetParent();
                }
                return m_AdminTracking;
            }
            set
            {
                m_AdminTracking = value;
                ResetParent();
                if (m_AdminTracking != null) 
                    ((XMLXCNodeBase)m_AdminTracking).LinkXMLObjects(this);    
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            xmlDataItem = null;
            xmlNodeList = null;

            PartyInvolved = null;
            
        }


        public XmlNode putAdminTable(string vName)
        {
            return Utils.putCodeItem(putNode, Constants.sAdmName, vName, "", NodeOrder); 
        }

        public string AdminTable
        {
            get
            {   
                return XML.XMLGetAttributeValue(Node, Constants.sAdmAdminTable);
            }
            set
            {   
                XML.XMLSetAttributeValue(putNode, Constants.sAdmDataName, value);
            }
        }

        public string Techkey 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }


        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   
            }
        }




        public int DataItemCount
        {
            get
            {
                return DataItemList.Count;
            }
        }

        public XmlNodeList DataItemList
        {
            get
            {
                
                if (null == xmlNodeList)
                {
                    return getNodeList(Constants.sAddDataItem, ref xmlNodeList, Node);
                }
                else
                {
                    return xmlNodeList;
                }
               
            }
        }

        private XmlNode DataItemNode
        {
            get
            {
                if (xmlDataItem == null)
                    xmlDataItem = XML.XMLaddNode(putNode, Constants.sAddDataItem, NodeOrder);
                return xmlDataItem;
            }
        }

        public XmlNodeList NodeList
        {
            get
            { 
                if(m_xmlAdmTblLst==null)
                {
                    m_xmlAdmTblLst = XML.XMLGetNodes(Parent.Node, Constants.sAdmAdminTable);       
                }
                return m_xmlAdmTblLst;
            }
            set
            {
                NodeList = m_xmlAdmTblLst;
            }
        }

        public XmlNode getAdminTable(bool reset)
        {
            
            return getNode(reset, ref m_xmlAdmTblLst, ref m_xmlAdminTable);
        }



        public XmlNode getDataItem(bool reset)
        {
            object o = DataItemList;
            return getNode(reset, ref xmlNodeList, ref xmlDataItem);
        }

        public void removeDataItem()
        {
            if (xmlDataItem != null)
                Node.RemoveChild(xmlDataItem);
            xmlDataItem = null;
        }

        public XmlNode addDataItem(string sName, string sData, string sCode, cxmlDataType cDataType)
        {
            xmlDataItem = null;
            return putDataItem(sName, sData, sCode, cDataType);
        }

        public XmlNode putDataItem(string sName, string sData, string sCode, cxmlDataType cDataType)
        {
            DataItemName = sName;
            XML.XMLInsertVariable_Node(DataItemNode, cDataType, sData, sCode);
            return xmlDataItem;
        }

        public string DataItemName
        {
            get
            {
                string strdata = "";
                if (xmlDataItem != null)
                    strdata = XML.XMLGetAttributeValue(xmlDataItem, Constants.sAdmDataName);
                return strdata;
            }
            set
            {
                if (value != "" && value != null)
                    XML.XMLSetAttributeValue(DataItemNode, Constants.sAdmDataName, value);
            }
        }

        public cxmlDataType DataItemType
        {
            get
            {
                string stype = XML.XMLGetAttributeValue(xmlDataItem, Constants.sAdmDataType);
                return XML.getType(stype);
            }
            set
            {
                string stype = XML.XmlDataType(value);
                DataItemType_Code = stype;
            }
        }

        public string DataItemType_Code
        {
            get
            {
                return XML.XMLGetAttributeValue(xmlDataItem, Constants.sAdmDataType);
            }
            set
            {
                XML.XMLSetAttributeValue(DataItemNode, Constants.sAdmDataType, value);
            }
        }

        public cxmlDataType DataItemValues(out string sValue, out string sCode)
        {
            return XML.XMLExtractVariableDataType(xmlDataItem, out sValue, out sCode);
        }

       
    }

    public class XMLAdminTracking : XMLXCNode
    {
        /*****************************************************************************
                ' The XMLAdminTrackings class provides the functionality to support and manage a
                ' AdminTrackings Collection Node
        '*****************************************************************************
                '<ADMIN_TRACKING NEXT_ID="0" COUNT=xx">
                '  <ADMIN_TABLE NAME=""> ...  </ADMIN_TABLE>
                '</ADMIN_TRACKING>
        '*****************************************************************************/



        private XmlNode m_xmlAdminTracking;
        private XMLAdminTableData m_AdminTable;
         

        public XMLAdminTracking()
        {
            m_AdminTable = null;
            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sAdmAdminTable);

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                ResetParent(); 
            }
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sAdmGroup; }
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc)
        {
            return Create(xmlDoc, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextAdmID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                
                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                NextAdmID = sNextAdmID;
                Count = 1;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLAdminTracking.Create");
                return null;
            }
        }



        public string getNextAdmID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextAdmID;
            if (nid == "" || nid == null || nid == "0")
                nid = "1";
            NextAdmID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextAdmID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sAdmNextID);    
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null)
                    strdata = "1";
                Utils.putAttribute(putNode, "", Constants.sAdmNextID, strdata, NodeOrder);   
            }
        }

        public int Count
        {
            get
            {
                string strdata;
                strdata = Utils.getAttribute(Node, "", Constants.sAdmCount);  
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sAdmCount, value + "", NodeOrder);   
            }
        }


        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    ResetParent();
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            
            if (m_AdminTable != null)

                m_AdminTable.Parent = this;    
           
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = (XML)value;
                ResetParent();
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XmlNode AddAdminTracking()
        {
            return AdminTableData.Create();
            
        }

        public XMLAdminTableData AdminTableData
        {
             

            get
            {
                if (m_AdminTable == null)
                {
                    m_AdminTable = new XMLAdminTableData();
                    m_AdminTable.Parent = this;
                    
                }
                
                if(m_AdminTable.Node==null)
                {
                    m_AdminTable.getAdminTable(Constants.xcGetFirst);
                }
                return m_AdminTable;
            }
            set
            {
                m_AdminTable = value;
                if (m_AdminTable != null)       
                {
                    m_AdminTable.Parent = this; 
                    
                }
            }
        }

        

    
        
        
    }
}
