/**********************************************************************************************
*  SIN7227 hfw 6/16/2011 - created
*  SIN7227 AS  6/30/2011 - Updates to handle all transaction requests
*  SIN8195 Vineet 02/29/2012 - Correcting the save of Reason code ID.
***********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;

namespace CCP.XmlComponents
{
   public class XMLTransactionRequest : XMLXCNodeWithList
    {
        /*'<TRANSACTION_REQUESTS NEXT_ID="0" COUNT="xx" FUNCTION="APPROVE|REJECT|OFFSET">
        '      <TRANSACTION_REQUEST >                             
        '         <HOLD_ID>HOLD_ID</HOLD_ID>
        '         <TRANS_TYPE>RESERVE or PAYMENT</TRANS_TYPE>        
        '         <TRANS_ID>trans ID</TRANS_ID>
        '         <WITH_ADJUSTMENT_FLAG INDICATOR='YESNO"/>
        '         <COMB_PAYMENT_FLAG INDICATOR='YESNO"/>
        '         <SUCCESS_FLAG INDICATOR='YESNO"/>
        '         <OVERRIDE_FLAG INDICATOR='YESNO"/>
        '         <OVERRIDE_AMOUNT>amount</OVERRIDE_AMOUNT>
        '         <REASON CODE="X">Because></REASON>
        '         <REASON_DESC="desc">
        '         <COMMENT="comment">
        '      </TRANSACTION_REQUEST>
        '</TRANSACTION_REQUESTS>*/
        public XMLTransactionRequest()
        {
            xmlThisNode = null;
            xmlNodeList = null;

            sThisNodeOrder = new ArrayList(11);
            sThisNodeOrder.Add(Constants.sTransHoldID);
            sThisNodeOrder.Add(Constants.sTransType);
            sThisNodeOrder.Add(Constants.sTransID);
            sThisNodeOrder.Add(Constants.sTransCombPayFlag);
            sThisNodeOrder.Add(Constants.sTransWithAdj);
            sThisNodeOrder.Add(Constants.sOverrideFlag);
            sThisNodeOrder.Add(Constants.sOverrideAmount);
            sThisNodeOrder.Add(Constants.sTransReason);
            sThisNodeOrder.Add(Constants.sTransReasonDesc);
            sThisNodeOrder.Add(Constants.sTransComment);
            sThisNodeOrder.Add(Constants.sTransSuccessFlag);

            Utils.subSetupDocumentNodeOrder();
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sTransNodeName; }
        }
        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "");
        }

       public XmlNode Create(string sTransID, string sTransType, string sCombPayInd, string sWithAdjInd, string sReason_Desc, 
                                string sReasonCd, string sReasonCodeID, string sReasonDesc, string sComment)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;
           
            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                if (sTransID != "0" && sTransID != "")TransID  = sTransID;
                if (sTransType != "") TransType = sTransType;
                if (sCombPayInd != "") CombPayInd = sCombPayInd;
                if (sWithAdjInd != "") WithAdjInd = sWithAdjInd;
                if (sReasonCodeID != "") Reason_CodeID = sReasonCodeID;  //SIN8195 
                if (sReasonCd != "") putReason(sReason_Desc, sReasonCd);
               // if (sReasonCodeID != "") Reason_CodeID = sReasonCodeID;  //SIN8195: Moved above,before populating the reason code and description.                 
                if (sReasonDesc != "") ReasonDesc = sReasonDesc;
                if (sComment != "") Comment = sComment;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;  
                Debug.PopProc();
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLTransactionRequest.Create");
                Debug.PopProc();
                return null;
            }
        }

        public string TransID
        {
            get
            {
                return Utils.getData(Node, Constants.sTransID); 
            }
            set
            {
                Utils.putData(putNode, Constants.sTransID, value, NodeOrder); 
            }
        }
        public string TransType
        {
            get
            {
                return Utils.getData(Node, Constants.sTransType);
            }
            set
            {
                Utils.putData(putNode, Constants.sTransType, value, NodeOrder);
            }
        }
        public string HoldID
        {
            get
            {
                return Utils.getData(Node, Constants.sTransHoldID);
            }
            set
            {
                Utils.putData(putNode, Constants.sTransHoldID, value, NodeOrder);
            }
        }
        public string SuccessInd
        {
            get
            {
                return Utils.getBool(Node, Constants.sTransSuccessFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sTransSuccessFlag, "", value, NodeOrder);
            }
        }
        public string CombPayInd
        {
            get
            {
                return Utils.getBool(Node, Constants.sTransCombPayFlag, ""); 
            }
            set
            {
                Utils.putBool(putNode, Constants.sTransCombPayFlag,"", value, NodeOrder); 
            }
        }
        public string WithAdjInd
        {
            get
            {
                return Utils.getBool(Node, Constants.sTransWithAdj,""); 
            }
            set
            {
                Utils.putBool(putNode, Constants.sTransWithAdj, "",value, NodeOrder); 
            }
        }
        public string OverrideInd
        {
            get
            {
                return Utils.getBool(Node, Constants.sOverrideFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sOverrideFlag, "", value, NodeOrder);
            }
        }
        public string OverrideAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sOverrideAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sOverrideAmount, value, NodeOrder);
            }
        }
        public XmlNode putReason(string sDesc, string sCode)
        {
            return putReason(sDesc, sCode, "");
        }

        public XmlNode putReason(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sTransReason, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Reason
        {
            get
            {
                return Utils.getData(Node, Constants.sTransReason);
            }
            set
            {
                Utils.putData(putNode, Constants.sTransReason, value, NodeOrder);
            }
        }

        public string Reason_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sTransReason);
            }
            set
            {
                Utils.putCode(putNode, Constants.sTransReason, value, NodeOrder);
            }
        }

        public string Reason_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sTransReason);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sTransReason, value, NodeOrder);
            }
        }
         public string ReasonDesc
        {
            get
            {
                return Utils.getData(Node, Constants.sTransReasonDesc);
            }
            set
            {
                Utils.putData(putNode, Constants.sTransReasonDesc, value, NodeOrder);
            }
        }
     
         public string Comment
        {
            get
            {
                return Utils.getData(Node, Constants.sTransComment);
            }
            set
            {
                Utils.putData(putNode, Constants.sTransComment, value, NodeOrder);
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLTransactionRequests Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLTransactionRequests();
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   
                    ((XMLTransactionRequests)m_Parent).TransactionRequest = this;                    
                    ResetParent();
                }
                return (XMLTransactionRequests)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) 
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);  
            }
        }
        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            ((XMLTransactionRequests)Parent).Count = ((XMLTransactionRequests)Parent).Count + 1;
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public void removeTransactionRequest()
        {
            if (xmlThisNode != null)
                Node.RemoveChild(xmlThisNode);
            xmlThisNode = null;
        }

        public XmlNode getTransactionRequest(bool reset)
        {
            return getNode(reset);
        }
        public XmlNode getTransactionRequestbyID(string sID)
        {
            if (TransID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (TransID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }
     public class XMLTransactionRequests : XMLXCNode
    {
        /*****************************************************************************
        '<TRANSACTION_REQUESTS NEXT_ID="0" COUNT=xx" FUNCTION="APPROVE|REJECT|OFFSET">
        '  <TRANSACTION_REQUEST></TRANSACTION_REQUEST>
        '</TRANSACTION_REQUESTS>
        '*****************************************************************************/

        private XMLTransactionRequest m_TransactionRequest;

        public XMLTransactionRequests()
        {
            m_TransactionRequest = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sTransNodeName);

            Utils.subSetupDocumentNodeOrder();

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public override XmlDocument Document
        {
            get
            {
                return Parent.Document;
            }
            protected set
            {
                Parent.Document = value;
            }
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sTransNodeGroup; } 
        }

        public override XmlNode Create()
        {
            return Create(null,"", "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sFunction, string sNextTransID )
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                FunctionName = sFunction;
                NextTransID = sNextTransID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;  
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLTransactionRequests.Create");
                return null;
            }
        }

        public string getNextTransID(XmlNode xnode)
        {
            if (xnode != null)
                Node = xnode;
            string s = NextTransID;
            NextTransID = (Int32.Parse(NextTransID) + 1) + "";
            return s;
        }
        public string FunctionName
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sTransFunction);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sTransFunction, value, NodeOrder);
            }
        }
        public string NextTransID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sTransNextID);
                if (strdata == "")
                    strdata = "1";
                return strdata;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = "1";
                Utils.putAttribute(putNode, "", Constants.sTransNextID, value, NodeOrder);
            }
        }
      
        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sTransCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sTransCount, value + "", NodeOrder);
                if (NextTransID != "1") NextTransID = (Int32.Parse(NextTransID) + 1) + "";
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLGetNode(Parent.Node, NodeName);
                return xmlThisNode;
            }//End SIW529
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XmlNode AddTransactionRequest()
        {
            return TransactionRequest.Create();
        }

        private void subClearPointers()
        {
            TransactionRequest = null;
        }

        public XmlNode getTransactionRequest(XmlDocument Doc, bool reset)
        {
            if (Doc != null)
                Document = Doc;
            return TransactionRequest.getTransactionRequest(reset);
        }


        public XmlNode getTransactionRequestbyID(XmlDocument Doc, string sTransID)
        {
            if (Doc != null)
                Document = Doc;
            return TransactionRequest.getTransactionRequestbyID(sTransID);
        }

        public XMLTransactionRequest TransactionRequest
        {
            get
            {
                if (m_TransactionRequest == null)
                {
                    m_TransactionRequest = new XMLTransactionRequest();
                    m_TransactionRequest.Parent = this;
                    m_TransactionRequest.getTransactionRequest(Constants.xcGetFirst);     
                }
                return m_TransactionRequest;
            }
            set
            {
                m_TransactionRequest = value;
            }
        }
     }
}
