/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/01/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/08/2009 | SIW11b  |    KCB     | Freeze Payments
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 05/15/2009 | SIW44   |    sw      | corrected XMLUnitStat error
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 06/01/2010 | SIW481  |   SW       | Comments performance Issue.
 * 06/29/2010 | SIW344  |    AP      | Liability Loss missing from drop down on Claim Entry screen.Adding  Liability Loss screen in AC Web.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 03/21/2011 | SIW7036 |   SW       | Incurred limit in Web.                                   
 * 06/15/2011 | SIW7214    |    AV   | Add a child node Medicare to the Claim
 * 08/15/2011 | SIW7388 | umahajan2 | Add a child node Version Number to the Claim
 * 09/21/2011 | SIW7327 |   ACH       | Add claim history
 * 12/08/2011 | SIW7896 |   ubora    | Implement Missing field - Security Department in ACWeb
 * 02/29/2012|  SIW7455 |   PV       | Add claim severity code.
 * 01/30/2012 | SIW7855 |   ubora    | Add a new claim to an existing event
 * 03/14/2012 | SIN8237 |   ubora    | Added Address Reference code
 * 05/14/2012 | SIN8499 |   SW       | Entity reference OrgSec and OrgHier
 ***********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLClaim : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        ' The cXMLClaim class provides the functionality to support and manage an
        ' Advanced Claims Claim Node
        '*****************************************************************************
        
        '<CLAIM ID="CLMxxx" EVENT="EVTxxx">
        '   <CLAIM_NUMBER>00000000000065</CLAIM_NUMBER>
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '   <NEW_CLAIM_NUMBER>offset_onset Claim Number</NEW_CLAIM_NUMBER>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>     'SI06023 ' SI06333 //SIW360
        '   <FILE_NUMBER>file number</FILE_NUMBER>
        '   <CLAIM_TYPE CODE="LTI">Lost Time</CLAIM_TYPE>
        '   <DATE_OF_LOSS YEAR="2002" MONTH="03" DAY="01" />
        '   <TIME_OF_LOSS HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_DISCOVERED YEAR="2002" MONTH="03" DAY="07" />           'SI05532
        '   <DATE_REPORTED YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME_REPORTED HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_RPTD_TO_RM YEAR="2002" MONTH="03" DAY="07" />           'SIW7855
        '   <DATE_ENTERED YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME_ENTERED HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_CLOSED YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME_CLOSED HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_DESTROYED YEAR="2002" MONTH="03" DAY="07" />               'SI04599
        '   <DESTROYED INDICATOR="True|False"/>                              'SI04599
        '   <FREEZE_PAYMENTS INDICATOR="True|False"/>                        'SI06358
        '   <FINANCIAL_ACTIVITY ISFROZEN='True|False'/>                      'SIW7036
        '   <SELF_INSURED INDICATOR="True|False"/>                           'SI05300
        '   <SPECIAL_HANDLING INDICATOR = "True|False"/>                     'SI05303
        '   <LOSS_LOC_ZIP>injury site zip code</LOSS_LOC_ZIP>                'SI03247
        '   <CLOSE_METHOD CODE="***">closed method description</CLOSE_METHOD>
        '   <HOME_OFFICE_REVIEW_DATE YEAR="2002" MONTH="03" DAY="07" />
        '   <SERVICE_CODE CODE="EST">Eastern Office</CLAIM_TYPE>
        '   <ACCIDENT_DESCRIPTION CODE="**">accident description</ACCIDENT_DESCRIPTION>
        '   <SEVERITY CODE="*" ID="****">Severity_type</SEVERITY>          'SIW7455
        '   <ACCIDENT_TYPE CODE="**">accident type description</ACCIDENT_TYPE>
        '   <AIA CODE='aiacode'>                                          'SI05532
        '      <AIA1 CODE='AIA1'>description</AIA1>                       'SI05532
        '      <AIA2 CODE='AIA2'>description</AIA2>                       'SI05532
        '      <AIA3 CODE='AIA3'>description</AIA3>                       'SI05532
        '   </AIA>                                                        'SI05532
        '   <CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
        '   <ACCIDENT_STATE CODE="NC">North Carolina</ACCIDENT_STATE>
        '   <FILING_STATE CODE="NC">North Carolina</FILING_STATE>
        '   <JURISDICTION_STATE CODE="NC">North Carolina</JURISDICTION_STATE>
        '   <INDEX_BUREAU_REQUEST>cibrequestflag</INDEX_BUREAU_REQUEST>
        '   <SEC_DEPT_EID ENTITY_ID="ENTXXX" DISPLAY_NAME="ABC - XYZ"></SEC_DEPT_EID> '  SI7021 'SIW7896 'SIN8499
        '   <AUTHORITY_CONTACTED>
        '     <AUTHORITY>authority contacted</AUTHORITY>
        '     <REPORT_NUMBER>report number</REPORT_NUMBER>
        '     <STATE_DOT_NOTIFIED_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <STATE_DOT_REPORT_NUMBER>report number</REPORT_NUMBER>
        '     <FEDERAL_DOT_NOTIFIED_DATE YEAR="2002" MONTH="03" DAY="07" />
        '   </AUTHORITY_CONTACTED>
        '   <PREVIOUSLY_REPORTED INDICATOR="YES|NO"/>
        '   <STATUS_HISTORY>                                                 'Start SIW7327
        '      <STATUS_CHANGE>
        '          <!-- Multiple history lines allowed -->
        '          <TECH_KEY>technical key</TECH_KEY>
        '          <HIST_CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
        '          <CHANGE_REASON CODE="x" CODE_ID="id">reason text</CHANGE_REASON>
        '          <DATE_CHANGED YEAR="2002" MONTH="03" DAY="07" />
        '          <CHANGED_BY_USER USER_NAME="login name">user name</CHANGED_BY_USER>
        '          <APPROVED_BY_USER USER_NAME="login name">user name</APPROVED_BY_USER>
        '      </STATUS_CHANGE>
        '   </STATUS_HISTORY>                                                'End SIW7327
        '   SI05360 Start
        '   <DOCUMENT_REFERENCE ID="DOCxx"/>
        '        <!--Muliple nodes allowed -->
        '   SI05360 End
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!--Muliple nodes allowed -->
        '   Start SIN8237
        '   <ADDRESS_REFERENCE ID="ARDid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">Address Type Translated {string}</ADDRESS_REFERENCE>
        '   End SIN8237
        '   <Policy> .... </Policy>
        '        <!--Muliple nodes allowed -->
        '   <UNIT_STAT_DCI>  ....   </UNIT_STAT_DCI>
        '   <INJURY ID="INJxxx">  .....  </INJURY>
        '   <INVOLVED_UNITS> ... </INVOLVED_UNITS>
        '   <VEHICLE_LOSS> ... </VEHICLE_LOSS>
        '   <LITIGATION> .... </LITIGATION>              'Multiples allowed  SI06333(SI No not found)
        '   <SUBROGATION> .... </SUBROGATION>            'Multiples allowed  SI06333(SI No not found)
        '   <ARBITRATION> .... </ARBITRATION>            'Multiples allowed  SI06333(SI No not found)
        '   <JURISDICTIONAL_DATA> .... </JURISDICTIONAL_DATA>
        '   <INVOICE_SUMMARY> .... </INVOICE_SUMMARY>
        '   <INVOICE> .... </INVOICE>
        '   <DEMAND_OFFER> ... </DEMAND_OFFER>
        '   <LOSS_DETAIL> ... </LOSS_DETAIL>
        '   <DATA_ITEM> ... </DATA_ITEM>
        '   <TRIGGERS> ... </TRIGGERS>                                 '(SI06023 - Implemented in SI06333)
        '   <DIARY_REFERENCE ID="DIAxxx">                              '(SI06023 - Implemented in SI06333)
        '        <!-- Multiple nodes allowed -->                       '(SI06023 - Implemented in SI06333)        
        '</CLAIM>*/

        private XmlNode m_xmlAuthority;
        private XmlNode m_xmlStatusHist; //SIW7327
        private XmlNodeList m_xmlStatusHistList; //SIW7327
        private XmlNode m_xmlStatusHistItem;  //SIW7327
        private XMLPartyInvolved m_PartyInvolved;
        private XMLPolicy m_Policy;
        private XMLInjury m_Injury;
        private XMLJurisdictionData m_JurisData;
        private XMLLitigation m_Litigation;
        private XMLSubrogation m_Subrogation;
        private XMLArbitration m_Arbitration;
        private XMLCommentReference m_Comment;
        private XMLAddressReference m_AddressReference; //SIN8237
        private XMLUnitStats m_UnitStats;
        private XMLInvoice m_Invoice;
        private XMLLossDetail m_LossDetail;
        private XMLUnits m_Units;
        private XMLEvent m_Event;
        private XMLVehLoss m_VehLoss;
        private XMLPropLoss m_PropLoss;
        private XMLVarDataItem m_DataItem;
        private XMLDemandOffer m_DemandOffer;
        private XMLDocumentReference m_Document;
        private XMLTriggers m_Triggers;
        private XMLDiaryReference m_Diary;
        private ArrayList sAuthNodeOrder;
        private ArrayList sAIANodeOrder;
        private ArrayList sSHNodeOrder; //SIW7327
        private XMLLiabilityLoss m_LiabilityLoss;//SIW344
        private XMLMedicare m_Medicare;          //SIW7214
        
        public XMLClaim()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;
            m_xmlAuthority = null;
            m_xmlStatusHist = null; //SIW7327
            m_xmlStatusHistList = null; //SIW7327
            m_xmlStatusHistItem = null; //SIW7327
            //sThisNodeOrder = new ArrayList(52);//(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(53);  //SIW11b
            //sThisNodeOrder = new ArrayList(54);    //SIW11b  //SIW344 
            //sThisNodeOrder = new ArrayList(55);  //SIW344 //SIW490
            //sThisNodeOrder = new ArrayList(56);  //SIW490 //SIW7036
            //sThisNodeOrder = new ArrayList(57);  //SIW7036 //SIW7388
            //sThisNodeOrder = new ArrayList(58);   //SIW7388 //SIW7327
            //sThisNodeOrder = new ArrayList(59);  //SIW7327
            //sThisNodeOrder = new ArrayList(60);  //SIW7896 //SIW7855
            //sThisNodeOrder = new ArrayList(63);  //SIW7855 //SIN8237
            sThisNodeOrder = new ArrayList(72); //vkumar258
            sThisNodeOrder.Add(Constants.sClmNumber);
            sThisNodeOrder.Add(Constants.sClmNewNumber);
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SI06023//SI06333  //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);//SIW490
            sThisNodeOrder.Add(Constants.sClmFileNumber);
            sThisNodeOrder.Add(Constants.sClmLOB);
            sThisNodeOrder.Add(Constants.sClmType);
            sThisNodeOrder.Add(Constants.sClmStatus);
            sThisNodeOrder.Add(Constants.sClmDOL);
            sThisNodeOrder.Add(Constants.sClmTOL);
            sThisNodeOrder.Add(Constants.sClmDateDiscovered);
            sThisNodeOrder.Add(Constants.sClmDRPT);
            sThisNodeOrder.Add(Constants.sClmTRPT);
            sThisNodeOrder.Add(Constants.sClmDRPTtoRM);  //SIW7855
            sThisNodeOrder.Add(Constants.sClmDENT);
            sThisNodeOrder.Add(Constants.sClmTENT);
            sThisNodeOrder.Add(Constants.sClmDCLSD);
            sThisNodeOrder.Add(Constants.sClmTCLSD);
            sThisNodeOrder.Add(Constants.sClmDtDstryed);
            sThisNodeOrder.Add(Constants.sClmDstryed);
            sThisNodeOrder.Add(Constants.sFreezePayments);  //SIW11b

            sThisNodeOrder.Add(Constants.sClmPreparedByUser);
            sThisNodeOrder.Add(Constants.sClmUpdatedByUser);
            sThisNodeOrder.Add(Constants.sClmDttmUpdated);
            sThisNodeOrder.Add(Constants.sClmDttmAdded);

            sThisNodeOrder.Add(Constants.sStdFinancialActivity); //SIW7036    
            sThisNodeOrder.Add(Constants.sClmSelfInsrd);
            sThisNodeOrder.Add(Constants.sClmSplHndlng);
            sThisNodeOrder.Add(Constants.sClmLossLocZip);
            sThisNodeOrder.Add(Constants.sClmCLSMethod);
            sThisNodeOrder.Add(Constants.sClmDHORvw);
            sThisNodeOrder.Add(Constants.sClmServiceCode);
            sThisNodeOrder.Add(Constants.sClmAccidentDesc);
            sThisNodeOrder.Add(Constants.sClmAccidentType);
            sThisNodeOrder.Add(Constants.sClmAIA);
            sThisNodeOrder.Add(Constants.sClmAccidentState);
            sThisNodeOrder.Add(Constants.sClmFilingState);
            sThisNodeOrder.Add(Constants.sClmJurisdictionState);
            sThisNodeOrder.Add(Constants.sClmCIBRequest);
            sThisNodeOrder.Add(Constants.sClmSecDept);   //SIW7896
            sThisNodeOrder.Add(Constants.sClmAuthorityCont);
            sThisNodeOrder.Add(Constants.sClmPreviouslyReported);
            sThisNodeOrder.Add(Constants.sClmSHNode);   //SIW7327
            sThisNodeOrder.Add(Constants.sClmPolicy);
            sThisNodeOrder.Add(Constants.sClmUnitStats);
            sThisNodeOrder.Add(Constants.sClmInjury);
            sThisNodeOrder.Add(Constants.sClmInvolvedUnits);
            sThisNodeOrder.Add(Constants.sClmVehLoss);
            sThisNodeOrder.Add(Constants.sClmPropLoss);
            //Start SIW7455
            sThisNodeOrder.Add(Constants.sClmSeverity);
            //end SIW7455
            //sThisNodeOrder.Add(Constants.sClmLitigation);//Start SI06333(SI No not found)
            sThisNodeOrder.Add(Constants.sLITNode);
            //sThisNodeOrder.Add(Constants.sClmSubrogation);
            sThisNodeOrder.Add(Constants.sSUBNode);
            //sThisNodeOrder.Add(Constants.sClmArbitration);
            sThisNodeOrder.Add(Constants.sARBNode);//End SI06333(SI No not found) 
            sThisNodeOrder.Add(Constants.sClmLiabLoss);       //SIW344
            sThisNodeOrder.Add(Constants.sClmJurisdictionalData);
            sThisNodeOrder.Add(Constants.sClmInvSummary);
            sThisNodeOrder.Add(Constants.sClmInvoice);
            sThisNodeOrder.Add(Constants.sClmDemandOffer);
            sThisNodeOrder.Add(Constants.sClmLossDetail);
            sThisNodeOrder.Add(Constants.sClmPartyInvolved);
            sThisNodeOrder.Add(Constants.sClmComment);
            sThisNodeOrder.Add(Constants.sAddAddressReference); //SIN8237
            sThisNodeOrder.Add(Constants.sClmDataItem);
            sThisNodeOrder.Add(Constants.sClmDoc);
            sThisNodeOrder.Add(Constants.sClmTrigger);
            sThisNodeOrder.Add(Constants.sDiaReference);
            sThisNodeOrder.Add(Constants.sClmMedicare);  //SIW7214

            sThisNodeOrder.Add(Constants.sClmMedicare);
            sThisNodeOrder.Add(Constants.sClmMedicare);
            sThisNodeOrder.Add(Constants.sClmMedicare);
            sThisNodeOrder.Add(Constants.sClmMedicare); 


            sThisNodeOrder.Add(Constants.sClmPreparedByUser);
   sThisNodeOrder.Add(Constants.sClmUpdatedByUser);
   sThisNodeOrder.Add(Constants.sClmDttmAdded);
   sThisNodeOrder.Add( Constants.sClmDttmUpdated);
   sThisNodeOrder.Add( "IS_WC_CLAIM");
 sThisNodeOrder.Add("FACT_SUMMARY_COMMENT_ID");
 sThisNodeOrder.Add( "DAMAGE_COMMENT_ID");
 sThisNodeOrder.Add( "CURRENT_STATUS_COMMENT_ID");


            //sThisNodeOrder.Add(Constants.sDiaTechKey);                      //(SI06023 - Implemented in SI06333) //SIW360
            sThisNodeOrder.Add(Constants.sClmVersionNumber); //SIW7388
            sAuthNodeOrder = new ArrayList(5);
            sAuthNodeOrder.Add(Constants.sClmACAuthority);
            sAuthNodeOrder.Add(Constants.sClmACReportNumber);
            sAuthNodeOrder.Add(Constants.sClmACStDOTNotDate);
            sAuthNodeOrder.Add(Constants.sClmACStDOTRprtNumber);
            sAuthNodeOrder.Add(Constants.sClmACFedDOTNotDate);

            sAIANodeOrder = new ArrayList(3);
            sAIANodeOrder.Add(Constants.sClmAIA1);
            sAIANodeOrder.Add(Constants.sClmAIA2);
            sAIANodeOrder.Add(Constants.sClmAIA3);
            //Start SIW7327
            sSHNodeOrder = new ArrayList(6);
            sSHNodeOrder.Add(Constants.sStdTechKey);
            sSHNodeOrder.Add(Constants.sClmStatus);
            sSHNodeOrder.Add(Constants.sClmSHReason);
            sSHNodeOrder.Add(Constants.sClmSHDateChanged);
            sSHNodeOrder.Add(Constants.sClmSHChangedBy);
            sSHNodeOrder.Add(Constants.sClmSHApprovedBy);
            //End SIW7327
            sSHNodeOrder.Add(Constants.sClmTotalClaimReserveBalance); //vkumar258
            Utils.subSetupDocumentNodeOrder();
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sClmNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "","");
        }

        public XmlNode Create(string sID, string sclmnumber, string sType, string stypecode, string sStatus, string sStatusCode,
                              string sdol, string stol, string sdrpt, string strpt, string saccddesc, string saccddesccode,
                              string saccdstate, string saccdstatecode, string sClmTotalClaimReserve)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ClaimID = sID;
                ClaimNumber = sclmnumber;
                DateOfLoss = sdol;
                TimeOfLoss = stol;
                DateReported = sdrpt;
                TimeReported = strpt;
                putClaimType(sType, stypecode);
                putClaimStatus(sStatus, sStatusCode);
                putAccidentDescription(saccddesc, saccddesccode);
                putAccidentState(saccdstate, saccdstatecode);
                TotalClaimReserveBalance = sClmTotalClaimReserve.ToString();//vkumar258
                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLClaim.Create");
                return null;
            }
        }

        public string ClaimNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sClmNumber);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmNumber, value, NodeOrder);    //SIW529 Remove local XML parameter
                
            }
        }

        public string Examiner
        {
            get
            {
                return Utils.getData(Node, "EXAMINER");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "EXAMINER", value, NodeOrder);    //SIW529 Remove local XML parameter

            }
        }
        public string ClaimCatastrophe
        {
            get
            {
                return Utils.getData(Node, "ClaimCatastrophe");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "ClaimCatastrophe", value, NodeOrder);    //SIW529 Remove local XML parameter

            }
        }
        public string ExaminerCd
        {
            get
            {
                return Utils.getData(Node, "EXAMINERCD");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "EXAMINERCD", value, NodeOrder);    //SIW529 Remove local XML parameter

            }
        }

        public string ActivityId
        {
            get
            {
                return Utils.getData(Node, "ActivityId");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "ActivityId", value, NodeOrder);    //SIW529 Remove local XML parameter

            }
        }


        public string FactSummaryCommentID
        {
            get
            {
                return Utils.getData(Node, "FACT_SUMMARY_COMMENT_ID");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "FACT_SUMMARY_COMMENT_ID", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DamageCommentId
        {
            get
            {
                return Utils.getData(Node, "DAMAGE_COMMENT_ID");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "DAMAGE_COMMENT_ID", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string CurrenStatusCommentId
        {
            get
            {
                return Utils.getData(Node, "CURRENT_STATUS_COMMENT_ID");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, "CURRENT_STATUS_COMMENT_ID", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }



        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sClmPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmPreparedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sClmUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sClmDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sClmDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }




        public string NewClaimNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sClmNewNumber);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmNewNumber, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string FileNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sClmFileNumber);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmFileNumber, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sClmIDPfx;
            }
        }

        public string ClaimID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sClmID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sClmIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lClmId = Globals.lClmId + 1;
                    lid = Globals.lClmId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sClmID, Constants.sClmIDPfx + lid);
            }
        }

        public string EventID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sClmEventID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sClmEventIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sClmEventID, Constants.sClmEventIDPfx + value);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if(nde != null)
                Node = nde;
            AddClaim();
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
                ResetParent();
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        protected override void ResetParent()
        {
            Node = null;
            xmlNodeList = null;
        }

        private void subClearPointers()
        {
            Policy = null;
            Injury = null;
            UnitStats = null;
            PartyInvolved = null;
            CommentReference = null;
            JurisdictionalData = null;
            Litigation = null;
            Subrogation = null;
            Arbitration = null;
            Invoice = null;
            LossDetail = null;
            InvolvedUnits = null;
            VehicleLoss = null;
            PropertyLoss = null;
            DataItem = null;
            DemandOffer = null;
            m_xmlAuthority = null;
            m_xmlStatusHist = null; //SIW7327
            m_xmlStatusHistList = null; //SIW7327
            m_xmlStatusHistItem = null; //SIW7327
            DocumentReference = null;
            Triggers = null;
            DiaryReference = null;
            LiabilityLoss = null;//SIW344
            Medicare = null; //SIW7214
        }
        
        //Start SIW139
        public XmlNode putLOB(string sDesc, string sCode)
        {
            return putLOB(sDesc, sCode, "");
        }

        public XmlNode putLOB(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmLOB, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string LOB
        {
            get
            {
                return Utils.getData(Node, Constants.sClmLOB);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmLOB, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string LOB_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmLOB);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmLOB, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string LOB_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmLOB);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmLOB, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139
        //Start SIW7455
        public string Severity
        {
            get
            {
                return Utils.getData(Node, Constants.sClmSeverity);    
            }
            set
            {
                Utils.putData(putNode, Constants.sClmSeverity, value, NodeOrder);    
            }
        }

        public string Severity_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmSeverity);   
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmSeverity, value, NodeOrder);    
            }
        }
        //end SIW7455
        //Start SIW139
        public XmlNode putClaimStatus(string sDesc, string sCode)
        {
            return putClaimStatus(sDesc, sCode, "");
        }

        public XmlNode putClaimStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmStatus, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string ClaimStatus
        {
            get
            {
                return Utils.getData(Node, Constants.sClmStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ClaimStatus_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ClaimStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCloseMethod(string sDesc, string sCode)
        {
            return putCloseMethod(sDesc, sCode, "");
        }

        public XmlNode putCloseMethod(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmCLSMethod, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string CloseMethod
        {
            get
            {
                return Utils.getData(Node, Constants.sClmCLSMethod);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmCLSMethod, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string CloseMethod_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmCLSMethod);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmCLSMethod, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string CloseMethod_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmCLSMethod);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmCLSMethod, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putServiceCode(string sDesc, string sCode)
        {
            return putServiceCode(sDesc, sCode, "");
        }

        public XmlNode putServiceCode(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmServiceCode, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string ServiceCode
        {
            get
            {
                return Utils.getData(Node, Constants.sClmServiceCode);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmServiceCode, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ServiceCode_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmServiceCode);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmServiceCode, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ServiceCode_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmServiceCode);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmServiceCode, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putClaimType(string sDesc, string sCode)
        {
            return putClaimType(sDesc, sCode, "");
        }
            
        public XmlNode putClaimType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmType, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string ClaimType
        {
            get
            {
                return Utils.getData(Node, Constants.sClmType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ClaimType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ClaimType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putAccidentDescription(string sDesc, string sCode)
        {
            return putAccidentDescription(sDesc, sCode, "");
        }

        public XmlNode putAccidentDescription(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmAccidentDesc, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string AccidentDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sClmAccidentDesc);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmAccidentDesc, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentDescription_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmAccidentDesc);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmAccidentDesc, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentDescription_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmAccidentDesc);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmAccidentDesc, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putAccidentType(string sDesc, string sCode)
        {
            return putAccidentType(sDesc, sCode, "");
        }

        public XmlNode putAccidentType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmAccidentType, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string AccidentType
        {
            get
            {
                return Utils.getData(Node, Constants.sClmAccidentType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmAccidentType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmAccidentType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmAccidentType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmAccidentType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmAccidentType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public XmlNode AIANode
        {
            get
            {
                return Utils.getNode(Node, Constants.sClmAIA);    //SIW529 Remove local XML parameter
            }
        }

        public XmlNode putAIANode
        {
            get
            {
                if (AIANode == null)
                    XML.XMLaddNode(putNode, Constants.sClmAIA, NodeOrder);
                return AIANode;
            }
        }

        public string AIA
        {
            get
            {
                return Utils.getCode(AIANode, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putAIANode, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Start SIW139
        public string AIA_CodeID
        {
            get
            {
                return Utils.getCodeID(AIANode, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putAIANode, "", value, sAIANodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public XmlNode putAIAn(string sdesc, string scode, int i)
        {
            return putAIAn(sdesc, scode, i, "");
        }

        public XmlNode putAIAn(string sdesc, string scode, int i, string scodeid)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                return Utils.putCodeItem(putAIANode, s, sdesc, scode, sAIANodeOrder, scodeid);    //SIW529 Remove local XML parameter
            }
            return null;
        }

        public string AIAn(int i)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                return Utils.getData(AIANode, s);    //SIW529 Remove local XML parameter
            }
            return "";
        }

        public void AIAn(int i, string nval)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                Utils.putData(putAIANode, s, nval, sAIANodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AIAnCode(int i)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                return Utils.getCode(AIANode, s);    //SIW529 Remove local XML parameter
            }
            return "";
        }

        public void AIAnCode(int i, string nval)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                Utils.putCode(putAIANode, s, nval, sAIANodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AIAnCodeID(int i)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                return Utils.getCodeID(AIANode, s);    //SIW529 Remove local XML parameter
            }
            return "";
        }

        public void AIAnCodeID(int i, string nval)
        {
            string s;
            if (i >= 1 && i <= 3)
            {
                s = Constants.sClmAIA + i;
                Utils.putCodeID(putAIANode, s, nval, sAIANodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string PreviouslyReported
        {
            get
            {
                return Utils.getBool(Node, Constants.sClmPreviouslyReported, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, Constants.sClmPreviouslyReported, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Start SIW139
        public XmlNode putAccidentState(string sDesc, string sCode)
        {
            return putAccidentState(sDesc, sCode, "");
        }

        public XmlNode putAccidentState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmAccidentState, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string AccidentState
        {
            get
            {
                return Utils.getData(Node, Constants.sClmAccidentState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmAccidentState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmAccidentState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmAccidentState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AccidentState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmAccidentState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmAccidentState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putJurisdictionState(string sDesc, string sCode)
        {
            return putJurisdictionState(sDesc, sCode, "");
        }

        public XmlNode putJurisdictionState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmJurisdictionState, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string JurisdictionState
        {
            get
            {
                return Utils.getData(Node, Constants.sClmJurisdictionState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmJurisdictionState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string JurisdictionState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmJurisdictionState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmJurisdictionState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string JurisdictionState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmJurisdictionState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmJurisdictionState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putFilingState(string sDesc, string sCode)
        {
            return putFilingState(sDesc, sCode, "");
        }

        public XmlNode putFilingState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sClmFilingState, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string FilingState
        {
            get
            {
                return Utils.getData(Node, Constants.sClmFilingState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmFilingState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string FilingState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sClmFilingState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sClmFilingState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string FilingState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sClmFilingState);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sClmFilingState, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string DateOfLoss
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDOL);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDOL, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateDiscovered
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDateDiscovered);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDateDiscovered, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateReported
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDRPT);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDRPT, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        // Start SIW7855
        public string DateRptdToRm
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDRPTtoRM);
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDRPTtoRM, value, NodeOrder);
            }
        }
        // End SIW7855

        public string HomeOfficeReviewDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDHORvw);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDHORvw, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateClosed
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDCLSD);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDCLSD, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateEntered
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDENT);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDENT, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DateDestroyed
        {
            get
            {
                return Utils.getDate(Node, Constants.sClmDtDstryed);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sClmDtDstryed, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Destroyed
        {
            get
            {
                return Utils.getBool(Node, Constants.sClmDstryed, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, Constants.sClmDstryed, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Begin SIW11b
        public string FreezePayments
        {
            get
            {
                return Utils.getBool(Node, Constants.sFreezePayments, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, Constants.sFreezePayments, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW11b
        //Start SIW7036
        //<FINANCIAL_ACTIVITY ISFROZEN='True|False'/>
        public string IsFrozen
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sStdFinancialActivity, Constants.sStdIsFrozen);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sStdFinancialActivity, Constants.sStdIsFrozen, value, NodeOrder);
            }
        }
        //End SIW7036
        public string SelfInsured
        {
            get
            {
                return Utils.getBool(Node, Constants.sClmSelfInsrd, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, Constants.sClmSelfInsrd, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string SplHandling
        {
            get
            {
                return Utils.getBool(Node, Constants.sClmSplHndlng, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, Constants.sClmSplHndlng, "", value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

         public string IsWcClaim
        {
            get
            {
                return Utils.getBool(Node, "IS_WC_CLAIM", "");   
            }
            set
            {
                Utils.putBool(putNode, "IS_WC_CLAIM", "", value, NodeOrder);  
            }
        }
        public string LossLocZip
        {
            get
            {
                return Utils.getData(Node, Constants.sClmLossLocZip);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmLossLocZip, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string TimeOfLoss
        {
            get
            {
                return Utils.getTime(Node, Constants.sClmTOL);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putTime(putNode, Constants.sClmTOL, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string TimeReported
        {
            get
            {
                return Utils.getTime(Node, Constants.sClmTRPT);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putTime(putNode, Constants.sClmTRPT, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string TimeClosed
        {
            get
            {
                return Utils.getTime(Node, Constants.sClmTCLSD);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putTime(putNode, Constants.sClmTCLSD, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string TimeEntered
        {
            get
            {
                return Utils.getTime(Node, Constants.sClmTENT);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putTime(putNode, Constants.sClmTENT, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string IndexBureauRqst
        {
            get
            {
                return Utils.getData(Node, Constants.sClmCIBRequest);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sClmCIBRequest, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        // Start SIW7896
        public string SecDeptEID
        {
            get
            {
                //SIN8499 return Utils.getAttribute(Node, Constants.sClmSecDept, Constants.sOrgSecID);
                return Utils.getEntityRef(Node, Constants.sClmSecDept);   
            }
            set
            {
                //SIN8499 Utils.putAttribute(putNode, Constants.sClmSecDept, Constants.sOrgSecID, value, NodeOrder);
                Utils.putEntityRef(putNode, Constants.sClmSecDept, value, NodeOrder);   
            }
        }       
        public string SecDeptEntDisplayName
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sClmSecDept, Constants.sEntDisplayName); //SIN8499
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sClmSecDept, Constants.sEntDisplayName, value, NodeOrder); //SIN8499
            }
        }
        // End SIW7896
        private XmlNode ACNode
        {
            get
            {
                if (m_xmlAuthority == null)
                    m_xmlAuthority = Utils.getNode(Node, Constants.sClmAuthorityCont);    //SIW529 Remove local XML parameter
                return m_xmlAuthority;
            }
        }

        private XmlNode putACNode
        {
            get
            {
                if (ACNode == null)
                    m_xmlAuthority = XML.XMLaddNode(putNode, Constants.sClmAuthorityCont, NodeOrder);
                return m_xmlAuthority;
            }
        }

        public XmlNode putAuthorityContacted(string sauthority, string sreport)
        {
            AuthorityContacted = sauthority;
            ReportNumber = sreport;
            return m_xmlAuthority;
        }

        public string AuthorityContacted
        {
            get
            {
                return Utils.getData(ACNode, Constants.sClmACAuthority);
            }
            set
            {
                Utils.putData(putACNode, Constants.sClmACAuthority, value, sAuthNodeOrder);
            }
        }

        public string ReportNumber
        {
            get
            {
                return Utils.getData(ACNode, Constants.sClmACReportNumber);
            }
            set
            {
                Utils.putData(putACNode, Constants.sClmACReportNumber, value, sAuthNodeOrder);
            }
        }

        public string DateReportedToStateDOT
        {
            get
            {
                return Utils.getDate(ACNode, Constants.sClmACStDOTNotDate);
            }
            set
            {
                Utils.putDate(putACNode, Constants.sClmACStDOTNotDate, value, sAuthNodeOrder);
            }
        }

        public string StateDotReportNumber
        {
            get
            {
                return Utils.getData(ACNode, Constants.sClmACStDOTRprtNumber);
            }
            set
            {
                Utils.putData(putACNode, Constants.sClmACStDOTRprtNumber, value, sAuthNodeOrder);
            }
        }

        public string DateReportedToFederalDOT
        {
            get
            {
                return Utils.getDate(ACNode, Constants.sClmACFedDOTNotDate);
            }
            set
            {
                Utils.putDate(putACNode, Constants.sClmACFedDOTNotDate, value, sAuthNodeOrder);
            }
        }

        //Start SIW7388
        public string VersionNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sClmVersionNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sClmVersionNumber, value, NodeOrder);
            }
        }
        //End SIW7388
        //Start SIW7327
        public XmlNodeList StatusHistoryList
        {
            get
            {
                if (m_xmlStatusHistList == null)
                {
                    m_xmlStatusHistList = XML.XMLGetNodes(StatusHistoryNode, Constants.sClmSHHistItem);
                }
                return m_xmlStatusHistList;
            }
        }
        public XmlNode StatusHistoryNode 
        {
            get
            {
                if (m_xmlStatusHist == null)
                {
                    m_xmlStatusHist = XML.XMLGetNode(Node, Constants.sClmSHNode);
                }
                return m_xmlStatusHist;
            }
            set
            {
                m_xmlStatusHist = null;
                m_xmlStatusHist = value;
            }
        }
        public XmlNode putStatusHistoryNode
        {
            get
            {
                if (StatusHistoryNode==null)
                {
                    StatusHistoryNode = XML.XMLaddNode(putNode, Constants.sClmSHNode, sThisNodeOrder);
                    StatusHistoryItemCount = 0;
                }
                return StatusHistoryNode;
            }
        }
        public int StatusHistoryItemCount 
        { 
            get 
            {
                string strData;
                strData = Utils.getAttribute(StatusHistoryNode, "", Constants.sClmSHHistItemCount);
                StatusHistoryItemCount = Convert.ToInt32(strData);
                return StatusHistoryItemCount;
            }
            set
            {
                Utils.putAttribute(putStatusHistoryNode, "", Constants.sClmSHHistItemCount, value.ToString(), sSHNodeOrder);
            }
        }
        public XmlNode getStatusHistoryItem(bool reset)
        {
            if (null != StatusHistoryList)
            {
                return getNode(reset, ref m_xmlStatusHistList, ref m_xmlStatusHistItem);
            }
            else
            {
                return null;
            }
        }

        public XmlNode putStatusHistoryItemNode
        {
            get
            {
                if (StatusHistoryItemNode == null)
                {
                    StatusHistoryItemNode = XML.XMLaddNode(putStatusHistoryNode, Constants.sClmSHHistItem, sSHNodeOrder);
                }
                return StatusHistoryItemNode;
            }

        }
        public XmlNode StatusHistoryItemNode 
        {
            get
            {
                //StatusHistoryItemNode = m_xmlStatusHistItem;
                //return StatusHistoryItemNode;
                return m_xmlStatusHistItem;
            }
            set
            {
                m_xmlStatusHistItem=null;
                m_xmlStatusHistItem=value;
            }
        }
        public XmlNode addStatusHistoryItem
        {
            get
            {
                StatusHistoryItemNode = null;
                StatusHistoryItemCount += 1;
                return putStatusHistoryItemNode;
            }
        }
        public XmlNode putStatusHistoryItem()
        {
            return putStatusHistoryItem("","","","","","","","");
        }
        public XmlNode putStatusHistoryItem(string sSHTechKey,string sReason,string sDateChanged,string sClmStatusHst, string sClmStatusHstCode,string sClmStatusHstCodeID,string sChangedBy,string sApprovedBy)
        {
            if (!sSHTechKey.Equals(string.Empty)) SHTechkey = sSHTechKey;
            if (!sReason.Equals(string.Empty)) StatusChangeReason = sReason;
            if (!sDateChanged.Equals(string.Empty)) SHDateChg = sDateChanged;
            if (!sClmStatusHst.Equals(string.Empty) || !sClmStatusHstCode.Equals(string.Empty) || !sClmStatusHstCodeID.Equals(string.Empty))
            {
                putSHClmStatus(sClmStatusHst, sClmStatusHstCode, sClmStatusHstCodeID);
            }
            if (!sChangedBy.Equals(string.Empty)) ClmStatusChangedBy = sChangedBy;
            if (!sApprovedBy.Equals(string.Empty)) ClmStatusApprovedBy = sApprovedBy;
            return StatusHistoryItemNode;
        }
        public XmlNode putSHClmStatus(string sStatus,string sStatusCode)
        {
            return putSHClmStatus(sStatus, sStatusCode, "");
        }
        public XmlNode putSHClmStatus(string sStatus, string sStatusCode, string sCodeId)
        {
            return Utils.putCodeItem(putStatusHistoryItemNode, Constants.sClmSHStatus, sStatus, sStatusCode, sSHNodeOrder, sCodeId);
        }
        public string SHClmStatus 
        {
            get
            {
                return Utils.getData(StatusHistoryItemNode, Constants.sClmSHStatus);
            }
            set
            {
                Utils.putData(putStatusHistoryItemNode, Constants.sClmSHStatus, value, sSHNodeOrder);
            }
        }
        public string SHClmStatus_Code
        {
            get
            {
                return Utils.getCode(StatusHistoryItemNode, Constants.sClmSHStatus);
            }
            set
            {
                Utils.putCode(putStatusHistoryItemNode, Constants.sClmSHStatus, value, sSHNodeOrder);
            }
        }
        public string SHClmStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(StatusHistoryItemNode, Constants.sClmSHStatus);
            }
            set
            {
                Utils.putCodeID(putStatusHistoryItemNode, Constants.sClmSHStatus, value, sSHNodeOrder);
            }
        }
        public string StatusChangeReason 
        {
            get
            {
                return Utils.getData(StatusHistoryItemNode, Constants.sClmSHReason);
            }
            set
            {
                Utils.putData(putStatusHistoryItemNode, Constants.sClmSHReason, value, sSHNodeOrder);
            }
        }
        public string SHDateChg 
        {
            get
            {
                return Utils.getDate(StatusHistoryItemNode, Constants.sClmSHDateChanged);
            }
            set
            {
                Utils.putDate(putStatusHistoryItemNode, Constants.sClmSHDateChanged, value, sSHNodeOrder);
            }
        }
        public string ClmStatusChangedBy 
        { 
            get
            {
                return Utils.getData(StatusHistoryItemNode, Constants.sClmSHChangedBy);
            }
            set
            {
                Utils.putData(putStatusHistoryItemNode, Constants.sClmSHChangedBy, value, sSHNodeOrder);
            }
        }
        public string ClmStatusApprovedBy 
        {
            get
            {
                return Utils.getData(StatusHistoryItemNode, Constants.sClmSHApprovedBy);
            }
            set
            {
                Utils.putData(putStatusHistoryItemNode, Constants.sClmSHApprovedBy, value, sSHNodeOrder);
            }
        }
        public string SHTechkey 
        {
            get
            {
                return Utils.getData(StatusHistoryItemNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putStatusHistoryItemNode, Constants.sStdTechKey, value, sSHNodeOrder);
            }
        }
        //End SIW7327
        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLDiaryReference DiaryReference
        {
            get
            {
                if (m_Diary == null)
                    m_Diary = ((Utils)Utils).funSetupDiaryReference(m_Diary, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_Diary;
            }
            set
            {
                m_Diary = value;
            }
        }

        public XMLDocumentReference DocumentReference
        {
            get
            {
                if (m_Document == null)
                {
                    m_Document = ((Utils)Utils).funSetupDocumentReference(m_Document, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Document.getFirst();
                }
                return m_Document;
            }
            set
            {
                m_Document = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        //Start SIN8237
        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);
            }
        }
        //End SIN8237

        public XMLPolicy Policy
        {
            get
            {
                if (m_Policy == null)
                {
                    m_Policy = new XMLPolicy();
                    m_Policy.Parent = this;
                    //SIW529 LinkXMLObjects(m_Policy);
                    m_Policy.getPolicy(Constants.xcGetFirst);    //SI06420
                }
                return m_Policy;
            }
            set
            {
                m_Policy = value;

            }
        }

        public XMLUnitStats UnitStats
        {
            get
            {
                if (m_UnitStats == null)
                {
                    m_UnitStats = new XMLUnitStats();
                    m_UnitStats.Parent = this;
                    //LinkXMLObjects(m_Units);//SIW44
                    //SIW529 LinkXMLObjects(m_UnitStats);//SIW44
                    //Note: Only 1 Allowed      //SI06420
                }
                return m_UnitStats;
            }
            set
            {
                m_UnitStats = value;
            }
        }

        public XMLInjury Injury
        {
            get
            {
                if (m_Injury == null)
                {
                    m_Injury = new XMLInjury();
                    m_Injury.Parent = this;
                    //SIW529 LinkXMLObjects(m_Injury);
                    m_Injury.getInjury(Constants.xcGetFirst);   //SI06420
                }
                return m_Injury;
            }
            set
            {
                m_Injury = value;
            }
        }

        public XMLInvoice Invoice
        {
            get
            {
                if (m_Invoice == null)
                {
                    m_Invoice = new XMLInvoice();
                    m_Invoice.Parent = this;
                    //SIW529 LinkXMLObjects(m_Invoice);
                    m_Invoice.getInvoice(Constants.xcGetFirst);     //SI06420
                }
                return m_Invoice;
            }
            set
            {
                m_Invoice = value;
            }
        }

        public XMLJurisdictionData JurisdictionalData
        {
            get
            {
                if (m_JurisData == null)
                {
                    m_JurisData = new XMLJurisdictionData();
                    m_JurisData.Parent = this;
                    //SIW529 LinkXMLObjects(m_JurisData);
                    m_JurisData.getDataItem(Constants.xcGetFirst);  //SI06420
                }
                return m_JurisData;
            }
            set
            {
                m_JurisData = value;
            }
        }

        public XMLLitigation Litigation
        {
            get
            {
                if (m_Litigation == null)
                {
                    m_Litigation = new XMLLitigation();
                    m_Litigation.Parent = this;
                    //SIW529 LinkXMLObjects(m_Litigation);
                    m_Litigation.getFirst();
                }
                return m_Litigation;
            }
            set
            {
                m_Litigation = value;
            }
        }

        public XMLSubrogation Subrogation
        {
            get
            {
                if (m_Subrogation == null)
                {
                    m_Subrogation = new XMLSubrogation();
                    m_Subrogation.Parent = this;
                    //SIW529 LinkXMLObjects(m_Subrogation);
                    m_Subrogation.getFirst();
                }
                return m_Subrogation;
            }
            set
            {
                m_Subrogation = value;
            }
        }

        public XMLArbitration Arbitration
        {
            get
            {
                if (m_Arbitration == null)
                {
                    m_Arbitration = new XMLArbitration();
                    m_Arbitration.Parent = this;
                    //SIW529 LinkXMLObjects(m_Arbitration);
                    m_Arbitration.getFirst();
                }
                return m_Arbitration;
            }
            set
            {
                m_Arbitration = value;
            }
        }

        public XMLLossDetail LossDetail
        {
            get
            {
                if (m_LossDetail == null)
                {
                    m_LossDetail = new XMLLossDetail();
                    m_LossDetail.Parent = this;
                    //SIW529 LinkXMLObjects(m_LossDetail);
                    m_LossDetail.getLossDetail(null, Constants.xcGetFirst);     //SI06420
                }
                return m_LossDetail;
            }
            set
            {
                m_LossDetail = value;
            }
        }

        public XMLTriggers Triggers
        {
            get
            {
                if (m_Triggers == null)
                {
                    m_Triggers = new XMLTriggers();
                    m_Triggers.Parent = this;
                    //SIW529 LinkXMLObjects(m_Triggers);
                    m_Triggers.getTrigger(null, Constants.xcGetFirst);    //SI06420
                }
                return m_Triggers;
            }
            set
            {
                m_Triggers = value;
            }
        }

        public XMLUnits InvolvedUnits
        {
            get
            {
                if (m_Units == null)
                {
                    m_Units = new XMLUnits();
                    m_Units.Parent = this;
                    //SIW529 LinkXMLObjects(m_Units);
                    m_Units.getUnit(null, Constants.xcGetFirst);  //SI06420
                }
                return m_Units;
            }
            set
            {
                m_Units = value;
            }
        }

        public XMLVehLoss VehicleLoss
        {
            get
            {
                if (m_VehLoss == null)
                {
                    m_VehLoss = new XMLVehLoss();
                    m_VehLoss.Parent = this;
                    //SIW529 LinkXMLObjects(m_VehLoss);
                    m_VehLoss.getVehLoss(Constants.xcGetFirst);  //SI06420
                }
                return m_VehLoss;
            }
            set
            {
                m_VehLoss = value;
            }
        }

        public XMLPropLoss PropertyLoss
        {
            get
            {
                if (m_PropLoss == null)
                {
                    m_PropLoss = new XMLPropLoss();
                    m_PropLoss.Parent = this;
                    //SIW529 LinkXMLObjects(m_PropLoss);
                    m_PropLoss.getPropLoss(Constants.xcGetFirst);   //SI06420
                }
                return m_PropLoss;
            }
            set
            {
                m_PropLoss = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();   //SI06420
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }

        //Start SIW7214
        public XMLMedicare Medicare
        {
            get
            {
                if (m_Medicare == null)
                {
                    m_Medicare = new XMLMedicare();
                    m_Medicare.Parent = this;
                    m_Medicare.getMedicare(Constants.xcGetFirst);
                }
                return m_Medicare;
            }
            set
            {
                m_Medicare = value;
            }
        }
        //End SIW7214
        public XMLEvent RelatedEvent
        {
            get
            {
                if (m_Event == null)
                {   //SIW529 
                    m_Event = new XMLEvent();
                    LinkXMLObjects(m_Event);    //SIW529
                }   //SIW529 
                m_Event.FindEventbyID(EventID, null);
                return m_Event;
            }
        }

        public void AddClaim()
        {
            XmlNode xmlNewNode = Node;
            if (FindClaimbyID(ClaimID, null) == null)
                XML.XMLInsertNodeInPlace(Parent_Node, xmlNewNode, Parent_NodeOrder);
            else
                Parent_Node.ReplaceChild(xmlNewNode, Node);
            Node = xmlNewNode;
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getClaim(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return getNode(reset);
        }
        //Start SIW481
        public XmlNode FindClaim()
        {
            return FindClaim("", "", "", null); 
        }
        public XmlNode FindClaim(string ClmId, string TchKy, string ClmNb, XmlDocument Doc)
        {
            bool rst = true;
            XmlDocument xDoc = Doc;
            getNode(rst);

            if (ClmId != "" && ClmId == ClaimID || TchKy != "" && TchKy == Techkey || ClmNb != "" && ClmNb == ClaimNumber)
                return Node;
            
            while (getClaim(xDoc, rst) != null)
            {
                rst = false;
                xDoc = null;
                if (TchKy != "")
                    if (Techkey == TchKy)
                        break;
                if (ClmNb != "")
                    if (ClaimNumber == ClmNb)
                        break;
                if (ClmId != "")
                    if (ClaimID == ClmId)
                        break;
            }
            return Node;
        }
        public XmlNode FindClaimbyTechID(string TechID)
        {
            return FindClaimbyTechID(TechID, null);
        }
        public XmlNode FindClaimbyTechID(string TechID, XmlDocument Doc)
        {
            bool rst = true;
            XmlDocument xdoc = Doc;
            getNode(rst);
            if (TechID != Techkey)
            {
                while (getClaim(xdoc, rst) != null)
                {
                    rst = false;
                    xdoc = null;
                    if (ClaimID == TechID)
                        break;
                }
            }
            return Node;
        }

        public XmlNode FindClaimbyID(string ClmID)
        {
            return FindClaimbyID(ClmID, null);  
        }
        public XmlNode FindClaimbyID(string ClmID, XmlDocument Doc)
        {
            bool rst = true;
            XmlDocument xdoc = Doc;
            getNode(rst); 
            if (ClmID != ClaimID)
            {
                while (getClaim(xdoc, rst) != null)
                {
                    rst = false;
                    xdoc = null;
                    if (ClaimID == ClmID)
                        break;
                }
            }
            return Node;
        }

        public XmlNode FindClaimbyNumber(string ClmNb)
        {
            return FindClaimbyNumber(ClmNb, null);  
        }
        public XmlNode FindClaimbyNumber(string ClmNb, XmlDocument Doc)
        {
            bool rst = true;
            XmlDocument xdoc = Doc;
            getNode(rst);
            if (ClmNb != ClaimNumber)
            {
                while (getClaim(xdoc, rst) != null)
                {
                    rst = false;
                    xdoc = null;
                    if (ClaimNumber == ClmNb)
                        break;
                }
            }
            return Node;
        }
        //End SIW481

        //Start (SI06023 - Implemented in SI06333)
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }
        //End (SI06023 - Implemented in SI06333)
        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490
        //SIW344 Starts
        public XMLLiabilityLoss LiabilityLoss
        {
            get
            {
                if (m_LiabilityLoss  == null)
                {
                    m_LiabilityLoss = new XMLLiabilityLoss ();
                    m_LiabilityLoss.Parent = this;
                    //SIW529 LinkXMLObjects(m_LiabilityLoss);
                    m_LiabilityLoss.getLiabLoss(Constants.xcGetFirst);  //SI06420
                }
                return m_LiabilityLoss;
            }
            set
            {
                m_LiabilityLoss = value;
            }
        }
        //SIW344 Ends
        public string TotalClaimReserveBalance  //vkumar258
        {
            get
            {
                return Utils.getData(Node, Constants.sClmTotalClaimReserveBalance);  
            }
            set
            {
                Utils.putData(putNode, Constants.sClmTotalClaimReserveBalance, value, NodeOrder); 
            }
        }
    }
}
