/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 11/19/2009 | SIW44   |    ASM     | WorkerComp (Employee) field's mapping
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 04/08/2010 | SIW336  |    SW      | Pay_Code type corrected from Int to double.
 * 04/15/2010 | SIW392  |    ASM     | Mapping for fields of LostDays and RestrictedDays screens
 * 04/15/2010 | SIW392  |   ASM      |  LostDays and RestrictedDays load/save
 * 04/28/2010 | SIW391  |    SW      | Time Day began field on Worker compensation
 * 08/13/2010 | SIW490 |    ASM     | Tree Label              
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 02/22/2012 | SIN8043 |    MS      | Adding Teck Key,weekly pay and duration node to the lost days node in Employee                                  
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLEmployee : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<UNIT_STAT_DCI DG_ID="DGxxxx">
        '   <EMPLOYEE CLAIMANT_NUMBER="1" ID="ENT249">
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>         '(SI06023 - Implemented in SI06333) 'SIw360
        '      <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '      <EMPLOYEE_NUMBER>employee number</EMPLOYEE_NUMBER>
        '      <EMPLOYMENT_STATUS ACTIVE="YES|NO {boolean}" STATUS="[FULLTIME|PARTTIME]"/>
        '      <RETURNED_TO_WORK NEXT_DAY="YES|NO {boolean}" DUTIES="YES|NO {boolean}"/>
        '      <OCCUPATION _OPTIONAL_="" CODE="Standard Occupation Code {string} [OCCUPATION]">Occupation Translated {string}</OCCUPATION>
        '      <DATE_HIRED _OPTIONAL_="" YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DATE_TERMINATED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <STATE_HIRED _OPTIONAL_="" CODE="State Abbreviation {string} [STATES]">State Name {string}</STATE_HIRED>        
        '      <COMPENSATION_RATE>{decimal}</COMPENSATION_RATE>      'SIW44
        '      <WORK_PERMIT_DATE YEAR="" MONTH="" DAY=""/>
        '      <WORK_PERMIT_NUMBER>number</WORK_PERMIT_NUMBER>
        '      <EXEMPT_STATUS INDICATOR="YES|NO"/>                   'SIW44
        '      <REGULAR_JOB_STATUS INDICATOR="YES|NO"/>              'SIW44
        '      <HIRED_IN_JURISDICTION INDICATOR="YES|NO"/>           'SIW44
        '      <AVG_WKLY_WAGE _OPTIONAL_="">{decimal}</AVG_WKLY_WAGE>
        '      <ADJUSTED_WAGE _OPTIONAL_="">{decimal}</ADJUSTED_WAGE>
        '      <NCCI_EMP_WAGE_RANG _OPTIONAL_="">Payroll Indicator</NCCI_EMP_WAGE_RANG>
        '      <PAY SPECIFIED="[PREINJURY|POSTINJURY|OTHER|CURRENT]
        '           CALCULATE_METHOD="calculatemethodcode"
        '           TYPE="SALARY | HOURLY {string} [PAY_TYPE]"
        '           PERIOD="[HOURLY|WEEKLY|BIWEEKLY|SEMIMONTHLY|ANNUAL]{string}"
        '           FULL_PAY_DAY_INJURED="YES|NO {boolean}"
        '           CONTINUED="YES|NO {boolean}"
        '           OT_ELIGIBLE="YES|NO {boolean}">
        '           {decimal}</PAY>
        '           <!-- Multiple Lines of different types allowed -->
        '      <ENTITLEMENT SPECIFIED="[PREINJURY|POSTINJURY|OTHER|CURRENT] TYPE="[description|TOTAL]" PERIOD="[HOURLY|WEEKLY|BIWEEKLY|SEMIMONTHLY|ANNUAL]" CONTINUED="YES|NO {boolean}">
        '            amount {decimal}</ENTITLEMENT>
        '         <!-One line for each appropriate entitlement. If submitting a TOTAL, do not submit itemized items and vice versa -->
        '      <TIME_DAY_BEGAN HOUR="17" MINUTE="35" SECOND="04" />        //SIW391
        '      <WEEKLY_SCHEDULE SPECIFIED="[PREINJURY|POSTINJURY|CURRENT|OTHER] _OPTIONAL_="" WEEK_STARTS_ON="the day of week normally considered the start of the work week[SUN,MON,TUE,WED,THU,FRI,SAT,AVG]" DAYS_WORKED="The number of days worked per week{integer}">
        '          <DAY CODE="Day of Week [SUN,MON,TUE,WED,THU,FRI,SAT,AVG]" _OPTIONAL_="" SCHD_REG_HOURS="{integer}" EST_OT_HOURS="{integer}">Day of week</DAY>
        '              <SHIFT_START_TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"  HOUR="Hour (2) {Integer} [0-23]"  MINUTE="Minute (2) {Integer} [0-60]" SECOND="Second (2) {Integer} [0-60=</TIME_CREATED>
        '              <SHIFT_END_TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"  HOUR="Hour (2) {Integer} [0-23]"  MINUTE="Minute (2) {Integer} [0-60]" SECOND="Second (2) {Integer} [0-60=</TIME_CREATED>
        '          </DAY>
        '          <!-Repeat for each appropriate day. If AVG submitted, this indicates the average days activities within a weeks time-->
        '      </WEEKLY_SCHEDULE >
        '          <!-- Repeat schedule as needed with different SPECIFIED values -->
        '      <WEEKLY_HOURS> integer </WEEKLY_HOURS>           'SI04900
        '      <FIRST_DAY_NOT_FULL_DAY _OPTIONAL_="" YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <ASSIGNED_DEPARTMENT _OPTIONAL_="" CODE="Department Code {string} [ORG_HIERARCHY]">Deparment Assigned {string}</ASSIGNED_DEPARTMENT>
        '      <NUMBER_OF_DEPENDANTS>0</NUMBER_OF_DEPENDANTS>
        '      <EDUCATION CODE="*">Default</EDUCATION>
        '      <PRIMARY_LANGUAGE CODE="*">Default</PRIMARY_LANGUAGE>  'SI05300
        '      <PAST_WORK_EX>Describe the work ex{string}</PAST_WORK_EX> 'SI05300
        '      <PRIOR_WORK_HIST>Describe the Prior Work History{string}</PRIOR_WORK_HIST> 'SI05301 
        '      <LOST_DAYS ID="LSDxxxxxx">                   'SIW392
        '      <TECH _KEY></TECH_KEY>                       'SIN8043
        '      <LAST_WORK_DAY YEAR="" MONTH="" DAY=""/>    'SIW392
        '       <DATE_RETURN_TO_WORK YEAR="" MONTH="04" DAY=""/>   'SIW392
        '       <WEEKLY_PAY>0</WEEKLY_PAY>                  'SIN8043
        '        <DURATION>0</DURATION>                      'SIN8043
        '      </LOST_DAYS>   'SIW392
        '           <!-- Multiple Lines of Lost Days of different ID's allowed -->   'SIW392
        '      <RESTRICTED_DAYS ID="RSDxxxx">   'SIW392
        '        <FIRST_DAY_RSTC YEAR="" MONTH="" DAY=""/>   'SIW392
        '        <LAST_DAY_RSTC YEAR="" MONTH="" DAY=""/>   'SIW392
        '        <PERCENT_DISABLED>{integer}</PERCENT_DISABLED>   'SIW392
        '        <POSITION CODE="*">Default</POSITION>   'SIW392
        '      </RESTRICTED_DAYS>   'SIW392
        '           <!-- Multiple Lines of Restricted Days of different ID's allowed -->   'SIW392
        '      <PREV_WC INDICATOR="YES|NO">                             'SI05301
        '         <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>    'SI05301
        '         <DESCRIPTION>Description of the previous WC claim {string}</DESCRIPTION> 'SI05301
        '      </PREV_WC>                                               'SI05301
        '      <DOMINANT_HAND CODE="*">Default</DOMINANT_HAND>          'SI05301
        '      <LIVING_ARRANGEMENTS>Describe the Living Arrangements{string}</LIVING_ARRANGEMENTS> 'SI05301
        '      <MEDICAL_INFORMATION>                                    'SI05301
        '         <ALLERGIES INDICATOR="YES|NO">list</ALLERGIES>          'SI05301
        '         <MEDICATIONS>List the Medications{string}</MEDICATIONS> 'SI05301
        '         <SPEC_NEEDS>Describe the Special Needs{string}</SPEC_NEEDS> 'SI05301
        '         <HEIGHT>{string}</HEIGHT>                                'SI05301
        '         <WEIGHT>{string}</WEIGHT>                                'SI05301
        '         <SMOKER INDICATOR="YES|NO">                              'SI05301
        '         <PPD>Number of Packs per Day</PPD>                       'SI05301
        '         <ALCOHOLUSE INDICATOR="YES|NO">                          'SI05301
        '         <CANCER INDICATOR="YES|NO">                              'SI05301
        '         <DIABETES INDICATOR="YES|NO">                            'SI05301
        '         <HEART_COND INDICATOR="YES|NO">                          'SI05301
        '         <HEPATITIS INDICATOR="YES|NO">                           'SI05301
        '         <HYPERTENSION INDICATOR="YES|NO">                        'SI05301
        '         <LUNG_DISEASE INDICATOR="YES|NO">                        'SI05301
        '         <SEIZURES INDICATOR="YES|NO">                            'SI05301
        '         <BLOOD_DISORDER INDICATOR="YES|NO">                      'SI05301
        '         <MENTAL_DISORDER INDICATOR="YES|NO">                     'SI05301
        '         <ARTHRITIS INDICATOR="YES|NO">                           'SI05301
        '         <PAST_SURG_DESCRIPTION>Describe the Past Surgeries{string}</PAST_SURG_DESCRIPTION> 'SI05301
        '         <OTHER_DESCRIPTION>List the other conditions{string}</OTHER_DESCRIPTION> 'SI05301
        '      </MEDICAL_INFORMATION>                                    'SI05301
        '      <ATTY_AUTH_REP INDICATOR="No" />
        '      <BENEFITS_OFFSET TYPE="[UNEMPLOYMENT|SOCIALSECURITY|PENSIONPLAN|SPECIALFUND|OTHER]">Amount</BENEFITS_OFFSET>
        '         <!-- Multiple rows allowed -->
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '         <!--Multiple Parties Involved at the employee level such as Dependents, Beneficiaries, etc.-->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '         <!-- multiple entries -->
        '   </EMPLOYEE_INFORMATION>        
        '</UNIT_STAT_DCI>*/

        private XmlNode xmlWklySched;
        private XmlNodeList xmlWklySchedList;
        //private IEnumerator xmlWklyShcedListEnum;
        private XmlNode xmlWklySchedDay;
        private XmlNodeList xmlWklySchedDayList;
        //private IEnumerator xmlWklySchedDayListEnum;
        private XmlNode xmlPay;
        private XmlNodeList xmlPayList;
        //private IEnumerator xmlPayListEnum;
        private XmlNode xmlBenefit;
        private XmlNodeList xmlBenefitList;
        //private IEnumerator xmlBenefitListEnum;
        private XmlNode xmlEntitlement;
        private XmlNodeList xmlEntitlementList;
        //private IEnumerator xmlEntitlementListEnum;
        private XmlNode xmlPrevWCNode;
        private XmlNode xmlMedInfoNode;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLEntity m_xmlEntity;
        private ArrayList sWSNodeOrder;
        private ArrayList sLSDNodeOrder;  //SIW392
        private XmlNode xmlLostDays; //SIW392
        private XmlNodeList xmlLostDaysList;  //SIW392
        private ArrayList sRSDNodeOrder;  //SIW392
        private XmlNode xmlRestrictedDays; //SIW392
        private XmlNodeList xmlRestrictedDaysList; //SIW392
        private ArrayList sPrevWCNodeOrder;
        private ArrayList sMedInfoNodeOrder;
        private Dictionary<object, object> DctDOW;
        private Dictionary<object, object> DctPayPeriod;
        private Dictionary<object, object> DctSpecified;
        private Dictionary<object, object> DctBenefitType;
        private Dictionary<object, object> DctPayType;
        
        public XMLEmployee()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;

            //sThisNodeOrder = new ArrayList(31);//(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(37);//(SI06023 - Implemented in SI06333) //SIW44 //SIW391 //SIW490
            sThisNodeOrder = new ArrayList(39);//SIW490 //dbisht 6 add Fein Node hence increasing the size of array
            sThisNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sEmpNumber);
            sThisNodeOrder.Add(Constants.sEmpStatus);
            sThisNodeOrder.Add(Constants.sEmpRTW);
            sThisNodeOrder.Add(Constants.sEmpOccupation);
            sThisNodeOrder.Add(Constants.sEmpHiredDate);
            sThisNodeOrder.Add(Constants.sEmpTerminatedDate);
            sThisNodeOrder.Add(Constants.sEmpStateHired);
            sThisNodeOrder.Add(Constants.sEmpWorkPermitDate);
            sThisNodeOrder.Add(Constants.sEmpWorkPermitNumber);
            sThisNodeOrder.Add(Constants.sEmpExemptStatus);           //SIW44
            sThisNodeOrder.Add(Constants.sEmpRegularJobStatus);       //SIW44
            sThisNodeOrder.Add(Constants.sEmpHiredInJurisdiction);    //SIW44
            sThisNodeOrder.Add(Constants.sEmpAVW);
            sThisNodeOrder.Add(Constants.sEmpPay);
            sThisNodeOrder.Add(Constants.sEmpEntitlement);
            sThisNodeOrder.Add(Constants.sEmpAdjustedW);
            sThisNodeOrder.Add(Constants.sEmpCR);  //SIW44
            sThisNodeOrder.Add(Constants.sEmpTimeDayBegan); //SIW391  
            sThisNodeOrder.Add(Constants.sEmpWeeklySchedule);
            sThisNodeOrder.Add(Constants.sEmpLostDays);        //SIW392
            sThisNodeOrder.Add(Constants.sEmpRestrictedDays);        //SIW392
            //sThisNodeOrder.Add(Constants.sEmpLastDayWorked); //SIW392
            sThisNodeOrder.Add(Constants.sEmp1stDayNotFullPay);
            //sThisNodeOrder.Add(Constants.sEmpRTWDate);       //SIW392
            sThisNodeOrder.Add(Constants.sEmpAssignedDept);
            sThisNodeOrder.Add(Constants.sEmpNumbOfDpndnts);
            sThisNodeOrder.Add(Constants.sEmpEducation);
            sThisNodeOrder.Add(Constants.sEmpPrimaryLanguage);
            sThisNodeOrder.Add(Constants.sEmpPastWorkEx);
            sThisNodeOrder.Add(Constants.sEmpLivingArrangements);
            sThisNodeOrder.Add(Constants.sEmpPrevWC);
            sThisNodeOrder.Add(Constants.sEmpPriorWorkHist);
            sThisNodeOrder.Add(Constants.sEmpAttyRepresented);
            sThisNodeOrder.Add(Constants.sEmpBenOffset);
            sThisNodeOrder.Add(Constants.sEmpWeeklyHours);
            sThisNodeOrder.Add(Constants.sEmpMedInfoNode);
            sThisNodeOrder.Add(Constants.sEmpPartyInvolved);
            sThisNodeOrder.Add(Constants.sEmpCommentReference);
            //sThisNodeOrder.Add(Constants.sEmpTechKey);//(SI06023 - Implemented in SI06333)//SIW360
            sThisNodeOrder.Add("EMPLOYEE_FEIN");
            sPrevWCNodeOrder = new ArrayList(2);
            sPrevWCNodeOrder.Add(Constants.sEmpPrevWCDate);
            sPrevWCNodeOrder.Add(Constants.sEmpPrevWCDesc);

            sMedInfoNodeOrder = new ArrayList(20);
            sMedInfoNodeOrder.Add(Constants.sEmpAllergies);
            sMedInfoNodeOrder.Add(Constants.sEmpMedications);
            sMedInfoNodeOrder.Add(Constants.sEmpSpecNeeds);
            sMedInfoNodeOrder.Add(Constants.sEmpPrevWC);
            sMedInfoNodeOrder.Add(Constants.sEmpHeight);
            sMedInfoNodeOrder.Add(Constants.sEmpWeight);
            sMedInfoNodeOrder.Add(Constants.sEmpDominantHand);
            sMedInfoNodeOrder.Add(Constants.sEmpSmoker);
            sMedInfoNodeOrder.Add(Constants.sEmpPPD);
            sMedInfoNodeOrder.Add(Constants.sEmpCancer);
            sMedInfoNodeOrder.Add(Constants.sEmpDiabetes);
            sMedInfoNodeOrder.Add(Constants.sEmpHeartCond);
            sMedInfoNodeOrder.Add(Constants.sEmpHypertension);
            sMedInfoNodeOrder.Add(Constants.sEmpLungDisease);
            sMedInfoNodeOrder.Add(Constants.sEmpSeizures);
            sMedInfoNodeOrder.Add(Constants.sEmpBloodDisorder);
            sMedInfoNodeOrder.Add(Constants.sEmpMentalDisorder);
            sMedInfoNodeOrder.Add(Constants.sEmpArthritis);
            sMedInfoNodeOrder.Add(Constants.sEmpOther);
            sMedInfoNodeOrder.Add(Constants.sEmpPastSurgeries);

            sWSNodeOrder = new ArrayList(3);
            sWSNodeOrder.Add(Constants.sEmpWSDay);
            sWSNodeOrder.Add(Constants.sEmpWSDayShiftStart);
            sWSNodeOrder.Add(Constants.sEmpWSDayShiftEnd);

            //sLSDNodeOrder = new ArrayList(2); //SIW392 SIN8043
            sLSDNodeOrder = new ArrayList(5); //SIW392  //SIN8043
            sLSDNodeOrder.Add(Constants.sStdTechKey);//SIN8043
            sLSDNodeOrder.Add(Constants.sLSDLastDayWorked); //SIW392
            sLSDNodeOrder.Add(Constants.sLSDRTWDate); //SIW392
            sLSDNodeOrder.Add(Constants.sLSDDuration); //SIN8043
            sLSDNodeOrder.Add(Constants.sLSDWeeklyPay); //SIN8043

            sRSDNodeOrder = new ArrayList(4); //SIW392
            sRSDNodeOrder.Add(Constants.sRsdFirstDayRstDate); //SIW392
            sRSDNodeOrder.Add(Constants.sRsdLastDayRstDate); //SIW392
            sRSDNodeOrder.Add(Constants.sRsdPercentDisabled); //SIW392
            sRSDNodeOrder.Add(Constants.sRsdPosition); //SIW392

            DctSpecified = new Dictionary<object, object>();
            DctSpecified.Add(Constants.iSpecifiedUnspecified, Constants.sSpecifiedUnspecified);
            DctSpecified.Add(Constants.sSpecifiedUnspecified, Constants.iSpecifiedUnspecified);
            DctSpecified.Add(Constants.iSpecifiedPreInjury, Constants.sSpecifiedPreInjury);
            DctSpecified.Add(Constants.sSpecifiedPreInjury, Constants.iSpecifiedPreInjury);
            DctSpecified.Add(Constants.iSpecifiedPostInjury, Constants.sSpecifiedPostInjury);
            DctSpecified.Add(Constants.sSpecifiedPostInjury, Constants.iSpecifiedPostInjury);
            DctSpecified.Add(Constants.iSpecifiedOther, Constants.sSpecifiedOther);
            DctSpecified.Add(Constants.sSpecifiedOther, Constants.iSpecifiedOther);
            DctSpecified.Add(Constants.iSpecifiedCurrent, Constants.sSpecifiedCurrent);
            DctSpecified.Add(Constants.sSpecifiedCurrent, Constants.iSpecifiedCurrent);

            DctBenefitType = new Dictionary<object, object>();
            DctBenefitType.Add(Constants.iBenefitTypeUnspecified, Constants.sBenefitTypeUnspecified);
            DctBenefitType.Add(Constants.sBenefitTypeUnspecified, Constants.iBenefitTypeUnspecified);
            DctBenefitType.Add(Constants.iBenefitTypeUnemployment, Constants.sBenefitTypeUnemployment);
            DctBenefitType.Add(Constants.sBenefitTypeUnemployment, Constants.iBenefitTypeUnemployment);
            DctBenefitType.Add(Constants.iBenefitTypeSocSec, Constants.sBenefitTypeSocSec);
            DctBenefitType.Add(Constants.sBenefitTypeSocSec, Constants.iBenefitTypeSocSec);
            DctBenefitType.Add(Constants.iBenefitTypePenPlan, Constants.sBenefitTypePenPlan);
            DctBenefitType.Add(Constants.sBenefitTypePenPlan, Constants.iBenefitTypePenPlan);
            DctBenefitType.Add(Constants.iBenefitTypeSpclFund, Constants.sBenefitTypeSpclFund);
            DctBenefitType.Add(Constants.sBenefitTypeSpclFund, Constants.iBenefitTypeSpclFund);
            DctBenefitType.Add(Constants.iBenefitTypeOther, Constants.sBenefitTypeOther);
            DctBenefitType.Add(Constants.sBenefitTypeOther, Constants.iBenefitTypeOther);

            DctPayType = new Dictionary<object, object>();
            DctPayType.Add(Constants.sUCPayTypeUnspecified, Constants.iUCPayTypeUnspecified);
            DctPayType.Add(Constants.iUCPayTypeUnspecified, Constants.sUCPayTypeUnspecified);
            DctPayType.Add(Constants.sUCPayTypeSalary, Constants.iUCPayTypeSalary);
            DctPayType.Add(Constants.iUCPayTypeSalary, Constants.sUCPayTypeSalary);
            DctPayType.Add(Constants.sUCPayTypeHourly, Constants.iUCPayTypeHourly);
            DctPayType.Add(Constants.iUCPayTypeHourly, Constants.sUCPayTypeHourly);

            DctPayPeriod = new Dictionary<object, object>();
            DctPayPeriod.Add(Constants.sUCPayPeriodUnspecified, Constants.iUCPayPeriodUnspecified);
            DctPayPeriod.Add(Constants.iUCPayPeriodUnspecified, Constants.sUCPayPeriodUnspecified);
            DctPayPeriod.Add(Constants.sUCPayPeriodHourly, Constants.iUCPayPeriodHourly);
            DctPayPeriod.Add(Constants.iUCPayPeriodHourly, Constants.sUCPayPeriodHourly);
            DctPayPeriod.Add(Constants.sUCPayPeriodWeekly, Constants.iUCPayPeriodWeekly);
            DctPayPeriod.Add(Constants.iUCPayPeriodWeekly, Constants.sUCPayPeriodWeekly);
            DctPayPeriod.Add(Constants.sUCPayPeriodBiWeekly, Constants.iUCPayPeriodBiWeekly);
            DctPayPeriod.Add(Constants.iUCPayPeriodBiWeekly, Constants.sUCPayPeriodBiWeekly);
            DctPayPeriod.Add(Constants.sUCPayPeriodSemiMonthly, Constants.iUCPayPeriodSemiMonthly);
            DctPayPeriod.Add(Constants.iUCPayPeriodSemiMonthly, Constants.sUCPayPeriodSemiMonthly);
            DctPayPeriod.Add(Constants.sUCPayPeriodMonthly, Constants.iUCPayPeriodMonthly);
            DctPayPeriod.Add(Constants.iUCPayPeriodMonthly, Constants.sUCPayPeriodMonthly);
            DctPayPeriod.Add(Constants.sUCPayPeriodQuarterly, Constants.iUCPayPeriodQuarterly);
            DctPayPeriod.Add(Constants.iUCPayPeriodQuarterly, Constants.sUCPayPeriodQuarterly);
            DctPayPeriod.Add(Constants.sUCPayPeriodAnnual, Constants.iUCPayPeriodAnnual);
            DctPayPeriod.Add(Constants.iUCPayPeriodAnnual, Constants.sUCPayPeriodAnnual);

            DctDOW = new Dictionary<object, object>();
            DctDOW.Add("SUN", DayOfWeek.Sunday);
            DctDOW.Add("SUNDAY", DayOfWeek.Sunday);
            DctDOW.Add(DayOfWeek.Sunday, "SUN");
            DctDOW.Add("MON", DayOfWeek.Monday);
            DctDOW.Add("MONDAY", DayOfWeek.Monday);
            DctDOW.Add(DayOfWeek.Monday, "MON");
            DctDOW.Add("TUE", DayOfWeek.Tuesday);
            DctDOW.Add("TUESDAY", DayOfWeek.Tuesday);
            DctDOW.Add(DayOfWeek.Tuesday, "TUE");
            DctDOW.Add("WED", DayOfWeek.Wednesday);
            DctDOW.Add("WEDNESDAY", DayOfWeek.Wednesday);
            DctDOW.Add(DayOfWeek.Wednesday, "WED");
            DctDOW.Add("THU", DayOfWeek.Thursday);
            DctDOW.Add("THURSDAY", DayOfWeek.Thursday);
            DctDOW.Add(DayOfWeek.Thursday, "THU");
            DctDOW.Add("FRI", DayOfWeek.Friday);
            DctDOW.Add("FRIDAY", DayOfWeek.Friday);
            DctDOW.Add(DayOfWeek.Friday, "FRI");
            DctDOW.Add("SAT", DayOfWeek.Saturday);
            DctDOW.Add("SATURDAY", DayOfWeek.Saturday);
            DctDOW.Add(DayOfWeek.Saturday, "SAT");

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sEmpNode; }
        }

        public XmlNode Create(string sEntityID)
        {
            return Create(sEntityID, "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sEntityID, string sClaimandNumber, string sDateHired, string sStateHired,
                              string sStateHiredCode, string sOccupation, string sOccupationCode, string sActive,
                              string sStatus, string sAverageWeeklyWage, string sWeeklyHours)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                EntityID = sEntityID;
                ClaimantNumber = sClaimandNumber;
                HiredDate = sDateHired;
                putHiredState(sStateHired, sStateHiredCode);
                putOccupation(sOccupation, sOccupationCode);
                ActiveEmployee = sActive;
                EmploymentStatus = sStatus;
                AvgWklyWage = sAverageWeeklyWage;
                WklyHours = sWeeklyHours;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLEmployee.Create");
                return null;
            }
        }

        public string ReturnToWorkDate
        {
            get
            {
                return Utils.getDate(LostDaysNode, Constants.sLSDRTWDate); //SIW392
            }
            set
            {
                Utils.putDate(putLostDaysNode, Constants.sLSDRTWDate, value, sLSDNodeOrder); //SIW392
            }
        }
        // Start SIN8043
        public string TechKeyLostDays
        {
            get
            {
                return Utils.getData(LostDaysNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putLostDaysNode, Constants.sStdTechKey, value, sLSDNodeOrder);
            }
        }
        public string Duration
        {
            get
            {
                return Utils.getData(LostDaysNode, Constants.sLSDDuration);
            }
            set
            {
                Utils.putData(putLostDaysNode, Constants.sLSDDuration, value, sLSDNodeOrder);
            }
        }
        public string WeeklyPay
        {
            get
            {
                return Utils.getData(LostDaysNode, Constants.sLSDWeeklyPay);
            }
            set
            {
                Utils.putData(putLostDaysNode, Constants.sLSDWeeklyPay, value, sLSDNodeOrder);
            }
        }

        //End SIN8043

        public string FirstDayNotFullPay
        {
            get
            {
                return Utils.getDate(Node, Constants.sEmp1stDayNotFullPay);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEmp1stDayNotFullPay, value, NodeOrder);
            }
        }

        public string LastDayWorked
        {
            get
            {
                return Utils.getDate(LostDaysNode, Constants.sLSDLastDayWorked); //SIW392
            }
            set
            {
                Utils.putDate(putLostDaysNode, Constants.sLSDLastDayWorked, value, sLSDNodeOrder);  //SIW392
            }
        }

        public string WorkPermitDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEmpWorkPermitDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEmpWorkPermitDate, value, NodeOrder);
            }
        }

        public string WorkPermitNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpWorkPermitNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpWorkPermitNumber, value, NodeOrder);
            }
        }
        //Start SIW44
        public string ExemptStatus
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpExemptStatus, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpExemptStatus, "", value, NodeOrder);
            }
        }
        public string RegularJobStatus
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpRegularJobStatus, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpRegularJobStatus, "", value, NodeOrder);
            }
        }
        public string HiredInJurisdiction
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpHiredInJurisdiction, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpHiredInJurisdiction, "", value, NodeOrder);
            }
        }
        //End SIW44
        public string HiredDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEmpHiredDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEmpHiredDate, value, NodeOrder);
            }
        }

        //dbish6 RMA-10309
        public string EmpFein
        {
            get
            {
                return Utils.getData(Node, "EMPLOYEE_FEIN");
            }
            set
            {
                Utils.putData(putNode, "EMPLOYEE_FEIN", value, NodeOrder);
            }
        }
        //dbisht6 end


        public string TerminatedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEmpTerminatedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEmpTerminatedDate, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putHiredState(string sDesc, string sCode)
        {
            return putHiredState(sDesc, sCode, "");
        }

        public XmlNode putHiredState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpStateHired, sDesc, sCode, NodeOrder, scodeid);
        }

        public string HiredState
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpStateHired);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpStateHired, value, NodeOrder);
            }
        }

        public string HiredState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpStateHired);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpStateHired, value, NodeOrder);
            }
        }

        public string HiredState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpStateHired);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpStateHired, value, NodeOrder);
            }
        }
        //End SIW139

        public string NumberOfDependents
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpNumbOfDpndnts);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpNumbOfDpndnts, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putEducation(string sDesc, string sCode)
        {
            return putEducation(sDesc, sCode, "");
        }

        public XmlNode putEducation(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpEducation, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Education
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpEducation);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpEducation, value, NodeOrder);
            }
        }

        public string Education_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpEducation);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpEducation, value, NodeOrder);
            }
        }

        public string Education_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpEducation);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpEducation, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putPrimaryLanguage(string sDesc, string sCode)
        {
            return putPrimaryLanguage(sDesc, sCode, "");
        }

        public XmlNode putPrimaryLanguage(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpPrimaryLanguage, sDesc, sCode, NodeOrder, scodeid);
        }

        public string PrimaryLanguage
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpPrimaryLanguage);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpPrimaryLanguage, value, NodeOrder);
            }
        }

        public string PrimaryLanguage_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpPrimaryLanguage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpPrimaryLanguage, value, NodeOrder);
            }
        }

        public string PrimaryLanguage_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpPrimaryLanguage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpPrimaryLanguage, value, NodeOrder);
            }
        }
        //End SIW139

        public string PastWorkEx
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpPastWorkEx);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpPastWorkEx, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putDominantHand(string sDesc, string sCode)
        {
            return putDominantHand(sDesc, sCode, "");
        }

        public XmlNode putDominantHand(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpDominantHand, sDesc, sCode, NodeOrder, scodeid);
        }

        public string DominantHand
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpDominantHand);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpDominantHand, value, NodeOrder);
            }
        }

        public string DominantHand_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpDominantHand);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpDominantHand, value, NodeOrder);
            }
        }

        public string DominantHand_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpDominantHand);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpDominantHand, value, NodeOrder);
            }
        }
        //End SIW139

        public string LivingArrangement
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpLivingArrangements);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpLivingArrangements, value, NodeOrder);
            }
        }

        public string PriorWorkHistory
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpPriorWorkHist);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpPriorWorkHist, value, NodeOrder);
            }
        }

        public string EmployeeNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpNumber, value, NodeOrder);
            }
        }

        public string AvgWklyWage
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpAVW);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpAVW, value, NodeOrder);
            }
        }

        public string WklyHours
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpWeeklyHours);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpWeeklyHours, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putOccupation(string sDesc, string sCode)
        {
            return putOccupation(sDesc, sCode, "");
        }

        public XmlNode putOccupation(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpOccupation, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Occupation
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpOccupation);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpOccupation, value, NodeOrder);
            }
        }

        public string Occupation_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpOccupation);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpOccupation, value, NodeOrder);
            }
        }

        public string Occupation_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpOccupation);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpOccupation, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putAssignedDept(string sDesc, string sCode)
        {
            return putAssignedDept(sDesc, sCode, "");
        }

        public XmlNode putAssignedDept(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpAssignedDept, sDesc, sCode, NodeOrder, scodeid);
        }

        public string AssignedDept
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpAssignedDept);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpAssignedDept, value, NodeOrder);
            }
        }

        public string AssignedDept_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpAssignedDept);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpAssignedDept, value, NodeOrder);
            }
        }

        public string AssignedDept_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEmpAssignedDept);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEmpAssignedDept, value, NodeOrder);
            }
        }
        //End SIW139

        public string AttorneyRepresented
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpAttyRepresented, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpAttyRepresented, "", value, NodeOrder);
            }
        }

        public string ReturnToWorkFullDuties
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpRTW, Constants.sEmpRTWNextDay);
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpRTW, Constants.sEmpRTWNextDay, value, NodeOrder);
            }
        }

        public string ReturnToWorkNextDay
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpRTW, Constants.sEmpRTWFullDuties);
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpRTW, Constants.sEmpRTWFullDuties, value, NodeOrder);
            }
        }

        public string ActiveEmployee
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpStatus, Constants.sEmpStatActive);
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpStatus, Constants.sEmpStatActive, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putEmploymentStatus(string sDesc, string sCode)
        {
            return putEmploymentStatus(sDesc, sCode, "");
        }

        public XmlNode putEmploymentStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEmpStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string EmploymentStatus
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpStatus, value, NodeOrder);
            }
        }

        public string EmploymentStatus_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEmpStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEmpStatus, value, NodeOrder);
            }
        }

        public string FullTime
        {
            get
            {
                return Utils.getBool(Node, Constants.sEmpStatus, Constants.sEmpStatFullTime);
            }
            set
            {
                Utils.putBool(putNode, Constants.sEmpStatus, Constants.sEmpStatFullTime, value, NodeOrder);
            }
        }

        public string ClaimantNumber
        {
            get
            {
                return XML.XMLGetAttributeValue(Node, Constants.sEmpClmntNumber);
            }
            set
            {
                XML.XMLSetAttributeValue(putNode, Constants.sEmpClmntNumber, value);
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sCCPXmlEntityIDPrefix;
            }
        }

        public string EntityID
        {
            get
            {
                return XML.XMLExtractEntityAttribute(Node);
            }
            set
            {
                XML.XMLInsertEntity_Node(putNode, value);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)
        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490

        public int BenefitOffsetCount
        {
            get
            {
                return BenefitOffsetList.Count;
            }
        }

        public XmlNodeList BenefitOffsetList
        {
            get
            {
                //Start SIW163
                if (null == xmlBenefitList)
                {
                    return getNodeList(Constants.sEmpBenOffset, ref xmlBenefitList, Node);
                }
                else
                {
                    return xmlBenefitList;
                }
                //End SIW163
            }
        }

        public XmlNode getBenefitOffset(bool reset)
        {
            //Start SIW163
            if (null != BenefitOffsetList)
            {
                return getNode(reset, ref xmlBenefitList, ref xmlBenefit);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeBenefitOffset()
        {
            if (BenefitNode != null)
                Node.RemoveChild(BenefitNode);
            BenefitNode = null;
        }

        public XmlNode putBenefitNode
        {
            get
            {
                if (BenefitNode == null)
                    BenefitNode = XML.XMLaddNode(putNode, Constants.sEmpBenOffset, NodeOrder);
                return BenefitNode;
            }
        }

        public XmlNode BenefitNode
        {
            get
            {
                return xmlBenefit;
            }
            set
            {
                xmlBenefit = value;
            }
        }

        public XmlNode addBenefitOffset(int iBenefit, xcEmpBenefitType cType)
        {
            BenefitNode = null;
            return putBenefitOffset(iBenefit, cType);
        }

        public XmlNode putBenefitOffset(int iBenefit, xcEmpBenefitType cType)
        {
            object sdata; //string
            DctBenefitType.TryGetValue((int)cType, out sdata);
            return putBenefitOffset_Code(iBenefit, (string)sdata);
        }

        public XmlNode addBenefitOffset_Code(int iBenefit, string sType)
        {
            BenefitNode = null;
            return putBenefitOffset_Code(iBenefit, sType);
        }

        public XmlNode putBenefitOffset_Code(int iBenefit, string sType)
        {
            Utils.putTypeItem(putBenefitNode, "", iBenefit + "", sType, NodeOrder);
            CheckBenefit();
            return BenefitNode;
        }

        private void CheckBenefit()
        {
            if ((BenefitOffset == "" || BenefitOffset == null) && (BenefitOffsetType_Code == "" || BenefitOffsetType_Code == null))
                removeBenefitOffset();
        }

        public string BenefitOffset
        {
            get
            {
                return Utils.getData(BenefitNode, "");
            }
            set
            {
                Utils.putData(putBenefitNode, "", value, NodeOrder);
            }
        }

        public xcEmpBenefitType BenefitOffsetType
        {
            get
            {
                object etype;
                string sdata = BenefitOffsetType_Code;
                if (!DctBenefitType.TryGetValue(sdata, out etype))
                    etype = xcEmpBenefitType.xcEmpBTUnspecified;
                return (xcEmpBenefitType)etype;
            }
            set
            {
                object sdata; //string
                DctBenefitType.TryGetValue((int)value, out sdata);
                BenefitOffsetType_Code = (string)sdata;
            }
        }

        public string BenefitOffsetType_Code
        {
            get
            {
                return Utils.getType(BenefitNode, "");
            }
            set
            {
                Utils.putType(putBenefitNode, "", value, NodeOrder);
            }
        }

        public int PayCount
        {
            get
            {
                return PayList.Count;
            }
        }

        public XmlNodeList PayList
        {
            get
            {
                //Start SIW163
                if (null == xmlPayList)
                {
                    return getNodeList(Constants.sEmpPay, ref xmlPayList, Node);
                }
                else
                {
                    return xmlPayList;
                }
                //End SIW163
            }
        }

        public XmlNode getPay(bool reset)
        {
            //Start SIW163
            if (null != PayList)
            {
                return getNode(reset, ref xmlPayList, ref xmlPay);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removePay()
        {
            if (PayNode != null)
                Node.RemoveChild(PayNode);
            PayNode = null;
        }

        private XmlNode putPayNode
        {
            get
            {
                if (PayNode == null)
                    PayNode = XML.XMLaddNode(putNode, Constants.sEmpPay, NodeOrder);
                return PayNode;
            }
        }

        public XmlNode PayNode
        {
            get
            {
                return xmlPay;
            }
            set
            {
                xmlPay = value;
            }
        }

        //Start SIW336
        //public XmlNode addPay(int iPay, xcEmpPayType cType, xcEmpPayPeriod cPeriod)
        //{
        //    object spt; //string;
        //    object spp; //string;
        //    DctPayType.TryGetValue((int)cType, out spt);
        //    DctPayPeriod.TryGetValue((int)cPeriod, out spp);
        //    return addPay_Code(iPay, (string)spt, (string)spp);
        //}

        //public XmlNode addPay_Code(int iPay, string sType, string sPeriod)
        //{
        //    PayNode = null;
        //    putPay_Code(iPay, sType);
        //    PayPeriod_Code = sPeriod;
        //    return PayNode;
        //}

        //public XmlNode putPay(int iPay, xcEmpPayType cType)
        //{
        //    object sdata; //string
        //    DctPayType.TryGetValue((int)cType, out sdata);
        //    return putPay_Code(iPay, (string)sdata);
        //}

        //public XmlNode putPay_Code(int iPay, string sType)
        //{
        //    Utils.putTypeItem(putPayNode, "", iPay + "", sType, NodeOrder);
        //    return PayNode;
        //}
        public XmlNode addPay(double dPay, xcEmpPayType cType, xcEmpPayPeriod cPeriod)
        {
            object spt; //string;
            object spp; //string;
            DctPayType.TryGetValue((int)cType, out spt);
            DctPayPeriod.TryGetValue((int)cPeriod, out spp);
            return addPay_Code(dPay, (string)spt, (string)spp);
        }

        public XmlNode addPay_Code(double dPay, string sType, string sPeriod)
        {
            PayNode = null;
            putPay_Code(dPay, sType);
            PayPeriod_Code = sPeriod;
            return PayNode;
        }

        public XmlNode putPay(double dPay, xcEmpPayType cType)
        {
            object sdata; //string
            DctPayType.TryGetValue((int)cType, out sdata);
            return putPay_Code(dPay, (string)sdata);
        }

        public XmlNode putPay_Code(double dPay, string sType)
        {
            Utils.putTypeItem(putPayNode, "", dPay + "", sType, NodeOrder);
            return PayNode;
        }
        //End SIW336

        public string Pay
        {
            get
            {
                return Utils.getData(PayNode, "");
            }
            set
            {
                Utils.putData(putPayNode, "", value, NodeOrder);
            }
        }

        public xcEmpPayType PayType
        {
            get
            {
                object etype;
                string sdata = PayType_Code;
                if (!DctPayType.TryGetValue(sdata, out etype))
                    etype = xcEmpPayType.xcEmpPTUnspecified;
                return (xcEmpPayType)etype;
            }
            set
            {
                object sdata; //string
                DctPayType.TryGetValue((int)value, out sdata);
                PayType_Code = (string)sdata;
            }
        }

        public string PayCalculateMethod
        {
            get
            {
                return Utils.getAttribute(PayNode, "", Constants.sEmpPayCalcMethod);
            }
            set
            {
                Utils.putAttribute(putPayNode, "", Constants.sEmpPayCalcMethod, value, NodeOrder);
            }
        }

        public string PayType_Code
        {
            get
            {
                return Utils.getType(PayNode, "");
            }
            set
            {
                Utils.putType(putPayNode, "", value, NodeOrder);
            }
        }

        public xcEmpSpecified PaySpecified
        {
            get
            {
                object etype;
                string sdata = PaySpecified_Code;
                if (!DctSpecified.TryGetValue(sdata, out etype))
                    etype = xcEmpSpecified.xcEmpUnspecified;
                return (xcEmpSpecified)etype;
            }
            set
            {
                object sdata; //string;
                DctSpecified.TryGetValue((int)value, out sdata);
                PaySpecified_Code = (string)sdata;
            }
        }

        public string PaySpecified_Code
        {
            get
            {
                return Utils.getAttribute(PayNode, "", Constants.sEmpSpecified);
            }
            set
            {
                Utils.putAttribute(putPayNode, "", Constants.sEmpSpecified, value, NodeOrder);
            }
        }

        public xcEmpPayPeriod PayPeriod
        {
            get
            {
                object etype;
                string sdata = PayPeriod_Code;
                if (!DctPayPeriod.TryGetValue(sdata, out etype))
                    etype = xcEmpPayPeriod.xcEmpPPUnspecified;
                return (xcEmpPayPeriod)etype;
            }
            set
            {
                object sdata; //string;
                DctPayPeriod.TryGetValue((int)value, out sdata);
                PayPeriod_Code = (string)sdata;
            }
        }

        public string PayPeriod_Code
        {
            get
            {
                return Utils.getAttribute(PayNode, "", Constants.sEmpPeriod);
            }
            set
            {
                Utils.putAttribute(putPayNode, "", Constants.sEmpPeriod, value, NodeOrder);
            }
        }

        public string FullPayDayInjured
        {
            get
            {
                return Utils.getBool(PayNode, "", Constants.sEmpPayFullPayDayInjured);
            }
            set
            {
                Utils.putBool(putPayNode, "", Constants.sEmpPayFullPayDayInjured, value, NodeOrder);
            }
        }

        public string PayContinued
        {
            get
            {
                return Utils.getBool(PayNode, "", Constants.sEmpPayContinued);
            }
            set
            {
                Utils.putBool(putPayNode, "", Constants.sEmpPayContinued, value, NodeOrder);
            }
        }

        public string OTEligible
        {
            get
            {
                return Utils.getBool(PayNode, "", Constants.sEmpPayOTEligible);
            }
            set
            {
                Utils.putBool(putPayNode, "", Constants.sEmpPayOTEligible, value, NodeOrder);
            }
        }

        public int EntitlementCount
        {
            get
            {
                return EntitlementList.Count;
            }
        }

        public XmlNodeList EntitlementList
        {
            get
            {
                // Start SIW163
                if (null == xmlEntitlementList)
                {
                    return getNodeList(Constants.sEmpEntitlement, ref xmlEntitlementList, Node);
                }
                else
                {
                    return xmlEntitlementList;
                }
                // End SIW163
            }
        }

        public XmlNode getEntitlement(bool reset)
        {
            // Start SIW163
            if (null != EntitlementList)
            {
                return getNode(reset, ref xmlEntitlementList, ref xmlEntitlement);
            }
            else
            {
                return null;
            }
            // End SIW163
        }

        public void removeEntitlement()
        {
            if (EntitlementNode != null)
                Node.RemoveChild(EntitlementNode);
            EntitlementNode = null;
        }

        private XmlNode putEntitlementNode
        {
            get
            {
                if (EntitlementNode == null)
                    EntitlementNode = XML.XMLaddNode(putNode, Constants.sEmpEntitlement, NodeOrder);
                return EntitlementNode;
            }
        }

        private XmlNode EntitlementNode
        {
            get
            {
                return xmlEntitlement;
            }
            set
            {
                xmlEntitlement = value;
            }
        }

        public XmlNode addEntitlement(int iEntitlement, string sType, xcEmpSpecified cSpecified,
                                      xcEmpPayPeriod cPeriod, bool bContinued)
        {
            object ssp, spp; //strings
            DctSpecified.TryGetValue((int)cSpecified, out ssp);
            DctPayPeriod.TryGetValue((int)cPeriod, out spp);
            return addEntitlement_Code(iEntitlement, sType, (string)ssp, (string)spp, bContinued);
        }

        public XmlNode addEntitlement_Code(int iEntitlement, string sType, string sSpecified, string sPeriod, bool bContinued)
        {
            EntitlementNode = null;
            Entitlement = iEntitlement + "";
            if (sType != "" && sType != null)
                EntitlementType = sType;
            if (sSpecified != "" && sSpecified != null)
                EntitlementSpecified_Code = sSpecified;
            if (sPeriod != "" && sPeriod != null)
                EntitlementPeriod_Code = sPeriod;
            EntitlementContinued = bContinued.ToString();
            return EntitlementNode;
        }

        public string Entitlement
        {
            get
            {
                return Utils.getData(EntitlementNode, "");
            }
            set
            {
                Utils.putData(putEntitlementNode, "", value, NodeOrder);
            }
        }

        public string EntitlementType
        {
            get
            {
                return Utils.getType(EntitlementNode, "");
            }
            set
            {
                Utils.putType(putEntitlementNode, "", value, NodeOrder);
            }
        }

        public xcEmpSpecified EntitlementSpecified
        {
            get
            {
                object etype;
                string sdata = EntitlementSpecified_Code;
                if (!DctSpecified.TryGetValue(sdata, out etype))
                    etype = xcEmpSpecified.xcEmpUnspecified;
                return (xcEmpSpecified)etype;
            }
            set
            {
                object sdata; //string;
                DctSpecified.TryGetValue((int)value, out sdata);
                EntitlementSpecified_Code = (string)sdata;
            }
        }

        public string EntitlementSpecified_Code
        {
            get
            {
                return Utils.getAttribute(EntitlementNode, "", Constants.sEmpSpecified);
            }
            set
            {
                Utils.putAttribute(putEntitlementNode, "", Constants.sEmpSpecified, value, NodeOrder);
            }
        }

        public xcEmpPayPeriod EntitlementPeriod
        {
            get
            {
                object etype;
                string sdata = EntitlementPeriod_Code;
                if (!DctPayPeriod.TryGetValue(sdata, out etype))
                    etype = xcEmpPayPeriod.xcEmpPPUnspecified;
                return (xcEmpPayPeriod)etype;
            }
            set
            {
                object sdata; //string;
                DctPayPeriod.TryGetValue((int)value, out sdata);
                EntitlementPeriod_Code = (string)sdata;
            }
        }

        public string EntitlementPeriod_Code
        {
            get
            {
                return Utils.getAttribute(EntitlementNode, "", Constants.sEmpPeriod);
            }
            set
            {
                Utils.putAttribute(putEntitlementNode, "", Constants.sEmpPeriod, value, NodeOrder);
            }
        }

        public string EntitlementContinued
        {
            get
            {
                return Utils.getBool(EntitlementNode, "", Constants.sEmpPayContinued);
            }
            set
            {
                Utils.putBool(putEntitlementNode, "", Constants.sEmpPayContinued, value, NodeOrder);
            }
        }

        private XmlNode putPrevWCNode
        {
            get
            {
                if (PrevWCNode == null)
                    PrevWCNode = XML.XMLaddNode(putNode, Constants.sEmpPrevWC, NodeOrder);
                return PrevWCNode;
            }
        }

        private XmlNode PrevWCNode
        {
            get
            {
                if (xmlPrevWCNode == null)
                    xmlPrevWCNode = Utils.getNode(Node, Constants.sEmpPrevWC);
                return xmlPrevWCNode;
            }
            set
            {
                xmlPrevWCNode = value;
            }
        }

        public XmlNode putPrevWC(string sIndicator, string sDate, string sdescription)
        {
            PrevWCNode = null;
            if (sIndicator != "" && sIndicator != null)
                PrevWCIndicator = sIndicator;
            if (sDate != "" && sDate != null)
                PrevWCDate = sDate;
            if (sdescription != "" && sdescription != null)
                PrevWCDesc = sdescription;
            return PrevWCNode;
        }

        public string PrevWCIndicator
        {
            get
            {
                return Utils.getBool(PrevWCNode, "", "");
            }
            set
            {
                Utils.putBool(putPrevWCNode, "", "", value, sPrevWCNodeOrder);
            }
        }

        public string PrevWCDate
        {
            get
            {
                return Utils.getDate(PrevWCNode, Constants.sEmpPrevWCDate);
            }
            set
            {
                Utils.putDate(putPrevWCNode, Constants.sEmpPrevWCDate, value, sPrevWCNodeOrder);
            }
        }

        public string PrevWCDesc
        {
            get
            {
                return Utils.getData(PrevWCNode, Constants.sEmpPrevWCDesc);
            }
            set
            {
                Utils.putData(putPrevWCNode, Constants.sEmpPrevWCDesc, value, sPrevWCNodeOrder);
            }
        }

        private XmlNode putMedInfoNode
        {
            get
            {
                if (MedInfoNode == null)
                    MedInfoNode = XML.XMLaddNode(putNode, Constants.sEmpMedInfoNode, NodeOrder);
                return MedInfoNode;
            }
        }

        private XmlNode MedInfoNode
        {
            get
            {
                if (xmlMedInfoNode == null)
                    xmlMedInfoNode = Utils.getNode(Node, Constants.sEmpMedInfoNode);
                return xmlMedInfoNode;
            }
            set
            {
                xmlMedInfoNode = value;
            }
        }
        //Start SIW392
        private XmlNode LostDaysNode
        {
            get
            {                
                return xmlLostDays;
            }
            set
            {
                xmlLostDays = value;
            }
        }
        private XmlNode putLostDaysNode
        {
            get
            {
                if (LostDaysNode == null)
                    LostDaysNode = XML.XMLaddNode(putNode, Constants.sEmpLostDays, NodeOrder);
                return LostDaysNode;
            }
        }       

        public string LostDaysID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(LostDaysNode, Constants.sLDID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sLSDIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                if (Node != null)
                    XML.XMLSetAttributeValue(putLostDaysNode, Constants.sLDID, Constants.sLSDIDPfx + value);
            }
        }
        public XmlNode putLostDay(string sLostDayId, string sDateLastWorked, string sDateReturnedToWork)
        {
            LostDaysNode = null;
            if (sLostDayId != "" && sLostDayId != null)
                LostDaysID = sLostDayId;
            if (sDateLastWorked != "" && sDateLastWorked != null)
                LastDayWorked = sDateLastWorked;
            if (sDateReturnedToWork != "" && sDateReturnedToWork != null)
                ReturnToWorkDate = sDateReturnedToWork;
            return LostDaysNode;
        }
        public XmlNodeList LostDaysList
        {
            get
            {
                if (null == xmlLostDaysList)
                {
                    return getNodeList(Constants.sEmpLostDays, ref xmlLostDaysList, Node);
                }
                else
                {
                    return xmlLostDaysList;
                }
            }
        }

        public XmlNode getLostDays(bool reset)
        {           
            if (null != LostDaysList)
            {
                return getNode(reset, ref xmlLostDaysList, ref xmlLostDays);
            }
            else
            {
                return null;
            }
        }
        public int LostDaysCount
        {
            get
            {
                return LostDaysList.Count;
            }
        }

        //End SIW392

        //RESTRICTED DAYS

        //Start SIW392
        private XmlNode RestrictedDaysNode
        {
            get
            {
                return xmlRestrictedDays;
            }
            set
            {
                xmlRestrictedDays = value;
            }
        }
        private XmlNode putRestrictedDaysNode
        {
            get
            {
                if (RestrictedDaysNode == null)
                    RestrictedDaysNode = XML.XMLaddNode(putNode, Constants.sEmpRestrictedDays, NodeOrder);
                return RestrictedDaysNode;
            }
        }

        public string RestrictedDaysID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(RestrictedDaysNode, Constants.sRSDId);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sRSDIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                if (Node != null)
                    XML.XMLSetAttributeValue(putRestrictedDaysNode, Constants.sRSDId, Constants.sRSDIDPfx + value);
            }
        }
        public XmlNode putRestrictedDay(string sRSDIdDayId, string sFirstDayRstc, string sLastDayRstc, string sPercntDisabled)
        {
            RestrictedDaysNode = null;
            if (sRSDIdDayId != "" && sRSDIdDayId != null)
                RestrictedDaysID = sRSDIdDayId;
            if (sFirstDayRstc != "" && sFirstDayRstc != null)
                FirstDayRstc = sFirstDayRstc;
            if (sLastDayRstc != "" && sLastDayRstc != null)
                LastDayRstc = sLastDayRstc;
            if (sPercntDisabled != "" && sPercntDisabled != null)
                PercntDisabled = sPercntDisabled;
            return RestrictedDaysNode;
        }

        public string FirstDayRstc
        {
            get
            {
                return Utils.getDate(RestrictedDaysNode, Constants.sRsdFirstDayRstDate);
            }
            set
            {
                Utils.putDate(putRestrictedDaysNode, Constants.sRsdFirstDayRstDate, value, sRSDNodeOrder); //SIW392
            }
        }
        public string LastDayRstc
        {
            get
            {
                return Utils.getDate(RestrictedDaysNode, Constants.sRsdLastDayRstDate);
            }
            set
            {
                Utils.putDate(putRestrictedDaysNode, Constants.sRsdLastDayRstDate, value, sRSDNodeOrder); //SIW392
            }
        }
        public string PercntDisabled
        {
            get
            {
                return Utils.getData(RestrictedDaysNode, Constants.sRsdPercentDisabled);
            }
            set
            {
                Utils.putData(putRestrictedDaysNode, Constants.sRsdPercentDisabled, value, sRSDNodeOrder);
            }
        }

        public XmlNode putPosition(string sDesc, string sCode)
        {
            return putPosition(sDesc, sCode, "");
        }

        public XmlNode putPosition(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putRestrictedDaysNode, Constants.sRsdPosition, sDesc, sCode, sRSDNodeOrder, scodeid);
        }

        public string Positioned
        {
            get
            {
                return Utils.getData(RestrictedDaysNode, Constants.sRsdPosition);
            }
            set
            {
                Utils.putData(putRestrictedDaysNode, Constants.sRsdPosition, value, sRSDNodeOrder);
            }
        }

        public string Positioned_Code
        {
            get
            {
                return Utils.getCode(RestrictedDaysNode, Constants.sRsdPosition);
            }
            set
            {
                Utils.putCode(putRestrictedDaysNode, Constants.sRsdPosition, value, sRSDNodeOrder);
            }
        }

        public string Positioned_CodeID
        {
            get
            {
                return Utils.getCodeID(RestrictedDaysNode, Constants.sRsdPosition);
            }
            set
            {
                Utils.putCodeID(putRestrictedDaysNode, Constants.sRsdPosition, value, sRSDNodeOrder);
            }
        }

        public XmlNodeList RestrictedDaysList
        {
            get
            {
                if (null == xmlRestrictedDaysList)
                {
                    return getNodeList(Constants.sEmpRestrictedDays, ref xmlRestrictedDaysList, Node);
                }
                else
                {
                    return xmlRestrictedDaysList;
                }
            }
        }

        public XmlNode getRestrictedDays(bool reset)
        {
            if (null != RestrictedDaysList)
            {
                return getNode(reset, ref xmlRestrictedDaysList, ref xmlRestrictedDays);
            }
            else
            {
                return null;
            }
        }
        public int RestrictedDaysCount
        {
            get
            {
                return RestrictedDaysList.Count;
            }
        }
        //End SIW392

        public string Allergies
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpAllergies, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpAllergies, "", value, sMedInfoNodeOrder);
            }
        }

        public string AllergiesDesc
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpAllergies);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpAllergies, value, sMedInfoNodeOrder);
            }
        }

        public string Medications
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpMedications);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpMedications, value, sMedInfoNodeOrder);
            }
        }

        public string SpecialNeeds
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpSpecNeeds);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpSpecNeeds, value, sMedInfoNodeOrder);
            }
        }

        public string Height
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpHeight);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpHeight, value, sMedInfoNodeOrder);
            }
        }

        public string Weight
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpWeight);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpWeight, value, sMedInfoNodeOrder);
            }
        }

        public string Smoker
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpSmoker, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpSmoker, "", value, sMedInfoNodeOrder);
            }
        }

        public string PPD
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpPPD);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpPPD, value, sMedInfoNodeOrder);
            }
        }

        public string AlcoholUse
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpAlcoholUse, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpAlcoholUse, "", value, sMedInfoNodeOrder);
            }
        }

        public string Cancer
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpCancer, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpCancer, "", value, sMedInfoNodeOrder);
            }
        }

        public string Diabetes
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpDiabetes, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpDiabetes, "", value, sMedInfoNodeOrder);
            }
        }

        public string HeartCond
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpHeartCond, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpHeartCond, "", value, sMedInfoNodeOrder);
            }
        }

        public string Hepatitis
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpHepatitis, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpHepatitis, "", value, sMedInfoNodeOrder);
            }
        }

        public string Hypertension
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpHypertension, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpHypertension, "", value, sMedInfoNodeOrder);
            }
        }

        public string LungDisease
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpLungDisease, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpLungDisease, "", value, sMedInfoNodeOrder);
            }
        }

        public string Seizures
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpSeizures, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpSeizures, "", value, sMedInfoNodeOrder);
            }
        }

        public string BloodDisorder
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpBloodDisorder, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpBloodDisorder, "", value, sMedInfoNodeOrder);
            }
        }

        public string MentalDisorder
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpMentalDisorder, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpMentalDisorder, "", value, sMedInfoNodeOrder);
            }
        }

        public string Arthritis
        {
            get
            {
                return Utils.getBool(MedInfoNode, Constants.sEmpArthritis, "");
            }
            set
            {
                Utils.putBool(putMedInfoNode, Constants.sEmpArthritis, "", value, sMedInfoNodeOrder);
            }
        }

        public string Other
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpOther);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpOther, value, sMedInfoNodeOrder);
            }
        }

        public string PastSurgeries
        {
            get
            {
                return Utils.getData(MedInfoNode, Constants.sEmpPastSurgeries);
            }
            set
            {
                Utils.putData(putMedInfoNode, Constants.sEmpPastSurgeries, value, sMedInfoNodeOrder);
            }
        }
        //Start SIW391
        public string TimeDayBegan
        {
            get
            {
                return Utils.getTime(Node, Constants.sEmpTimeDayBegan);
            }
            set
            {
                Utils.putTime(putNode, Constants.sEmpTimeDayBegan, value, NodeOrder);
            }
        }
        //End SIW391
        public int WklySchedCount
        {
            get
            {
                return WklySchedList.Count;
            }
        }

        public XmlNodeList WklySchedList
        {
            get
            {
                //Start SIW163
                if (null == xmlWklySchedList)
                {
                    return getNodeList(Constants.sEmpWeeklySchedule, ref xmlWklySchedList, putNode);
                }
                else
                {
                    return xmlWklySchedList;
                }
                //End SIW163
            }
        }

        public XmlNode getWklySched(bool reset)
        {
            if (WklySchedList != null)
            {
                xmlWklySchedDay = null;
                xmlWklySchedDayList = null;
            }
            return getNode(reset, ref xmlWklySchedList, ref xmlWklySched);            
        }

        public void removeWklySched()
        {
            if (WklySchedNode != null)
                Node.RemoveChild(WklySchedNode);
            WklySchedNode = null;
        }

        private XmlNode putWklySchedNode
        {
            get
            {
                if (WklySchedNode == null)
                    WklySchedNode = XML.XMLaddNode(putNode, Constants.sEmpWeeklySchedule, NodeOrder);
                return WklySchedNode;
            }
        }

        private XmlNode WklySchedNode
        {
            get
            {
                return xmlWklySched;
            }
            set
            {
                xmlWklySched = value;
                xmlWklySchedDay = null;
                xmlWklySchedDayList = null;
            }
        }

        public XmlNode addWklySched(xcEmpSpecified cSpecified, DayOfWeek cFirstDayOfWeek, int iDaysWorked)
        {
            object ssp, sfd; //strings
            DctSpecified.TryGetValue((int)cSpecified, out ssp);
            DctDOW.TryGetValue((int)cFirstDayOfWeek, out sfd);
            return addWklySched_Code((string)ssp, (string)sfd, iDaysWorked + "");
        }

        public XmlNode addWklySched_Code(string sSpecified, string sFirstDayOfWeek, string sDaysWorked)
        {
            WklySchedNode = null;
            if (sSpecified != "" && sSpecified != null)
                WklySchedSpecified_Code = sSpecified;
            if (sFirstDayOfWeek != "" && sFirstDayOfWeek != null)
                WklySchedFirstDayOfWeek_Code = sFirstDayOfWeek;
            if (sDaysWorked != "" && sDaysWorked != null)
                WklySchedDaysWorked = sDaysWorked;
            return WklySchedNode;
        }

        public xcEmpSpecified WklySchedSpecified
        {
            get
            {
                object etype;
                string sdata = WklySchedSpecified_Code;
                if (!DctSpecified.TryGetValue(sdata, out etype))
                    etype = xcEmpSpecified.xcEmpUnspecified;
                return (xcEmpSpecified)etype;
            }
            set
            {
                object sdata; //string
                DctSpecified.TryGetValue((int)value, out sdata);
                WklySchedSpecified_Code = (string)sdata;
            }
        }

        public string WklySchedSpecified_Code
        {
            get
            {
                return Utils.getAttribute(WklySchedNode, "", Constants.sEmpSpecified);
            }
            set
            {
                Utils.putAttribute(putWklySchedNode, "", Constants.sEmpSpecified, value, NodeOrder);
            }
        }

        public DayOfWeek WklySchedFirstDayOfWeek
        {
            get
            {
                object etype;
                string sdata = WklySchedFirstDayOfWeek_Code;
                if (!DctDOW.TryGetValue(sdata, out etype))
                    etype = DayOfWeek.Sunday;
                return (DayOfWeek)etype;
            }
            set
            {
                object sdata; //string
                DctDOW.TryGetValue((int)value, out sdata);
                WklySchedFirstDayOfWeek_Code = (string)sdata;
            }
        }

        public string WklySchedFirstDayOfWeek_Code
        {
            get
            {
                return Utils.getAttribute(WklySchedNode, "", Constants.sEmpWSStartDay);
            }
            set
            {
                Utils.putAttribute(putWklySchedNode, "", Constants.sEmpWSStartDay, value, NodeOrder);
            }
        }

        public string WklySchedDaysWorked
        {
            get
            {
                return Utils.getAttribute(WklySchedNode, "", Constants.sEmpWSNumDaysWorked);
            }
            set
            {
                Utils.putAttribute(putWklySchedNode, "", Constants.sEmpWSNumDaysWorked, value, NodeOrder);
            }
        }

        public int WklySchedDayCount
        {
            get
            {
                return WklySchedDayList.Count;
            }
        }

        public XmlNodeList WklySchedDayList
        {
            get
            {
                //Start SIW163
                if (null == xmlWklySchedDayList)
                {
                    if (null == WklySchedNode)
                    {
                        getWklySched(true);
                    }
                    return getNodeList(Constants.sEmpWSDay, ref xmlWklySchedDayList, WklySchedNode);
                }
                else
                {
                    return xmlWklySchedDayList;
                }
                //End SIW163
            }
        }

        public XmlNode getWklySchedDay(bool reset)
        {
            // Start SIW163
            if (null != WklySchedDayList)
            {
                return getNode(reset, ref xmlWklySchedDayList, ref xmlWklySchedDay);
            }
            else
            {
                return null;
            }
            // End SIW163
        }

        public void removeWklySchedDay()
        {
            if (WklySchedDayNode != null)
                Node.RemoveChild(WklySchedDayNode);
            WklySchedDayNode = null;
        }

        private XmlNode putWklySchedDayNode
        {
            get
            {
                if (WklySchedDayNode == null)
                    WklySchedDayNode = XML.XMLaddNode(putWklySchedNode, Constants.sEmpWSDay, sWSNodeOrder);
                return WklySchedDayNode;
            }
        }

        private XmlNode WklySchedDayNode
        {
            get
            {
                return xmlWklySchedDay;
            }
            set
            {
                xmlWklySchedDay = value;
            }
        }

        public XmlNode addWklySchedDay(DayOfWeek cDayOfWeek, int iRegHours, int iOTHours, string sStartTime, string sEndTime)
        {
            object sdata; //string
            DctDOW.TryGetValue((int)cDayOfWeek, out sdata);
            return addWklySchedDay_Code((string)sdata, iRegHours + "", iOTHours + "", sStartTime, sEndTime);
        }

        public XmlNode addWklySchedDay_Code(string sDayOfWeek, string sRegHours, string sOTHours, string sStartTime, string sEndTime)
        {
            WklySchedDayNode = null;
            if (sDayOfWeek != "" && sDayOfWeek != null)
                WklySchedDayOfWeek_Code = sDayOfWeek;
            if (sRegHours != "" && sRegHours != null)
                WklySchedDayRegHours = sRegHours;
            if (sOTHours != "" && sOTHours != null)
                WklySchedDayOTHours = sOTHours;
            if (sStartTime != "" && sStartTime != null)
                WklySchedDayShiftStart = sStartTime;
            if (sEndTime != "" && sEndTime != null)
                WklySchedDayShiftEnd = sEndTime;
            return WklySchedDayNode;
        }

        public DayOfWeek WklySchedDayOfWeek
        {
            get
            {
                object etype;
                string sdata = WklySchedDayOfWeek_Code.ToUpper();
                if (!DctDOW.TryGetValue(sdata, out etype))
                    etype = DayOfWeek.Sunday;
                return (DayOfWeek)etype;
            }
            set
            {
                object sdata; //string
                DctDOW.TryGetValue((int)value, out sdata);
                sdata = ((string)sdata).ToUpper();
                WklySchedDayOfWeek_Code = (string)sdata;
            }
        }

        public string WklySchedDayOfWeek_Code
        {
            get
            {
                return Utils.getCode(WklySchedDayNode, "");
            }
            set
            {
                Utils.putCode(putWklySchedDayNode, "", value, sWSNodeOrder);
            }
        }

        public string WklySchedDayRegHours
        {
            get
            {
                return Utils.getAttribute(WklySchedDayNode, "", Constants.sEmpWSDayRegHours);
            }
            set
            {
                Utils.putAttribute(putWklySchedDayNode, "", Constants.sEmpWSDayRegHours, value, sWSNodeOrder);
            }
        }

        public string WklySchedDayOTHours
        {
            get
            {
                return Utils.getAttribute(WklySchedDayNode, "", Constants.sEmpWSDayEstOTHours);
            }
            set
            {
                Utils.putAttribute(putWklySchedDayNode, "", Constants.sEmpWSDayEstOTHours, value, sWSNodeOrder);
            }
        }

        public string WklySchedDayShiftStart
        {
            get
            {
                return Utils.getTime(WklySchedDayNode, Constants.sEmpWSDayShiftStart);
            }
            set
            {
                Utils.putTime(putWklySchedDayNode, Constants.sEmpWSDayShiftStart, value, sWSNodeOrder);
            }
        }

        public string WklySchedDayShiftEnd
        {
            get
            {
                return Utils.getTime(WklySchedDayNode, Constants.sEmpWSDayShiftEnd);
            }
            set
            {
                Utils.putTime(putWklySchedDayNode, Constants.sEmpWSDayShiftEnd, value, sWSNodeOrder);
            }
        }

        public string AdjustedWage
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpAdjustedW);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpAdjustedW, value, NodeOrder);
            }
        }

        //Start SIW44
        public string CompnstnRate
        {
            get
            {
                return Utils.getData(Node, Constants.sEmpCR);
            }
            set
            {
                Utils.putData(putNode, Constants.sEmpCR, value, NodeOrder);
            }
        }
        //End SIW44
        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            xmlWklySched = null;
            xmlWklySchedList = null;
            xmlWklySchedDay = null;
            xmlWklySchedDayList = null;
            xmlPay = null;
            xmlPayList = null;
            xmlBenefit = null;
            xmlBenefitList = null;
            xmlEntitlement = null;
            xmlEntitlementList = null;
            xmlPrevWCNode = null;
            xmlMedInfoNode = null;
            xmlLostDays = null;  //SIW392
            xmlLostDaysList = null; //SIW392
            xmlLostDaysList = null; //SIW392
            xmlRestrictedDaysList = null; //SIW392
            PartyInvolved = null;
            CommentReference = null;
            EmployeeEntity = null;
        }

        public XMLEntity EmployerEntity
        {
            get
            {
                if (PartyInvolved.findpartyInvolvedbyRole(Constants.sEmpEmployerRelationship) != null)
                    return PartyInvolved.Entity;
                else
                    return null;
            }
        }

        public XMLEntity EmployeeEntity
        {
            get
            {
                if (m_xmlEntity == null)
                {
                    m_xmlEntity = new XMLEntity();
                    //SIW529 m_xmlEntity.Document = Document;
                    LinkXMLObjects(m_xmlEntity);    //SIW529
                }
                m_xmlEntity.getEntitybyID(EntityID);
                return m_xmlEntity;
            }
            set
            {
                m_xmlEntity = value;
                if (m_xmlEntity != null)
                    EntityID = m_xmlEntity.EntityID;
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create("0");
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            Node = Utils.getNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLUnitStats();
                    ((XMLUnitStats)m_Parent).Employee = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }
    }
}
