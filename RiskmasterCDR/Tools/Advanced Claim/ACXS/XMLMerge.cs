﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 06/25/2011 |  N7268  |   AM       | Mail Merge Devlopment
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLMerge : XMLXSNodeWithList
    {
        /******************************************************************************************
         * The XMLSearch class provides the functionality to support and manage a Search Node
         ******************************************************************************************
        '  <MERGE ID="MRG$">
        '    <NAME>Friendly name stored on MERGE_FORM_DEF.FORM_NAME</NAME>
        '    <FORM ID="45" NAME= "FearlessTest" FORMFILE="MMT20110623151054_10173516.DOC"/>
        '    <MAX_NUMBER_OF_ROWS>Maximum Number of rows to return</MAX_NUMBER_OF_ROWS>
        '    <SEARCH_DIRECTION>FORWARD</SEARCH_DIRECTION>
        '    <FIELDS COUNT=nn NEXTID=nn>
        '        <FIELD ID=FLD1 TYPE="[BOOLEAN,DATE,CODE,STRING,NUMBER]">
        '           <FILTER_NAME>Field name in SEARCH_DICTIONARY</FILTER_NAME>
        '           <OPERATOR>String representing the range (GT, LT, GE, LE, BET, EQ, ...)</OPERATOR>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <SORT>Value of sorting preferences 1,2 or 3</SORT>
        '           <DISPLAY_NAME>Resulting FIELD_DESC value in SEARCH_DICTIOANRY table</DISPLAY_NAME>
        '           <DISPLAY_MASK>Resulting OPT_MASK value in SEARCH_DICTIOANRY table</DISPLAY_NAME>  // SI06501
        '        </FIELD>
        '    </FIELDS>
        '    <DATA_ROWS COUNT = nn NEXTID= nn>
        '        <ROW ID=recordkey>  
        '            <COL IDREF=FLD1 TYPE="[BOOLEAN,DATE,CODE,STRING,NUMBER]">resulting value</COL>
        '            <COL IDREF=FLD2 TYPE="CODE" CODE_ID="nn" CODE="xx">resulting value</COL>
        '            <COL IDREF=FLD3 TYPE="DATE" YEAR="nnnn" MONTH="nn" DAY="nn">resulting value</COL>
        '            <COL IDREF=FLD3 TYPE="BOOLEAN" INDICATOR="Yes"/>        
        '        </ROW>
        '    </DATA_ROWS>
        '  </MERGE>
        '******************************************************************************************/

        private XmlNode m_xmlField;
        private XmlNode m_xmlFields;
        private XmlNodeList m_xmlFieldList;
        private XmlNode m_xmlDataRow;
        private XmlNode m_xmlDataRows;
        private XmlNodeList m_xmlDataRowList;
        private XmlNode m_xmlColumn;
        private XmlNodeList m_xmlColumnList;
        private ArrayList sFieldNodeOrder;
        private ArrayList sDataRowNodeOrder;
        private XmlNode m_xmlForm;

        public XMLMerge()
        {

            xmlThisNode = null;

            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sMergeName);
            sThisNodeOrder.Add(Constants.sMrgFormName);            
            sThisNodeOrder.Add(Constants.sSrchMaxRows);
            sThisNodeOrder.Add(Constants.sSrchNextSortKey);                 
            sThisNodeOrder.Add(Constants.sSrchPreviousSortKey);             
            sThisNodeOrder.Add(Constants.sSrchNextTechKey);                 
            sThisNodeOrder.Add(Constants.sSrchPreviousTechKey);             
            sThisNodeOrder.Add(Constants.sSrchSearchDirection);             
            sThisNodeOrder.Add(Constants.sSrchFields);
            sThisNodeOrder.Add(Constants.sSrchDataRows);

            sFieldNodeOrder = new ArrayList();
            sFieldNodeOrder.Add(Constants.sSrchFldFilterName);
            sFieldNodeOrder.Add(Constants.sSrchFldOper);
            sFieldNodeOrder.Add(Constants.sSrchFldFilterVal1);            
            sFieldNodeOrder.Add(Constants.sSrchFldDispName);

            sDataRowNodeOrder = new ArrayList();
            sDataRowNodeOrder.Add(Constants.sSrchRowKey);
            sDataRowNodeOrder.Add(Constants.sSrchRowCol);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sMergeNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "");
        }

        public XmlNode Create(string sMergeId, string sMergeCat, string sFormId, string sFormName, string sFormFile, string sMaxRows)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);                
                Node = xmlElement;
                
                MergeID = sMergeId;
                MergeName = sMergeCat;
                MaxNumberOfRows = sMaxRows;

                xmlElement = (XmlElement)XML.XMLNewElement(Constants.sMrgFormName, true, xmlDocument, Node);
                Form = xmlElement;
                FormId = sFormId;
                FormName = sFormName;
                FormFile = sFormFile;

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLMerge.Create");
                return null;
            }
        }

        public string MergeID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sMrgFormId);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sMrgIDPfx  && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                XML.XMLSetAttributeValue(putNode, Constants.sMrgFormId, Constants.sMrgIDPfx + lid);
            }
        }

        public string MergeName
        {
            get
            {
                return Utils.getData(Node, Constants.sMergeName);
            }
            set
            {
                Utils.putData(putNode, Constants.sMergeName, value, NodeOrder);
            }
        }

        public XmlNode Form
        {
            get{ return this.m_xmlForm ;}
            protected set {this.m_xmlForm=value;}
        }

        public XmlNode putForm
        {
            get
            {
                if (Form == null)
                    Create();
                return Form ;
            }
        }

        public string FormId
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Form, Constants.sMrgFormId);
                return strdata.Trim();
            }
            set
            {
                string lid = value;
                XML.XMLSetAttributeValue(putForm, Constants.sMrgFormId, lid);
            }
        }

        public string FormName
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Form, Constants.sMergeName);
                return strdata.Trim();
            }
            set
            {
                string lid = value;
                XML.XMLSetAttributeValue(putForm, Constants.sMergeName, lid);
            }
        }

        public string FormFile
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Form, Constants.sMrgFormFile);
                return strdata.Trim();
            }
            set
            {
                string lid = value;
                XML.XMLSetAttributeValue(putForm, Constants.sMrgFormFile, lid);
            }
        }

        public string MaxNumberOfRows
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchMaxRows);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchMaxRows, value, NodeOrder);
            }
        }

        public string NextSortKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchNextSortKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchNextSortKey, value, NodeOrder);
            }
        }

        public string PreviousSortKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchPreviousTechKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchPreviousTechKey, value, NodeOrder);
            }
        }

        public string NextTechKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchNextTechKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchNextTechKey, value, NodeOrder);
            }
        }

        public string PreviousTechKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchPreviousTechKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchPreviousTechKey, value, NodeOrder);
            }
        }

        public string SearchDirection
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchSearchDirection);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchSearchDirection, value, NodeOrder);
            }
        }

        public XmlNodeList FieldList
        {
            get
            {
                return getNodeList(Constants.sSrchField, ref m_xmlFieldList, FieldsNode); 

            }
        }

        public XmlNode FieldsNode
        {
            get
            {
                if (m_xmlFields == null)
                    m_xmlFields = XML.XMLGetNode(Node, Constants.sSrchFields);
                return m_xmlFields;
            }
            set
            {
                m_xmlFields = value;
            }
        }

        public XmlNode putFieldsNode
        {
            get
            {
                if (FieldsNode == null)
                {
                    FieldsNode = XML.XMLaddNode(putNode, Constants.sSrchFields, NodeOrder);
                    NextFieldID = "1";
                    FieldCount = 0;
                }
                return FieldsNode;
            }
        }

        public int FieldCount
        {
            get
            {
                string strdata = Utils.getAttribute(FieldsNode, "", Constants.sSrchFldCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putFieldsNode, "", Constants.sSrchFldCount, value + "", sFieldNodeOrder);
            }
        }

        public string NextFieldID
        {
            get
            {
                return Utils.getAttribute(FieldsNode, "", Constants.sSrchFldNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putFieldsNode, "", Constants.sSrchFldNextID, value, sFieldNodeOrder);
            }
        }

        public string getNextFieldID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextFieldID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextFieldID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public XmlNode getField(bool reset)
        {
            object o = FieldList;
            return getNode(reset, ref m_xmlFieldList, ref m_xmlField);
        }

        public XmlNode getFieldbyID(string sID)
        {
            if (FieldID != sID)
            {
                getField(true);
                while (FieldNode != null)
                {
                    if (FieldID == sID)
                        break;
                    getField(false);
                }
            }
            return FieldNode;
        }

        public XmlNode putFieldNode
        {
            get
            {
                if (FieldNode == null)
                    FieldNode = XML.XMLaddNode(putFieldsNode, Constants.sSrchField, Utils.sNodeOrder);
                return FieldNode;
            }
        }

        public XmlNode FieldNode
        {
            get
            {
                return m_xmlField;
            }
            set
            {
                m_xmlField = value;
            }
        }

        public XmlNode addField()
        {
            FieldNode = null;
            FieldCount = FieldCount + 1;
            return putFieldNode;
        }

        public XmlNode putField(string sFieldID, string sFilterName, string sOperator,
                                   string sValue1, string sSort, string sDispName)
        {
            if (sFieldID != "" && sFieldID != null)
                FieldID = sFieldID;
            if (sFilterName != "" && sFilterName != null)
                FilterName = sFilterName;
            if (sOperator != "" && sOperator != null)
                Operator = sOperator;
            if (sValue1 != "" && sValue1 != null)
                Value1 = sValue1;
            if (sSort != "" && sSort != null)
                Sort = sSort;
            if (sDispName != "" && sDispName != null)
                DisplayName = sDispName;
            return FieldNode;
        }

        public string FldIDPrefix
        {
            get
            {
                return Constants.sSrchFldIDPfx;
            }
        }

        public string extFieldID(string strdata)
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sSrchFldIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string FieldID
        {
            get
            {
                string strdata;
                string lid = string.Empty;
                if (FieldNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(FieldNode, Constants.sSrchFldID);
                    lid = extFieldID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = getNextFieldID(null);
                XML.XMLSetAttributeValue(putFieldNode, Constants.sSrchFldID, Constants.sSrchFldIDPfx + value);
            }
        }

        public string FieldType
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(FieldNode, Constants.sdtType);
                return lid;
            }
            set
            {
                Utils.putAttribute(putFieldNode, "", Constants.sdtType, value, Utils.sNodeOrder);	
            }
        }

        public string FilterName
        {
            get
            {                
                return Utils.getData(FieldNode, Constants.sSrchFldFilterName);
            }
            set
            {
                Utils.putData(putFieldNode, Constants.sSrchFldFilterName, value, NodeOrder);
            }
        }
        public string Operator
        {
            get
            {                
                return Utils.getData(FieldNode, Constants.sSrchFldOper);
            }
            set
            {                
                Utils.putData(putFieldNode, Constants.sSrchFldOper, value, NodeOrder);
            }
        }
        public string Value1
        {
            get
            {
                return Utils.getData(FieldNode, Constants.sSrchFldFilterVal1);
            }
            set
            {
                Utils.putData(putFieldNode, Constants.sSrchFldFilterVal1, value, sFieldNodeOrder);
            }
        }
        public string Sort
        {
            get
            {                
                return Utils.getData(FieldNode, Constants.sSrchFldSort);
            }
            set
            {                
                Utils.putData(putFieldNode, Constants.sSrchFldSort, value, NodeOrder);
            }
        }
        public string DisplayName
        {
            get
            {
                return Utils.getData(FieldNode, Constants.sSrchFldDispName);
            }
            set
            {
                Utils.putData(putFieldNode, Constants.sSrchFldDispName, value, NodeOrder);
            }
        }

        public string DisplayMask
        {
            get
            {            
                return Utils.getData(FieldNode, Constants.sSrchFldDispMask);
            }
            set
            {                
                Utils.putData(putFieldNode, Constants.sSrchFldDispMask, value, NodeOrder);
            }
        }

        public XmlNodeList DataRowList
        {
            get
            {
                return getNodeList(Constants.sSrchDataRow, ref m_xmlDataRowList, DataRowsNode); // SI06501
            }
        }

        public XmlNode DataRowsNode
        {
            get
            {
                if (m_xmlDataRows == null)
                    m_xmlDataRows = XML.XMLGetNode(Node, Constants.sSrchDataRows);
                return m_xmlDataRows;
            }
            set
            {
                m_xmlDataRows = value;
            }
        }

        public XmlNode putDataRowsNode
        {
            get
            {
                if (DataRowsNode == null)
                {
                    DataRowsNode = XML.XMLaddNode(putNode, Constants.sSrchDataRows, NodeOrder);
                    NextDataRowID = "1";
                    DataRowCount = 0;
                }
                return DataRowsNode;
            }
        }

        public int DataRowCount
        {
            get
            {
                string strdata = Utils.getAttribute(DataRowsNode, "", Constants.sSrchDRCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putDataRowsNode, "", Constants.sSrchDRCount, value + "", sDataRowNodeOrder);
            }
        }

        public string NextDataRowID
        {
            get
            {
                return Utils.getAttribute(DataRowsNode, "", Constants.sSrchDRNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putDataRowsNode, "", Constants.sSrchDRNextID, value, sDataRowNodeOrder);
            }
        }

        public String getNextDataRowID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextDataRowID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextDataRowID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public XmlNode getDataRow(bool reset)
        {
            object o = DataRowList;
            return getNode(reset, ref m_xmlDataRowList, ref m_xmlDataRow);
        }

        public XmlNode getDataRowbyID(string sID)
        {
            if (DataRowID != sID)
            {
                getDataRow(true);
                while (DataRowNode != null)
                {
                    if (DataRowID == sID)
                        break;
                    getDataRow(false);
                }
            }
            return DataRowNode;
        }

        public XmlNode putDataRowNode
        {
            get
            {
                if (DataRowNode == null)
                    DataRowNode = XML.XMLaddNode(putDataRowsNode, Constants.sSrchDataRow, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
                return DataRowNode;
            }
        }

        public XmlNode DataRowNode
        {
            get
            {
                return m_xmlDataRow;
            }
            set
            {
                m_xmlDataRow = value;
            }
        }

        public XmlNode addDataRow()
        {
            DataRowNode = null;
            DataRowCount = DataRowCount + 1;
            return putDataRowNode;
        }

        public XmlNode putDataRow(string sRowID, string sKey)
        {
            if (sRowID != "" && sRowID != null)
                DataRowID = sRowID;
            if (sKey != "" && sKey != null)
                RowKey = sKey;
            return DataRowNode;
        }

        public string DataRowID
        {
            get
            {
                return Utils.getAttribute(DataRowNode, "", Constants.sSrchDRID); // SI06501
            }
            set
            {
                Utils.putAttribute(putDataRowNode, "", Constants.sSrchDRID, value, sDataRowNodeOrder); // SI06501
            }
        }
        // Start SI06453
        public string FilterCodeId
        {
            get
            {
                return Utils.getAttribute(FieldNode, Constants.sSrchFldFilterVal1, Constants.sSrchFldFltCodeId);
            }
            set
            {
                Utils.putAttribute(putFieldNode, Constants.sSrchFldFilterVal1, Constants.sSrchFldFltCodeId, value.Trim(), NodeOrder);
            }
        }
        // End SI06453
        public string RowKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchRowKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchRowKey, value, NodeOrder);
            }
        }
        public XmlNode ColumnNode
        {
            get
            {
                return m_xmlColumn;
            }
            set
            {
                m_xmlColumn = value;
            }
        }

        public XmlNode putColumnNode
        {
            get
            {
                if (ColumnNode == null)
                    ColumnNode = XML.XMLaddNode(putDataRowNode, Constants.sSrchRowCol, NodeOrder);
                return ColumnNode;
            }
        }

        public XmlNodeList ColumnList
        {
            get
            {
                return getNodeList(Constants.sSrchRowCol, ref m_xmlColumnList, DataRowNode);
            }
        }

        public int ColumnCount
        {
            get
            {
                return ColumnList.Count;
            }
        }

        public XmlNode getColumn(bool rst)
        {
            object o = ColumnList;
            return getNode(rst, ref m_xmlColumnList, ref m_xmlColumn);
        }

        public XmlNode getColumnByID(string sid)
        {
            bool rst;
            if (ColumnFieldIDRef != sid) // SI06501
            {
                rst = true;
                while (getColumn(rst) != null)
                {
                    rst = false;
                    if (ColumnFieldIDRef == sid) // SI06501
                        break;
                }
            }
            return ColumnNode;
        }

        // SI06501  public string ColumnID
        public string ColumnFieldIDRef
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = extFieldID(XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColID)); // SI06501
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowCol, Constants.sSrchFldIDPfx + value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        // Start SI06501
        public string ColumnCode
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColShortCode);
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowColShortCode, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string ColumnCodeID
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColCodeID);
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowColCodeID, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string ColumnType
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sdtType); // SI06501
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sdtType, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }
        

        public string ColumnValue
        {
            get
            {
                return Utils.getData(ColumnNode, ""); 
            }
            set
            {
                Utils.putData(putColumnNode, "", value, NodeOrder);
            }
        }

        public XmlNode putColumn(string sid, string sColumnValue, string sColumnType, string sColumnCode, string sColumnCodeID)
        {
            if (sid != "" && sid != null)
                ColumnFieldIDRef = sid;
            if (sColumnValue != "" && sColumnValue != null)
                ColumnValue = sColumnValue;
            if (!string.IsNullOrEmpty(sColumnType))
                ColumnType = sColumnType;
            if (!string.IsNullOrEmpty(sColumnCode))
                ColumnCode = sColumnCode;
            if (!string.IsNullOrEmpty(sColumnCodeID))
                ColumnCodeID = sColumnCodeID;
            return ColumnNode;
        }        
        
        public XmlNode addColumn(string sid, string sColumnValue, string sColumnType, string sColumnCode, string sColumnCodeID)
        {
            ColumnNode = null;
            object o = putColumnNode;
            return putColumn(sid, sColumnValue, sColumnType, sColumnCode, sColumnCodeID);
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sMrgIDPfx;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public void AddMerge()
        {
            XmlNode xmlNewNode = Node;
            if (FindMergebyID(MergeID, null) == null)
                XML.XMLInsertNodeInPlace(Parent_Node, xmlNewNode, Parent_NodeOrder);
            else
                Parent_Node.ReplaceChild(xmlNewNode, Node);
            Node = xmlNewNode;
        }
        
        public XmlNode getFirstMerge()
        {
            return getMerge(null, true);
        }

        public XmlNode getMerge(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return getNode(reset);
        }

        public XmlNode FindMergebyID(string sID, XmlDocument xdoc)
        {
            bool rst;
            if (MergeID != sID)
            {
                rst = true;
                while (getMerge(xdoc, rst) != null)
                {
                    rst = false;
                    if (MergeID == sID)
                        break;
                }
            }
            return Node;
        }

        private void subClearPointers()
        {
            m_xmlField = null;
            m_xmlFields = null;
            m_xmlFieldList = null;
            m_xmlDataRows = null;
            m_xmlDataRow = null;
            m_xmlDataRowList = null;
            m_xmlColumn = null;
            m_xmlColumnList = null;
            m_xmlForm = null;
        }
    }
}