/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/22/2007 |         |    Lau     | Created
 * 05/09/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/13/2008 | SI06367 |    NAB     | Build Search using AC .Net objects
 * 08/27/2008 | SI06453 |    AS      | added CODE_ID attribute to FILTER_VALUE_1
 * 12/10/2008 | SI06501 |    NDB     | added CODE_ID attribute to FILTER_VALUE_1
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 10/07/2009 | SIW177  |    JTC     | Updates for Header processing
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 12/23/2011 | SIW7844 |  AV/BS -   |Fix to modify search headers in ACWEB for financials
 * 02/06/2012 | SIW8084 |    ND      | Resolved Field type issue.
 * 03/28/2012 | SIW8307 |   ubora    | PreviousSortKey Corrected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLSearch : XMLXSNodeWithList    //SIW529
    {
        /******************************************************************************************
         * The XMLSearch class provides the functionality to support and manage a Search Node
         ******************************************************************************************
        '  <SEARCH ID="SRC1">
        '    <NAME>Friendly name stored on SEARCH_VIEW.VIEW_NAME</NAME>
        '    <TABLE>name of the primary table in the search</TABLE>
        '    <SOUNDEX INDICATOR=YesNo />
        '    <MAX_NUMBER_OF_ROWS>Maximum Number of rows to return</MAX_NUMBER_OF_ROWS>
        '    <SEARCH_DIRECTION>Use FORWARD to use NEXT SORT_KEY or REVERSE to use PREVIOUS SORT_KEY) to search</SEARCH_DIRECTION>
        '    <FIELDS COUNT=nn NEXTID=nn>
        ' Begin SI06501
        '        <FIELD ID=FLD1 TYPE="[BOOLEAN,DATE,CODE,STRING,NUMBER]">
        ' End SI06501
        '           <FILTER_NAME>Field name in SEARCH_DICTIONARY</FILTER_NAME>
        '           <OPERATOR>String representing the range (GT, LT, GE, LE, BET, EQ, ...)</OPERATOR>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <SORT>Value of sorting preferences 1,2 or 3</SORT>
        '           <DISPLAY_NAME>Resulting FIELD_DESC value in SEARCH_DICTIOANRY table</DISPLAY_NAME>
        '           <DISPLAY_MASK>Resulting OPT_MASK value in SEARCH_DICTIOANRY table</DISPLAY_NAME>  // SI06501
        '        </FIELD>
        '    </FIELDS>
        '    <DATA_ROWS COUNT = nn NEXTID= nn>
        '        <ROW ID=recordkey>  
        ' Begin SI06501
        '            <COL IDREF=FLD1 TYPE="[BOOLEAN,DATE,CODE,STRING,NUMBER]">resulting value</COL>
        '            <COL IDREF=FLD2 TYPE="CODE" CODE_ID="nn" CODE="xx">resulting value</COL>
        '            <COL IDREF=FLD3 TYPE="DATE" YEAR="nnnn" MONTH="nn" DAY="nn">resulting value</COL>
        '            <COL IDREF=FLD3 TYPE="BOOLEAN" INDICATOR="Yes"/>        
        ' End SI06501
        '        </ROW>
        '    </DATA_ROWS>
        '  </SEARCH>
        '******************************************************************************************/

        private XmlNode m_xmlField;
        private XmlNode m_xmlFields;
        private XmlNodeList m_xmlFieldList;
        private XmlNode m_xmlDataRow;
        private XmlNode m_xmlDataRows;
        private XmlNodeList m_xmlDataRowList;
        private XmlNode m_xmlColumn;
        private XmlNodeList m_xmlColumnList;
        private ArrayList sFieldNodeOrder;
        private ArrayList sDataRowNodeOrder;

        public XMLSearch()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;

            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sSrchName);
            sThisNodeOrder.Add(Constants.sSrchTable);
            sThisNodeOrder.Add(Constants.sSrchSoundex);
            sThisNodeOrder.Add(Constants.sSrchMaxRows);
            sThisNodeOrder.Add(Constants.sSrchNextSortKey);                 //SI06333
            sThisNodeOrder.Add(Constants.sSrchPreviousSortKey);             //SI06333
            sThisNodeOrder.Add(Constants.sSrchNextTechKey);                 //SI06298 - Implemented in SI06333
            sThisNodeOrder.Add(Constants.sSrchPreviousTechKey);             //SI06298 - Implemented in SI06333
            sThisNodeOrder.Add(Constants.sSrchSearchDirection);             //SI06333
            sThisNodeOrder.Add(Constants.sSrchFields);
            sThisNodeOrder.Add(Constants.sSrchDataRows);
            sThisNodeOrder.Add(Constants.sSrchHeader);   //SIW7844

            sFieldNodeOrder = new ArrayList();
            sFieldNodeOrder.Add(Constants.sSrchFldFilterName);
            sFieldNodeOrder.Add(Constants.sSrchFldOper);
            sFieldNodeOrder.Add(Constants.sSrchFldFilterVal1);
            sFieldNodeOrder.Add(Constants.sSrchFldFilterVal2);
            sFieldNodeOrder.Add(Constants.sSrchFldSort);
            sFieldNodeOrder.Add(Constants.sSrchFldDispName);


            sDataRowNodeOrder = new ArrayList();
            sDataRowNodeOrder.Add(Constants.sSrchRowKey);
            sDataRowNodeOrder.Add(Constants.sSrchRowCol);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sSrchNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "");
        }

        public XmlNode Create(string sID, string sName, string sTable, string sSoundex, string sMaxRows, string sStartSearchId)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                SearchID = sID;
                SearchName = sName;
                Table = sTable;
                Soundex = sSoundex;
                MaxNumberOfRows = sMaxRows;
                //StartWithSortKey = sStartSearchId;        //SI06333

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSearch.Create");
                return null;
            }
        }

        public string SearchID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sSrchID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sSrchIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lSearchId = Globals.lSearchId + 1;
                    lid = Globals.lSearchId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sSrchID, Constants.sSrchIDPfx + lid);
            }
        }
        public string SearchName
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchName);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchName, value, NodeOrder);
            }
        }
        //Start SIW7844
        public string SearchHeader
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchHeader);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchHeader, value, NodeOrder);
            }
        }
        //End SIW7844
        public string Table
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchTable);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchTable, value, NodeOrder);
            }
        }
        public string Soundex
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchSoundex);   //SI06367
                return Utils.getBool(Node, Constants.sSrchSoundex, "");
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchSoundex, value, NodeOrder);   SI06367
                Utils.putBool(putNode, Constants.sSrchSoundex, "", value, NodeOrder);
            }
        }

        public string MaxNumberOfRows
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchMaxRows);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchMaxRows, value, NodeOrder);
            }
        }

        //Start SI06333
        public string NextSortKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchNextSortKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchNextSortKey, value, NodeOrder);
            }
        }

        public string PreviousSortKey
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchPreviousTechKey); //SIW8307
                return Utils.getData(Node, Constants.sSrchPreviousSortKey); //SIW8307
            }
            set
            {
                if (value == "")
                    value = "0";
                //Utils.putData(putNode, Constants.sSrchPreviousTechKey, value, NodeOrder); //SIW8307
                Utils.putData(putNode, Constants.sSrchPreviousSortKey, value, NodeOrder); //SIW8307
            }
        }

        //Start (SI06298 - Implemented in SI06333)
        public string NextTechKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchNextTechKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchNextTechKey, value, NodeOrder);
            }
        }

        public string PreviousTechKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchPreviousTechKey);
            }
            set
            {
                if (value == "")
                    value = "0";
                Utils.putData(putNode, Constants.sSrchPreviousTechKey, value, NodeOrder);
            }
        }
        //End (SI6298 - Implemented in SI06333)

        public string SearchDirection
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchSearchDirection);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchSearchDirection, value, NodeOrder);
            }
        }

        //public string StartWithSortKey
        //{
        //    get
        //    {
        //        return Utils.getData(Node, Constants.sSrchStartWithSortKey);
        //    }
        //    set
        //    {
        //        Utils.putData(putNode, Constants.sSrchStartWithSortKey, value, NodeOrder);
        //    }
        //}
        //End SI06333

        public XmlNodeList FieldList
        {
            get
            {
                return getNodeList(Constants.sSrchField, ref m_xmlFieldList, FieldsNode); // SI06501

            }
        }

        public XmlNode FieldsNode
        {
            get
            {
                if (m_xmlFields == null)
                    m_xmlFields = XML.XMLGetNode(Node, Constants.sSrchFields);
                return m_xmlFields;
            }
            set
            {
                m_xmlFields = value;
            }
        }

        public XmlNode putFieldsNode
        {
            get
            {
                if (FieldsNode == null)
                {
                    FieldsNode = XML.XMLaddNode(putNode, Constants.sSrchFields, NodeOrder);
                    NextFieldID = "1";
                    FieldCount = 0;
                }
                return FieldsNode;
            }
        }

        public int FieldCount
        {
            get
            {
                string strdata = Utils.getAttribute(FieldsNode, "", Constants.sSrchFldCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putFieldsNode, "", Constants.sSrchFldCount, value + "", sFieldNodeOrder);
            }
        }

        public string NextFieldID
        {
            get
            {
                return Utils.getAttribute(FieldsNode, "", Constants.sSrchFldNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putFieldsNode, "", Constants.sSrchFldNextID, value, sFieldNodeOrder);
            }
        }

        public string getNextFieldID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextFieldID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextFieldID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }
        public XmlNode getField(bool reset)
        {
            object o = FieldList;
            return getNode(reset, ref m_xmlFieldList, ref m_xmlField);
        }
        public XmlNode getFieldbyID(string sID)
        {
            if (FieldID != sID)
            {
                getField(true);
                while (FieldNode != null)
                {
                    if (FieldID == sID)
                        break;
                    getField(false);
                }
            }
            return FieldNode;
        }
        public XmlNode putFieldNode
        {
            get
            {
                if (FieldNode == null)
                    FieldNode = XML.XMLaddNode(putFieldsNode, Constants.sSrchField, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
                return FieldNode;
            }
        }
        public XmlNode FieldNode
        {
            get
            {
                return m_xmlField;
            }
            set
            {
                m_xmlField = value;
            }
        }
        public XmlNode addField()
        {
            FieldNode = null;
            FieldCount = FieldCount + 1;
            return putFieldNode;
        }
        public XmlNode putField(string sFieldID, string sFilterName, string sOperator,
                                   string sValue1, string sValue2, string sSort, string sDispName)
        {
            return putField(sFieldID, sFilterName, sOperator, sValue1, sValue2, sSort, sDispName, string.Empty);
        }
        public XmlNode putField(string sFieldID, string sFilterName, string sOperator,
                                   string sValue1, string sValue2, string sSort, string sDispName, string Type)//SIW7606-PR17(increased trpe parameter)
        {
            if (sFieldID != "" && sFieldID != null)
                FieldID = sFieldID;
            if (sFilterName != "" && sFilterName != null)
                FilterName = sFilterName;
            if (sOperator != "" && sOperator != null)
                Operator = sOperator;
            if (sValue1 != "" && sValue1 != null)
                Value1 = sValue1;
            if (sValue2 != "" && sValue2 != null)
                Value2 = sValue2;
            if (sSort != "" && sSort != null)
                Sort = sSort;
            if (sDispName != "" && sDispName != null)
                DisplayName = sDispName;
            if (!string.IsNullOrEmpty(Type))//SIW7606-PR17
                FieldType = Type;
            return FieldNode;
        }
        public string FldIDPrefix
        {
            get
            {
                return Constants.sSrchFldIDPfx;
            }
        }
        public string extFieldID(string strdata)
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sSrchFldIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }
        public string FieldID
        {
            get
            {
                string strdata;
                string lid = "";
                if (FieldNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(FieldNode, Constants.sSrchFldID);
                    lid = extFieldID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = getNextFieldID(null);
                XML.XMLSetAttributeValue(putFieldNode, Constants.sSrchFldID, Constants.sSrchFldIDPfx + value);
            }
        }
        // Start SI06501
        public string FieldType
        {
            get
            {
                string lid = "";
                //if (ColumnNode != null)//SIW8084
                if (FieldNode != null)  //SIW8084
                    lid = XML.XMLGetAttributeValue(FieldNode, Constants.sdtType); // SI06501
                return lid;
            }
            set
            {
                Utils.putAttribute(putFieldNode, "", Constants.sdtType, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }
        // End SI06501

        public string FilterName
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchFldFilterName);   //SI06367
                return Utils.getData(FieldNode, Constants.sSrchFldFilterName);
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchFldFilterName, value, NodeOrder);   //SI06367
                Utils.putData(putFieldNode, Constants.sSrchFldFilterName, value, NodeOrder);
            }
        }
        public string Operator
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchFldOper);   //SI06367
                return Utils.getData(FieldNode, Constants.sSrchFldOper);
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchFldOper, value, NodeOrder);   //SI06367
                Utils.putData(putFieldNode, Constants.sSrchFldOper, value, NodeOrder);
            }
        }
        public string Value1
        {
            get
            {
                return Utils.getData(FieldNode, Constants.sSrchFldFilterVal1);
            }
            set
            {
                Utils.putData(putFieldNode, Constants.sSrchFldFilterVal1, value, sFieldNodeOrder);
            }
        }
        public string Value2
        {
            get
            {
                return Utils.getData(FieldNode, Constants.sSrchFldFilterVal2);
            }
            set
            {
                Utils.putData(putFieldNode, Constants.sSrchFldFilterVal2, value, sFieldNodeOrder);
            }
        }
        public string Sort
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchFldSort);   //SI06367
                return Utils.getData(FieldNode, Constants.sSrchFldSort);
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchFldSort, value, NodeOrder);   //SI06367
                Utils.putData(putFieldNode, Constants.sSrchFldSort, value, NodeOrder);
            }
        }
        public string DisplayName
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchFldDispName);   //SI06367
                return Utils.getData(FieldNode, Constants.sSrchFldDispName);
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchFldDispName, value, NodeOrder);   //SI06367
                Utils.putData(putFieldNode, Constants.sSrchFldDispName, value, NodeOrder);
            }
        }
        // Start SI06501
        public string DisplayMask
        {
            get
            {
                //return Utils.getData(Node, Constants.sSrchFldDispName);   //SI06367
                return Utils.getData(FieldNode, Constants.sSrchFldDispMask);
            }
            set
            {
                //Utils.putData(putNode, Constants.sSrchFldDispName, value, NodeOrder);   //SI06367
                Utils.putData(putFieldNode, Constants.sSrchFldDispMask, value, NodeOrder);
            }
        }
        // End SI06501

        public XmlNodeList DataRowList
        {
            get
            {
                return getNodeList(Constants.sSrchDataRow, ref m_xmlDataRowList, DataRowsNode); // SI06501
            }
        }

        public XmlNode DataRowsNode
        {
            get
            {
                // Start SI06501
                if (m_xmlDataRows == null)
                    m_xmlDataRows = XML.XMLGetNode(Node, Constants.sSrchDataRows);
                // End SI06501
                return m_xmlDataRows;
            }
            set
            {
                m_xmlDataRows = value;
            }
        }

        public XmlNode putDataRowsNode
        {
            get
            {
                if (DataRowsNode == null)
                {
                    DataRowsNode = XML.XMLaddNode(putNode, Constants.sSrchDataRows, NodeOrder);
                    NextDataRowID = "1";
                    DataRowCount = 0;
                }
                return DataRowsNode;
            }
        }

        public int DataRowCount
        {
            get
            {
                string strdata = Utils.getAttribute(DataRowsNode, "", Constants.sSrchDRCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putDataRowsNode, "", Constants.sSrchDRCount, value + "", sDataRowNodeOrder);
            }
        }

        public string NextDataRowID
        {
            get
            {
                return Utils.getAttribute(DataRowsNode, "", Constants.sSrchDRNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putDataRowsNode, "", Constants.sSrchDRNextID, value, sDataRowNodeOrder);
            }
        }

        public String getNextDataRowID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextDataRowID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextDataRowID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public XmlNode getDataRow(bool reset)
        {
            object o = DataRowList;
            return getNode(reset, ref m_xmlDataRowList, ref m_xmlDataRow);
        }

        public XmlNode getDataRowbyID(string sID)
        {
            if (DataRowID != sID)
            {
                getDataRow(true);
                while (DataRowNode != null)
                {
                    if (DataRowID == sID)
                        break;
                    getDataRow(false);
                }
            }
            return DataRowNode;
        }

        public XmlNode putDataRowNode
        {
            get
            {
                if (DataRowNode == null)
                    DataRowNode = XML.XMLaddNode(putDataRowsNode, Constants.sSrchDataRow, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
                return DataRowNode;
            }
        }

        public XmlNode DataRowNode
        {
            get
            {
                return m_xmlDataRow;
            }
            set
            {
                m_xmlDataRow = value;
            }
        }

        public XmlNode addDataRow()
        {
            DataRowNode = null;
            DataRowCount = DataRowCount + 1;
            return putDataRowNode;
        }

        public XmlNode putDataRow(string sRowID, string sKey)
        {
            if (sRowID != "" && sRowID != null)
                DataRowID = sRowID;
            if (sKey != "" && sKey != null)
                RowKey = sKey;
            return DataRowNode;
        }

        public string DataRowID
        {
            get
            {
                return Utils.getAttribute(DataRowNode, "", Constants.sSrchDRID); // SI06501
            }
            set
            {
                Utils.putAttribute(putDataRowNode, "", Constants.sSrchDRID, value, sDataRowNodeOrder); // SI06501
            }
        }
        // Start SI06453
        public string FilterCodeId
        {
            get
            {
                return Utils.getAttribute(FieldNode, Constants.sSrchFldFilterVal1, Constants.sSrchFldFltCodeId);
            }
            set
            {
                Utils.putAttribute(putFieldNode, Constants.sSrchFldFilterVal1, Constants.sSrchFldFltCodeId, value.Trim(), NodeOrder);
            }
        }
        // End SI06453
        public string RowKey
        {
            get
            {
                return Utils.getData(Node, Constants.sSrchRowKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sSrchRowKey, value, NodeOrder);
            }
        }
        public XmlNode ColumnNode
        {
            get
            {
                return m_xmlColumn;
            }
            set
            {
                m_xmlColumn = value;
            }
        }

        public XmlNode putColumnNode
        {
            get
            {
                if (ColumnNode == null)
                    ColumnNode = XML.XMLaddNode(putDataRowNode, Constants.sSrchRowCol, NodeOrder);
                return ColumnNode;
            }
        }

        public XmlNodeList ColumnList
        {
            get
            {
                return getNodeList(Constants.sSrchRowCol, ref m_xmlColumnList, DataRowNode);
            }
        }

        public int ColumnCount
        {
            get
            {
                return ColumnList.Count;
            }
        }

        public XmlNode getColumn(bool rst)
        {
            object o = ColumnList;
            return getNode(rst, ref m_xmlColumnList, ref m_xmlColumn);
        }

        public XmlNode getColumnByID(string sid)
        {
            bool rst;
            if (ColumnFieldIDRef != sid) // SI06501
            {
                rst = true;
                while (getColumn(rst) != null)
                {
                    rst = false;
                    if (ColumnFieldIDRef == sid) // SI06501
                        break;
                }
            }
            return ColumnNode;
        }

        // SI06501  public string ColumnID
        public string ColumnFieldIDRef
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = extFieldID(XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColID)); // SI06501
                return lid;
            }
            set
            {
                //Utils.putAttribute(putColumnNode, "", Constants.sSrchRowCol, Constants.sSrchFldIDPfx + value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils 
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowColID, Constants.sSrchFldIDPfx + value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils// SIW7606 - changed sSrchRowCol to change COL to IDREF for quick query result
            }
        }

        // Start SI06501
        public string ColumnCode
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColShortCode);
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowColShortCode, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string ColumnCodeID
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sSrchRowColCodeID);
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sSrchRowColCodeID, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string ColumnType
        {
            get
            {
                string lid = "";
                if (ColumnNode != null)
                    lid = XML.XMLGetAttributeValue(ColumnNode, Constants.sdtType); // SI06501
                return lid;
            }
            set
            {
                Utils.putAttribute(putColumnNode, "", Constants.sdtType, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }
        // End SI06501

        public string ColumnValue
        {
            get
            {
                return Utils.getData(ColumnNode, ""); // SI06501
            }
            set
            {
                Utils.putData(putColumnNode, "", value, NodeOrder); // SI06501
            }
        }

        // Start SI06501 
        // public XmlNode putColumn(string sid, string sColumnValue)
        public XmlNode putColumn(string sid, string sColumnValue, string sColumnType, string sColumnCode, string sColumnCodeID)
        {
            if (sid != "" && sid != null)
                ColumnFieldIDRef = sid;
            if (sColumnValue != "" && sColumnValue != null)
                ColumnValue = sColumnValue;
            if (!string.IsNullOrEmpty(sColumnType))
                ColumnType = sColumnType;
            if (!string.IsNullOrEmpty(sColumnCode))
                ColumnCode = sColumnCode;
            if (!string.IsNullOrEmpty(sColumnCodeID))
                ColumnCodeID = sColumnCodeID;
            return ColumnNode;
        }
        // End SI06501

        // SI06501 public XmlNode addColumn(string sid, string sColumnValue)
        public XmlNode addColumn(string sid, string sColumnValue, string sColumnType, string sColumnCode, string sColumnCodeID)
        {
            ColumnNode = null;
            object o = putColumnNode;
            return putColumn(sid, sColumnValue, sColumnType, sColumnCode, sColumnCodeID);  // SI06501
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sSrchIDPfx;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public void AddSearch()
        {
            XmlNode xmlNewNode = Node;
            if (FindSearchbyID(SearchID, null) == null)
                XML.XMLInsertNodeInPlace(Parent_Node, xmlNewNode, Parent_NodeOrder);
            else
                Parent_Node.ReplaceChild(xmlNewNode, Node);
            Node = xmlNewNode;
        }

        //Start SIW177
        public XmlNode getFirstSearch()
        {
            return getSearch(null, true);
        }
        //End SIW177

        public XmlNode getSearch(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return getNode(reset);
        }

        public XmlNode FindSearchbyID(string sID, XmlDocument xdoc)
        {
            bool rst;
            if (SearchID != sID)
            {
                rst = true;
                while (getSearch(xdoc, rst) != null)
                {
                    rst = false;
                    if (SearchID == sID)
                        break;
                }
            }
            return Node;
        }

        private void subClearPointers()
        {
            m_xmlField = null;
            m_xmlFields = null;
            m_xmlFieldList = null;
            m_xmlDataRows = null;
            m_xmlDataRow = null;
            m_xmlDataRowList = null;
            m_xmlColumn = null;
            m_xmlColumnList = null;
        }
    }
}