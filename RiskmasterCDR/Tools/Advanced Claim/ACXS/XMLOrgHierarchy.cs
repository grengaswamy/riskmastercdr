/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
//using CCP.XmlComponents;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLOrgHierarchy : XMLXSNodeWithList    //SIW529
    {
        /*<ORG_HIERARCHY>
        '   <CLIENT ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '      <COMPANY ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '         <OPERATION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '            <REGION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '               <DIVISION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                  <LOCATION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                     <FACILITY ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                        <DEPARTMENT ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO"/>
        '                     </FACILITY>
        '                  </LOCATION>
        '               </DIVISION>
        '            </REGION>
        '         </OPERATION>
        '      </COMPANY>
        '   </CLIENT>
        '</ORG_HIERARCHY>*/

        private XmlNode xmlClient;
        private XmlNodeList xmlClientList;
        private XmlNode xmlCompany;
        private XmlNodeList xmlCompanyList;
        private XmlNode xmlOperation;
        private XmlNodeList xmlOperationList;
        private XmlNode xmlRegion;
        private XmlNodeList xmlRegionList;
        private XmlNode xmlDivision;
        private XmlNodeList xmlDivisionList;
        private XmlNode xmlLocation;
        private XmlNodeList xmlLocationList;
        private XmlNode xmlFacility;
        private XmlNodeList xmlFacilityList;
        private XmlNode xmlDepartment;
        private XmlNodeList xmlDepartmentList;

        public XMLOrgHierarchy()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            subClearPointers();
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                    m_Parent = XML;
                subClearPointers();
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            xmlClient = null;
            xmlClientList = null;
            xmlCompany = null;
            xmlCompanyList = null;
            xmlOperation = null;
            xmlOperationList = null;
            xmlRegion = null;
            xmlRegionList = null;
            xmlDivision = null;
            xmlDivisionList = null;
            xmlLocation = null;
            xmlLocationList = null;
            xmlFacility = null;
            xmlFacilityList = null;
            xmlDepartment = null;
            xmlDepartmentList = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                base.Document = value;
                subClearPointers();
            }
        }
        protected override string DefaultNodeName
        {
            get { return Constants.sOrgHierNode; }
        }

        public XmlNode addHierarchy()
        {
            Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            return Node;
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    addHierarchy();
                return Node;
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = Utils.getNode(Parent_Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return Utils.sNodeOrder;	//SIW485 changed Globals to Utils
            }
        }

        public XmlNode ClientNode
        {
            get
            {
                return xmlClient;
            }
            set
            {
                xmlClient = value;

                xmlCompany = null;
                xmlCompanyList = null;
                xmlOperation = null;
                xmlOperationList = null;
                xmlRegion = null;
                xmlRegionList = null;
                xmlDivision = null;
                xmlDivisionList = null;
                xmlLocation = null;
                xmlLocationList = null;
                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putClientNode
        {
            get
            {
                if (ClientNode == null)
                    ClientNode = XML.XMLaddNode(putNode, Constants.sOrgHierClient, NodeOrder);
                return ClientNode;
            }
        }

        public XmlNodeList ClientList
        {
            get
            {
                return getNodeList(Constants.sOrgHierClient, ref xmlClientList, Node);
            }
        }

        public int ClientCount
        {
            get
            {
                return ClientList.Count;
            }
        }

        public XmlNode getClient(bool rst)
        {
            Object o = ClientList;
            return getNode(rst, ref xmlClientList, ref xmlClient);
        }

        public XmlNode getClientByID(string sid)
        {
            bool rst;
            if (ClientID != sid)
            {
                rst = true;
                while (getClient(rst) != null)
                {
                    rst = false;
                    if (ClientID == sid)
                        break;
                }
            }
            return ClientNode;
        }

        public XmlNode getClientByKey(string sid)
        {
            bool rst;
            if (ClientKey != sid)
            {
                rst = true;
                while (getClient(rst) != null)
                {
                    rst = false;
                    if (ClientKey == sid)
                        break;
                }
            }
            return ClientNode;
        }

        public string ClientID
        {
            get
            {
                return Utils.getEntityRef(ClientNode, "");
            }
            set
            {
                Utils.putEntityRef(putClientNode, "", value, NodeOrder);
            }
        }

        public string ClientKey
        {
            get
            {
                return Utils.getAttribute(ClientNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putClientNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string ClientDelete
        {
            get
            {
                return Utils.getBool(ClientNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putClientNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putClient(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                ClientID = sid;
            if (sKey != "" && sKey != null)
                ClientKey = sKey;
            return ClientNode;
        }

        public XmlNode addClient(string sid, string sKey)
        {
            ClientNode = null;
            object o = putClientNode;
            return putClient(sid, sKey);
        }

        public XmlNode CompanyNode
        {
            get
            {
                return xmlCompany;
            }
            set
            {
                xmlCompany = value;

                xmlOperation = null;
                xmlOperationList = null;
                xmlRegion = null;
                xmlRegionList = null;
                xmlDivision = null;
                xmlDivisionList = null;
                xmlLocation = null;
                xmlLocationList = null;
                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putCompanyNode
        {
            get
            {
                if (CompanyNode == null)
                    CompanyNode = XML.XMLaddNode(putClientNode, Constants.sOrgHierCompany, NodeOrder);
                return CompanyNode;
            }
        }

        public XmlNodeList CompanyList
        {
            get
            {
                return getNodeList(Constants.sOrgHierCompany, ref xmlCompanyList, ClientNode);
            }
        }

        public int CompanyCount
        {
            get
            {
                return CompanyList.Count;
            }
        }

        public XmlNode getCompany(bool rst)
        {
            object o = CompanyList;
            return getNode(rst, ref xmlCompanyList, ref xmlCompany);
        }

        public XmlNode getCompanyByID(string sid)
        {
            bool rst;
            if (CompanyID != sid)
            {
                rst = true;
                while (getCompany(rst) != null)
                {
                    rst = false;
                    if (CompanyID == sid)
                        break;
                }
            }
            return CompanyNode;
        }

        public XmlNode getCompanyByKey(string sid)
        {
            bool rst;
            if (CompanyKey != sid)
            {
                rst = true;
                while (getCompany(rst) != null)
                {
                    rst = false;
                    if (CompanyKey == sid)
                        break;
                }
            }
            return CompanyNode;
        }

        public string CompanyID
        {
            get
            {
                return Utils.getEntityRef(CompanyNode, "");
            }
            set
            {
                Utils.putEntityRef(putCompanyNode, "", value, NodeOrder);
            }
        }

        public string CompanyKey
        {
            get
            {
                return Utils.getAttribute(CompanyNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putCompanyNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string CompanyDelete
        {
            get
            {
                return Utils.getBool(CompanyNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putCompanyNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putCompany(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                CompanyID = sid;
            if (sKey != "" && sKey != null)
                CompanyKey = sKey;
            return CompanyNode;
        }

        public XmlNode addCompany(string sid, string sKey)
        {
            CompanyNode = null;
            object o = putCompanyNode;
            return putCompany(sid, sKey);
        }

        public XmlNode OperationNode
        {
            get
            {
                return xmlOperation;
            }
            set
            {
                xmlOperation = value;

                xmlRegion = null;
                xmlRegionList = null;
                xmlDivision = null;
                xmlDivisionList = null;
                xmlLocation = null;
                xmlLocationList = null;
                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putOperationNode
        {
            get
            {
                if (OperationNode == null)
                    OperationNode = XML.XMLaddNode(putCompanyNode, Constants.sOrgHierOperation, NodeOrder);
                return OperationNode;
            }
        }

        public XmlNodeList OperationList
        {
            get
            {
                return getNodeList(Constants.sOrgHierOperation, ref xmlOperationList, CompanyNode);
            }
        }

        public int OperationCount
        {
            get
            {
                return OperationList.Count;
            }
        }

        public XmlNode getOperation(bool rst)
        {
            object o = OperationList;
            return getNode(rst, ref xmlOperationList, ref xmlOperation);
        }

        public XmlNode getOperationByID(string sid)
        {
            bool rst;
            if (OperationID != sid)
            {
                rst = true;
                while (getOperation(rst) != null)
                {
                    rst = false;
                    if (OperationID == sid)
                        break;
                }
            }
            return OperationNode;
        }

        public XmlNode getOperationByKey(string sid)
        {
            bool rst;
            if (OperationKey != sid)
            {
                rst = true;
                while (getOperation(rst) != null)
                {
                    rst = false;
                    if (OperationKey == sid)
                        break;
                }
            }
            return OperationNode;
        }

        public string OperationID
        {
            get
            {
                return Utils.getEntityRef(OperationNode, "");
            }
            set
            {
                Utils.putEntityRef(putOperationNode, "", value, NodeOrder);
            }
        }

        public string OperationKey
        {
            get
            {
                return Utils.getAttribute(OperationNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putOperationNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string OperationDelete
        {
            get
            {
                return Utils.getBool(OperationNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putOperationNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putOperation(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                OperationID = sid;
            if (sKey != "" && sKey != null)
                OperationKey = sKey;
            return OperationNode;
        }

        public XmlNode addOperation(string sid, string sKey)
        {
            OperationNode = null;
            object o = putOperationNode;
            return putOperation(sid, sKey);
        }

        public XmlNode RegionNode
        {
            get
            {
                return xmlRegion;
            }
            set
            {
                xmlRegion = value;

                xmlDivision = null;
                xmlDivisionList = null;
                xmlLocation = null;
                xmlLocationList = null;
                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putRegionNode
        {
            get
            {
                if (RegionNode == null)
                    RegionNode = XML.XMLaddNode(putOperationNode, Constants.sOrgHierRegion, NodeOrder);
                return RegionNode;
            }
        }

        public XmlNodeList RegionList
        {
            get
            {
                return getNodeList(Constants.sOrgHierRegion, ref xmlRegionList, OperationNode);
            }
        }

        public int RegionCount
        {
            get
            {
                return RegionList.Count;
            }
        }

        public XmlNode getRegion(bool rst)
        {
            object o = RegionList;
            return getNode(rst, ref xmlRegionList, ref xmlRegion);
        }

        public XmlNode getRegionByID(string sid)
        {
            bool rst;
            if (RegionID != sid)
            {
                rst = true;
                while (getRegion(rst) != null)
                {
                    rst = false;
                    if (RegionID == sid)
                        break;
                }
            }
            return RegionNode;
        }

        public XmlNode getRegionByKey(string sid)
        {
            bool rst;
            if (RegionKey != sid)
            {
                rst = true;
                while (getRegion(rst) != null)
                {
                    rst = false;
                    if (RegionKey == sid)
                        break;
                }
            }
            return RegionNode;
        }

        public string RegionID
        {
            get
            {
                return Utils.getEntityRef(RegionNode, "");
            }
            set
            {
                Utils.putEntityRef(putRegionNode, "", value, NodeOrder);
            }
        }

        public string RegionKey
        {
            get
            {
                return Utils.getAttribute(RegionNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putRegionNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string RegionDelete
        {
            get
            {
                return Utils.getBool(RegionNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putRegionNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putRegion(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                RegionID = sid;
            if (sKey != "" && sKey != null)
                RegionKey = sKey;
            return RegionNode;
        }

        public XmlNode addRegion(string sid, string sKey)
        {
            RegionNode = null;
            object o = putRegionNode;
            return putRegion(sid, sKey);
        }

        public XmlNode DivisionNode
        {
            get
            {
                return xmlDivision;
            }
            set
            {
                xmlDivision = value;

                xmlLocation = null;
                xmlLocationList = null;
                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putDivisionNode
        {
            get
            {
                if (DivisionNode == null)
                    DivisionNode = XML.XMLaddNode(putRegionNode, Constants.sOrgHierDivision, NodeOrder);
                return DivisionNode;
            }
        }

        public XmlNodeList DivisionList
        {
            get
            {
                return getNodeList(Constants.sOrgHierDivision, ref xmlDivisionList, RegionNode);
            }
        }

        public int DivisionCount
        {
            get
            {
                return DivisionList.Count;
            }
        }

        public XmlNode getDivision(bool rst)
        {
            object o = DivisionList;
            return getNode(rst, ref xmlDivisionList, ref xmlDivision);
        }

        public XmlNode getDivisionByID(string sid)
        {
            bool rst;
            if (DivisionID != sid)
            {
                rst = true;
                while (getDivision(rst) != null)
                {
                    rst = false;
                    if (DivisionID == sid)
                        break;
                }
            }
            return DivisionNode;
        }

        public XmlNode getDivisionByKey(string sid)
        {
            bool rst;
            if (DivisionKey != sid)
            {
                rst = true;
                while (getDivision(rst) != null)
                {
                    rst = false;
                    if (DivisionKey == sid)
                        break;
                }
            }
            return DivisionNode;
        }

        public string DivisionID
        {
            get
            {
                return Utils.getEntityRef(DivisionNode, "");
            }
            set
            {
                Utils.putEntityRef(putDivisionNode, "", value, NodeOrder);
            }
        }

        public string DivisionKey
        {
            get
            {
                return Utils.getAttribute(DivisionNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putDivisionNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string DivisionDelete
        {
            get
            {
                return Utils.getBool(DivisionNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putDivisionNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putDivision(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                DivisionID = sid;
            if (sKey != "" && sKey != null)
                DivisionKey = sKey;
            return DivisionNode;
        }

        public XmlNode addDivision(string sid, string sKey)
        {
            DivisionNode = null;
            object o = putDivisionNode;
            return putDivision(sid, sKey);
        }

        public XmlNode LocationNode
        {
            get
            {
                return xmlLocation;
            }
            set
            {
                xmlLocation = value;

                xmlFacility = null;
                xmlFacilityList = null;
                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putLocationNode
        {
            get
            {
                if (LocationNode == null)
                    LocationNode = XML.XMLaddNode(putDivisionNode, Constants.sOrgHierLocation, NodeOrder);
                return LocationNode;
            }
        }

        public XmlNodeList LocationList
        {
            get
            {
                return getNodeList(Constants.sOrgHierLocation, ref xmlLocationList, DivisionNode);
            }
        }

        public int LocationCount
        {
            get
            {
                return LocationList.Count;
            }
        }

        public XmlNode getLocation(bool rst)
        {
            object o = LocationList;
            return getNode(rst, ref xmlLocationList, ref xmlLocation);
        }

        public XmlNode getLocationByID(string sid)
        {
            bool rst;
            if (LocationID != sid)
            {
                rst = true;
                while (getLocation(rst) != null)
                {
                    rst = false;
                    if (LocationID == sid)
                        break;
                }
            }
            return LocationNode;
        }

        public XmlNode getLocationByKey(string sid)
        {
            bool rst;
            if (LocationKey != sid)
            {
                rst = true;
                while (getLocation(rst) != null)
                {
                    rst = false;
                    if (LocationKey == sid)
                        break;
                }
            }
            return LocationNode;
        }

        public string LocationID
        {
            get
            {
                return Utils.getEntityRef(LocationNode, "");
            }
            set
            {
                Utils.putEntityRef(putLocationNode, "", value, NodeOrder);
            }
        }

        public string LocationKey
        {
            get
            {
                return Utils.getAttribute(LocationNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putLocationNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string LocationDelete
        {
            get
            {
                return Utils.getBool(LocationNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putLocationNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putLocation(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                LocationID = sid;
            if (sKey != "" && sKey != null)
                LocationKey = sKey;
            return LocationNode;
        }

        public XmlNode addLocation(string sid, string sKey)
        {
            LocationNode = null;
            object o = putLocationNode;
            return putLocation(sid, sKey);
        }

        public XmlNode FacilityNode
        {
            get
            {
                return xmlFacility;
            }
            set
            {
                xmlFacility = value;

                xmlDepartment = null;
                xmlDepartmentList = null;
            }
        }

        public XmlNode putFacilityNode
        {
            get
            {
                if (FacilityNode == null)
                    FacilityNode = XML.XMLaddNode(putLocationNode, Constants.sOrgHierFacility, NodeOrder);
                return FacilityNode;
            }
        }

        public XmlNodeList FacilityList
        {
            get
            {
                return getNodeList(Constants.sOrgHierFacility, ref xmlFacilityList, LocationNode);
            }
        }

        public int FacilityCount
        {
            get
            {
                return FacilityList.Count;
            }
        }

        public XmlNode getFacility(bool rst)
        {
            object o = FacilityList;
            return getNode(rst, ref xmlFacilityList, ref xmlFacility);
        }

        public XmlNode getFacilityByID(string sid)
        {
            bool rst;
            if (FacilityID != sid)
            {
                rst = true;
                while (getFacility(rst) != null)
                {
                    rst = false;
                    if (FacilityID == sid)
                        break;
                }
            }
            return FacilityNode;
        }

        public XmlNode getFacilityByKey(string sid)
        {
            bool rst;
            if (FacilityKey != sid)
            {
                rst = true;
                while (getFacility(rst) != null)
                {
                    rst = false;
                    if (FacilityKey == sid)
                        break;
                }
            }
            return FacilityNode;
        }

        public string FacilityID
        {
            get
            {
                return Utils.getEntityRef(FacilityNode, "");
            }
            set
            {
                Utils.putEntityRef(putFacilityNode, "", value, NodeOrder);
            }
        }

        public string FacilityKey
        {
            get
            {
                return Utils.getAttribute(FacilityNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putFacilityNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string FacilityDelete
        {
            get
            {
                return Utils.getBool(FacilityNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putFacilityNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putFacility(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                FacilityID = sid;
            if (sKey != "" && sKey != null)
                FacilityKey = sKey;
            return FacilityNode;
        }

        public XmlNode addFacility(string sid, string sKey)
        {
            FacilityNode = null;
            object o = putFacilityNode;
            return putFacility(sid, sKey);
        }

        public XmlNode DepartmentNode
        {
            get
            {
                return xmlDepartment;
            }
            set
            {
                xmlDepartment = value;
            }
        }

        public XmlNode putDepartmentNode
        {
            get
            {
                if (DepartmentNode == null)
                    DepartmentNode = XML.XMLaddNode(putFacilityNode, Constants.sOrgHierDepartment, NodeOrder);
                return DepartmentNode;
            }
        }

        public XmlNodeList DepartmentList
        {
            get
            {
                return getNodeList(Constants.sOrgHierDepartment, ref xmlDepartmentList, FacilityNode);
            }
        }

        public int DepartmentCount
        {
            get
            {
                return DepartmentList.Count;
            }
        }

        public XmlNode getDepartment(bool rst)
        {
            object o = DepartmentList;
            return getNode(rst, ref xmlDepartmentList, ref xmlDepartment);
        }

        public XmlNode getDepartmentByID(string sid)
        {
            bool rst;
            if (DepartmentID != sid)
            {
                rst = true;
                while (getDepartment(rst) != null)
                {
                    rst = false;
                    if (DepartmentID == sid)
                        break;
                }
            }
            return DepartmentNode;
        }

        public XmlNode getDepartmentByKey(string sid)
        {
            bool rst;
            if (DepartmentKey != sid)
            {
                rst = true;
                while (getDepartment(rst) != null)
                {
                    rst = false;
                    if (DepartmentKey == sid)
                        break;
                }
            }
            return DepartmentNode;
        }

        public string DepartmentID
        {
            get
            {
                return Utils.getEntityRef(DepartmentNode, "");
            }
            set
            {
                Utils.putEntityRef(putDepartmentNode, "", value, NodeOrder);
            }
        }

        public string DepartmentKey
        {
            get
            {
                return Utils.getAttribute(DepartmentNode, "", Constants.sOrgHierKey);
            }
            set
            {
                Utils.putAttribute(putDepartmentNode, "", Constants.sOrgHierKey, value, NodeOrder);
            }
        }

        public string DepratmentDelete
        {
            get
            {
                return Utils.getBool(DepartmentNode, "", Constants.sOrgHierDelete);
            }
            set
            {
                Utils.putBool(putDepartmentNode, "", Constants.sOrgHierDelete, value, NodeOrder);
            }
        }

        public XmlNode putDepartment(string sid, string sKey)
        {
            if (sid != "" && sid != null)
                DepartmentID = sid;
            if (sKey != "" && sKey != null)
                DepartmentKey = sKey;
            return DepartmentNode;
        }

        public XmlNode addDepartment(string sid, string sKey)
        {
            DepartmentNode = null;
            object o = putDepartmentNode;
            return putDepartment(sid, sKey);
        }
    }
}
