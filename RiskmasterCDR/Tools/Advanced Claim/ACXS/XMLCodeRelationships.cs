/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/31/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XM
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
//using CCP.XmlComponents;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLCodeRelationships : XMLXSNodeWithList    //SIW529
    {
        /*'<CODE_RELATIONSHIPS RELATIONSHIP_COUNT="1">
        '  <RELATIONSHIP ID="CXR1623" RELATION_SHORT_CODE="LOBTOCT" RELATION_COUNT=1 DESC_ONLY="No" REVERSABLE="Yes" SHOW_REVERSE="No" ONE_ONLY="No" REV_ONE_ONLY="No" AUTO_REV_REL="No" EXCL_MATCH_VAL="No">
        '    <CODE_TABLE_NAME>Table_Name</CODE_TABLE_NAME>
        '    <RELATE_TABLE_NAME>Table_Name</RELATE_TABLE_NAME>
        '    <RELATION_DESC>Line of Business to Claim Type</RELATION_DESC>
        '    <RELATION_REV_DESC>Claim Type to Line of Business</RELATION_REV_DESC>
        '    <CODE_CAPTION>Line Of Business</CODE_CAPTION>
        '    <RELATED_CAPTION>Claim Types</RELATED_CAPTION>
        '    <RELATION_ENTRY ID="CXC5677" SHORT_CODE="APV" REL_SHORT_CODE="AA" DELETED="No"/>
        '     ...
        '  </RELATIONSHIP>
        '</CODE_RELATIONSHIPS>*/

        private int lRelationshipID;
        private XmlNode xmlRelationship;
        private XmlNodeList xmlRelationshipList;
        private int lRelationID;
        private XmlNode xmlRelation;
        private XmlNodeList xmlRelationList;

        public XMLCodeRelationships()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            subClearPointers();
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = XML;
                    subClearPointers();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            lRelationshipID = 0;
            lRelationID = 0;
            xmlThisNode = null;
            xmlRelation = null;
            xmlRelationList = null;
            xmlRelationship = null;
            xmlRelationshipList = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sCRNode; }
        }

        public XmlNode addCodeRelationships()
        {
            Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            return Node;
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    addCodeRelationships();
                return Node;
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = Utils.getNode(Parent_Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return Utils.sNodeOrder;	//SIW485 change Globals to Utils
            }
        }

        public int RelationshipsCount
        {
            get
            {
                return Int32.Parse(Utils.getAttribute(Node, "", Constants.sCRRelationshipCount));
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sCRRelationshipCount, value + "", NodeOrder);
            }
        }

        public XmlNode RelationshipNode
        {
            get
            {
                return xmlRelationship;
            }
            set
            {
                xmlRelationship = value;
                xmlRelation = null;
                xmlRelationList = null;
            }
        }

        public XmlNode putRelationshipNode
        {
            get
            {
                if (RelationshipNode == null)
                    RelationshipNode = XML.XMLaddNode(putNode, Constants.sCRRelationship, NodeOrder);
                return RelationshipNode;
            }
        }

        public XmlNodeList RelationshipList
        {
            get
            {
                return getNodeList(Constants.sCRRelationship, ref xmlRelationshipList, Node);
            }
        }

        public int RelationshipCount
        {
            get
            {
                return RelationshipList.Count;
            }
        }

        public XmlNode getRelationship(bool reset)
        {
            return getNode(reset, ref xmlRelationshipList, ref xmlRelationship);
        }

        public XmlNode getRelationshipByID(string sID)
        {
            bool rst;
            if (RelationshipID != sID)
            {
                rst = true;
                while (getRelationship(rst) != null)
                {
                    rst = false;
                    if (RelationshipID == sID)
                        break;
                }
            }
            return RelationshipNode;
        }

        public string RelationshipID
        {
            get
            {
                string strdata = Utils.getAttribute(RelationshipNode, "", Constants.sCRRelationshipID);
                string sID = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sCRRelationshipIDPfx && strdata.Length >= 4)
                        sID = StringUtils.Right(strdata, strdata.Length - 3);
                return sID;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    lRelationshipID = lRelationshipID + 1;
                    lid = lRelationshipID + "";
                }
                Utils.putAttribute(putRelationshipNode, "", Constants.sCRRelationshipID, Constants.sCRRelationshipIDPfx + lid, NodeOrder);
            }
        }

        public int RelationsCount
        {
            get
            {
                return Int32.Parse(Utils.getAttribute(RelationshipNode, "", Constants.sCRRelationshipCount));
            }
            set
            {
                Utils.putAttribute(putRelationshipNode, "", Constants.sCRRelationshipCount, value + "", NodeOrder);
            }
        }

        public string RelationshipShortCode
        {
            get
            {
                return Utils.getAttribute(RelationshipNode, "", Constants.sCRRelationshipShort);
            }
            set
            {
                if (RelationshipNode == null)
                    RelationshipID = "";
                Utils.putAttribute(putRelationshipNode, "", Constants.sCRRelationshipShort, value, NodeOrder);
            }
        }

        public string RelationshipDescOnly
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRDescOnly);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRDescOnly, value, NodeOrder);
            }
        }

        public string RelationshipReversable
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRReversable);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRReversable, value, NodeOrder);
            }
        }

        public string RelationshipShowReverse
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRShowReverse);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRShowReverse, value, NodeOrder);
            }
        }

        public string RelationshipOneOnly
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCROneOnly);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCROneOnly, value, NodeOrder);
            }
        }

        public string RelationshipRevOneOnly
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRRevOneOnly);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRRevOneOnly, value, NodeOrder);
            }
        }

        public string RelationshipAutoReverse
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRAutoReverse);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRAutoReverse, value, NodeOrder);
            }
        }

        public string RelationshipExcludeMatch
        {
            get
            {
                return Utils.getBool(RelationshipNode, "", Constants.sCRExcludeMatch);
            }
            set
            {
                Utils.putBool(putRelationshipNode, "", Constants.sCRExcludeMatch, value, NodeOrder);
            }
        }

        public string CodeTableName
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRCodeTableName);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRCodeTableName, value, NodeOrder);
            }
        }

        public string RelateTableName
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRRelateTableName);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRRelateTableName, value, NodeOrder);
            }
        }

        public string RelationDesc
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRRelationDesc);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRRelationDesc, value, NodeOrder);
            }
        }

        public string RelationRevDesc
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRRelationRevDesc);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRRelationRevDesc, value, NodeOrder);
            }
        }

        public string CodeCaption
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRCodeCaption);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRCodeCaption, value, NodeOrder);
            }
        }

        public string RelatedCaption
        {
            get
            {
                return Utils.getData(RelationshipNode, Constants.sCRRelatedCaption);
            }
            set
            {
                Utils.putData(putRelationshipNode, Constants.sCRRelatedCaption, value, NodeOrder);
            }
        }

        public XmlNode putRelationship(string sID, string sRelShort)
        {
            if (sID != "" && sID != null)
                RelationshipID = sID;
            if (sRelShort != "" && sRelShort != null)
                RelationshipShortCode = sRelShort;
            return RelationshipNode;
        }

        public XmlNode addRelationship(string sID, string sRelShort, string sDescOnly, string sReversable,
                                       string sShowReverse, string sOneOnly, string sRevOneOnly, string sAutoReverse,
                                       string sExcludeMatch, string sCodeTableName, string sRelateTableName,
                                       string sRelationDesc, string sRelationRevDesc, string sCodeCaption,
                                       string sRelatedCaption)
        {
            RelationshipNode = null;
            object o = putRelationshipNode;
            putRelationship(sID, sRelShort);
            if (sDescOnly != "" && sDescOnly != null)
                RelationshipDescOnly = sDescOnly;
            if (sReversable != "" && sReversable != null)
                RelationshipReversable = sReversable;
            if (sShowReverse != "" && sShowReverse != null)
                RelationshipShowReverse = sShowReverse;
            if (sOneOnly != "" && sOneOnly != null)
                RelationshipOneOnly = sOneOnly;
            if (sRevOneOnly != "" && sRevOneOnly != null)
                RelationshipRevOneOnly = sRevOneOnly;
            if (sAutoReverse != "" && sAutoReverse != null)
                RelationshipAutoReverse = sAutoReverse;
            if (sExcludeMatch != "" && sExcludeMatch != null)
                RelationshipExcludeMatch = sExcludeMatch;
            if (sCodeTableName != "" && sCodeTableName != null)
                CodeTableName = sCodeTableName;
            if (sRelateTableName != "" && sRelateTableName != null)
                RelateTableName = sRelateTableName;
            if (sRelationDesc != "" && sRelationDesc != null)
                RelationDesc = sRelationDesc;
            if (sRelationRevDesc != "" && sRelationRevDesc != null)
                RelationRevDesc = sRelationRevDesc;
            if (sCodeCaption != "" && sCodeCaption != null)
                CodeCaption = sCodeCaption;
            if (sRelatedCaption != "" && sRelatedCaption != null)
                RelatedCaption = sRelatedCaption;
            return RelationshipNode;
        }

        public XmlNode RelationNode
        {
            get
            {
                return xmlRelation;
            }
            set
            {
                xmlRelation = value;
            }
        }

        public XmlNode putRelationNode
        {
            get
            {
                if (RelationNode == null)
                    RelationNode = XML.XMLaddNode(putRelationshipNode, Constants.sCRRelation, NodeOrder);
                return RelationNode;
            }
        }

        public XmlNodeList RelationList
        {
            get
            {
                return getNodeList(Constants.sCRRelation, ref xmlRelationList, RelationshipNode);
            }
        }

        public int RelationCount
        {
            get
            {
                return RelationList.Count;
            }
        }

        public XmlNode getRelation(bool rst)
        {
            return getNode(rst, ref xmlRelationList, ref xmlRelation);
        }

        public XmlNode getRelationByID(string sID)
        {
            bool rst;
            if (RelationID != sID)
            {
                rst = true;
                while (getRelation(rst) != null)
                {
                    rst = false;
                    if (RelationID == sID)
                        break;
                }
            }
            return RelationNode;
        }

        public string RelationID
        {
            get
            {
                string strdata = Utils.getAttribute(RelationNode, "", Constants.sCRRelationID);
                string sid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sCRRelationIDPfx && strdata.Length >= 4)
                        sid = StringUtils.Right(strdata, strdata.Length - 3);
                return sid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    lRelationID = lRelationID + 1;
                    lid = lRelationID + "";
                }
                Utils.putAttribute(putRelationNode, "", Constants.sCRRelationID, Constants.sCRRelationIDPfx + lid, NodeOrder);
            }
        }

        public string RelationShortCode
        {
            get
            {
                return Utils.getAttribute(RelationNode, "", Constants.sCRRelationShort);
            }
            set
            {
                Utils.putAttribute(putRelationNode, "", Constants.sCRRelationShort, value, NodeOrder);
            }
        }

        public string RelationRelatedShortCode
        {
            get
            {
                return Utils.getAttribute(RelationNode, "", Constants.sCRRelationRelShort);
            }
            set
            {
                Utils.putAttribute(putRelationNode, "", Constants.sCRRelationRelShort, value, NodeOrder);
            }
        }

        public string RelationDeleted
        {
            get
            {
                return Utils.getBool(RelationNode, "", Constants.sCRRelationDeleted);
            }
            set
            {
                Utils.putBool(putRelationNode, "", Constants.sCRRelationDeleted, value, NodeOrder);
            }
        }

        public XmlNode putRelation(string sID, string sShortCode)
        {
            if (sID != "" && sID != null)
                RelationID = sID;
            if (sShortCode != "" && sShortCode != null)
                RelationShortCode = sShortCode;
            return RelationNode;
        }

        public XmlNode addRelation(string sID, string sShortCode, string sRelShortCode, string sDeleted)
        {
            RelationNode = null;
            object o = putRelationNode;
            putRelation(sID, sShortCode);
            if (sRelShortCode != "" && sRelShortCode != null)
                RelationRelatedShortCode = sRelShortCode;
            if (sDeleted != "" && sDeleted != null)
                RelationDeleted = sDeleted;
            return RelationNode;
        }
    }
}
