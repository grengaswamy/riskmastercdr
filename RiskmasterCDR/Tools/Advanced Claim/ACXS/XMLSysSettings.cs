/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 03/04/2009 | SIW119 |    sw      | Created
 * 04/10/2009 | SIW183 |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                  | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLSysSetting : XMLXSNodeWithList    //SIW529
    { 
        /*****************************************************************************
        ' The cXMLSysSettings object provides the ability to store and retrieve various
        ' control values within a System Settings XML block.
        '*****************************************************************************
        '<Parent Node>
        '<SYSTEM_SETTINGS NEXT_ID="0" COUNT="xx">
        '   <SYSTEM_SETTING ID="SYS1">
        '    <USER_ID>User ID for whom the setting apply. "0" is for all users </USER_ID>
        '    <TYPE>defines the type of setting</TYPE>
        '    <COMPONENT>component for which the setting apply</COMPONENT>
        '    <ITEM>item to which the setting apply. Subset of Component</ITEM>
        '    <VALUE>Vlaue of the setting</VALUE>
        '    <COMPUTER_NAME>computer name to which the setting apply</COMPUTER_NAME>
        '    <DESCRIPTION>plain english description of the setting</DESCRIPTION>
        '    <GROUP_ID>Group ID for whom the setting apply. "0" is for all users.</GROUP_ID>
        '    <CATEGORY>Category to which the setting apply</CATEGORY>
        '   </SYSTEM_SETTING>
        '</SYSTEM_SETTINGS>
        '</Parent Node>*/
    
        //private XmlNode m_xmlSysSetting;
        
        private XMLSysSettings m_SysSettings;
        //private ArrayList sThisNodeOrder;

        protected override string DefaultNodeName
        {
            get { return Constants.sSysNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "","",
                          "", "", "", "","");
        }

        public XmlNode Create(string sSysID, string sUserID, string sType, string sComponent,
                              string sItem, string sValue, string sComputerName, string sDescription, string sGroupID, string sCategory)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLElementWithEntity_Node(NodeName, "0");
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                if (sSysID != null && sSysID != "")
                    SysID = sSysID;
                if (sUserID != null && sUserID != "")
                    UserID = sUserID;
                if (sType != null && sType != "")
                    SysType = sType;
                if (sComponent != null && sComponent != "")
                    Component = sComponent;
                if (sItem != null && sItem != "")
                    Item = sItem;
                if (sValue != null && sValue != "")
                    SysValue = sValue;
                if (sComputerName != null && sComputerName != "")
                    ComputerName = sComputerName;
                if (sDescription != null && sDescription != "")
                    Description = sDescription;
                if (sGroupID != null && sGroupID != "")
                    GroupID = sGroupID;
                if (sCategory != null && sCategory != "")
                    Category = sCategory;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;  
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSysSetting.Create");
                return null;
            }
        }
        public string SysID
        {
            get 
            {
                string strdata;
                string sSId;
                strdata = XML.XMLGetAttributeValue(this.Node,Constants.sSysID);
                if (strdata != "")
                {
                    if ((strdata.Substring(0, 3) == Constants.sSysIDPfx) && (strdata.Length >= 4))
                        sSId = strdata.Substring(3, strdata.Length);
                    else
                        sSId = "";
                }
                else
                    sSId = "";
               return sSId;
            }
            set 
            {
                string lid;
                if (value == null || value == "" || value == "0")
                {
                    Globals.lSysId += 1;
                    lid = Globals.lSysId.ToString();
                }
                else
                    lid = value;

                XML.XMLSetAttributeValue(putNode, Constants.sSysID, Constants.sSysIDPfx + lid.ToString().Trim());
            }
        }
        public string UserID
        {
            get
            {
                return Utils.getData(Node, Constants.sSysUserID);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysUserID, value, NodeOrder);
            }
        }
        public string SysType
        {
            get
            {
                return Utils.getData(Node, Constants.sSysType);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysType, value, NodeOrder);
            }
        }
        public string Component
        {
            get
            {
                return Utils.getData(Node, Constants.sSysComponent);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysComponent, value, NodeOrder);
            }
        }
        public string Item
        {
            get
            {
                return Utils.getData(Node, Constants.sSysItem);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysItem, value, NodeOrder);
            }
        }
        public string SysValue
        {
            get
            {
                return Utils.getData(Node, Constants.sSysValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysValue, value, NodeOrder);
            }
        }
        public string ComputerName
        {
            get
            {
                return Utils.getData(Node, Constants.sSysComputerName);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysComputerName, value, NodeOrder);
            }
        }
        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sSysDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysDescription, value, NodeOrder);
            }
        }
        public string GroupID
        {
            get
            {
                return Utils.getData(Node, Constants.sSysGroupID);
            }
            set
            {
                Utils.putData(putNode, Constants.sSysGroupID, value, NodeOrder);
            }
        }
        public string Category
        {
            get
            {
                return Utils.getData(Node, Constants.sSysCategory );
            }
            set
            {
                Utils.putData(putNode, Constants.sSysCategory, value, NodeOrder);
            }
        }
        
        public XMLSysSetting()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            sThisNodeOrder  = new ArrayList(10);
            sThisNodeOrder.Add(Constants.sSysID);
            sThisNodeOrder.Add(Constants.sSysUserID);
            sThisNodeOrder.Add(Constants.sSysType);
            sThisNodeOrder.Add(Constants.sSysComponent);
            sThisNodeOrder.Add(Constants.sSysItem);
            sThisNodeOrder.Add(Constants.sSysValue);
            sThisNodeOrder.Add(Constants.sSysComputerName);
            sThisNodeOrder.Add(Constants.sSysDescription);
            sThisNodeOrder.Add(Constants.sSysGroupID);
            sThisNodeOrder.Add(Constants.sSysCategory);
            
        }
        
        private void subClearPointers()
        {
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            
        }
        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            subClearPointers();
        }

        public new XMLSysSettings Parent
        {
            get
            {
                if (m_SysSettings == null)
                {
                    m_SysSettings = new XMLSysSettings();
                    m_SysSettings.SysSetting = this;
                    LinkXMLObjects((XMLXCNodeBase)m_SysSettings);   //SIW529
                    ResetParent();  //SIJIim
                }
                return m_SysSettings;
            }
            set
            {
                m_SysSettings = value;
                ResetParent();
                if (m_SysSettings != null) //SIW529
                    ((XMLXCNodeBase)m_SysSettings).LinkXMLObjects(this);    //SIW529
            }
        }
        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
        
        public XmlNode getSysSetting(bool reset)
        {
            return getNode(reset);
        }
    
        public XmlNode getSysSettingByName(string sUserID, string sType, string sComponent, string sItem)
        {
           bool bMatch;
           bool rst;

           sUserID = sUserID.ToUpper().Trim();
           sType = sType.ToUpper().Trim();
           sComponent = sComponent.ToUpper().Trim();
           sItem = sItem.ToUpper().Trim();
           rst = true;
           while (getSysSetting(rst) != null)
           {
                bMatch = true;
                if(sUserID != "" && sUserID != UserID.ToUpper())  
                       bMatch = false;
                if (sType != "" && sType != SysType.ToUpper())
                       bMatch = false;
                if(sComponent != "" && sComponent != Component.ToUpper())  
                       bMatch = false; 
                if(sItem != "" && sItem != Item.ToUpper())  
                       bMatch = false; ; 
                rst = false;
                if (bMatch) 
                     break;
           }
           return this.Node;   
        }
        public XmlNode getSysSettingbyID(string sID)
        {
            bool rst; 
            if( SysID != sID)
            {
                rst = true;
               while(getSysSetting(rst) != null)
                {
                         if (SysID == sID) 
                         {
                             rst = false;
                             break;
                         }
                }
            }
            return this.Node;    
        }
    }

    public class XMLSysSettings : XMLXSNode    //SIW529
    {
        /*****************************************************************************
        ' The cxmlSysSettings class provides the functionality to support and manage an
        ' AC System Settings Collection Node
        '*****************************************************************************
        '<SYSTEM_SETTINGS NEXT_ID="0" COUNT="xx">
        '   <SYSTEM_SETTING>
        '         ..........
        '   </SYSTEM_SETTING>
        '</SYSTEM_SETTINGS>
        '*****************************************************************************/

        private XMLSysSetting m_SysSetting;

        public XMLSysSettings()
        {
            xmlThisNode = null;
            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sSysNode);

            Node = XML.XMLGetNode(Parent.Node, NodeName);
        
        }

        private void subClearPointers()
        {
            if (m_SysSetting != null)
                //SIW529 m_SysSetting.Node = null;
                m_SysSetting.Parent = this; //SIW529 Resets SysSetting object
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sSysCollection; }
        }

        public override XmlNode Create()
        {
            return Create(null, "");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();
            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = (XmlNode)xmlElement;

                bool bblanking = XML.Blanking;
                XML.Blanking = false;
    
                NextID = sNextID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;  
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSysSettings.Create");
                return null;
            }
        }

        public string getNextID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sSysNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putNode, "", Constants.sSysNextID, value, NodeOrder);
            }
        }
        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sSysCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sSysCount, value + "", NodeOrder);
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    ResetParent();
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }
        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                Utils.gControl.XML = (XML)value;	//SIW485 changed Globals to Utils
                ResetParent();
            }
        }

        public XMLSysSetting SysSetting
        {
            get
            {
                if (m_SysSetting == null)
                {
                    m_SysSetting = new XMLSysSetting();
                    m_SysSetting.Parent = this;
                    
                }
                if (Node != null)
                {
                    if (m_SysSetting.Node == null)
                        m_SysSetting.getSysSetting(Constants.xcGetFirst); 
                }
                return m_SysSetting;
            }
            set
            {
                m_SysSetting = value;
                if (m_SysSetting != null)       
                    m_SysSetting.Parent = this; 
            }
        }
        public void AddSysSetting()
        {
            SysSetting.Create();
        }
    }
}
