/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
//using CCP.XmlComponents;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLGlossary : XMLXSNodeWithList    //SIW529
    {
        /*<GLOSSARY TABLE_COUNT="tablecount">                          <-- Glossary Node
        '   <!-- Multiple Table Entries -->
        '   <GLOSSARY_TABLE ID="TBLxxxx"                              <-- Node
        '               TYPE="type of table (SYS,USER,etc.)"
        '               LINE_OF_BUS_IND="boolean"
        '               DELETED="boolean"
        '               ATTACHMENT="boolean"
        '               CODES_COUNT="count">
        '      <TABLE_NAME>system table name</TABLE_NAME>
        '      <IND_STD_TABLE REQUIRED="boolean">industry standard table name</IND_STD_TABLE_NAME>
        '      <RELATED_TABLE REQUIRED="boolean">Related table name</RELATED_TABLE_NAME>
        '      <INDEX_COLUMNS>index</INDEX_COLUMNS>
        '      <!-- Multiple Friendly Names -->
        '      <FRIENDLY_NAME>
        '         <NAME>friendly name</NAME>
        '         <LANGUAGE CODE="language code">language</LANGUAGE>
        '         <USEAGE>table useage</USEAGE>
        '      </FRIENDLY_NAME>
        '      <!-- Multiple Code Entries -->
        '      <CODE_ENTRY> ... </CODE_ENTRY>
        '   </GLOSSARY_TABLE>
        '</GLOSSARY>*/

        private int lTblId;
        private XmlNode m_xmlGlossary;
        private XmlNode m_xmlTableName;
        private XmlNodeList m_xmlTableNameList;
        private XMLCodes m_Codes;
        private ArrayList sGlsNodeOrder;
        private Dictionary<object, object> dctTableType;

        public XMLGlossary()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_xmlGlossary = null;
            xmlThisNode = null;
            xmlNodeList = null;
            m_xmlTableName = null;
            m_xmlTableNameList = null;
            lTblId = 0;

            sGlsNodeOrder = new ArrayList(1);
            sGlsNodeOrder.Add(Constants.sCDTable);

            sThisNodeOrder = new ArrayList(5);
            sThisNodeOrder.Add(Constants.sCDTableName);
            sThisNodeOrder.Add(Constants.sCDTableIndStdTable);
            sThisNodeOrder.Add(Constants.sCDTableRelatedTable);
            sThisNodeOrder.Add(Constants.sCDTableIndex);
            sThisNodeOrder.Add(Constants.sCDTableFriendlyNameNode);

            dctTableType = new Dictionary<object, object>();
            dctTableType.Add(Constants.sCDTableTypeSystem, Constants.iCDTableTypeSystem);
            dctTableType.Add(Constants.iCDTableTypeSystem, Constants.sCDTableTypeSystem);
            dctTableType.Add(Constants.sCDTableTypeSystemCode, Constants.iCDTableTypeSystemCode);
            dctTableType.Add(Constants.iCDTableTypeSystemCode, Constants.sCDTableTypeSystemCode);
            dctTableType.Add(Constants.sCDTableTypeUserCode, Constants.iCDTableTypeUserCode);
            dctTableType.Add(Constants.iCDTableTypeUserCode, Constants.sCDTableTypeUserCode);
            dctTableType.Add(Constants.sCDTableTypeEntityCode, Constants.iCDTableTypeEntityCode);
            dctTableType.Add(Constants.iCDTableTypeEntityCode, Constants.sCDTableTypeEntityCode);
            dctTableType.Add(Constants.sCDTableTypeOrgHier, Constants.iCDTableTypeOrgHier);
            dctTableType.Add(Constants.iCDTableTypeOrgHier, Constants.sCDTableTypeOrgHier);
            dctTableType.Add(Constants.sCDTableTypeSupp, Constants.iCDTableTypeSupp);
            dctTableType.Add(Constants.iCDTableTypeSupp, Constants.sCDTableTypeSupp);
            dctTableType.Add(Constants.sCDTableTypePeople, Constants.iCDTableTypePeople);
            dctTableType.Add(Constants.iCDTableTypePeople, Constants.sCDTableTypePeople);
            dctTableType.Add(Constants.sCDTableTypeAdminTrack, Constants.iCDTableTypeAdminTrack);
            dctTableType.Add(Constants.iCDTableTypeAdminTrack, Constants.sCDTableTypeAdminTrack);
            dctTableType.Add(Constants.sCDTableTypeJurisdic, Constants.iCDTableTypeJurisdic);
            dctTableType.Add(Constants.iCDTableTypeJurisdic, Constants.sCDTableTypeJurisdic);
            dctTableType.Add(Constants.sCDTableTypeIndStdCodes, Constants.iCDTableTypeIndStdCodes);
            dctTableType.Add(Constants.iCDTableTypeIndStdCodes, Constants.sCDTableTypeIndStdCodes);
            dctTableType.Add(Constants.sCDTableTypeGlossary, Constants.iCDTableTypeGlossary);
            dctTableType.Add(Constants.iCDTableTypeGlossary, Constants.sCDTableTypeGlossary);
            dctTableType.Add(Constants.sUndefined, Constants.iUndefined);
            dctTableType.Add(Constants.iUndefined, Constants.sUndefined);
        }

        public string Tablecount
        {
            get
            {
                return Utils.getAttribute(GlossaryNode, "", Constants.sCDTableCount);
            }
            set
            {
                Utils.putAttribute(putGlossaryNode, "", Constants.sCDTableCount, value, sGlsNodeOrder);
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sCDTableIDPfx;
            }
        }

        public string TableID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sCDTableID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sCDTableIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    lTblId = lTblId + 1;
                    lid = lTblId + "";
                }
                Utils.putAttribute(putNode, "", Constants.sCDTableID, Constants.sCDTableIDPfx + lid, NodeOrder);
            }
        }

        public cdsTableType TableType
        {
            get
            {
                string sdata = Utils.getAttribute(Node, "", Constants.sCDTableType);
                object cType;
                if (!dctTableType.TryGetValue(sdata, out cType))
                    cType = cdsTableType.cdttUnspecified;
                return (cdsTableType)cType;
            }
            set
            {
                object sType;
                dctTableType.TryGetValue((int)value, out sType);
                Utils.putAttribute(putNode, "", Constants.sCDTable, (string)sType, NodeOrder);
            }
        }

        public string TableType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sCDTableType);
            }
            set
            {
                object odata;
                int iType;
                string sdata = value;
                bool i = false;

                if (Int32.TryParse(value, out iType))
                {
                    i = true;
                    odata = iType;
                }
                else
                    odata = value;

                if (dctTableType.ContainsKey(odata))
                {
                    if (i)
                        dctTableType.TryGetValue(iType, out odata);
                    sdata = (string)odata;
                    Utils.putAttribute(putNode, "", Constants.sCDTableType, sdata, NodeOrder);
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, "XMLGlossary.TableType_Code",
                                     value + "", Document, Node);
            }
        }

        public string LOBRequired
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sCDTableLOBInd);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sCDTableLOBInd, value, NodeOrder);
            }
        }

        public string Deleted
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sCDDeleted);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sCDDeleted, value, NodeOrder);
            }
        }

        public string Attachments
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sCDTableAttachment);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sCDTableAttachment, value, NodeOrder);
            }
        }

        public string CodesCount
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sCDTableCodesCount);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sCDTableCodesCount, value, NodeOrder);
            }
        }

        public string SystemName
        {
            get
            {
                return Utils.getData(Node, Constants.sCDTableName);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDTableName, value, NodeOrder);
            }
        }

        public string InsStdTable
        {
            get
            {
                return Utils.getData(Node, Constants.sCDTableIndStdTable);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDTableIndStdTable, value, NodeOrder);
            }
        }

        public string InsStdTableRequired
        {
            get
            {
                return Utils.getBool(Node, Constants.sCDTableIndStdTable, Constants.sCDRequired);
            }
            set
            {
                Utils.putBool(putNode, Constants.sCDTableIndStdTable, Constants.sCDRequired, value, NodeOrder);
            }
        }

        public string RelatedTable
        {
            get
            {
                return Utils.getData(Node, Constants.sCDTableRelatedTable);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDTableRelatedTable, value, NodeOrder);
            }
        }

        public string RelatedTableRequired
        {
            get
            {
                return Utils.getBool(Node, Constants.sCDTableRelatedTable, Constants.sCDRequired);
            }
            set
            {
                Utils.putBool(putNode, Constants.sCDTableRelatedTable, Constants.sCDRequired, value, NodeOrder);
            }
        }

        public string Index
        {
            get
            {
                return Utils.getData(Node, Constants.sCDTableIndex);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDTableIndex, value, NodeOrder);
            }
        }

        public XmlNode FindFriendlyName(string sName)
        {
            bool rst;
            if (FriendlyName != sName)
            {
                rst = true;
                while (getFriendlyName(rst) != null)
                {
                    rst = false;
                    if (FriendlyName == sName)
                        break;
                }
            }
            return m_xmlTableName;
        }

        public XmlNodeList FriendlyNameList
        {
            get
            {
                return getNodeList(Constants.sCDTableFriendlyNameNode, ref m_xmlTableNameList, Node);
            }
        }

        public XmlNode getFriendlyName(bool rst)
        {
            object o = FriendlyNameList;
            return getNode(rst, ref m_xmlTableNameList, ref m_xmlTableName);
        }

        public int FriendlyNameCount
        {
            get
            {
                return FriendlyNameList.Count;
            }
        }

        public XmlNode addFriendlyName(string sFriendlyName, string sLanguage, string sLanguageCode, string sUseage)
        {
            FriendlyNameNode = null;

            FriendlyName = sFriendlyName;
            if ((sLanguage != "" && sLanguage != null) || (sLanguageCode != "" && sLanguageCode != null))
                putFriendlyNameLanguage(sLanguage, sLanguageCode);
            Useage = sUseage;

            return FriendlyNameNode;
        }

        public XmlNode putFriendlyNameNode
        {
            get
            {
                if (m_xmlTableName == null)
                    m_xmlTableName = XML.XMLaddNode(putNode, Constants.sCDTableFriendlyNameNode, NodeOrder);
                return m_xmlTableName;
            }
        }

        public XmlNode FriendlyNameNode
        {
            get
            {
                return m_xmlTableName;
            }
            set
            {
                m_xmlTableName = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return Utils.getData(FriendlyNameNode, Constants.sCDTableFriendlyName);
            }
            set
            {
                Utils.putData(putFriendlyNameNode, Constants.sCDTableFriendlyName, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public XmlNode putFriendlyNameLanguage(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putFriendlyNameNode, Constants.sCDLanguage, sDesc, sCode, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
        }

        public string FriendlyNameLanguage
        {
            get
            {
                return Utils.getData(FriendlyNameNode, Constants.sCDLanguage);
            }
            set
            {
                Utils.putData(putFriendlyNameNode, Constants.sCDLanguage, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string FriendlyNameLanguage_Code
        {
            get
            {
                return Utils.getCode(FriendlyNameNode, Constants.sCDLanguage);
            }
            set
            {
                Utils.putCode(putFriendlyNameNode, Constants.sCDLanguage, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public string Useage
        {
            get
            {
                return Utils.getData(FriendlyNameNode, Constants.sCDUseage);
            }
            set
            {
                Utils.putData(putFriendlyNameNode, Constants.sCDUseage, value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        private void subClearPointers()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            subClearTablePointers();
        }

        private void subClearTablePointers()
        {
            m_xmlTableName = null;
            m_xmlTableNameList = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                base.Document = value;
                subClearPointers();
            }
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                Utils.gControl.XML = value;	//SIW485 changed Globals to Utils
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sCDGlossary; }
        }

        public XmlNode putGlossaryNode
        {
            get
            {
                if (GlossaryNode == null)
                    m_xmlGlossary = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
                return m_xmlGlossary;
            }
        }

        public XmlNode GlossaryNode
        {
            get
            {
                if (m_xmlGlossary == null)
                    m_xmlGlossary = XML.XMLGetNode(Parent.Node, NodeName);
                return m_xmlGlossary;
            }
            set
            {
                m_xmlGlossary = value;
                subClearPointers();
            }
        }

        public ArrayList GlossaryNodeOrder
        {
            get
            {
                return sGlsNodeOrder;
            }
        }

        public XmlNode addTable(string sid, cdsTableType cType, string sName, string sFriendlyName, string sFriendlyNameLang,
                                string sFriendlyNameLangCode, string sLOBInd, string sIndStdTable, string sIndStdTableRqrd,
                                string sRelatedTable, string sRelatedTableRqrd, string sUseage, string sDeleted,
                                string sIndex, bool bAttachmentFlag)
        {
            Node = null;
            TableID = sid;
            TableType = cType;
            LOBRequired = sLOBInd;
            SystemName = sName;
            if (sIndStdTable != "" && sIndStdTable != null)
            {
                InsStdTable = sIndStdTable;
                InsStdTableRequired = sIndStdTableRqrd;
            }
            if (sRelatedTable != "" && sRelatedTable != null)
            {
                RelatedTable = sRelatedTable;
                RelatedTableRequired = sRelatedTableRqrd;
            }

            if (sFriendlyName != "" && sFriendlyName != null)
                addFriendlyName(sFriendlyName, sFriendlyNameLang, sFriendlyNameLangCode, sUseage);
            Deleted = sDeleted;
            Index = sIndex;
            Attachments = bAttachmentFlag.ToString();
            return Node;
        }

        public override XmlNode putNode
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLaddNode(putGlossaryNode, Constants.sCDTable, sGlsNodeOrder);
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearTablePointers();
            }
        }

        public XmlNodeList TableList
        {
            get
            {
                return getNodeList(Constants.sCDTable, ref xmlNodeList, GlossaryNode);
            }
        }

        public XmlNode getTable(XmlDocument xdoc, bool rst)
        {
            if (xdoc != null)
                Document = xdoc;
            object o = TableList;
            return getNode(rst);
        }

        public void removeTable()
        {
            if (Node != null)
                Node.RemoveChild(Node);
            Node = null;
        }

        public XmlNode findTableByName(string sTableName, XmlDocument xdoc)
        {
            bool rst;
            if (SystemName != sTableName)
            {
                rst = true;
                while (getTable(xdoc, rst) != null)
                {
                    rst = false;
                    if (SystemName == sTableName)
                        break;
                }
            }
            return Node;
        }

        public XmlNode findTableByID(string sid, XmlDocument xdoc)
        {
            bool rst;
            if (sid != TableID)
            {
                rst = true;
                while (getTable(xdoc, rst) != null)
                {
                    rst = false;
                    if (sid == TableID)
                        break;
                }
            }
            return Node;
        }

        public XMLCodes Codes
        {
            get
            {
                if (m_Codes == null)
                {
                    m_Codes = new XMLCodes();
                    m_Codes.Parent = this;
                }
                return m_Codes;
            }
            set
            {
                m_Codes = value;
                if (m_Codes != null)
                    m_Codes.Parent = this;
            }
        }
    }
}
