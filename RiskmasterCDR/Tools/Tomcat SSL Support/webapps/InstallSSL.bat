@ECHO OFF
:START

REM Open the web.xml file for editing
notepad .\webapps\oxf\WEB-INF\web.xml

REM Open the global-parameters file for editing
notepad .\webapps\oxf\WEB-INF\resources\config\global-parameters.xml

REM Open the cas_jaas.conf file for editing
notepad .\webapps\SSO\META-INF\cas_jaas.conf

REM Open the web.xml file for editing
notepad .\webapps\jetspeed\WEB-INF\web.xml

REM Open the CSC_JetspeedSecurity.properties file for editing
notepad .\webapps\jetspeed\WEB-INF\conf\CSC_JetspeedSecurity.properties

REM Open the JSP template for editing
notepad .\webapps\jetspeed\WEB-INF\templates\jsp\navigations\html\top_default.jsp

REM Open the PSML file for editing
"%ProgramFiles%\Windows NT\Accessories\wordpad.exe" .\webapps\jetspeed\WEB-INF\psml\role\user\html\default.psml

REM Re-start the Tomcat service
REM Stop the Tomcat service
net stop tomcat5

REM Start the Tomcat service
net start tomcat5

ECHO This Batch file was created with the Easy Batch Builder from Octopussy Software.