ECHO Setting the path to GACUtil executable
SET PATH="C:\WINDOWS\Microsoft.NET\Framework\v1.1.4322\"

ECHO Installing Assemblies to the GAC
ECHO Installing DB2 Assembly
gacutil /i IBM.DB2.dll
ECHO Installing Oracle DataAccess Assembly
gacutil /i Oracle.DataAccess.dll
ECHO Installing Riskmaster Common Globalization Assembly
gacutil /i Riskmaster.Common.Globalization.dll
ECHO Installing Riskmaster Common Assembly
gacutil /i Riskmaster.Common.dll
ECHO Installing Riskmaster Common Cache Assembly
gacutil /i Riskmaster.Common.Cache.dll
ECHO Installing Riskmaster Conversion Assembly
gacutil /i Riskmaster.Conversion.dll
ECHO Installing Riskmaster DataModel Assembly
gacutil /i Riskmaster.DataModel.dll
ECHO Installing Riskmaster DB Assembly
gacutil /i Riskmaster.Db.dll
ECHO Installing Riskmaster ExceptionTypes Assembly
gacutil /i Riskmaster.ExceptionTypes.dll
ECHO Installing Riskmaster Settings Assembly
gacutil /i Riskmaster.Settings.dll
ECHO Installing Riskmaster Security Assembly
gacutil /i Riskmaster.Security.dll
ECHO Installing Riskmaster MailMerge Assemlby
gacutil /i Riskmaster.Application.MailMerge.dll
ECHO Installing Riskmaster PrintChecks Assembly
gacutil /i Riskmaster.Application.PrintChecks.dll
ECHO Installing Riskmaster Reserves Assembly
gacutil /i Riskmaster.Application.Reserves.dll
ECHO Installing Riskmaster ReportInterfaces Assemlby
gacutil /i Riskmaster.Application.ReportInterfaces.dll
ECHO Installing Riskmaster ZipUtil Assembly
gacutil /i Riskmaster.Application.ZipUtil.dll
