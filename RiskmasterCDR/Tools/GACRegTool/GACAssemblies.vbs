'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 3.1
'
' NAME: GACAssemblies.vbs
'
' AUTHOR: CSC , CSC
' DATE  : 3/24/2005
'
' COMMENT: Script for creating a path in the HKLM registry hive to point
'          to a local source for GAC Assemblies
'
'==========================================================================
Dim objWSH
Dim HKLM, DotNetFramework
Dim strGACAssembliesPath

'Retrieve the GACAssemblies Path
strGACAssembliesPath = InputBox("Please enter the path to the GACAssemblies")

'Initialize the variable to the HKLM registry hive
HKLM = "HKEY_LOCAL_MACHINE\"

'Create the Windows Scripting Host object
Set objWSH = CreateObject("WScript.Shell")


'Create necessary keys in the Registry
DotNetFramework = HKLM & "SOFTWARE\Microsoft\.NETFramework\"

'Create the .Net Framework GACAssemblies Registry Key
objWSH.RegWrite DotNetFramework & "AssemblyFolders\GACAssemblies\", strGACAssembliesPath, "REG_SZ"

'Create the GACAssemblies Registry Key
'objWSH.RegWrite DotNetFramework & "AssemblyFolders\", "GACAssemblies"

'Set the local path to the GACAssemblies directory
'objWSH.RegWrite DotNetFramework & "AssemblyFolders\GACAssemblies", strGACAssembliesPath, "REG_SZ"
 

'Destroy the object
Set objWSH = Nothing
