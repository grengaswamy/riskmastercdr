﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Db;

namespace RMXJurRulesDBUpgradeWizard
{

    public class CJRBenefitRuleTTD
    {

        private const string sClassName = "CJRBenefitRuleTTD";

        private const string sTableName = "WCP_RULE_TTD";

        private int m_DeletedFlag;

        private int m_TableRowID;

        private double m_TotalAmount;

        private int m_TotalWeeks;

        private double m_PrimeRate;

        private int m_PrimeRateMaxWeeks;

        private int m_PrimeRateMaxAmount;

        private double m_SecondRate;

        private int m_SecondRateMaxWeeks;

        private double m_SecondRateMaxAmount;

        private int m_JurisRowID;

        private string m_EffectiveDateDTG;

        private string m_EndingDateDTG;

        private double m_FloorAmount;

        private int m_PayFloorAmount;

        private int m_DollarForDollar;

        private double m_MaxAWW;

        private double m_MaxCompRate;

        private double m_PercentSAWW;

        private int m_PayCurrentRateAfterTwoYears;

        private int m_PayConCurrentPPD;

        private bool m_DataHasChanged;

        private int m_UseSAWWMaximumCode;

        private int m_UseSAWWMinimumCode;

        public bool DataHasChanged
        {
            get
            {
                return m_DataHasChanged;
            }
            set
            {
                m_DataHasChanged = value;
            }
        }

        public int DeletedFlag
        {
            get
            {
                return m_DeletedFlag;
            }
            set
            {
                m_DeletedFlag = value;
            }
        }

        public int PayConCurrentPPD
        {
            get
            {
                return m_PayConCurrentPPD;
            }
            set
            {
                m_PayConCurrentPPD = value;
            }
        }

        public double SecondRate
        {
            get
            {
                return m_SecondRate;
            }
            set
            {
                m_SecondRate = value;
            }
        }

        public double PrimeRate
        {
            get
            {
                return m_PrimeRate;
            }
            set
            {
                m_PrimeRate = value;
            }
        }

        public double PercentSAWW
        {
            get
            {
                return m_PercentSAWW;
            }
            set
            {
                m_PercentSAWW = value;
            }
        }

        public double MaxAWW
        {
            get
            {
                return m_MaxAWW;
            }
            set
            {
                m_MaxAWW = value;
            }
        }

        public double MaxCompRate
        {
            get
            {
                return m_MaxCompRate;
            }
            set
            {
                m_MaxCompRate = value;
            }
        }

        public int DollarForDollar
        {
            get
            {
                return m_DollarForDollar;
            }
            set
            {
                m_DollarForDollar = value;
            }
        }

        public int PayFloorAmount
        {
            get
            {
                return m_PayFloorAmount;
            }
            set
            {
                m_PayFloorAmount = value;
            }
        }

        public double FloorAmount
        {
            get
            {
                return m_FloorAmount;
            }
            set
            {
                m_FloorAmount = value;
            }
        }

        public string EndingDateDTG
        {
            get
            {
                return m_EndingDateDTG;
            }
            set
            {
                m_EndingDateDTG = value;
            }
        }

        public string EffectiveDateDTG
        {
            get
            {
                return m_EffectiveDateDTG;
            }
            set
            {
                m_EffectiveDateDTG = value;
            }
        }

        public int JurisRowID
        {
            get
            {
                return m_JurisRowID;
            }
            set
            {
                m_JurisRowID = value;
            }
        }

        public double SecondRateMaxAmount
        {
            get
            {
                return m_SecondRateMaxAmount;
            }
            set
            {
                m_SecondRateMaxAmount = value;
            }
        }

        public int SecondRateMaxWeeks
        {
            get
            {
                return m_SecondRateMaxWeeks;
            }
            set
            {
                m_SecondRateMaxWeeks = value;
            }
        }

        public int PrimeRateMaxAmount
        {
            get
            {
                return m_PrimeRateMaxAmount;
            }
            set
            {
                m_PrimeRateMaxAmount = value;
            }
        }

        public int PrimeRateMaxWeeks
        {
            get
            {
                return m_PrimeRateMaxWeeks;
            }
            set
            {
                m_PrimeRateMaxWeeks = value;
            }
        }

        public int TotalWeeks
        {
            get
            {
                return m_TotalWeeks;
            }
            set
            {
                m_TotalWeeks = value;
            }
        }

        public double TotalAmount
        {
            get
            {
                return m_TotalAmount;
            }
            set
            {
                m_TotalAmount = value;
            }
        }

        public int PayCurrentRateAfterTwoYears
        {
            get
            {
                return m_PayCurrentRateAfterTwoYears;
            }
            set
            {
                m_PayCurrentRateAfterTwoYears = value;
            }
        }

        public int TableRowID
        {
            get
            {
                return m_TableRowID;
            }
            set
            {
                m_TableRowID = value;
            }
        }

        public int UseSAWWMaximumCode
        {
            get
            {
                return m_UseSAWWMaximumCode;
            }
            set
            {
                m_UseSAWWMaximumCode = value;
            }
        }

        public int UseSAWWMinimumCode
        {
            get
            {
                return m_UseSAWWMinimumCode;
            }
            set
            {
                m_UseSAWWMinimumCode = value;
            }
        }

        public int LoadData(ref int lJurisRowID, ref string sBeginDateDTG)
        {
            string sSQL;
            DbReader objReader;
            int LoadData = 0;

            try
            {
                sSQL = GetSQLFields();
                sSQL = (sSQL + (" FROM " + sTableName));
                sSQL = (sSQL + (" WHERE JURIS_ROW_ID = " + lJurisRowID));

                if ((sBeginDateDTG != ""))
                {
                    sSQL = (sSQL + (" AND EFFECTIVE_DATE = \'"
                                + (sBeginDateDTG + "\'")));
                }
                sSQL = (sSQL + " ORDER BY EFFECTIVE_DATE DESC");

                objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL);
               
                while (objReader.Read())
                {
                    m_TableRowID = objReader.GetInt("TABLE_ROW_ID");
                }

                objReader = null;
                LoadData = -1;

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + "- [CJRBenefitRuleTTD.LoadData]  " + ex.Message);
                return 0;
            }
            return LoadData;
        }

        private void ClearObject()
        {
            m_DataHasChanged = false;
            m_DollarForDollar = 0;
            m_EffectiveDateDTG = "";
            m_EndingDateDTG = "";
            m_FloorAmount = 0;
            m_JurisRowID = 0;
            m_MaxAWW = 0;
            m_MaxCompRate = 0;
            m_PayFloorAmount = 0;
            m_PercentSAWW = 0;
            m_PayConCurrentPPD = 0;
            m_PayCurrentRateAfterTwoYears = -1;
            m_PrimeRate = 0;
            m_PrimeRateMaxWeeks = 0;
            m_PrimeRateMaxAmount = 0;
            m_SecondRate = 0;
            m_SecondRateMaxWeeks = 0;
            m_SecondRateMaxAmount = 0;
            m_TotalAmount = 0;
            m_TotalWeeks = 0;
            m_TableRowID = 0;
        }

        public int SaveData()
        {
            string sSQL;
            DbReader objReader;
            DbWriter objWriter;
            int SaveData = 0;
            try
            {
                sSQL = GetSQLFields();
                sSQL = (sSQL + (" FROM " + sTableName));
                sSQL = (sSQL + (" WHERE TABLE_ROW_ID = " + m_TableRowID));
				//JIRA RMA-1052 
                using (objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        objWriter = DbFactory.GetDbWriter(objReader, true);
                        objWriter.Fields["DTTM_RCD_LAST_UPD"].Value = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                        objWriter.Fields["UPDATED_BY_USER"].Value = "CSC-6db";
                        objWriter.Fields["EFFECTIVE_DATE"].Value = m_EffectiveDateDTG;
                        objWriter.Fields["JURIS_ROW_ID"].Value = m_JurisRowID;
                        objWriter.Fields["DELETED_FLAG"].Value = m_DeletedFlag;
                        objWriter.Fields["BEGIN_DATE"].Value = m_EffectiveDateDTG;
                        objWriter.Fields["PAY_DOLLAR_CODE"].Value = m_DollarForDollar;
                        objWriter.Fields["END_DATE"].Value = m_EndingDateDTG;
                        objWriter.Fields["MIN_BENEFIT"].Value = m_FloorAmount;
                        objWriter.Fields["MAX_AWW"].Value = m_MaxAWW;
                        objWriter.Fields["MAX_COMP_RATE"].Value = m_MaxCompRate;
                        objWriter.Fields["PAY_FLOOR_CODE"].Value = m_PayFloorAmount;
                        objWriter.Fields["MAX_BENEFIT"].Value = m_PercentSAWW;
                        objWriter.Fields["USE_TWOYEAR_CODE"].Value = m_PayCurrentRateAfterTwoYears;
                        objWriter.Fields["PRIME_RATE"].Value = m_PrimeRate;
                        objWriter.Fields["PR_RAT_MAX_WEEKS"].Value = m_PrimeRateMaxWeeks;
                        objWriter.Fields["MAX_BENEFIT"].Value = m_PrimeRateMaxAmount;
                        objWriter.Fields["SECOND_RATE"].Value = m_SecondRate;
                        objWriter.Fields["SC_RAT_MAX_WEEKS"].Value = m_SecondRateMaxWeeks;
                        objWriter.Fields["SC_RAT_MAX_BENFIT"].Value = m_SecondRateMaxAmount;
                        objWriter.Fields["TOTAL_AMT"].Value = m_TotalAmount;
                        objWriter.Fields["TOTAL_WEEKS"].Value = m_TotalWeeks;
                        objWriter.Fields["PAY_CONC_PPD_CODE"].Value = m_PayConCurrentPPD;
                        objWriter.Fields["USE_SAWW_MAX_CODE"].Value = m_UseSAWWMaximumCode;
                        objWriter.Fields["USE_SAWW_MIN_CODE"].Value = m_UseSAWWMinimumCode;
                    }
                    else
                    {
                        objWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_sConnectString);
                        objWriter.Tables.Add(sTableName);
                        objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID);
                        objWriter.Fields.Add("ADDED_BY_USER", "CSC-6db");
                        objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                        objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                        objWriter.Fields.Add("UPDATED_BY_USER", "CSC-6db");
                        objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG);
                        // check column exist or not 
                        objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID);
                        objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag);
                        objWriter.Fields.Add("BEGIN_DATE", m_EffectiveDateDTG);
                        // check column exist or not 
                        objWriter.Fields.Add("PAY_DOLLAR_CODE", m_DollarForDollar);
                        objWriter.Fields.Add("END_DATE", m_EndingDateDTG);
                        objWriter.Fields.Add("MIN_BENEFIT", m_FloorAmount);
                        objWriter.Fields.Add("MAX_AWW", m_MaxAWW);
                        objWriter.Fields.Add("MAX_COMP_RATE", m_MaxCompRate);
                        objWriter.Fields.Add("PAY_FLOOR_CODE", m_PayFloorAmount);
                        objWriter.Fields.Add("MAX_BENEFIT", m_PercentSAWW);
                        objWriter.Fields.Add("USE_TWOYEAR_CODE", m_PayCurrentRateAfterTwoYears);
                        objWriter.Fields.Add("PRIME_RATE", m_PrimeRate);
                        objWriter.Fields.Add("PR_RAT_MAX_WEEKS", m_PrimeRateMaxWeeks);
                        objWriter.Fields.Add("MAX_BENEFIT", m_PrimeRateMaxAmount);
                        objWriter.Fields.Add("SECOND_RATE", m_SecondRate);
                        objWriter.Fields.Add("SC_RAT_MAX_WEEKS", m_SecondRateMaxWeeks);
                        objWriter.Fields.Add("SC_RAT_MAX_BENFIT", m_SecondRateMaxAmount);
                        objWriter.Fields.Add("TOTAL_AMT", m_TotalAmount);
                        objWriter.Fields.Add("TOTAL_WEEKS", m_TotalWeeks);
                        objWriter.Fields.Add("PAY_CONC_PPD_CODE", m_PayConCurrentPPD);
                        objWriter.Fields.Add("USE_SAWW_MAX_CODE", m_UseSAWWMaximumCode);
                        objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode);
                    }
                    objWriter.Execute();
                    objWriter = null;
                }
                SaveData = -1;
                
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + "- [CJRBenefitRuleTTD.SaveData]  " + ex.Message);
                return 0;
            }
            return SaveData;
        }

        private string GetSQLFields()
        {
            string sSQL;
            sSQL = "";
            sSQL = (sSQL + "SELECT");
            sSQL = (sSQL + " TABLE_ROW_ID");
            sSQL = (sSQL + ", DTTM_RCD_ADDED, ADDED_BY_USER");
            sSQL = (sSQL + ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER");
            sSQL = (sSQL + ", JURIS_ROW_ID");
            sSQL = (sSQL + ", DELETED_FLAG");
            sSQL = (sSQL + ", EFFECTIVE_DATE");
            sSQL = (sSQL + ", END_DATE");
            sSQL = (sSQL + ", MIN_BENEFIT,MAX_AWW");
            sSQL = (sSQL + ", MAX_COMP_RATE");
            sSQL = (sSQL + ", PAY_FLOOR_CODE");
            sSQL = (sSQL + ", MAX_BENEFIT");
            sSQL = (sSQL + ", PAY_DOLLAR_CODE");
            sSQL = (sSQL + ", PR_RAT_MAX_BENFIT");
            sSQL = (sSQL + ", PR_RAT_MAX_WEEKS");
            sSQL = (sSQL + ", SC_RAT_MAX_BENFIT");
            sSQL = (sSQL + ", SC_RAT_MAX_WEEKS");
            sSQL = (sSQL + ", TOTAL_WEEKS");
            sSQL = (sSQL + ", TOTAL_AMT,USE_TWOYEAR_CODE");
            sSQL = (sSQL + ", PRIME_RATE, SECOND_RATE");
            sSQL = (sSQL + ", PAY_CONC_PPD_CODE");
            sSQL = (sSQL + ", USE_SAWW_MAX_CODE");
            sSQL = (sSQL + ", USE_SAWW_MIN_CODE");
            return sSQL;
        }
    }
}
