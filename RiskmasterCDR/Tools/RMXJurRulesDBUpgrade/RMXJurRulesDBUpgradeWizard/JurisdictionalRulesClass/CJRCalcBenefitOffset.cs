﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Riskmaster.Db;
using Riskmaster.Common; //JIRA RMA-1052 

namespace RMXJurRulesDBUpgradeWizard
{

    public class CJRCalcBenefitOffset
    {

        private const string sClassName = "CJRCalcBenefitOffset";
        //local copy
        private int m_TableRowID;
        //local copy
        private int m_UseCode;
        //local copy
        private int m_TableRowIDParent;
        //local copy
        private int m_TableRowIDFriendlyName;
        //local copy
        private string m_FriendlyName;
        public string ConnectionString
        {
            get { return DisplayDBUpgrade.g_sConnectString; }
            set { DisplayDBUpgrade.g_sConnectString = value; }
        }
        public string FriendlyName
        {
            get { return m_FriendlyName; }
            set { m_FriendlyName = value; }
        }
        public int TableRowIDFriendlyName
        {
            get { return m_TableRowIDFriendlyName; }
            set { m_TableRowIDFriendlyName = value; }
        }
        public int TableRowIDParent
        {
            get { return m_TableRowIDParent; }
            set { m_TableRowIDParent = value; }
        }
        public int useCode
        {
            get { return m_UseCode; }
            set { m_UseCode = value; }
        }
        public int TableRowID
        {
            get { return m_TableRowID; }
            set { m_TableRowID = value; }
        }
        //---------------------------------------------------------------------------------------
        // Procedure : SaveData
        // DateTime  : 8/17/2004 16:50
        // Author    : jtodd22
        // Purpose   : To save data
        // Note......: This is called from a function that has all the Rocket connections in place
        //---------------------------------------------------------------------------------------
        //
        public int SaveData()
        {
            int functionReturnValue = 0;
            int lTest = 0;
            string sSQL = null;
            Riskmaster.Db.DbReader objReader = null;
            Riskmaster.Db.DbWriter objWriter = null;
            functionReturnValue = 0;

            try
            {

                sSQL = GetBaseSQL();
                sSQL = sSQL + " FROM WCP_BEN_X_SWCH";
                sSQL = sSQL + " WHERE WCP_BEN_X_SWCH.FN_TABLE_ROW_ID = " + m_TableRowIDFriendlyName;
                sSQL = sSQL + " AND WCP_BEN_X_SWCH.PT_TABLE_ROW_ID = " + m_TableRowIDParent;
				//JIRA RMA-1052 
                using (objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        //existing record -- do an edit
                        objWriter = DbFactory.GetDbWriter(objReader, true);
                        objWriter.Fields["FN_TABLE_ROW_ID"].Value = m_TableRowIDFriendlyName;
                        objWriter.Fields["PT_TABLE_ROW_ID"].Value = m_TableRowIDParent;
                        objWriter.Fields["USE_CODE"].Value = m_UseCode;
                    }
                    else
                    {
                        //new record
                        objWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_sConnectString);
                        //lTest = modGlobals.GetNextUID("WCP_BEN_X_SWCH");
                        lTest = Utilities.GetNextUID(DisplayDBUpgrade.g_sConnectString, "WCP_BEN_X_SWCH"); //JIRA RMA-1052 
                        objWriter.Tables.Add("WCP_BEN_X_SWCH");
                        objWriter.Fields.Add("TABLE_ROW_ID", lTest);
                        objWriter.Fields.Add("FN_TABLE_ROW_ID", m_TableRowIDFriendlyName);
                        objWriter.Fields.Add("PT_TABLE_ROW_ID", m_TableRowIDParent);
                        objWriter.Fields.Add("USE_CODE", m_UseCode);
                    }
                    objWriter.Execute();
                    objWriter = null; //JIRA RMA-1052 
                }

                functionReturnValue = -1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcBenefitOffset.SaveData]" + ex.Message);
            }
            return functionReturnValue;

        }
        private string GetBaseSQL()
        {
            string functionReturnValue = null;
            string sSQL = null;
            functionReturnValue = "";
            sSQL = "";
            sSQL = sSQL + "SELECT";
            sSQL = sSQL + " FN_TABLE_ROW_ID";
            sSQL = sSQL + ", PT_TABLE_ROW_ID";
            sSQL = sSQL + ", TABLE_ROW_ID";
            sSQL = sSQL + ", USE_CODE";
            functionReturnValue = sSQL;
            return functionReturnValue;
        }
    }

}
