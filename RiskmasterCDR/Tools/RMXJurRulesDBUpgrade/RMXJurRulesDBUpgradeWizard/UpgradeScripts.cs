﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data;
using Microsoft.Win32;
using Riskmaster.Security.Encryption;
using Riskmaster.Db;

namespace RMXJurRulesDBUpgradeWizard
{
    class UpgradeScripts
    {
        #region variables
        public static bool _isSilent;
        const string SQL_SERVER_FILTER = "SQL Server";
        const string SQL_NATIVE_CLIENT_FILTER = "SQL Native Client";
        const string SQL_SERVER_NATIVE_CLIENT_FILTER = "SQL Server Native Client";
        const string ORACLE_FILTER = "Oracle in";
        #endregion

        /// <summary>
        /// returns a string array returning the names of the SQL Script files
        /// </summary>
        /// <param name="strSQLScriptFiles"></param>
        /// <param name="strDelimiter"></param>
        /// <returns></returns>
        public static string[] GetSQLScriptFiles(string strSQLScriptFiles, string strDelimiter)
        {
            List<string> arrListSQLScriptFiles = new List<string>();

            if ((!String.IsNullOrEmpty(strSQLScriptFiles)) && (strSQLScriptFiles.Length > 0))
            {
                return strSQLScriptFiles.Split(strDelimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }

            return arrListSQLScriptFiles.ToArray();
        }

        /// <summary>
        /// Gets a list of the ODBC Drivers on the system for SQL Server and Oracle
        /// </summary>
        /// <returns>string array containing all of the currently installed SQL Server and Oracle drivers</returns>
        public static string[] GetODBCDrivers()
        {
            List<string> arrFilteredDrivers = new List<string>();
            const string ODBC_DRIVERS = @"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers";

            RegistryKey regODBCKey = Registry.LocalMachine.OpenSubKey(ODBC_DRIVERS);
            string[] arrODBCDrivers = regODBCKey.GetValueNames();

            foreach (string strODBCDriver in arrODBCDrivers)
            {
                if (strODBCDriver.Contains(SQL_SERVER_FILTER) || strODBCDriver.Contains(SQL_NATIVE_CLIENT_FILTER) 
                    || strODBCDriver.Contains(SQL_SERVER_NATIVE_CLIENT_FILTER) || strODBCDriver.Contains(ORACLE_FILTER))
                {
                    arrFilteredDrivers.Add(strODBCDriver);
                } 
            }
            return arrFilteredDrivers.ToArray();
        }

      
        /// <summary>
        /// upgrade the selected RISKMASTER base database(s)
        /// </summary>
        public static void UpgradeRMBase(bool bIsHPUpgrade, bool bInsertViewsOnly)
        {
            const string STRING_DELIMITER = ",";

            //run dBUpgrade on selected datasources
            foreach (KeyValuePair<string,int> kvp in DisplayDBUpgrade.dictPassedItems)
            {
                int iOverall = 0;

                if (!_isSilent)
                {
                    //reset the progressbars on wizard processing page for each selected dB
                    frmWizard.SetOverallProgressBarProperties(0);
                    frmWizard.SetCurrentProgressBarProperties(0);
                    frmWizard.UpdateCurrentProgressBar(0);
                    frmWizard.UpdateOverallProgressBar(0);
                }

                string[] arrSQLFiles = null;
               
                arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.BASESCRIPTS, STRING_DELIMITER);

                if (arrSQLFiles.Length > 0)
                {
                    DisplayDBUpgrade.sODBCName = kvp.Key;
                    DisplayDBUpgrade.g_iDSNID = kvp.Value;

                    if (!bInsertViewsOnly)
                    {
                        using (var dbDSNRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(DisplayDBUpgrade.g_iDSNID)))
                        {
                            while (dbDSNRdr.Read())
                            {
                                //var blnDBExists = true;

                                //form a connection string so we can connect and do rest of the upgrade
                                DisplayDBUpgrade.g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString()), RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));
                                DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_sConnectString);

                                //deal with Document DB (if it exists in the security db)
                                string sDocDB = String.Empty;
                                string sDocUserId = String.Empty;

                                string sDocConnString = dbDSNRdr["GLOBAL_DOC_PATH"].ToString();

                                if (!String.IsNullOrEmpty(sDocConnString))
                                {
                                    if (sDocConnString.Contains("Driver"))
                                    {
                                        DisplayDBUpgrade.g_DocumentConnectString = RISKMASTERScripts.DecryptDocumentConnectionString(sDocConnString, ref sDocUserId, ref sDocDB);
                                
                                        DisplayDBUpgrade.g_DocUserId = sDocUserId;
                                        DisplayDBUpgrade.g_DocDB = sDocDB;
                                        DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_DocumentConnectString);

                                        UpgradeRMDocument(); //process document scripts
                                    }
                                }

                                //get the database make
                                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                                //get the database release and version information
                                DisplayDBUpgrade.gsDBRelease = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                                DisplayDBUpgrade.gsDBVersion = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                                if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                                {
                                    DisplayDBUpgrade.g_bOrgSecOn = true;
                                }
                            }
                        }

                        //create the necessary log file
                        CreateLogFile();

                        //MJP - this hides the password so it doesn't record it into the logfile...
                        string sPasswordHiddenConnString = String.Empty;
                        string[] result = DisplayDBUpgrade.g_sConnectString.Split(new string[] { "PWD" }, StringSplitOptions.None);
                        sPasswordHiddenConnString = String.Format(@"{0}PWD=*** NOT SHOWN ***;", result[0]);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "DSN ID: " + DisplayDBUpgrade.g_iDSNID + "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connection String: " + sPasswordHiddenConnString + "\r\n");

                       
                        if (!_isSilent)
                        {
                            frmWizard.UpdateDBName(kvp.Key);
                           // frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);

                            if (bIsHPUpgrade)
                            {
                                frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                            }
                            else
                            {
                                frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                            }
                        }

                        UpdateFunctions.ConnectedDB = "RMX Base DB";
                        //Base scripts
                        if (arrSQLFiles.Length > 0)
                        {
                            string sDbchkUpgradeDatabase = frmWizard._sDbchkUpgradeDatabase.ToString().ToUpper();

                            if (sDbchkUpgradeDatabase.Equals("TRUE"))
                                {
                                    ExecuteSqlScripts(ref iOverall, arrSQLFiles);
                                } 
                        }
                    }
                }
            }
        }
       
        private static void ExecuteSqlScripts(ref int  iOverall, string[] arrSQLFiles)
        {
            foreach (string strSQLFile in arrSQLFiles)
            {
                iOverall++;

                // Enhancement Mits id:  -- Govind - Start 

                if(strSQLFile.Equals("RM_X_base_Tables.sql"))
                {
                    //set sBaseLogType to Tables
                    DisplayDBUpgrade.sBaseLogType = "Tables";
                    //Create seprate Logfile here for Tables
                    CreateLogFile();

                }

                if (strSQLFile.Equals("RM_X_base_Indexes.sql"))
                {
                    //set sBaseLogType to Tables
                    DisplayDBUpgrade.sBaseLogType = "Indexes";
                    //Create seprate Logfile here for Tables
                    CreateLogFile();

                }

                // Enhancement Mits id:  -- Govind - End
 

                if (!_isSilent)
                {
                    //display name of script on the update form
                    frmWizard.UpdateScriptName(strSQLFile);
                    frmWizard.UpdateCurrentProgressBar(0);
                }
                else
                {
                    //Console.WriteLine("");
                    //Console.WriteLine("Script: " + strSQLFile);
                }

                if (DisplayDBUpgrade.g_dbMake == 4 && strSQLFile == "oracle-maindb-final.sql")
                {
                    bool bExecute = false;
                    bExecute = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_sConnectString, strSQLFile, DisplayDBUpgrade.g_dbMake, _isSilent);
                }
                else
                {
                //execute each individual SQL file against the database
                UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, strSQLFile, String.Empty, _isSilent);
                }
                if (!_isSilent)
                {
                    frmWizard.UpdateOverallProgressBar(iOverall);
                }
            }
            
        }
      
        //Mgaba2: MITS 23998: Getting the information whether DBUpgrade has been run on Document Storage DB to remove "\" from file name
        private static int GetDocStoreRemoveSlashFlag()
        {
            string sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'DOC_STORE_REM_SLASH'";
            return (ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL));
        }

        //Mgaba2: MITS 23998 :setting the information that DBUpgrade has been run on Document Storage DB to remove "\" from file name
        private static void SetDocStoreRemoveSlashFlag()
        {
            string sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = -1 WHERE PARM_NAME = 'DOC_STORE_REM_SLASH'";
            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
        }

        /// <summary>
        /// upgrades the RISKMASTER document database
        /// </summary>
        public static void UpgradeRMDocument()
        {
            int iDB_Make = -1;
            string sSQL = String.Empty;
            string strDataType = String.Empty;
            string strMaxLength = String.Empty;

            const string DOC_TABLE  = "DOCUMENT_STORAGE";
            const string DOC_COLUMN = "DOC_BLOB";
            const string DOC_TYPE   = "VARBINARY(MAX)";
            bool bInsertStoredProc = false; //Mgaba2: MITS 23998 
            int iDocStoreRemoveSlashFlag = 0; //Mgaba2: MITS 23998 

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_DocumentConnectString);
            iDB_Make = DisplayDBUpgrade.g_dbMake;

            string strQuery = RISKMASTERScripts.GetDataTypeAndLength(DOC_TABLE, DOC_COLUMN, DisplayDBUpgrade.g_DocumentConnectString, DisplayDBUpgrade.g_DocUserId);

            System.Data.DataSet dsObject = ADONetDbAccess.GetDataSet(DisplayDBUpgrade.g_DocumentConnectString, strQuery);
            foreach (System.Data.DataRow drObject in dsObject.Tables[0].Rows)
            {
                strDataType = drObject["c_datatype"].ToString();
            }
            dsObject.Dispose();

            //Mgaba2: MITS 23998 
            iDocStoreRemoveSlashFlag = GetDocStoreRemoveSlashFlag();         

            switch (iDB_Make)
            {
                case 1:                 // DBMS_IS_SQLSRVR  = 1
                    switch (strDataType)
                    {
                        case "varbinary":
                            break;
                        default:
                            sSQL = String.Format("ALTER TABLE {0} ALTER COLUMN {1} {2}", DOC_TABLE, DOC_COLUMN, DOC_TYPE);
                            ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_DocumentConnectString, sSQL);
                            break;
                    }
                    //Mgaba2: MITS 23998 :Start
                    if (iDocStoreRemoveSlashFlag == 0)
                    {
                        try
                        {
                            //execute each individual SQL file against the database
                            bInsertStoredProc = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME_SQL.sql", iDB_Make, _isSilent);
                            if (bInsertStoredProc)
                            {
                                ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME");
                                SetDocStoreRemoveSlashFlag();
                            }
                        }
                        catch (Exception ex)
                        {
                            string strLogFilePath = String.Format(@"{0}\\JurRules_dBUpgrade.log", Application.StartupPath);
                            DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                        }
                    }
                    //Mgaba2: MITS 23998 :End
                    break;
                case 4:                 // DBMS_IS_ORACLE   = 4
                    switch (strDataType)
                    {
                        case "blob":
                            break;
                        default:
                            try
                            {
                                sSQL = String.Format("ALTER TABLE {0} ADD {1} {2}", DOC_TABLE, "DOC_BLOB_TEMP", "BLOB");
                                ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_DocumentConnectString, sSQL);
                            }
                            catch (Exception)
                            {
                            }

                            //execute SQL file against the database
                            string sSQLFile = "USP_CLOB_BLOB_Conversion_Oracle.SQL";
                            bool bExecuteSP = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, sSQLFile, iDB_Make, _isSilent);

                            //execute stored procedure
                            //if (bExecuteSP)
                            //{
                                try
                                {
                                    string strExecuteSP = String.Format("BEGIN {0}; COMMIT; END;", "USP_CLOB_BLOB_CONVERSION");
                                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_DocumentConnectString, strExecuteSP);
                                }//try
                                catch (Exception ex)
                                {
                                    string strLogFilePath = String.Format(@"{0}\\JurRules_dBUpgrade.log", Application.StartupPath);
                                    DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                                }
                            //}//if
                            break;
                    }
                    //Mgaba2: MITS 23998 :Start
                    if (iDocStoreRemoveSlashFlag == 0)
                    {                        
                        try
                        {
                            //execute each individual SQL file against the database
                            bInsertStoredProc = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME_ORACLE.sql", iDB_Make, _isSilent);
                            if (bInsertStoredProc)
                            {                                
                                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_DocumentConnectString, string.Format("BEGIN USP_REM_SLASH_FILENAME('{0}'); COMMIT; END;", DisplayDBUpgrade.g_DocUserId.ToUpper()));
                                SetDocStoreRemoveSlashFlag();
                            }
                        }
                        catch (Exception ex)
                        {
                            string strLogFilePath = String.Format(@"{0}\\JurRules_dBUpgrade.log", Application.StartupPath);
                            DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                        }
                    }
                    //Mgaba2: MITS 23998 :End
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get Database ID
        /// </summary>
        /// <returns>DatabaseId</returns>
        private static int GetDBId()
        {
            string sMainDbName = string.Empty;
            string sSql = string.Empty;
            int iDbId = 0;
            int iPos1;
            int iPos2;
            int iDB_Make = -1;
            string sConnstringTemp = string.Empty;
            DbReader objDbReader = null;

            try
            {
                 iDB_Make = DisplayDBUpgrade.g_dbMake;
                 sConnstringTemp = DisplayDBUpgrade.g_sConnectString;
                 switch (iDB_Make)
                 {
                     case 1://SQL server                        

                         iPos1 = sConnstringTemp.IndexOf("Database=");
                         iPos2 = sConnstringTemp.IndexOf("UID=");
                         sMainDbName = sConnstringTemp.Substring(iPos1 + 9, iPos2 - iPos1 - 10);
                         sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + sMainDbName + "'";
                         break;
                     case 4://Oracle Server

                         iPos1 = sConnstringTemp.IndexOf("UID=");
                         iPos2 = sConnstringTemp.IndexOf("PWD=");
                         sMainDbName = sConnstringTemp.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                          sSql = "SELECT USER_ID FROM ALL_USERS WHERE USERNAME='" + sMainDbName.ToUpper() + "'";
                         break;
                     default:
                         break;
                 }

                 using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSql))
                 {

                     if (objDbReader != null)
                     {
                         while (objDbReader.Read())
                         {
                             if (iDB_Make ==1)
                             {
                                 iDbId = objDbReader.GetInt32("DATABASE_ID");
                             }
                             else
                             {
                                 iDbId = objDbReader.GetInt32("USER_ID");
                             }
                         }

                     }
                 }

                

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Getting DB Id - " + ex.Message);
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();

            }

            return iDbId;
        }

        //nadim added for updating stored proc with params-start
        public static bool  GetprimarykeySetting()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            //int iPrimaryKeysetting = 0;
            bool bSetting = false;
           
            try
            {
                sSQL = "SELECT PRIMARY_KEY_TASK FROM DATA_SOURCE_TABLE WHERE DSNID = "+DisplayDBUpgrade.g_DsnId;
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_SecConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                           
                            bSetting = Convert.ToBoolean(objDbReader.GetInt("PRIMARY_KEY_TASK"));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task - " + ex.Message);
            }
           
            return bSetting;
        }

        //nadim added for getting confidential flag setting -start
        /// <summary>
        /// etting confidential flag setting
        /// </summary>

        public static int GetConfFlagSetting()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;            
            int iConfRec = 0;

            try
            {
                sSQL = "SELECT CONF_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + DisplayDBUpgrade.g_DsnId;
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_SecConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                          
                             iConfRec = objDbReader.GetInt32("CONF_FLAG");
                            
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating triggers - " + ex.Message);
            }

            return iConfRec;
        }
        //nadim added for getting confidential flag setting -end

        public static void UpdateprimarykeySetting()
        {
            
            string sSQL = string.Empty;


            sSQL = "UPDATE DATA_SOURCE_TABLE SET PRIMARY_KEY_TASK =-1 WHERE DSNID= " + DisplayDBUpgrade.g_DsnId;
            try
            {
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_SecConnectString, sSQL);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task - " + ex.Message);
            }

           
        }
        //nadim added for rebuilding indexes-end

        private static void UpgradePKInHD()
        {
            StringBuilder sbSql = null;
            DbReader objDbReader = null;

            DisplayDBUpgrade.sODBCName = "PK_FLAG_UPDATE_TASK";
            CreateLogFile();

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            try
            {
                sbSql = new StringBuilder();
                if (DisplayDBUpgrade.g_dbMake == 4)//Oracle
                {
                    sbSql.Length = 0;
                    sbSql.Append("SELECT UC.TABLE_NAME,UCC.COLUMN_NAME,UCC.CONSTRAINT_NAME,HT.TABLE_ID ");
                    sbSql.Append(" FROM USER_CONS_COLUMNS UCC INNER JOIN USER_CONSTRAINTS UC ON UCC.CONSTRAINT_NAME = UC.CONSTRAINT_NAME");
                    sbSql.Append(" INNER JOIN HIST_TRACK_TABLES HT ON HT.TABLE_NAME=UC.TABLE_NAME ");
                    sbSql.Append(" WHERE UC.CONSTRAINT_TYPE = 'P'");
                }
                else//Sql
                {
                    sbSql.Length = 0;
                    sbSql.Append("SELECT TC.TABLE_NAME,CC.COLUMN_NAME,TC.CONSTRAINT_NAME,HT.TABLE_ID ");
                    sbSql.Append(" FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CC");
                    sbSql.Append(" INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON TC.CONSTRAINT_NAME = CC.CONSTRAINT_NAME ");
                    sbSql.Append(" INNER JOIN HIST_TRACK_TABLES HT ON HT.TABLE_NAME = TC.TABLE_NAME");
                    sbSql.Append(" WHERE TC.CONSTRAINT_TYPE = 'PRIMARY KEY' ");
                }

                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sbSql.ToString()))
                {
                    while (objDbReader.Read())
                    {
                        sbSql.Length = 0;
                        sbSql.Append(" UPDATE HIST_TRACK_DICTIONARY SET PRIMARY_KEY_FLAG = -1 ");
                        sbSql.AppendFormat(" WHERE TABLE_ID ={0}  AND COLUMN_NAME = '{1}'", objDbReader.GetInt32("TABLE_ID"), objDbReader.GetString("COLUMN_NAME"));
                        try
                        {
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sbSql.ToString());
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error:Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task - " + ex.Message);
                        }

                    }
                }




            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task - " + ex.Message);
            }


            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task Finished - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

        }





        /// <summary>
        /// start upgrade process
        /// </summary>
        public static void UpgradeRMDatabase(bool bIsCustom, bool bIsHPUpgrade, bool bIsSilent)
        {
            bool bIsError = false;
            string strSQLFile = String.Empty;
            string strException = String.Empty;
            string strStackTrace = String.Empty;
            string strRunningScript = String.Empty;
            string strErrConnType = String.Empty;
            string strErrConnString = String.Empty;
            StringBuilder strErrorMessage = new StringBuilder();

            bool bRunPrimarykeys = (DisplayDBUpgrade.RunPrimarykey.ToUpper() == bool.TrueString.ToUpper()) ? true : false;
            _isSilent = bIsSilent;

            try
            {
                if (bIsCustom)
                {
                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeCustom";
                    //Console.WriteLine(strRunningScript);
                    UpgradeCustom(); //process customized scripts
                }
                else
                {
                    DisplayDBUpgrade.sBaseLogType = ""; //Govind

                    
                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMBase";
                    
                    strErrConnType = "RMX Base DB";
                    DisplayDBUpgrade.sBaseLogType = "Default"; //Govind

                    
                    strErrConnString = DisplayDBUpgrade.g_sConnectString;
                    
                    UpgradeRMBase(bIsHPUpgrade, false); 
                }
            }
            catch (System.Data.Odbc.OdbcException exODBC)
            {
                bIsError = true;
                strException = exODBC.Message;
                strStackTrace = exODBC.StackTrace;
            }
            catch (Exception ex)
            {
                bIsError = true;

                //Format of the initialization string does not conform to specification starting at index 0.
                Regex regex = new Regex(@"^(.*)(Format\sof\sthe\sinitialization\sstring)(.*)$", RegexOptions.IgnoreCase);
                Match match = regex.Match(ex.Message);

                if (match.Success)
                {
                    strException = String.Format("Connection failed.<br />Likely reason: connectionStrings.config has an invalid {0} setting: {1}", strErrConnType, strErrConnString);
                }
                else
                {
                    strException = ex.Message;
                }
                
                strStackTrace = ex.StackTrace;
            }

            string strLogFilePath = String.Format(@"{0}\\JurRules_dBUpgrade.log", Application.StartupPath);

            if (bIsError)
            {
                string sErrorMessage = "Please check <font color='#000000'>JurRules_dBUpgrade.log</font> file for errors and " + 
                                       "contact RISKMASTER Support.<br /><br />Press Cancel to close program.";

                strErrorMessage.AppendFormat("Exception Error/StackTrace: {0}", strStackTrace);
                strErrorMessage.AppendLine();
                strErrorMessage.AppendLine();

                if (_isSilent)
                {
                    Console.Error.WriteLine("Error: " + strException);
                }
                else
                {
                    frmWizard.ErrorDisplay(sErrorMessage, "error", DevComponents.DotNetBar.eWizardButtonState.False, strException);
                }

                DisplayDBUpgrade.bStop = true;
            }

            strErrorMessage.AppendLine("dBUgrade Finished: " + DateTime.Now);
            strErrorMessage.AppendLine();
            DisplayDBUpgrade.LogErrors(strLogFilePath, strErrorMessage.ToString());
        }

        /// <summary>
        /// creates the error log file (logs all errors that occur on [FORGIVE] lines)
        /// </summary>
        public static void CreateLogFile()
        {
            //get the path to the Error Log file
            string strLogFilePath = DisplayDBUpgrade.GetLogFilePath();

            //append the required text to the Error Log file
            File.AppendAllText(strLogFilePath, "Update " + DisplayDBUpgrade.sODBCName + " Database to RISKMASTER " + DisplayDBUpgrade.DBTARGETVER + " " + DateTime.Now.ToShortDateString());

            //insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }



        
        /// <summary>
        /// upgrades RISKMASTER database(s) with customized scripts
        /// </summary>
        public static void UpgradeCustom()
        {
            const string STRING_DELIMITER = ",";

            int i = DisplayDBUpgrade.dictPassedItems.Count;

            if (_isSilent)
            {
                Console.WriteLine("# of items in dictPassedItems: " + i.ToString());
            }
                //run dBUpgrade on selected datasources
                foreach (KeyValuePair<string, int> kvp in DisplayDBUpgrade.dictPassedItems)
                {
                    if (!_isSilent)
                    {
                        //reset the progressbars on wizard processing page for each selected dB
                        frmWizard.SetOverallProgressBarProperties(0);
                        frmWizard.SetCurrentProgressBarProperties(0);
                        frmWizard.UpdateCurrentProgressBar(0);
                        frmWizard.UpdateOverallProgressBar(0);
                    }
                    else
                    {
                        Console.WriteLine("key: " + kvp.Key + " | value: " + kvp.Value);
                    }

                    //get the list of custom SQL files
                    string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.CUSTOMSCRIPTS, STRING_DELIMITER);

                    int iOverall = 0;

                    if (arrSQLFiles.Length > 0)
                    {
                        DisplayDBUpgrade.sODBCName = kvp.Key;
                        DisplayDBUpgrade.g_iDSNID = kvp.Value;

                        Console.WriteLine("g_SecConnectString: " + DisplayDBUpgrade.g_SecConnectString);

                        using (var dbDSNRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(DisplayDBUpgrade.g_iDSNID)))
                        {
                            while (dbDSNRdr.Read())
                            {
                                //form a connection string so we can connect and do rest of upgrade
                                DisplayDBUpgrade.g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString()), RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));

                                DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_sConnectString);

                                Console.WriteLine("g_sConnectString: " + DisplayDBUpgrade.g_sConnectString);

                                //get the database make
                                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                                //get the database release and version information
                                DisplayDBUpgrade.gsDBRelease = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                                DisplayDBUpgrade.gsDBVersion = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                                if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                                {
                                    DisplayDBUpgrade.g_bOrgSecOn = true;
                                }
                            }
                        }

                        //create the necessary log file
                        CreateLogFile();

                        //MJP - this hides the password so it doesn't record into the logfile...
                        string sPasswordHiddenConnString = String.Empty;
                        string[] result = DisplayDBUpgrade.g_sConnectString.Split(new string[] { "PWD" }, StringSplitOptions.None);
                        sPasswordHiddenConnString = String.Format(@"{0}PWD=*** NOT SHOWN ***;", result[0]);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "DSN ID: " + DisplayDBUpgrade.g_iDSNID + "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connection String: " + sPasswordHiddenConnString + "\r\n");
                        if (!_isSilent)
                        {
                            frmWizard.UpdateDBName(kvp.Key);
                            frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                        }

                        foreach (string strSQLFile in arrSQLFiles)
                        {
                            iOverall++;

                            if (!_isSilent)
                            {
                                //display name of script on the update form
                                frmWizard.UpdateScriptName(strSQLFile);
                                frmWizard.UpdateCurrentProgressBar(0);
                            }

                            //execute each individual SQL file against the database
                            UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, strSQLFile, String.Empty, _isSilent);

                            if (!_isSilent)
                            {
                                frmWizard.UpdateOverallProgressBar(iOverall);
                            }
                        }
                    }
                    else
                    {
                        if (!_isSilent)
                        {
                            string sErrorMessage = "Custom scripts were not found.<br /><br />Contact RISKMASTER for further support.<br /><br />Press Cancel to close program.";
                            frmWizard.ErrorDisplay(sErrorMessage, "error", DevComponents.DotNetBar.eWizardButtonState.False, String.Empty);
                        }
                        else
                        {
                            string sErrorMessage = "Custom scripts were not found.\n\nContact RISKMASTER for further support.\n";
                            Console.Error.WriteLine(sErrorMessage);
                        }

                        DisplayDBUpgrade.bStop = true;
                        break;
                    }
                }
            
        }

    }
}

