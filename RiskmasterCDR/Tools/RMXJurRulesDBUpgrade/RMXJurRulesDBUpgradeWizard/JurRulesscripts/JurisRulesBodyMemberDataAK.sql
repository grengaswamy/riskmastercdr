;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;#################################################################################
;#################################################################################
;Alaska
[ASSIGN %%1=ADD_JURISDICTION(AK,Alaska,0,0,0,0,0,0)]
;Title 23, Chapter 30 Article Five 
;Sec. 23.30.190. Compensation for permanent partial impairment; rating guides.
;(a) In case of impairment partial in character but permanent in quality, and not resulting
;in permanent total disability, the compensation is $177,000 multiplied by the employee's 
;percentage of permanent impairment of the whole person.
;   The percentage of permanent impairment of the whole person is the percentage of impairment
;to the particular body part, system, or function converted to the percentage of impairment
;to the whole person as provided under (b) of this section.
;   The compensation is payable in a single lump sum, except as otherwise provided in AS 23.30.041, 
;but the compensation may not be discounted for any present value considerations. 
;(b) All determinations of the existence and degree of permanent impairment shall be made strictly and 
;solely under the whole person determination as set out in the American Medical Association Guides to the 
;Evaluation of Permanent Impairment, except that an impairment rating may not be rounded to the next five percent. 
;The board shall adopt a supplementary recognized schedule for injuries that cannot be rated by use of 
;the American Medical Association Guides.
