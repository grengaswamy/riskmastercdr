;*********************************************************************************
;California
[ASSIGN %%1=ADD_JURISDICTION(CA,California,0,0,0,0,0,0)]

;*********************************************************************************
[ASSIGN %%2=QUERY(SELECT CODES.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY WHERE SYSTEM_TABLE_NAME = 'SEX_CODE' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND (CODES_TEXT.CODE_DESC = 'Male' OR CODES_TEXT.CODE_DESC = 'MALE'))]
[ASSIGN %%3=QUERY(SELECT CODES.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY WHERE SYSTEM_TABLE_NAME = 'SEX_CODE' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND (CODES_TEXT.CODE_DESC = 'Female' OR CODES_TEXT.CODE_DESC = 'FEMALE'))]

[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,1,78.75)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,2,77.81)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,3,76.84)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,4,75.86)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,5,74.88)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,6,73.90)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,7,72.91)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,8,71.93)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,9,70.94)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,10,69.65)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,11,68.96)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,12,67.97)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,13,66.98)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,14,65.99)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,15,65.01)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,16,64.03)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,17,63.06)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,18,62.08)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,19,61.11)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,20,60.14)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,21,59.17)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,22,58.20)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,23,57.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,24,56.26)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,25,55.29)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,26,54.32)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,27,53.35)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,28,52.38)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,29,51.41)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,30,50.44)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,31,49.47)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,32,48.51)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,33,47.54)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,34,46.58)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,35,45.62)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,36,44.66)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,37,43.71)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,38,42.75)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,39,41.80)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,40,40.85)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,41,39.91)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,42,38.97)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,43,38.03)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,44,37.09)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,45,36.17)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,46,35.24)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,47,34.32)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,48,33.41)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,49,32.51)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,50,31.61)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,51,30.72)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,52,29.83)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,53,28.96)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,54,28.09)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,55,27.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,56,26.37)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,57,25.53)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,58,24.69)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,59,23.86)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,60,23.05)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,61,22.24)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,62,21.45)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,63,20.66)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,64,19.88)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,65,19.12)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,66,18.36)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,67,17.61)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,68,16.87)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,69,16.15)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,70,15.43)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,71,14.73)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,72,14.05)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,73,13.39)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,74,12.74)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,75,12.11)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,76,11.49)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,77,10.88)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,78,10.29)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,79,9.71)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,80,9.15)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,81,8.61)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,82,8.09)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,83,7.59)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,84,7.11)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,85,6.66)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,86,6.22)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,87,5.81)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,88,5.43)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,89,5.06)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,90,4.72)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,91,4.40)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,92,4.12)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,93,3.86)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,94,3.63)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,95,3.40)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,96,3.20)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,97,3.01)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,98,2.84)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,99,2.68)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,100,2.52)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,101,2.37)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,102,2.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,103,2.10)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,104,1.97)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,105,1.84)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,106,1.72)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,107,1.61)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,108,1.50)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,109,1.40)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%3,0,79.19)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,0,72.53)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,1,72.18)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,2,71.24)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,3,70.27)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,4,69.30)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,5,68.32)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,6,67.34)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,7,66.36)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,8,65.37)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,9,64.39)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,10,63.4)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,11,62.41)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,12,61.42)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,13,60.43)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,14,59.45)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,15,58.49)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,16,57.55)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,17,56.51)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,18,55.69)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,19,54.77)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,20,53.86)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,21,52.94)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,22,52.02)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,23,51.11)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,24,50.20)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,25,49.28)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,26,48.37)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,27,47.48)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,28,46.54)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,29,45.63)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,30,44.72)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,31,43.82)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,32,42.91)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,33,42.01)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,34,41.12)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,35,40.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,36,39.34)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,37,38.46)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,38,37.58)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,39,36.70)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,40,35.82)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,41,34.95)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,42,34.07)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,43,33.20)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,44,32.33)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,45,31.46)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,46,30.59)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,47,29.73)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,48,28.87)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,49,28.02)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,50,27.18)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,51,26.34)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,52,25.51)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,53,24.69)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,54,23.88)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,55,23.07)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,56,22.28)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,57,21.50)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,58,20.72)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,59,19.97)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,60,19.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,61,18.50)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,62,17.79)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,63,17.09)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,64,16.41)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,65,15.74)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,66,15.07)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,67,14.42)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,68,13.78)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,69,13.15)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,70,12.53)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,71,11.93)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,72,11.35)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,73,10.79)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,74,10.24)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,75,9.25)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,76,9.20)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,77,8.71)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,78,8.23)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,79,7.76)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,80,7.31)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,81,6.88)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,82,6.48)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,83,6.10)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,84,5.73)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,85,5.38)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,86,5.04)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,87,4.73)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,88,4.43)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,89,4.16)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,90,3.90)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,91,3.66)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,92,3.45)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,93,3.25)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,94,3.08)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,95,2.92)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,96,2.77)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,97,2.63)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,98,2.50)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,99,2.37)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,100,2.25)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,101,2.13)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,102,2.02)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,103,1.91)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,104,1.81)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,105,1.71)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,106,1.61)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,107,1.52)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,108,1.43)]
[ASSIGN %%4=ADD_WCP_CDC_LIFE(,1989-1991,%%1,%%2,109,1.35)]
;
