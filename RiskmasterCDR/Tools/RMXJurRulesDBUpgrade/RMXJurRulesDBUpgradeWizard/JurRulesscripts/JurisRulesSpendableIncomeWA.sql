;*********************************************************************************
;Washington, State of 
[ASSIGN %%2=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'WA')]
;*********************************************************************************
[ASSIGN %%3=ADD_CODE(S,1033,Single,TAX_STATS_WA_CODE,0)]
[ASSIGN %%5=ADD_CODE(M,1033,Married,TAX_STATS_WA_CODE,0)]
;*********************************************************************************
;Washington does not publish a Spendable Income Table
;They built percentages into the Statutes based on martial status and children
; EffectiveDateDtg,JurisRowID,Exemptions,Percentage,MinimumDollars,MaxSAWWPercentage
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,0,60,185.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,1,62,222.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,2,64,253.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,3,66,276.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,4,68,299.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%3,5,70,322.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,0,65,215.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,1,67,252.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,2,69,283.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,3,71,306.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,4,73,329.00,120.00)]
[ASSIGN %%1=ADD_WCP_TTD_WA(19960630,%%2,%%5,5,75,352.00,120.00)]

