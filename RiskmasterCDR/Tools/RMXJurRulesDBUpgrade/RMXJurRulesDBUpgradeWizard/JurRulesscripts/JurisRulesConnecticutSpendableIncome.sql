;*********************************************************************************
;Connecticut
[ASSIGN %%2=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'CT')]
;*********************************************************************************
[ASSIGN %%3=ADD_CODE(S,1033,Single,TAX_STATS_CT_CODE,0)]
[ASSIGN %%4=ADD_CODE(HOH,1033,Head of Household,TAX_STATS_CT_CODE,0)]
[ASSIGN %%5=ADD_CODE(MJ,1033,Married Filing Joint,TAX_STATS_CT_CODE,0)]
[ASSIGN %%6=ADD_CODE(MS,1033,Married Filing Separate,TAX_STATS_CT_CODE,0)]
;*********************************************************************************