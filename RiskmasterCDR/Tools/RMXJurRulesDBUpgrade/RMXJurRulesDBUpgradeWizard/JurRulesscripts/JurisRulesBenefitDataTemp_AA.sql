;##################################################################################
;WCP_RULE_TDD
[ASSIGN %%1= QUERY(SELECT MAX(TABLE_ROW_ID) + 1 FROM WCP_RULE_TTD)]
[FORGIVE] UPDATE GLOSSARY SET NEXT_UNIQUE_ID = %%1 WHERE SYSTEM_TABLE_NAME = 'WCP_RULE_TTD'
[ASSIGN %%2=ADD_CODE(Y,1033,Yes,YES_NO,0)]
[ASSIGN %%3=ADD_CODE(N,1033,No,YES_NO,0)]
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9)) %%2(Yes in California)
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;                                                                                   1   1    1  1   1 1 1  1  1 1 1
;                               1   2        3        4     5  6      7     8   9   0   1    2      3 4 5  6  7 8 9
;*********************************************************************************
;Alaska
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'AK')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101,20031231,000.00,%%1, 814.00,000.00,%%3,%%2,%%2,,0.8000, , ,0.0, , , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,        ,000.00,%%1, 832.00,000.00,%%3,%%2,%%2,,0.8000, , ,0.0, , , ,,%%2,%%2)]
;*********************************************************************************
;Alabama
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'AL')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010601,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667, , ,0.0, , , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020601,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667, , ,0.0, , , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030601,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667, , ,0.0, , , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040601,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667, , ,0.0, , , ,,%%2,%%2)]
;*********************************************************************************
;Arkansas
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'AR')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,        ,000.00,%%1, 440.00,000.00,%%3,%%2,%%2,,0.6667, ,,0.0, , , ,,%%2,%%2)]
;*********************************************************************************
;Arizona
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'AZ')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19910701,        ,000.00,%%1, 327.94,000.00,%%3,%%2,%%2,,0.6667,,,0.0, , , ,,%%2,%%2)]
;*********************************************************************************
;California
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'CA')]
;*********************************************************************************
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19830101,19831231, 84.00,%%1, 294.00,196.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19840101,19891231,112.00,%%1, 336.00,224.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19900101,19901231,126.00,%%1, 399.00,266.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19910101,19940630,126.00,%%1, 504.00,336.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19940701,19950630,126.00,%%1, 609.00,406.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19950701,19960630,126.00,%%1, 672.00,448.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19960701,20021231,126.00,%%1, 735.00,490.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101,20031231,126.00,%%1, 903.00,602.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,20041231,126.00,%%1,1092.00,728.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050101,20051231,126.00,%%1,1260.00,840.00,%%3,%%2,%%2,,0.6667, , , , , , ,,%%3,%%3)]
;*********************************************************************************************************
;Colorado
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'CO')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701,,0,%%1, 674.59,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Connecticut
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'CT')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 838.00,0,%%3,%%2,%%2,,0.7500,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Delaware
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'DE')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010620,,0,%%1, 469.10,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020614,,0,%%1, 491.57,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030604,,0,%%1, 506.81,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040521,,0,%%1, 523.83,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;FLorida
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'FL')]
;                                                                                 1  1  1      1 1 1 1  1  1 1 2
;                               1   2        3 4       5   6       7      8   9   0  1  2      3 4 5 6  7  8 9 0
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010101, , 000.00,%%1, 000.00,541.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101, , 000.00,%%1, 000.00,571.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101, , 000.00,%%1, 000.00,594.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101, , 000.00,%%1, 000.00,626.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050101, , 000.00,%%1, 000.00,651.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20060101, , 000.00,%%1, 000.00,683.00,%%3,%%2,%%2, ,0.6667, , , ,0.0, , , ,%%3,%%3)]
;*********************************************************************************
;Georgia
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'GA')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19990701,,0,%%1, 350.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000701,,0,%%1, 375.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010701,,0,%%1, 400.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030701,,0,%%1, 425.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701,,0,%%1, 450.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Hawaii
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'HI')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Idaho
[ASSIGN %%1=ADD_JURISDICTION(ID,Idaho,0,0,0,0,0,0)]
;Idaho does not use the generic rule--see Pennsylvania for a simialiar jurisdictions
;*********************************************************************************
;Illinois
[ASSIGN %%1=ADD_JURISDICTION(IL,Illinois,0,0,0,0,0,0)]
;jtodd22 09/15/2005--Illinois is in a seperate table
;*********************************************************************************
;Indiana
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'IN')]
;                               1   2        3 4      5   6      7      8   9   10  11    12     13        14
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000701, ,050.00,%%1,762.00,508.00,%%3,%%3,%%2,00.00,0.6667,254000.00,000,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010701, ,050.00,%%1,822.00,548.00,%%3,%%3,%%2,00.00,0.6667,274000.00,000,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701, ,050.00,%%1,882.00,588.00,%%3,%%3,%%2,00.00,0.6667,294000.00,000,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
[ASSIGN %%2=ADD_CODE(Y,1033,Yes,YES_NO,0)]
[ASSIGN %%3=ADD_CODE(N,1033,No,YES_NO,0)]
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9)) %%2(Yes in California)
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;***********************************************************************************************************
;Iowa
;Spendable Income Jurisdiction
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'IA')]
;                               1   2        3 4      5   6 7    8   9   10  11 12
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701, ,000.00,%%1,0,1103,%%3,%%3,%%3, ,0.8000,0,,,0.0, , ,,%%2,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030701, ,000.00,%%1,0,1133,%%3,%%3,%%3, ,0.8000,0,,,0.0, , ,,%%2,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701, ,000.00,%%1,0,1173,%%3,%%3,%%3, ,0.8000,0,,,0.0, , ,,%%2,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701, ,000.00,%%1,0,1226,%%3,%%3,%%3, ,0.8000,0,,,0.0, , ,,%%2,%%3)]
;****************************************************************************************************************
;Kansas
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'KS')]
;                               1   2        3 4      5   6       7      8   9   10  11 12
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000701, ,025.00,%%1,0000.00,401.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010701, ,025.00,%%1,0000.00,417.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701, ,025.00,%%1,0000.00,432.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030701, ,025.00,%%1,0000.00,440.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701, ,025.00,%%1,0000.00,449.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701, ,025.00,%%1,0000.00,467.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
;******************************************************************************
;Kentucky
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'KY')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 000.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Louisiana
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'LA')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 000.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Massachusetts
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'MA')]
;                               1   2        3 4      5   6      7      8   9   10  11    12     13     14
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20001001, ,000.00,%%1,000.00,000.00,%%3,%%2,%%2,00.00,0.6000,000.00,156,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Maryland
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'MD')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19900101,,000.00,%%1, 432.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19910101,,000.00,%%1, 452.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19920101,,000.00,%%1, 475.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19930101,,000.00,%%1, 494.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19940101,,000.00,%%1, 510.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19950101,,000.00,%%1, 525.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19960101,,000.00,%%1, 540.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19970101,,000.00,%%1, 553.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19980101,,000.00,%%1, 573.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19990101,,000.00,%%1, 602.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 631.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010101,,000.00,%%1, 668.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101,,000.00,%%1, 700.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101,,000.00,%%1, 722.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,,000.00,%%1, 740.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Maine
;Spendable Income Jurisdiction
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'ME')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.8000,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;Michigan
;Spendable Income Jurisdiction
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'MI')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.8000,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;Minnesota
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'MN')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20021001,,0,%%1, 750.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;[ASSIGN %%2=ADD_CODE(Y,1033,Yes,YES_NO,0)]
;[ASSIGN %%3=ADD_CODE(N,1033,No,YES_NO,0)]
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9)) %%2(Yes in California)
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;************************************************************************************************************
;Mississippi
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'MS')]
;                               1   2        3 4    5   6      7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20050101, ,0.0,%%1, 000.00,351.14,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,158013.00,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20040101, ,0.0,%%1, 000.00,341.11,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,153499.50,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20030101, ,0.0,%%1, 000.00,331.06,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,148977.00,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20020101, ,0.0,%%1, 000.00,322.90,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,145305.00,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20010101, ,0.0,%%1, 000.00,316.46,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,142407.00,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20000101, ,0.0,%%1, 000.00,303.35,%%3,%%3,%%3,00.00,0.6667,00.00,00.00,00.00,0.0,00,136507.50,,%%3,%%3)]
;*********************************************************************************
;Missouri
[ASSIGN %%1=ADD_JURISDICTION(MO,Missouri,0,0,0,0,0,0)]
;                               1   2        3 4    5   6      7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101, ,0.0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Montana
[ASSIGN %%1=ADD_JURISDICTION(MT,Montana,0,0,0,0,0,0)]
;                               1   2        3 4    5   6      7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701, ,0.0,%%1, 000.00,520.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701, ,0.0,%%1, 000.00,504.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030701, ,0.0,%%1, 000.00,487.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701, ,0.0,%%1, 000.00,473.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010701, ,0.0,%%1, 000.00,439.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000701, ,0.0,%%1, 000.00,425.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;Nebraska
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NE')]
;                               1   2        3 4     5   6       7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050101, ,49.00,%%1, 000.00,579.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101, ,49.00,%%1, 000.00,562.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101, ,49.00,%%1, 000.00,542.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101, ,49.00,%%1, 000.00,528.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010101, ,49.00,%%1, 000.00,508.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101, ,49.00,%%1, 000.00,487.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;Nevada
[ASSIGN %%1=ADD_JURISDICTION(NV,Nevada,0,0,0,0,0,0)]
;                               1   2        3 4    5   6       7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20050701, ,0.00,%%1, 000.00,690.83,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20040701, ,0.00,%%1, 000.00,656.74,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20030701, ,0.00,%%1, 000.00,633.08,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20020701, ,0.00,%%1, 000.00,616.70,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20010701, ,0.00,%%1, 000.00,600.25,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20000701, ,0.00,%%1, 000.00,580.72,%%3,%%3,%%3,,0.6667,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;New Hampshire
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NH')]
;                               1   2        3 4    5   6       7      8   9   10  11    12     13    14    15    16  17 18
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701, ,0.00,%%1,0224.70,1123.50,%%3,%%3,%%2,,0.6000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701, ,0.00,%%1,0213.90,1069.50,%%3,%%3,%%2,,0.6000,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9))
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;                                                                                      1   1    1  1   1 1 1  1  1 1 1
;                               1   2        3        4      5   6       7     8   9   0   1    2      3 4 5  6  7 8 9
;New Jersey
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NJ')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,151.00,%%1, 000.00,568.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010101,        ,158.00,%%1, 000.00,591.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101,        ,168.00,%%1, 000.00,629.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101,        ,170.00,%%1, 000.00,638.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,        ,173.00,%%1, 000.00,650.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050101,        ,178.00,%%1, 000.00,666.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20060101,        ,184.00,%%1, 000.00,691.00,%%3,%%3,%%2,,0.7000,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;New Mexico
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NM')]
;                               1   2        3 4     5   6      7      8   9   10  11    12     13    14 15    16  17 18  9
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050101, ,36.00,%%1,000.00,492.98,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101, ,36.00,%%1,000.00,516.89,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101, ,36.00,%%1,000.00,540.07,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101, ,36.00,%%1,000.00,000.00,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010101, ,36.00,%%1,000.00,549.37,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101, ,36.00,%%1,000.00,563.32,%%3,%%3,%%2,00.00,0.6667,00.00,00,0.000,0.0,00,700,,%%3,%%3)]
;*********************************************************************************
;New York
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NY')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;North Carolina
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'NC')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%3,20000101,,30.00,%%1, 000.00,730.00,%%3,%%3,%%2,,0.6667,,,,0.0, , ,,%%3,%%3)]
;*********************************************************************************
;North Dakota
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'ND')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,,0,%%1, 740.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Ohio
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'OH')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.7200,,,,0.6667, , ,,%%2,%%2)]
;*********************************************************************************
;Oklahoma
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'OK')]
;                               1   2        3 4     5   6      7 8   9   10  11 12     13 14 15 16  17 18 19
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19941101, ,0.00,%%1, 000.00,0,%%3,%%3,%%2,  ,0.7000,00,00,00,0.0,00,00,300,%%3,%%3)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19971101, ,0.00,%%1, 000.00,0,%%3,%%3,%%2,  ,0.7000,00,00,00,0.0,00,00,300,%%3,%%3)]
;*********************************************************************************
;Oregon
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'OR')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701,,0,%%1, 865.78,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Pennsylvania
[ASSIGN %%1=ADD_JURISDICTION(PA,Pennsylvania,0,0,0,0,0,0)]
;Pennsylvania does not use the generic rule--see Idaho for a simialiar jurisdictions
;[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Rhode Island
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'RI')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,0,%%3,%%2,%%2,,0.7500,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;South Carolina
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'SC')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;South Dakota
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'SD')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Tennessee
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'TN')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Texas
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'TX')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.7000,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Utah
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'UT')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19980701,,0,%%1, 487.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19990701,,0,%%1, 509.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 529.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020101,,0,%%1, 562.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030101,,0,%%1, 579.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040101,,0,%%1, 589.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9)) %%2(Yes in California)
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;******************************************************************************************************
;Virginia
[ASSIGN %%1=ADD_JURISDICTION(VA,Virginia,0,0,0,0,0,0)]
;                               1   2        3 4      5   6      7      8   9   10  11 12     13 14 15 16  17 18 19  20
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20050701, ,184.00,%%1,000.00,736.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20040701, ,176.50,%%1,000.00,706.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20030701, ,172.75,%%1,000.00,691.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701, ,170.25,%%1,000.00,681.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20010701, ,161.25,%%1,000.00,645.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000701, ,151.50,%%1,000.00,606.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,19990701, ,141.75,%%1,000.00,567.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,09980701, ,133.50,%%1,000.00,534.00,%%3,%%2,%%2,  ,0.6667,  ,  ,  ,0.0,  ,  ,500,%%2,%%2)]
;*********************************************************************************
;Vermont
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'VT')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701,,0,%%1, 865.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Washington, City of
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'DC')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,,0,%%1, 000.00,0,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Washington, State of 
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'WA')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20020701,        ,000.00,%%1, 868.68,000.00,%%3,%%2,%%2,,0.6000,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Wisconsin
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'WI')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;West Virginia
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'WV')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.7000,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Wyoming
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'WY')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Federal Employee Compensation Act
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'FECA')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
;Longshore and Harbor Workers Act
[ASSIGN %%1=QUERY(SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = 'LHWA')]
[ASSIGN %%9=ADD_WCP_BENEFITRULE(%%2,20000101,        ,000.00,%%1, 000.00,000.00,%%3,%%2,%%2,,0.6667,,,,0.0, , ,,%%2,%%2)]
;*********************************************************************************
[FORGIVE] UPDATE WCP_RULE_TTD SET PRIME_RATE = 0.666667 WHERE PRIME_RATE = 0.6667
;[FORGIVE] INSERT INTO FAKE_ERROR (END_OF_FILE) VALUES ('End of JurisRulesDataRuleTTD_AA.sql')
