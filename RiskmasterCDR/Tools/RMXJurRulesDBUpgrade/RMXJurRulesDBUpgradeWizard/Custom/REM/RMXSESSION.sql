--PROVIDE THE NAME OF YOUR SPECIFIC TABLESPACE
--NOTE: ALL CURRENT TABLE CREATION SCRIPTS USE THE DEFAULT TABLESPACE NAME OF RMXSESSION
/*
CREATE TABLESPACE RMXSESSION DATAFILE 
  'D:\ORACLE\PRODUCT\10.1.0\ORADATA\ORCL\RMXSESSION.ORA' SIZE 50M AUTOEXTEND OFF
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO
FLASHBACK ON;
*/

CREATE TABLE ASYNC_TRACKER
(
  ASYNCID               VARCHAR2(50 BYTE),
  ASYNCSTATUS           NUMBER(10),
  ASYNCSTATE            BLOB,
  PERCENTAGECOMPLETION  NUMBER
)
TABLESPACE RMXSESSION
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (ASYNCSTATE) STORE AS 
      ( TABLESPACE RMXSESSION
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
        STORAGE    (
                    INITIAL          64K
                    MINEXTENTS       1
                    MAXEXTENTS       2147483645
                    PCTINCREASE      0
                    FREELISTS        1
                    FREELIST GROUPS  1
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;

CREATE TABLE CUSTOMIZE
(
  ID         NUMBER(10),
  FOLDER     VARCHAR2(100 BYTE),
  FILENAME   VARCHAR2(100 BYTE),
  TYPE       NUMBER(5),
  IS_BINARY  NUMBER(5),
  CONTENT    CLOB
)
TABLESPACE RMXSESSION
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (CONTENT) STORE AS 
      ( TABLESPACE RMXSESSION
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
        STORAGE    (
                    INITIAL          64K
                    MINEXTENTS       1
                    MAXEXTENTS       2147483645
                    PCTINCREASE      0
                    FREELISTS        1
                    FREELIST GROUPS  1
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX PK_CUSTOMIZE ON CUSTOMIZE
(ID)
LOGGING
TABLESPACE RMXSESSION
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE TABLE SESSIONS
(
  SID             VARCHAR2(40 BYTE),
  LASTCHANGE      VARCHAR2(14 BYTE),
  DATA            CLOB,
  USERDATA1       NUMBER(10),
  SEARCH_XML      CLOB,
  SESSION_ROW_ID  NUMBER(10)
)
TABLESPACE RMXSESSION
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (SEARCH_XML) STORE AS 
      ( TABLESPACE RMXSESSION
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
        STORAGE    (
                    INITIAL          64K
                    MINEXTENTS       1
                    MAXEXTENTS       2147483645
                    PCTINCREASE      0
                    FREELISTS        1
                    FREELIST GROUPS  1
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DATA) STORE AS 
      ( TABLESPACE RMXSESSION
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
        STORAGE    (
                    INITIAL          64K
                    MINEXTENTS       1
                    MAXEXTENTS       2147483645
                    PCTINCREASE      0
                    FREELISTS        1
                    FREELIST GROUPS  1
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;

CREATE TABLE SESSIONS_X_BINARY
(
  SESSION_X_BIN_ROW_ID  NUMBER(10),
  SESSION_ROW_ID        NUMBER(10),
  BINARY_TYPE           VARCHAR2(100 BYTE),
  BINARY_NAME           VARCHAR2(100 BYTE),
  BINARY_VALUE          BLOB
)
TABLESPACE RMXSESSION
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (BINARY_VALUE) STORE AS 
      ( TABLESPACE RMXSESSION
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        PCTVERSION  10
        NOCACHE
        STORAGE    (
                    INITIAL          64K
                    MINEXTENTS       1
                    MAXEXTENTS       2147483645
                    PCTINCREASE      0
                    FREELISTS        1
                    FREELIST GROUPS  1
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;

CREATE TABLE SESSION_IDS
(
  SYSTEM_TABLE_NAME  VARCHAR2(80 BYTE),
  NEXT_UNIQUE_ID     NUMBER(10)
)
TABLESPACE RMXSESSION
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


--INSERT DATA INTO THE SESSION DATABASE
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS_X_BINARY',1);
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS',1);
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('CUSTOMIZE',1);
--CREATE FUNCTIONS REQUIRED FOR RUNNING RMX EXPIRE WEB SESSION
CREATE OR REPLACE FUNCTION DATEDIFF (DATE_TYPE IN VARCHAR2, DATE1 IN DATE, DATE2 IN DATE) RETURN NUMBER IS
/******************************************************************************
   NAME:       DATEDIFF
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/17/2007          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     DATEDIFF
      Sysdate:         1/17/2007
      Date and Time:   1/17/2007, 9:50:18 AM, and 1/17/2007 9:50:18 AM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
     DATE_DIFFERENCE NUMBER;
	 
BEGIN
	 IF UPPER(DATE_TYPE) = 'Q' THEN
	 	DATE_DIFFERENCE := ((TO_CHAR(DATE2, 'yyyy') - TO_CHAR(DATE1, 'yyyy')) * 4)
						+ (TO_CHAR(DATE2, 'q') - TO_CHAR(DATE1, 'q'));
	ELSE
		SELECT (DATE2 - DATE1) * DECODE (UPPER(DATE_TYPE),
			   'SS', 24*60*60, 
			   'MI', 24*60,
			   'HH', 24,
			   		 NULL)
		INTO DATE_DIFFERENCE
		FROM DUAL;							
	 END IF;
	 
	 RETURN(DATE_DIFFERENCE);
END DATEDIFF;
/

CREATE OR REPLACE FUNCTION UDF_PROCESSDATE (LASTCHANGE IN VARCHAR2) RETURN DATE IS
/******************************************************************************
   NAME:       UDF_PROCESSDATE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/16/2007          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     UDF_PROCESSDATE
      Sysdate:         1/16/2007
      Date and Time:   1/16/2007, 6:12:51 PM, and 1/16/2007 6:12:51 PM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
	--DECLARE ALL REQUIRE VARIABLES FOR PROCESSING THE DATE STRING
	LASTCHANGEDATE varchar2(50);
	YEARDATE varchar2(4);
	MONTHDATE varchar2(2);
	DAYDATE varchar2(2);
	HOURDATE varchar2(2);
	MINUTES varchar2(2);
	SECONDS varchar2(2);
	SESSIONDATE date;

BEGIN

	--SET UP ALL THE REQUIRED VALUES FOR CONSTRUCTING THE PROPER DATETIME STRING
	SELECT SUBSTR(LASTCHANGE, 1, 4) INTO YEARDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 5,2) INTO MONTHDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 7,2) INTO DAYDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 9,2) INTO HOURDATE FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 11,2) INTO MINUTES FROM DUAL;
	SELECT SUBSTR(LASTCHANGE, 13,2) INTO SECONDS FROM DUAL;

	--CHECK IF THE SECONDS FIELD IS EMPTY TO PROVIDE
	--BACKWARDS COMPATIBILITY WITH THE OLDER WEBFARMSESSION DATABASE
	IF SECONDS = '' THEN
	   SECONDS := '00';
	END IF;
			
	--ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH12:MI:SS';

	--CONCATENATE THE DATE TIME STRING SO THAT IT CAN BE PROPERLY CAST TO A DATE TIME DATA TYPE
	LASTCHANGEDATE := YEARDATE || '-' || MONTHDATE || '-' || DAYDATE || ' ' || HOURDATE || ':' || MINUTES || ':' || SECONDS;

	SELECT TO_DATE(LASTCHANGEDATE,'YYYY-MM-DD HH24:MI:SS') INTO SESSIONDATE FROM DUAL;
	
	RETURN(SESSIONDATE);
	


END UDF_PROCESSDATE;
/

CREATE OR REPLACE PROCEDURE EXPIRE_RMX_SESSION (SESSION_TIMEOUT_SECONDS NUMBER) IS SESSIONS_TIMED_OUT NUMBER;
/******************************************************************************
   NAME:       EXPIRE_RMX_SESSION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/17/2007          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     EXPIRE_RMX_SESSION
      Sysdate:         1/17/2007
      Date and Time:   1/17/2007, 10:15:02 AM, and 1/17/2007 10:15:02 AM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
CURRENT_SYSTEM_DATE DATE;
 

BEGIN
	 --SET A DEFAULT VALUE OF 2 HRS FOR THE SESSION TIMEOUT
	 --SESSION_TIMEOUT_SECONDS := 7200;
	 
	 --ASSIGN THE CURRENT SYSTEM DATE TO A VARIABLE
	 SELECT SYSDATE INTO CURRENT_SYSTEM_DATE FROM DUAL;
	 
	 
	 --BEGIN THE SQL STATEMENT WHICH WILL PERFORM THE CLEANUP
	 /*
	  SELECT * FROM SESSIONS_X_BINARY 
	 WHERE 
	 SESSION_ROW_ID IN 
	 	(SELECT SESSION_ROW_ID FROM SESSIONS WHERE DATA IS NULL OR
			(DATEDIFF('SS',UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS));
			
	 SELECT * FROM SESSIONS WHERE DATA IS NULL OR
 	 (DATEDIFF('SS',UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS);
	 */


	 --DELETE THE EXPIRED SESSIONS
	 DELETE FROM SESSIONS_X_BINARY 
	 WHERE 
	 SESSION_ROW_ID IN 
	 (SELECT SESSION_ROW_ID FROM SESSIONS WHERE DATA IS NULL OR
	 (DATEDIFF('SS', UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS));

	 DELETE FROM SESSIONS WHERE DATA IS NULL OR
	 (DATEDIFF('SS', UDF_PROCESSDATE(LASTCHANGE),CURRENT_SYSTEM_DATE) > SESSION_TIMEOUT_SECONDS);
	
END EXPIRE_RMX_SESSION;
/

CREATE OR REPLACE PROCEDURE INSERT_CUSTOMIZE IS
customize_data CLOB;
/******************************************************************************
   NAME:       INSERT_CUSTOMIZE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/2/2007          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     INSERT_CUSTOMIZE
      Sysdate:         2/2/2007
      Date and Time:   2/2/2007, 12:53:47 PM, and 2/2/2007 12:53:47 PM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
	customize_data := '<customize_captions>
				<RMAdminSettings title="System Customization">
					<Captions title="Captions/Messages">
						<CompanyName default="***" title="Company Name:">THE COMPANY</CompanyName>
						<AppTitle default="Riskmaster.Net" title="Application Title:">Riskmaster.Net</AppTitle>
						<ReportAppTitle default="Sortmaster" title="Application Title:">Sortmaster</ReportAppTitle>
						<AppCopyright default=") 2004 by CSC, All Rights Reserved." title="Application Title:">) 2004 by CSC, All Rights Reserved.</AppCopyright>
						<ErrContact default="For assistance please consult the System Administrator." title="Contact on Error:">For assistance please consult the System Administrator.</ErrContact>
						<AlertExistingRec default="You are working on a new record and this functionality is available for existing records only. Please save the data and try again." title="Alert Existing Record Required:">You are working on a new record and this functionality is available for existing records only. Please save the data and try again.</AlertExistingRec>
					</Captions>
					<Paths title="Paths">
						<OverrideDocPath default="***" title="Override Documents Path:"></OverrideDocPath>
					</Paths>
				</RMAdminSettings>
			</customize_captions>';
	
	INSERT INTO CUSTOMIZE
	   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
	 VALUES
	   (1, 'customize_captions', 0, 0, customize_data);

   customize_data := '<customize_settings>
	<RMAdminSettings title="System Customization">
		<TextAreaSize title="Text Area Size">
			<TextML title="TextMl">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</TextML>
			<FreeCode title="Freecode">
				<Width default="30">30</Width>
				<Height default="8">8</Height>
			</FreeCode>
			<ReadOnlyMemo title="Readonly Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</ReadOnlyMemo>
			<Memo title="Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</Memo>
		</TextAreaSize>
		<Buttons title="Buttons">
			<Document title="Documents">-1</Document>
			<Search title="Search">-1</Search>
			<File title="Files">-1</File>
			<Diary title="Diaries">-1</Diary>
			<Report title="Reports">-1</Report>
		</Buttons>
		<Other>
			<Soundex title="Choose Soundex on Searches">-1</Soundex>
			<ShowName title="Show User Name on Menu">-1</ShowName>
			<ShowLogin title="Show Login on Menu">-1</ShowLogin>
			<SaveShowActiveDiary title="Save Show Active Diary">-1</SaveShowActiveDiary>
		</Other>
		<Funds title="Funds Page Settings">
			<OFAC title="OFAC Check:">-1</OFAC>
		</Funds>
	</RMAdminSettings>
</customize_settings>';

   INSERT INTO CUSTOMIZE
	   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
	 VALUES
	   (2, 'customize_settings', 0, 0, customize_data);
	   
    customize_data := '<customize_reports>
				<RMAdminSettings title="System Customization">
					<ReportEmail title="Report Email Settings">
						<From title="Override From:"/>
						<FromAddr title="Override From Address:"/>
					</ReportEmail>
					<ReportMenu title="Report Menu Links">
						<ReportLabel title="Std Reports Queue" default="Std Reports Queue" value="-1">
							<AvlReports title="Available Reports:">-1</AvlReports>
							<JobQueue title="Job Queue:">-1</JobQueue>
							<NewReport title="Post New Report:">-1</NewReport>
							<DeleteReport title="Delete Report:">-1</DeleteReport>
							<ScheduleReport title="Schedule Reports:">-1</ScheduleReport>
							<ViewSchedReport title="View Scheduled Reports:">-1</ViewSchedReport>
						</ReportLabel>
						<SMLabel title="Std Reports Designers" default="Std Reports Designers" value="-1">
							<Designer title="Designer:">-1</Designer>
							<DraftReport title="Draft Reports:">-1</DraftReport>
							<PostDraftRpt title="Post Draft Reports:">-1</PostDraftRpt>
						</SMLabel>
						<ExecSummLabel title="Exec. Summary" default="Exec. Summary" value="-1">
							<Configuration title="Configuration:">-1</Configuration>
							<Claim title="Claim:">-1</Claim>
							<Event title="Event:">-1</Event>
						</ExecSummLabel>
						<OtherLabel title="Other Reports" default="Other Reports" value="-1">
							<OSHA300 title="OSHA 200:">-1</OSHA300>
							<OSHA301 title="OSHA 301:">-1</OSHA301>
							<OSHA300A title="OSHA 300A:">-1</OSHA300A>
							<DCCLabel title="DCC" default="DCC">-1</DCCLabel>
						</OtherLabel>
						<ReportQueue title="Report Queue Buttons">
							<Archive title="OSHA 200:">-1</Archive>
							<Email title="OSHA 200:">-1</Email>
							<Delete title="OSHA 200:">-1</Delete>
						</ReportQueue>
					</ReportMenu>
				</RMAdminSettings>
			</customize_reports>';


   INSERT INTO CUSTOMIZE
    (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
    VALUES
   	(3, 'customize_reports', 0, 0, customize_data);
	
	customize_data := '<customize_search>
	<RMAdminSettings title="System Customization">
		<Search title="Search Links">
			<Claim title="Claims">-1</Claim>
			<Event title="Events">-1</Event>
			<Employee title="Employees">-1</Employee>
			<Entity title="Entities">-1</Entity>
			<Vehicle title="Vehicles">-1</Vehicle>
			<Policy title="Policies">-1</Policy>
			<Fund title="Funds">-1</Fund>
			<Patient title="Patients">-1</Patient>
			<Physician title="Physicians">-1</Physician>
			<LeavePlan title="LeavePlans">-1</LeavePlan>
		</Search>
	</RMAdminSettings>
</customize_search>';
	

  INSERT INTO CUSTOMIZE
   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
    VALUES
   (4, 'customize_search', 0, 0, customize_data);
   
   customize_data := '<RMAdminSettings title="System Customization">
	<ImageTop title="Images for Top Frame" imagepath="" imageurl="">
		<Top1>
			<current name="Top1.gif"></current>
			<default name="Top1.gif"></default>
		</Top1>
		<Top2>
			<current name="Top2.gif"></current>
			<default name="Top2.gif"></default>
		</Top2>
		<Top3>
			<current name="Top3.gif"></current>
			<default name="Top3.gif"></default>
		</Top3>
	</ImageTop>
	<ImageTopAdmin title="Images for Top Admin Frame" imagepath="" imageurl="">
		<Top1>
			<current name="Top1Admin.gif"></current>
			<default name="Top1Admin.gif"></default>
		</Top1>
		<Top2>
			<current name="Top2Admin.gif"></current>
			<default name="Top2Admin.gif"></default>
		</Top2>
		<Top3>
			<current name="Top3Admin.gif"></current>
			<default name="Top3Admin.gif"></default>
		</Top3>
	</ImageTopAdmin>
</RMAdminSettings>';
     INSERT INTO CUSTOMIZE
   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
   VALUES
   (5, 'customize_images', 0, 0, customize_data);


customize_data :='<customize_custom>
	<RMAdminSettings title="System Customization">
		<Caption title="Caption Custom For">
		 <ExecutiveSummary default="Executive Summary Report">Executive Summary 
	Report</ExecutiveSummary>
		<DCCDisplay default="DCCDisplay">DCCDisplay</DCCDisplay>
	</Caption>
		 <ReportEmailOptions title="Report Email Options">
		 <SendEmailwithLink title="Send Email with Link to Report Output">
			 <text default="Send Email with Link to Report Output">Send Email with 
	Link to Report Outputs</text>
			 <show>-1</show>
		 </SendEmailwithLink>
		 <SendEmailwithReport title="Send Email with Report Output Attached">
			  <text default="Send Email with Report Output Attached">Send Email with 
	Report Output Attached</text>
			 <show>-1</show>
		  </SendEmailwithReport>
		 <SendEmailOnly title="Send Email Only (no report output attached or linked)">
			   <text default="Send Email Only (no report output attached or 
	linked)">Send Email Only (no report output attached or linked)</text>
			  <show>-1</show>
		 </SendEmailOnly>
		  <None title="None">
			   <text default="None">None</text>
			   <show />
		  </None>
		  <DefaultOption>0</DefaultOption>
		</ReportEmailOptions>
		 <ReportRunOptions title="Report Email Options">
			<Immediately title="Immediately">
			    <text default="Immediately">Immediately</text>
			   <show />
			</Immediately>
			 <AtSpecificDate title="At Specific Date/Time ">
				 <text default="At Specific Date/Time ">At Specific Date/Time -</text>
				 <show>-1</show>
			 </AtSpecificDate>
               <DefaultOption>0</DefaultOption>
		</ReportRunOptions>
          <ReservesSettings>
               <AddPayment title="Add Payment">-1</AddPayment>
               <AddCollection title="Add Collection">-1</AddCollection>
          </ReservesSettings>
          <DocumentManagement title="Document Management Settings Custom For">
               <Files_Email title="Files Email">-1</Files_Email>
               <Files_Transfer title="Files Transfer">-1</Files_Transfer>
               <Files_Copy title="Files Copy">-1</Files_Copy>
               <Files_Move title="Files Move">-1</Files_Move>
               <Document_View title="Document View">-1</Document_View>
               <Document_Download title="Document Download">-1</Document_Download>
               <Document_Email title="Document Email">-1</Document_Email>
               <Document_Transfer title="Document Transfer">-1</Document_Transfer>
               <Document_Copy title="Document Copy">-1</Document_Copy>
		<Document_Move title="Document Move">-1</Document_Move>
		<Attachments_Delete title="Attachments Delete">-1</Attachments_Delete>
		<Attachments_Email title="Attachments Email">-1</Attachments_Email>
		<Attachments_P_Download title="Attachments 
	Download">-1</Attachments_P_Download>
               <Attachments_P_Email title="Attachments Email">-1</Attachments_P_Email>
          </DocumentManagement>
	  <SecuritySetting title="Security Setting Custom For">
		<ChangeDatabase title="Change Database">-1</ChangeDatabase>
		<ChangePassword title="Change Password">-1</ChangePassword>
	  </SecuritySetting>
          <SpecialSettings title="Special Settings">
		<Show_Maintenance title="Show Maintenance Menu">-1</Show_Maintenance>
               <Show_AdjusterList title="Show AdjusterList">-1</Show_AdjusterList>
               <ViewOnlyBES title="View Only BES for Oracle">-1</ViewOnlyBES>
               <EnhanceBES title="Enhance BES for Oracle">-1</EnhanceBES>
               <text />
          </SpecialSettings>
     </RMAdminSettings>
</customize_custom>';

INSERT INTO CUSTOMIZE
   (ID, FILENAME, TYPE, IS_BINARY, CONTENT)
   VALUES
   (6, 'customize_custom', 0, 0, customize_data);



   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END INSERT_CUSTOMIZE;
/

--CALL THE PROCEDURE TO INSERT DEFAULT VALUES INTO THE CUSTOMIZE TABLE
BEGIN
	INSERT_CUSTOMIZE;	
END;
/

--CREATE THE JOB IN ORACLE
--AND SPECIFY THAT IT SHOULD RUN STARTING TODAY
--AND THEN RUN EVERY 2 HOURS SUBSEQUENT TO THE INITIAL RUN

--NOTE: THIS JOB CAN TESTED AND RUN THROUGH THE FOLLOWING COMMAND:
--EXEC DBMS_JOB.RUN(<JOB NUMBER>);  EX: EXEC DBMS_JOB.RUN(999);

BEGIN
DBMS_JOB.isubmit(
   job => 999, 
   what => 'EXPIRE_RMX_SESSION(7200);', 
   next_date => SYSDATE,
   interval => 'trunc(SYSDATE+2/24,''HH'')');
COMMIT;
END;
/
