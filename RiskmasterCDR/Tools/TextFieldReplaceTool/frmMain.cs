using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Db;
using System.IO;
using System.Xml;

namespace TextFieldRepaceTool
{
    public partial class frmMain : Form
    {
        private const string m_ConfigXmlFile = "ReplaceText.xml";
        private static string m_sDSN = string.Empty;
        private string m_sUserID = string.Empty;
        private string m_sPassword = string.Empty;
        private string m_sConnectionString = string.Empty;
        private eDatabaseType m_DatabaseType = eDatabaseType.DBMS_IS_SQLSRVR;
        private string m_sOriginal = string.Empty;
        private string m_sNew = string.Empty;
        private ReplaceTextClass m_RText = null;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            UserLogin();
            string sPath = AppDomain.CurrentDomain.BaseDirectory;
            if (!sPath.EndsWith("\\"))
            {
                sPath += "\\";
            }
            string sFile = sPath + m_ConfigXmlFile;
            if (!File.Exists(sFile))
            {
                btnSearch.Enabled = false;
                btnReplace.Enabled = false;
                string sMessage = "Can not find file ReplaceText.xml. It should be in the same folder as the executable.";
                MessageBox.Show(sMessage);
                Application.Exit();
                return;
            }

            //get original and new text values
            XmlDocument oDocument = new XmlDocument();
            string sOriginal = string.Empty;
            string sNew = string.Empty;
            try
            {
                oDocument.Load(sFile);
                XmlElement oText = (XmlElement)oDocument.SelectSingleNode("/configSettings/textValue");
                if (oText != null)
                {
                    sOriginal = oText.Attributes["original"].Value;
                    sNew = oText.Attributes["new"].Value;
                    txtOriginal.Text = sOriginal;
                    txtNew.Text = sNew;
                }
                else
                {
                    MessageBox.Show("Original and New values are not set in ReplaceText.xml.");
                    Application.Exit();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in accessing ReplaceText.xml." + ex.Message);
                Application.Exit();
                return;
            }
            m_RText = new ReplaceTextClass(m_sConnectionString, m_DatabaseType, sPath, sOriginal, sNew);
            m_RText.showProgress += new ReplaceTextClass.progressDelegate(displayProgress);
            m_RText.showMessage += new ReplaceTextClass.messageDelegate(displayMessage);
        }

        /// <summary>
        /// Login to the system
        /// </summary>
        private void UserLogin()
        {
            frmLogin objForm = new frmLogin();
            try
            {
                objForm.ShowDialog();
                if (!objForm.LoginSuccess)
                {
                    System.Windows.Forms.Application.Exit();
                    return;
                }

                m_sUserID = objForm.UserID;
                m_sPassword = objForm.Password;
                m_sDSN = objForm.DSN;
                m_sConnectionString = objForm.ConnectionString;
                m_DatabaseType = objForm.DatabaseType;
                objForm.Dispose();
                objForm = null;

                this.Visible = true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); };

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            m_RText.OriginalValue = txtOriginal.Text;
            m_RText.NewValue = txtNew.Text;
            m_RText.ReplaceText();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            m_RText.OriginalValue = txtOriginal.Text;
            m_RText.NewValue = txtNew.Text;
            m_RText.scanTextField();
        }

        private void displayProgress(string sMessage)
        {
            lblMessage.Text = sMessage;
        }

        private void displayMessage(string sMessage, bool bError)
        {
            if (bError)
            {
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                lblMessage.ForeColor = Color.Green;
            }
            lblMessage.Text = sMessage;
        }
    }
}