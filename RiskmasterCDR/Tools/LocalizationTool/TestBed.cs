using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Riskmaster.Common;
using System.Threading;
using System.Globalization;

namespace Riskmaster.Tools
{
	/// <summary>
	/// Summary description for TestBed.
	/// </summary>
	public class TestBed : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button cmdGlobalize;
		private System.Windows.Forms.Button cmdGlobalizeUK;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtResourceName;
		private System.Windows.Forms.Button cmdExit;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtLocaleInfo;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new TestBed());
		}

		public TestBed()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdGlobalize = new System.Windows.Forms.Button();
			this.cmdGlobalizeUK = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.txtResourceName = new System.Windows.Forms.TextBox();
			this.cmdExit = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtLocaleInfo = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cmdGlobalize
			// 
			this.cmdGlobalize.Location = new System.Drawing.Point(272, 80);
			this.cmdGlobalize.Name = "cmdGlobalize";
			this.cmdGlobalize.Size = new System.Drawing.Size(144, 23);
			this.cmdGlobalize.TabIndex = 0;
			this.cmdGlobalize.Text = "Localize...";
			this.cmdGlobalize.Click += new System.EventHandler(this.cmdGlobalize_Click);
			// 
			// cmdGlobalizeUK
			// 
			this.cmdGlobalizeUK.Location = new System.Drawing.Point(272, 112);
			this.cmdGlobalizeUK.Name = "cmdGlobalizeUK";
			this.cmdGlobalizeUK.Size = new System.Drawing.Size(144, 23);
			this.cmdGlobalizeUK.TabIndex = 2;
			this.cmdGlobalizeUK.Text = "Localize United Kingdom...";
			this.cmdGlobalizeUK.Click += new System.EventHandler(this.cmdGlobalizeUnitedKingdom_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 240);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(112, 23);
			this.button1.TabIndex = 3;
			this.button1.Text = "Localization Notes...";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtResourceName
			// 
			this.txtResourceName.Location = new System.Drawing.Point(96, 16);
			this.txtResourceName.Name = "txtResourceName";
			this.txtResourceName.Size = new System.Drawing.Size(240, 20);
			this.txtResourceName.TabIndex = 4;
			this.txtResourceName.Text = "IDbCommand.Connection.InvalidTypeMessage";
			// 
			// cmdExit
			// 
			this.cmdExit.Location = new System.Drawing.Point(344, 240);
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.TabIndex = 5;
			this.cmdExit.Text = "Cancel";
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 23);
			this.label1.TabIndex = 6;
			this.label1.Text = "Resource Name:";
			// 
			// txtLocaleInfo
			// 
			this.txtLocaleInfo.Location = new System.Drawing.Point(136, 48);
			this.txtLocaleInfo.Name = "txtLocaleInfo";
			this.txtLocaleInfo.Size = new System.Drawing.Size(88, 20);
			this.txtLocaleInfo.TabIndex = 8;
			this.txtLocaleInfo.Text = "en-US";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 23);
			this.label2.TabIndex = 9;
			this.label2.Text = "CultureInfo Name:";
			// 
			// TestBed
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(432, 273);
			this.Controls.Add(this.txtLocaleInfo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtResourceName);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cmdExit);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.cmdGlobalizeUK);
			this.Controls.Add(this.cmdGlobalize);
			this.Name = "TestBed";
			this.Text = "Localization Tool";
			this.Load += new System.EventHandler(this.TestBed_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void cmdGlobalize_Click(object sender, System.EventArgs e)
		{
			CultureInfo prevCulture = Thread.CurrentThread.CurrentCulture;

			Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(txtLocaleInfo.Text);
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

			MessageBox.Show(Globalization.GetString(txtResourceName.Text));
			
			
			Thread.CurrentThread.CurrentCulture = prevCulture;
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

		}

		private void cmdGlobalizeUnitedKingdom_Click(object sender, System.EventArgs e)
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			MessageBox.Show(Globalization.GetString("IDbCommand.Connection.InvalidTypeMessage"));
			MessageBox.Show(Globalization.GetString("QueryTool.ENONLY"));

		}

		private void TestBed_Load(object sender, System.EventArgs e)
		{
		
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(@"
													The 'project name' and  'default namespace' properties of the project must match
													exactly in order for the common resources code to function properly.
													");
	}

		private void cmdGlobalizeGerman_Click(object sender, System.EventArgs e)
		{
		
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}


	}
}
