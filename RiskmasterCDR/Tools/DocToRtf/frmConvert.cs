using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Text; 
using System.Reflection;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;  
using System.Diagnostics;

namespace Riskmaster.Tools.DocToRtf
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmConvert : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblNote;
		private System.Windows.Forms.Button cmdConvert;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.StatusBar stBar;
		private System.Windows.Forms.StatusBarPanel statusBarPanel;
		private System.Windows.Forms.TextBox txtDestination;
		private System.Windows.Forms.Label lblDestination;
		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.Label lblSource;
		private System.Windows.Forms.Label lblHeader;
		private System.Windows.Forms.GroupBox grpFileServer;
		private System.Windows.Forms.GroupBox grpOption;
		private System.Windows.Forms.RadioButton optFileServer;
		private System.Windows.Forms.RadioButton optDatabaseServer;
		private System.Windows.Forms.Button cmdDestination;
		private System.Windows.Forms.Button cmdSourceFile;
		private System.Windows.Forms.GroupBox grpDatabaseServer;
		private System.Windows.Forms.Label lblDbServer;
		private System.Windows.Forms.TextBox txtConnectString;
		private System.Windows.Forms.Label lblMsg;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmConvert()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblNote = new System.Windows.Forms.Label();
			this.cmdConvert = new System.Windows.Forms.Button();
			this.stBar = new System.Windows.Forms.StatusBar();
			this.statusBarPanel = new System.Windows.Forms.StatusBarPanel();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.grpFileServer = new System.Windows.Forms.GroupBox();
			this.cmdSourceFile = new System.Windows.Forms.Button();
			this.cmdDestination = new System.Windows.Forms.Button();
			this.lblHeader = new System.Windows.Forms.Label();
			this.lblSource = new System.Windows.Forms.Label();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.lblDestination = new System.Windows.Forms.Label();
			this.txtDestination = new System.Windows.Forms.TextBox();
			this.grpOption = new System.Windows.Forms.GroupBox();
			this.optDatabaseServer = new System.Windows.Forms.RadioButton();
			this.optFileServer = new System.Windows.Forms.RadioButton();
			this.grpDatabaseServer = new System.Windows.Forms.GroupBox();
			this.lblMsg = new System.Windows.Forms.Label();
			this.txtConnectString = new System.Windows.Forms.TextBox();
			this.lblDbServer = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel)).BeginInit();
			this.grpFileServer.SuspendLayout();
			this.grpOption.SuspendLayout();
			this.grpDatabaseServer.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblNote
			// 
			this.lblNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblNote.Location = new System.Drawing.Point(17, 216);
			this.lblNote.Name = "lblNote";
			this.lblNote.Size = new System.Drawing.Size(312, 24);
			this.lblNote.TabIndex = 2;
			this.lblNote.Text = "Note : You must have Microsoft Word installed on this machine in order to use thi" +
				"s converter.";
			// 
			// cmdConvert
			// 
			this.cmdConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdConvert.Location = new System.Drawing.Point(125, 256);
			this.cmdConvert.Name = "cmdConvert";
			this.cmdConvert.Size = new System.Drawing.Size(96, 40);
			this.cmdConvert.TabIndex = 8;
			this.cmdConvert.Text = "Convert";
			this.cmdConvert.Click += new System.EventHandler(this.cmdConvert_Click);
			// 
			// stBar
			// 
			this.stBar.Location = new System.Drawing.Point(0, 303);
			this.stBar.Name = "stBar";
			this.stBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																					 this.statusBarPanel});
			this.stBar.ShowPanels = true;
			this.stBar.Size = new System.Drawing.Size(346, 16);
			this.stBar.TabIndex = 9;
			// 
			// statusBarPanel
			// 
			this.statusBarPanel.Width = 334;
			// 
			// folderBrowserDialog
			// 
			this.folderBrowserDialog.ShowNewFolderButton = false;
			// 
			// grpFileServer
			// 
			this.grpFileServer.Controls.Add(this.cmdSourceFile);
			this.grpFileServer.Controls.Add(this.cmdDestination);
			this.grpFileServer.Controls.Add(this.lblHeader);
			this.grpFileServer.Controls.Add(this.lblSource);
			this.grpFileServer.Controls.Add(this.txtSource);
			this.grpFileServer.Controls.Add(this.lblDestination);
			this.grpFileServer.Controls.Add(this.txtDestination);
			this.grpFileServer.Location = new System.Drawing.Point(9, 40);
			this.grpFileServer.Name = "grpFileServer";
			this.grpFileServer.Size = new System.Drawing.Size(328, 160);
			this.grpFileServer.TabIndex = 10;
			this.grpFileServer.TabStop = false;
			// 
			// cmdSourceFile
			// 
			this.cmdSourceFile.Location = new System.Drawing.Point(288, 64);
			this.cmdSourceFile.Name = "cmdSourceFile";
			this.cmdSourceFile.Size = new System.Drawing.Size(24, 21);
			this.cmdSourceFile.TabIndex = 15;
			this.cmdSourceFile.Text = "...";
			this.cmdSourceFile.Click += new System.EventHandler(this.cmdSourceFile_Click);
			// 
			// cmdDestination
			// 
			this.cmdDestination.Location = new System.Drawing.Point(288, 129);
			this.cmdDestination.Name = "cmdDestination";
			this.cmdDestination.Size = new System.Drawing.Size(24, 21);
			this.cmdDestination.TabIndex = 14;
			this.cmdDestination.Text = "...";
			this.cmdDestination.Click += new System.EventHandler(this.cmdDestination_Click);
			// 
			// lblHeader
			// 
			this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblHeader.Location = new System.Drawing.Point(8, 16);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new System.Drawing.Size(312, 24);
			this.lblHeader.TabIndex = 13;
			this.lblHeader.Text = "Choose a source and a destination, then press \"Convert\".";
			// 
			// lblSource
			// 
			this.lblSource.Location = new System.Drawing.Point(8, 40);
			this.lblSource.Name = "lblSource";
			this.lblSource.Size = new System.Drawing.Size(192, 24);
			this.lblSource.TabIndex = 12;
			this.lblSource.Text = "Source Folder :";
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(8, 64);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(272, 20);
			this.txtSource.TabIndex = 11;
			this.txtSource.Text = "";
			// 
			// lblDestination
			// 
			this.lblDestination.Location = new System.Drawing.Point(8, 104);
			this.lblDestination.Name = "lblDestination";
			this.lblDestination.Size = new System.Drawing.Size(192, 24);
			this.lblDestination.TabIndex = 9;
			this.lblDestination.Text = "Destination Folder :";
			// 
			// txtDestination
			// 
			this.txtDestination.Location = new System.Drawing.Point(8, 128);
			this.txtDestination.Name = "txtDestination";
			this.txtDestination.Size = new System.Drawing.Size(272, 20);
			this.txtDestination.TabIndex = 6;
			this.txtDestination.Text = "";
			// 
			// grpOption
			// 
			this.grpOption.Controls.Add(this.optDatabaseServer);
			this.grpOption.Controls.Add(this.optFileServer);
			this.grpOption.Location = new System.Drawing.Point(9, 0);
			this.grpOption.Name = "grpOption";
			this.grpOption.Size = new System.Drawing.Size(328, 40);
			this.grpOption.TabIndex = 12;
			this.grpOption.TabStop = false;
			// 
			// optDatabaseServer
			// 
			this.optDatabaseServer.Location = new System.Drawing.Point(200, 12);
			this.optDatabaseServer.Name = "optDatabaseServer";
			this.optDatabaseServer.Size = new System.Drawing.Size(112, 24);
			this.optDatabaseServer.TabIndex = 13;
			this.optDatabaseServer.Text = "Database Server";
			this.optDatabaseServer.CheckedChanged += new System.EventHandler(this.optDatabaseServer_CheckedChanged);
			// 
			// optFileServer
			// 
			this.optFileServer.Checked = true;
			this.optFileServer.Location = new System.Drawing.Point(8, 12);
			this.optFileServer.Name = "optFileServer";
			this.optFileServer.TabIndex = 12;
			this.optFileServer.TabStop = true;
			this.optFileServer.Text = "File Server";
			this.optFileServer.CheckedChanged += new System.EventHandler(this.optFileServer_CheckedChanged);
			// 
			// grpDatabaseServer
			// 
			this.grpDatabaseServer.Controls.Add(this.label1);
			this.grpDatabaseServer.Controls.Add(this.lblMsg);
			this.grpDatabaseServer.Controls.Add(this.txtConnectString);
			this.grpDatabaseServer.Controls.Add(this.lblDbServer);
			this.grpDatabaseServer.Location = new System.Drawing.Point(9, 40);
			this.grpDatabaseServer.Name = "grpDatabaseServer";
			this.grpDatabaseServer.Size = new System.Drawing.Size(328, 160);
			this.grpDatabaseServer.TabIndex = 13;
			this.grpDatabaseServer.TabStop = false;
			this.grpDatabaseServer.Visible = false;
			// 
			// lblMsg
			// 
			this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblMsg.Location = new System.Drawing.Point(8, 120);
			this.lblMsg.Name = "lblMsg";
			this.lblMsg.Size = new System.Drawing.Size(296, 32);
			this.lblMsg.TabIndex = 2;
			this.lblMsg.Text = "The mail merge documents already saved in the database will be converted.";
			// 
			// txtConnectString
			// 
			this.txtConnectString.Location = new System.Drawing.Point(8, 80);
			this.txtConnectString.Name = "txtConnectString";
			this.txtConnectString.Size = new System.Drawing.Size(296, 20);
			this.txtConnectString.TabIndex = 1;
			this.txtConnectString.Text = "";
			// 
			// lblDbServer
			// 
			this.lblDbServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblDbServer.Location = new System.Drawing.Point(8, 16);
			this.lblDbServer.Name = "lblDbServer";
			this.lblDbServer.Size = new System.Drawing.Size(296, 24);
			this.lblDbServer.TabIndex = 0;
			this.lblDbServer.Text = "Enter a valid connection string :";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(296, 32);
			this.label1.TabIndex = 3;
			this.label1.Text = "e.g. Driver = {SQL Server}; Server=ServerName;Database=DBName;UID=sa;PWD=;";
			// 
			// frmConvert
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(346, 319);
			this.Controls.Add(this.grpOption);
			this.Controls.Add(this.stBar);
			this.Controls.Add(this.cmdConvert);
			this.Controls.Add(this.lblNote);
			this.Controls.Add(this.grpFileServer);
			this.Controls.Add(this.grpDatabaseServer);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "frmConvert";
			this.Text = ".Doc to .Rtf Converter";
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel)).EndInit();
			this.grpFileServer.ResumeLayout(false);
			this.grpOption.ResumeLayout(false);
			this.grpDatabaseServer.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			System.Windows.Forms.Application.Run(new frmConvert());
		}

		private void cmdConvert_Click(object sender, System.EventArgs e)
		{	
			//StreamWriter myOutputWriter=null;
			//TextWriterTraceListener listner=null;
			try
			{
				int iTotalFiles = 0;
				int iFileNumber = 0;
				//if ( !Directory.Exists( "./logs" ))
				//	Directory.CreateDirectory("./logs");
				//string myFileName ="./logs/log.txt";
				//if(!File.Exists(myFileName)) 
				//{
				//	Stream myFile = File.Create(myFileName);
				//	// If the file cannot be created, exit the application.
				//	if(myFile == null) 
				//	{
				//		MessageBox.Show("Error generating log file");
				//		System.Windows.Forms.Application.Exit();
				//	}
				//}
 
				// Assign output file to the output stream.
				//myOutputWriter = File.AppendText(myFileName);

				//listner = new TextWriterTraceListener(myOutputWriter);
				//Trace.Listeners.Add( listner );
				// Use reflection to create instance of Word...avoids version compatibility issue
				Type tApp = System.Type.GetTypeFromProgID("Word.Application");
				Object oApp = Activator.CreateInstance(tApp);

				if(optFileServer.Checked) // File Server case
				{
					string sSrcFolder = txtSource.Text;  
					string sDestFolder = txtDestination.Text;  

					// Check Source Exists
					if(!System.IO.Directory.Exists(sSrcFolder))
					{
						MessageBox.Show(null, "Invalid source selected. Please select a valid source directory and try again.", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
						return; 
					}

					// Check valid Destination
					if(!System.IO.Directory.Exists(sDestFolder))
					{
						MessageBox.Show(null, "Invalid destination selected. Please select a valid destination directory and try again.", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
						return; 
					}

					// Check can write to destination
					FileStream oFS = File.Create(sDestFolder + "\\DocConv.txt");
					if( oFS != null)
					{
						// Release the file stream
						oFS.Close(); 
						// Delete test file
						File.Delete(sDestFolder + "\\DocConv.txt");
					}
					else
					{
						// Release the file stream
						oFS.Close(); 
						MessageBox.Show(null, "Could not write to selected destination. Please select a valid destination directory and try again.", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
						return;
					}

					// Determine how many .doc files for status bar update
//					foreach(string f in System.IO.Directory.GetFiles(sSrcFolder))  
//					{
//						if( f.ToLower().Substring( f.Length - 4, 4) == ".doc")
//							iTotalFiles++;
//					}
					foreach(string f in System.IO.Directory.GetFiles(sSrcFolder))  
					{
						if( f.ToLower().Substring( f.Length - 4, 4) == ".hdr")
							iTotalFiles++;
					}
			
					if(iTotalFiles == 0)
					{
						MessageBox.Show(null, "No files found to convert...", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
						return;
					}

					// Loop through the doc files to convert
					foreach (string f in System.IO.Directory.GetFiles(sSrcFolder))  
					{
						//if( f.ToLower().Substring( f.Length - 4, 4) == ".doc")
						if( f.ToLower().Substring( f.Length - 4, 4) == ".hdr")
						{
							string ftmp = f.ToLower().Replace(".hdr", ".doc");
							if (System.IO.File.Exists(ftmp))
							{
								// Type-safety
								object srcFile = ftmp;
						
								// Retrieve only file name
								object sNewFilename = ftmp.ToString().Substring( sSrcFolder.Length + 1, ftmp.Length - sSrcFolder.Length - 5);

								object oDocs = tApp.InvokeMember("Documents", BindingFlags.GetProperty, null, oApp, null);
								object aDoc = oDocs.GetType().InvokeMember("Open",BindingFlags.InvokeMethod, null, oDocs, new object[]{srcFile});
								aDoc.GetType().InvokeMember("Activate", BindingFlags.InvokeMethod, null, aDoc, null);
				
								object oActiveDoc =	tApp.InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, oApp, null);
								oActiveDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, oActiveDoc,new object[]{sDestFolder + "\\" + sNewFilename + ".rtf",6});
			
								aDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, aDoc, new object[]{false, null, null});

								iFileNumber++;
								statusBarPanel.Text = "Converting file " + iFileNumber + " of " + iTotalFiles + " files..." + sNewFilename + ".DOC";
							}
						}
					}
					if ( File.Exists(sSrcFolder+"//mergemaster.def"))
					{
						string sFileContent =string.Empty;
						using( StreamReader readerMergemaster = new StreamReader(sSrcFolder+"//mergemaster.def"))
						{
							sFileContent= readerMergemaster.ReadToEnd();
						}
						if(File.Exists(sSrcFolder+"//mergemaster.def.old"))
							File.Delete(sSrcFolder+"//mergemaster.def.old");
						FileInfo fRename = new FileInfo(sSrcFolder+"//mergemaster.def");
						fRename.MoveTo(sSrcFolder+"//mergemaster.def.old");
						
						using(StreamWriter writerMergeMaster = new StreamWriter(sSrcFolder+"//mergemaster.def"))
						{
							writerMergeMaster.Write( sFileContent.Replace(".doc",".rtf"));
						}
					}
				}
				else // Database Server case
				{
					if(txtConnectString.Text == "")
					{
						MessageBox.Show(null, "Please enter a valid connection string and try again.", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
						return; 
					}
					else
					{
						string m_ConnStr = txtConnectString.Text;
						
						DbConnection cn = DbFactory.GetDbConnection(m_ConnStr);
						DbTransaction oTxn = null;
						DbReader objReader = null;
						DbWriter objWriter = null;
						StringBuilder sbSql = new StringBuilder();
						FileStorageManager oFileManager = null;

						try
						{
							cn.Open();
							oTxn = cn.BeginTransaction();

							try
							{
								oFileManager = new FileStorageManager();
								oFileManager.FileStorageType = StorageType.DatabaseStorage;
								oFileManager.DestinationStoragePath = m_ConnStr;

								sbSql.Remove(0,sbSql.Length);
								sbSql.Append("SELECT FILENAME FROM DOCUMENT_STORAGE");
					
								// Get total no. of files to convert
								objReader = DbFactory.GetDbReader(m_ConnStr,sbSql.ToString());
								string sSrcFile = "";
								if(objReader != null)
									while(objReader.Read())
									{
										sSrcFile = objReader.GetString("FILENAME");
										// if( sSrcFile.ToLower().Substring( sSrcFile.Length - 4, 4) == ".doc")
										if( sSrcFile.ToLower().Substring( sSrcFile.Length - 4, 4) == ".hdr")
											iTotalFiles++;
									}

								// Determine how many .doc files for status bar update
								if(iTotalFiles == 0)
								{
									MessageBox.Show(null, "No .doc files found to convert...", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
									return;
								}

								string sRetrieveFile = "";
								objReader = DbFactory.GetDbReader(m_ConnStr, sbSql.ToString());
								objWriter = DbFactory.GetDbWriter(cn);
								while(objReader.Read())
								{
									sSrcFile = objReader.GetString("FILENAME");

									// if( sSrcFile.ToLower().Substring( sSrcFile.Length - 4, 4) == ".doc")
									if( sSrcFile.ToLower().Substring( sSrcFile.Length - 4, 4) == ".hdr")
									{

										sSrcFile = sSrcFile.ToLower().Replace(".hdr", ".doc");

										//Assume FileStorage checks if the folder is writable...
										// oFileManager.RetrieveFile(sSrcFile, sSrcFile, out sRetrieveFile); 
										bool bProcess = true;
										try
										{
											oFileManager.RetrieveFile(sSrcFile, sSrcFile, out sRetrieveFile); 
										}
										catch(Exception ex)
										{
											bProcess = false;
										}

										if (!bProcess) continue;

										// Retrieve only file name
										object sNewFilename = sSrcFile.ToString().Substring(0, sSrcFile.Length - 4);

										object oDocs = tApp.InvokeMember("Documents", BindingFlags.GetProperty, null, oApp, null);
										object aDoc = oDocs.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, oDocs, new object[]{AppDomain.CurrentDomain.BaseDirectory + "\\" + sSrcFile});
										aDoc.GetType().InvokeMember("Activate", BindingFlags.InvokeMethod, null, aDoc, null);
			
										object oActiveDoc =	tApp.InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, oApp, null);
										oActiveDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, oActiveDoc, new object[]{AppDomain.CurrentDomain.BaseDirectory + "\\" + sNewFilename + ".rtf",6});
		
										aDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, aDoc, new object[]{false, null, null});

										oFileManager.DeleteFile(sSrcFile); 
										oFileManager.StoreFile(sNewFilename + ".rtf", sNewFilename + ".rtf");

										//Clean-up
										if(File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\" + sSrcFile))
											File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\" + sSrcFile);
										if(File.Exists(AppDomain.CurrentDomain.BaseDirectory  + "\\" + sNewFilename + ".rtf"))
											File.Delete(AppDomain.CurrentDomain.BaseDirectory  + "\\" + sNewFilename + ".rtf");

										iFileNumber++;
										statusBarPanel.Text = "Converting file " + iFileNumber + " of " + iTotalFiles + " files..." + sNewFilename + ".DOC";
									}
								}
								oTxn.Commit();
							}
							catch( Exception ex )
							{
								oTxn.Rollback(); 
								MessageBox.Show(null, "Error in conversion on database...", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
								statusBarPanel.Text = "";
								Trace.WriteLine("Error generated on : " +DateTime.Now.ToString());
								Trace.WriteLine(ex.Message);
								Trace.WriteLine(ex.StackTrace);
								return;
							}
						}
						catch( Exception exp )
						{
							cn = null;
							MessageBox.Show(null, "Couldn't connect to database. Please enter a valid connection string or contact your system administrator.", "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
							Trace.WriteLine("Error generated on : " +DateTime.Now.ToString());
							Trace.WriteLine(exp.Message);
							Trace.WriteLine(exp.StackTrace);
							return; 

						}
						finally
						{
							if(cn != null)
							{
								cn.Close();
								cn = null;
							}
							if(oTxn != null)
							{
								oTxn.Dispose();
								oTxn = null;
							}
							objReader = null;
							objWriter = null;
							oFileManager = null;
						}
					}
				}

				//Closing the application
				oApp.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, oApp, new object[]{false, null, null});

				// Conversion Successful
				statusBarPanel.Text = "Ready";
				MessageBox.Show(null,"Conversion Completed Successfully...", "DocToRtf Conversion",MessageBoxButtons.OK, MessageBoxIcon.Information); 
			}
			catch(Exception Ex)
			{
				// Throw exception
				Trace.WriteLine("Error generated on : " +DateTime.Now.ToString());
				Trace.WriteLine(Ex.Message);
				Trace.WriteLine(Ex.StackTrace);
							
				MessageBox.Show(null, Ex.Message, "DocToRtf Conversion",MessageBoxButtons.OK,MessageBoxIcon.Error); 
			}
			finally
			{
				// Flush and close the output stream.
				//if ( myOutputWriter != null)
				//{
				//	myOutputWriter.Flush();
				//	myOutputWriter.Close();
				//}
				//Trace.Listeners.Remove( listner );
				//listner = null;
			}
		}

		private void cmdDestination_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.Description = "Choose the directory where the created RTF files should be placed:";
			// Show the FolderBrowserDialog.
			DialogResult result = folderBrowserDialog.ShowDialog();
			if( result == DialogResult.OK )
			{
				txtDestination.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void cmdSourceFile_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.Description = "Choose the directory containing the Word Files to translate:";
			// Show the FolderBrowserDialog.
			DialogResult result = folderBrowserDialog.ShowDialog();
			if( result == DialogResult.OK )
			{
				txtSource.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void optDatabaseServer_CheckedChanged(object sender, System.EventArgs e)
		{
			grpFileServer.Visible = false; 
			grpDatabaseServer.Visible = true;
		}

		private void optFileServer_CheckedChanged(object sender, System.EventArgs e)
		{
			grpDatabaseServer.Visible = false;
			grpFileServer.Visible = true; 
		}
	}
}
