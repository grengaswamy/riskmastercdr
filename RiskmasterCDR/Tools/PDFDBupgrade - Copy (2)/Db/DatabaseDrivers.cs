﻿using System;

namespace Riskmaster.Db
{
    /// <summary>
    /// Manages the retrieval of Database Drivers present on the system
    /// </summary>
    public class DatabaseDrivers
    {
        ///// <summary>
        ///// Retrieves the list of currently installed Database Drivers
        ///// on the system
        ///// </summary>
        ///// <returns>string array containing the list of all database drivers
        ///// present on the system</returns>
        //public static void GetInstalledDatabaseDrivers(out Hashtable OdbcDriverList,
        //    out Hashtable OdbcDLLProviders)
        //{
        //    RegistryKey key = Registry.LocalMachine.OpenSubKey(DBDrivers.DatabaseDriverPath);

        //    RegistryKey SubKey = null;
        //    string sDriver = string.Empty, sProviderTmp = string.Empty;
           
        //    foreach (string sProviderName in key.GetSubKeyNames())
        //    {
        //        SubKey = key.OpenSubKey(sProviderName);
        //        sDriver = (string)SubKey.GetValue(DBDrivers.Driver);

        //        //Note that if there are duplicate "providers" for a single
        //        // driver .dll file just chain them together for later string search.
        //        if (!String.IsNullOrEmpty(sDriver))
        //        {
        //            sProviderTmp = sProviderName;
        //            sDriver = System.IO.Path.GetFileName(sDriver);
        //            if (OdbcDriverList.ContainsKey(sDriver))
        //            {
        //                sProviderTmp = String.Format("{0}|{1}", sProviderName, (string)OdbcDriverList[sDriver]);
        //                OdbcDriverList.Remove(sDriver);
        //            }
        //            OdbcDriverList.Add(sDriver, sProviderTmp);
        //            OdbcDLLProviders.Add(sProviderName, sDriver); //Allow two mapping steps.
        //            //Tanuj - BSB Hack for provider names that contain ";" which doesn't parse properly
        //            // within an ODBC connection string.  Just adds a truncated entry for it to match on later.
        //            if (sProviderName.IndexOf(";") > 0)
        //                OdbcDLLProviders.Add(sProviderName.Split(';')[0], sDriver);

        //        }//if
        //    }//foreach
        //}//method: InstalledDatabaseDrivers()

        /// <summary>
        /// Gets the type of database driver for the database provider
        /// </summary>
        /// <param name="strDBProvider">string containing database provider information</param>
        /// <returns>enumeration for the database type</returns>
        public static eDatabaseType GetDatabaseDriverType(string strDBProvider)
        {
            eDatabaseType eDBDriverType = eDatabaseType.DBMS_IS_ODBC;

            if (IsSQLServerDriver(strDBProvider))
            {
                eDBDriverType = eDatabaseType.DBMS_IS_SQLSRVR;
            }
            else if (strDBProvider.Contains(DBDriverTypes.Oracle.ToUpper()))
            {
                eDBDriverType = eDatabaseType.DBMS_IS_ORACLE;
            }
            else if (strDBProvider.Contains(DBDriverTypes.Informix.ToUpper()))
            {
                eDBDriverType = eDatabaseType.DBMS_IS_INFORMIX;             
            }
            else if (strDBProvider.Contains(DBDriverTypes.Access.ToUpper()))
	        {
                eDBDriverType = eDatabaseType.DBMS_IS_ACCESS;
	        }
            else if (strDBProvider.Contains(DBDriverTypes.DB2.ToUpper()))
            {
                eDBDriverType = eDatabaseType.DBMS_IS_DB2;
            }

            return eDBDriverType;
        }//method: GetDatabaseDriverType()

        /// <summary>
        /// Determines whether or not a particular database provider
        /// is one of the supported SQL Server platforms
        /// </summary>
        /// <param name="strDBProvider">string containing database provider-specific information</param>
        /// <returns>boolean indicating whether or not the database driver is a supported SQL Server Driver</returns>
        private static bool IsSQLServerDriver(string strDBProvider)
        {
            bool blnIsSQLServer = false;

            if (strDBProvider.Contains(DBDriverTypes.SQLServerLegacy.ToUpper()) 
                || strDBProvider.Contains(DBDriverTypes.SQLServer2005.ToUpper())
                || strDBProvider.Contains(DBDriverTypes.SQLServer2008.ToUpper()))
            {
                blnIsSQLServer = true;
            }//if

            return blnIsSQLServer;
        }//method: IsSQLServerDriver()

        /// <summary>
        /// Translates an ODBC connection string into its native provider format
        /// </summary>
        /// <param name="strODBCConnString">string containing an ODBC connection string</param>
        /// <returns>native provider connection string for a target database</returns>
        /// <remarks>currently only supports SQL Server implementations</remarks>
        public static string GetNativeProviderConnectionString(string strODBCConnString)
        {
            bool blnIsSQLServer = false;


            if (strODBCConnString.Contains(DBDriverTypes.SQLServerLegacy)
                || strODBCConnString.Contains(DBDriverTypes.SQLServer2005)
                || strODBCConnString.Contains(DBDriverTypes.SQLServer2008))
            {
                blnIsSQLServer = true;
            }//if

            string strNativeConnString = string.Empty;

            if (blnIsSQLServer)
            {
                strNativeConnString = strODBCConnString.Replace(String.Format("Driver={{{0}}};", DBDriverTypes.SQLServerLegacy), string.Empty);
                strNativeConnString = strNativeConnString.Replace(String.Format("Driver={{{0}}};", DBDriverTypes.SQLServer2005), string.Empty);
                strNativeConnString = strNativeConnString.Replace(String.Format("Driver={{{0}}};", DBDriverTypes.SQLServer2008), string.Empty);
            } // if

            return strNativeConnString.Trim();
        } // method: GetNativeConnectionString

    }//class
}
