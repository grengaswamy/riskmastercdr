using System;
using System.Collections;
using System.Data;
using Oracle.DataAccess.Client;

namespace Riskmaster.Db
{
	/// <summary>
	/// DbReader wraps the Native .Net reader objects.  In addition it is used to create DbWriter objects 
	/// and has added support for the RMOptimistic locktype feature from DTGRocket.
	/// </summary>
	public class DbReader:IDataReader, IDisposable, IDataRecord, IEnumerable
	{
		private IDataReader m_anyReader=null; //Star of the Show
		internal DbCommand m_DbCommand=null;
        private string m_timeStamp = string.Empty, m_userStamp = string.Empty;
		private eLockType m_optLock = eLockType.None;
        private bool m_disposed;//=false;
		
         /// <summary>
         /// This DbReader constructor overload allows internal Data Layer classes to wrap a native reader and specify 
         /// the a command and the LockType to use..
         /// </summary>
         /// <param name="reader">The native reader to wrap.</param>
         /// <param name="command">The command that was used to create the reader dataset.  
         /// This is used internally only to have access through it to the db connection in order to preserve\manage it.</param>
         /// <param name="optLock">Riskmaster Locking type desired.</param>
         internal DbReader(IDataReader reader, DbCommand command, eLockType optLock)
		{
			m_anyReader=reader;
			m_DbCommand=command;
			m_optLock = optLock; 
		}
		
		/// <summary>
		/// This DbReader constructor overload allows internal Data Layer classes to wrap a native reader.
		/// </summary>
		/// <param name="reader">The native reader to wrap.</param>
		/// <param name="command">The command that was used to create the reader dataset.  
		/// This is used internally only to have access through it to the db connection in order to preserve\manage it.</param>
		internal DbReader(IDataReader reader, DbCommand command)
		{
			m_anyReader=reader;
			m_DbCommand=command;
		}

		#region Destructor Logic - BSB Note: Sensitive Code Do Not Modify.

//		~DbReader()
//		{
//			Dispose();
//		}
		/// <summary>
		/// Releases the resources used by the Component.
		/// Responsible for disposing of the command object and it's connection.
		/// </summary>
		/// 
		protected  void Dispose(bool disposing)
		{
			
			if(!m_disposed)
			{
				DbFactory.Dbg("Reader Dispose Called",DbTrace.Connect);
				if(disposing)
				{
					this.Close();
					m_anyReader=null;
					m_disposed = true;
				}
			}
		}

        /// <summary>
        /// Performs garbage collection/cleanup of existing Database connections
        /// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Closes the DbReader 0bject.
		/// </summary>
		 public void Close()
		{
			if(m_DbCommand!=null)
				if (m_DbCommand.Connection !=null)
				{
					if (m_DbCommand.Connection.State != ConnectionState.Closed) 
						m_DbCommand.Cancel();
					m_DbCommand.Dispose(true);
					m_DbCommand=null;
				}

			if(m_anyReader!=null && !m_anyReader.IsClosed)
				m_anyReader.Close();
		}
		#endregion
		/// <summary>
		/// Gets a value indicating the depth of nesting for the current row.
		/// </summary>
		  public int Depth
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader.Depth;
				throw new InvalidOperationException("No database reader set.");
			}
		}

		/// <summary>
		/// Gets a value indicating whether the data reader is closed.
		/// </summary>
		  public bool IsClosed
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader.IsClosed;
				return false;
			}
		}

		/// <summary>
		/// Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.
		/// </summary>
		  public int RecordsAffected
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader.RecordsAffected;
				throw new InvalidOperationException("No database reader set.");
			}
		}
		/// <summary>
		/// Returns a DataTable that describes the column metadata of the DbReader.
		/// </summary>
		/// <returns></returns>
		  public DataTable GetSchemaTable()
		{
			if(m_anyReader!=null)
				return m_anyReader.GetSchemaTable();
			throw new InvalidOperationException("No database reader set.");	
		}

		/// <summary>
		/// Advances the data reader to the next result, when reading the results of batch SQL statements.
		/// </summary>
		/// <returns></returns>
		  public bool NextResult()
		{
			if(m_anyReader!=null)
				return m_anyReader.NextResult();
			throw new InvalidOperationException("No database reader set.");	
		}

		/// <summary>
		/// Advances the IDataReader to the next record.
		/// </summary>
		/// <returns></returns>
		  public bool Read()
		{
			if(m_anyReader!=null)
				return m_anyReader.Read();
			throw new InvalidOperationException("No database reader set.");	
		}

		// IDataRecord Implementation

		/// <summary>
		/// Gets the number of columns in the current row.
		/// </summary>
		  public int FieldCount
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader.FieldCount;
				throw new InvalidOperationException("No database reader set.");	
			}
		}
		
		/// <summary>
		/// Gets the specified column. In C#, this property is the indexer for the IDataRecord class.
		/// </summary>
		  public object this[string name]
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader[name];
				throw new InvalidOperationException("No database reader set.");					
			}
		}

		/// <summary>
		/// Gets the specified column. In C#, this property is the indexer for the IDataRecord class.
		/// </summary>
		  public object this[int i]
		{
			get
			{
				if(m_anyReader!=null)
					return m_anyReader[i];
				throw new InvalidOperationException("No database reader set.");					
			}
		}

		/// <summary>
		/// Gets the value of the specified column as a Boolean.
		/// </summary>
		/// <param name="i">The zero-based column ordinal.</param>
		/// <returns>The value of the column.</returns>
		  public bool GetBoolean(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return false;
				else
				{
					object o=null;
					o = m_anyReader.GetValue(i);
					if(o.GetType().Name!="Boolean")
						return (o.ToString()) != "0";
//							return true;
//						else
//							return false;

					return m_anyReader.GetBoolean(i);
				}
			}
			throw new InvalidOperationException("No database reader set.");		
		}

		/// <summary>
		/// Gets the value of the specified column as a Boolean.
		/// </summary>
		/// <param name="name">The name of the column.</param>
		/// <returns>The value of the column.</returns>
		  public bool GetBoolean(string name)
		{
			return GetBoolean(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the 8-bit unsigned integer value of the specified column.
		/// </summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The 8-bit unsigned integer value of the specified column.</returns>
		  public byte GetByte(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetByte(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the 8-bit unsigned integer value of the specified column.
		/// </summary>
		/// <param name="name">The name of the column. </param>
		/// <returns>The 8-bit unsigned integer value of the specified column.</returns>
		  public byte GetByte(string name)
		{
			return GetByte(GetOrdinal(name));
		}

		/// <summary>
		/// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
		/// </summary>
		/// <param name="i">The zero-based column ordinal.</param>
		/// <param name="fieldOffset">The index within the field from which to begin the read operation. </param>
		/// <param name="buffer">The buffer into which to read the stream of bytes. </param>
		/// <param name="bufferoffset">The index for buffer to begin the read operation. </param>
		/// <param name="length">The number of bytes to read. </param>
		/// <returns></returns>
		  public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetBytes(i,fieldOffset,buffer,bufferoffset,length);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
		/// </summary>
		/// <param name="i">The zero-based column ordinal.</param>
		/// <returns></returns>
		 public byte[] GetAllBytes(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return null;
				else
				{
					int bufferlength= (int) m_anyReader.GetBytes(i,0,null,0,0);
					byte[] buffer=new byte[bufferlength];
					m_anyReader.GetBytes(i,0,buffer,0,bufferlength);
					return buffer;
				}
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
		/// </summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="fieldOffset">The index within the field from which to begin the read operation. </param>
		/// <param name="buffer">The buffer into which to read the stream of bytes. </param>
		/// <param name="bufferoffset">The index for buffer to begin the read operation. </param>
		/// <param name="length">The number of bytes to read. </param>
		/// <returns></returns>
		  public long GetBytes(string name, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			return GetBytes(GetOrdinal(name),fieldOffset,buffer,bufferoffset,length);
		}

		/// <summary>
		/// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
		/// </summary>
		/// <param name="name">The name of the column.</param>
		/// <returns></returns>
		  public byte[] GetAllBytes(string name)
		{
			return GetAllBytes(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the character value of the specified column.
		/// </summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The character value of the specified column.</returns>
		  public char GetChar(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return char.MinValue;
				else
					return m_anyReader.GetChar(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the character value of the specified column.
		/// </summary>
		/// <param name="name">The name of the column. </param>
		/// <returns>The character value of the specified column.</returns>
		  public char GetChar(string name)
		{
			return GetChar(GetOrdinal(name));
		}

		/// <summary>
		/// Reads a stream of characters from the specified column offset into the buffer as an array starting at the given buffer offset.
		/// </summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <param name="fieldoffset">The index within the row from which to begin the read operation.</param>
		/// <param name="buffer">The buffer into which to copy data.</param>
		/// <param name="bufferoffset">The index for buffer to begin the read operation. </param>
		/// <param name="length">The maximum number of characters to read.</param>
		/// <returns></returns>
		  public long GetChars(int i,long fieldoffset,char[] buffer,int bufferoffset,int length)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetChars(i,fieldoffset,buffer,bufferoffset,length);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Reads a stream of characters from the specified column offset into the buffer as an array starting at the given buffer offset.
		/// </summary>
		/// <param name="name">Field Name</param>
		/// <param name="fieldoffset">The index within the row from which to begin the read operation.</param>
		/// <param name="buffer">The buffer into which to copy data.</param>
		/// <param name="bufferoffset">The index for buffer to begin the read operation.</param>
		/// <param name="length">The maximum number of characters to read.</param>
		/// <returns></returns>
		  public long GetChars(string name,long fieldoffset,char[] buffer,int bufferoffset,int length)
		{
			return GetChars(name,fieldoffset,buffer,bufferoffset,length);
		}

		/// <summary>
		/// Gets an IDataReader to be used when the field points to more remote structured data.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>An IDataReader to be used when the field points to more remote structured data.</returns>
		  public IDataReader GetData(int i)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetData(i);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets an IDataReader to be used when the field points to more remote structured data.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>An IDataReader to be used when the field points to more remote structured data.</returns>
		  public IDataReader GetData(string name)
		{
			return GetData(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the data type information for the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The data type information for the specified field.</returns>
		  public string GetDataTypeName(int i)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetDataTypeName(i);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the data type information for the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The data type information for the specified field.</returns>
		  public string GetDataTypeName(string name)
		{
			return GetDataTypeName(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the date and time data value of the spcified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The date and time data value of the spcified field.</returns>
		  public DateTime GetDateTime(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return DateTime.MinValue;
				else
					return m_anyReader.GetDateTime(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the date and time data value of the spcified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The date and time data value of the spcified field.</returns>
		  public DateTime GetDateTime(string name)
		{
			return GetDateTime(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the fixed-position numeric value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The fixed-position numeric value of the specified field.</returns>
		  public decimal GetDecimal(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetDecimal(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the fixed-position numeric value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The fixed-position numeric value of the specified field.</returns>
		  public decimal GetDecimal(string name)
		{
			return GetDecimal(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the double-precision floating point number of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The double-precision floating point number of the specified field.</returns>
		  public double GetDouble(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
				{
					if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
					{	
						System.Diagnostics.Trace.WriteLine(m_anyReader.GetFieldType(i).Name);
						if(m_anyReader.GetFieldType(i).Name == "Decimal") 
							try{return Convert.ToDouble(m_anyReader.GetDecimal(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int32") 
							try{return Convert.ToDouble(m_anyReader.GetInt32(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int64") 
							try{return Convert.ToDouble(m_anyReader.GetInt64(i));}
							catch{return 0;}
					}
					return m_anyReader.GetDouble(i);
				}
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the double-precision floating point number of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The double-precision floating point number of the specified field.</returns>
		  public double GetDouble(string name)
		{
			return GetDouble(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the Type information corresponding to the type of Object that would be returned from GetValue.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The Type information corresponding to the type of Object that would be returned from GetValue.</returns>
		  public Type GetFieldType(int i)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetFieldType(i);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the Type information corresponding to the type of Object that would be returned from GetValue.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The Type information corresponding to the type of Object that would be returned from GetValue.</returns>
		  public Type GetFieldType(string name)
		{
			return GetFieldType(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the single-precision floating point number of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The single-precision floating point number of the specified field.</returns>
		  public float GetFloat(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetFloat(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the single-precision floating point number of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The single-precision floating point number of the specified field.</returns>
		  public float GetFloat(string name)
		{
			return GetFloat(GetOrdinal(name));
		}

		/// <summary>
		/// Returns the guid value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The guid value of the specified field.</returns>
		  public Guid GetGuid(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return Guid.Empty;
				else
					return m_anyReader.GetGuid(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Returns the guid value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The guid value of the specified field.</returns>
		  public Guid GetGuid(string name)
		{
			return GetGuid(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the 16-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The 16-bit signed integer value of the specified field.</returns>
		  public short GetInt16(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
				{
					if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_ACCESS)
					{	
						object o=null;
						o = m_anyReader.GetValue(i);
						if(o.GetType().Name=="Boolean")
							if((bool) o)
								return 1;
							else
								return 0;
					}
					//BSB 12.08.2004 Oracle database fielding for logical Booleans often goes from 
					// small int (16 bit integer) in SQL server to a flavor of decimal in Oracle.
					// To support code expecting the "small int" to be available via GetInt16 in all data sources,
					// we'll coerce it here if Oracle.
					if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
					{	
						//System.Diagnostics.Trace.WriteLine(m_anyReader.GetFieldType(i).Name);
						if(m_anyReader.GetFieldType(i).Name == "Decimal") 
							try{return Convert.ToInt16(m_anyReader.GetDecimal(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int32") 
							try{return Convert.ToInt16(m_anyReader.GetInt32(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int64") 
							try{return Convert.ToInt16(m_anyReader.GetInt64(i));}
							catch{return 0;}
					}
					if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
					{	
						if(m_anyReader.GetFieldType(i).Name == "Boolean")
						{
							try{return Convert.ToInt16(m_anyReader.GetBoolean(i));}
							catch{return 0;}
						}
						else if(m_anyReader.GetFieldType(i).Name == "Int32")
							return Convert.ToInt16(m_anyReader.GetInt32(i));

					}
					return m_anyReader.GetInt16(i);
				}
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the 16-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The 16-bit signed integer value of the specified field.</returns>
		  public short GetInt16(string name)
		{
			return GetInt16(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the 32-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The 32-bit signed integer value of the specified field.</returns>
		  public int GetInt32(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
				{
					//BSB 12.08.2004 Oracle database fielding for logical Booleans often goes from 
					// small int (16 bit integer) in SQL server to a flavor of decimal in Oracle.
					// To support code expecting the "small int" to be available via GetInt16 in all data sources,
					// we'll coerce it here if Oracle.
					if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
					{	
						//System.Diagnostics.Trace.WriteLine(m_anyReader.GetFieldType(i).Name);
						if(m_anyReader.GetFieldType(i).Name == "Decimal") 
							try{return Convert.ToInt32(m_anyReader.GetDecimal(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Double") 
							try{return Convert.ToInt32(m_anyReader.GetDouble(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int64") 
							try{return Convert.ToInt32(m_anyReader.GetInt64(i));}
							catch{return 0;}
						if(m_anyReader.GetFieldType(i).Name == "Int16") 
							try{return Convert.ToInt32(m_anyReader.GetInt16(i));}
							catch{return 0;}
					}
					//Mukul Added 2/22/2007:MITS 8867:There are many tables where columns are smallint and in application we read it through
					//GetInt32 so,condition added if its a smallint then it will convert to int32
					else if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
					{
						if(m_anyReader.GetFieldType(i).Name == "Int16") 
							try{return Convert.ToInt32(m_anyReader.GetInt16(i));}
							catch{return 0;}
					}
					return m_anyReader.GetInt32(i);
				}
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the 32-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The 32-bit signed integer value of the specified field.</returns>
		  public int GetInt32(string name)
		{
			return GetInt32(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the 32-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="i">Field Index.</param>
		/// <returns></returns>
		  public int GetInt(int i)
		{
			if(m_anyReader is System.Data.Odbc.OdbcDataReader ||
			(m_anyReader is OracleDataReader))
			{
				if(m_anyReader!=null)
				{
					if(m_anyReader.IsDBNull(i))
						return 0;
					Type t=m_anyReader.GetFieldType(i);
					if(t==typeof(decimal))
						return (int)m_anyReader.GetDecimal(i);
					else if(t==typeof(System.Double))
						return (int)m_anyReader.GetDouble(i);
					else if(t==typeof(System.Int64))
						return (int) m_anyReader.GetInt64(i);
                    else
						return m_anyReader.GetInt32(i);
				}
			}
			else
			{
				if(m_anyReader!=null)
				{
					if(m_anyReader.IsDBNull(i))
						return 0;
					else
						return m_anyReader.GetInt32(i);
				}
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the 32-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="name">Field Name</param>
		/// <returns></returns>
		  public int GetInt(string name)
		{
			return GetInt(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the 64-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The 64-bit signed integer value of the specified field.</returns>
		  public long GetInt64(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
					return 0;
				else
					return m_anyReader.GetInt64(i);
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the 64-bit signed integer value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The 64-bit signed integer value of the specified field.</returns>
		  public long GetInt64(string name)
		{
			return GetInt64(GetOrdinal(name));
		}

		/// <summary>
		/// Gets the name for the field to find.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The name of the field or the empty string (""), if there is no value to return.</returns>
		  public string GetName(int i)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetName(i);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Return the index of the named field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The index of the named field.</returns>
		  public int GetOrdinal(string name)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetOrdinal(name);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the string value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The string value of the specified field.</returns>
		  public string GetString(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_anyReader.IsDBNull(i))
				{
                    return string.Empty;
				} // if
                else
                {
                    return m_anyReader.GetString(i);
                } // else
			}
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Gets the string value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The string value of the specified field.</returns>
		  public string GetString(string name)
		{
			return GetString(GetOrdinal(name));
		}

		/// <summary>
		/// Return the value of the specified field.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>The Object which will contain the field value upon return.</returns>
		  public object GetValue(int i)
		{
			if(m_anyReader!=null)
			{
				if(m_DbCommand.Connection.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
				{	
					// Convert decimal to double
                    if (m_anyReader.GetFieldType(i).Name == "Decimal")
                    {
                        //Raman :04/14/2009 In cases where no data is there in DB then we DO NOT WANT to return 0.0
                        if(m_anyReader.GetValue(i) is DBNull)
                        {
                            return m_anyReader.GetValue(i);
                        }
                        
                        //Double will not accommodate any time whole number part is more
                        // than 15 for ex 1234567890123456 or total length exceeds
                        // 16 including decimal for ex 12345.1234512345
                        // So assuming RM behaviour is alright till now
                        // we just need to handle the big whole number conversion
                        // Since double conversions are still reqd for perf reasons
                        // so will still keep on doing it
                        // long can only accommodate till 18 digits
                        // ex 999999999999999999. For MITS 11052 by Shivendu.
                        string streqv = string.Empty;
                        decimal d = 0;
                        try
                        {
                            d = m_anyReader.GetDecimal(i);
                            streqv = d.ToString();
                        }
                        catch { return (object)(double)0.0; }

                        if (streqv != null && streqv.IndexOf(".") < 0 && streqv.Length > 15)
                        {
                            //try to accommodate this big whole number in long
                            try { return (object)(long)(d); }
                            catch { return (object)(long)0; }
                        }
                        try { return (object)Convert.ToDouble(d); }
                        catch { return (object)(double)0.0; }
                    }                                            
					// Convert int64 to int32
					if(m_anyReader.GetFieldType(i).Name == "Int64") 
						try{return (object) Convert.ToInt32(m_anyReader.GetInt64(i));}
						catch{return (object) (int) 0;}
				}

				return m_anyReader.GetValue(i);
			}
			else
				throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Return the value of the specified field.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>The Object which will contain the field value upon return.</returns>
		  public object GetValue(string name)
		{
			return GetValue(GetOrdinal(name));
		}

		/// <summary>
		/// Gets all the attribute fields in the collection for the current record.
		/// </summary>
		/// <param name="values">An array of Object to copy the attribute fields into. </param>
		/// <returns>The number of instances of Object in the array.</returns>
		  public int GetValues(object[] values)
		{
			if(m_anyReader!=null)
				return m_anyReader.GetValues(values);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Return whether the specified field is set to null.
		/// </summary>
		/// <param name="i">The index of the field to find. </param>
		/// <returns>true if the specified field is set to null, otherwise false.</returns>
		  public bool IsDBNull(int i)
		{
			if(m_anyReader!=null)
				return m_anyReader.IsDBNull(i);
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Return whether the specified field is set to null.
		/// </summary>
		/// <param name="name">The name of the field to find. </param>
		/// <returns>true if the specified field is set to null, otherwise false.</returns>
		  public bool IsDBNull(string name)
		{
			return IsDBNull(GetOrdinal(name));
		}

		
		/// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
		IEnumerator IEnumerable.GetEnumerator()
		{
			if(m_anyReader!=null)
				return ((IEnumerable)m_anyReader).GetEnumerator();
			throw new InvalidOperationException("No database reader set.");
		}

		/// <summary>
		/// Returns the type of the database connection used.
		/// </summary>
		  public eConnectionType ConnectionType
		{
			get
			{
				if(m_anyReader is System.Data.Odbc.OdbcDataReader)
					return eConnectionType.Odbc;
				if(m_anyReader is System.Data.SqlClient.SqlDataReader)
					return eConnectionType.ManagedSqlServer;
				if(m_anyReader is OracleDataReader)
					return eConnectionType.ManagedOracleServer;

				throw new System.Exception("Unknown connection type wrapped in DbReader.");
				//				if(m_anyReader is DB2DataReader)
				//					return eConnectionType.Odbc;
				
			}
		}

		
         /// <summary>
         /// CreateWriterWithData creates a writer based on the schema of this data reader and populates it's field collection 
         /// with the current row's data.  The locking option can be specified as well.
         /// </summary>
         /// <param name="optLock">Specifies the locking type for the writer.</param>
         /// <remarks>Note that the Date Time Stamp fields must be present on this reader before Optimistic locking can be 
         /// actually be used.</remarks>
          public DbWriter CreateWriterWithData(eLockType optLock){return new DbWriter(this,true,optLock);}

		///<summary>
		/// CreateWriter creates a writer based on the schema of this data reader and does NOT populates it's field collection 
		/// with the current row's data.  The locking option is defaulted to None.
		/// </summary>
          public DbWriter CreateWriter(){return new DbWriter(this,false, eLockType.None);}
		
         /// <summary>Riskmaster.Db.DbReader.TimeStamp returns or sets the time stamp to be applied if a write is successfull.</summary>
         /// <value>String containing the TimeStamp value.(typically "YYYYMMDDHHmmSS")</value>
         /// <remarks>This value must be explicitly set for each update in order to use Optimistic Locking.</remarks>
          public string TimeStamp{get{return m_timeStamp;}set{ m_timeStamp = value;}}
		
         /// <summary>Riskmaster.Db.DbReader.UserStamp returns or sets the UserStamp to be applied if a write is successfull.</summary>
         /// <returns>String containing the .UserStamp value. (typically a login name) </returns>
		/// <remarks>This value must be explicitly set for each update in order to use Optimistic Locking.</remarks>
		 public string UserStamp{get{return m_userStamp;}set{ m_userStamp = value;}}
		
         /// <summary>Riskmaster.Db.DbReader.SourceTimeStamp will return the TimeStamp of the current 
         /// record that was in place when the record was read into this reader.</summary>
         /// <value>String containing the TimeStamp field value from the current ResultSet. (Typically in "YYYYMMDDHHmmSS" format.)</value>
		/// <remarks>Will return an empty string if RMOptimistic Locking is not enabled.
		///  Will raise an exception if Optimistic locking is enabled and the DTTM_RCD_LAST_UPD  Stamp field is not available.</remarks>
         /// <example>No example available.</example>
          public string SourceTimeStamp
		{
			get
			{
				if (m_optLock == eLockType.RMOptimistic)
					return (string)m_anyReader["DTTM_RCD_LAST_UPD"];
				else
					return "";
			}
		}
	}
}
