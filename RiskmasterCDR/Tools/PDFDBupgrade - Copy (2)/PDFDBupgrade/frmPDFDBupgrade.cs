﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RMXdBUpgradeWizard;
using System.Configuration;
using System.Xml;
using System.Security.Cryptography;
using System.IO;
using System.Threading;



namespace PDFDBupgrade
{
    public partial class Form1 : Form
    {
        public const string strDSNColumn = "DSN";
        public const string strDSNIDColumn = "DSNID";
        private string _errorCache;
        public static string conn = string.Empty;
        public static string RunningDSN = "";
        public static int iTotalQuery=1;
        public static Form1 _Form1;
        private static int iQuarter;
        private static int iHalf;
        private static int iThreeQuarters;
        private static int iBeforeComplete;
        private static int iBeforeHalf;
        private static int iBeforeThreeQuarters;
        private ListView ListViewWithToolTips;
        public Form1()
        {
            InitializeComponent();
            _Form1 = this;
        }

        
       

        private void Form1_Load(object sender, EventArgs e)
        {

            SetDefaultVaules();
            //prgBar.Visible = false;

            bool bflag = System.IO.File.Exists("connectionStrings.config");
            if (bflag)
            {
                GetConnection();
                if (!string.IsNullOrEmpty(conn) && DisplayDBUpgrade.VerifyConnection(conn))
                {
                    LoadDSNs(conn);
                }
                else
                {
                    Finalreport("Connectionstring not valid");
                }
            }
            else
            {
                string sNull=string.Empty;
                sNull = "The connectionStrings.config file does not exist";

               Finalreport(sNull);
               btnUpgrade.Enabled = false;

            }
        }

        private  bool LoadDSNs(string sConnString)
        {
            DataTable dtListItems = new DataTable();

            dtListItems.Columns.Add(strDSNColumn);
            dtListItems.Columns.Add(strDSNIDColumn);

            //clear list box of any previous items
            DSNList.Items.Clear();
            try
            {
                if (String.IsNullOrEmpty(sConnString))
                {
                    DisplayDBUpgrade.g_SecConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                }
                else
                {
                    DisplayDBUpgrade.g_SecConnectString = sConnString;
                }
                                
                using (var drDSNs = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetOnlyDSNs()))
                {
                    string strValue, strKey, strelipse;
                    string[] str;
                   
                    while (drDSNs.Read())
                    {
                        strValue  = drDSNs[DisplayDBUpgrade.strDSNColumn].ToString();
                        strKey = drDSNs[DisplayDBUpgrade.strDSNIDColumn].ToString();

                        DisplayDBUpgrade.dictLoadItems.Add(strValue, Convert.ToInt32(strKey.Trim()));


                        str = new string[2];
                        strelipse = strValue;
                        if (strValue.Count() > 25)
                        {
                            strelipse = strValue.Substring(0, 7) + "...";
                        }
                        str[0] = strelipse;
                        str[1] = strKey ;
                        ListViewItem lvi = new ListViewItem(str);
                        lvi.ToolTipText = strValue;
                        DSNList.Items.Add(lvi);                      
                        
                       
                    }
                }

                if (DSNList.Items.Count <= 0)
                {
                    _errorCache = "An error has occurred during population of DSNs." +
                                  Environment.NewLine + Environment.NewLine +
                                  "There are no datasources configured in the specified security database. " +
                                  "Please correct and try again later.";
                    ErrorDisplay(_errorCache, String.Empty);

                    return false;
                }

                DSNList.SelectedItems.Clear();
               

                return true;
            }
            catch (Exception ex)
            {
                _errorCache = "An error has occurred during population of DSNs." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused by not being able to connect to the security database, " +
                              "please double check connection information before trying again.";
                string sNull = ex.Message +" StackTrace " + ex.StackTrace;
                ErrorDisplay(_errorCache, ex.Message +" StackTrace " + ex.StackTrace);

                return false;
            }
        }
        private static void ErrorDisplay(string sError, string sException)       
        {       
            
        }
        private  string ValidateselectionDSN(int selecteddsncount)
        {
             string errorReport = string.Empty;
            if (selecteddsncount != 0)            
            {          
                int iFailed = 0;
                string sErrorMsg = String.Empty;

                //reset lists before proceeding
                DisplayDBUpgrade.dictFailedItems.Clear();
                DisplayDBUpgrade.dictPassedItems.Clear();
                //lvFailed.Items.Clear();
                //lvPassed.Items.Clear();
                string FailedDSNs = "";

                //test each selected connection for connectivity
                foreach (var kvp in DisplayDBUpgrade.dictSelectedItems)
                {
                    
                    //try connection
                    DisplayDBUpgrade.bAborted = DisplayDBUpgrade.SelectDatabase(kvp.Value, ref sErrorMsg);
                    
                    //handle failed connection
                    if (!DisplayDBUpgrade.bAborted)
                    {
                        DisplayDBUpgrade.dictFailedItems.Add(kvp.Key, kvp.Value);
                        iFailed++;
                        if (FailedDSNs == "")
                        {
                            FailedDSNs = iFailed + ". " + kvp.Key;
                        }
                        else
                        {
                            FailedDSNs = FailedDSNs + Environment.NewLine + iFailed + ". " + kvp.Key;
                        }
                        //lvFailed.Items.Add(kvp.Key, sErrorMsg);
                        
                        continue;
                    }

                    //handle passed connections
                    DisplayDBUpgrade.dictPassedItems.Add(kvp.Key, kvp.Value);
                    
                    //lvPassed.Items.Add(kvp.Key);
                    
                }

                //display approriate navigation message
                if (iFailed == DisplayDBUpgrade.dictSelectedItems.Count)
                {
                    //errorReport = "<font color='red'>Connections to all of your selections have failed, press <b>Back</b> to retry, or <b>Cancel</b> to exit.</font>";
                    //errorReport = "Connections to all of your selections have failed";
                    errorReport = "Unable to connect to the following database(s) : " + Environment.NewLine + FailedDSNs + ".";
                     
                   
                }
                else if (iFailed > 0 && iFailed != DisplayDBUpgrade.dictSelectedItems.Count)
                {
                    errorReport = "Unable to connect to the following database(s) : " + Environment.NewLine + FailedDSNs + ".";
                                        
                }
                else
                {
                   
                    UpgradeScripts.UpgradeRMBase("generic.sql");
                    
                    errorReport="";

                }
                
            }
            return errorReport;

            
        }

        public void visualizeDBProces(string CurrentDSN, string QueryStatus)
        {
            this.prgDBProcess.Visible = true;
            RunningDSN = Convert.ToString(CurrentDSN);
            if (QueryStatus == "Completed") { this.prgDBProcess.Value = 100; this.Refresh(); Thread.Sleep(20000); }
            if (QueryStatus == "Started") this.prgDBProcess.Value = 10;
            this.lblDSNProcess.Text = "Upgrading:     " + CurrentDSN;
            this.Refresh();
            if (QueryStatus == "Started") Thread.Sleep(10000);
            //DisplayProcesText(CurrentDSN + " : " + QueryStatus);
            //MessageBox.Show(lblDSNProcess.Text);
        }
        public void visualizeDBProces(int iQueryCount, string QueryStatus)
        {
            this.lblDSNProcess.Text = "Upgrading:     " + RunningDSN;
            this.prgDBProcess.Value = 15;
            this.Refresh();
            //DisplayProcesText(RunningDSN + " : " + QueryStatus);
           //MessageBox.Show(lblDSNProcess.Text);
            iTotalQuery = iQueryCount;
            iHalf = iTotalQuery / 2;
            iQuarter = iTotalQuery / 4;
            iThreeQuarters = iHalf + iQuarter;
            iBeforeHalf = iQuarter + (iQuarter / 2);
            iBeforeThreeQuarters = iHalf + (iQuarter / 2);
            iBeforeComplete = iThreeQuarters + (iQuarter / 2);
        }
        public void visualizeDBProces(int ProgressCount)
        {
            if (ProgressCount < iQuarter)
            {
                this.prgDBProcess.Value = 15;
            }
            else if (iQuarter == ProgressCount)
            {
                this.prgDBProcess.Value = 25;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iBeforeHalf == ProgressCount)
            {
                this.prgDBProcess.Value = 35;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iBeforeHalf+20 == ProgressCount)
            {
                this.prgDBProcess.Value = 40;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iHalf == ProgressCount)
            {
                this.prgDBProcess.Value = 50;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iHalf+20 == ProgressCount)
            {
                this.prgDBProcess.Value = 55;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iBeforeThreeQuarters == ProgressCount)
            {
                this.prgDBProcess.Value = 60;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iThreeQuarters == ProgressCount)
            {
                this.prgDBProcess.Value = 75;
                this.Refresh();
                Thread.Sleep(3000);
            }
            else if (iBeforeComplete == ProgressCount)
            {
                this.prgDBProcess.Value = 99;
                this.Refresh();
                Thread.Sleep(1000);
                
            }
            //else
            //{
                
            //    this.Refresh();
            //    //Thread.Sleep(2000);
                 
            //}
                      
        }
        public void visualizeDBQuery(string SqlQuery)
        {
           
        }
       
        public void DisplayProcesText(string DSNProcessStatus)
        {
            lblDSNProcess.Text = DSNProcessStatus;
        }

        private void btnUpgrade_Click(object sender, EventArgs e)
        {
            txtMsg.Text = "";
            txtMsg.Refresh();
            string statusMessage;
            //const string message ="Are you sure that you would like to Start the DSN upgrade?";
            const string message = "Please be ABSOLUTELY certain that a backup of your database or database server has been performed before proceeding. Are you sure you want to proceed with the upgrade process? Computer Sciences Corporation cannot be held responsible for unauthorized or unapproved use of this program.";
            const string caption = "Confirm Upgrade";
            if (DSNList.CheckedItems.Count > 0)
            {
                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);

                // If the Yes button was pressed ... 
                if (result == DialogResult.Yes)
                {
                    btnUpgrade.Enabled = false;
                    DSNList.Enabled = false;

                    DSNList.MultiSelect = false;
                    btnCancel.Text = "Cancel";


                    //clear out any previously added datasources
                    DisplayDBUpgrade.dictSelectedItems.Clear();
                    int dsncount = 0;
                    
                    //dsncount = DSNList.SelectedItems.Count;

            dsncount = DSNList.CheckedItems.Count;

                    if (dsncount > 0)
                    {
                        //foreach (ListViewItem lvi in DSNList.SelectedItems)
                        foreach (ListViewItem lvi in DSNList.CheckedItems)
                        {
                            DisplayDBUpgrade.dictSelectedItems.Add(lvi.Text.Trim(), Convert.ToInt32(lvi.SubItems[1].Text));
                        }
                    }

                    if (dsncount == 0)
                    {
                        statusMessage = "DSN is not selected";
                    }
                    else
                    {
                        statusMessage = ValidateselectionDSN(dsncount);
                    }
                    //Finalreport(statusMessage);
                    if (statusMessage != "")
                    {
                        var results = MessageBox.Show("Database upgradation process could not start.", "RISKMASTER",
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Error);

                        // If the Ok button was pressed ... 
                        if (results == DialogResult.OK)
                        {
                            Finalreport(statusMessage);
                        }
                    }
                    else
                    {
                        var results = MessageBox.Show("Database(s) upgraded successfully.", "RISKMASTER",
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Information);

                        // If the Ok button was pressed ... 
                        if (results == DialogResult.OK)
                        {
                            //DSNList.Enabled = true;
                            Finalreport(statusMessage);
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                }

            }
            else
            {
                statusMessage = "DSN is not selected";
                Finalreport(statusMessage);
            }
           
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        private void Finalreport(string report)
        {
            string status = report.ToString();
            txtMsg.BackColor = SystemColors.Control;
            int MaxChars = 130;

            if (status != "")
            {
               
                    DSNList.Enabled = true;
                    btnCancel.Text = "Cancel";

                    btnUpgrade.Enabled = true;
                   
                    txtMsg.Text = status;

                    if (txtMsg.Text.Count() > MaxChars)
                        txtMsg.ScrollBars = ScrollBars.Vertical;
                    else
                        txtMsg.ScrollBars = ScrollBars.None;
                   
                    txtMsg.ForeColor = Color.Red;
                    txtMsg.MinimumSize = new Size(200, 20);
                   
                
                
            }
            else
            {
                txtMsg.ScrollBars = ScrollBars.None;
                txtMsg.Text = "";                

                lblDSNProcess.Text = "";
                this.prgDBProcess.Minimum = 0;
                this.prgDBProcess.Visible = false;

                DSNList.Enabled = true;
                btnCancel.Text = "Finish";//DSNList_ItemChecked
                //btnSelecanotherDSN.Visible = true;

                btnUpgrade.Enabled = true;
              
                //txtMsg.ForeColor = Color.Green;
                //txtMsg.Text = "Database(s) upgraded successfully.";
            }
            
            
        }

        private void SetDefaultVaules()
        {
            
            txtMsg.Text = "";
            //lblDSNProcess.Text = "Bala";
            lblDSNProcess.Text = "";
            this.prgDBProcess.Minimum = 0;
            this.prgDBProcess.Maximum = 100;
            this.prgDBProcess.Visible = false;
            

        }

        private void DSNList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
            //txtMsg.Text = "";
            //btnCancel.Text = "Cancel";
            //txtMsg.ScrollBars = ScrollBars.None;

        }
        private void GetConnection()
        {
            XmlTextReader reader = new XmlTextReader("connectionStrings.config");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            XmlElement root = doc.DocumentElement;
            XmlNodeList xnList = doc.SelectNodes("/connectionStrings");
            XmlNode xmlEncryptedNode = xnList[0];
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), " xmlEncryptedNode.InnerXml : " + xmlEncryptedNode.InnerXml.ToString() + "\r\n\n\n");
            string sChildElement = xmlEncryptedNode.FirstChild.Name;
            if (sChildElement.ToLower() == "encrypteddata")
            {
                XmlNode xmlEncryptedData = xmlEncryptedNode.SelectSingleNode("EncryptedData");
                DpapiProtectedConfigurationProvider objDpapi = new DpapiProtectedConfigurationProvider();
                XmlNode xmlDecryptedData = objDpapi.Decrypt(xmlEncryptedData);
                foreach (XmlNode chldNode in xmlDecryptedData.ChildNodes)
                {
                    if (!(chldNode.Attributes == null))
                    {
                        if (chldNode.Attributes["name"].Value == "RMXSecurity")
                        {
                            conn = chldNode.Attributes["connectionString"].Value;
                            break;
                        }
                    }

                }
            }
            else if (sChildElement.ToLower() == "add")
            {
                reader = new XmlTextReader("connectionStrings.config");
                while (reader.Read())
                {
                    if (reader.GetAttribute("name") == "RMXSecurity")
                        conn = reader.GetAttribute("connectionString");
                    if (!string.IsNullOrEmpty(conn)) break;
                }
            }
        }

        private void tmrProgress_Tick(object sender, EventArgs e)
        {
            this.prgDBProcess.Increment(1);
        }

        private void DSNList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            //if (btnCancel.Text == "Finish")
            //e.Item.Checked = false;
            txtMsg.ScrollBars = ScrollBars.None;
            txtMsg.Text = "";
            btnCancel.Text = "Cancel";

            lblDSNProcess.Text = "";
            this.prgDBProcess.Minimum = 0;            
            this.prgDBProcess.Visible = false;
        }

        private void DSNList_ItemMouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            //ListViewWithToolTips = new ListView();
            //ListViewItem item1WithToolTip = new ListViewItem(e.Item.ToString());

            //item1WithToolTip.ToolTipText = "This is the item tooltip.";
            //this.DSNList.Items.Add(item1WithToolTip);



        }

        //private void btnSelecanotherDSN_Click(object sender, EventArgs e)
        //{
        //    DSNList.Enabled = true;
        //    btnSelecanotherDSN.Visible = false;
        //    btnUpgrade.Enabled = true;
        //    btnCancel.Text = "Cancel";




        //}


    }

}
