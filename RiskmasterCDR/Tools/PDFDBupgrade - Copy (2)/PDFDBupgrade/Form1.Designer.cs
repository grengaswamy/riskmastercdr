﻿namespace PDFDBupgrade
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.DSNList = new System.Windows.Forms.ListView();
            this.prgDBProcess = new System.Windows.Forms.ProgressBar();
            this.lblDSNProcess = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Location = new System.Drawing.Point(50, 359);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(56, 23);
            this.btnUpgrade.TabIndex = 1;
            this.btnUpgrade.Text = "Upgrade";
            this.btnUpgrade.UseVisualStyleBackColor = true;
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(223, 359);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsg.Location = new System.Drawing.Point(47, 78);
            this.lblmsg.MaximumSize = new System.Drawing.Size(250, 0);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(169, 13);
            this.lblmsg.TabIndex = 3;
            this.lblmsg.Text = "Select Database(s) to Upgrade :";
            // 
            // DSNList
            // 
            this.DSNList.CheckBoxes = true;
            this.DSNList.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DSNList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.DSNList.HideSelection = false;
            this.DSNList.Location = new System.Drawing.Point(50, 94);
            this.DSNList.MultiSelect = false;
            this.DSNList.Name = "DSNList";
            this.DSNList.ShowItemToolTips = true;
            this.DSNList.Size = new System.Drawing.Size(232, 177);
            this.DSNList.TabIndex = 4;
            this.DSNList.UseCompatibleStateImageBehavior = false;
            this.DSNList.View = System.Windows.Forms.View.SmallIcon;
            this.DSNList.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.DSNList_ItemChecked);
            this.DSNList.ItemMouseHover += new System.Windows.Forms.ListViewItemMouseHoverEventHandler(this.DSNList_ItemMouseHover);
            this.DSNList.SelectedIndexChanged += new System.EventHandler(this.DSNList_SelectedIndexChanged_1);
            // 
            // prgDBProcess
            // 
            this.prgDBProcess.Location = new System.Drawing.Point(50, 292);
            this.prgDBProcess.Name = "prgDBProcess";
            this.prgDBProcess.Size = new System.Drawing.Size(232, 16);
            this.prgDBProcess.TabIndex = 5;
            // 
            // lblDSNProcess
            // 
            this.lblDSNProcess.AutoSize = true;
            this.lblDSNProcess.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDSNProcess.Location = new System.Drawing.Point(47, 323);
            this.lblDSNProcess.Name = "lblDSNProcess";
            this.lblDSNProcess.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDSNProcess.Size = new System.Drawing.Size(38, 13);
            this.lblDSNProcess.TabIndex = 6;
            this.lblDSNProcess.Text = "label1";
            // 
            // txtMsg
            // 
            this.txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMsg.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsg.Location = new System.Drawing.Point(12, 12);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.ReadOnly = true;
            this.txtMsg.Size = new System.Drawing.Size(309, 54);
            this.txtMsg.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 410);
            this.Controls.Add(this.txtMsg);
            this.Controls.Add(this.lblDSNProcess);
            this.Controls.Add(this.prgDBProcess);
            this.Controls.Add(this.DSNList);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpgrade);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RM Database Upgrade";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpgrade;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.ListView DSNList;
        private System.Windows.Forms.ProgressBar prgDBProcess;
        private System.Windows.Forms.Label lblDSNProcess;
        private System.Windows.Forms.TextBox txtMsg;
    }
}

