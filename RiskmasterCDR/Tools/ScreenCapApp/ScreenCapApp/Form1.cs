﻿namespace ScreenCapApp
{
    using System;
    using System.ComponentModel;
    using System.Configuration;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class Form1 : Form
    {
        private CheckBox chkEnabled;
        private Container components;
        private IntPtr nextClipboardViewer;
        private Label label1;
        private TextBox txtLocation;
        private TextBox txtPrefix;
        private Label label2;
        private RichTextBox richTextBox1;
        private IDataObject dataObject;
        private string str;
        private string str3;

        public Form1()
        {
            this.InitializeComponent();
            this.nextClipboardViewer = (IntPtr) SetClipboardViewer((int) base.Handle);
            txtLocation.Text = ConfigurationSettings.AppSettings["SaveLoc"];
        }

        [DllImport("User32.dll", CharSet=CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);
        private void DisplayClipboardData()
        {
            try
            {
                str = txtLocation.Text;
                str3 = txtPrefix.Text + string.Format("{0: # yyyy-MM-dd # hh-mm-ss-ff}", DateTime.Now) + ".jpg";
                if (this.chkEnabled.Checked)
                {
                    dataObject = new DataObject();
                    dataObject = Clipboard.GetDataObject();
                    if (dataObject.GetDataPresent(DataFormats.Bitmap))
                    {
                        ((Bitmap) dataObject.GetData(DataFormats.Bitmap)).Save(Path.Combine(str, str3), ImageFormat.Jpeg);
                        this.richTextBox1.Text = "Saved:" + str3;
                    }
                    dataObject = null;
                }
            }
            catch (Exception exception)
            {
                this.richTextBox1.Text = exception.ToString();
                dataObject = null;
                MessageBox.Show(exception.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            ChangeClipboardChain(base.Handle, this.nextClipboardViewer);
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(249, 43);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.WordWrap = false;
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Location = new System.Drawing.Point(13, 51);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(65, 17);
            this.chkEnabled.TabIndex = 1;
            this.chkEnabled.Text = "Enabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "location";
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(58, 77);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(179, 20);
            this.txtLocation.TabIndex = 3;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(58, 103);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(179, 20);
            this.txtPrefix.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Prefix";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(249, 129);
            this.Controls.Add(this.txtPrefix);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkEnabled);
            this.Controls.Add(this.richTextBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "CSC ScreenCap app";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        [STAThread]
        private static void Main()
        {
            Application.Run(new Form1());
        }

        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x308:
                    this.DisplayClipboardData();
                    SendMessage(this.nextClipboardViewer, m.Msg, m.WParam, m.LParam);
                    return;

                case 0x30d:
                    if (m.WParam == this.nextClipboardViewer)
                    {
                        this.nextClipboardViewer = m.LParam;
                        return;
                    }
                    SendMessage(this.nextClipboardViewer, m.Msg, m.WParam, m.LParam);
                    return;
            }
            base.WndProc(ref m);
        }
    }
}

