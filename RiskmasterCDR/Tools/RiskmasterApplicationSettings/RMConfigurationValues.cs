using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    /// <summary>
    /// Provides the names and values of the Registry Keys
    /// utilized by Riskmaster
    /// </summary>
    internal struct RiskmasterRegistryKeys
    {
        public static string SMServerKey = @"Software\DTG\SORTMASTER Server";
        public static string SMServerDSNValue = "DSN";
        public static string SMTPServerValue = "SMTPServer";
        public static string DTGSecurity32UserKey = @"Software\DTG\RISKMASTER\Security\EncDBUserID";
        public static string DTGSecurity32PasswordKey = @"Software\DTG\RISKMASTER\Security\EncDBPassword";
    }//struct

    /// <summary>
    /// Provides the names of the Global.asa
    /// Application settings stored in the Global.asa file
    /// </summary>
    internal struct GlobalASAAppSettings
    {
        public static string DTGSecurityAppSetting = "Application(APP_SECURITYDSN)";
        public static string WebFarmSessionAppSetting = "Application(APP_SESSIONDSN)";
        public static string SMServerAppSetting = "Application(APP_SMDSN)";
        public static string SMTPServerAppSetting = "Application(APP_SMTPSERVER)";
        public static string SecurityConnectionString = "Application(APP_SECURITYDSN)";
        public static string SessionConnectionString = "Application(APP_SESSIONDSN)";
    }//struct

    /// <summary>
    /// Provides the names of relevant elements
    /// that provide configuration information
    /// in the Riskmaster.config file
    /// </summary>
    internal struct RiskmasterConfigSettingElements
    {
        public static string RMSecurity = "SecuritySettings";
        public static string BIS = "BIS"; //structure is <BIS><BISURL>
        public static string BISXPath = "BIS/BISURL";
        public static string SMTPServer = "SMTPServer";
        public static string AppServer = "AppServer";
        public static string SMNetServer = "SMNetServer";
        public static string RMSession = "SessionDataSource";
        public static string PasswordPolicy = "PasswordPolicy";
        public static string Pabblo = "Pabblo";
        public static string IPAddress = "CommonWebServiceHandler/TrustedIPPool";  //Format is <CommonWebServiceHandler><TrustedIPPool><IP>
        public static string ReportServer = "ReportServer_ConnectionString";
        public static string Acrosoft = "Acrosoft"; //Format is <Acrosoft><child elements>
    }//struct

    
    /// <summary>
    /// Provides a set of commonly needed regular expressions
    /// that can be used to parse various Riskmaster Setings
    /// </summary>
    /// <remarks>
    /// Currenly provides access to the following Regular Expression Patterns
    /// 1.  URLs
    /// 2.  Application Settings
    /// 3.  Database Connection Strings
    /// </remarks>
    internal struct RMRegularExpressions
    {
        public static string strURL = @"^http://.+$";
        /// <summary>
        /// Allow any characters to precede the Application Name
        /// </summary>
        /// <remarks>
        /// The most common character to precede the Application Name will be 
        /// a tab character i.e. \t
        /// </remarks>
        public static string strApplication = @"Application\((.+\))\s+=\s+.+";
        /// <summary>
        /// Allow any characters to precede the Application Name
        /// and capture the relevant database connection string information
        /// </summary>
        public static string strApplicationConnString = @"Application\(.+\)\s=\s+""(?<CONNECTION_STRING>.*)""";

        /// <summary>
        /// Provides a means of parsing out a SQL Server connection string into its respective elements
        /// </summary>
        /// <remarks>This connection string is ODBC-compliant ONLY</remarks>
        public static string strSQLServerConnString = @"Driver=(?<DRIVER>.*);Server=(?<SERVER>.*);Database=(?<DATABASE>.*);UID=(?<UID>.*);PWD=(?<PWD>.*);";

        /// <summary>
        /// Provides a means of parsing out an Oracle connection string into its respective elements
        /// </summary>
        /// <remarks>This connection string is ODBC-compliant ONLY</remarks>
        public static string strOracleConnString = @"Driver=(?<DRIVER>.*);Server=(?<SERVER>.*);DBQ=(?<DATABASE>.*);UID=(?<UID>.*);PWD=(?<PWD>.*);";

        /// <summary>
        /// Allow any characters to precede the Application Name
        /// and capture the relevant database connection string information
        /// for relevant DSNs
        /// </summary>
        public static string strApplicationDSN = @"^.+Application\((?<APPLICATION_NAME>.+)\)\s+=\s+""DSN=(?<DSN_VALUE>.+);UID=(?<UID_VALUE>.+);PWD=(?<PWD_VALUE>.*);""$";


    }//struct RMRegularExpressions
}
