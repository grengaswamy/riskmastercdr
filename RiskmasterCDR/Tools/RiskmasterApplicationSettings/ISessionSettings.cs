using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface ISessionSettings
    {
        string SessionConnectionString
        {
            get;
            set;
        }//property: SessionConnectionString
    }
}
