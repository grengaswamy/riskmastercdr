using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface IDataSourceConnection
    {
         /// <summary>
        /// Gets and sets the Driver element of a 
        /// database connection string
        /// </summary>
        string Driver
        {
            get;
            set;
        } // property Driver

        /// <summary>
        /// Gets and sets the entirety of a
        /// database connection string
        /// </summary>
        string ConnectionString
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets whether or not the 
        /// specified connection string is an
        /// ODBC DSN Connection string
        /// </summary>
        bool IsODBCDSN
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the DSN element
        /// of a database connection string
        /// </summary>
        string DSN
        {
            get;
            set;
        } // property DSN

        /// <summary>
        /// Gets and sets the Server element
        /// of a database connection string
        /// </summary>
        string SERVER
        {
            get;
            set;
        } // property SERVER

        /// <summary>
        /// Gets and sets the Database element
        /// of a database connection string
        /// </summary>
        string DATABASE
        {
            get;
            set;
        } // property DATABASE

        /// <summary>
        /// Gets and sets the UID element (User ID)
        /// of a database connection string
        /// </summary>
        string UID
        {
            get;
            set;
        } // property UID

        /// <summary>
        /// Gets and sets the PWD (password) element
        /// of a database connection string
        /// </summary>
        string PWD
        {
            get;
            set;
        } // property PWD 
    }
}
