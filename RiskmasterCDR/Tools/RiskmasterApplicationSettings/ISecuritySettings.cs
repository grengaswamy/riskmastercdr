using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface ISecuritySettings
    {
        string SecurityUID
        {
            get;
            set;
        }

        string SecurityPwd
        {
            get;
            set;
        }

        string SecurityConnectionString
        {
            get;
            set;
        }//property: SecurityConnectionString
    }
}
