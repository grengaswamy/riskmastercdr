using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.ApplicationSettings
{
    /// <summary>
    /// Class for managing database connection strings
    /// and the individual connection string elements
    /// </summary>
    public class ConnectionStringManager : IDataSourceConnection
    {
        #region Private Class Member variables
        private bool m_blnIsODBCDSN = false;
        private string m_strUID;
        private string m_strDSN;
        private string m_strConnString;
        private string m_strDriver;
        private string m_strSERVER;
        private string m_strPWD;
        private string m_strDATABASE; 
        #endregion

        #region Class constructors
        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private ConnectionStringManager()
        {
            
        } // constructor


        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="strDBConnString">string containing the database connection string
        /// to be parsed</param>
        public ConnectionStringManager(string strDBConnString)
        {
            InitializeValues();
            m_strConnString = strDBConnString;

            //Call the Parse Connection String method
            //to populate all the relevant and required values
            ParseConnectionString(m_strConnString);
        } // constructor 
        #endregion

        #region IDataSourceConnection Members
            #region ConnectionString Properties

            /// <summary>
            /// Gets and sets the Driver element of a 
            /// database connection string
            /// </summary>
            public string Driver
            {
                get { return m_strDriver; }
                set { m_strDriver = value; }
            } // property Driver

            /// <summary>
            /// Gets and sets the entirety of a
            /// database connection string
            /// </summary>
            public string ConnectionString
            {
                get { return m_strConnString; }
                set { m_strConnString = value; }
            }


            /// <summary>
            /// Gets and sets whether or not the 
            /// specified connection string is an
            /// ODBC DSN Connection string
            /// </summary>
            public bool IsODBCDSN
            {
                get { return m_blnIsODBCDSN; }
                set { m_blnIsODBCDSN = value; }
            }

            /// <summary>
            /// Gets and sets the DSN element
            /// of a database connection string
            /// </summary>
            public string DSN
            {
                get { return m_strDSN; }
                set { m_strDSN = value; }
            } // property DSN

            /// <summary>
            /// Gets and sets the Server element
            /// of a database connection string
            /// </summary>
            public string SERVER
            {
                get { return m_strSERVER; }
                set { m_strSERVER = value; }
            } // property SERVER

            /// <summary>
            /// Gets and sets the Database element
            /// of a database connection string
            /// </summary>
            public string DATABASE
            {
                get { return m_strDATABASE; }
                set { m_strDATABASE = value; }
            } // property DATABASE

            /// <summary>
            /// Gets and sets the UID element (User ID)
            /// of a database connection string
            /// </summary>
            public string UID
            {
                get { return m_strUID; }
                set { m_strUID = value; }
            } // property UID

            /// <summary>
            /// Gets and sets the PWD (password) element
            /// of a database connection string
            /// </summary>
            public string PWD
            {
                get { return m_strPWD; }
                set { m_strPWD = value; }
            } // property PWD 
            #endregion 
        #endregion

        #region ConnectionString Methods
        /// <summary>
        /// Parse the specified connection string
        /// and assign the appropriate elements to the class properties
        /// </summary>
        /// <param name="strConnString">string containing the entirety of the database connection string</param>
        public void ParseConnectionString(string strConnString)
        {
            bool blnIsDSNConnString = false;

            //If the connection string contains double quotes 
            //surrounding the entire connection string
            strConnString = strConnString.Replace(@"""", string.Empty);

            //Determine which type of connection string is being used
            blnIsDSNConnString = RegExManager.IsMatch(strConnString, ConnectionStringRegExpressions.DSN_CONNECTION_STRING);

            //Set the connection string property
            this.ConnectionString = strConnString;

            if (blnIsDSNConnString)
            {
                //Specify that this Connection String is an ODBC DSN Connection string
                this.IsODBCDSN = true;

                //Capture all of the matches to the appropriate properties
                this.DSN = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.DSN_CONNECTION_STRING, ConnectionStringRegExpressions.DSN);
                this.UID = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.DSN_CONNECTION_STRING, ConnectionStringRegExpressions.UID);
                this.PWD = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.DSN_CONNECTION_STRING, ConnectionStringRegExpressions.PWD);
                
            } // if
            else
            {
                this.Driver = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, ConnectionStringRegExpressions.DRIVER);
                this.SERVER = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, ConnectionStringRegExpressions.SERVER);
                this.DATABASE = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, ConnectionStringRegExpressions.DATABASE);
                this.UID = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, ConnectionStringRegExpressions.UID);
                this.PWD = RegExManager.MatchGroups(strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, ConnectionStringRegExpressions.PWD);
            } // else
        } // method: ParseConnectionString

        /// <summary>
        /// Replaces a DSN connection string
        /// </summary>
        /// <param name="strUID">string containing the user name for the connection string</param>
        /// <param name="strPWD">string containing the password for the connection string</param>
        /// <returns>string containing the replaced DSN connection string</returns>
        public string ReplaceDSN(string strUID, string strPWD)
        {
            string strReplacedString = string.Empty;

            List<string> arrConnStrValues = new List<string>();

            //Build the Connection String elements for a DSN
            arrConnStrValues.Add(ConnectionStringRegExpressions.DSN_STRING);
            arrConnStrValues.Add(DSN);
            arrConnStrValues.Add(ConnectionStringRegExpressions.CONN_STRING_DELIMITER);
            arrConnStrValues.Add(ConnectionStringRegExpressions.UID_STRING);
            arrConnStrValues.Add(strUID);
            arrConnStrValues.Add(ConnectionStringRegExpressions.CONN_STRING_DELIMITER);
            arrConnStrValues.Add(ConnectionStringRegExpressions.PWD_STRING);
            arrConnStrValues.Add(strPWD);

            //If the password does not already contain a semi-colon at the end
            if (! strPWD.Contains(";"))
            {
                //Add a semi-colon to the end of the DSN string
                arrConnStrValues.Add(ConnectionStringRegExpressions.CONN_STRING_DELIMITER);
            } // if

            //Concatenate the individual elements of the connection string
            //to result in a final replaced connection string
            strReplacedString = string.Concat(arrConnStrValues.ToArray());

            //Clean up
            arrConnStrValues = null;

            //return the replaced connection string
            return strReplacedString;
        } // method: ReplaceDSN

        /// <summary>
        /// Replaces an OLEDB Connection string
        /// </summary>
        /// <returns></returns>
        private string ReplaceOLEDBConnectionString()
        {
           const string OLEDB_REPLACEMENT_CONNECTION_STRING="Driver={${DRIVER_NAME}};SERVER=${SERVER_VALUE};DATABASE=${DATABASE_VALUE};UID=${UID_VALUE};PWD=${PWD_VALUE}";

           return Regex.Replace(m_strConnString, ConnectionStringRegExpressions.OLEDB_CONNECTION_STRING, OLEDB_REPLACEMENT_CONNECTION_STRING);
        } // method: ReplaceOLEDBConnectionString



        /// <summary>
        /// Sets the new connection string
        /// by replacing it with the specified value
        /// </summary>
        /// <param name="strCurrrentString">string containing the current value of the database connection string</param>
        /// <param name="strReplacementString">string containing the replacement value for the database connection string</param>
        /// <returns>string containing the newly constructed/replaced database connection string</returns>
        public string ReplaceConnectionString(string strCurrrentString, string strReplacementString)
        {
            //Capture the old contents of the existing connection string
            string strOldConnString = this.ConnectionString;

            //Create a container for the new connection string
            string strNewConnString = string.Empty;

            //Perform a replacement of the string with the specified value
            strNewConnString = strOldConnString.Replace(strCurrrentString, strReplacementString);

            //Set the new connection string
            this.ConnectionString = strNewConnString;

            return strNewConnString;
        } // method: ReplaceConnectionString


        /// <summary>
        /// Initializes all member variables within the class
        /// </summary>
        private void InitializeValues()
        {
            m_blnIsODBCDSN = false;
            m_strUID = string.Empty;
            m_strDSN = string.Empty;
            m_strConnString = string.Empty;
            m_strDriver = string.Empty;
            m_strSERVER = string.Empty;
            m_strPWD = string.Empty;
            m_strDATABASE = string.Empty;
        } // method: InitializeValues 
        #endregion

	
    }//class ConnectionStringManager

    /// <summary>
    /// Contains a listing of all the Regular Expressions
    /// required for parsing Riskmaster connection string elements
    /// </summary>
    public struct ConnectionStringRegExpressions
    {
        /// <summary>
        /// Regular expression for ODBC DSN Connection string
        /// </summary>
        /// <remarks>
        /// Capture the Connection String elements into designated groups
        /// The Connection string can contain any character of choice excluding semi-colons 
        /// since this is used as a delimiter
        ///</remarks>
        ///<example>
        /// The connection string must be in the following format:
        /// DSN=MyDSN;UID=myuserid;PWD=mypwd [;] --The final semicolon is optional
        ///</example>
        public static string DSN_CONNECTION_STRING = @"^DSN=(?<DSN_VALUE>[^;]+);UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";

        /// <summary>
        /// Regular expression for OLEDB Connection String
        /// </summary>
        /// <remarks>
        /// Capture the Connection String elements into designated groups
        /// The Connection string can contain any character of choice excluding semi-colons 
        /// since this is used as a delimiter
        /// </remarks>
        /// <example>
        /// The connection string must be in the following format for SQL Server:
        /// Driver={SQL Server};Server=myserver;Database=mydatabase;UID=myuserid;PWD=mypwd
        /// The connection string must be in the following format for Oracle:
        /// Driver={Oracle in OraClient10g_home1};Server=myserver;DBQ=mydatabase;UID=myuserid;PWD=mypwd;
        /// </example>
        public static string OLEDB_CONNECTION_STRING = string.Format("{0}|{1}", RMRegularExpressions.strSQLServerConnString, RMRegularExpressions.strOracleConnString);


        #region Regular Expression Group Names
        public const string DRIVER = "DRIVER";
        public const string SERVER = "SERVER";
        public const string DATABASE = "DATABASE";
        public const string UID = "UID";
        public const string PWD = "PWD"; 
        #endregion

        public const string DSN = "DSN_VALUE";
        public const string DSN_STRING = "DSN=";
        public const string SERVER_STRING = "SERVER=";
        public const string DATABASE_STRING = "DATABASE=";
        public const string DBQ_STRING = "DBQ=";
        public const string UID_STRING = "UID=";
        public const string PWD_STRING = "PWD=";
        public const string CONN_STRING_DELIMITER = ";";
    }//struct
}
