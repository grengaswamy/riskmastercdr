using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface ISMTPServerSettings
    {
        string SMTPServer
        {
            get;
            set;
        }
    }
}
