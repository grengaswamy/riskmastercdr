﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Riskmaster.Application.ApplicationSettings")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Computer Sciences Corporation (CSC)")]
[assembly: AssemblyProduct("Riskmaster.Application.ApplicationSettings")]
[assembly: AssemblyCopyright("Copyright © Computer Sciences Corporation (CSC) 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

//Allow this assembly to be called from partially trusted callers
//rather than exclusively to strong-named assemblies
[assembly: AllowPartiallyTrustedCallers]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a00c8017-9ce0-4b7a-9883-b3106fe94986")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
