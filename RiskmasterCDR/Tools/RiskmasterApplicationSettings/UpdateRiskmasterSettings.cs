using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Riskmaster.Application.RiskmasterSettings;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.ApplicationSettings
{
    /// <summary>
    /// Updates all of the required Riskmaster Settings
    /// </summary>
    public class UpdateRiskmasterSettings : ISecuritySettings, ISMServerSettings, ISessionSettings, ISMTPServerSettings,
                                        IRMConfigSettings
    {

        /// <summary>
        /// Private class member variable for holding
        /// Sortmaster Server DSN information
        /// </summary>
        /// <remarks>This member variable has been primarily
        /// created as a holding area for SMServer information
        /// which is not correct in the ReportServer_ConnectionString
        /// element in Riskmaster.config</remarks>
        private string m_strSMServerDSN = string.Empty;

        #region Class constructors
        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private UpdateRiskmasterSettings()
        {

        } // class constructor

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="strGlobalASAPath"></param>
        /// <param name="strRMConfigPath"></param>
        public UpdateRiskmasterSettings(string strGlobalASAPath, string strRMConfigPath)
        {
            RiskmasterSettingsManager.GlobalASAPath = strGlobalASAPath;
            RiskmasterSettingsManager.RiskmasterConfigPath = strRMConfigPath;
        } // constructor 
        #endregion

        #region Update Riskmaster Settings Methods


        /// <summary>
        /// Loads the DTG Security32 and SMServer information
        /// </summary>
        /// <permission cref="">Requires access to the Local Registry on the system</permission>
        public void UpdateRegistrySettings()
        {
            RegistryConfiguration objRegConfig = new RegistryConfiguration();
            NameValueCollection nvCollSettings = new NameValueCollection();
            nvCollSettings.Add("EncDBUserID", RiskmasterRegistryKeys.DTGSecurity32UserKey);
            nvCollSettings.Add("EncDBUserValue", string.Empty);

            string strEncryptedUID = RMCryptography.EncryptString(this.SecurityUID);

            objRegConfig.WriteSetting(nvCollSettings, strEncryptedUID); 

            nvCollSettings.Clear();

            nvCollSettings.Add("EncDBUserPwd", RiskmasterRegistryKeys.DTGSecurity32PasswordKey);
            nvCollSettings.Add("EncDBPwdValue", string.Empty);

            string strEncryptedPwd = RMCryptography.EncryptString(this.SecurityPwd);

            objRegConfig.WriteSetting(nvCollSettings, strEncryptedPwd);

            nvCollSettings.Clear();

            nvCollSettings.Add("SMServerKey", RiskmasterRegistryKeys.SMServerKey);
            nvCollSettings.Add("SMSererDSNValue", RiskmasterRegistryKeys.SMServerDSNValue);

            string strSMServerDSN = objRegConfig.ReadSetting(nvCollSettings);
            ConnectionStringManager objConnMgr = new ConnectionStringManager(strSMServerDSN);

            //Replace the existing DSN with the new credentials
            string strNewSMServerDSN = objConnMgr.ReplaceDSN(this.SMServerUID, this.SMServerPwd);

            //Write the setting back to the Registry
            objRegConfig.WriteSetting(nvCollSettings, strNewSMServerDSN);

            //Update the SMTP Server
            nvCollSettings.Clear();

            nvCollSettings.Add("SMServerKey", RiskmasterRegistryKeys.SMServerKey);
            nvCollSettings.Add("SMTPServer", RiskmasterRegistryKeys.SMTPServerValue);

            //Write the setting back to the Registry
            objRegConfig.WriteSetting(nvCollSettings, m_strSMTPServer);

            //Clean up
            nvCollSettings = null;
            objRegConfig = null;
            objConnMgr = null;
        }

        /// <summary>
        /// Loads the Session DSN and SMTP Server
        /// </summary>
        /// /// <permission cref="">Requires access to read and write to the File System</permission>
        public void UpdateGlobalASASettings()
        {
            RiskmasterSettingsManager.SetGlobalASASetting("RMSecurity", GlobalASAAppSettings.DTGSecurityAppSetting, this.SecurityConnectionString);

            RiskmasterSettingsManager.SetGlobalASASetting("SessionDSN", GlobalASAAppSettings.WebFarmSessionAppSetting, this.SessionConnectionString);

            //Retrieve the updated SMServer DSN value
            string strReplacedSMServerDSN = UpdateSMServerDSN();

            //Update the class member variable with the new SMServer DSN
            m_strSMServerDSN = strReplacedSMServerDSN;

            //Update the SMServer DSN in the Global.asa
            RiskmasterSettingsManager.SetGlobalASASetting("SMServerDSN", GlobalASAAppSettings.SMServerAppSetting, strReplacedSMServerDSN);

            //Update the SMTP Server information
            RiskmasterSettingsManager.SetGlobalASASetting("SMTPServer", GlobalASAAppSettings.SMTPServerAppSetting, m_strSMTPServer);
        }//method: UpdateGlobalASASettings()

        /// <summary>
        /// Reads the Global.asa SMServer DSN information
        /// and stores it in a class member variable
        /// </summary>
        /// <returns>string containing the SMServerDSN value</returns>
        private string GetSMServerDSN()
        {
            //Retrieve the value for the SMServer DSN
            m_strSMServerDSN = RiskmasterSettingsManager.GetGlobalASASetting("SMServerDSN", GlobalASAAppSettings.SMServerAppSetting);

            //return the value of the SMServerDSN
            return m_strSMServerDSN;
        } // method: GetSMServerDSN

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string UpdateSMServerDSN()
        {
            string strUpdatedSMServerDSN = string.Empty;

            //Retrieve the SMServer DSN to replace
            string strSMServerDSN = GetSMServerDSN();

            //Create an instance of the ConnectionStringManager object
            //with the specified connection string
            ConnectionStringManager objConnMgr = new ConnectionStringManager(strSMServerDSN);

            //Retrieve the updated value for the SMServer DSN
            strUpdatedSMServerDSN = objConnMgr.ReplaceDSN(this.SMServerUID, this.SMServerPwd);

            //Clean up
            objConnMgr = null;

            //Update the class member variable with the new SMServer DSN
            m_strSMServerDSN = strUpdatedSMServerDSN;

            //return the value of the Updated SMServer DSN
            return strUpdatedSMServerDSN;
        } // method: UpdateSMServerDSN



        /// <summary>
        /// Loads the Session DSN and SMTP Server settings
        /// </summary>
        /// <permission cref="">Requires read and write access to the file system</permission>
        /// <exception cref="">File may be locked by other processes</exception>
        public void UpdateRiskmasterConfigSettings()
        {
            //Update the Session DSN information
            //Read the existing Session DSN information
            string strSessionDSN = RiskmasterSettingsManager.GetRMConfigSetting("SessionDSN", RiskmasterConfigSettingElements.RMSession);

            //Update the Session DSN information
            RiskmasterSettingsManager.SetRMConfigSetting("SessionDSN", RiskmasterConfigSettingElements.RMSession, strSessionDSN);

            //Retrieve the new version of the SMServer DSN
            string strReplacedSMServerDSN = UpdateSMServerDSN();

            //Update the ReportServer Connection String element
            RiskmasterSettingsManager.SetRMConfigSetting("ReportServer", RiskmasterConfigSettingElements.ReportServer, strReplacedSMServerDSN);
            
            //Update the SMTP Server information
            RiskmasterSettingsManager.SetRMConfigSetting("SMTP Server", RiskmasterConfigSettingElements.SMTPServer, m_strSMTPServer);

            //Update the BIS URL information
            RiskmasterSettingsManager.SetRMConfigSetting("BusObjectsServer", RiskmasterConfigSettingElements.BISXPath, m_strBusObjectsServer);

            //Update the SMNet Server URL information
            RiskmasterSettingsManager.SetRMConfigSetting("SMNetServer", RiskmasterConfigSettingElements.SMNetServer, m_strSMNetServer);

            //Update the App Server IP Address information
            RiskmasterSettingsManager.SetRMConfigSetting("AppServer", RiskmasterConfigSettingElements.AppServer, m_strAppServer);

            //Update the TrustedIPPool IP Addresses
            RiskmasterSettingsManager.SetRMConfigSettingDataSet("CommonWebServiceHandler/TrustedIPPool", RiskmasterConfigSettingElements.IPAddress, m_dstIPAddresses);
        }
        #endregion

        #region ISMServerSettings Members

        public string SMServerUID
        {
            get;
            set;
        }

        public string SMServerPwd
        {
            get;
            set;
        }

        #endregion

        #region ISMTPServerSettings Members

        private string m_strSMTPServer;

        public string SMTPServer
        {
            get
            {
                return m_strSMTPServer;
            }
            set
            {
                m_strSMTPServer = value;
            }
        }

        #endregion

        #region IRMConfigSettings Members

        private string m_strBusObjectsServer;
        private string m_strAppServer;
        private string m_strSMNetServer;
        private DataSet m_dstIPAddresses;

        public string BusObjectsServer
        {
            get
            {
                return m_strBusObjectsServer;
            }
            set
            {
                m_strBusObjectsServer = value;
            }
        }

        public string AppServer
        {
            get { return m_strAppServer; }
            set { m_strAppServer = value; }
        } // property AppServer


        public string SMNetServer
        {
            get { return m_strSMNetServer; }
            set { m_strSMNetServer = value; }
        } // property SMNetServer

        public DataSet IPAddresses
        {
            get
            {
                return m_dstIPAddresses;
            }
            set
            {
                m_dstIPAddresses = value;
            }//property IPAddresses
        }

        #endregion

        #region ISecuritySettings Members

        public string SecurityUID
        {
            get;
            set;
        }

        public string SecurityPwd
        {
            get;
            set;
        }

        public string SecurityConnectionString
        {
            get;
            set;
        }

        #endregion

        #region ISessionSettings Members
        public string SessionConnectionString
        {
            get;
            set;
        }

        #endregion
    }//class
}
