using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.ApplicationSettings
{

    /// <summary>
    /// Manages the Riskmaster.config Configuration Settings
    /// </summary>
    public class RMConfigurationSettings
    {
        const string RISKMASTER_CONFIG_APP_SETTING = "RiskmasterConfigPath";
        const string SORTMASTER_DIRECTORY = "WIZARDSM";
        const string GLOBAL_ASA_FILE = "Global.asa";
        private string m_strRMConfigPath = string.Empty;
        private string m_strGlobalASAPath = string.Empty;

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private RMConfigurationSettings()
        {
        } // constructor

        
        /// <summary>
        /// OVERLOADED: Default class constructor to initialize all values
        /// </summary>
        /// <param name="strRMConfigPath">string containing the 
        /// absolute file path to the Riskmaster.config file</param>
        public RMConfigurationSettings(string strRMConfigPath)
        {
            //Set the Riskmaster.config class member value
            //to the specified value
            m_strRMConfigPath = strRMConfigPath;

            //Call the function to populate the derived file path
            PopulateGlobalASAPath();

            
        }//class constructor

        /// <summary>
        /// OVERLOADED: Class constructor to initialize values
        /// for file paths given the specified paths for the 
        /// Riskmaster.config file and the Global.asa file
        /// </summary>
        /// <param name="strRMConfigPath"></param>
        /// <param name="strGlobalASAPath"></param>
        public RMConfigurationSettings(string strRMConfigPath, string strGlobalASAPath)
        {
            m_strRMConfigPath = strRMConfigPath;
            m_strGlobalASAPath = strGlobalASAPath;
        } // constructor

        /// <summary>
        /// Derives the file path for the Global.asa file
        /// based on the current path of the Riskmaster.config file
        /// </summary>
        private void PopulateGlobalASAPath()
        {
            //Based on the path specified to the Riskmaster.config file
            //Derive the path to the Global.asa file required for Sortmaster
            FileInfo objFileInfo = new FileInfo(m_strRMConfigPath);
            DirectoryInfo objDirInfo = objFileInfo.Directory;
            DirectoryInfo objParentDirInfo = objDirInfo.Parent;
            string strParentDir = objParentDirInfo.FullName;

            //Set the path to the Global.asa file
            m_strGlobalASAPath = strParentDir + "\\" + SORTMASTER_DIRECTORY + "\\" + GLOBAL_ASA_FILE;

            //Clean upl
            objFileInfo = null;
            objDirInfo = null;
            objParentDirInfo = null;
        } // method: PopulateGlobalASAPath



        /// <summary>
        /// Gets or sets the path to the Global.asa file
        /// </summary>
        public string GlobalASAPath
        {
            get { return m_strGlobalASAPath; }
            set { m_strGlobalASAPath = value; }
        }


        /// <summary>
        /// Gets or sets the path to the Riskmaster.config file
        /// </summary>
        public string RMConfigurationPath
        {
            get { return m_strRMConfigPath; }
            set { m_strRMConfigPath = value; }
        }
        
    }//class RMConfigurationSettings
}
