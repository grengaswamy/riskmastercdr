using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface IRMConfigSettings
    {
        /// <summary>
        /// Gets and sets the Business Objects Server URL
        /// </summary>
        string BusObjectsServer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the Application Server
        /// </summary>
        string AppServer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the Sortmaster Server URL
        /// </summary>
        string SMNetServer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the IP Addresses
        /// </summary>
        DataSet IPAddresses
        {
            get;
            set;
        }
    }
}