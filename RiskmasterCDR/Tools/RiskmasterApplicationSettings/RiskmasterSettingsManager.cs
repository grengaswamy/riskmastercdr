using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Riskmaster.Application.RiskmasterSettings;

namespace Riskmaster.Application.ApplicationSettings
{
    /// <summary>
    /// Static class for managing Riskmaster Settings
    /// </summary>
    public static class RiskmasterSettingsManager
    {
        private static string m_strGlobalASAPath;
        private static string m_strRiskmasterConfigPath;

        #region Public Properties
        public static string GlobalASAPath
        {
            get { return m_strGlobalASAPath; }
            set { m_strGlobalASAPath = value; }
        } // property GlobalASAPath

        public static string RiskmasterConfigPath
        {
            get { return m_strRiskmasterConfigPath; }
            set { m_strRiskmasterConfigPath = value; }
        } // property RiskmasterConfigPath 
        #endregion

        #region Get Methods
        /// <summary>
        /// Generic method for reading settings from the Windows Registry
        /// </summary>
        /// <param name="nvCollSettings"></param>
        /// <returns></returns>
        public static string GetRegistrySetting(NameValueCollection nvCollSettings)
        {
            string strRegistryValue = string.Empty;

            //Create the Registry configuration object
            RegistryConfiguration objRegConfig = new RegistryConfiguration();

            //Read the specified setting from the Riskmaster.config file
            strRegistryValue = objRegConfig.ReadSetting(nvCollSettings);

            //Clean up
            nvCollSettings = null;
            objRegConfig = null;

            //return the Registry value
            return strRegistryValue;
        } // method: GetRegistrySetting()

        /// <summary>
        /// Generic method for reading settings from the Global.asa file
        /// </summary>
        /// <param name="strSettingName"></param>
        /// <param name="strSettingValue"></param>
        /// <returns></returns>
        public static string GetGlobalASASetting(string strSettingName, string strSettingValue)
        {
            string strGlobalASAValue = string.Empty;
            NameValueCollection nvCollSettings = new NameValueCollection();

            //Create the Riskmaster Configuration object
            GlobalASAConfiguration objGlobalASAConfig = new GlobalASAConfiguration(m_strGlobalASAPath);

            //Add the specified element to the NameValueCollection
            nvCollSettings.Add(strSettingName, strSettingValue);

            //Read the specified setting from the Riskmaster.config file
            strGlobalASAValue = objGlobalASAConfig.ReadSetting(nvCollSettings);

            //Clean up
            nvCollSettings = null;
            objGlobalASAConfig = null;

            //return the Global.asa value
            return strGlobalASAValue;
        } // method: GetGlobalASASetting()

        /// <summary>
        /// Generic method for reading settings from the Riskmaster.config file
        /// </summary>
        /// <param name="strSettingName"></param>
        /// <param name="strSettingValue"></param>
        /// <returns></returns>
        public static string GetRMConfigSetting(string strSettingName, string strSettingValue)
        {
            string strRMConfigValue = string.Empty;
            NameValueCollection nvCollSettings = new NameValueCollection();

            //Create the Riskmaster Configuration object
            RiskmasterConfigConfiguration objRMConfig = new RiskmasterConfigConfiguration(m_strRiskmasterConfigPath);

            //Add the specified element to the NameValueCollection
            nvCollSettings.Add(strSettingName, strSettingValue);

            //Read the specified setting from the Riskmaster.config file
            strRMConfigValue = objRMConfig.ReadSetting(nvCollSettings);

            //Clean up
            nvCollSettings = null;
            objRMConfig = null;

            //return the Riskmaster.config value
            return strRMConfigValue;
        } // method: GetRMConfigSetting() 
        #endregion

        #region Set Methods
            
        /// <summary>
        /// Generic method for reading settings from the Windows Registry
        /// </summary>
        /// <param name="nvCollSettings"></param>
        /// <param name="strRegistryValue"></param>
        public static void SetRegistrySetting(NameValueCollection nvCollSettings,
            string strRegistryValue)
        {
            //Create the Registry configuration object
            RegistryConfiguration objRegConfig = new RegistryConfiguration();

            //Write the specified setting back to the Registry
            objRegConfig.WriteSetting(nvCollSettings, strRegistryValue);

            //Clean up
            nvCollSettings = null;
            objRegConfig = null;
        } // method: SetRegistrySetting()

        /// <summary>
        /// Generic method for reading settings from the Global.asa file
        /// </summary>
        /// <param name="strSettingName"></param>
        /// <param name="strSettingValue"></param>
        /// <returns></returns>
        public static void SetGlobalASASetting(string strSettingName, string strSettingValue,
            string strGlobalASAValue)
        {
            NameValueCollection nvCollSettings = new NameValueCollection();

            //Create the Riskmaster Configuration object
            GlobalASAConfiguration objGlobalASAConfig = new GlobalASAConfiguration(m_strGlobalASAPath);

            //Add the specified element to the NameValueCollection
            nvCollSettings.Add(strSettingName, strSettingValue);

            //Write the specified setting back to the Global.asa
            objGlobalASAConfig.WriteSetting(nvCollSettings, strGlobalASAValue);

            //Clean up
            nvCollSettings = null;
            objGlobalASAConfig = null;
        } // method: SetGlobalASASetting()

        /// <summary>
        /// Generic method for reading settings from the Riskmaster.config file
        /// </summary>
        /// <param name="strSettingName"></param>
        /// <param name="strSettingValue"></param>
        /// <returns></returns>
        public static void SetRMConfigSetting(string strSettingName, string strSettingValue,
            string strRMConfigValue)
        {
            NameValueCollection nvCollSettings = new NameValueCollection();

            //Create the Riskmaster Configuration object
            RiskmasterConfigConfiguration objRMConfig = new RiskmasterConfigConfiguration(m_strRiskmasterConfigPath);

            //Add the specified element to the NameValueCollection
            nvCollSettings.Add(strSettingName, strSettingValue);

            //Read the specified setting from the Riskmaster.config file
            objRMConfig.WriteSetting(nvCollSettings, strRMConfigValue);

            //Clean up
            nvCollSettings = null;
            objRMConfig = null;
        } // method: SetRMConfigSetting()  
        #endregion
    }
}
