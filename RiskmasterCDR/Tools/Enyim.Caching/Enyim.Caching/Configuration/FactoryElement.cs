using System;
using System.ComponentModel;
using System.Configuration;
using System.Collections.Generic;
using Enyim.Caching.Memcached;
using Enyim.Reflection;
using System.Xml.Linq;

namespace Enyim.Caching.Configuration
{
	/// <summary>
	/// This element is used to define locator/transcoder/keyTransformer instances. It also provides custom initializations for them using a factory.
	/// </summary>
	/// <typeparam name="TFactory"></typeparam>
	public class FactoryElement<TFactory> : ConfigurationElement
		where TFactory : class, IProvider
	{
		protected readonly Dictionary<string, string> Parameters = new Dictionary<string, string>();
		private TFactory instance;

		protected virtual bool IsOptional { get { return false; } }

		/// <summary>
		/// Gets or sets the type of the factory.
		/// </summary>
		[ConfigurationProperty("factory"), TypeConverter(typeof(TypeNameConverter))]
		public Type Factory
		{
			get { return (Type)base["factory"]; }
			set { base["factory"] = value; }
		}

		protected override bool OnDeserializeUnrecognizedAttribute(string name, string value)
		{
			ConfigurationProperty property = new ConfigurationProperty(name, typeof(string), value);
			base[property] = value;

			this.Parameters[name] = value;

			return true;
		}

		/// <summary>
		/// Creates the provider by using the factory (if present) or directly instantiating by type name
		/// </summary>
		/// <returns></returns>
		public TFactory CreateInstance()
		{
			//check if we have a factory
			if (this.instance == null)
			{
				var type = this.Factory;
				if (type == null)
				{
					if (this.IsOptional || !this.ElementInformation.IsPresent)
						return null;

					throw new ConfigurationErrorsException("factory must be defined");
				}

				this.instance = (TFactory)FastActivator.Create(type);
				this.instance.Initialize(this.Parameters);
			}

			return this.instance;
		}
	}

	public class OptionalFactoryElement<TResult> : FactoryElement<TResult>
		where TResult : class, IProvider
	{
		protected override bool IsOptional
		{
			get { return true; }
		}
	}

}

