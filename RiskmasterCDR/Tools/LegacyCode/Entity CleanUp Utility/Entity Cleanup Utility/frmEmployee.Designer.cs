﻿namespace Entity_Cleanup_Utility
{
    partial class frmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblemployee = new System.Windows.Forms.Label();
            this.ctlTabs = new System.Windows.Forms.TabControl();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLastN = new System.Windows.Forms.TextBox();
            this.rb_Emp_Srch_EmpNo = new System.Windows.Forms.RadioButton();
            this.rb_Emp_Srch_LNFN = new System.Windows.Forms.RadioButton();
            this.rb_Emp_Srch_LNS = new System.Windows.Forms.RadioButton();
            this.rb_Emp_Srch_TaxId = new System.Windows.Forms.RadioButton();
            this.gbEmployeeType = new System.Windows.Forms.GroupBox();
            this.rb_Emp_Srch_Type = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.ctlPRGrid = new System.Windows.Forms.DataGridView();
            this.gr_prLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prEmployeeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prTaxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.datetimepicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblCriteriaDet = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblEmpNo = new System.Windows.Forms.Label();
            this.lblTaxId = new System.Windows.Forms.Label();
            this.txtEmailDet = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtBirthDate = new System.Windows.Forms.TextBox();
            this.lblStZip = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblHomePhone = new System.Windows.Forms.Label();
            this.btnProcess = new System.Windows.Forms.Button();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txtHomPhn = new System.Windows.Forms.TextBox();
            this.txtEmpNo = new System.Windows.Forms.TextBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtAddr2 = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddr1 = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblOfficePh = new System.Windows.Forms.Label();
            this.txtOffPhn = new System.Windows.Forms.TextBox();
            this.ctlPGridDet = new System.Windows.Forms.DataGridView();
            this.Keep = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Del = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.S1099 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PDLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ctlTabs.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbEmployeeType.SuspendLayout();
            this.tabResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).BeginInit();
            this.tabDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(476, 412);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblemployee
            // 
            this.lblemployee.AutoSize = true;
            this.lblemployee.BackColor = System.Drawing.SystemColors.Control;
            this.lblemployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemployee.ForeColor = System.Drawing.Color.Maroon;
            this.lblemployee.Location = new System.Drawing.Point(26, 16);
            this.lblemployee.Name = "lblemployee";
            this.lblemployee.Size = new System.Drawing.Size(149, 17);
            this.lblemployee.TabIndex = 1;
            this.lblemployee.Text = "Employee Clean Up";
            // 
            // ctlTabs
            // 
            this.ctlTabs.Controls.Add(this.tabSearch);
            this.ctlTabs.Controls.Add(this.tabResults);
            this.ctlTabs.Controls.Add(this.tabDetail);
            this.ctlTabs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctlTabs.Location = new System.Drawing.Point(29, 36);
            this.ctlTabs.Name = "ctlTabs";
            this.ctlTabs.SelectedIndex = 0;
            this.ctlTabs.Size = new System.Drawing.Size(453, 379);
            this.ctlTabs.TabIndex = 2;
            this.ctlTabs.Selected += new System.Windows.Forms.TabControlEventHandler(this.ctlTabs_Selected);
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.groupBox1);
            this.tabSearch.Controls.Add(this.gbEmployeeType);
            this.tabSearch.Controls.Add(this.btnSearch);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(445, 353);
            this.tabSearch.TabIndex = 0;
            this.tabSearch.Text = "1. Search";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLastN);
            this.groupBox1.Controls.Add(this.rb_Emp_Srch_EmpNo);
            this.groupBox1.Controls.Add(this.rb_Emp_Srch_LNFN);
            this.groupBox1.Controls.Add(this.rb_Emp_Srch_LNS);
            this.groupBox1.Controls.Add(this.rb_Emp_Srch_TaxId);
            this.groupBox1.Location = new System.Drawing.Point(7, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 123);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for duplicates using";
            // 
            // txtLastN
            // 
            this.txtLastN.Location = new System.Drawing.Point(223, 88);
            this.txtLastN.Name = "txtLastN";
            this.txtLastN.Size = new System.Drawing.Size(151, 21);
            this.txtLastN.TabIndex = 3;
            this.txtLastN.Visible = false;
            // 
            // rb_Emp_Srch_EmpNo
            // 
            this.rb_Emp_Srch_EmpNo.AutoSize = true;
            this.rb_Emp_Srch_EmpNo.Location = new System.Drawing.Point(10, 42);
            this.rb_Emp_Srch_EmpNo.Name = "rb_Emp_Srch_EmpNo";
            this.rb_Emp_Srch_EmpNo.Size = new System.Drawing.Size(130, 17);
            this.rb_Emp_Srch_EmpNo.TabIndex = 4;
            this.rb_Emp_Srch_EmpNo.TabStop = true;
            this.rb_Emp_Srch_EmpNo.Text = "Employee Number";
            this.rb_Emp_Srch_EmpNo.UseVisualStyleBackColor = true;
            // 
            // rb_Emp_Srch_LNFN
            // 
            this.rb_Emp_Srch_LNFN.AutoSize = true;
            this.rb_Emp_Srch_LNFN.Location = new System.Drawing.Point(10, 65);
            this.rb_Emp_Srch_LNFN.Name = "rb_Emp_Srch_LNFN";
            this.rb_Emp_Srch_LNFN.Size = new System.Drawing.Size(150, 17);
            this.rb_Emp_Srch_LNFN.TabIndex = 3;
            this.rb_Emp_Srch_LNFN.TabStop = true;
            this.rb_Emp_Srch_LNFN.Text = "Last Name,First Name";
            this.rb_Emp_Srch_LNFN.UseVisualStyleBackColor = true;
            // 
            // rb_Emp_Srch_LNS
            // 
            this.rb_Emp_Srch_LNS.AutoSize = true;
            this.rb_Emp_Srch_LNS.Location = new System.Drawing.Point(10, 88);
            this.rb_Emp_Srch_LNS.Name = "rb_Emp_Srch_LNS";
            this.rb_Emp_Srch_LNS.Size = new System.Drawing.Size(143, 17);
            this.rb_Emp_Srch_LNS.TabIndex = 2;
            this.rb_Emp_Srch_LNS.TabStop = true;
            this.rb_Emp_Srch_LNS.Text = "Last Name (Specific)";
            this.rb_Emp_Srch_LNS.UseVisualStyleBackColor = true;
            this.rb_Emp_Srch_LNS.CheckedChanged += new System.EventHandler(this.rb_Emp_Srch_LNS_CheckedChanged);
            // 
            // rb_Emp_Srch_TaxId
            // 
            this.rb_Emp_Srch_TaxId.AutoSize = true;
            this.rb_Emp_Srch_TaxId.Location = new System.Drawing.Point(10, 19);
            this.rb_Emp_Srch_TaxId.Name = "rb_Emp_Srch_TaxId";
            this.rb_Emp_Srch_TaxId.Size = new System.Drawing.Size(64, 17);
            this.rb_Emp_Srch_TaxId.TabIndex = 1;
            this.rb_Emp_Srch_TaxId.TabStop = true;
            this.rb_Emp_Srch_TaxId.Text = "Tax ID";
            this.rb_Emp_Srch_TaxId.UseVisualStyleBackColor = true;
            // 
            // gbEmployeeType
            // 
            this.gbEmployeeType.Controls.Add(this.rb_Emp_Srch_Type);
            this.gbEmployeeType.Location = new System.Drawing.Point(7, 7);
            this.gbEmployeeType.Name = "gbEmployeeType";
            this.gbEmployeeType.Size = new System.Drawing.Size(445, 50);
            this.gbEmployeeType.TabIndex = 1;
            this.gbEmployeeType.TabStop = false;
            this.gbEmployeeType.Text = "Type";
            // 
            // rb_Emp_Srch_Type
            // 
            this.rb_Emp_Srch_Type.AutoSize = true;
            this.rb_Emp_Srch_Type.Location = new System.Drawing.Point(10, 19);
            this.rb_Emp_Srch_Type.Name = "rb_Emp_Srch_Type";
            this.rb_Emp_Srch_Type.Size = new System.Drawing.Size(104, 17);
            this.rb_Emp_Srch_Type.TabIndex = 0;
            this.rb_Emp_Srch_Type.TabStop = true;
            this.rb_Emp_Srch_Type.Text = "all Employees";
            this.rb_Emp_Srch_Type.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(357, 315);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.lblCriteria);
            this.tabResults.Controls.Add(this.btnDisplay);
            this.tabResults.Controls.Add(this.ctlPRGrid);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(445, 353);
            this.tabResults.TabIndex = 1;
            this.tabResults.Text = "2. Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteria.Location = new System.Drawing.Point(17, 7);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(50, 13);
            this.lblCriteria.TabIndex = 3;
            this.lblCriteria.Text = "Criteria";
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(367, 315);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnDisplay.TabIndex = 1;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // ctlPRGrid
            // 
            this.ctlPRGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPRGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ctlPRGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPRGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gr_prLastName,
            this.gr_prFirstName,
            this.gr_prEmployeeNo,
            this.gr_prTaxID,
            this.gr_prCount});
            this.ctlPRGrid.Location = new System.Drawing.Point(19, 26);
            this.ctlPRGrid.MultiSelect = false;
            this.ctlPRGrid.Name = "ctlPRGrid";
            this.ctlPRGrid.Size = new System.Drawing.Size(415, 283);
            this.ctlPRGrid.TabIndex = 0;
            // 
            // gr_prLastName
            // 
            this.gr_prLastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prLastName.DefaultCellStyle = dataGridViewCellStyle2;
            this.gr_prLastName.HeaderText = "Last Name";
            this.gr_prLastName.Name = "gr_prLastName";
            this.gr_prLastName.ReadOnly = true;
            this.gr_prLastName.Width = 92;
            // 
            // gr_prFirstName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prFirstName.DefaultCellStyle = dataGridViewCellStyle3;
            this.gr_prFirstName.HeaderText = "First Name";
            this.gr_prFirstName.Name = "gr_prFirstName";
            this.gr_prFirstName.ReadOnly = true;
            // 
            // gr_prEmployeeNo
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prEmployeeNo.DefaultCellStyle = dataGridViewCellStyle4;
            this.gr_prEmployeeNo.HeaderText = "Employee";
            this.gr_prEmployeeNo.Name = "gr_prEmployeeNo";
            this.gr_prEmployeeNo.ReadOnly = true;
            this.gr_prEmployeeNo.Visible = false;
            // 
            // gr_prTaxID
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prTaxID.DefaultCellStyle = dataGridViewCellStyle5;
            this.gr_prTaxID.HeaderText = "Tax ID/SSN";
            this.gr_prTaxID.Name = "gr_prTaxID";
            this.gr_prTaxID.ReadOnly = true;
            // 
            // gr_prCount
            // 
            this.gr_prCount.HeaderText = "Count";
            this.gr_prCount.Name = "gr_prCount";
            this.gr_prCount.ReadOnly = true;
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.cmbState);
            this.tabDetail.Controls.Add(this.datetimepicker1);
            this.tabDetail.Controls.Add(this.lblCriteriaDet);
            this.tabDetail.Controls.Add(this.btnSave);
            this.tabDetail.Controls.Add(this.btnEdit);
            this.tabDetail.Controls.Add(this.lblEmail);
            this.tabDetail.Controls.Add(this.lblEmpNo);
            this.tabDetail.Controls.Add(this.lblTaxId);
            this.tabDetail.Controls.Add(this.txtEmailDet);
            this.tabDetail.Controls.Add(this.label2);
            this.tabDetail.Controls.Add(this.label1);
            this.tabDetail.Controls.Add(this.txtAge);
            this.tabDetail.Controls.Add(this.txtBirthDate);
            this.tabDetail.Controls.Add(this.lblStZip);
            this.tabDetail.Controls.Add(this.lblCity);
            this.tabDetail.Controls.Add(this.lblHomePhone);
            this.tabDetail.Controls.Add(this.btnProcess);
            this.tabDetail.Controls.Add(this.txttax);
            this.tabDetail.Controls.Add(this.txtHomPhn);
            this.tabDetail.Controls.Add(this.txtEmpNo);
            this.tabDetail.Controls.Add(this.txtZip);
            this.tabDetail.Controls.Add(this.txtCity);
            this.tabDetail.Controls.Add(this.txtAddr2);
            this.tabDetail.Controls.Add(this.lblAddress);
            this.tabDetail.Controls.Add(this.txtAddr1);
            this.tabDetail.Controls.Add(this.txtFirstName);
            this.tabDetail.Controls.Add(this.lblName);
            this.tabDetail.Controls.Add(this.txtLastName);
            this.tabDetail.Controls.Add(this.lblOfficePh);
            this.tabDetail.Controls.Add(this.txtOffPhn);
            this.tabDetail.Controls.Add(this.ctlPGridDet);
            this.tabDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDetail.Location = new System.Drawing.Point(4, 22);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Size = new System.Drawing.Size(445, 353);
            this.tabDetail.TabIndex = 2;
            this.tabDetail.Text = "3. Detail";
            this.tabDetail.UseVisualStyleBackColor = true;
            // 
            // cmbState
            // 
            this.cmbState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbState.Enabled = false;
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(51, 279);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(42, 21);
            this.cmbState.TabIndex = 65;
            // 
            // datetimepicker1
            // 
            this.datetimepicker1.CustomFormat = "yyyyMMdd";
            this.datetimepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimepicker1.Location = new System.Drawing.Point(51, 326);
            this.datetimepicker1.Name = "datetimepicker1";
            this.datetimepicker1.Size = new System.Drawing.Size(77, 20);
            this.datetimepicker1.TabIndex = 43;
            this.datetimepicker1.Visible = false;
            this.datetimepicker1.CloseUp += new System.EventHandler(this.datetimepicker1_CloseUp);
            // 
            // lblCriteriaDet
            // 
            this.lblCriteriaDet.AutoSize = true;
            this.lblCriteriaDet.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteriaDet.Location = new System.Drawing.Point(13, 1);
            this.lblCriteriaDet.Name = "lblCriteriaDet";
            this.lblCriteriaDet.Size = new System.Drawing.Size(39, 13);
            this.lblCriteriaDet.TabIndex = 42;
            this.lblCriteriaDet.Text = "Criteria";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(269, 326);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 21);
            this.btnSave.TabIndex = 41;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(194, 326);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(72, 21);
            this.btnEdit.TabIndex = 40;
            this.btnEdit.Text = "Edit Entity";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(244, 301);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 39;
            this.lblEmail.Text = "Email:";
            // 
            // lblEmpNo
            // 
            this.lblEmpNo.AutoSize = true;
            this.lblEmpNo.Location = new System.Drawing.Point(231, 280);
            this.lblEmpNo.Name = "lblEmpNo";
            this.lblEmpNo.Size = new System.Drawing.Size(48, 13);
            this.lblEmpNo.TabIndex = 38;
            this.lblEmpNo.Text = "Emp No:";
            // 
            // lblTaxId
            // 
            this.lblTaxId.AutoSize = true;
            this.lblTaxId.Location = new System.Drawing.Point(237, 259);
            this.lblTaxId.Name = "lblTaxId";
            this.lblTaxId.Size = new System.Drawing.Size(42, 13);
            this.lblTaxId.TabIndex = 37;
            this.lblTaxId.Text = "Tax ID:";
            // 
            // txtEmailDet
            // 
            this.txtEmailDet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmailDet.Enabled = false;
            this.txtEmailDet.Location = new System.Drawing.Point(282, 298);
            this.txtEmailDet.Name = "txtEmailDet";
            this.txtEmailDet.Size = new System.Drawing.Size(148, 20);
            this.txtEmailDet.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 305);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Age :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Birth Dt :";
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAge.Enabled = false;
            this.txtAge.Location = new System.Drawing.Point(156, 305);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(43, 20);
            this.txtAge.TabIndex = 33;
            // 
            // txtBirthDate
            // 
            this.txtBirthDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtBirthDate.Enabled = false;
            this.txtBirthDate.Location = new System.Drawing.Point(51, 302);
            this.txtBirthDate.Name = "txtBirthDate";
            this.txtBirthDate.Size = new System.Drawing.Size(71, 20);
            this.txtBirthDate.TabIndex = 32;
            // 
            // lblStZip
            // 
            this.lblStZip.AutoSize = true;
            this.lblStZip.Location = new System.Drawing.Point(10, 283);
            this.lblStZip.Name = "lblStZip";
            this.lblStZip.Size = new System.Drawing.Size(38, 13);
            this.lblStZip.TabIndex = 26;
            this.lblStZip.Text = "St,Zip:";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(21, 262);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 25;
            this.lblCity.Text = "City:";
            // 
            // lblHomePhone
            // 
            this.lblHomePhone.AutoSize = true;
            this.lblHomePhone.Location = new System.Drawing.Point(225, 190);
            this.lblHomePhone.Name = "lblHomePhone";
            this.lblHomePhone.Size = new System.Drawing.Size(54, 13);
            this.lblHomePhone.TabIndex = 24;
            this.lblHomePhone.Text = "Home Ph:";
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(359, 326);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 21);
            this.btnProcess.TabIndex = 22;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // txttax
            // 
            this.txttax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txttax.Enabled = false;
            this.txttax.Location = new System.Drawing.Point(282, 254);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(148, 20);
            this.txttax.TabIndex = 18;
            // 
            // txtHomPhn
            // 
            this.txtHomPhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtHomPhn.Enabled = false;
            this.txtHomPhn.Location = new System.Drawing.Point(283, 187);
            this.txtHomPhn.Name = "txtHomPhn";
            this.txtHomPhn.Size = new System.Drawing.Size(148, 20);
            this.txtHomPhn.TabIndex = 17;
            // 
            // txtEmpNo
            // 
            this.txtEmpNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmpNo.Enabled = false;
            this.txtEmpNo.Location = new System.Drawing.Point(282, 276);
            this.txtEmpNo.Name = "txtEmpNo";
            this.txtEmpNo.Size = new System.Drawing.Size(148, 20);
            this.txtEmpNo.TabIndex = 16;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtZip.Enabled = false;
            this.txtZip.Location = new System.Drawing.Point(97, 280);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(102, 20);
            this.txtZip.TabIndex = 14;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(51, 257);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(148, 20);
            this.txtCity.TabIndex = 12;
            // 
            // txtAddr2
            // 
            this.txtAddr2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr2.Enabled = false;
            this.txtAddr2.Location = new System.Drawing.Point(52, 233);
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.Size = new System.Drawing.Size(301, 20);
            this.txtAddr2.TabIndex = 11;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(3, 214);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(48, 13);
            this.lblAddress.TabIndex = 10;
            this.lblAddress.Text = "Address:";
            // 
            // txtAddr1
            // 
            this.txtAddr1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr1.Enabled = false;
            this.txtAddr1.Location = new System.Drawing.Point(53, 210);
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.Size = new System.Drawing.Size(301, 20);
            this.txtAddr1.TabIndex = 9;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Location = new System.Drawing.Point(53, 188);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(148, 20);
            this.txtFirstName.TabIndex = 8;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(14, 170);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 7;
            this.lblName.Text = "Name:";
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtLastName.Enabled = false;
            this.txtLastName.Location = new System.Drawing.Point(53, 166);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(148, 20);
            this.txtLastName.TabIndex = 6;
            // 
            // lblOfficePh
            // 
            this.lblOfficePh.AutoSize = true;
            this.lblOfficePh.Location = new System.Drawing.Point(225, 168);
            this.lblOfficePh.Name = "lblOfficePh";
            this.lblOfficePh.Size = new System.Drawing.Size(54, 13);
            this.lblOfficePh.TabIndex = 4;
            this.lblOfficePh.Text = "Office Ph:";
            // 
            // txtOffPhn
            // 
            this.txtOffPhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtOffPhn.Enabled = false;
            this.txtOffPhn.Location = new System.Drawing.Point(283, 163);
            this.txtOffPhn.Name = "txtOffPhn";
            this.txtOffPhn.Size = new System.Drawing.Size(147, 20);
            this.txtOffPhn.TabIndex = 3;
            // 
            // ctlPGridDet
            // 
            this.ctlPGridDet.AllowUserToAddRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPGridDet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.ctlPGridDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPGridDet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Keep,
            this.Del,
            this.S1099,
            this.PDLastName,
            this.PDFirstName,
            this.PDEntityID,
            this.PDEntityType,
            this.PDEntType});
            this.ctlPGridDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ctlPGridDet.Location = new System.Drawing.Point(14, 19);
            this.ctlPGridDet.MultiSelect = false;
            this.ctlPGridDet.Name = "ctlPGridDet";
            this.ctlPGridDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ctlPGridDet.Size = new System.Drawing.Size(422, 138);
            this.ctlPGridDet.TabIndex = 0;
            this.ctlPGridDet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlPGridDet_CellClick);
            // 
            // Keep
            // 
            this.Keep.FalseValue = "0";
            this.Keep.HeaderText = "Keep";
            this.Keep.Name = "Keep";
            this.Keep.TrueValue = "-1";
            this.Keep.Width = 45;
            // 
            // Del
            // 
            this.Del.FalseValue = "0";
            this.Del.HeaderText = "Del";
            this.Del.Name = "Del";
            this.Del.TrueValue = "-1";
            this.Del.Width = 45;
            // 
            // S1099
            // 
            this.S1099.FalseValue = "0";
            this.S1099.HeaderText = "1099";
            this.S1099.Name = "S1099";
            this.S1099.ReadOnly = true;
            this.S1099.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.S1099.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.S1099.TrueValue = "-1";
            this.S1099.Width = 45;
            // 
            // PDLastName
            // 
            this.PDLastName.HeaderText = "Last Name";
            this.PDLastName.Name = "PDLastName";
            this.PDLastName.ReadOnly = true;
            // 
            // PDFirstName
            // 
            this.PDFirstName.HeaderText = "First Name";
            this.PDFirstName.Name = "PDFirstName";
            this.PDFirstName.ReadOnly = true;
            // 
            // PDEntityID
            // 
            this.PDEntityID.DataPropertyName = "EID";
            this.PDEntityID.HeaderText = "Entity ID";
            this.PDEntityID.Name = "PDEntityID";
            this.PDEntityID.ReadOnly = true;
            // 
            // PDEntityType
            // 
            this.PDEntityType.HeaderText = "Type";
            this.PDEntityType.Name = "PDEntityType";
            this.PDEntityType.ReadOnly = true;
            this.PDEntityType.Visible = false;
            // 
            // PDEntType
            // 
            this.PDEntType.HeaderText = "Ent Type";
            this.PDEntType.Name = "PDEntType";
            this.PDEntType.ReadOnly = true;
            // 
            // frmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(503, 452);
            this.Controls.Add(this.ctlTabs);
            this.Controls.Add(this.lblemployee);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmEmployee";
            this.Text = "frmEmployee";
            this.Load += new System.EventHandler(this.frmEmployee_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ctlTabs.ResumeLayout(false);
            this.tabSearch.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbEmployeeType.ResumeLayout(false);
            this.gbEmployeeType.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.tabResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblemployee;
        internal System.Windows.Forms.TabControl ctlTabs;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDisplay;
        internal System.Windows.Forms.DataGridView ctlPRGrid;
        internal System.Windows.Forms.DataGridView ctlPGridDet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblOfficePh;
        private System.Windows.Forms.TextBox txtOffPhn;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txtHomPhn;
        private System.Windows.Forms.TextBox txtEmpNo;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtAddr2;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAddr1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.GroupBox gbEmployeeType;
        private System.Windows.Forms.RadioButton rb_Emp_Srch_Type;
        private System.Windows.Forms.RadioButton rb_Emp_Srch_EmpNo;
        private System.Windows.Forms.RadioButton rb_Emp_Srch_LNFN;
        private System.Windows.Forms.RadioButton rb_Emp_Srch_LNS;
        private System.Windows.Forms.RadioButton rb_Emp_Srch_TaxId;
        private System.Windows.Forms.Label lblHomePhone;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblStZip;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.TextBox txtBirthDate;
        private System.Windows.Forms.TextBox txtEmailDet;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblEmpNo;
        private System.Windows.Forms.Label lblTaxId;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label lblCriteria;
        private System.Windows.Forms.TextBox txtLastN;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prEmployeeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prTaxID;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prCount;
        private System.Windows.Forms.Label lblCriteriaDet;
        private System.Windows.Forms.DateTimePicker datetimepicker1;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Keep;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Del;
        private System.Windows.Forms.DataGridViewCheckBoxColumn S1099;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntType;

    }
}