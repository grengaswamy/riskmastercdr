﻿namespace Entity_Cleanup_Utility
{
    partial class frmUndo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblCET = new System.Windows.Forms.Label();
            this.tabCET = new System.Windows.Forms.TabControl();
            this.TabEt = new System.Windows.Forms.TabPage();
            this.lblDisplayChanges = new System.Windows.Forms.Label();
            this.afterdtpicker = new System.Windows.Forms.DateTimePicker();
            this.rbOn = new System.Windows.Forms.RadioButton();
            this.rbBefore = new System.Windows.Forms.RadioButton();
            this.rbAfter = new System.Windows.Forms.RadioButton();
            this.gbCET = new System.Windows.Forms.GroupBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.grdDisplay = new System.Windows.Forms.DataGridView();
            this.btnProcess = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPurge = new System.Windows.Forms.Button();
            this.dtpickerPurge = new System.Windows.Forms.DateTimePicker();
            this.rbHistoryDate = new System.Windows.Forms.RadioButton();
            this.rbPurgeAll = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.colLastKept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFirstKept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTypeKept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastChng = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTypeChnge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EID_KEPT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EID_CHANGED = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ECU_ROW_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabCET.SuspendLayout();
            this.TabEt.SuspendLayout();
            this.gbCET.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDisplay)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCET
            // 
            this.lblCET.AutoSize = true;
            this.lblCET.BackColor = System.Drawing.SystemColors.Control;
            this.lblCET.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCET.ForeColor = System.Drawing.Color.Maroon;
            this.lblCET.Location = new System.Drawing.Point(26, 16);
            this.lblCET.Name = "lblCET";
            this.lblCET.Size = new System.Drawing.Size(182, 17);
            this.lblCET.TabIndex = 1;
            this.lblCET.Text = "Undo Previous Changes";
            // 
            // tabCET
            // 
            this.tabCET.Controls.Add(this.TabEt);
            this.tabCET.Controls.Add(this.tabPage1);
            this.tabCET.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCET.Location = new System.Drawing.Point(29, 49);
            this.tabCET.Name = "tabCET";
            this.tabCET.SelectedIndex = 0;
            this.tabCET.Size = new System.Drawing.Size(449, 357);
            this.tabCET.TabIndex = 2;
            // 
            // TabEt
            // 
            this.TabEt.Controls.Add(this.lblDisplayChanges);
            this.TabEt.Controls.Add(this.afterdtpicker);
            this.TabEt.Controls.Add(this.rbOn);
            this.TabEt.Controls.Add(this.rbBefore);
            this.TabEt.Controls.Add(this.rbAfter);
            this.TabEt.Controls.Add(this.gbCET);
            this.TabEt.Location = new System.Drawing.Point(4, 22);
            this.TabEt.Name = "TabEt";
            this.TabEt.Padding = new System.Windows.Forms.Padding(3);
            this.TabEt.Size = new System.Drawing.Size(441, 331);
            this.TabEt.TabIndex = 0;
            this.TabEt.Text = "1. Past Changes";
            this.TabEt.UseVisualStyleBackColor = true;
            // 
            // lblDisplayChanges
            // 
            this.lblDisplayChanges.AutoSize = true;
            this.lblDisplayChanges.Location = new System.Drawing.Point(5, 20);
            this.lblDisplayChanges.Name = "lblDisplayChanges";
            this.lblDisplayChanges.Size = new System.Drawing.Size(141, 13);
            this.lblDisplayChanges.TabIndex = 11;
            this.lblDisplayChanges.Text = "Display changes made:";
            // 
            // afterdtpicker
            // 
            this.afterdtpicker.CustomFormat = "yyyyMMdd";
            this.afterdtpicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.afterdtpicker.Location = new System.Drawing.Point(336, 16);
            this.afterdtpicker.Name = "afterdtpicker";
            this.afterdtpicker.Size = new System.Drawing.Size(87, 21);
            this.afterdtpicker.TabIndex = 10;
            // 
            // rbOn
            // 
            this.rbOn.AutoSize = true;
            this.rbOn.Checked = true;
            this.rbOn.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbOn.Location = new System.Drawing.Point(218, 18);
            this.rbOn.Name = "rbOn";
            this.rbOn.Size = new System.Drawing.Size(41, 17);
            this.rbOn.TabIndex = 8;
            this.rbOn.TabStop = true;
            this.rbOn.Text = "On";
            this.rbOn.UseVisualStyleBackColor = true;
            this.rbOn.CheckedChanged += new System.EventHandler(this.rbOn_CheckedChanged);
            // 
            // rbBefore
            // 
            this.rbBefore.AutoSize = true;
            this.rbBefore.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbBefore.Location = new System.Drawing.Point(154, 18);
            this.rbBefore.Name = "rbBefore";
            this.rbBefore.Size = new System.Drawing.Size(62, 17);
            this.rbBefore.TabIndex = 6;
            this.rbBefore.Tag = "";
            this.rbBefore.Text = "before";
            this.rbBefore.UseVisualStyleBackColor = true;
            this.rbBefore.CheckedChanged += new System.EventHandler(this.rbBefore_CheckedChanged);
            // 
            // rbAfter
            // 
            this.rbAfter.AutoSize = true;
            this.rbAfter.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAfter.Location = new System.Drawing.Point(267, 18);
            this.rbAfter.Name = "rbAfter";
            this.rbAfter.Size = new System.Drawing.Size(52, 17);
            this.rbAfter.TabIndex = 4;
            this.rbAfter.Text = "after";
            this.rbAfter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbAfter.UseVisualStyleBackColor = true;
            this.rbAfter.CheckedChanged += new System.EventHandler(this.rbAfter_CheckedChanged);
            // 
            // gbCET
            // 
            this.gbCET.Controls.Add(this.btnUndo);
            this.gbCET.Controls.Add(this.grdDisplay);
            this.gbCET.Controls.Add(this.btnProcess);
            this.gbCET.ForeColor = System.Drawing.Color.Red;
            this.gbCET.Location = new System.Drawing.Point(3, 67);
            this.gbCET.Name = "gbCET";
            this.gbCET.Size = new System.Drawing.Size(432, 258);
            this.gbCET.TabIndex = 2;
            this.gbCET.TabStop = false;
            this.gbCET.Text = "The list below contains the Entities that were kept originally";
            // 
            // btnUndo
            // 
            this.btnUndo.ForeColor = System.Drawing.Color.Black;
            this.btnUndo.Location = new System.Drawing.Point(327, 227);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(75, 23);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // grdDisplay
            // 
            this.grdDisplay.AllowUserToAddRows = false;
            this.grdDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLastKept,
            this.colFirstKept,
            this.ColTypeKept,
            this.colLastChng,
            this.colTypeChnge,
            this.EID_KEPT,
            this.EID_CHANGED,
            this.ECU_ROW_ID});
            this.grdDisplay.Location = new System.Drawing.Point(8, 21);
            this.grdDisplay.MultiSelect = false;
            this.grdDisplay.Name = "grdDisplay";
            this.grdDisplay.Size = new System.Drawing.Size(418, 187);
            this.grdDisplay.TabIndex = 4;
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnProcess.Location = new System.Drawing.Point(339, 274);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Undo";
            this.btnProcess.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(441, 331);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "3. Utilites";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPurge);
            this.groupBox1.Controls.Add(this.dtpickerPurge);
            this.groupBox1.Controls.Add(this.rbHistoryDate);
            this.groupBox1.Controls.Add(this.rbPurgeAll);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 146);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Purge History";
            // 
            // btnPurge
            // 
            this.btnPurge.ForeColor = System.Drawing.Color.Black;
            this.btnPurge.Location = new System.Drawing.Point(280, 117);
            this.btnPurge.Name = "btnPurge";
            this.btnPurge.Size = new System.Drawing.Size(75, 23);
            this.btnPurge.TabIndex = 12;
            this.btnPurge.Text = "Purge";
            this.btnPurge.UseVisualStyleBackColor = true;
            this.btnPurge.Click += new System.EventHandler(this.btnPurge_Click);
            // 
            // dtpickerPurge
            // 
            this.dtpickerPurge.CustomFormat = "yyyyMMdd";
            this.dtpickerPurge.Enabled = false;
            this.dtpickerPurge.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpickerPurge.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpickerPurge.Location = new System.Drawing.Point(280, 53);
            this.dtpickerPurge.Name = "dtpickerPurge";
            this.dtpickerPurge.Size = new System.Drawing.Size(94, 21);
            this.dtpickerPurge.TabIndex = 11;
            // 
            // rbHistoryDate
            // 
            this.rbHistoryDate.AutoSize = true;
            this.rbHistoryDate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHistoryDate.Location = new System.Drawing.Point(16, 53);
            this.rbHistoryDate.Name = "rbHistoryDate";
            this.rbHistoryDate.Size = new System.Drawing.Size(258, 17);
            this.rbHistoryDate.TabIndex = 1;
            this.rbHistoryDate.Text = "Purge historical created records prior to:";
            this.rbHistoryDate.UseVisualStyleBackColor = true;
            this.rbHistoryDate.CheckedChanged += new System.EventHandler(this.rbHistoryDate_CheckedChanged);
            // 
            // rbPurgeAll
            // 
            this.rbPurgeAll.AutoSize = true;
            this.rbPurgeAll.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPurgeAll.Location = new System.Drawing.Point(16, 30);
            this.rbPurgeAll.Name = "rbPurgeAll";
            this.rbPurgeAll.Size = new System.Drawing.Size(188, 17);
            this.rbPurgeAll.TabIndex = 0;
            this.rbPurgeAll.Text = "Purge ALL historical records.";
            this.rbPurgeAll.UseVisualStyleBackColor = true;
            this.rbPurgeAll.CheckedChanged += new System.EventHandler(this.rbPurgeAll_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(476, 406);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // colLastKept
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Blue;
            this.colLastKept.DefaultCellStyle = dataGridViewCellStyle1;
            this.colLastKept.HeaderText = "Last(Kept)";
            this.colLastKept.Name = "colLastKept";
            this.colLastKept.ReadOnly = true;
            // 
            // colFirstKept
            // 
            this.colFirstKept.HeaderText = "First(Kept)";
            this.colFirstKept.Name = "colFirstKept";
            this.colFirstKept.ReadOnly = true;
            // 
            // ColTypeKept
            // 
            this.ColTypeKept.HeaderText = "Type(Kept)";
            this.ColTypeKept.Name = "ColTypeKept";
            this.ColTypeKept.ReadOnly = true;
            // 
            // colLastChng
            // 
            this.colLastChng.HeaderText = "Last(Chng)";
            this.colLastChng.Name = "colLastChng";
            this.colLastChng.ReadOnly = true;
            // 
            // colTypeChnge
            // 
            this.colTypeChnge.HeaderText = "Type(Chng)";
            this.colTypeChnge.Name = "colTypeChnge";
            this.colTypeChnge.ReadOnly = true;
            // 
            // EID_KEPT
            // 
            this.EID_KEPT.HeaderText = "EID_KEPT";
            this.EID_KEPT.Name = "EID_KEPT";
            this.EID_KEPT.ReadOnly = true;
            this.EID_KEPT.Visible = false;
            // 
            // EID_CHANGED
            // 
            this.EID_CHANGED.HeaderText = "EID_CHANGED";
            this.EID_CHANGED.Name = "EID_CHANGED";
            this.EID_CHANGED.ReadOnly = true;
            this.EID_CHANGED.Visible = false;
            // 
            // ECU_ROW_ID
            // 
            this.ECU_ROW_ID.HeaderText = "ECU_ROW_ID";
            this.ECU_ROW_ID.Name = "ECU_ROW_ID";
            this.ECU_ROW_ID.ReadOnly = true;
            this.ECU_ROW_ID.Visible = false;
            // 
            // frmUndo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(503, 452);
            this.Controls.Add(this.tabCET);
            this.Controls.Add(this.lblCET);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUndo";
            this.Text = "FormPayee";
            this.tabCET.ResumeLayout(false);
            this.TabEt.ResumeLayout(false);
            this.TabEt.PerformLayout();
            this.gbCET.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDisplay)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCET;
        private System.Windows.Forms.TabControl tabCET;
        private System.Windows.Forms.TabPage TabEt;
        private System.Windows.Forms.GroupBox gbCET;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.RadioButton rbAfter;
        private System.Windows.Forms.RadioButton rbOn;
        private System.Windows.Forms.RadioButton rbBefore;
        private System.Windows.Forms.DateTimePicker afterdtpicker;
        private System.Windows.Forms.DataGridView grdDisplay;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Label lblDisplayChanges;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbHistoryDate;
        private System.Windows.Forms.RadioButton rbPurgeAll;
        private System.Windows.Forms.DateTimePicker dtpickerPurge;
        private System.Windows.Forms.Button btnPurge;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastKept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFirstKept;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTypeKept;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastChng;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTypeChnge;
        private System.Windows.Forms.DataGridViewTextBoxColumn EID_KEPT;
        private System.Windows.Forms.DataGridViewTextBoxColumn EID_CHANGED;
        private System.Windows.Forms.DataGridViewTextBoxColumn ECU_ROW_ID;

    }
}