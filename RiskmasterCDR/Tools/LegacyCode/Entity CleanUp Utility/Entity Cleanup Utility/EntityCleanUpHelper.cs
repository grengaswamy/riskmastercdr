﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Cleanup_Utility
{
    static class EntityCleanUpHelper
    {
        /// <summary>
        /// Gets or sets the gs entities.
        /// </summary>
        /// <value>
        /// The gs entities.
        /// </value>
        public static string gsEntities { get; set; }

        public static string gsStr { get; set; }
    }
}
