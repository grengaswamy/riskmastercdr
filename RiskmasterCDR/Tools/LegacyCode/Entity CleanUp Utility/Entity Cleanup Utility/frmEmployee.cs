﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;

namespace Entity_Cleanup_Utility
{
    public partial class frmEmployee : Form, IEntityForm
    {

        public System.Windows.Forms.StatusBarPanel pfrm_sbp1 = null;
        // bpaskova JIRA 4338
        private const string officePhoneCode = "O";
        private const string homePhoneCode = "H";

        #region IEntityForm Members

        /// <summary>
        /// Gets the control pr grid.
        /// </summary>
        /// <value>
        /// The control pr grid.
        /// </value>
        public DataGridView CtlPRGrid
        {
            get
            {
                return this.ctlPRGrid;
            }
        }

        /// <summary>
        /// Gets the control p grid det.
        /// </summary>
        /// <value>
        /// The control p grid det.
        /// </value>
        public DataGridView CtlPGridDet
        {
            get
            {
                return this.ctlPGridDet;
            }
        }

        /// <summary>
        /// Gets the control tabs.
        /// </summary>
        /// <value>
        /// The control tabs.
        /// </value>
        public TabControl CtlTabs
        {
            get
            {
                return this.ctlTabs;
            }
        }

        #endregion

        #region Others Global Prop
        /// <summary>
        /// Search Type
        /// </summary>
        /// 
        bool buttonClick = false;
        static Boolean rowsflag = true;
        DateTime fBirthdate;

        private enum eSearchType
        {
            stLFName = 0,
            stLNameSpec = 1,
            stTaxId = 2,
            stEmpNo = 3
        }
        private enum LockFields
        {
            Lock = 0,
            Unlock = 1
        }

        public frmEmployee()
        {
            InitializeComponent();
            rb_Emp_Srch_Type.Checked = true;
            rb_Emp_Srch_Type.Enabled = false;
            rb_Emp_Srch_TaxId.Checked = true;

        }
        #endregion

        #region frmEmployee_Load
        private void frmEmployee_Load(object sender, EventArgs e)
        {
            try
            {
                #region Defining Events Handler
                ctlTabs.DrawMode = TabDrawMode.OwnerDrawFixed;
                ctlTabs.DrawItem += new DrawItemEventHandler(ctlTabs_DrawItem);
                #endregion

                getEntities();
                //loadSexCodes(); Not been used in existing app
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region ctlTabs_Selected
        private void ctlTabs_Selected(object sender, TabControlEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                switch (e.TabPageIndex)
                {
                    case 0:     // Search

                        rb_Emp_Srch_LNFN.Checked = false;
                        rb_Emp_Srch_TaxId.Checked = true;
                        rb_Emp_Srch_EmpNo.Checked = false;
                        rb_Emp_Srch_LNS.Checked = false;
                        rowsflag = true;   // Setting true . So need to call search again. As criteria may change
                        break;
                    case 1:     // Results
                        if (!buttonClick)           // In case of buttonclick search is 1. Means this function is already called at button click
                        {                               // No need to call again while just setting index
                            if (rowsflag == true)
                            {
                                searchCriteria();
                            }
                        }
                        break;
                    case 2:         // Details
                        if (!buttonClick)           // In case of buttonclick Display is 1. Means this function is already called at button click
                        {
                            if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                            {
                                rowsflag = false;       // Setting false . So no need to call search again
                                ctlTabs.SelectTab(1);
                                MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                                return;

                            }
                            else
                            {
                                rowsflag = false;       // Setting false . So no need to call search again. As grid is full
                                sDisplay();
                            }
                        }
                        break;
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }


        #endregion

        #region RadioButton Events

        #region rbPayeeLastNameSpecific_CheckedChanged
        private void rb_Emp_Srch_LNS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rb_Emp_Srch_LNS.Checked)
                {
                    txtLastN.Visible = true;
                    txtLastN.Text = string.Empty;
                    txtLastN.Focus();
                }
                else
                {
                    txtLastN.Text = string.Empty;
                    txtLastN.Visible = false;

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion
        #endregion

        #region Private Methods

        #region ctlTabs_DrawItem for making active index bold
        void ctlTabs_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == ctlTabs.SelectedIndex)
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        new Font(ctlTabs.Font, FontStyle.Bold),
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
                else
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        ctlTabs.Font,
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region SearchByNameType
        /// <summary>
        /// Searches the last name of the by.
        /// </summary>
        // bpaskova JIRA 4050 : added method return value
        // private void searchWithNameOptions(Int16 _searchCriteria, String _optCaption = "")
        private Int16 searchWithNameOptions(Int16 _searchCriteria, String _optCaption = "")
        {
            Int16 iNextTabId = 1;
            try
            {
                ctlPRGrid.Rows.Clear();
                ctlPGridDet.Rows.Clear();

                ctlPRGrid.Columns[0].Visible = false;
                ctlPRGrid.Columns[1].Visible = false;
                ctlPRGrid.Columns[2].Visible = false;
                ctlPRGrid.Columns[3].Visible = false;
                ctlPRGrid.Columns[4].Visible = false;


                StringBuilder _Criteria = new StringBuilder();
                StringBuilder sbSql = new StringBuilder();

                Double _counter = 0;

                if (_searchCriteria == Convert.ToInt16(eSearchType.stLFName))   // done
                {
                    #region If Criteria is last name
                    _Criteria.Append("Searched by: Last Name,First Name  ");
                    sbSql.Append(" SELECT LAST_NAME, FIRST_NAME, COUNT(*) AS CNT, MAX(TAX_ID) TAX_Column  FROM ENTITY")
                         .Append(" WHERE DELETED_FLAG = 0 AND ENTITY_TABLE_ID IN (1060) GROUP BY LAST_NAME, FIRST_NAME HAVING COUNT(*) > 1")
                         .Append(" ORDER BY LAST_NAME, FIRST_NAME");



                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            _counter++;
                            ctlPRGrid.Rows.Add(dbReader.GetString("LAST_NAME")
                                , dbReader.GetString("FIRST_NAME")
                                , dbReader.GetString("TAX_Column"), dbReader.GetInt("CNT"));
                        }


                        ctlPRGrid.Columns[0].Visible = true;
                        ctlPRGrid.Columns[0].HeaderText = "Last Name";

                        ctlPRGrid.Columns[1].Visible = true;
                        ctlPRGrid.Columns[1].HeaderText = "First Name";

                        ctlPRGrid.Columns[2].Visible = true;
                        ctlPRGrid.Columns[2].HeaderText = "Tax-ID/SSN";

                        ctlPRGrid.Columns[3].Visible = true;
                        ctlPRGrid.Columns[3].HeaderText = "Count";


                        _Criteria.Append("Results: " + _counter);
                        lblCriteria.Text = _Criteria.ToString();

                    }

                    #endregion

                }
                else if (_searchCriteria == Convert.ToInt16(eSearchType.stLNameSpec))   // Done
                {
                    #region If Criteria is last name specific
                    if (_optCaption == string.Empty)
                    {
                        MessageBox.Show("Last name is required for selected option.", "Entity CleanUp Utility Says :");
                        // bpaskova JIRA 4050 start
                        //ctlTabs.SelectTab(0);
                        iNextTabId = 0;
                        ctlTabs.SelectTab(iNextTabId);
                        // bpaskova JIRA 4050 end
                        rb_Emp_Srch_LNS.Checked = true;

                    }
                    else
                    {
                        _Criteria.Append("Searched by: Last Name(Specific)  ");
                        sbSql.Append(@"SELECT LAST_NAME, FIRST_NAME, COUNT(*) AS CNT,TABLE_NAME, ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID
                                             FROM ENTITY, GLOSSARY_TEXT WHERE DELETED_FLAG = 0 
                                             AND  ENTITY_TABLE_ID IN (1060)                                                                  
							                 AND ENTITY_TABLE_ID = TABLE_ID  
                                             AND UPPER(LAST_NAME) LIKE " + "'%" + _optCaption.ToUpper() + "%'"
                                               + " GROUP BY LAST_NAME, FIRST_NAME, TABLE_NAME,ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID ORDER BY LAST_NAME, FIRST_NAME ");


                        using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                        {
                            while (dbReader.Read())
                            {
                                _counter++;
                                ctlPRGrid.Rows.Add(dbReader.GetString("LAST_NAME")
                                    , dbReader.GetString("FIRST_NAME")
                                    , dbReader.GetString("TABLE_NAME")
                                    , dbReader.GetInt("ENTITY_ID")
                                    , dbReader.GetInt("ENTITY_TABLE_ID"));
                            }

                            ctlPRGrid.Columns[0].Visible = true;
                            ctlPRGrid.Columns[0].HeaderText = "Last Name";

                            ctlPRGrid.Columns[1].Visible = true;
                            ctlPRGrid.Columns[1].HeaderText = "First Name";

                            ctlPRGrid.Columns[2].Visible = true;
                            ctlPRGrid.Columns[2].HeaderText = "Entity Type";

                            ctlPRGrid.Columns[3].Visible = true;
                            ctlPRGrid.Columns[3].HeaderText = "Enttity ID";

                            _Criteria.Append("Results: " + _counter);
                            lblCriteria.Text = _Criteria.ToString();

                        }
                    }
                    #endregion

                }
                else if (_searchCriteria == Convert.ToInt16(eSearchType.stTaxId))       // done
                {
                    #region If Criteria is Tax Id

                    _Criteria.Append("Searched by: Tax ID  ");
                    sbSql.Append(@"SELECT TAX_ID, COUNT(*) AS CNT FROM ENTITY WHERE DELETED_FLAG = 0 AND  ENTITY_TABLE_ID IN (1060)
                                   GROUP BY TAX_ID HAVING COUNT(*) > 1  ORDER BY TAX_ID");


                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                    {
                        // bpaskova JIRA 4336 begin
                        /*
                        while (dbReader.Read())
                        {
                            _counter++;
                            ctlPRGrid.Rows.Add(dbReader.GetString("TAX_ID")
                                , dbReader.GetInt("CNT"));
                        }
                         */
                        Int32 iEmptyCount = 0;
                        Boolean haveToAddEmpty = false;
                        while (dbReader.Read())
                        {
                            String sTmpTaxId = dbReader.GetString("TAX_ID");
                            Int32 iTmpCount = dbReader.GetInt("CNT");
                            if (String.IsNullOrWhiteSpace(sTmpTaxId))
                            {
                                haveToAddEmpty = true;
                                iEmptyCount += iTmpCount;
                            }
                            else
                            {
                                if (haveToAddEmpty)
                                {
                                    haveToAddEmpty = false;
                                    _counter++;
                                    ctlPRGrid.Rows.Add(String.Empty, iEmptyCount);
                                }
                                _counter++;
                                ctlPRGrid.Rows.Add(sTmpTaxId, iTmpCount);
                            }
                        }
                        // bpaskova JIRA 4336 end

                        ctlPRGrid.Columns[0].Visible = true;
                        ctlPRGrid.Columns[0].HeaderText = "Tax-ID/SSN";

                        ctlPRGrid.Columns[1].Visible = true;
                        ctlPRGrid.Columns[1].HeaderText = "Count";


                        _Criteria.Append("Results: " + _counter);
                        lblCriteria.Text = _Criteria.ToString();

                    }

                    #endregion

                }
                else if (_searchCriteria == Convert.ToInt16(eSearchType.stEmpNo))       // Done
                {
                    #region If Criteria is stEmpNo

                    _Criteria.Append("Searched by: Employee Number  ");
                    sbSql.Append("SELECT EM.EMPLOYEE_NUMBER, COUNT(*) AS CNT")
                        .Append(" FROM ENTITY EN JOIN EMPLOYEE EM ON EN.ENTITY_ID = EM.EMPLOYEE_EID")
                        .Append(" WHERE DELETED_FLAG = 0 AND ENTITY_TABLE_ID = 1060")
                        .Append(" GROUP BY EM.EMPLOYEE_NUMBER HAVING COUNT(*) > 1")
                        .Append(" ORDER BY EM.EMPLOYEE_NUMBER");

                    //'fill grid
                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            _counter++;
                            ctlPRGrid.Rows.Add(dbReader.GetString("EMPLOYEE_NUMBER")
                                , dbReader.GetInt("CNT")
                               );
                        }

                        ctlPRGrid.Columns[0].Visible = true;
                        ctlPRGrid.Columns[0].HeaderText = "Employee Number";

                        ctlPRGrid.Columns[1].Visible = true;
                        ctlPRGrid.Columns[1].HeaderText = "Count";

                        _Criteria.Append("Results: " + _counter);
                        lblCriteria.Text = _Criteria.ToString();

                    }

                    #endregion

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            // bpaskova JIRA 4050
            return iNextTabId;
        }
        #endregion

        #region searchCriteria
        // bpaskova JIRA 4050 : added method return value
        // private void searchCriteria()
        private Int16 searchCriteria()
        {
            Int16 iNextTabId = 1;
            try
            {
                frmMain.updateStatus("Searching for Employees............");
                if (rb_Emp_Srch_LNFN.Checked)
                {
                    searchWithNameOptions(Convert.ToInt16(eSearchType.stLFName));
                }
                else if (rb_Emp_Srch_TaxId.Checked)
                {
                    searchWithNameOptions(Convert.ToInt16(eSearchType.stTaxId));
                }
                else if (rb_Emp_Srch_EmpNo.Checked)
                {
                    searchWithNameOptions(Convert.ToInt16(eSearchType.stEmpNo));
                }
                else if (rb_Emp_Srch_LNS.Checked)
                {
                    // bpaskova JIRA 4050 start
                    //searchWithNameOptions(Convert.ToInt16(eSearchType.stLNameSpec), txtLastN.Text.Trim());
                    iNextTabId = searchWithNameOptions(Convert.ToInt16(eSearchType.stLNameSpec), txtLastN.Text.Trim());
                    // bpaskova JIRA 4050 end
                }
                frmMain.updateStatus(" ");
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            // bpaskova JIRA 4050 
            return iNextTabId;
        }
        #endregion

        #region sFieldsType
        private void sFieldsType(LockFields Status)
        {
            try
            {
                if (Status == LockFields.Unlock)
                {
                    txtLastName.Enabled = true;
                    txtFirstName.Enabled = true;
                    txtAddr1.Enabled = true;
                    txtAddr2.Enabled = true;
                    txtCity.Enabled = true;
                    cmbState.Enabled = true;
                    txtZip.Enabled = true;
                    txtBirthDate.Enabled = true;
                    // txtAge.Enabled = true;
                    txtOffPhn.Enabled = true;
                    txtHomPhn.Enabled = true;
                    txttax.Enabled = true;
                    txtEmpNo.Enabled = true;
                    txtEmailDet.Enabled = true;

                    btnSave.Enabled = true;
                    btnEdit.Enabled = false;
                    btnProcess.Enabled = false;

                    txtBirthDate.Visible = false;
                    datetimepicker1.Visible = true;
                    datetimepicker1.Location = new Point(51, 302);

                    if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = " ";
                        //  
                    }
                    else
                    {
                        datetimepicker1.Value = fBirthdate;
                    }
                }
                else if (Status == LockFields.Lock)
                {
                    txtLastName.Enabled = false;
                    txtFirstName.Enabled = false;
                    txtAddr1.Enabled = false;
                    txtAddr2.Enabled = false;
                    txtCity.Enabled = false;
                    cmbState.Enabled = false;
                    txtZip.Enabled = false;
                    txtBirthDate.Enabled = false;
                    txtAge.Enabled = false;
                    txtOffPhn.Enabled = false;
                    txtHomPhn.Enabled = false;
                    txttax.Enabled = false;
                    txtEmpNo.Enabled = false;
                    txtEmailDet.Enabled = false;

                    btnSave.Enabled = false;
                    btnEdit.Enabled = true;
                    btnProcess.Enabled = true;

                    txtBirthDate.Visible = true;
                    datetimepicker1.Location = new Point(51, 326);
                    datetimepicker1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region sClearFields
        private void sClearFields()
        {
            try
            {
                txtLastName.Text = string.Empty;
                txtFirstName.Text = string.Empty;
                txtAddr1.Text = string.Empty;
                txtAddr2.Text = string.Empty;
                txtCity.Text = string.Empty;
                cmbState.SelectedItem = null;
                txtZip.Text = string.Empty;
                txtBirthDate.Text = string.Empty;
                txtAge.Text = string.Empty;
                txtOffPhn.Text = string.Empty;
                txtHomPhn.Text = string.Empty;
                txttax.Text = string.Empty;
                txtEmpNo.Text = string.Empty;
                txtEmailDet.Text = string.Empty;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region fSaveEntity
        private void fSaveEntity()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();

                String sLastName = string.Empty;
                String sFirstname = string.Empty;
                int lEID = 0;
                String sAddr1 = string.Empty;
                String sAddr2 = string.Empty;
                int lStateID = 0;
                String sCity = string.Empty;
                String sZip = string.Empty;
                String sOffPh = string.Empty;
                String sHomePh = string.Empty;
                long lEntityET = 0;
                String sTaxID = string.Empty;
                String sEmail = string.Empty;
                String sDOB = string.Empty;
                String sAge = string.Empty;


                frmMain.updateStatus("Saving entity information.....");
                //'Setting variables from selected records
                sLastName = txtLastName.Text;
                sFirstname = txtFirstName.Text;
                sAddr1 = txtAddr1.Text;
                sAddr2 = txtAddr2.Text;
                sCity = txtCity.Text;


                sZip = txtZip.Text;

                if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    datetimepicker1.Format = DateTimePickerFormat.Custom;
                    datetimepicker1.CustomFormat = " ";
                    //  
                }
                else
                {
                    sDOB = datetimepicker1.Value.ToString("yyyyMMdd");
                }

                sAge = txtAge.Text;
                sOffPh = txtOffPhn.Text;
                sHomePh = txtHomPhn.Text;
                sTaxID = txttax.Text;
                lEID = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells["PDEntityID"].Value);// EntityID           
                sEmail = txtEmailDet.Text;
                lEntityET = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells["PDEntityType"].Value);// Entity_table_id           

                FillCombo selectedStateData = (FillCombo)cmbState.SelectedItem;
                lStateID = (int)selectedStateData.Value;

                //'Update entity record
                gsSql.Append("UPDATE ENTITY SET ");
                gsSql.Append(" LAST_NAME = " + "'" + sLastName + "'");
                gsSql.Append(" ,FIRST_NAME = " + "'" + sFirstname + "'");
                gsSql.Append(" ,ADDR1 = " + "'" + sAddr1 + "'");
                gsSql.Append(" ,ADDR2 = " + "'" + sAddr2 + "'");
                gsSql.Append(" ,CITY = " + "'" + sCity + "'");
                gsSql.Append(" ,ZIP_CODE = " + "'" + sZip + "'");
                gsSql.Append(" ,STATE_ID = " + lStateID);
                gsSql.Append(" ,PHONE1 = " + "'" + sOffPh + "'");
                gsSql.Append(" ,PHONE2 = " + "'" + sHomePh + "'");
                gsSql.Append(" ,ENTITY_TABLE_ID =" + lEntityET);
                gsSql.Append(" ,TAX_ID = " + "'" + sTaxID + "'");
                gsSql.Append(" ,EMAIL_ADDRESS = " + "'" + sEmail + "'");
                gsSql.Append(" ,BIRTH_DATE = " + "'" + sDOB + "'");
                //     gsSql.Append("UPDATED_BY_USER = " + "'" + sLastName + "'");      CHECK IT LATER ON
                //     gsSql.Append("DTTM_RCD_LAST_UPD = " + "'" + sLastName + "'"); CHECK IT LATER ON
                gsSql.Append(" WHERE ENTITY_ID  = " + lEID);


                DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                // bpaskova JIRA 4338 start
                updatePhoneInDb(officePhoneCode, lEID, sOffPh);
                updatePhoneInDb(homePhoneCode, lEID, sHomePh);
                // bpaskova JIRA 4338 end

                sCellClick();   // Get the Updated values
                sFieldsType(LockFields.Lock);

                frmMain.updateStatus(" ");

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        // bpaskova JIRA 4338 start
        private void updatePhoneInDb(string shortPhoneCode, int entityId, string phoneNo)
        {
            DbWriter objDbWriter = null;
            try
            {
                int phoneCodeId = phoneCodeIdByShortCode(shortPhoneCode);
                if (phoneCodeId > 0)
                {
                    if (String.IsNullOrWhiteSpace(phoneNo))
                    {
                        string sSql = string.Format("DELETE FROM ADDRESS_X_PHONEINFO WHERE ENTITY_ID={0} AND PHONE_CODE = {1}", entityId, phoneCodeId);
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, sSql);
                    }
                    else
                    {
                        bool recordExists = false;
                        string sSql = string.Format("SELECT * FROM ADDRESS_X_PHONEINFO WHERE ENTITY_ID={0} AND PHONE_CODE = {1}", entityId, phoneCodeId);
                        using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sSql))
                        {
                            recordExists = !object.ReferenceEquals(dbReader, null) && dbReader.Read();
                        }

                        objDbWriter = DbFactory.GetDbWriter(Helper.gsConnectString);
                        objDbWriter.Tables.Add("ADDRESS_X_PHONEINFO");
                        objDbWriter.Fields.Add("PHONE_NO", phoneNo);

                        if (recordExists)
                        {
                            objDbWriter.Where.Add(string.Format("ENTITY_ID={0} AND PHONE_CODE = {1}", entityId, phoneCodeId));
                        }
                        else
                        {
                            int phoneId = getNextUID("ADDRESS_X_PHONEINFO");
                            objDbWriter.Fields.Add("PHONE_ID", phoneId);
                            objDbWriter.Fields.Add("ENTITY_ID", entityId);
                            objDbWriter.Fields.Add("PHONE_CODE", phoneCodeId);
                        }
                        objDbWriter.Execute();
                    }
                }
            }
            finally
            {
                if (objDbWriter != null)
                {
                    objDbWriter = null;
                }
            }
        }

        private int phoneCodeIdByShortCode(string phoneShortCode)
        {
            int codeId = 0;
            string sSql = string.Format("SELECT CODE_ID FROM CODES,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='PHONES_CODES' AND " +
                                            "GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.SHORT_CODE='{0}'", phoneShortCode);
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sSql))
            {
                if (dbReader.Read())
                {
                    codeId = dbReader.GetInt32("CODE_ID");
                }
            }
            return codeId;
        }

        private int getNextUID(string p_sTableNameSystem)
        {
            int iNextId = 0;
            using (DbConnection dbConnection = DbFactory.GetDbConnection(Helper.gsConnectString))
            {
                dbConnection.Open();
                using (DbTransaction dbTrans = dbConnection.BeginTransaction())
                {
                    //Get current Id
                    int iCurrentId = 0;
                    using (DbReader objReader = dbConnection.ExecuteReader("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableNameSystem + "'", dbTrans))
                    {
                        objReader.Read();
                        iCurrentId = objReader.GetInt32(0);
                    }
                    iNextId = iCurrentId + 1;

                    //Save new Id
                    string sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextId;
                    sSQL = sSQL + " WHERE SYSTEM_TABLE_NAME = '" + p_sTableNameSystem + "'";
                    sSQL = sSQL + " AND NEXT_UNIQUE_ID = " + iCurrentId;
                    dbConnection.ExecuteNonQuery(sSQL, dbTrans);
                    dbTrans.Commit();
                }
            }

            return iNextId;
        }
        // bpaskova JIRA 4338 end
        #endregion

        #region CellClick Function

        private void sCellClick()
        {
            bool bEmpDetailsFound = false;
            try
            {
                int lEntityId;
                StringBuilder gsSql = new StringBuilder();
                if (ctlPGridDet.Rows.Count < 1)
                {
                    return;
                }
                //'enable Edit button
                btnEdit.Enabled = true;
                btnProcess.Enabled = true;
                btnSave.Enabled = false;

                lEntityId = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[5].Value); // First Column

                //getEntities();
                //loadSexCodes();   runs on load

                // bpaskova JIRA 4338
                //gsSql.Append(" SELECT EN.ENTITY_ID, EN.LAST_NAME, EN.FIRST_NAME, EN.PHONE1, EN.PHONE2, EN.ADDR1, EN.ADDR2, EN.CITY, S.STATE_ID ST_CD,S.STATE_ROW_ID, ");
                if (Helper.gsDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    gsSql.Append(" SELECT EN.ENTITY_ID, EN.LAST_NAME, EN.FIRST_NAME, EN.ADDR1, EN.ADDR2, EN.CITY, S.STATE_ID ST_CD,S.STATE_ROW_ID, ");
                    gsSql.Append(" EN.ZIP_CODE, EN.DTTM_RCD_ADDED, EN.ADDED_BY_USER, EN.TAX_ID, EM.EMPLOYEE_NUMBER,EN.EMAIL_ADDRESS,");
                    gsSql.Append(" (CASE  WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN TO_DATE(BIRTH_DATE,'yyyymmdd') END) AS FORMAT_BIRTH_DATE, ");
                    gsSql.Append(" (CASE  WHEN BIRTH_DATE IS NULL THEN NULL WHEN BIRTH_DATE='' THEN NULL WHEN BIRTH_DATE=0 THEN NULL ELSE TO_DATE(BIRTH_DATE,'yyyymmdd') END) AS BIRTH_DATE,");
                    gsSql.Append(" (CASE WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM TO_DATE(EN.BIRTH_DATE,'yyyymmdd')) WHEN ((EN.BIRTH_DATE=NULL AND EN.BIRTH_DATE IS NULL )) THEN 0 END) AS AGE");
                    gsSql.Append(" FROM ENTITY EN ");
                    gsSql.Append(" LEFT JOIN EMPLOYEE EM ON EN.ENTITY_ID = EM.EMPLOYEE_EID LEFT JOIN STATES S ON EN.STATE_ID = s.STATE_ROW_ID");
                    gsSql.AppendFormat(" WHERE EN.ENTITY_ID = {0} ", lEntityId);
                }
                else
                {
                    gsSql.Append(" SELECT EN.ENTITY_ID, EN.LAST_NAME, EN.FIRST_NAME, EN.ADDR1, EN.ADDR2, EN.CITY, S.STATE_ID ST_CD,S.STATE_ROW_ID, ");
                    gsSql.Append(" EN.ZIP_CODE, EN.DTTM_RCD_ADDED, EN.ADDED_BY_USER, EN.TAX_ID, EM.EMPLOYEE_NUMBER,EN.EMAIL_ADDRESS,");
                    gsSql.Append(" (CASE  WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN CAST(BIRTH_DATE AS DATETIME) END) AS FORMAT_BIRTH_DATE, ");
                    gsSql.Append(" (CASE  WHEN BIRTH_DATE IS NULL THEN 'NULL' WHEN BIRTH_DATE='' THEN 'NULL' WHEN BIRTH_DATE=0 THEN 'NULL' ELSE BIRTH_DATE END) AS BIRTH_DATE,");
                    gsSql.Append(" (CASE WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN DATEDIFF(YY,EN.BIRTH_DATE,GETDATE()) WHEN ((EN.BIRTH_DATE=NULL AND EN.BIRTH_DATE IS NULL )) THEN 0 END) AS AGE");
                    gsSql.Append(" FROM ENTITY EN ");
                    gsSql.Append(" LEFT JOIN EMPLOYEE EM ON EN.ENTITY_ID = EM.EMPLOYEE_EID LEFT JOIN STATES S ON EN.STATE_ID = s.STATE_ROW_ID");
                    gsSql.AppendFormat(" WHERE EN.ENTITY_ID = {0} ", lEntityId);
                }

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    if (dbReader.Read())
                    {
                        bEmpDetailsFound = true;
                        txtLastName.Text = dbReader.GetString("LAST_NAME");
                        txtFirstName.Text = dbReader.GetString("FIRST_NAME");
                        txtAddr1.Text = dbReader.GetString("ADDR1");
                        txtAddr2.Text = dbReader.GetString("ADDR2");
                        txtCity.Text = dbReader.GetString("CITY");
                        // bpaskova JIRA 4338
                        //txtOffPhn.Text = dbReader.GetString("PHONE1");
                        //txtHomPhn.Text = dbReader.GetString("PHONE2");
                        txttax.Text = dbReader.GetString("TAX_ID");
                        txtEmailDet.Text = dbReader.GetString("EMAIL_ADDRESS");
                        txtBirthDate.Text = dbReader.GetDateTime("BIRTH_DATE").ToShortDateString();
                        txtAge.Text = dbReader.GetInt("AGE").ToString();
                        txtZip.Text = dbReader.GetString("ZIP_CODE");
                        txtEmpNo.Text = dbReader.GetString("EMPLOYEE_NUMBER");
                        fBirthdate = dbReader.GetDateTime("FORMAT_BIRTH_DATE");
                        FillCombo obj1 = new FillCombo();
                        obj1.Name = dbReader.GetString("ST_CD").ToString();
                        obj1.Value = dbReader.GetInt("STATE_ROW_ID");
                        cmbState.SelectedItem = obj1;

                    }
                    else
                    {
                        bEmpDetailsFound = false;
                        txtLastName.Text = string.Empty;
                        txtFirstName.Text = string.Empty;
                        txtAddr1.Text = string.Empty;
                        txtAddr2.Text = string.Empty;
                        txtCity.Text = string.Empty;
                        // bpaskova JIRA 4338
                        //txtOffPhn.Text = string.Empty;
                        //txtHomPhn.Text = string.Empty;
                        txttax.Text = string.Empty;
                        txtEmailDet.Text = string.Empty;
                        txtBirthDate.Text = string.Empty;
                        txtAge.Text = string.Empty;
                        txtZip.Text = string.Empty;
                        txtEmpNo.Text = string.Empty;
                        fBirthdate = default(DateTime);
                        FillCombo obj1 = new FillCombo();
                        obj1.Name = string.Empty;
                        obj1.Value = default(int);
                        cmbState.SelectedItem = obj1;

                    }
                }
                // bpaskova JIRA 4338 start
                gsSql.Clear();
                txtOffPhn.Text = string.Empty;
                txtHomPhn.Text = string.Empty;
                gsSql.Append("SELECT CODES.SHORT_CODE, PHONE_NO ");
                gsSql.Append("FROM ADDRESS_X_PHONEINFO, CODES, GLOSSARY ");
                gsSql.Append("WHERE ADDRESS_X_PHONEINFO.PHONE_CODE=CODES.CODE_ID AND GLOSSARY.SYSTEM_TABLE_NAME='PHONES_CODES' AND ");
                gsSql.AppendFormat("GLOSSARY.TABLE_ID=CODES.TABLE_ID AND ENTITY_ID={0} AND CODES.SHORT_CODE IN ('{1}', '{2}')", lEntityId, officePhoneCode, homePhoneCode);
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        string phoneCode = dbReader.GetString("SHORT_CODE");
                        if (phoneCode.Equals(officePhoneCode))
                        {
                            txtOffPhn.Text = dbReader.GetString("PHONE_NO");
                        }
                        else if (phoneCode.Equals(homePhoneCode))
                        {
                            txtHomPhn.Text = dbReader.GetString("PHONE_NO");
                        }
                    }
                }
                // bpaskova JIRA 4338 end
                sFieldsType(LockFields.Lock);
                btnEdit.Enabled = bEmpDetailsFound;
                if (ctlPGridDet.CurrentRow.Cells[0].Tag == "NO_EMP")
                {
                    MessageBox.Show("Entities with \"Keep\" field disabled have no associated data with employee table and should be removed."," Entity cleanup code says :");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region DisplayFunction

        private void sDisplay()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                frmMain.updateStatus("Details for Employees............");
                String sTaxID = string.Empty;
                String sLtName = string.Empty;
                String sFirstname = string.Empty;
                String EmpNo = string.Empty;
                int lEntityID = 0;
                int lTableID;
                String sTableName = string.Empty;
                String sFName = string.Empty;
                String sLName = string.Empty;
                String s1099Parent = string.Empty;
                StringBuilder sbSql = new StringBuilder();
                Double counter = 0;

                lblCriteriaDet.Text = string.Empty;
                ctlPGridDet.Rows.Clear();


                if (ctlPRGrid.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for duplicates before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPRGrid.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }

                if (rb_Emp_Srch_EmpNo.Checked == true)
                {
                    EmpNo = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[0].Value.ToString();   // First Column

                    sbSql.Append(" SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME,E.ENTITY_TABLE_ID, G.TABLE_NAME, E.PARENT_1099_EID, EM.EMPLOYEE_EID FROM ENTITY E");
                    sbSql.Append(" JOIN GLOSSARY_TEXT G ON E.ENTITY_TABLE_ID = G.TABLE_ID");
                    sbSql.Append(" JOIN EMPLOYEE EM ON E.ENTITY_ID = EM.EMPLOYEE_EID");

                    if (EmpNo == "NULL" || EmpNo == "")
                    {
                        sbSql.Append(" WHERE EM.EMPLOYEE_NUMBER IS NULL");
                    }
                    else
                    {
                        sbSql.Append(" WHERE UPPER(EM.EMPLOYEE_NUMBER) =" + "'" + EmpNo + "'");
                    }

                    sbSql.Append(" AND E.ENTITY_TABLE_ID IN (12, 1060) AND E.DELETED_FLAG = 0 ORDER BY LAST_NAME, FIRST_NAME");
                    lblCriteriaDet.Text = "Display for: " + EmpNo;

                }
                else if (rb_Emp_Srch_LNFN.Checked == true)
                {
                    sLtName = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[0].Value.ToString();// First Column
                    sFirstname = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[1].Value.ToString(); // Second Column

                    sbSql.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME, ENTITY_TABLE_ID, TABLE_NAME, PARENT_1099_EID, EMPLOYEE_EID")
                    .Append(" FROM ENTITY JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = TABLE_ID")
                    .Append(" LEFT JOIN EMPLOYEE ON ENTITY_ID = EMPLOYEE_EID")
                    .AppendFormat(" WHERE UPPER(LAST_NAME) ='{0}'", sLtName.ToUpper());
                    sbSql.Append(" AND ((UPPER(FIRST_NAME) LIKE " + "'%" + sFirstname.ToUpper() + "%'");
                    sbSql.Append(" OR (FIRST_NAME IS NULL))  AND ENTITY_TABLE_ID in(12,1060) AND DELETED_FLAG = 0)");
                    sbSql.Append(" ORDER BY LAST_NAME, FIRST_NAME");

                    lblCriteriaDet.Text = "Display for: " + sLtName + " " + sFirstname;
                }
                else if (rb_Emp_Srch_TaxId.Checked == true)
                {
                    sTaxID = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[0].Value.ToString(); // First Column
                    sbSql.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME, ENTITY_TABLE_ID, TABLE_NAME, PARENT_1099_EID, EMPLOYEE_EID FROM ENTITY")
                    .Append(" JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = TABLE_ID")
                    .Append(" LEFT JOIN EMPLOYEE ON ENTITY_ID = EMPLOYEE_EID");

                    if (sTaxID == "NULL" || string.IsNullOrEmpty(sTaxID))
                    {
                        sbSql.Append(" WHERE TAX_ID IS NULL");
                    }
                    else
                    {
                        sbSql.Append(" WHERE TAX_ID =" + "'" + sTaxID + "'");
                    }
                    sbSql.Append(" AND ENTITY_TABLE_ID = TABLE_ID   AND ENTITY_TABLE_ID in(12,1060)  AND DELETED_FLAG = 0 ORDER BY LAST_NAME, FIRST_NAME");
                    lblCriteriaDet.Text = "Display for: " + sTaxID;
                }
                else
                {
                    sLtName = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[0].Value.ToString();// First Column
                    sFirstname = ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells[1].Value.ToString(); // Second Column

                    sbSql.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME, ENTITY_TABLE_ID, TABLE_NAME, PARENT_1099_EID, EMPLOYEE_EID")
                    .Append(" FROM ENTITY JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = TABLE_ID")
                    .Append(" LEFT JOIN EMPLOYEE ON ENTITY_ID = EMPLOYEE_EID")
                    .AppendFormat(" WHERE UPPER(LAST_NAME) ='{0}'", sLtName.ToUpper())
                    .Append(" AND ENTITY_TABLE_ID in(12,1060) AND DELETED_FLAG = 0")
                    .Append(" ORDER BY LAST_NAME, FIRST_NAME");

                    lblCriteriaDet.Text = "Display for: " + sLtName;
                }

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        counter++;
                        lEntityID = dbReader.GetInt("ENTITY_ID");
                        sLtName = dbReader.GetString("LAST_NAME");
                        sFirstname = dbReader.GetString("FIRST_NAME");
                        lTableID = dbReader.GetInt("ENTITY_TABLE_ID");
                        s1099Parent = dbReader.GetInt("PARENT_1099_EID") == 0 ? "0" : "1";
                        sTableName = dbReader.GetString("TABLE_NAME");
                        int rowIndex = ctlPGridDet.Rows.Add(0, 0, s1099Parent, sLtName, sFirstname, lEntityID, lTableID, sTableName);
                        if (dbReader.IsDBNull("EMPLOYEE_EID"))
                        {
                            this.ctlPGridDet.Rows[rowIndex].Cells[0].Tag = "NO_EMP";
                            this.ctlPGridDet.Rows[rowIndex].Cells[0].ReadOnly = true;
                            this.ctlPGridDet.Rows[rowIndex].Cells[0].Style.BackColor = Color.LightGray;
                        }
                    }
                }

                lblCriteriaDet.Text += "    Results: " + counter;
                ctlPGridDet.Enabled = true;
                ctlPGridDet.ClearSelection();
                sClearFields(); // To clear the fields in detailed tab
                btnEdit.Enabled = false;
                btnProcess.Enabled = false;

                bool success;
                success = Helper.gobjSettings.Show1099ParentColumn == true ? true : false;
                ctlPGridDet.Columns[2].Visible = success;

                Cursor = Cursors.Default;
                frmMain.updateStatus(" ");
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region Get Entities
        private void getEntities()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT STATES.STATE_ID, STATES.STATE_ROW_ID FROM STATES ORDER BY STATE_ID    ");
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        cmbState.Items.Add(new FillCombo(dbReader.GetString("STATE_ID"), dbReader.GetInt("STATE_ROW_ID")));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }


            // SELECT STATES.STATE_ID, STATES.STATE_ROW_ID FROM STATES 
        }

        #endregion

        #region loadSexCodes

        private void loadSexCodes()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT CODE_DESC, CT.CODE_ID FROM GLOSSARY, CODES, CODES_TEXT CT WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'SEX_CODE' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CT.CODE_ID ORDER BY CODE_DESC  ");
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {

                    //while (dbReader.Read())
                    //{
                    //    // cmbSex.Items.Add(new FillCombo(dbReader.GetString("CODE_DESC"), dbReader.GetInt("CODE_ID")));
                    //}
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #endregion

        #region Button Events
        #region btnDisplay_Click Events
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;                // To control that sdisplay function to be called only once on button click. validation at tab events
                Cursor = Cursors.WaitCursor;
                sDisplay();
                Cursor = Cursors.Default;
                if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                {

                    ctlTabs.SelectTab(1);
                }
                else
                {
                    ctlTabs.SelectTab(2);
                }
                buttonClick = false;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region btnSearch_Click
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;         // To control that sdisplay function to be called only once on button click. VAlidation at tab events
                Cursor = Cursors.WaitCursor;
                // bpaskova JIRA 4050 start
                //searchCriteria();
                //Cursor = Cursors.Default;
                //ctlTabs.SelectTab(1);

                Int16 iNextTabId = searchCriteria();
                Cursor = Cursors.Default;
                ctlTabs.SelectTab(iNextTabId);
                // bpaskova JIRA 4050 end
                buttonClick = false;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

        }

        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ctlPGridDet.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for duplicates before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPGridDet.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }
                fSaveEntity();


                Cursor = Cursors.Default;


            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #region btnEdit_Click
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ctlPGridDet.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for duplicates before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPGridDet.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }
                sFieldsType(LockFields.Unlock);


                Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region btnProcess_Click
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;                // To control that sdisplay function to be called only once on button click. validation at tab events
                Cursor = Cursors.WaitCursor;
                Helper.sProcChanges<frmEmployee>(this);
                sClearFields();
                Cursor = Cursors.Default;
                buttonClick = false;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region ctlPGridDet_CellClick
        private void ctlPGridDet_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                sCellClick();
                Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region datetimepicker1_CloseUp
        private void datetimepicker1_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (datetimepicker1.Value > System.DateTime.Now)
                {
                    if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = " ";
                    }
                    else
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = "yyyyMMdd";
                        datetimepicker1.Value = fBirthdate;
                    }
                    MessageBox.Show("Date cannot be greater than current date.", "Entity CleanUp Utility Says :");
                    return;
                }

                if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    datetimepicker1.Format = DateTimePickerFormat.Custom;
                    datetimepicker1.CustomFormat = "yyyyMMdd";
                    fBirthdate = datetimepicker1.Value;
                    datetimepicker1.Value = fBirthdate;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion


        #region IEntityForm Members


        public bool ButtonClick
        {
            get
            {
                return buttonClick;
            }
            set
            {
                buttonClick = value;
            }
        }

        #endregion
    }
}
