﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.Common;
using RMXdBUpgradeWizard;

namespace Entity_Cleanup_Utility
{
    public partial class frmChangeEntity : Form
    {
        /// <summary>
        /// Search Type
        /// </summary>
        private enum eSearchType
        {
            stLName = 0,
            stLNameSpec = 1
        }
        public frmChangeEntity()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #region FormPayee_Load
        private void FormPayee_Load(object sender, EventArgs e)
        {
            try
            {
                #region Defining Events Handler
                tabCET.DrawMode = TabDrawMode.OwnerDrawFixed;
                tabCET.DrawItem += new DrawItemEventHandler(ctlTabs1_DrawItem);
                #endregion

                getEntities();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region Private Methods

        #region ctlTabs_DrawItem for making active index bold
        public void ctlTabs1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == tabCET.SelectedIndex)
                {
                    e.Graphics.DrawString(tabCET.TabPages[e.Index].Text,
                        new Font(tabCET.Font, FontStyle.Bold),
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
                else
                {
                    e.Graphics.DrawString(tabCET.TabPages[e.Index].Text,
                        tabCET.Font,
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region Load Entities
        private void getEntities()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT TABLE_NAME, GT.TABLE_ID FROM GLOSSARY G, GLOSSARY_TEXT GT ");
                gsSql.Append("WHERE G.TABLE_ID = GT.TABLE_ID AND GLOSSARY_TYPE_CODE IN(4,7) ");
                gsSql.Append("AND TABLE_NAME NOT IN('Adjusters','Dependents','Employees','Reported By') ORDER BY TABLE_NAME");

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        cmbEntityFrom.Items.Add(new FillCombo(dbReader.GetString("TABLE_NAME"), dbReader.GetInt("TABLE_ID")));
                        cmbEntityTo.Items.Add(new FillCombo(dbReader.GetString("TABLE_NAME"), dbReader.GetInt("TABLE_ID")));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region GetSelectedRowsEntitiesFromGrid
        private String GetSelectedRowsEntitiesFromGrid()
        {
            
            String SelEntitiesID = string.Empty;
            foreach (DataGridViewRow selRow in ctlPRGrid.SelectedRows)
            {
                SelEntitiesID += (selRow.Cells[2].Value) + ",";


            }
            return SelEntitiesID.Substring(0, (SelEntitiesID.Length) - 1);
        }
        #endregion

        #region sReFillGrid
        private void sReFillGrid()
        {
            try
            {
                int counter = 0;
                StringBuilder gsSql = new StringBuilder();
                FillCombo selectedData = (FillCombo)cmbEntityFrom.SelectedItem;
                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(Helper.gsConnectString);

                ctlPRGrid.Rows.Clear();
                if (Convert.ToString(selectedData.Value) != "")
                {
                    if (DisplayDBUpgrade.g_dbMake == 4)
                    {
                        // For Oracle
                        gsSql.Append("SELECT A.LAST_NAME, A.FIRST_NAME, A.ENTITY_ID, ROWNUM FROM ");
                        gsSql.Append("(SELECT LAST_NAME, FIRST_NAME, ENTITY_ID FROM ENTITY WHERE DELETED_FLAG<>-1 AND ENTITY_TABLE_ID =" + selectedData.Value + " ");
                        gsSql.Append(") A WHERE ROWNUM <= 20000 ORDER BY LAST_NAME");
                    }
                    else
                    {
                        // For Sql Server
                        gsSql.Append("SELECT top 20000 LAST_NAME, FIRST_NAME, ENTITY_ID FROM ENTITY WHERE DELETED_FLAG<>-1 ");
                        gsSql.Append("AND ENTITY_TABLE_ID = " + selectedData.Value + " ");
                        gsSql.Append("ORDER BY LAST_NAME");

                    }

                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            counter++;
                            ctlPRGrid.Rows.Add(dbReader.GetString("FIRST_NAME")
                                 , dbReader.GetString("LAST_NAME")
                                 , dbReader.GetInt("ENTITY_ID"));
                        }
                    }

                    lblCriteria.Visible = true;
                    lblCriteria.Text = "Record Count for " + selectedData.Name + " : " + Convert.ToString(counter);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region CheckForConversionType
        private String CheckForConversionType(double lNewTableId)
        {
            StringBuilder gsSql = new StringBuilder();
            String SYSTEM_TABLE_NAME = string.Empty;
            gsSql.Append("SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = " + lNewTableId);
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
            {
                while (dbReader.Read())
                {
                    SYSTEM_TABLE_NAME = dbReader.GetString("SYSTEM_TABLE_NAME");
                }
            }

            return SYSTEM_TABLE_NAME;

        }
        #endregion

        #region ProcessEntities
        private void sProcessEntities(String SelectedEntites, long lNewTableUId, long lOldTableId)
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("UPDATE ENTITY SET ENTITY_TABLE_ID = " + lNewTableUId + "  WHERE ENTITY_TABLE_ID = " + lOldTableId + " AND ENTITY_ID IN (" + SelectedEntites + ")");
                int results = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                //'Ankur MITS 12006 6/9/2008
                //'When the utility is run against a Physician, their entity type code is changed to whatever
                //'corresponds to the type they are being changed to. However, Their entry in the Physician table
                //'is not removed causing the person to continue to show up as a physician in searches in riskmaster.

                if (lOldTableId == 1086)   //  'Physician table
                {
                    gsSql.Clear();
                    gsSql.Append("DELETE FROM PHYSICIAN WHERE PHYS_EID IN (" + SelectedEntites + ")");
                    int flag = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());
                }

                //Call for grid
                sReFillGrid();
                //  'ngupta20 MITS 7486 - Changes end
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region button Events
        #region  btnProcess_Click
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                frmMain.updateStatus(" Processing the records.....");
                long lOldTableID;
                long lNewTableId;
                string sLtName = string.Empty;
                string sFirstname = string.Empty;
                string sTempSql = string.Empty;         //    'ngupta20 12/08/2006 MITS 7486
                string sTableName = string.Empty;

                if (cmbEntityTo.SelectedItem == null)
                {
                    MessageBox.Show("You must select an entity type to change to.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPRGrid.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected any entities to process.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (cmbEntityTo.SelectedItem == null && cmbEntityFrom.SelectedItem == null && ctlPRGrid.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected required fields.", "Entity CleanUp Utility Says :");
                    return;
                }
                else
                {
                    FillCombo EntityFrom = (FillCombo)cmbEntityFrom.SelectedItem;
                    FillCombo EntityTo = (FillCombo)cmbEntityTo.SelectedItem;

                    lOldTableID = EntityFrom.Value;
                    lNewTableId = EntityTo.Value;

                    //   'ngupta20 12/08/2006 MITS 7486
                    //  'Check for conversion to Physician type - prompt for creating a record in RMWorld and then merge using ECU

                    sTableName = CheckForConversionType(lNewTableId);      // cases like       Case "PHYSICIANS,MEDICAL_STAFF,PATIENTS

                    if (sTableName == "PHYSICIANS")
                    {
                        MessageBox.Show("The Entity change to Physician type is not allowed using ECU Change Entity Type. Please create a Physician entity using RMWorld Physician Maintenance and then use ECU to merge the Duplicate Records.", "Entity CleanUp Utility Says :");
                        return;
                    }
                    else if (sTableName == "MEDICAL_STAFF")
                    {
                        MessageBox.Show("The Entity change to Medical Staff type is not allowed using ECU Change Entity Type. Please create a Medical Staff entity using RMWorld Staff Maintenance and then use ECU to merge the Duplicate Records.", "Entity CleanUp Utility Says :");
                        return;
                    }
                    else if (sTableName == "PATIENTS")
                    {
                        MessageBox.Show("The Entity change to Patient type is not allowed using ECU Change Entity Type. Please create a Patient entity using RMWorld People Maintenance and then use ECU to merge the Duplicate Records.", "Entity CleanUp Utility Says :");
                        return;
                    }
                    else
                    {
                        string selectedEntites = GetSelectedRowsEntitiesFromGrid();

                        if (selectedEntites != string.Empty)
                        {
                            sProcessEntities(selectedEntites, lNewTableId, lOldTableID);
                        }
                    }
                }
                frmMain.updateStatus(" ");
                // bpaskova JIRA 4407
                MessageBox.Show("Records processed.", "Entity CleanUp Utility Says :");

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region Dropdown Events

        #region cmbEntityFrom_SelectedIndexChanged
        private void cmbEntityFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                frmMain.updateStatus("Refreshing the grid.....");
                cmbEntityTo.SelectedItem = null;
                sReFillGrid();
                frmMain.updateStatus(" ");
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region Gridview Events

        #region ctlPRGrid_CellClick
        private void ctlPRGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                lblRowSel.Text = "Row Selected : ";
                lblRowSel.Text += Convert.ToString(ctlPRGrid.SelectedRows.Count);
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion
        #endregion


    }
}
