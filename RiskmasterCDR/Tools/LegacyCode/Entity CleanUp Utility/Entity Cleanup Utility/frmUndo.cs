﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.Common;
using RMXdBUpgradeWizard;

namespace Entity_Cleanup_Utility
{
    public partial class frmUndo : Form
    {
        public frmUndo()
        {

            InitializeComponent();
            fFillHistGrid(Convert.ToInt32(eSearchType.on), afterdtpicker.Value.ToString("yyyyMMdd"));
        }


        private enum eSearchType
        {
            before = 1,
            on = 2,
            after = 3
        }

        #region Private Methods

        #region fFillHistGrid
        private void fFillHistGrid(int Options, string date)
        {
            string gsStr = string.Empty;
            StringBuilder gsSql = new StringBuilder();
            string LastKept = string.Empty;
            string FirstKept = string.Empty;
            string TypeKept = string.Empty;
            string LastChng = string.Empty;
            string TypeChng = string.Empty;

            double lEntKeepEID;
            double lEntChangeEID;
            double lEntRowEID;
            try
            {


                Cursor = Cursors.WaitCursor;
                grdDisplay.Rows.Clear();

                if (Options == Convert.ToInt32(eSearchType.before))
                {
                    gsSql.Append("SELECT E.LAST_NAME, E.FIRST_NAME, E2.LAST_NAME CHNAME, GT.TABLE_NAME, EID_KEPT, ECU_ROW_ID, EID_CHANGED, GT2.TABLE_NAME CHTAB ");
                    gsSql.Append("FROM ENTITY E, ECU_HISTORY ECUH, GLOSSARY_TEXT GT, ENTITY E2, GLOSSARY_TEXT GT2 ");
                    gsSql.Append("WHERE ECUH.EID_KEPT = E.ENTITY_ID AND E.ENTITY_TABLE_ID = GT.TABLE_ID AND E2.ENTITY_ID = EID_CHANGED AND GT2.TABLE_ID = E2.ENTITY_TABLE_ID ");
                    gsSql.AppendFormat("AND (EID_KEPT_ET <> 1060 AND EID_CHANGED_ET <> 1060) AND SUBSTRING(ECUH.DTTM_RCD_ADDED,1,8) < {0} ORDER BY E.LAST_NAME, E.FIRST_NAME", date);
                }
                else if (Options == Convert.ToInt32(eSearchType.on))
                {
                    gsSql.Append("SELECT E.LAST_NAME, E.FIRST_NAME, E2.LAST_NAME CHNAME, GT.TABLE_NAME, EID_KEPT, ECU_ROW_ID, EID_CHANGED, GT2.TABLE_NAME CHTAB ");
                    gsSql.Append("FROM ENTITY E, ECU_HISTORY ECUH, GLOSSARY_TEXT GT, ENTITY E2, GLOSSARY_TEXT GT2 ");
                    gsSql.Append("WHERE ECUH.EID_KEPT = E.ENTITY_ID AND E.ENTITY_TABLE_ID = GT.TABLE_ID AND E2.ENTITY_ID = EID_CHANGED AND GT2.TABLE_ID = E2.ENTITY_TABLE_ID ");
                    gsSql.AppendFormat("AND (EID_KEPT_ET <> 1060 AND EID_CHANGED_ET <> 1060) AND SUBSTRING(ECUH.DTTM_RCD_ADDED,1,8) = {0} ORDER BY E.LAST_NAME, E.FIRST_NAME", date);
                }
                else if (Options == Convert.ToInt32(eSearchType.after))
                {
                    gsSql.Append("SELECT E.LAST_NAME, E.FIRST_NAME, E2.LAST_NAME CHNAME, GT.TABLE_NAME, EID_KEPT, ECU_ROW_ID, EID_CHANGED, GT2.TABLE_NAME CHTAB ");
                    gsSql.Append("FROM ENTITY E, ECU_HISTORY ECUH, GLOSSARY_TEXT GT, ENTITY E2, GLOSSARY_TEXT GT2 ");
                    gsSql.Append("WHERE ECUH.EID_KEPT = E.ENTITY_ID AND E.ENTITY_TABLE_ID = GT.TABLE_ID AND E2.ENTITY_ID = EID_CHANGED AND GT2.TABLE_ID = E2.ENTITY_TABLE_ID ");
                    gsSql.AppendFormat("AND (EID_KEPT_ET <> 1060 AND EID_CHANGED_ET <> 1060) AND SUBSTRING(ECUH.DTTM_RCD_ADDED,1,8) > {0} ORDER BY E.LAST_NAME, E.FIRST_NAME", date);
                }

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        LastKept = dbReader.GetString("LAST_NAME");
                        FirstKept = dbReader.GetString("FIRST_NAME");
                        TypeKept = dbReader.GetString("TABLE_NAME");
                        LastChng = dbReader.GetString("CHNAME");
                        TypeChng = dbReader.GetString("CHTAB");

                        lEntKeepEID = Convert.ToDouble(dbReader.GetValue("EID_KEPT"));
                        lEntChangeEID = Convert.ToDouble(dbReader.GetValue("EID_CHANGED"));
                        lEntRowEID = Convert.ToDouble(dbReader.GetValue("ECU_ROW_ID"));


                        grdDisplay.Rows.Add(LastKept, FirstKept, TypeKept, LastChng, TypeChng, lEntKeepEID, lEntChangeEID, lEntRowEID);

                    }
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }


        }

        #endregion

        #region sDisplay
        private void sDisplay()
        {
            int Upresults;
            string sTableName = string.Empty;
            string sFieldName = string.Empty; ;
            double lTabID;
            double lEntKeepEID;
            double lEntChangeEID;
            double lECU_ROWID;
            string sKeyFieldName = string.Empty;
            StringBuilder gsSql = new StringBuilder();

            lEntKeepEID = Convert.ToDouble(grdDisplay.Rows[grdDisplay.CurrentRow.Index].Cells["EID_KEPT"].Value);//            
            lEntChangeEID = Convert.ToDouble(grdDisplay.Rows[grdDisplay.CurrentRow.Index].Cells["EID_CHANGED"].Value);//      //      
            lECU_ROWID = Convert.ToDouble(grdDisplay.Rows[grdDisplay.CurrentRow.Index].Cells["ECU_ROW_ID"].Value);//  

            gsSql.AppendFormat("SELECT * FROM  ECU_HISTORY_DETAIL WHERE ECU_ROW_ID = {0}", lECU_ROWID);
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
            {
                if (dbReader.Read())
                {
                    lTabID = Convert.ToDouble(dbReader.GetValue("TABLE_ID"));
                    sTableName = GetTableName(lTabID);
                    sFieldName = Convert.ToString(dbReader.GetValue("FIELD_NAME"));
                    sKeyFieldName = GetKeyFieldName(sTableName);

                    StringBuilder gsUp = new StringBuilder();
                    gsUp.AppendFormat("UPDATE {0} SET {1} = {2} WHERE {3} = {4}", sTableName, sFieldName, lEntChangeEID, sKeyFieldName, lECU_ROWID);

                }
            }
            gsSql.Clear();
            //'Set deleted flag in ENTITY table

            gsSql.AppendFormat("UPDATE ENTITY SET DELETED_FLAG = 0 WHERE ENTITY_ID = {0}", lEntChangeEID);
            Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

            gsSql.Clear();
            // 'Delete records from history tables
            gsSql.AppendFormat("DELETE FROM ECU_HISTORY WHERE ECU_ROW_ID = {0}", lECU_ROWID);
            Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());


            gsSql.Clear();
            // 'Delete records from history tables
            gsSql.AppendFormat("DELETE FROM ECU_HISTORY_DETAIL WHERE ECU_ROW_ID = {0}", lECU_ROWID);
            Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

            if (grdDisplay.Rows.Count == 1)
            {
                grdDisplay.Rows.Clear();
            }
            else
            {
                int selectedIndex = grdDisplay.CurrentCell.RowIndex;
                grdDisplay.Rows.RemoveAt(selectedIndex);
                grdDisplay.Refresh();
            }



        }
        #endregion


        #region sPurge
        private void sPurge()
        {
            if (rbPurgeAll.Checked == false && rbHistoryDate.Checked == false)
            {
                MessageBox.Show("You have not Selected any option to purge history", "Entity Cleanup Says:");
                return;
            }

            int Upresults;
            StringBuilder gsSql = new StringBuilder();
            if (rbPurgeAll.Checked == true)
            {
                gsSql.AppendFormat("DELETE FROM ECU_HISTORY");
                Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                gsSql.Clear();

                gsSql.AppendFormat("DELETE FROM ECU_HISTORY_DETAIL");
                Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                MessageBox.Show("Purged ALL historical Entity Clean Up transactions.", "Entity Cleanup Says:");


            }
            else if (rbHistoryDate.Checked == true)
            {
                gsSql.Clear();
                gsSql.AppendFormat("DELETE FROM ECU_HISTORY WHERE SUBSTRING(DTTM_RCD_ADDED,1,8) < {0} ", dtpickerPurge.Value.ToString("yyyyMMdd"));
                Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                gsSql.Clear();
                gsSql.AppendFormat("DELETE FROM ECU_HISTORY_DETAIL WHERE NOT EXISTS (SELECT ECU_ROW_ID FROM ECU_HISTORY WHERE ECU_HISTORY_DETAIL.ECU_ROW_ID = ECU_HISTORY.ECU_ROW_ID)");
                Upresults = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());

                rbBefore.Checked = false;
                rbAfter.Checked = false;
                rbOn.Checked = false;
                grdDisplay.Rows.Clear();
                grdDisplay.Refresh();

                MessageBox.Show("Purged historical Entity Clean Up transactions that occured prior to ( " + dtpickerPurge.Value.ToString("yyyyMMdd") + " ).", "Entity Cleanup Says:");
            }
        }
        #endregion

        #region GetTableName
        private string GetTableName(double TableID)
        {

            string sTableName = string.Empty;
            StringBuilder gsSql = new StringBuilder();
            gsSql.AppendFormat("SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = {0}", TableID);
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
            {
                if (dbReader.Read())
                {
                    sTableName = Convert.ToString(dbReader.GetValue("SYSTEM_TABLE_NAME"));
                }
            }
            return sTableName;

        }
        #endregion

        #region GetKeyFieldName
        private string GetKeyFieldName(String TableName)
        {

            string sKeyFieldName = string.Empty;
            StringBuilder gsSql = new StringBuilder();
            gsSql.AppendFormat("SELECT KEY_FIELD_NAME FROM ECU_TABLE_CHECK WHERE TABLE_NAME = '{0}'", TableName);
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
            {
                if (dbReader.Read())
                {
                    sKeyFieldName = Convert.ToString(dbReader.GetValue("KEY_FIELD_NAME"));
                }
            }
            return sKeyFieldName;

        }
        #endregion


        #endregion

        #region Buttons Events

        #region btnUndo_Click Events
        private void btnUndo_Click(object sender, EventArgs e)
        {
            try
            {

                if (grdDisplay.Rows.Count == 0)
                {
                    MessageBox.Show("No rows in the grid to process.", "Entity CleanUp Utility Says :");
                    return;
                }

                if (grdDisplay.SelectedRows.Count < 1)
                {
                    MessageBox.Show("You have not selected a row in the grid to process.", "Entity CleanUp Utility Says :");
                    return;
                }

                Cursor = Cursors.WaitCursor;
                sDisplay();
                Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region btnPurge_Click Events
        private void btnPurge_Click(object sender, EventArgs e)
        {
            try
            {

                Cursor = Cursors.WaitCursor;

                sPurge();

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion


        #endregion

        #region RadioButton Events

        private void rbBefore_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBefore.Checked == true)
            {
                fFillHistGrid(Convert.ToInt32(eSearchType.before), afterdtpicker.Value.ToString("yyyyMMdd"));
            }
        }

        private void rbOn_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOn.Checked == true)
            {
                fFillHistGrid(Convert.ToInt32(eSearchType.on), afterdtpicker.Value.ToString("yyyyMMdd"));
            }
        }

        private void rbAfter_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAfter.Checked == true)
            {
                fFillHistGrid(Convert.ToInt32(eSearchType.after), afterdtpicker.Value.ToString("yyyyMMdd"));
            }
        }

        private void rbHistoryDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHistoryDate.Checked == true)
            {
                dtpickerPurge.Enabled = true;
            }
            else
            {
                dtpickerPurge.Enabled = false;

            }
        }

        private void rbPurgeAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPurgeAll.Checked == true)
            {
                dtpickerPurge.Enabled = false;
            }
            else
            {
                dtpickerPurge.Enabled = true;

            }

        }

        #endregion




    }
}
