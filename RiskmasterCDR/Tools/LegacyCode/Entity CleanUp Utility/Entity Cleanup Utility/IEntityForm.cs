﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Entity_Cleanup_Utility
{
    interface IEntityForm
    {
        /// <summary>
        /// Gets the control pr grid.
        /// </summary>
        /// <value>
        /// The control pr grid.
        /// </value>
        DataGridView CtlPRGrid{get;}

        /// <summary>
        /// Gets the control p grid det.
        /// </summary>
        /// <value>
        /// The control p grid det.
        /// </value>
        DataGridView CtlPGridDet { get; }

        /// <summary>
        /// Gets the control tabs.
        /// </summary>
        /// <value>
        /// The control tabs.
        /// </value>
        TabControl CtlTabs{get;}

        /// <summary>
        /// Gets or sets the button click.
        /// </summary>
        /// <value>
        /// The button click.
        /// </value>
        bool ButtonClick { get; set; }
    }
}
