﻿using System.Windows.Forms;
namespace Entity_Cleanup_Utility
{
    partial class FormPayee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPayee = new System.Windows.Forms.Label();
            this.ctlTabs = new System.Windows.Forms.TabControl();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLastN = new System.Windows.Forms.TextBox();
            this.rbPayeeLastNameSpecific = new System.Windows.Forms.RadioButton();
            this.rbPayeeLastName = new System.Windows.Forms.RadioButton();
            this.gbPayeeType = new System.Windows.Forms.GroupBox();
            this.rbClaimants = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.ctlPRGrid = new System.Windows.Forms.DataGridView();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayeeLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoofDuplicates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClaimantEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayeeEID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.lblcriteriaDet = new System.Windows.Forms.Label();
            this.comboStZip = new System.Windows.Forms.ComboBox();
            this.lblStateZip = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtBirthDate = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailCity = new System.Windows.Forms.Label();
            this.lblPayeeDetailEMail = new System.Windows.Forms.Label();
            this.lblPayeeDetailTaxId = new System.Windows.Forms.Label();
            this.lblPayeeDetailEntityType = new System.Windows.Forms.Label();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cmbET = new System.Windows.Forms.ComboBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtAddr2 = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailAddress = new System.Windows.Forms.Label();
            this.txtAddr1 = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtHomePhn = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailPhone = new System.Windows.Forms.Label();
            this.txtOffPhn = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailAbbr = new System.Windows.Forms.Label();
            this.txtAbbr = new System.Windows.Forms.TextBox();
            this.ctlPGridDet = new System.Windows.Forms.DataGridView();
            this.Keep = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Del = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.S1099 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PDLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEID = new System.Windows.Forms.TextBox();
            this.txtStateID = new System.Windows.Forms.TextBox();
            this.txtSexID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ctlTabs.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbPayeeType.SuspendLayout();
            this.tabResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).BeginInit();
            this.tabDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(478, 406);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblPayee
            // 
            this.lblPayee.AutoSize = true;
            this.lblPayee.BackColor = System.Drawing.SystemColors.Control;
            this.lblPayee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPayee.ForeColor = System.Drawing.Color.Maroon;
            this.lblPayee.Location = new System.Drawing.Point(26, 16);
            this.lblPayee.Name = "lblPayee";
            this.lblPayee.Size = new System.Drawing.Size(124, 17);
            this.lblPayee.TabIndex = 1;
            this.lblPayee.Text = "Payee Clean Up";
            // 
            // ctlTabs
            // 
            this.ctlTabs.Controls.Add(this.tabSearch);
            this.ctlTabs.Controls.Add(this.tabResults);
            this.ctlTabs.Controls.Add(this.tabDetail);
            this.ctlTabs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctlTabs.Location = new System.Drawing.Point(24, 39);
            this.ctlTabs.Name = "ctlTabs";
            this.ctlTabs.SelectedIndex = 0;
            this.ctlTabs.Size = new System.Drawing.Size(467, 369);
            this.ctlTabs.TabIndex = 2;
            this.ctlTabs.Selected += new System.Windows.Forms.TabControlEventHandler(this.ctlTabs_Selected);
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.label3);
            this.tabSearch.Controls.Add(this.groupBox1);
            this.tabSearch.Controls.Add(this.gbPayeeType);
            this.tabSearch.Controls.Add(this.btnSearch);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(459, 343);
            this.tabSearch.TabIndex = 0;
            this.tabSearch.Text = "1. Search";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLastN);
            this.groupBox1.Controls.Add(this.rbPayeeLastNameSpecific);
            this.groupBox1.Controls.Add(this.rbPayeeLastName);
            this.groupBox1.Location = new System.Drawing.Point(7, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 67);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for duplicates using";
            // 
            // txtLastN
            // 
            this.txtLastN.Location = new System.Drawing.Point(278, 20);
            this.txtLastN.Name = "txtLastN";
            this.txtLastN.Size = new System.Drawing.Size(151, 21);
            this.txtLastN.TabIndex = 2;
            this.txtLastN.Visible = false;
            // 
            // rbPayeeLastNameSpecific
            // 
            this.rbPayeeLastNameSpecific.AutoSize = true;
            this.rbPayeeLastNameSpecific.Location = new System.Drawing.Point(118, 20);
            this.rbPayeeLastNameSpecific.Name = "rbPayeeLastNameSpecific";
            this.rbPayeeLastNameSpecific.Size = new System.Drawing.Size(143, 17);
            this.rbPayeeLastNameSpecific.TabIndex = 1;
            this.rbPayeeLastNameSpecific.TabStop = true;
            this.rbPayeeLastNameSpecific.Text = "Last Name (Specific)";
            this.rbPayeeLastNameSpecific.UseVisualStyleBackColor = true;
            this.rbPayeeLastNameSpecific.CheckedChanged += new System.EventHandler(this.rbPayeeLastNameSpecific_CheckedChanged);
            // 
            // rbPayeeLastName
            // 
            this.rbPayeeLastName.AutoSize = true;
            this.rbPayeeLastName.Checked = true;
            this.rbPayeeLastName.Location = new System.Drawing.Point(7, 20);
            this.rbPayeeLastName.Name = "rbPayeeLastName";
            this.rbPayeeLastName.Size = new System.Drawing.Size(85, 17);
            this.rbPayeeLastName.TabIndex = 0;
            this.rbPayeeLastName.TabStop = true;
            this.rbPayeeLastName.Text = "Last Name";
            this.rbPayeeLastName.UseVisualStyleBackColor = true;
            // 
            // gbPayeeType
            // 
            this.gbPayeeType.Controls.Add(this.rbClaimants);
            this.gbPayeeType.Location = new System.Drawing.Point(7, 7);
            this.gbPayeeType.Name = "gbPayeeType";
            this.gbPayeeType.Size = new System.Drawing.Size(445, 50);
            this.gbPayeeType.TabIndex = 1;
            this.gbPayeeType.TabStop = false;
            this.gbPayeeType.Text = "Payee Type";
            // 
            // rbClaimants
            // 
            this.rbClaimants.AutoSize = true;
            this.rbClaimants.Checked = true;
            this.rbClaimants.Location = new System.Drawing.Point(7, 20);
            this.rbClaimants.Name = "rbClaimants";
            this.rbClaimants.Size = new System.Drawing.Size(82, 17);
            this.rbClaimants.TabIndex = 0;
            this.rbClaimants.TabStop = true;
            this.rbClaimants.Text = "Claimants";
            this.rbClaimants.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(361, 315);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.lblCriteria);
            this.tabResults.Controls.Add(this.btnDisplay);
            this.tabResults.Controls.Add(this.ctlPRGrid);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(459, 343);
            this.tabResults.TabIndex = 1;
            this.tabResults.Text = "2. Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteria.Location = new System.Drawing.Point(4, 8);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(50, 13);
            this.lblCriteria.TabIndex = 2;
            this.lblCriteria.Text = "Criteria";
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(367, 315);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnDisplay.TabIndex = 1;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // ctlPRGrid
            // 
            this.ctlPRGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPRGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ctlPRGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.ctlPRGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.ctlPRGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPRGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LastName,
            this.FirstName,
            this.PayeeLastName,
            this.NoofDuplicates,
            this.ClaimantEID,
            this.PayeeEID});
            this.ctlPRGrid.Location = new System.Drawing.Point(6, 26);
            this.ctlPRGrid.MultiSelect = false;
            this.ctlPRGrid.Name = "ctlPRGrid";
            this.ctlPRGrid.Size = new System.Drawing.Size(447, 261);
            this.ctlPRGrid.TabIndex = 0;
            this.ctlPRGrid.Click += new System.EventHandler(this.ctlPRGrid_Click);
            // 
            // LastName
            // 
            this.LastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LastName.DefaultCellStyle = dataGridViewCellStyle2;
            this.LastName.HeaderText = "Claimant Last Name";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            this.LastName.Width = 105;
            // 
            // FirstName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FirstName.DefaultCellStyle = dataGridViewCellStyle3;
            this.FirstName.HeaderText = "Claimant First Name";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            this.FirstName.Width = 105;
            // 
            // PayeeLastName
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PayeeLastName.DefaultCellStyle = dataGridViewCellStyle4;
            this.PayeeLastName.HeaderText = "Payee Last Name";
            this.PayeeLastName.Name = "PayeeLastName";
            this.PayeeLastName.ReadOnly = true;
            this.PayeeLastName.Width = 120;
            // 
            // NoofDuplicates
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NoofDuplicates.DefaultCellStyle = dataGridViewCellStyle5;
            this.NoofDuplicates.HeaderText = "# Trans";
            this.NoofDuplicates.Name = "NoofDuplicates";
            this.NoofDuplicates.ReadOnly = true;
            this.NoofDuplicates.Width = 71;
            // 
            // ClaimantEID
            // 
            this.ClaimantEID.HeaderText = "Claimant EID";
            this.ClaimantEID.Name = "ClaimantEID";
            this.ClaimantEID.ReadOnly = true;
            this.ClaimantEID.Visible = false;
            // 
            // PayeeEID
            // 
            this.PayeeEID.HeaderText = "Payee EID";
            this.PayeeEID.Name = "PayeeEID";
            this.PayeeEID.ReadOnly = true;
            this.PayeeEID.Visible = false;
            // 
            // tabDetail
            // 
            this.tabDetail.BackColor = System.Drawing.Color.LightGray;
            this.tabDetail.Controls.Add(this.lblcriteriaDet);
            this.tabDetail.Controls.Add(this.comboStZip);
            this.tabDetail.Controls.Add(this.lblStateZip);
            this.tabDetail.Controls.Add(this.label2);
            this.tabDetail.Controls.Add(this.label1);
            this.tabDetail.Controls.Add(this.txtAge);
            this.tabDetail.Controls.Add(this.txtBirthDate);
            this.tabDetail.Controls.Add(this.lblPayeeDetailCity);
            this.tabDetail.Controls.Add(this.lblPayeeDetailEMail);
            this.tabDetail.Controls.Add(this.lblPayeeDetailTaxId);
            this.tabDetail.Controls.Add(this.lblPayeeDetailEntityType);
            this.tabDetail.Controls.Add(this.btnProcess);
            this.tabDetail.Controls.Add(this.cmbET);
            this.tabDetail.Controls.Add(this.txtEmail);
            this.tabDetail.Controls.Add(this.txtTaxID);
            this.tabDetail.Controls.Add(this.txtZip);
            this.tabDetail.Controls.Add(this.txtCity);
            this.tabDetail.Controls.Add(this.txtAddr2);
            this.tabDetail.Controls.Add(this.lblPayeeDetailAddress);
            this.tabDetail.Controls.Add(this.txtAddr1);
            this.tabDetail.Controls.Add(this.txtFirstName);
            this.tabDetail.Controls.Add(this.lblPayeeDetailName);
            this.tabDetail.Controls.Add(this.txtLastName);
            this.tabDetail.Controls.Add(this.txtHomePhn);
            this.tabDetail.Controls.Add(this.lblPayeeDetailPhone);
            this.tabDetail.Controls.Add(this.txtOffPhn);
            this.tabDetail.Controls.Add(this.lblPayeeDetailAbbr);
            this.tabDetail.Controls.Add(this.txtAbbr);
            this.tabDetail.Controls.Add(this.ctlPGridDet);
            this.tabDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDetail.Location = new System.Drawing.Point(4, 22);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Size = new System.Drawing.Size(459, 343);
            this.tabDetail.TabIndex = 2;
            this.tabDetail.Text = "3. Detail";
            // 
            // lblcriteriaDet
            // 
            this.lblcriteriaDet.AutoSize = true;
            this.lblcriteriaDet.ForeColor = System.Drawing.Color.Maroon;
            this.lblcriteriaDet.Location = new System.Drawing.Point(8, 3);
            this.lblcriteriaDet.Name = "lblcriteriaDet";
            this.lblcriteriaDet.Size = new System.Drawing.Size(39, 13);
            this.lblcriteriaDet.TabIndex = 34;
            this.lblcriteriaDet.Text = "Criteria";
            // 
            // comboStZip
            // 
            this.comboStZip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.comboStZip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStZip.Enabled = false;
            this.comboStZip.FormattingEnabled = true;
            this.comboStZip.Location = new System.Drawing.Point(51, 278);
            this.comboStZip.Name = "comboStZip";
            this.comboStZip.Size = new System.Drawing.Size(40, 21);
            this.comboStZip.TabIndex = 33;
            // 
            // lblStateZip
            // 
            this.lblStateZip.AutoSize = true;
            this.lblStateZip.Location = new System.Drawing.Point(10, 286);
            this.lblStateZip.Name = "lblStateZip";
            this.lblStateZip.Size = new System.Drawing.Size(38, 13);
            this.lblStateZip.TabIndex = 32;
            this.lblStateZip.Text = "St,Zip:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(354, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Age :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Birth Date :";
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAge.Enabled = false;
            this.txtAge.Location = new System.Drawing.Point(387, 277);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(43, 20);
            this.txtAge.TabIndex = 29;
            // 
            // txtBirthDate
            // 
            this.txtBirthDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtBirthDate.Enabled = false;
            this.txtBirthDate.Location = new System.Drawing.Point(283, 278);
            this.txtBirthDate.Name = "txtBirthDate";
            this.txtBirthDate.Size = new System.Drawing.Size(71, 20);
            this.txtBirthDate.TabIndex = 28;
            // 
            // lblPayeeDetailCity
            // 
            this.lblPayeeDetailCity.AutoSize = true;
            this.lblPayeeDetailCity.Location = new System.Drawing.Point(21, 262);
            this.lblPayeeDetailCity.Name = "lblPayeeDetailCity";
            this.lblPayeeDetailCity.Size = new System.Drawing.Size(27, 13);
            this.lblPayeeDetailCity.TabIndex = 27;
            this.lblPayeeDetailCity.Text = "City:";
            // 
            // lblPayeeDetailEMail
            // 
            this.lblPayeeDetailEMail.AutoSize = true;
            this.lblPayeeDetailEMail.Location = new System.Drawing.Point(238, 258);
            this.lblPayeeDetailEMail.Name = "lblPayeeDetailEMail";
            this.lblPayeeDetailEMail.Size = new System.Drawing.Size(38, 13);
            this.lblPayeeDetailEMail.TabIndex = 26;
            this.lblPayeeDetailEMail.Text = "Email :";
            // 
            // lblPayeeDetailTaxId
            // 
            this.lblPayeeDetailTaxId.AutoSize = true;
            this.lblPayeeDetailTaxId.Location = new System.Drawing.Point(207, 191);
            this.lblPayeeDetailTaxId.Name = "lblPayeeDetailTaxId";
            this.lblPayeeDetailTaxId.Size = new System.Drawing.Size(45, 13);
            this.lblPayeeDetailTaxId.TabIndex = 25;
            this.lblPayeeDetailTaxId.Text = "Tax ID :";
            // 
            // lblPayeeDetailEntityType
            // 
            this.lblPayeeDetailEntityType.AutoSize = true;
            this.lblPayeeDetailEntityType.Location = new System.Drawing.Point(207, 170);
            this.lblPayeeDetailEntityType.Name = "lblPayeeDetailEntityType";
            this.lblPayeeDetailEntityType.Size = new System.Drawing.Size(66, 13);
            this.lblPayeeDetailEntityType.TabIndex = 24;
            this.lblPayeeDetailEntityType.Text = "Entity Type :";
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(357, 313);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 22;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmbET
            // 
            this.cmbET.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbET.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbET.Enabled = false;
            this.cmbET.FormattingEnabled = true;
            this.cmbET.Location = new System.Drawing.Point(283, 166);
            this.cmbET.Name = "cmbET";
            this.cmbET.Size = new System.Drawing.Size(149, 21);
            this.cmbET.TabIndex = 20;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(282, 254);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(148, 20);
            this.txtEmail.TabIndex = 18;
            // 
            // txtTaxID
            // 
            this.txtTaxID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTaxID.Enabled = false;
            this.txtTaxID.Location = new System.Drawing.Point(283, 187);
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.Size = new System.Drawing.Size(148, 20);
            this.txtTaxID.TabIndex = 17;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtZip.Enabled = false;
            this.txtZip.Location = new System.Drawing.Point(97, 280);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(102, 20);
            this.txtZip.TabIndex = 14;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(51, 257);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(148, 20);
            this.txtCity.TabIndex = 12;
            // 
            // txtAddr2
            // 
            this.txtAddr2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr2.Enabled = false;
            this.txtAddr2.Location = new System.Drawing.Point(52, 233);
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.Size = new System.Drawing.Size(301, 20);
            this.txtAddr2.TabIndex = 11;
            // 
            // lblPayeeDetailAddress
            // 
            this.lblPayeeDetailAddress.AutoSize = true;
            this.lblPayeeDetailAddress.Location = new System.Drawing.Point(4, 214);
            this.lblPayeeDetailAddress.Name = "lblPayeeDetailAddress";
            this.lblPayeeDetailAddress.Size = new System.Drawing.Size(48, 13);
            this.lblPayeeDetailAddress.TabIndex = 10;
            this.lblPayeeDetailAddress.Text = "Address:";
            // 
            // txtAddr1
            // 
            this.txtAddr1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr1.Enabled = false;
            this.txtAddr1.Location = new System.Drawing.Point(53, 210);
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.Size = new System.Drawing.Size(301, 20);
            this.txtAddr1.TabIndex = 9;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Location = new System.Drawing.Point(53, 188);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(148, 20);
            this.txtFirstName.TabIndex = 8;
            // 
            // lblPayeeDetailName
            // 
            this.lblPayeeDetailName.AutoSize = true;
            this.lblPayeeDetailName.Location = new System.Drawing.Point(10, 170);
            this.lblPayeeDetailName.Name = "lblPayeeDetailName";
            this.lblPayeeDetailName.Size = new System.Drawing.Size(38, 13);
            this.lblPayeeDetailName.TabIndex = 7;
            this.lblPayeeDetailName.Text = "Name:";
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtLastName.Enabled = false;
            this.txtLastName.Location = new System.Drawing.Point(53, 166);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(148, 20);
            this.txtLastName.TabIndex = 6;
            // 
            // txtHomePhn
            // 
            this.txtHomePhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtHomePhn.Enabled = false;
            this.txtHomePhn.Location = new System.Drawing.Point(360, 145);
            this.txtHomePhn.Name = "txtHomePhn";
            this.txtHomePhn.Size = new System.Drawing.Size(72, 20);
            this.txtHomePhn.TabIndex = 5;
            // 
            // lblPayeeDetailPhone
            // 
            this.lblPayeeDetailPhone.AutoSize = true;
            this.lblPayeeDetailPhone.Location = new System.Drawing.Point(207, 149);
            this.lblPayeeDetailPhone.Name = "lblPayeeDetailPhone";
            this.lblPayeeDetailPhone.Size = new System.Drawing.Size(73, 13);
            this.lblPayeeDetailPhone.TabIndex = 4;
            this.lblPayeeDetailPhone.Text = "Ph Off/Home:";
            // 
            // txtOffPhn
            // 
            this.txtOffPhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtOffPhn.Enabled = false;
            this.txtOffPhn.Location = new System.Drawing.Point(282, 145);
            this.txtOffPhn.Name = "txtOffPhn";
            this.txtOffPhn.Size = new System.Drawing.Size(72, 20);
            this.txtOffPhn.TabIndex = 3;
            // 
            // lblPayeeDetailAbbr
            // 
            this.lblPayeeDetailAbbr.AutoSize = true;
            this.lblPayeeDetailAbbr.Location = new System.Drawing.Point(12, 148);
            this.lblPayeeDetailAbbr.Name = "lblPayeeDetailAbbr";
            this.lblPayeeDetailAbbr.Size = new System.Drawing.Size(32, 13);
            this.lblPayeeDetailAbbr.TabIndex = 2;
            this.lblPayeeDetailAbbr.Text = "Abbr:";
            // 
            // txtAbbr
            // 
            this.txtAbbr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAbbr.Enabled = false;
            this.txtAbbr.Location = new System.Drawing.Point(53, 144);
            this.txtAbbr.Name = "txtAbbr";
            this.txtAbbr.Size = new System.Drawing.Size(148, 20);
            this.txtAbbr.TabIndex = 1;
            // 
            // ctlPGridDet
            // 
            this.ctlPGridDet.AllowUserToAddRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPGridDet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.ctlPGridDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPGridDet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Keep,
            this.Del,
            this.S1099,
            this.PDLastName,
            this.PDFirstName,
            this.PDEntityID,
            this.PDEntityType,
            this.PDEntType});
            this.ctlPGridDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ctlPGridDet.Location = new System.Drawing.Point(9, 22);
            this.ctlPGridDet.MultiSelect = false;
            this.ctlPGridDet.Name = "ctlPGridDet";
            this.ctlPGridDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ctlPGridDet.Size = new System.Drawing.Size(433, 117);
            this.ctlPGridDet.TabIndex = 0;
            this.ctlPGridDet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlPGridDet_CellClick);
            // 
            // Keep
            // 
            this.Keep.FalseValue = "0";
            this.Keep.HeaderText = "Keep";
            this.Keep.Name = "Keep";
            this.Keep.TrueValue = "-1";
            this.Keep.Width = 45;
            // 
            // Del
            // 
            this.Del.FalseValue = "0";
            this.Del.HeaderText = "Del";
            this.Del.Name = "Del";
            this.Del.TrueValue = "-1";
            this.Del.Width = 45;
            // 
            // S1099
            // 
            this.S1099.FalseValue = "0";
            this.S1099.HeaderText = "1099";
            this.S1099.Name = "S1099";
            this.S1099.ReadOnly = true;
            this.S1099.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.S1099.TrueValue = "-1";
            this.S1099.Width = 45;
            // 
            // PDLastName
            // 
            this.PDLastName.HeaderText = "Last Name";
            this.PDLastName.Name = "PDLastName";
            this.PDLastName.ReadOnly = true;
            // 
            // PDFirstName
            // 
            this.PDFirstName.HeaderText = "First Name";
            this.PDFirstName.Name = "PDFirstName";
            this.PDFirstName.ReadOnly = true;
            // 
            // PDEntityID
            // 
            this.PDEntityID.HeaderText = "Entity ID";
            this.PDEntityID.Name = "PDEntityID";
            this.PDEntityID.ReadOnly = true;
            // 
            // PDEntityType
            // 
            this.PDEntityType.HeaderText = "Type";
            this.PDEntityType.Name = "PDEntityType";
            this.PDEntityType.ReadOnly = true;
            this.PDEntityType.Visible = false;
            // 
            // PDEntType
            // 
            this.PDEntType.HeaderText = "Ent Type";
            this.PDEntType.Name = "PDEntType";
            this.PDEntType.ReadOnly = true;
            // 
            // txtEID
            // 
            this.txtEID.Location = new System.Drawing.Point(13, 424);
            this.txtEID.Name = "txtEID";
            this.txtEID.Size = new System.Drawing.Size(43, 20);
            this.txtEID.TabIndex = 14;
            this.txtEID.Visible = false;
            // 
            // txtStateID
            // 
            this.txtStateID.Location = new System.Drawing.Point(62, 424);
            this.txtStateID.Name = "txtStateID";
            this.txtStateID.Size = new System.Drawing.Size(43, 20);
            this.txtStateID.TabIndex = 15;
            this.txtStateID.Visible = false;
            // 
            // txtSexID
            // 
            this.txtSexID.Location = new System.Drawing.Point(111, 424);
            this.txtSexID.Name = "txtSexID";
            this.txtSexID.Size = new System.Drawing.Size(43, 20);
            this.txtSexID.TabIndex = 16;
            this.txtSexID.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(11, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(426, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Can not replace Claimants/Employee with any other entity type.";
            // 
            // FormPayee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(503, 452);
            this.Controls.Add(this.txtSexID);
            this.Controls.Add(this.txtStateID);
            this.Controls.Add(this.txtEID);
            this.Controls.Add(this.ctlTabs);
            this.Controls.Add(this.lblPayee);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPayee";
            this.Text = "FormPayee";
            this.Load += new System.EventHandler(this.FormPayee_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ctlTabs.ResumeLayout(false);
            this.tabSearch.ResumeLayout(false);
            this.tabSearch.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbPayeeType.ResumeLayout(false);
            this.gbPayeeType.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.tabResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblPayee;
        internal System.Windows.Forms.TabControl ctlTabs;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDisplay;
        internal System.Windows.Forms.DataGridView ctlPRGrid;
        internal System.Windows.Forms.DataGridView ctlPGridDet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbPayeeLastName;
        private System.Windows.Forms.GroupBox gbPayeeType;
        private System.Windows.Forms.RadioButton rbClaimants;
        private System.Windows.Forms.RadioButton rbPayeeLastNameSpecific;
        private System.Windows.Forms.TextBox txtHomePhn;
        private System.Windows.Forms.Label lblPayeeDetailPhone;
        private System.Windows.Forms.TextBox txtOffPhn;
        private System.Windows.Forms.Label lblPayeeDetailAbbr;
        private System.Windows.Forms.TextBox txtAbbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayeeLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoofDuplicates;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClaimantEID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayeeEID;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblPayeeDetailName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.ComboBox cmbET;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtTaxID;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtAddr2;
        private System.Windows.Forms.Label lblPayeeDetailAddress;
        private System.Windows.Forms.TextBox txtAddr1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox txtEID;
        private System.Windows.Forms.TextBox txtStateID;
        private System.Windows.Forms.TextBox txtSexID;
        private System.Windows.Forms.Label lblPayeeDetailEntityType;
        private System.Windows.Forms.Label lblPayeeDetailTaxId;
        private System.Windows.Forms.Label lblPayeeDetailEMail;
        private System.Windows.Forms.Label lblPayeeDetailCity;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStateZip;
        private System.Windows.Forms.ComboBox comboStZip;
        private System.Windows.Forms.TextBox txtBirthDate;
        private System.Windows.Forms.Label lblCriteria;
        private System.Windows.Forms.TextBox txtLastN;
        private System.Windows.Forms.Label lblcriteriaDet;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Keep;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Del;
        private System.Windows.Forms.DataGridViewCheckBoxColumn S1099;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntType;
        private Label label3;

    }
}