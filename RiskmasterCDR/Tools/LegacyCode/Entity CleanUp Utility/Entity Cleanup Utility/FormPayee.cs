﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;
using System.Data.SqlClient;

namespace Entity_Cleanup_Utility
{
    public partial class FormPayee : Form,IEntityForm
    {

        bool buttonClick = false;
        static Boolean rowsflag = true;

        #region IEntityForm Members

        public DataGridView CtlPRGrid
        {
            get { return this.ctlPRGrid; }
        }

        public DataGridView CtlPGridDet
        {
            get { return this.ctlPGridDet; }
        }

        public TabControl CtlTabs
        {
            get
            {
                return this.ctlTabs;
            }
        }

        #endregion

        #region Enums
        /// <summary>
        /// Search Type
        /// </summary>
        private enum eSearchType
        {
            stLName = 0,
            stLNameSpec = 1
        }
        #endregion

        public FormPayee()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #region FormPayee_Load
        private void FormPayee_Load(object sender, EventArgs e)
        {
            try
            {
                #region Defining Events Handler
                ctlTabs.DrawMode = TabDrawMode.OwnerDrawFixed;
                ctlTabs.DrawItem += new DrawItemEventHandler(ctlTabs1_DrawItem);
                loadStates();
                loadEntities();
                //loadSexCodes(); Not been used in existing app
      
                #endregion
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

        }
        #endregion

        #region Tab Events

        /// <summary>
        /// Handles the Selected event of the ctlTabs control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TabControlEventArgs"/> instance containing the event data.</param>
        private void ctlTabs_Selected(object sender, TabControlEventArgs e)
        {
            try
            {

                Cursor = Cursors.WaitCursor;

                switch (e.TabPageIndex)
                {
                    case 0:     // Search
                        rbPayeeLastName.Checked = true;
                        rowsflag = true;
                        break;
                    case 1:     // Results
                        if (!buttonClick)           // In case of buttonclick search is 1. Means this function is already called at button click
                        {
                            if (rowsflag == true)
                            {
                                if (rbPayeeLastName.Checked)
                                {
                                    searchWithNameOptions(Convert.ToInt16(eSearchType.stLName));
                                }

                                else if (rbPayeeLastNameSpecific.Checked)
                                {
                                    searchWithNameOptions(Convert.ToInt16(eSearchType.stLNameSpec), txtLastN.Text.Trim());
                                }
                            }
                        }
                        break;
                    case 2:         // Details
                        if (!buttonClick)           // In case of buttonclick search is 1. Means this function is already called at button click
                        {
                            if (rbPayeeLastName.Checked)
                            {
                                if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                                {
                                    rowsflag = false;
                                    ctlTabs.SelectTab(1);
                                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                                    return;

                                }
                                else
                                {
                                    rowsflag = false;
                                    sDisplay();

                                }
                            }
                        }
                        break;
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region Private Methods

        #region ctlTabs_DrawItem for making active index bold
        public void ctlTabs1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == ctlTabs.SelectedIndex)
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        new Font(ctlTabs.Font, FontStyle.Bold),
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
                else
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        ctlTabs.Font,
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region SearchByNameType
        /// <summary>
        /// Searches the last name of the by.
        /// </summary>
        // bpaskova JIRA 4050 added method return value
        // private void searchWithNameOptions(Int16 _searchCriteria, String _optCaption = "")
        private Int16 searchWithNameOptions(Int16 _searchCriteria, String _optCaption = "")
        {
            Int16 iNextTabId = 1;
            try
            {

                frmMain.updateStatus("Searching for Claimants............");
                ctlPRGrid.Rows.Clear();
                ctlPGridDet.Rows.Clear();

                StringBuilder _Criteria = new StringBuilder();
                StringBuilder sbSql = new StringBuilder();
                Double _counter = 0;
                if (_searchCriteria == Convert.ToInt16(eSearchType.stLName))
                {
                    #region If Criteria is last name
                    _Criteria.Append("Searched by: Last Name  ");
                    sbSql.AppendFormat(@"SELECT E.LAST_NAME AS ELN, E.FIRST_NAME AS EFN, F.LAST_NAME AS FLN,COUNT(*) AS CNT, 
                                             F.CLAIMANT_EID, F.PAYEE_EID, E.PARENT_1099_EID
                                             FROM FUNDS F JOIN ENTITY E ON F.CLAIMANT_EID = E.ENTITY_ID 
                                                AND UPPER(F.LAST_NAME) LIKE '%' + UPPER(E.LAST_NAME) + '%' 
                                                WHERE F.CLAIMANT_EID <> F.PAYEE_EID     
                                                AND E.LAST_NAME IS NOT NULL 
                                                AND F.PAYEE_EID <> 0 
                                                GROUP BY E.LAST_NAME, E.FIRST_NAME, F.LAST_NAME, F.CLAIMANT_EID, F.PAYEE_EID, E.PARENT_1099_EID 
                                                ORDER BY E.LAST_NAME, E.FIRST_NAME");

                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            _counter++;
                            ctlPRGrid.Rows.Add(dbReader.GetString("ELN")
                                , dbReader.GetString("EFN"), dbReader.GetString("FLN")
                                , dbReader.GetInt("cnt")
                                , dbReader.GetInt("CLAIMANT_EID"), dbReader.GetInt("PAYEE_EID"));
                        }

                        _Criteria.Append("Results: " + _counter);
                        lblCriteria.Text = _Criteria.ToString();

                    }

                    #endregion
                    //WHERE f.CLAIMANT_EID <> f.PAYEE_EID               NNED TO APPEND THIS ON 4TH LINE OF ABOVE QUERY
                }
                else if (_searchCriteria == Convert.ToInt16(eSearchType.stLNameSpec))
                {
                    #region If Criteria is last name specific
                    if (_optCaption == string.Empty)
                    {
                        MessageBox.Show("Last name is required for selected option.", "Entity CleanUp Utility Says :");
                        // bpaskova JIRA 4050 start
                        //ctlTabs.SelectTab(0);
                        iNextTabId = 0;
                        ctlTabs.SelectTab(iNextTabId);
                        // bpaskova JIRA 4050 end
                        rbPayeeLastNameSpecific.Checked = true;

                    }
                    else
                    {

                        _Criteria.Append("Searched by: Last Name(Specific)  ");
                        sbSql.AppendFormat(@"SELECT e.LAST_NAME AS ELN, e.FIRST_NAME AS EFN, f.LAST_NAME AS FLN,COUNT(*) AS CNT, 
                                             f.CLAIMANT_EID, f.PAYEE_EID, e.PARENT_1099_EID
                                             FROM FUNDS f JOIN ENTITY e on f.CLAIMANT_EID = e.ENTITY_ID 
                                                AND UPPER(f.LAST_NAME) LIKE '%' + UPPER(e.LAST_NAME) + '%'
                                                WHERE f.CLAIMANT_EID <> f.PAYEE_EID     
                                                AND e.LAST_NAME IS NOT NULL 
                                                AND f.PAYEE_EID <> 0 
                                                AND UPPER(e.LAST_NAME) LIKE " + "'%" + _optCaption.ToUpper() + "%'"
                                                     + " GROUP BY e.LAST_NAME, e.FIRST_NAME, f.LAST_NAME, f.CLAIMANT_EID, f.PAYEE_EID, e.PARENT_1099_EID"
                                                     + " ORDER BY e.LAST_NAME, e.FIRST_NAME");


                        using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                        {
                            while (dbReader.Read())
                            {
                                _counter++;
                                ctlPRGrid.Rows.Add(dbReader.GetString("ELN")
                                    , dbReader.GetString("EFN"), dbReader.GetString("FLN")
                                    , dbReader.GetInt("cnt")
                                    , dbReader.GetInt("CLAIMANT_EID"), dbReader.GetInt("PAYEE_EID"));
                            }

                            _Criteria.Append("Results: " + _counter);
                            lblCriteria.Text = _Criteria.ToString();
                        }
                    }
                    #endregion
                    //WHERE f.CLAIMANT_EID <> f.PAYEE_EID               NNED TO APPEND THIS ON 4TH LINE OF ABOVE QUERY
                }
                frmMain.updateStatus(" ");
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            // bpaskova JIRA 4050
            return iNextTabId;
        }
        #endregion

        #region GetStates

        private void loadStates()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT STATES.STATE_ID, STATES.STATE_ROW_ID FROM STATES ORDER BY STATE_ID  ");
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        comboStZip.Items.Add(new FillCombo(dbReader.GetString("STATE_ID"), dbReader.GetInt("STATE_ROW_ID")));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region loadEntities

        private void loadEntities()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT TABLE_NAME, GT.TABLE_ID FROM GLOSSARY G, GLOSSARY_TEXT GT WHERE G.TABLE_ID = GT.TABLE_ID AND GLOSSARY_TYPE_CODE IN(4,7) ORDER BY TABLE_NAME  ");
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        cmbET.Items.Add(new FillCombo(dbReader.GetString("TABLE_NAME"), dbReader.GetInt("TABLE_ID")));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion
        
        #region loadSexCodes

        private void loadSexCodes()
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                gsSql.Append("SELECT CODE_DESC, CT.CODE_ID FROM GLOSSARY, CODES, CODES_TEXT CT WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'SEX_CODE' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CT.CODE_ID ORDER BY CODE_DESC  ");
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {

                    while (dbReader.Read())
                    {
                        // cmbSex.Items.Add(new FillCombo(dbReader.GetString("CODE_DESC"), dbReader.GetInt("CODE_ID")));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region Display Function
        private void sDisplay()
        {
            try
            {
                frmMain.updateStatus("Getting details........... ");
                Cursor = Cursors.WaitCursor;


                if (ctlPRGrid.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for duplicates before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPRGrid.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }

                StringBuilder sbSql = new StringBuilder();
                string s1099Parent = string.Empty;
                int lEntityID;
                string sLastName = string.Empty;
                string sFirstname = string.Empty;
                int lTableID;
                string sTableName = string.Empty;
                Double counter = 0;
                ctlPGridDet.Rows.Clear();
                int rowIndex = default(int);

                sbSql.Append("SELECT ENTITY_ID, LAST_NAME, FIRST_NAME, ENTITY_TABLE_ID, TABLE_NAME, PARENT_1099_EID ");
                sbSql.Append("FROM ENTITY JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = TABLE_ID ");
                sbSql.AppendFormat(" WHERE DELETED_FLAG = 0 AND ENTITY_ID {0} ", EntityCleanUpHelper.gsEntities);
                sbSql.Append("ORDER BY LAST_NAME, FIRST_NAME");

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sbSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        counter++;
                        lEntityID = dbReader.GetInt("ENTITY_ID");
                        sLastName = dbReader.GetString("LAST_NAME");
                        sFirstname = dbReader.GetString("FIRST_NAME");
                        lTableID = dbReader.GetInt("ENTITY_TABLE_ID");
                        s1099Parent = dbReader.GetInt("PARENT_1099_EID") == 0 ? "0" : "1";
                        sTableName = dbReader.GetString("TABLE_NAME");
                        rowIndex = ctlPGridDet.Rows.Add(0, 0, s1099Parent, sLastName, sFirstname, lEntityID, lTableID, sTableName);
                         
                        if (lTableID != 1060 && lTableID != 12)
                         {
                             this.ctlPGridDet.Rows[rowIndex].Cells[0].ReadOnly = true;
                             this.ctlPGridDet.Rows[rowIndex].Cells[0].Style.BackColor = Color.LightGray;
                         }
                    }
                }


                lblcriteriaDet.Text = "Display for: " + sLastName + "    Results: " + counter;
                ctlPGridDet.Enabled = true;
                ctlPGridDet.ClearSelection();

                bool success;
                success = Helper.gobjSettings.Show1099ParentColumn == true ? true : false;
                ctlPGridDet.Columns[2].Visible = success;
                frmMain.updateStatus(" ");

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #endregion

        #region Button Events
        #region  btnDisplay_Click
        /// <summary>
        /// Handles the Click event of the btnDisplay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;
                Cursor = Cursors.WaitCursor;
                sDisplay();
                Cursor = Cursors.Default;
                if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                {

                    ctlTabs.SelectTab(1);
                }
                else
                {
                    ctlTabs.SelectTab(2);
                }
                buttonClick = false;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region btnProcess_Click
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Helper.sProcChanges<FormPayee>(this);
                Cursor = Cursors.Default;
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region btnSearch_Click
        private void btnSearch_Click(object sender, EventArgs e)
        {
            // bpaskova JIRA 4050
            Int16 iNextTabId = 1;  //Results tab
            try
            {
                buttonClick = true;
                Cursor = Cursors.WaitCursor;

                if (rbPayeeLastName.Checked)
                {
                    searchWithNameOptions(Convert.ToInt16(eSearchType.stLName));
                }

                else if (rbPayeeLastNameSpecific.Checked)
                {
                    // bpaskova JIRA 4050 start
                    //searchWithNameOptions(Convert.ToInt16(eSearchType.stLNameSpec), txtLastN.Text.Trim());
                    iNextTabId = searchWithNameOptions(Convert.ToInt16(eSearchType.stLNameSpec), txtLastN.Text.Trim());
                    // bpaskova JIRA 4050 end
                }

                Cursor = Cursors.Default;
                // bpaskova JIRA 4050 start
                //ctlTabs.SelectTab(1);
                ctlTabs.SelectTab(iNextTabId);
                // bpaskova JIRA 4050 end

                buttonClick = false;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region Grid Events

        #region ctlPRGrid_Click
        /// <summary>
        /// Handles the Click event of the ctlPRGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ctlPRGrid_Click(object sender, EventArgs e)
        {
            try
            {
                EntityCleanUpHelper.gsEntities = string.Empty;
                EntityCleanUpHelper.gsStr = string.Empty;
                int lEID;
                if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                {
                    return;
                }

                lEID = (int)ctlPRGrid.SelectedRows[0].Cells["ClaimantEID"].Value;
                if (ctlPRGrid.Rows.Count == 1)
                {
                    EntityCleanUpHelper.gsEntities = string.Format("IN ({0},{1})", lEID, (int)ctlPRGrid.SelectedRows[0].Cells["PayeeEID"].Value);
                }
                else
                {
                    foreach (DataGridViewRow row in ctlPRGrid.Rows)
                    {
                        if ((int)row.Cells["ClaimantEID"].Value == lEID)
                        {
                            EntityCleanUpHelper.gsStr += string.Format("{0},", row.Cells["PayeeEID"].Value);
                        }
                    }
                    EntityCleanUpHelper.gsEntities = string.Format("IN({0},{1})", lEID, EntityCleanUpHelper.gsStr.TrimEnd(','));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region ctlPGridDet_CellClick
        /// <summary>
        /// Handles the CellClick event of the ctlPGridDet control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void ctlPGridDet_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int lEntityId;
            StringBuilder gsSql = new StringBuilder();
            try
            {
                if (ctlPGridDet.Rows.Count < 1)
                {
                    return;
                }
                lEntityId = (int)ctlPGridDet.SelectedRows[0].Cells["PDEntityID"].Value;
                int Entity_Table_ID = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[6].Value); // Seixth Column
                string Entity_Table_Name = Convert.ToString(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[7].Value); // Seventh Column

                if (Helper.gsDatabaseType.Equals(eDatabaseType.DBMS_IS_ORACLE))
                {
                    gsSql.Append("SELECT ENTITY.*, STATES.STATE_ID ST_CD, STATES.STATE_ROW_ID, (EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM TO_DATE(EN.BIRTH_DATE,'yyyymmdd'))) AGE ");
                }
                else
                {
                    gsSql.Append("SELECT ENTITY.*, STATES.STATE_ID ST_CD, STATES.STATE_ROW_ID,DATEDIFF(YY,BIRTH_DATE,GETDATE()) AGE ");
                }

                gsSql.Append("FROM ENTITY JOIN STATES ON ENTITY.STATE_ID = STATE_ROW_ID");
                gsSql.AppendFormat("WHERE ENTITY.ENTITY_ID = {0} AND ", lEntityId);

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    if (dbReader.Read())
                    {


                        txtAbbr.Text = dbReader.GetString("ABBREVIATION");
                        txtEID.Text = dbReader.GetInt("ENTITY_ID").ToString();
                        txtLastName.Text = dbReader.GetString("LAST_NAME");
                        txtFirstName.Text = dbReader.GetString("FIRST_NAME");
                        txtAddr1.Text = dbReader.GetString("ADDR1");
                        txtAddr2.Text = dbReader.GetString("ADDR2");
                        txtCity.Text = dbReader.GetString("City");
                        txtHomePhn.Text = dbReader.GetString("PHONE2");
                        txtOffPhn.Text = dbReader.GetString("PHONE1");
                        txtTaxID.Text = dbReader.GetString("TAX_ID");
                        txtEmail.Text = dbReader.GetString("EMAIL_ADDRESS");
                        txtBirthDate.Text = dbReader.GetString("BIRTH_DATE");
                        txtAge.Text = dbReader.GetInt("AGE").ToString();
                        txtZip.Text = dbReader.GetString("ZIP_CODE");
                        //if (dbReader.GetInt("SEX_CODE") != default(int))
                        //{                                                        //using (LocalCache objLocalCache = new LocalCache(Helper.sConnectString))
                        //    //{
                        //    //    txtSex.Text = uCodes.GetCodeDesc(rS!SEX_CODE)
                        //    //}
                        //}

                        FillCombo obj1 = new FillCombo();
                        obj1.Name = Convert.ToString(dbReader.GetString("ST_CD"));
                        obj1.Value = dbReader.GetInt("STATE_ROW_ID");
                        comboStZip.SelectedItem = obj1;

                        FillCombo obj = new FillCombo();
                        obj.Name = Entity_Table_Name;
                        obj.Value = Entity_Table_ID;
                        cmbET.SelectedItem = obj;

                        txtBirthDate.Text = dbReader.GetString("BIRTH_DATE");
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

        }

        #endregion

        #endregion

        #region RadioButton Events

        #region rbPayeeLastNameSpecific_CheckedChanged
        private void rbPayeeLastNameSpecific_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbPayeeLastNameSpecific.Checked)
                {
                    txtLastN.Visible = true;
                    txtLastN.Text = string.Empty;
                    txtLastN.Focus();
                }
                else
                {
                    txtLastN.Text = string.Empty;
                    txtLastN.Visible = false;

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion



        #endregion


        #region IEntityForm Members


        public bool ButtonClick
        {
            get
            {
                return buttonClick;
            }
            set
            {
                buttonClick = value;
            }
        }

        #endregion
    }
}
