﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Common;
namespace Entity_Cleanup_Utility
{
    public partial class DateOfBirth : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateOfBirth"/> class.
        /// </summary>
        public DateOfBirth()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.TextChanged" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnTextChanged(EventArgs e)
        {
            this.txtDOB.Text = Conversion.GetDBDateFormat(this.Text, "MM/dd/yyyy");
            this.txtAge.Text = !string.IsNullOrEmpty(this.Text)? Utilities.CalculateAgeInYears(this.Text):string.Empty;
        }
    }
}
