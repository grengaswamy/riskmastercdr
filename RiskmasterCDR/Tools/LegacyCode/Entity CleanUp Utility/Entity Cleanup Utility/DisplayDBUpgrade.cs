﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;

using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;

namespace RMXdBUpgradeWizard
{
   public class DisplayDBUpgrade
    {
        #region "Global Variables"
        public static int g_iDSNID;
        public static int dbLookup;
        public static int g_dbMake; //dB manufacturer
        public static string sODBCName;
        public static string sBaseLogType; //Govind


        public static string gsDBRelease; //holds release_number of current DB (one logged into)
        public static string gsDBVersion; //holds dtg_version of current DB
        const string STRING_DELIMITER = ",";

        #region public properties
        public static string gsDBTargetRel;
        public static string gsDBTargetVer;
        public static string gsFORM_TITLE;
        public static string gsSecurityScripts;
        public static string gsBaseScripts;
        public static string gsBasePriorScripts;
        public static string gsSessionScripts;
        public static string gsTaskManagerScripts;
        public static string gsStoredProcs;
        public static string gsSqlStoredProcs;
        public static string gsOracleStoredProcs;
        public static string gsHPBaseScripts;
        public static string gsHPSecurityScripts;
        //NADIM PRIMARY KEY
        public static string gsStoredProcs4Primarkeys;
        public static string gsRunPrimarykeys;
        public static string gsIsBRSEnabled;
        public static string gsViewScripts;
        public static string gsCustomScripts;
        public static string gsInsertXmlViews;
        public static string gsInsertViewsOnly;
        public static string gsHistoryTrackingScripts; //Mgaba2:R7:History Tracking
        public static string gsHistoryStoredProcs;//Mgaba2:R7:History Database Stored Procedures
        public static string gsSortMasterScripts;
        public static string gsCustomViewOnly;

        /// <summary>
        /// Gets all of the configured triggers to run on the target database
        /// </summary>
     
        internal static string DBTARGETREL
        {
            get
            {
                try
                {
                    gsDBTargetRel = ConfigurationManager.AppSettings["RMRELEASEDATE"];
                }
                catch (Exception)
                {
                }

                return gsDBTargetRel;
            }
            set
            {
                gsDBTargetRel = value;
            }
        }
        internal static string DBTARGETVER
        {
            get
            {
                try
                {
                    gsDBTargetVer = ConfigurationManager.AppSettings["RMRELEASEVERSION"];
                }
                catch (Exception)
                {
                }

                return gsDBTargetVer;
            }
            set
            {
                gsDBTargetVer = value;
            }
        }
        internal static string FORMTITLE
        {
            get
            {
                try
                {
                    gsFORM_TITLE = ConfigurationManager.AppSettings["FORM_TITLE"];
                }
                catch (Exception)
                {
                }

                return gsFORM_TITLE;
            }
            set
            {
                gsFORM_TITLE = value;
            }
        }

        // Addded by abhinav for MITS33063
        internal static string OSHOMIGSCRIPTS
        {
            get
            {
                try
                {
                    gsFORM_TITLE = ConfigurationManager.AppSettings["OSHOMIGSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsFORM_TITLE;
            }
            set
            {
                gsFORM_TITLE = value;
            }
        }


        //-----------------------------------------------
        internal static string SECURITYSCRIPTS
        {
            get
            {
                try
                {
                    gsSecurityScripts = ConfigurationManager.AppSettings["RMSECURITYSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSecurityScripts;
            }
            set
            {
                gsSecurityScripts = value;
            }
        }
        internal static string BASESCRIPTS
        {
            get
            {
                try
                {
                    gsBaseScripts = ConfigurationManager.AppSettings["RMSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsBaseScripts;
            }
            set
            {
                gsBaseScripts = value;
            }
        }

        internal static string BASEPRIORSCRIPTS
        {
            get
            {
                try
                {
                    gsBasePriorScripts = ConfigurationManager.AppSettings["RMPRIORSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsBasePriorScripts;
            }
            set
            {
                gsBasePriorScripts = value;
            }
        }
        internal static string RMHPMAINDBSCRIPTS
        {
            get
            {
                try
                {
                    gsHPBaseScripts = ConfigurationManager.AppSettings["RMHPMAINDBSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsHPBaseScripts;
            }
            set
            {
                gsHPBaseScripts = value;
            }
        }
        internal static string RMHPSECURITYDBSCRIPTS
        {
            get
            {
                try
                {
                    gsHPSecurityScripts = ConfigurationManager.AppSettings["RMHPSECURITYDBSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsHPSecurityScripts;
            }
            set
            {
                gsHPSecurityScripts = value;
            }
        }
        internal static string SESSIONSCRIPTS
        {
            get
            {
                try
                {
                    gsSessionScripts = ConfigurationManager.AppSettings["RMSESSIONSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSessionScripts;
            }
            set
            {
                gsSessionScripts = value;
            }
        }
        internal static string TASKMANAGERSCRIPTS
        {
            get
            {
                try
                {
                    gsTaskManagerScripts = ConfigurationManager.AppSettings["RMTASKMANAGERCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsTaskManagerScripts;
            }
            set
            {
                gsTaskManagerScripts = value;
            }
        }
        internal static string STOREDPROCS
        {
            get
            {
                try
                {
                    gsStoredProcs = ConfigurationManager.AppSettings["RMSTOREDPROCEDURES"];
                }
                catch (Exception)
                {
                }

                return gsStoredProcs;
            }
            set
            {
                gsStoredProcs = value;
            }
        }
        // akaushik5 Added for MITS 33420 Starts
        /// <summary>
        /// Gets or sets the sqlstoredprocs.
        /// </summary>
        /// <value>
        /// The sqlstoredprocs.
        /// </value>
        internal static string SQLSTOREDPROCS
        {
            get
            {
                try
                {
                    gsSqlStoredProcs = ConfigurationManager.AppSettings["RMSQLSPECIFICSTOREDPROCEDURES"];
                }
                catch
                {
                }

                return gsSqlStoredProcs;
            }
        }

        /// <summary>
        /// Gets or sets the oraclestoredprocs.
        /// </summary>
        /// <value>
        /// The oraclestoredprocs.
        /// </value>
        internal static string ORACLESTOREDPROCS
        {
            get
            {
                try
                {
                    gsOracleStoredProcs = ConfigurationManager.AppSettings["RMORACLESPECIFICSTOREDPROCEDURES"];
                }
                catch
                {
                }

                return gsOracleStoredProcs;
            }
        }
        // akaushik5 Added for MITS 33420 Ends

        //NADIM PRIMARY KEY
        internal static string STOREDPROCS4PRIMARYKEYS
        {
            get
            {
                try
                {
                    gsStoredProcs4Primarkeys = ConfigurationManager.AppSettings["RMPRIMARYKEYSPROCEDURES"];
                }
                catch (Exception)
                {
                }

                return gsStoredProcs4Primarkeys;
            }
            set
            {
                gsStoredProcs4Primarkeys = value;
            }
        }
        //NADIM PRIMARY KEY

        //NADIM PRIMARY KEY
        internal static string RunPrimarykey
        {
            get
            {
                try
                {
                    gsRunPrimarykeys = ConfigurationManager.AppSettings["RUN_PRIMARY_KEYS"];
                }
                catch (Exception)
                {
                }

                return gsRunPrimarykeys;
            }
            set
            {
                gsRunPrimarykeys = value;
            }
        }
        internal static string EnableBRS
        {
            get
            {
                try
                {
                    gsIsBRSEnabled = ConfigurationManager.AppSettings["IS_BRS_ACTIVATED"];
                }
                catch (Exception)
                {
                }

                return gsIsBRSEnabled;
            }
            set
            {
                gsIsBRSEnabled = value;
            }
        }
        //NADIM PRIMARY KEY
        internal static string VIEWSCRIPTS
        {
            get
            {
                try
                {
                    gsViewScripts = ConfigurationManager.AppSettings["RMVIEWSQLSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsViewScripts;
            }
            set
            {
                gsViewScripts = value;
            }
        }
        internal static string CUSTOMSCRIPTS
        {
            get
            {
                try
                {
                    gsCustomScripts = ConfigurationManager.AppSettings["CUSTOMSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsCustomScripts;
            }
            set
            {
                gsCustomScripts = value;
            }
        }
        internal static string CUSTOMVIEWONLY
        {
            get
            {
                try
                {
                    gsCustomViewOnly = ConfigurationManager.AppSettings["CUSTOMVIEWONLY"];
                }
                catch (Exception)
                {
                }

                return gsCustomViewOnly;
            }
            set
            {
                gsCustomViewOnly = value;
            }
        }
        internal static string INSERTXMLVIEWS
        {
            get
            {
                try
                {
                    gsInsertXmlViews = ConfigurationManager.AppSettings["INSERT_XML_VIEWS"];
                }
                catch (Exception)
                {
                }

                return gsInsertXmlViews;
            }
            set
            {
                gsInsertXmlViews = value;
            }
        }
        internal static string INSERTVIEWSONLY
        {
            get
            {
                try
                {
                    gsInsertViewsOnly = ConfigurationManager.AppSettings["INSERT_VIEWS_ONLY_NO_DB_UPGRADE"];
                }
                catch (Exception)
                {
                }

                return gsInsertViewsOnly;
            }
            set
            {
                gsInsertViewsOnly = value;
            }
        }

        //Mgaba2:R7:History Tracking
        //This script will upgrade the database where Audit tables will be stored
        //internal static string HISTORYTRACKINGSCRIPTS
        //{
        //    get
        //    {
        //        try
        //        {
        //            gsHistoryTrackingScripts = ConfigurationManager.AppSettings["RMHISTORYTRACKINGSCRIPTS"];
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        return gsHistoryTrackingScripts;
        //    }
        //    set
        //    {
        //        gsHistoryTrackingScripts = value;
        //    }
        //}

        //Mgaba2:R7:History Tracking
        //This will return the stored procedures to be run on History/Audit database
        //internal static string HISTORYSTOREDPROCS
        //{
        //    get
        //    {
        //        try
        //        {
        //            gsHistoryStoredProcs = ConfigurationManager.AppSettings["RMHISTORYSTOREDPROCEDURES"];
        //        }
        //        catch (Exception)
        //        {
        //        }

        //        return gsHistoryStoredProcs;
        //    }
        //    set
        //    {
        //        gsHistoryStoredProcs = value;
        //    }
        //}

        internal static string RMSORTMASTERSCRIPTS
        {
            get
            {
                try
                {
                    gsSortMasterScripts = ConfigurationManager.AppSettings["RMSORTMASTERSCRIPTS"];
                }
                catch (Exception)
                {
                }

                return gsSortMasterScripts;
            }
            set
            {
                gsSortMasterScripts = value;
            }
        }
        #endregion

        //support "Silent" operation for inclusion into Unit Testing
        public static bool g_bNoPromptMode; //no USER interaction allowed - will be run in an automated env.
        public static bool g_bNoSessionMode; //Do NOT attempt Session DB Update - will run w/o touching session & not running any .Net Interop code
        public static string g_sNoPromptTargetDSN; //passed in command line option to specify which dB to upgrade
        //public string g_sNoPromptLogFullPath;

        public static Dictionary<string, int> dictSelectedItems = new Dictionary<string, int>();
        public static Dictionary<string, int> dictFailedItems = new Dictionary<string, int>();
        public static Dictionary<string, int> dictPassedItems = new Dictionary<string, int>();

        //===============RISKMASTER Login Functions====================
        public static bool bAborted;
        public static bool bStop;
        public static bool g_bOrgSecOn;
        //public static Login objLogin;
        public static string g_sConnectString;
        public static string g_SecConnectString;
        public static string g_ViewConnectString;
        public static string g_SessionConnectString;
        public static string g_TaskManagerConnectString;
        public static string g_DocumentConnectString;
        public static string g_UserId;
        public static string g_DocUserId;
        public static string g_DocDB;
        public const string strDSNColumn = "DSN";
        public const string strDSNIDColumn = "DSNID";
        public static string g_HistTrackConnectString;//Mgaba2:R7: History Tracking
        public static string g_ReportServer_ConnectionString;
        public static int g_DsnId = 0;//nadim for primary key

        #endregion


        /// <summary>
        /// SelectDatabase
        /// </summary>
        /// <returns></returns>

        /// <summary>
        /// gets the database type based on the specified database connection string
        /// </summary>
        /// <param name="strDBConnString">connection string</param>
        /// <returns>database type</returns>
        public static int GetDatabaseMake(string strDBConnString)
        {
            g_dbMake = ADONetDbAccess.DbType(strDBConnString);

            return g_dbMake;
        }



        /// <summary>
        /// Enumeration for Database Upgrade Connection Strings
        /// </summary>
        public enum DbUpgradeConnectionStrings
        {
            RMTaskManager,
            RMSecurity,
            //RMHistory,
            RMViews,
            RMSession,
            RMSortmaster
        }
    }
}//namespace