﻿namespace Entity_Cleanup_Utility
{
    partial class SearchSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chktax = new System.Windows.Forms.CheckBox();
            this.lblchk1 = new System.Windows.Forms.Label();
            this.chk_funds = new System.Windows.Forms.CheckBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.chkParent_1099 = new System.Windows.Forms.CheckBox();
            this.lblchk2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chktax
            // 
            this.chktax.AutoSize = true;
            this.chktax.Location = new System.Drawing.Point(13, 13);
            this.chktax.Name = "chktax";
            this.chktax.Size = new System.Drawing.Size(280, 17);
            this.chktax.TabIndex = 0;
            this.chktax.Text = "Strip formatting characters from TAX_ID during search";
            this.chktax.UseVisualStyleBackColor = true;
            // 
            // lblchk1
            // 
            this.lblchk1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchk1.ForeColor = System.Drawing.Color.Red;
            this.lblchk1.Location = new System.Drawing.Point(17, 37);
            this.lblchk1.Name = "lblchk1";
            this.lblchk1.Size = new System.Drawing.Size(270, 68);
            this.lblchk1.TabIndex = 1;
            this.lblchk1.Text = "*** This option will not work for all database platforms. SQL Server 7.0, SQL Ser" +
    "ver 2000, and ORACLE 9, have been tested. If you select this option and receive " +
    "an error, please uncheck the box. ***";
            // 
            // chk_funds
            // 
            this.chk_funds.AutoSize = true;
            this.chk_funds.Location = new System.Drawing.Point(9, 141);
            this.chk_funds.Name = "chk_funds";
            this.chk_funds.Size = new System.Drawing.Size(281, 17);
            this.chk_funds.TabIndex = 3;
            this.chk_funds.Text = "Change Payee Last and First Names in FUNDS table?";
            this.chk_funds.UseVisualStyleBackColor = true;
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(188, 229);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 4;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // chkParent_1099
            // 
            this.chkParent_1099.AutoSize = true;
            this.chkParent_1099.Location = new System.Drawing.Point(9, 116);
            this.chkParent_1099.Name = "chkParent_1099";
            this.chkParent_1099.Size = new System.Drawing.Size(229, 17);
            this.chkParent_1099.TabIndex = 2;
            this.chkParent_1099.Text = "Show (1099 Parent) column in display grid?";
            this.chkParent_1099.UseVisualStyleBackColor = true;
            // 
            // lblchk2
            // 
            this.lblchk2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchk2.ForeColor = System.Drawing.Color.Red;
            this.lblchk2.Location = new System.Drawing.Point(17, 161);
            this.lblchk2.Name = "lblchk2";
            this.lblchk2.Size = new System.Drawing.Size(270, 68);
            this.lblchk2.TabIndex = 5;
            this.lblchk2.Text = "*** This option will change the names of the PAYEE in the FUNDS table. If changed" +
    ", there is the possibility that the name won\'t match that which is printed on th" +
    "e check ***";
            // 
            // SearchSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(299, 262);
            this.Controls.Add(this.lblchk2);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.chk_funds);
            this.Controls.Add(this.chkParent_1099);
            this.Controls.Add(this.lblchk1);
            this.Controls.Add(this.chktax);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(150, 50);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SearchSettings....";
            this.Load += new System.EventHandler(this.SearchSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chktax;
        private System.Windows.Forms.Label lblchk1;
        private System.Windows.Forms.CheckBox chk_funds;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.CheckBox chkParent_1099;
        private System.Windows.Forms.Label lblchk2;
    }
}