﻿namespace Entity_Cleanup_Utility
{
    using System;
    using System.Xml.Linq;
    using Riskmaster.Common;
    using Riskmaster.Db;
    using System.Linq;

    // ERROR: Not supported in C#: OptionDeclaration
    internal class CSettings
    {
        //E = 69
        //C = 67
        //U = 85
        const int m_strParmCategory = 696785;

        /// <summary>
        /// The M_B change payee name
        /// </summary>
        private bool m_bChangePAYEEName;

        /// <summary>
        /// The M_B disable edit
        /// </summary>
        private bool m_bDisableEdit;

        /// <summary>
        /// The M_B remove taxid formatting
        /// </summary>
        private bool m_bRemoveTAXIDFormatting;

        /// <summary>
        /// The M_B show1099 parent column
        /// </summary>
        private bool m_bShow1099ParentColumn;

        /// <summary>
        /// Gets or sets a value indicating whether [change payee name].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [change payee name]; otherwise, <c>false</c>.
        /// </value>
        public bool ChangePAYEEName
        {
            get { return m_bChangePAYEEName; }
            set { m_bChangePAYEEName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [disable edit].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable edit]; otherwise, <c>false</c>.
        /// </value>
        public bool DisableEdit
        {
            get { return m_bDisableEdit; }
            set { m_bDisableEdit = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [remove taxid formatting].
        /// </summary>
        /// <value>
        /// <c>true</c> if [remove taxid formatting]; otherwise, <c>false</c>.
        /// </value>
        public bool RemoveTAXIDFormatting
        {
            get { return m_bRemoveTAXIDFormatting; }
            set { m_bRemoveTAXIDFormatting = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [show1099 parent column].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show1099 parent column]; otherwise, <c>false</c>.
        /// </value>
        public bool Show1099ParentColumn
        {
            get { return m_bShow1099ParentColumn; }
            set { m_bShow1099ParentColumn = value; }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            XDocument settings;
            bool success;
            string sql = string.Format("SELECT STR_PARM_VALUE, PARM_CATEGORY FROM PARMS_NAME_VALUE WHERE PARM_CATEGORY = {0}", m_strParmCategory);
            string xml = string.Empty;
            try
            {
                using (DbReader objDbReader = DbFactory.GetDbReader(Helper.gsConnectString, sql))
                {
                    if (!object.ReferenceEquals(objDbReader, null) && objDbReader.Read())
                    {
                        xml = objDbReader.GetString("STR_PARM_VALUE");
                    }
                }

                if (!string.IsNullOrEmpty(xml))
                {
                    settings = XDocument.Parse(xml);
                    if (!object.ReferenceEquals(settings, null))
                    {
                        XElement root = settings.Root;
                        if (!object.ReferenceEquals(root, null))
                        {
                            this.ChangePAYEEName = !object.ReferenceEquals(root.Element("bChangePAYEEName"), null) ? Conversion.CastToType<bool>(root.Element("bChangePAYEEName").Value, out success) : false;
                            this.DisableEdit = !object.ReferenceEquals(root.Element("bDisableEdit"), null) ? Conversion.CastToType<bool>(root.Element("bDisableEdit").Value, out success) : false;
                            this.RemoveTAXIDFormatting = !object.ReferenceEquals(root.Element("bRemoveTAXIDFormatting"), null) ? Conversion.CastToType<bool>(root.Element("bRemoveTAXIDFormatting").Value, out success) : false;
                            this.Show1099ParentColumn = !object.ReferenceEquals(root.Element("bShow1099ParentColumn"), null) ? Conversion.CastToType<bool>(root.Element("bShow1099ParentColumn").Value, out success) : false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        /// <summary>
        /// Saves the data.
        /// </summary>
        public void SaveData()
        {
            bool recordExists;
            DbWriter objDbWriter = default(DbWriter);
            try
            {
                XDocument settings = new XDocument(
                    new XElement("Settings",
                        new XElement("bChangePAYEEName", ChangePAYEEName),
                        new XElement("bDisableEdit", DisableEdit),
                        new XElement("bRemoveTAXIDFormatting", RemoveTAXIDFormatting),
                        new XElement("bShow1099ParentColumn", Show1099ParentColumn)
                        ));

                string sql = string.Format("SELECT * FROM PARMS_NAME_VALUE WHERE PARM_CATEGORY = {0}", m_strParmCategory);
                using (DbReader objDbReader = DbFactory.GetDbReader(Helper.gsConnectString, sql))
                {
                    recordExists = !object.ReferenceEquals(objDbReader, null) && objDbReader.Read();
                }

                objDbWriter = DbFactory.GetDbWriter(Helper.gsConnectString);
                objDbWriter.Tables.Add("PARMS_NAME_VALUE");
                objDbWriter.Fields.Add("STR_PARM_VALUE", settings.ToString());

                if (recordExists)
                {
                    objDbWriter.Where.Add(string.Format("PARM_CATEGORY = {0}", m_strParmCategory));
                }
                else
                {
                    objDbWriter.Fields.Add("PARM_CATEGORY", m_strParmCategory);
                    objDbWriter.Fields.Add("PARM_NAME", "EntityCleanUP");
                    objDbWriter.Fields.Add("PARM_DESC", "Entity Clean Up Settings");
                }
                objDbWriter.Execute();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

            finally
            {
                if (!object.ReferenceEquals(objDbWriter, null))
                {
                    objDbWriter = null;
                }
            }
        }
    }
}