﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.Common;

namespace Entity_Cleanup_Utility
{
    public partial class frmEntityCleanUp : Form, IEntityForm
    {
        #region IEntityForm Members

        /// <summary>
        /// Gets the control pr grid.
        /// </summary>
        /// <value>
        /// The control pr grid.
        /// </value>
        public DataGridView CtlPRGrid
        {
            get
            {
                return this.ctlPRGrid;
            }
        }

        /// <summary>
        /// Gets the control p grid det.
        /// </summary>
        /// <value>
        /// The control p grid det.
        /// </value>
        public DataGridView CtlPGridDet
        {
            get
            {
                return this.ctlPGridDet;
            }
        }

        /// <summary>
        /// Gets the control tabs.
        /// </summary>
        /// <value>
        /// The control tabs.
        /// </value>
        public TabControl CtlTabs
        {
            get
            {
                return this.ctlTabs;
            }
        }

        #endregion
        bool buttonClick = false;
        static Boolean rowsflag = true;
        long? lTableID = null;
        DateTime fBirthdate;
        #region Enums
        /// <summary>
        /// Search Type
        /// </summary>
        /// 
        private enum eSearchType
        {
            stLFName = 0,
            stLNameSpec = 1,
            stTaxId = 2,
            stLastNTaxID = 3
        }
        private enum LockFields
        {
            Lock = 0,
            Unlock = 1
        }

        private enum DropDownFill
        {
            EntityTypeFirstTab = 0,
            EntityTypeThirdTab = 1,
            State=2
        }
        #endregion

        public frmEntityCleanUp()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        private void frmEntityCleanUp_Load(object sender, EventArgs e)
        {
            try
            {
                #region Defining Events Handler
                ctlTabs.DrawMode = TabDrawMode.OwnerDrawFixed;
                ctlTabs.DrawItem += new DrawItemEventHandler(ctlTabs1_DrawItem);
                #endregion

                getEntities(Convert.ToInt32(DropDownFill.EntityTypeFirstTab));
                getEntities(Convert.ToInt32(DropDownFill.EntityTypeThirdTab));
                //loadSexCodes(); Not been used in existing app
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #region Private Methods

        #region ctlTabs_DrawItem for making active index bold
        public void ctlTabs1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == ctlTabs.SelectedIndex)
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        new Font(ctlTabs.Font, FontStyle.Bold),
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
                else
                {
                    e.Graphics.DrawString(ctlTabs.TabPages[e.Index].Text,
                        ctlTabs.Font,
                        Brushes.Black,
                        new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region searchCriteria
        // bpaskova JIRA 4050 : added method return value
        // private void searchCriteria()
        private Int16 searchCriteria()
        {
            Int16 iNextTabId = 1;
            try
            {

                frmMain.updateStatus("Searching for Entities............");

                #region Variables at Function Level
              
                String sLastName = string.Empty;
                String sFirstname = string.Empty;
                String sTaxID = string.Empty;
                String sTableName = string.Empty;
                String sName = string.Empty;
                String s1099Parent = string.Empty;

                ctlPRGrid.Rows.Clear();
                ctlPGridDet.Rows.Clear();
                StringBuilder gsSql = new StringBuilder();
                StringBuilder gsDisplayQuery = new StringBuilder();
                StringBuilder _Criteria = new StringBuilder();
                Double _counter = 0;
                #endregion

               
                // 3 Parts to form Query

                #region Making Select Statement
                //ctlOptSel2(0)- TAXID
                if (rb_Entity_Srch_TaxId.Checked)
                {
                    gsSql.Append(" SELECT A.TAX_ID, TABLE_NAME , TABLE_ID, COUNT(*) CNT FROM ( SELECT COALESCE(TAX_ID,'') TAX_ID, ENTITY_ID FROM ENTITY WHERE DELETED_FLAG = 0 ");
                    if (rb_Entity_srch_allEnt.Checked)
                    {
                        gsSql.Append(" AND ENTITY_TABLE_ID NOT IN (1005,1006,1007,1008,1009,1010,1011,1012) "); //Changeset-97807
                    }
                    else
                    {
                        gsSql.Append(" AND ENTITY_TABLE_ID = " + lTableID);
                    }

                    gsSql.Append(" ) AS A JOIN ENTITY ON A.ENTITY_ID = ENTITY.ENTITY_ID JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = GLOSSARY_TEXT.TABLE_ID GROUP BY A.TAX_ID,TABLE_NAME , TABLE_ID HAVING COUNT(*) > 1 ORDER BY A.TAX_ID,TABLE_NAME");

                    _Criteria.Append("Searched by: Tax ID  ");
                }

                // ctlOptSel2(1)- Last Name, First Name
                if (rb_Entity_Srch_LNFN.Checked)
                {
                    gsSql.Append(" SELECT A.LAST_NAME,A.FIRST_NAME,TABLE_NAME, TABLE_ID, COUNT(*) AS CNT FROM ( SELECT COALESCE(FIRST_NAME,'') FIRST_NAME,COALESCE(LAST_NAME,'') LAST_NAME, ENTITY_ID  FROM ENTITY WHERE DELETED_FLAG = 0 ");
                    if (rb_Entity_srch_allEnt.Checked)
                    {
                        gsSql.Append("  AND ENTITY_TABLE_ID NOT IN (1005,1006,1007,1008,1009,1010,1011,1012) "); //Changeset-97807
                    }
                    else
                    {
                        gsSql.Append(" AND ENTITY_TABLE_ID = " + lTableID);
                    }
                    gsSql.Append(" ) AS A JOIN ENTITY ON A.ENTITY_ID = ENTITY.ENTITY_ID JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = GLOSSARY_TEXT.TABLE_ID GROUP BY A.LAST_NAME,A.FIRST_NAME,TABLE_NAME, TABLE_ID HAVING COUNT(*) > 1 ORDER BY A.LAST_NAME,A.FIRST_NAME,TABLE_NAME, TABLE_ID");

                    _Criteria.Append("Searched by: Last Name, First Name  ");
                }

                // ctlOptSel2(2)- TAX_ID, LAST_NAME
                if (rb_Entity_Srch_TaxIDLN.Checked) 
                {
                    gsSql.Append(" SELECT A.TAX_ID, A.LAST_NAME,TABLE_NAME, TABLE_ID, COUNT(*) AS CNT FROM ( SELECT COALESCE(TAX_ID,'') TAX_ID,COALESCE(LAST_NAME,'') LAST_NAME, ENTITY_ID  FROM ENTITY WHERE DELETED_FLAG = 0 ");
                    if (rb_Entity_srch_allEnt.Checked)
                    {
                        gsSql.Append(" AND ENTITY_TABLE_ID NOT IN (1005,1006,1007,1008,1009,1010,1011,1012) "); //Changeset-97807
                    }
                    else
                    {
                        gsSql.Append(" AND ENTITY_TABLE_ID = " + lTableID);
                    }
                    gsSql.Append(" ) AS A JOIN ENTITY ON A.ENTITY_ID = ENTITY.ENTITY_ID JOIN GLOSSARY_TEXT ON ENTITY_TABLE_ID = GLOSSARY_TEXT.TABLE_ID  GROUP BY A.TAX_ID,A.LAST_NAME, TABLE_NAME, TABLE_ID  HAVING COUNT(*) > 1 ORDER BY A.TAX_ID,A.LAST_NAME,TABLE_NAME, TABLE_ID");
                    _Criteria.Append("Searched by: TAX_ID, LAST_NAME  ");
                }

                //ctlOptSel2(3)- Last Name Specific
                if (rb_Entity_Srch_LNS.Checked)
                {
                    if (txtLastN.Text == string.Empty)
                    {
                        MessageBox.Show("Last name is required for selected option.", "Entity CleanUp Utility Says :");
                        // bpaskova JIRA 4050 start
                        //ctlTabs.SelectTab(0);
                        //rb_Entity_Srch_LNS.Checked = true;
                        //return;

                        iNextTabId = 0;
                        ctlTabs.SelectTab(iNextTabId);
                        rb_Entity_Srch_LNS.Checked = true;
                        return iNextTabId;
                        // bpaskova JIRA 4050 end
                    }

                    gsSql.Append(" SELECT TAX_ID, LAST_NAME, FIRST_NAME, COUNT(*) CNT, TABLE_NAME, ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID ");
                    gsSql.Append("FROM ENTITY , GLOSSARY_TEXT");
                    if (rb_Entity_srch_allEnt.Checked)
                    {
                        gsSql.Append(" WHERE DELETED_FLAG = 0 AND ENTITY_TABLE_ID NOT IN (1005,1006,1007,1008,1009,1010,1011,1012) "); //Changeset-97807
                    }
                    else
                    {
                        gsSql.Append(" WHERE DELETED_FLAG = 0 AND ENTITY_TABLE_ID = " + lTableID);
                    }
                    
                    if (string.IsNullOrEmpty(txtLastN.Text.Trim()))
                    {
                        gsSql.Append(" AND (LAST_NAME IS NULL OR LAST_NAME = '')");
                    }
                    else
                    {
                        gsSql.Append(" AND UPPER(LAST_NAME) LIKE " + "'%" + txtLastN.Text.Trim().ToUpper() + "%'");
                    }

                    gsSql.Append(" AND ENTITY_TABLE_ID = TABLE_ID GROUP BY LAST_NAME, FIRST_NAME, TAX_ID, TABLE_NAME, ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID ");
                    gsSql.Append(" ORDER BY LAST_NAME, FIRST_NAME ");
                    _Criteria.Append("Searched by: Last Name Specific  ");
                }
                #endregion

                #region Getting Result from Database
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        _counter++;
                        if (rb_Entity_Srch_TaxId.Checked)                   //ctlOptSel2(0)- TAXID
                        {

                            ctlPRGrid.Columns[0].Visible = false;
                            ctlPRGrid.Columns[1].Visible = false;
                            ctlPRGrid.Columns[2].Visible = true;
                            ctlPRGrid.Columns[3].Visible = true;
                            ctlPRGrid.Rows.Add("", "", dbReader.GetString("TAX_ID"), dbReader.GetString("TABLE_NAME"), dbReader.GetInt("CNT"), dbReader.GetInt("TABLE_ID"));

                        }
                        else if (rb_Entity_Srch_LNFN.Checked)               //ctlOptSel2(1)- Last Name, First Name
                        {

                            ctlPRGrid.Columns[0].Visible = true;
                            ctlPRGrid.Columns[1].Visible = true;
                            ctlPRGrid.Columns[2].Visible = false;
                            ctlPRGrid.Columns[3].Visible = true;
                            ctlPRGrid.Rows.Add(dbReader.GetString("LAST_NAME"), dbReader.GetString("FIRST_NAME"), "", dbReader.GetString("TABLE_NAME"), dbReader.GetInt("CNT"), dbReader.GetInt("TABLE_ID"));

                        }
                        else if (rb_Entity_Srch_LNS.Checked)               //ctlOptSel2(3)- Last Name Specific
                        {

                            ctlPRGrid.Columns[0].Visible = true;
                            ctlPRGrid.Columns[1].Visible = true;
                            ctlPRGrid.Columns[2].Visible = true;
                            ctlPRGrid.Columns[3].Visible = true;

                            ctlPRGrid.Rows.Add(dbReader.GetString("LAST_NAME"), dbReader.GetString("FIRST_NAME"), "", dbReader.GetString("TABLE_NAME"), dbReader.GetInt("CNT"), dbReader.GetInt("ENTITY_TABLE_ID"));
                        }
                        else               //ctlOptSel2(2)- TAX_ID, LAST_NAME
                        {

                            ctlPRGrid.Columns[0].Visible = true;
                            ctlPRGrid.Columns[1].Visible = false;
                            ctlPRGrid.Columns[2].Visible = true;
                            ctlPRGrid.Columns[3].Visible = true;
                            ctlPRGrid.Rows.Add(dbReader.GetString("LAST_NAME"), "", dbReader.GetString("TAX_ID"), dbReader.GetString("TABLE_NAME"), dbReader.GetInt("CNT"), dbReader.GetInt("TABLE_ID"));

                        }

                    }
                }
                _Criteria.Append("Results: " + _counter);
                lblCriteria.Text = _Criteria.ToString();
                lblCriteriaDet.Text = _Criteria.ToString();
                #endregion

                frmMain.updateStatus(" ");
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            // bpaskova JIRA 4050 
            return iNextTabId;
        }
        #endregion

        #region Get Entities
        private void getEntities(int _type)
        {
            try
            {
                StringBuilder gsSql = new StringBuilder();
                if (Convert.ToInt32(DropDownFill.EntityTypeFirstTab) == _type)   // Main Page DropDown
                {

                    gsSql.Append("SELECT TABLE_NAME, GT.TABLE_ID FROM GLOSSARY G, GLOSSARY_TEXT GT ");
                    gsSql.Append("WHERE G.TABLE_ID = GT.TABLE_ID AND GLOSSARY_TYPE_CODE IN(4,7) ORDER BY TABLE_NAME");

                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            cmb_entityType.Items.Add(new FillCombo(dbReader.GetString("TABLE_NAME"), dbReader.GetInt("TABLE_ID")));
                        }
                    }

                }
                else if (Convert.ToInt32(DropDownFill.EntityTypeThirdTab) == _type || Convert.ToInt32(DropDownFill.State) == _type)
                {
                    gsSql.Append("SELECT TABLE_NAME, GT.TABLE_ID FROM GLOSSARY G, GLOSSARY_TEXT GT ");
                    gsSql.Append("WHERE G.TABLE_ID = GT.TABLE_ID AND GLOSSARY_TYPE_CODE IN(4,7) ORDER BY TABLE_NAME");

                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            cmbET.Items.Add(new FillCombo(dbReader.GetString("TABLE_NAME"), dbReader.GetInt("TABLE_ID")));
                        }
                    }

                    gsSql.Clear();

                    gsSql.Append("SELECT STATES.STATE_ID, STATES.STATE_ROW_ID FROM STATES ORDER BY STATE_ID ");
                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            cmbState.Items.Add(new FillCombo(dbReader.GetString("STATE_ID"), dbReader.GetInt("STATE_ROW_ID")));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            // SELECT STATES.STATE_ID, STATES.STATE_ROW_ID FROM STATES 
        }

        #endregion

        #region loadSexCodes

        //private void loadSexCodes()
        //{
        //    try
        //    {
        //        StringBuilder gsSql = new StringBuilder();
        //        gsSql.Append("SELECT CODE_DESC, CT.CODE_ID FROM GLOSSARY, CODES, CODES_TEXT CT WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'SEX_CODE' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CT.CODE_ID ORDER BY CODE_DESC  ");
        //        using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
        //        {

        //            while (dbReader.Read())
        //            {
        //                // cmbSex.Items.Add(new FillCombo(dbReader.GetString("CODE_DESC"), dbReader.GetInt("CODE_ID")));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogger.AddtoLogFile(ex);
        //    }
        //}

        #endregion

        #region DisplayFunction

        /// <summary>
        /// Ss the display.
        /// </summary>
        private void sDisplay()
        {
            try
            {
                String sTaxID = string.Empty;
                String sLastName = string.Empty;
                String sFirstname = string.Empty;
                long lEntityID;
                long lTableID;
                String sTableName = string.Empty;
                String sLName = string.Empty;
                String sFName = string.Empty;
                int s1099Parent;
                int lTableIDCombo;
                StringBuilder gsSql = new StringBuilder();
                int counter = 0;
                lblCriteriaDet.Text = string.Empty;
                ctlPGridDet.Rows.Clear();

                frmMain.updateStatus("Details for Entities............");
                if (ctlPRGrid.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }

                if (ctlPRGrid.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }
                sTaxID = Convert.ToString(ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells["gTaxID"].Value);// TaxId  
                sLastName = Convert.ToString(ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells["gLastName"].Value);// LastName  
                sFirstname = Convert.ToString(ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells["gFirstName"].Value);// FirstName  

                if (rb_Entity_srch_byEntType.Checked)
                {
                    FillCombo selectedData = (FillCombo)cmb_entityType.SelectedItem;
                    lTableIDCombo = selectedData.Value;
                }
                else
                {
                    lTableIDCombo = Convert.ToInt32(ctlPRGrid.Rows[ctlPRGrid.CurrentRow.Index].Cells["EntityTableId"].Value);
                }

                if (rb_Entity_Srch_TaxId.Checked)                   //ctlOptSel2(0)- TAXID
                {
                    gsSql.Append("SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ENTITY_TABLE_ID, GT.TABLE_NAME, E.PARENT_1099_EID FROM ENTITY E ");
                    gsSql.Append("LEFT JOIN GLOSSARY_TEXT GT ON E.ENTITY_TABLE_ID = GT.TABLE_ID ");

                    if (string.IsNullOrEmpty(sTaxID))
                    {
                        gsSql.Append("WHERE (TAX_ID IS NULL OR TAX_ID = '') ");
                    }
                    else
                    {
                        gsSql.AppendFormat("WHERE TAX_ID = '{0}' ", sTaxID);
                    }

                    if (!lTableIDCombo.Equals(default(int)))
                    {
                        gsSql.AppendFormat("AND E.ENTITY_TABLE_ID = {0} ", lTableIDCombo);
                    }

                    gsSql.Append("AND DELETED_FLAG = 0 AND e.ENTITY_TABLE_ID NOT IN (1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012) ");
                    gsSql.Append("ORDER BY e.LAST_NAME, e.FIRST_NAME ");
                    lblCriteriaDet.Text = "Display for: " + sTaxID;

                }
                else if (rb_Entity_Srch_LNFN.Checked)               //ctlOptSel2(1)- Last Name, First Name
                {
                    gsSql.Append("SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ENTITY_TABLE_ID, GT.TABLE_NAME, E.PARENT_1099_EID ");
                    gsSql.Append("FROM ENTITY E LEFT JOIN GLOSSARY_TEXT GT ON E.ENTITY_TABLE_ID = GT.TABLE_ID ");
                    gsSql.AppendFormat("WHERE {0} AND (UPPER(FIRST_NAME) = '{1}' OR FIRST_NAME IS NULL) ", string.IsNullOrEmpty(sLastName) ? "(LAST_NAME IS NULL OR LAST_NAME = '')" : string.Format("UPPER(LAST_NAME) = '{0}'", sLastName.ToUpper()), sFirstname.ToUpper());

                    if (!lTableIDCombo.Equals(default(int)))
                    {
                        gsSql.AppendFormat("AND E.ENTITY_TABLE_ID = {0} ", lTableIDCombo);
                    }
                    
                    gsSql.Append("AND DELETED_FLAG = 0 ORDER BY LAST_NAME, FIRST_NAME ");
                    lblCriteriaDet.Text = string.Format("Display for: {0},{1}", sLastName, sFirstname);
                }
                else if (rb_Entity_Srch_TaxIDLN.Checked)   //ctlOptSel2(1)- Taxid, Last Name
                {
                    gsSql.Append("SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ENTITY_TABLE_ID, GT.TABLE_NAME, E.PARENT_1099_EID FROM ENTITY E ");
                    gsSql.Append("LEFT JOIN GLOSSARY_TEXT GT ON E.ENTITY_TABLE_ID = GT.TABLE_ID ");

                    if (string.IsNullOrEmpty(sTaxID))
                    {
                        gsSql.Append("WHERE (TAX_ID IS NULL OR TAX_ID = '') ");
                    }
                    else
                    {
                        gsSql.AppendFormat("WHERE TAX_ID = '{0}' ", sTaxID);
                    }

                    if (!lTableIDCombo.Equals(default(int)))
                    {
                        gsSql.AppendFormat("AND E.ENTITY_TABLE_ID = {0} ", lTableIDCombo);
                    }

                    gsSql.AppendFormat("AND {0} AND DELETED_FLAG = 0 ORDER BY LAST_NAME, FIRST_NAME", string.IsNullOrEmpty(sLastName) ? "(LAST_NAME IS NULL OR LAST_NAME = '')" : string.Format("UPPER(LAST_NAME) = '{0}'", sLastName.ToUpper()));

                    lblCriteriaDet.Text = "Display for: " + sTaxID + "," + sLastName;
                }
                else
                {

                    gsSql.Append(" SELECT TAX_ID, LAST_NAME, FIRST_NAME, COUNT(*) CNT, TABLE_NAME, ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID FROM ENTITY , GLOSSARY_TEXT ")
                        .Append(" WHERE DELETED_FLAG = 0 ");

                    if (!lTableIDCombo.Equals(default(int)))
                    {
                        gsSql.AppendFormat("AND ENTITY_TABLE_ID = {0} ", lTableIDCombo);
                    }
                    else
                    {
                        gsSql.Append("AND ENTITY_TABLE_ID NOT IN (1005,1006,1007,1008,1009,1010,1011,1012) ");
                    }

                    gsSql.AppendFormat("AND UPPER(LAST_NAME) LIKE '%{0}%' ", sLastName.ToUpper())
                        .Append("AND ENTITY_TABLE_ID = TABLE_ID GROUP BY LAST_NAME, FIRST_NAME, TAX_ID, TABLE_NAME, ENTITY_TABLE_ID, ENTITY_ID, PARENT_1099_EID ORDER BY LAST_NAME, FIRST_NAME  ");
                    lblCriteriaDet.Text = "Display for: " + sLastName;
                }
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    while (dbReader.Read())
                    {
                        lEntityID = (long)dbReader.GetInt("ENTITY_ID");
                        sLastName = dbReader.GetString("LAST_NAME");
                        sFirstname = dbReader.GetString("FIRST_NAME");
                        lTableID = (long)dbReader.GetInt("ENTITY_TABLE_ID");
                        s1099Parent = dbReader.GetInt("PARENT_1099_EID") == 0 ? 0 : 1;
                        sTableName = dbReader.GetString("TABLE_NAME");
                        counter++;
                        ctlPGridDet.Rows.Add(0, 0, s1099Parent, sLastName, sFirstname, lEntityID, lTableID, sTableName);

                    }

                    lblCriteriaDet.Text += ", Results: " + counter;

                    sFieldsType(LockFields.Lock);
                    ctlPGridDet.Enabled = true;
                    ctlPGridDet.ClearSelection();
                    sClearFields(); // To clear the fields in detailed tab
                    btnEdit.Enabled = false;
                    btnProcess.Enabled = false;

                    bool success;
                    success = Helper.gobjSettings.Show1099ParentColumn == true ? true : false;
                    ctlPGridDet.Columns[2].Visible = success;
                    frmMain.updateStatus(" ");
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region CellClick Function
        private void sCellClick()
        {
            try
            {
                int lEntityId;
                StringBuilder gsSql = new StringBuilder();
                if (ctlPGridDet.Rows.Count < 1)
                {
                    return;
                }
                //'enable Edit button
                btnEdit.Enabled = true;
                btnProcess.Enabled = true;
                btnSave.Enabled = false;

                lEntityId = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[5].Value); // Fifth Column
                int Entity_Table_ID = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[6].Value); // Seixth Column
                string Entity_Table_Name = Convert.ToString(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells[7].Value); // Seventh Column

              //  getEntities(Convert.ToInt32(DropDownFill.EntityTypeThirdTab));      // Filling the State and Entitytype Drop down || Runs on Load


                if (Helper.gsDatabaseType.Equals(eDatabaseType.DBMS_IS_ORACLE)) {

                    gsSql.Append("SELECT EN.*, STATES.STATE_ID AS ST_CD, STATES.STATE_ROW_ID AS ST_ID, ");
                    gsSql.Append("(CASE  WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN TO_DATE(BIRTH_DATE,'yyyymmdd') END) AS FORMAT_BIRTH_DATE, ");
                    gsSql.Append("(CASE  WHEN BIRTH_DATE IS NULL THEN NULL WHEN BIRTH_DATE='' THEN NULL WHEN BIRTH_DATE=0 THEN NULL ELSE TO_DATE(BIRTH_DATE,'yyyymmdd') END) AS BIRTH_DATE, ");
                    gsSql.Append("(CASE WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM TO_DATE(EN.BIRTH_DATE,'yyyymmdd')) WHEN ((EN.BIRTH_DATE=NULL AND EN.BIRTH_DATE IS NULL )) THEN 0 END) AS AGE ");
                    gsSql.Append("FROM ENTITY EN, STATES ");
                    gsSql.Append("WHERE EN.ENTITY_ID = " + lEntityId + " AND EN.STATE_ID = STATE_ROW_ID");
                }
                else
                {
                    gsSql.Append("SELECT EN.*, STATES.STATE_ID AS ST_CD, STATES.STATE_ROW_ID AS ST_ID, ");
                    gsSql.Append("(CASE  WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN CAST(BIRTH_DATE AS DATETIME) END) AS FORMAT_BIRTH_DATE, ");
                    gsSql.Append("(CASE  WHEN BIRTH_DATE IS NULL THEN 'NULL' WHEN BIRTH_DATE='' THEN 'NULL' WHEN BIRTH_DATE=0 THEN 'NULL' ELSE BIRTH_DATE END) AS BIRTH_DATE, ");
                    gsSql.Append("(CASE WHEN ((EN.BIRTH_DATE IS NOT NULL AND EN.BIRTH_DATE<>0)) THEN DATEDIFF(YY,EN.BIRTH_DATE,GETDATE()) WHEN ((EN.BIRTH_DATE=NULL AND EN.BIRTH_DATE IS NULL )) THEN 0 END) AS AGE ");
                    gsSql.Append("FROM ENTITY EN, STATES ");
                    gsSql.Append("WHERE EN.ENTITY_ID = " + lEntityId + " AND EN.STATE_ID = STATE_ROW_ID");
                }

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql.ToString()))
                {
                    if (dbReader.Read())
                    {
                        txtAbbr.Text = dbReader.GetString("ABBREVIATION");
                        txtLastName.Text = dbReader.GetString("LAST_NAME");
                        txtFirstName.Text = dbReader.GetString("FIRST_NAME");
                        txtAddr1.Text = dbReader.GetString("ADDR1");
                        txtAddr2.Text = dbReader.GetString("ADDR2");
                        txtCity.Text = dbReader.GetString("CITY");
                        txtOffPhn.Text = dbReader.GetString("PHONE1");
                        txtHomPhn.Text = dbReader.GetString("PHONE2");
                        txttax.Text = dbReader.GetString("TAX_ID");
                        txtEmailDet.Text = dbReader.GetString("EMAIL_ADDRESS");
                        txtBirthDate.Text = dbReader.GetString("BIRTH_DATE");
                        txtAge.Text = dbReader.GetInt("AGE").ToString();
                        txtZip.Text = dbReader.GetString("ZIP_CODE");      
                        fBirthdate = dbReader.GetDateTime("FORMAT_BIRTH_DATE");

                        FillCombo obj = new FillCombo();
                        obj.Name = Entity_Table_Name;
                        obj.Value = Entity_Table_ID;
                        cmbET.SelectedItem = obj;

                        FillCombo obj1 = new FillCombo();
                        obj1.Name = dbReader.GetString("ST_CD").ToString();
                        obj1.Value = dbReader.GetInt("ST_ID");
                        cmbState.SelectedItem = obj1;

                    }
                }
                sFieldsType(LockFields.Lock);
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region sFieldsType
        private void sFieldsType(LockFields Status)
        {
            try
            {
                if (Status == LockFields.Unlock)
                {
                    txtLastName.Enabled = true;
                    txtFirstName.Enabled = true;
                    txtAddr1.Enabled = true;
                    txtAddr2.Enabled = true;
                    txtCity.Enabled = true;
                    cmbState.Enabled = true;
                    txtZip.Enabled = true;
                    txtBirthDate.Enabled = true;
                    // txtAge.Enabled = true;
                    txtOffPhn.Enabled = true;
                    txtHomPhn.Enabled = true;
                    txttax.Enabled = true;                   
                    txtEmailDet.Enabled = true;


                  
                    cmbET.Enabled = true;

                    btnSave.Enabled = true;
                    btnEdit.Enabled = false;
                    btnProcess.Enabled = false;

                    txtBirthDate.Visible = false;
                    datetimepicker1.Visible = true;
                    datetimepicker1.Location = new Point(282, 282);

                    if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = " ";
                        //  
                    }
                    else
                    {
                        datetimepicker1.Value = fBirthdate;
                    }
                }
                else if (Status == LockFields.Lock)
                {
                    txtLastName.Enabled = false;
                    txtFirstName.Enabled = false;
                    txtAddr1.Enabled = false;
                    txtAddr2.Enabled = false;
                    txtCity.Enabled = false;

                    cmbET.Enabled = false;
                    cmbState.Enabled = false;
                    txtZip.Enabled = false;
                    txtBirthDate.Enabled = false;
                    txtAge.Enabled = false;
                    txtOffPhn.Enabled = false;
                    txtHomPhn.Enabled = false;
                    txttax.Enabled = false;
                  
                    txtEmailDet.Enabled = false;

                    btnSave.Enabled = false;
                    btnEdit.Enabled = true;
                    btnProcess.Enabled = true;

                    txtBirthDate.Visible = true;
                    datetimepicker1.Location = new Point(23, 315);
                    datetimepicker1.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region fSaveEntity
        private void fSaveEntity()
        {
            try
            {
                frmMain.updateStatus("Saving Entity............");
                StringBuilder gsSql = new StringBuilder();

                String sLastName = string.Empty;
                String sFirstname = string.Empty;
                int lEID = 0;
                String sAddr1 = string.Empty;
                String sAddr2 = string.Empty;
                int lStateID = 0;
                String sCity = string.Empty;
                String sZip = string.Empty;
                String sOffPh = string.Empty;
                String sHomePh = string.Empty;
                long lEntityET = 0;
                String sTaxID = string.Empty;
                String sEmail = string.Empty;
                String sDOB = string.Empty;
                String sAge = string.Empty;


                //'Setting variables from selected records
                sLastName = txtLastName.Text;
                sFirstname = txtFirstName.Text;
                sAddr1 = txtAddr1.Text;
                sAddr2 = txtAddr2.Text;
                sCity = txtCity.Text;
                sZip = txtZip.Text;

                if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    datetimepicker1.Format = DateTimePickerFormat.Custom;
                    datetimepicker1.CustomFormat = " ";
                    //  
                }
                else
                {
                    sDOB = datetimepicker1.Value.ToString("yyyyMMdd");
                }

                sAge = txtAge.Text;
                sOffPh = txtOffPhn.Text;
                sHomePh = txtHomPhn.Text;
                sTaxID = txttax.Text;
                lEID = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells["PDEntityID"].Value);// EntityID           
                sEmail = txtEmailDet.Text;

                FillCombo selectedStateData = (FillCombo)cmbState.SelectedItem;
                FillCombo selectedEntityTableData = (FillCombo)cmbET.SelectedItem;

                lEntityET = selectedEntityTableData.Value;
                lStateID =  selectedStateData.Value;
                //lEntityET = Convert.ToInt32(ctlPGridDet.Rows[ctlPGridDet.CurrentRow.Index].Cells["PDEntityType"].Value);// Entity_table_id           
                
                //'Update entity record
                gsSql.Append("UPDATE ENTITY SET ");
                gsSql.Append(" LAST_NAME = " + "'" + sLastName + "'");
                gsSql.Append(" ,FIRST_NAME = " + "'" + sFirstname + "'");
                gsSql.Append(" ,ADDR1 = " + "'" + sAddr1 + "'");
                gsSql.Append(" ,ADDR2 = " + "'" + sAddr2 + "'");
                gsSql.Append(" ,CITY = " + "'" + sCity + "'");
                gsSql.Append(" ,ZIP_CODE = " + "'" + sZip + "'");
               // gsSql.Append("STATE_ID = " + lStateID);              CHECK IT LATER ON
                gsSql.Append(" ,PHONE1 = " + "'" + sOffPh + "'");
                gsSql.Append(" ,PHONE2 = " + "'" + sHomePh + "'");
                gsSql.Append(" ,ENTITY_TABLE_ID =" + lEntityET);
                gsSql.Append(" ,STATE_ID= " + lStateID);
                gsSql.Append(" ,TAX_ID = " + "'" + sTaxID + "'");
                gsSql.Append(" ,EMAIL_ADDRESS = " + "'" + sEmail + "'");
                gsSql.Append(" ,BIRTH_DATE = " + "'" + sDOB + "'");
                //     gsSql.Append("UPDATED_BY_USER = " + "'" + sLastName + "'");      CHECK IT LATER ON
                //     gsSql.Append("DTTM_RCD_LAST_UPD = " + "'" + sLastName + "'"); CHECK IT LATER ON
                gsSql.Append(" WHERE ENTITY_ID  = " + lEID);


                int Results = DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql.ToString());
                if (Results > 0)
                {
                    sCellClick();   // Get the Updated values
                    sFieldsType(LockFields.Lock);
                }
                frmMain.updateStatus(" ");

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region sClearFields
        private void sClearFields()
        {
            try
            {
                txtLastName.Text = string.Empty;
                txtFirstName.Text = string.Empty;
                txtAddr1.Text = string.Empty;
                txtAddr2.Text = string.Empty;
                txtCity.Text = string.Empty;
                cmbState.SelectedItem = null;
                cmbET.SelectedItem = null;
                txtZip.Text = string.Empty;
                txtBirthDate.Text = string.Empty;
                txtAge.Text = string.Empty;
                txtOffPhn.Text = string.Empty;
                txtHomPhn.Text = string.Empty;
                txttax.Text = string.Empty;               
                txtEmailDet.Text = string.Empty;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion
        #endregion

        #region Button Events
        #region  btnSearch_Click
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;
               
                if (rb_Entity_srch_byEntType.Checked == true)
                {
                    if (cmb_entityType.SelectedItem == null)
                    {
                        MessageBox.Show("Please select entity type.", "Entity CleanUp Utility Says :");
                        ctlTabs.SelectTab(0);
                        return;
                    }
                    else
                    {
                        Cursor = Cursors.WaitCursor;
                        FillCombo selectedData = (FillCombo)cmb_entityType.SelectedItem;
                        lTableID = (long)selectedData.Value;
                        // bpaskova JIRA 4050 start
                        // searchCriteria();
                        // Cursor = Cursors.Default;
                        // ctlTabs.SelectTab(1);

                        Int16 iNextTabId = searchCriteria();
                        Cursor = Cursors.Default;
                        ctlTabs.SelectTab(iNextTabId);
                        // bpaskova JIRA 4050 end
                    }
                }
                else
                {
                    Cursor = Cursors.WaitCursor;
                    // bpaskova JIRA 4050 start
                    // searchCriteria();
                    // Cursor = Cursors.Default;
                    // ctlTabs.SelectTab(1);

                    Int16 iNextTabId = searchCriteria();
                    Cursor = Cursors.Default;
                    ctlTabs.SelectTab(iNextTabId);
                    // bpaskova JIRA 4050 end
                }

               
                buttonClick = false;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region  btnDisplay_Click
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                buttonClick = true;                // To control that sdisplay function to be called only once on button click. validation at tab events
                Cursor = Cursors.WaitCursor;
                sDisplay();
                Cursor = Cursors.Default;
                if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                {

                    ctlTabs.SelectTab(1);
                }
                else
                {
                    ctlTabs.SelectTab(2);
                }
                buttonClick = false;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region  btnProcess_Click
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                Helper.sProcChanges<frmEntityCleanUp>(this);
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region  btnEdit_Click
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ctlPGridDet.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPGridDet.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }
                sFieldsType(LockFields.Unlock);
                Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region  btnSave_Click
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ctlPGridDet.Rows.Count < 1)
                {
                    MessageBox.Show("You must choose and perform a search for duplicates before proceeding.", "Entity CleanUp Utility Says :");
                    return;
                }
                if (ctlPGridDet.SelectedRows.Count == 0)
                {
                    MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                    return;
                }
                fSaveEntity();
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #endregion

        #region RadioButton Events

        #region rb_Entity_srch_byEntType_CheckedChanged
        private void rb_Entity_srch_byEntType_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rb_Entity_srch_byEntType.Checked)
                {
                   
                    cmb_entityType.Visible = true;
                    //cmb_entityType.Items.Clear();
                    //getEntities(Convert.ToInt32(DropDownFill.EntityTypeFirstTab));  || Runs on Load
                }
                else
                {
                    cmb_entityType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region rb_Entity_Srch_LNS_CheckedChanged
        private void rb_Entity_Srch_LNS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rb_Entity_Srch_LNS.Checked)
                {
                    txtLastN.Visible = true;
                    txtLastN.Text = string.Empty;
                    txtLastN.Focus();
                }
                else
                {
                    txtLastN.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

        }

        #endregion
        
        #endregion

        #region ctlTabs_Selected
        private void ctlTabs_Selected(object sender, TabControlEventArgs e)
        {
            try
            {
                switch (e.TabPageIndex)
                {
                    case 0:     // Search

                        rb_Entity_srch_allEnt.Checked = true;
                        rb_Entity_srch_byEntType.Checked = false;
                        rb_Entity_Srch_TaxId.Checked = true;
                        rb_Entity_Srch_LNS.Checked = false;
                        rb_Entity_Srch_LNFN.Checked = false;
                        rb_Entity_Srch_TaxIDLN.Checked = false;
                        rowsflag = true;
                        break;
                    case 1:     // Results
                        if (!buttonClick)           // In case of buttonclick search is 1. Means this function is already called at button click
                        {
                            if (rowsflag == true)
                            {
                                searchCriteria();
                            }
                        }
                        break;
                    case 2:         // Details
                        if (!buttonClick)           // In case of buttonclick Display is 1. Means this function is already called at button click
                        {
                            if (ctlPRGrid.Rows.Count < 1 || ctlPRGrid.SelectedRows.Count == 0)
                            {
                                rowsflag = false;
                                ctlTabs.SelectTab(1);
                                MessageBox.Show("You have not selected a row in the grid to display.", "Entity CleanUp Utility Says :");
                                return;

                            }
                            else
                            {
                                rowsflag = false;
                                sDisplay();

                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region Cell Click
        private void ctlPGridDet_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                sCellClick();
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region  datetimepicker1_CloseUp
        private void datetimepicker1_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (datetimepicker1.Value > System.DateTime.Now)
                {
                    if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = " ";
                    }
                    else
                    {
                        datetimepicker1.Format = DateTimePickerFormat.Custom;
                        datetimepicker1.CustomFormat = "yyyyMMdd";
                        datetimepicker1.Value = fBirthdate;
                    }
                    MessageBox.Show("Date cannot be greater than current date.", "Entity CleanUp Utility Says :");
                    return;
                }

                if (fBirthdate == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                {
                    datetimepicker1.Format = DateTimePickerFormat.Custom;
                    datetimepicker1.CustomFormat = "yyyyMMdd";
                    fBirthdate = datetimepicker1.Value;
                    datetimepicker1.Value = fBirthdate;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion


        #region IEntityForm Members


        /// <summary>
        /// Gets or sets the button click.
        /// </summary>
        /// <value>
        /// The button click.
        /// </value>
        /// <exception cref="System.NotImplementedException">
        /// </exception>
        public bool ButtonClick
        {
            get
            {
                return buttonClick;
            }
            set
            {
                buttonClick = value;
            }
        }

        #endregion
    }       
}
