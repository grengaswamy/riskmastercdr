﻿namespace Entity_Cleanup_Utility
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using Riskmaster.Common;
    using Riskmaster.Db;
    using Riskmaster.Security;

    /// <summary>
    /// Helper class.
    /// </summary>
    static class Helper
    {
        #region Fields

        /// <summary>
        /// Gets the DSN.
        /// </summary>
        /// <value>
        /// The DSN.
        /// </value>
        public static string DSN { get; private set; }

        /// <summary>
        /// The gs string
        /// </summary>
        public static string gsStr;

        /// <summary>
        /// The gs SQL
        /// </summary>
        public static string gsSql;

        /// <summary>
        /// The gl count
        /// </summary>
        public static int glCnt;

        /// <summary>
        /// The gs user
        /// </summary>
        public static string gsUser = string.Empty;

        /// <summary>
        /// The gl re sult
        /// </summary>
        public static int glReSult;

        /// <summary>
        /// The gl keep eid
        /// </summary>
        public static int KeepEID;

        /// <summary>
        /// The gl keep ei d_ et
        /// </summary>
        public static int glKeepEID_ET;

        /// <summary>
        /// The gl change eid
        /// </summary>
        public static List<int> glChangeEID = new List<int>();

        /// <summary>
        /// The gl change ei d_ et
        /// </summary>
        public static List<int> glChangeEID_ET = new List<int>();

        /// <summary>
        /// The obj settings
        /// </summary>
        public static CSettings gobjSettings = new CSettings();

        /// <summary>
        /// The dbo user identifier
        /// </summary>
        public static string gsDBOUserId;

        /// <summary>
        /// The connect string
        /// </summary>
        public static string gsConnectString;

        /// <summary>
        /// The database type
        /// </summary>
        public static eDatabaseType gsDatabaseType;

        public static StringBuilder buildLogs = new StringBuilder();

        //JIRA 3451 Start
        /// <summary>
        /// List of DataGridViewRow which has same calim association as of original entity. 
        /// </summary>
        public static List<DataGridViewRow> gEntitiesWithSameClaim = new List<DataGridViewRow>();
        //JIRA 3451 End 

        /// <summary>
        /// The CM_STR module name
        /// </summary>
        const string cm_strModuleName = "modUtility";

        /// <summary>
        /// The Setting Global Variables
        /// </summary>
        public static Boolean RemoveTAXIDFormatting = false;
        public static Boolean Show1099ParentColumn = false;
        public static Boolean ChangePAYEEName = false;

        #endregion
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Welcome objsrch = new Welcome();
            objsrch.Show();

            UserLogin oUserLogin = null;
            Login objLogin = new Login();
            bool bCon = false;
            System.Threading.Thread.Sleep(1000);
            objsrch.Close();
            bCon = objLogin.RegisterApplication(0, 0, ref oUserLogin);
            if (bCon && !oUserLogin.DatabaseId.Equals(default(int)))
            {
                Initalize(oUserLogin);
                if (!Helper.WasDatabaseUpgraded())
                {
                    MessageBox.Show("ECU database tables where not found.\nPlease run the database upgrade.\nThis application will close after clicking OK.");
                    return;
                }

                Helper.gsUser = oUserLogin.LoginName;   // Added to get UserName
                gobjSettings.LoadData();
                Application.Run(new frmMain());

            }
            else
            {
                return;
            }
        }



        /// <summary>
        /// Fs the format tax identifier.
        /// </summary>
        /// <param name="sTaxID">The s tax identifier.</param>
        /// <param name="iEntType">Type of the i ent.</param>
        /// <returns></returns>
        public static string fFormatTaxID(string sTaxID, int iEntType)
        {
            string functionReturnValue = null;

            string sTmp = null;

            sTmp = sTaxID;

            sTmp = sTmp.Replace("-", string.Empty);

            if (sTmp.Length == 9)
            {
                functionReturnValue = sTaxID;
            }
            else
            {
                if (iEntType == 7)
                {
                    functionReturnValue = sTmp.Substring(0, 3) + "-" + sTmp.Substring(3, 2) + "-" + sTmp.Substring(5, 4);
                }
                else if (iEntType == 4)
                {
                    functionReturnValue = sTmp.Substring(0, 2) + "-" + sTmp.Substring(3, 7);
                }
                else
                {
                    functionReturnValue = sTaxID;
                }
            }
            return functionReturnValue;
        }

        public static string fGetClientName()
        {
            String getClientName = string.Empty;
            gsSql = "SELECT CLIENT_NAME FROM SYS_PARMS";
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    getClientName = dbReader.GetString("CLIENT_NAME");
                }
            }
            return getClientName;
        }

        /// <summary>
        /// Fs the get employee number.
        /// </summary>
        /// <param name="lEID">The l eid.</param>
        /// <returns></returns>
        public static string fGetEmployeeNumber(int lEID)
        {
            string functionReturnValue = null;

            gsSql = "SELECT EMPLOYEE_NUMBER " + "FROM EMPLOYEE " + "WHERE EMPLOYEE_EID = " + lEID;

            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetString("EMPLOYEE_NUMBER");
                }
            }
            return functionReturnValue;
        }

        //JIRA 3451 Begin
        /// <summary>
        /// Checks if both duplicate (marked for deletion) and original (marked for keep) entities are associated with same claim in Funds table. 
        /// </summary>
        /// <param name="iKeepEID">Employee ID which is being kept.</param>
        /// /// <param name="iDelEID">Employee ID which is being deleted.</param>
        /// <returns></returns>
        public static bool IsAssociatedWithSameClaim(int iKeepEID, int iDelEID)
        {
            bool bReturnValue = false;

            gsSql = "SELECT CLAIM_ID FROM FUNDS WHERE CLAIMANT_EID = " + iDelEID + " AND CLAIM_ID in (SELECT CLAIM_ID FROM FUNDS WHERE CLAIMANT_EID = " + iKeepEID + ")";


            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {

                bReturnValue = dbReader.Read();

            }
            return bReturnValue;
        }
        //JIRA 3451 Ends


        /// <summary>
        /// Fs the first name of the get.
        /// </summary>
        /// <param name="lEID">The l eid.</param>
        /// <returns></returns>
        public static string fGetFirstName(ref int lEID)
        {
            string functionReturnValue = string.Empty;

            gsSql = "SELECT FIRST_NAME " + "FROM ENTITY " + "WHERE ENTITY_ID = " + lEID;
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetString("FIRST_NAME");
                }
            }

            return functionReturnValue;
        }

        

        /// <summary>
        /// Fs the get glossary table identifier.
        /// </summary>
        /// <param name="sStr">The s string.</param>
        /// <returns></returns>
        public static int fGetGlossaryTableID(ref string sStr)
        {
            int functionReturnValue = 0;

            gsSql = "SELECT table_id " + "FROM glossary_text " + "WHERE table_name = '" + sStr + "'";

            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetInt("TABLE_ID");
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Fs the name of the get key field.
        /// </summary>
        /// <param name="sTableName">Name of the s table.</param>
        /// <returns></returns>
        public static string fGetKeyFieldName(string sTableName)
        {
            string functionReturnValue = null;

            gsSql = "SELECT KEY_FIELD_NAME " + "FROM ECU_TABLE_CHECK " + "WHERE TABLE_NAME = '" + sTableName + "'";
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetString("KEY_FIELD_NAME");
                }
            }

            return functionReturnValue;
        }

        /// <summary>
        /// Gets the last name.
        /// </summary>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        private static string GetEntityLastName(int entityId)
        {
            string functionReturnValue = null;

            string sql = string.Format("SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID = {0}", entityId);
            using (DbReader objDbReader = DbFactory.ExecuteReader(Helper.gsConnectString, sql))
            {
                if (objDbReader.Read())
                {
                    functionReturnValue = objDbReader.GetString("LAST_NAME");
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Fs the get state row identifier.
        /// </summary>
        /// <param name="sStr">The s string.</param>
        /// <returns></returns>
        public static int fGetStateRowID(ref string sStr)
        {
            int functionReturnValue = 0;

            gsSql = "SELECT STATE_ROW_ID " + "FROM STATES " + "WHERE STATE_ID = '" + sStr + "' ";
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetInt("STATE_ROW_ID");
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Fs the name of the get table.
        /// </summary>
        /// <param name="lTableID">The l table identifier.</param>
        /// <returns></returns>
        public static string fGetTableName(ref int lTableID)
        {
            string functionReturnValue = null;

            gsSql = "SELECT SYSTEM_TABLE_NAME " + "FROM GLOSSARY " + "WHERE TABLE_ID = " + lTableID;
            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
            {
                if (dbReader.Read())
                {
                    functionReturnValue = dbReader.GetString("SYSTEM_TABLE_NAME");
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Fs the in table.
        /// </summary>
        /// <param name="lEntityID">The l entity identifier.</param>
        /// <param name="sTable">The s table.</param>
        /// <param name="sField">The s field.</param>
        /// <returns></returns>
        public static bool fInTable(int lEntityID, string sTable, string sField)
        {
            bool returnValue = default(bool);
            try
            {
                gsSql = string.Format("SELECT COUNT(*) AS CNT FROM {0} WHERE {1} = {2}", sTable, sField, lEntityID);
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                {
                    returnValue = dbReader.Read() ? !dbReader.GetInt("CNT").Equals(0) : false;
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                ExceptionLogger.AddtoLogFile(ex);
            }
            return returnValue;
        }

        /// <summary>
        /// Ss the proc changes.
        /// </summary>
        /// <typeparam name="T">Of type IEntityForm.</typeparam>
        /// <param name="fFrm">The f FRM.</param>
        internal static void sProcChanges<T>(T fFrm) where T : IEntityForm
        {
            bool bSuccess;
            int keepValue;
            int delValue;
            glChangeEID.Clear();
            glChangeEID_ET.Clear();
            int glCnt = 0;
            List<DataGridViewRow> removeList = null;
            try
            {

                if (fFrm.CtlPGridDet.Rows.Count < 1)
                {
                    MessageBox.Show("Nothing to process. Process halted.", " Entity cleanup code says :");
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                glCnt = 0;

                int keepCheckedCount = fFrm.CtlPGridDet.Rows.Cast<DataGridViewRow>().Count(x => Conversion.CastToType<int>(x.Cells[0].Value.ToString(), out bSuccess).Equals(-1));

                if (keepCheckedCount == 0)
                {
                    MessageBox.Show("Please select an entity to keep. You cannot just delete an entity from this screen, at least one entity must be marked as keep and at least one entity must be checked as delete.", " Entity cleanup code says :");
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                    return;
                }
                else if (keepCheckedCount > 1)
                {
                    MessageBox.Show("You have selected more than one entity to keep, there can only be one selected.", " Entity cleanup code says :");
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                    return;
                }

                if (fFrm.CtlPGridDet.Rows.Cast<DataGridViewRow>().Count(x => Conversion.CastToType<int>(x.Cells[1].Value.ToString(), out bSuccess).Equals(-1)) == 0)
                {
                    MessageBox.Show("You have not selected any entities to delete.");
                    return;
                }

                if (fFrm.CtlPGridDet.Rows.Cast<DataGridViewRow>().Count(x => Conversion.CastToType<int>(x.Cells[0].Value.ToString(), out bSuccess).Equals(-1) && Conversion.CastToType<int>(x.Cells[1].Value.ToString(), out bSuccess).Equals(-1)) > 0)
                {
                    MessageBox.Show("An entity can not be selected to both keep and delete. Please uncheck one of the boxes.", " Entity cleanup code says :");
                    return;
                }

                // Fill arrays with ENTITY ID's
                foreach (DataGridViewRow row in fFrm.CtlPGridDet.Rows)
                {
                    glCnt++;
                    keepValue = Conversion.CastToType<int>(row.Cells[0].Value.ToString(), out bSuccess);
                    if (keepValue.Equals(-1))
                    {
                        KeepEID = Conversion.CastToType<int>(row.Cells[5].Value.ToString(), out bSuccess);
                        glKeepEID_ET = Conversion.CastToType<int>(row.Cells[6].Value.ToString(), out bSuccess);
                    }

                    delValue = Conversion.CastToType<int>(row.Cells[1].Value.ToString(), out bSuccess);
                    if (delValue.Equals(-1))
                    {
                        glChangeEID.Add(Conversion.CastToType<int>(row.Cells[5].Value.ToString(), out bSuccess));
                        glChangeEID_ET.Add(Conversion.CastToType<int>(row.Cells[6].Value.ToString(), out bSuccess));
                    }
                }

                //'Write process log entries

                Helper.buildLogs.AppendLine("   Processing the following: ");
                Helper.buildLogs.AppendLine("   Keeping the following entity: " + KeepEID + " - " + GetEntityLastName(KeepEID));
                Helper.buildLogs.AppendLine("   Deleting the following entities)");

                foreach (int changeEID in glChangeEID)
                {
                    if (changeEID.Equals(default(int)))
                    {
                        break;
                    }
                    else
                    {
                        Helper.buildLogs.AppendLine(changeEID + " - " + GetEntityLastName(changeEID));
                    }
                }

                //JIRA 3451 Begin
                foreach (int changeEID in glChangeEID)
                {
                    if (changeEID.Equals(default(int)))
                    {
                        break;
                    }

                    if (IsAssociatedWithSameClaim(KeepEID, changeEID))
                    {

                        foreach (DataGridViewRow row in fFrm.CtlPGridDet.Rows)
                        {
                            int iEID;
                            iEID = Conversion.CastToType<int>(row.Cells[5].Value.ToString(), out bSuccess);
                            if (iEID == changeEID)
                            {
                                gEntitiesWithSameClaim.Add(row);
                                row.DefaultCellStyle.BackColor = System.Drawing.Color.Red;
                            }
                        }
                    }
                }




                if (gEntitiesWithSameClaim.Count > 0)
                {
                    MessageBox.Show("Highlighted entity(s) can not be deleted as it has association with same claim as of kept entity, this may lead to data discrepancy in funds table.");
                    return;
                }
                //JIRA 3451 End

                int counter = 0;
                foreach (int changeEID in glChangeEID)
                {
                    if (changeEID.Equals(default(int)))
                    {
                        break;
                    }
                    glReSult = Helper.fTestTables(changeEID, KeepEID, glKeepEID_ET, glChangeEID_ET.ElementAt(counter));
                    counter++;
                }


                fFrm.CtlPGridDet.Rows.Clear();
                removeList = new List<DataGridViewRow>();

                frmMain.updateStatus("Refreshing result grid.....please wait.");
                AddtoExecutionLogFile(buildLogs);
                buildLogs.Clear();


               

                fFrm.ButtonClick = false;
                fFrm.CtlTabs.SelectedTab = fFrm.CtlTabs.TabPages[1];
                frmMain.updateStatus(" ");
                MessageBox.Show("Records processed.", "Entity CleanUp Utility Says :");
               
            }
            catch (Exception ex)
            {
                AddtoExecutionLogFile(buildLogs);
                buildLogs.Clear();
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        /// <summary>
        /// Fs the test tables.
        /// </summary>
        /// <param name="lEntityID">The l entity identifier.</param>
        /// <param name="lKeepEnt">The l keep ent.</param>
        /// <param name="lKeepET">The l keep et.</param>
        /// <param name="lChangeET">The l change et.</param>
        /// <returns></returns>
        private static int fTestTables(int lEntityID, int lKeepEnt, int lKeepET, int lChangeET)
        {
            int lTableCnt = default(int);
            double dPerc = 0;
            string sLastName = null;
            int iCnt = 0;
            try
            {
                Helper.gsSql = "SELECT COUNT(*) AS CNT FROM ECU_TABLE_CHECK";

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, Helper.gsSql))
                {
                    lTableCnt = dbReader.Read() ? dbReader.GetInt("CNT") : 1;
                }

                dPerc = 100 / lTableCnt;
                glCnt = 0;
                iCnt = 0;

                gsSql = "SELECT * FROM ECU_TABLE_CHECK ORDER BY TABLE_NAME";
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                {
                    while (dbReader.Read())
                    {
                        if (lKeepET == 1047 && lChangeET == 1047)
                        {
                            // This covers 1) TABLE_NAME = "ADJUST_DATED_TEXT", "CLAIM_ADJUSTER", "CLAIM_ADJ_SUPP"). Here, we change one adjuster to another adjuster.
                            // 2)TABLE_NAME = other tables (for example, we change payees or claimants, and noth payees happen to be adjuster types.

                            

                            if (fInTable(lEntityID, dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim()))
                            {
                                frmMain.updateStatus("Checking: " + dbReader.GetString("TABLE_NAME").Trim() + ".");

                                Helper.buildLogs.AppendLine("   " + lEntityID + " found in: " + dbReader.GetString("TABLE_NAME").Trim() + " table.");
                                glReSult = fUpdateTables(dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim(), dbReader.GetString("KEY_FIELD_NAME").Trim(), lKeepEnt, lEntityID, lKeepET, lChangeET);
                                iCnt = 2;
                            }
                        }
                        else if (lChangeET == 1047 && lKeepET != 1047)
                        {
                            if (!dbReader.GetString("TABLE_NAME").Trim().Equals("ADJUST_DATED_TEXT") && !dbReader.GetString("TABLE_NAME").Trim().Equals("CLAIM_ADJUSTER") && !dbReader.GetString("TABLE_NAME").Trim().Equals("CLAIM_ADJ_SUPP"))
                            {

                                if (fInTable(lEntityID, dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim()))
                                {
                                    frmMain.updateStatus("Checking: " + dbReader.GetString("TABLE_NAME").Trim() + ".");

                                    Helper.buildLogs.AppendLine("   " + lEntityID + " found in: " + dbReader.GetString("TABLE_NAME").Trim() + " table.");
                                    glReSult = fUpdateTables(dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim(), dbReader.GetString("KEY_FIELD_NAME").Trim(), lKeepEnt, lEntityID, lKeepET, lChangeET);
                                    iCnt = 2;
                                }
                            }
                            else
                            {
                                // we can not switch them in the content of adjuster because the new one is non-adjuster.
                            }
                            // old entity is non-adjuster
                        }
                        else if (lChangeET != 1047)
                        {
                            if (fInTable(lEntityID, dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim()))
                            {
                                frmMain.updateStatus("Checking: " + dbReader.GetString("TABLE_NAME").Trim() + ".");
                                Helper.buildLogs.AppendLine("   " + lEntityID + " found in: " + dbReader.GetString("TABLE_NAME").Trim() + " table.");
                                glReSult = fUpdateTables(dbReader.GetString("TABLE_NAME").Trim(), dbReader.GetString("FIELD_NAME").Trim(), dbReader.GetString("KEY_FIELD_NAME").Trim(), lKeepEnt, lEntityID, lKeepET, lChangeET);
                                iCnt = 2;
                            }
                        }
                    }
                }

                if (!(iCnt == 2))
                {
                    // Mark entity as deleted in Entity table
                    if (!lChangeET.Equals(1060))
                    {
                        gsSql = string.Format("UPDATE ENTITY SET DELETED_FLAG = -1 WHERE ENTITY_ID = {0}", lEntityID);
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                       
                        Helper.buildLogs.AppendLine(lEntityID + " - " + GetEntityLastName(lEntityID) + " marked as deleted in ENTITY table.");
                    }

                    // Delete employee record if to change is an employee
                    if (lChangeET == 1060)
                    {
                        gsSql = string.Format("DELETE FROM EMPLOYEE WHERE EMPLOYEE_EID = {0}", lEntityID);
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                       
                        Helper.buildLogs.AppendLine(lEntityID + " - " + GetEntityLastName(lEntityID) + " deleted from EMPLOYEE table.");
                        sLastName = GetEntityLastName(lEntityID);
                        gsSql = string.Format("DELETE FROM ENTITY WHERE ENTITY_ID = {0}", lEntityID);
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                       
                        Helper.buildLogs.AppendLine(lEntityID + " - " + sLastName + " deleted from ENTITY table.");
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

                frmMain.updateStatus(" ");
            return 0;
        }

        /// <summary>
        /// Fs the update tables.
        /// </summary>
        /// <param name="sTableName">Name of the s table.</param>
        /// <param name="sFieldName">Name of the s field.</param>
        /// <param name="sKeyFieldName">Name of the s key field.</param>
        /// <param name="lEntKeep">The l ent keep.</param>
        /// <param name="lEntChange">The l ent change.</param>
        /// <param name="lEntKeepET">The l ent keep et.</param>
        /// <param name="lEntChangeET">The l ent change et.</param>
        /// <returns></returns>
        private static int fUpdateTables(string sTableName, string sFieldName, string sKeyFieldName, int lEntKeep, int lEntChange, int lEntKeepET, int lEntChangeET)
        {
            int lRowID = 0;
            int lDetRowID = 0;
            int lTableID = 0;
            string sDTTM = null;
            int lPayeeTypeCd = 0;
            string sLastName = null;
            int lPITypeCode = 0;
            int iGlossType = 0;
            int lEventID = 0;
            int lClaimID = 0;
            string sFirstname = null;
            string sAddr2 = string.Empty;
            string sAddr1 = string.Empty;
            string sCity = string.Empty;
            string sZip = string.Empty;
            int lStateID = 0;
            lPITypeCode = 0;
            try
            {
                using (LocalCache localCache = new LocalCache(Helper.gsConnectString))
                {
                    if (sTableName == "PERSON_INVOLVED")
                    {
                        switch (lEntKeepET)
                        {
                            case 1060:
                                // Employees
                                lPITypeCode = localCache.GetCodeId("E", "PERSON_INV_TYPE");
                                break;
                            case 1061:
                                // Patients
                                lPITypeCode = localCache.GetCodeId("P", "PERSON_INV_TYPE");
                                break;
                            case 1063:
                                // Witness
                                lPITypeCode = localCache.GetCodeId("W", "PERSON_INV_TYPE");
                                break;
                            case 1503:
                                // Medical staff
                                lPITypeCode = localCache.GetCodeId("MED", "PERSON_INV_TYPE");
                                break;
                            case 1086:
                                // Physician
                                lPITypeCode = localCache.GetCodeId("PHYS", "PERSON_INV_TYPE");
                                break;
                            default:
                                // Other People
                                lPITypeCode = localCache.GetCodeId("O", "PERSON_INV_TYPE");
                                break;
                        }
                    }
                    lPayeeTypeCd = localCache.GetCodeId("C", "PAYEE_TYPE");
                }

                sDTTM = Conversion.ToDbDateTime(DateTime.Now);

                // Get next unique ID's from GLOSSARY
                lRowID = Utilities.GetNextUID(Helper.gsConnectString, "ECU_HISTORY");
                lDetRowID = Utilities.GetNextUID(Helper.gsConnectString, "ECU_HISTORY_DETAIL");

                gsSql = string.Format("SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '{0}'", sTableName);
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                {
                    lTableID = dbReader.Read() ? dbReader.GetInt("TABLE_ID") : default(int);
                }

                glCnt = 0;

                gsSql = "INSERT INTO ECU_HISTORY VALUES(" + lRowID + ", " + lEntKeep + ", " + lEntKeepET + ", " + lEntChange + ", " + lEntChangeET + ", '" + sDTTM + "', '" + gsUser + "')";
                DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);

                gsSql = "SELECT " + sKeyFieldName + " AS FLDVALUE FROM " + sTableName + " WHERE " + sFieldName + " = " + lEntChange;

                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                {
                    while (dbReader.Read())
                    {
                        gsSql = "INSERT INTO ECU_HISTORY_DETAIL VALUES(" + lRowID + ", " + lDetRowID + glCnt + ", " + lTableID + ", " + dbReader.GetInt("FLDVALUE") + ", '" + sFieldName + "')";
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                        glCnt = glCnt + 1;
                    }

                    Helper.buildLogs.AppendLine("   "+ glCnt + " records updated in " + sTableName +".");
                }

                // Delete employee record if to change is an employee
                switch (lEntChangeET)
                {
                    case 1060:
                        gsSql = "DELETE EMPLOYEE WHERE EMPLOYEE_EID = " + lEntChange;
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                        gsStr = "       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " deleted from EMPLOYEE table.";
                        sLastName = GetEntityLastName(lEntChange);
                        gsSql = "DELETE ENTITY WHERE ENTITY_ID = " + lEntChange;
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                        Helper.buildLogs.AppendLine("       " + lEntChange + " - " + sLastName + " deleted from ENTITY table.");
                        break;
                    case 1611:
                    case 1086:
                        Helper.buildLogs.AppendLine(gsStr = "       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " marked as deleted in ENTITY table.");
                        gsSql = "DELETE PHYSICIAN WHERE PHYS_EID = " + lEntChange;
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);

                        Helper.buildLogs.AppendLine("    Deleted " + lEntChange + " from PHYSICIAN table to avoid duplicates");
                        break;
                    case 1613:
                    case 1601:
                        Helper.buildLogs.AppendLine("       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " marked as deleted in ENTITY table.");
                        gsSql = "DELETE MED_STAFF WHERE STAFF_EID = " + lEntChange;
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                        Helper.buildLogs.AppendLine("    Deleted " + lEntChange + " from MED_STAFF table to avoid duplicates");
                        break;
                }

                //'Set the proper employee_number for person_involved records that where changed
                if (sTableName.Equals("PERSON_INVOLVED", StringComparison.OrdinalIgnoreCase))
                {
                    gsStr = fGetEmployeeNumber(lEntKeep);
                    gsSql = string.Format("UPDATE PERSON_INVOLVED SET EMPLOYEE_NUMBER = '{0}' WHERE PI_EID = {1} AND PI_TYPE_CODE = 237 ", gsStr, lEntChange);
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);

                    Helper.buildLogs.AppendLine("       Changed PI_TYPE_CODE to " + lPITypeCode + " for " + lEntKeep);

                    

                    // If keep and delete entities are preset on same event then delete the records from Persen_involved 
                    // where event are common and entity_id is of deleted entity.
                    //This case is handeled below in switch case
                    //gsSql = string.Format("DELETE FROM PERSON_INVOLVED WHERE EVENT_ID IN (SELECT EVENT_ID FROM PERSON_INVOLVED WHERE PI_EID = {0}) AND PI_EID = {1}", lEntKeep, lEntChange);
                    //DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                }

                try
                {
                    gsSql = "UPDATE " + sTableName + " SET " + sFieldName + " = " + lEntKeep + " WHERE " + sFieldName + " = " + lEntChange;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                }
                catch (Exception ex)
                {



                    switch (sTableName)
                    {
                        case "CLAIMANT":
                            gsSql = "SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID = " + lEntChange + " OR CLAIMANT_EID = " + lEntKeep + " GROUP BY CLAIM_ID HAVING COUNT(CLAIM_ID) > 1 ";
                            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                            {
                                while (dbReader.Read())
                                {
                                    lClaimID = dbReader.GetInt("CLAIM_ID");
                                    gsSql = "DELETE CLAIMANT WHERE CLAIM_ID = " + lClaimID + " AND CLAIMANT_EID = " + lEntChange;
                                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                                    Helper.buildLogs.AppendLine("    Deleted " + lEntChange + " from CLAIMANT table to avoid duplicates for CLAIM_ID " + lClaimID );
               
                                }
                            }
                            break;
                        case "PHYSICIAN":
                            Helper.buildLogs.AppendLine("       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " marked as deleted in ENTITY table.");
                            gsSql = "DELETE PHYSICIAN WHERE PHYS_EID = " + lEntChange;
                            DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);

                            Helper.buildLogs.AppendLine("    Deleted " + lEntChange + " from PHYSICIAN table to avoid duplicates");
                            break;
                        case "MED_STAFF":
                            Helper.buildLogs.AppendLine("       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " marked as deleted in ENTITY table.");
                            gsSql = "DELETE MED_STAFF WHERE STAFF_EID = " + lEntChange;
                            DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                            Helper.buildLogs.AppendLine("    Deleted " + lEntChange + " from MED_STAFF table to avoid duplicates");

                            break;
                        case "PERSON_INVOLVED":
                            gsSql = "SELECT EVENT_ID FROM PERSON_INVOLVED WHERE PI_EID = " + lEntChange + " OR PI_EID = " + lEntKeep + " " + "GROUP BY EVENT_ID HAVING COUNT(EVENT_ID) > 1 ";
                            using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                            {
                                while (dbReader.Read())
                                {
                                    lEventID = dbReader.GetInt("EVENT_ID");
                                    gsSql = "DELETE PERSON_INVOLVED WHERE EVENT_ID = " + lEventID + " AND PI_EID = " + lEntChange;
                                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                                    Helper.buildLogs.AppendLine("       Deleted " + lEntChange + " from PERSON_INVOLVED table to  avoid duplicates for EVENT_ID " + lEventID +".");

                                }
                                gsSql = "UPDATE " + sTableName + " SET " + sFieldName + " = " + lEntKeep + " WHERE " + sFieldName + " = " + lEntChange;
                                DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                            }
                            break;
                    }
                }
                // Mark entity as deleted in Entity table
                if (!(lEntChangeET == 1060))
                {
                    gsSql = "UPDATE ENTITY SET DELETED_FLAG = -1 WHERE ENTITY_ID = " + lEntChange;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                    if (sTableName != "ADJUST_DATED_TEXT" && sTableName != "CLAIM_ADJUSTER" && sTableName != "CLAIM_ADJ_SUPP" && lEntChangeET == 1047 && lEntKeepET != 1047 && fInTable(lEntChange, "CLAIM_ADJUSTER", "ADJUSTER_EID"))
                    {
                        DbFactory.ExecuteNonQuery(Helper.gsConnectString, "UPDATE ENTITY SET DELETED_FLAG = 0 WHERE ENTITY_ID = " + lEntChange);
                         Helper.buildLogs.AppendLine("       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " cannot be deleted in ENTITY table since it is a adjuster and new entity is not an adjuster.");
                    }
                    else
                    {
                         Helper.buildLogs.AppendLine("       " + lEntChange + " - " + GetEntityLastName(lEntChange) + " marked as deleted in ENTITY table.");
                    }
                }

                // If FUNDS table and keep is a claimant then change PAYEE_TYPE_CODE
                if (sTableName == "FUNDS")
                {
                    gsSql = "UPDATE FUNDS " + "SET PAYEE_TYPE_CODE = " + lPayeeTypeCd + " " + "WHERE CLAIMANT_EID = PAYEE_EID AND CLAIMANT_EID = " + lEntKeep;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                    Helper.buildLogs.AppendLine( "       Changed PAYEE_TYPE_CODE to 'Claimant' where applicable.");
                }

                // If PERSON_INVOLVED table test for valid PI_TYPE_CODE
                if (sTableName == "PERSON_INVOLVED" && !(lPITypeCode == 0))
                {
                    gsSql = "UPDATE PERSON_INVOLVED " + "SET PI_TYPE_CODE = " + lPITypeCode + " " + "WHERE PI_EID = " + lEntKeep;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                    Helper.buildLogs.AppendLine("       Changed PAYEE_TYPE_CODE to 'Claimant' where applicable.");
                }
                else if (sTableName == "PERSON_INVOLVED" && lPITypeCode == 0)
                {
                    gsSql = "DELETE PERSON_INVOLVED " + "WHERE PI_EID = " + lEntKeep;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                }

                // Change TAX_ID mask, if necessary for kept entity
                gsSql = "SELECT TAX_ID, ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID = " + lEntKeep;
                using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                {
                    if (dbReader.Read())
                    {
                        lTableID = dbReader.GetInt("ENTITY_TABLE_ID");
                        gsStr = dbReader.GetString("TAX_ID");
                    }
                }

                if (gsStr.Length > 8)
                {
                    gsSql = "SELECT GLOSSARY_TYPE_CODE FROM GLOSSARY WHERE TABLE_ID = " + lTableID;
                    using (DbReader dbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                    {
                        if (dbReader.Read())
                        {
                            iGlossType = dbReader.GetInt("GLOSSARY_TYPE_CODE");
                        }
                    }

                    gsSql = "UPDATE ENTITY SET TAX_ID = '" + fFormatTaxID(gsStr, iGlossType) + "' WHERE ENTITY_ID = " + lEntKeep;
                    DbFactory.ExecuteNonQuery(Helper.gsConnectString, gsSql);
                }

                // Update payee name in FUNDS table if option is selected on settings form
                if (gobjSettings.ChangePAYEEName && (sTableName.Equals("FUNDS", StringComparison.OrdinalIgnoreCase) || (sTableName.Equals("FUNDS_AUTO", StringComparison.OrdinalIgnoreCase))))
                {
                    gsSql = string.Format("SELECT TAX_ID, LAST_NAME, FIRST_NAME, ADDR1, ADDR2, CITY, STATE_ID, ZIP_CODE FROM ENTITY WHERE ENTITY_ID = {0}", lEntKeep);
                    using (DbReader objDbReader = DbFactory.ExecuteReader(Helper.gsConnectString, gsSql))
                    {
                        if (objDbReader.Read())
                        {
                            sLastName = objDbReader.GetString("LAST_NAME");
                            sFirstname = objDbReader.GetString("FIRST_NAME");
                            sAddr1 = objDbReader.GetString("ADDR1");
                            sAddr2 = objDbReader.GetString("addr2");
                            sCity = objDbReader.GetString("City");
                            lStateID = objDbReader.GetInt("STATE_ID");
                            sZip = objDbReader.GetString("ZIP_CODE");
                        }
                    }

                    DbWriter objWriter = DbFactory.GetDbWriter(Helper.gsConnectString);
                    objWriter.Tables.Add(sTableName);
                    objWriter.Fields.Add("LAST_NAME", sLastName);
                    objWriter.Fields.Add("FIRST_NAME", sFirstname);
                    objWriter.Fields.Add("ADDR1", sAddr1);
                    objWriter.Fields.Add("ADDR2", sAddr2);
                    objWriter.Fields.Add("CITY", sCity);
                    objWriter.Fields.Add("STATE_ID", lStateID);
                    objWriter.Fields.Add("ZIP_CODE", sZip);
                    objWriter.Where.Add(string.Format("PAYEE_EID = {0}", lEntKeep));
                    objWriter.Execute();

                    if (!object.ReferenceEquals(objWriter, default(DbWriter)))
                    {
                        objWriter = null;
                    }
                }
               
                gsStr = string.Empty;
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
            return 0;
        }

        /// <summary>
        /// Initalizes the specified o user login.
        /// </summary>
        /// <param name="oUserLogin">The o user login.</param>
        private static void Initalize(UserLogin oUserLogin)
        {
            try
            {
                DSN = oUserLogin.DatabaseId.ToString();
                gsDBOUserId = oUserLogin.objRiskmasterDatabase.RMUserId;
                gsConnectString = oUserLogin.objRiskmasterDatabase.ConnectionString;
                gsDatabaseType = DbFactory.GetDatabaseType(gsConnectString);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Wases the database upgraded.
        /// </summary>
        /// <returns></returns>
        private static bool WasDatabaseUpgraded()
        {
            string sql = "SELECT ECU_TC_ROW_ID FROM ECU_TABLE_CHECK WHERE ECU_TC_ROW_ID = -123";
            bool isDatabaseUpgraded = default(bool);
            try
            {
                using (DbReader objDbReader = DbFactory.GetDbReader(Helper.gsConnectString, sql))
                {
                    isDatabaseUpgraded = !object.ReferenceEquals(objDbReader, default(DbReader));
                }
            }
            catch
            {
                isDatabaseUpgraded = false;
            }
            return isDatabaseUpgraded;
        }

        #region AddtoExecutionLogFile
        private static void AddtoExecutionLogFile(StringBuilder Message)
        {
            try
            {
                StreamWriter writer;
                string LogPath = Path.GetDirectoryName(Application.ExecutablePath);
                string filename = ConfigurationManager.AppSettings["LogFileName"].ToString();
                string path = LogPath + filename;
                if (File.Exists(path))
                {
                    writer = File.AppendText(path);
                }
                else
                {
                    writer = File.CreateText(path);
                }


                writer.WriteLine("-------------------START-------------" + DateTime.Now);                
                writer.WriteLine(Message);
                writer.WriteLine("-------------------END-------------" + DateTime.Now);
                writer.Close();
                // }
            }
            catch (FileNotFoundException fnf)
            {
                ExceptionLogger.AddtoLogFile(fnf);
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);

            }
        }

        #endregion
    }
}