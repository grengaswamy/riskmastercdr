﻿namespace Entity_Cleanup_Utility
{
    partial class frmEntityCleanUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblEntity = new System.Windows.Forms.Label();
            this.ctlTabs = new System.Windows.Forms.TabControl();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLastN = new System.Windows.Forms.TextBox();
            this.rb_Entity_Srch_TaxIDLN = new System.Windows.Forms.RadioButton();
            this.rb_Entity_Srch_LNFN = new System.Windows.Forms.RadioButton();
            this.rb_Entity_Srch_LNS = new System.Windows.Forms.RadioButton();
            this.rb_Entity_Srch_TaxId = new System.Windows.Forms.RadioButton();
            this.gbEmployeeType = new System.Windows.Forms.GroupBox();
            this.cmb_entityType = new System.Windows.Forms.ComboBox();
            this.rb_Entity_srch_byEntType = new System.Windows.Forms.RadioButton();
            this.rb_Entity_srch_allEnt = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.ctlPRGrid = new System.Windows.Forms.DataGridView();
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.datetimepicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblCriteriaDet = new System.Windows.Forms.Label();
            this.lblStateZip = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtBirthDate = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailCity = new System.Windows.Forms.Label();
            this.lblPayeeDetailEMail = new System.Windows.Forms.Label();
            this.lblPayeeDetailTaxId = new System.Windows.Forms.Label();
            this.lblPayeeDetailEntityType = new System.Windows.Forms.Label();
            this.cmbET = new System.Windows.Forms.ComboBox();
            this.txtEmailDet = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtAddr2 = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailAddress = new System.Windows.Forms.Label();
            this.txtAddr1 = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailName = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtHomPhn = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailPhone = new System.Windows.Forms.Label();
            this.txtOffPhn = new System.Windows.Forms.TextBox();
            this.lblPayeeDetailAbbr = new System.Windows.Forms.Label();
            this.txtAbbr = new System.Windows.Forms.TextBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.ctlPGridDet = new System.Windows.Forms.DataGridView();
            this.Keep = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Del = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.S1099 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PDLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntityType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDEntType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEID = new System.Windows.Forms.TextBox();
            this.txtStateID = new System.Windows.Forms.TextBox();
            this.txtSexID = new System.Windows.Forms.TextBox();
            this.gLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gTaxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoofDuplicates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityTableId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ctlTabs.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbEmployeeType.SuspendLayout();
            this.tabResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).BeginInit();
            this.tabDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(517, 406);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblEntity
            // 
            this.lblEntity.AutoSize = true;
            this.lblEntity.BackColor = System.Drawing.SystemColors.Control;
            this.lblEntity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntity.ForeColor = System.Drawing.Color.Maroon;
            this.lblEntity.Location = new System.Drawing.Point(26, 16);
            this.lblEntity.Name = "lblEntity";
            this.lblEntity.Size = new System.Drawing.Size(120, 17);
            this.lblEntity.TabIndex = 1;
            this.lblEntity.Text = "Entity Clean Up";
            // 
            // ctlTabs
            // 
            this.ctlTabs.Controls.Add(this.tabSearch);
            this.ctlTabs.Controls.Add(this.tabResults);
            this.ctlTabs.Controls.Add(this.tabDetail);
            this.ctlTabs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctlTabs.Location = new System.Drawing.Point(24, 39);
            this.ctlTabs.Name = "ctlTabs";
            this.ctlTabs.SelectedIndex = 0;
            this.ctlTabs.Size = new System.Drawing.Size(495, 369);
            this.ctlTabs.TabIndex = 2;
            this.ctlTabs.Selected += new System.Windows.Forms.TabControlEventHandler(this.ctlTabs_Selected);
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.groupBox1);
            this.tabSearch.Controls.Add(this.gbEmployeeType);
            this.tabSearch.Controls.Add(this.btnSearch);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(487, 343);
            this.tabSearch.TabIndex = 0;
            this.tabSearch.Text = "1. Search";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLastN);
            this.groupBox1.Controls.Add(this.rb_Entity_Srch_TaxIDLN);
            this.groupBox1.Controls.Add(this.rb_Entity_Srch_LNFN);
            this.groupBox1.Controls.Add(this.rb_Entity_Srch_LNS);
            this.groupBox1.Controls.Add(this.rb_Entity_Srch_TaxId);
            this.groupBox1.Location = new System.Drawing.Point(7, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 77);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search for duplicates using";
            // 
            // txtLastN
            // 
            this.txtLastN.Location = new System.Drawing.Point(316, 47);
            this.txtLastN.Name = "txtLastN";
            this.txtLastN.Size = new System.Drawing.Size(136, 21);
            this.txtLastN.TabIndex = 4;
            this.txtLastN.Visible = false;
            // 
            // rb_Entity_Srch_TaxIDLN
            // 
            this.rb_Entity_Srch_TaxIDLN.AutoSize = true;
            this.rb_Entity_Srch_TaxIDLN.Location = new System.Drawing.Point(169, 25);
            this.rb_Entity_Srch_TaxIDLN.Name = "rb_Entity_Srch_TaxIDLN";
            this.rb_Entity_Srch_TaxIDLN.Size = new System.Drawing.Size(132, 17);
            this.rb_Entity_Srch_TaxIDLN.TabIndex = 8;
            this.rb_Entity_Srch_TaxIDLN.Text = "Tax ID, Last Name";
            this.rb_Entity_Srch_TaxIDLN.UseVisualStyleBackColor = true;
            // 
            // rb_Entity_Srch_LNFN
            // 
            this.rb_Entity_Srch_LNFN.AutoSize = true;
            this.rb_Entity_Srch_LNFN.Location = new System.Drawing.Point(14, 48);
            this.rb_Entity_Srch_LNFN.Name = "rb_Entity_Srch_LNFN";
            this.rb_Entity_Srch_LNFN.Size = new System.Drawing.Size(150, 17);
            this.rb_Entity_Srch_LNFN.TabIndex = 7;
            this.rb_Entity_Srch_LNFN.Text = "Last Name,First Name";
            this.rb_Entity_Srch_LNFN.UseVisualStyleBackColor = true;
            // 
            // rb_Entity_Srch_LNS
            // 
            this.rb_Entity_Srch_LNS.AutoSize = true;
            this.rb_Entity_Srch_LNS.Location = new System.Drawing.Point(169, 48);
            this.rb_Entity_Srch_LNS.Name = "rb_Entity_Srch_LNS";
            this.rb_Entity_Srch_LNS.Size = new System.Drawing.Size(143, 17);
            this.rb_Entity_Srch_LNS.TabIndex = 6;
            this.rb_Entity_Srch_LNS.Text = "Last Name (Specific)";
            this.rb_Entity_Srch_LNS.UseVisualStyleBackColor = true;
            this.rb_Entity_Srch_LNS.CheckedChanged += new System.EventHandler(this.rb_Entity_Srch_LNS_CheckedChanged);
            // 
            // rb_Entity_Srch_TaxId
            // 
            this.rb_Entity_Srch_TaxId.AutoSize = true;
            this.rb_Entity_Srch_TaxId.Checked = true;
            this.rb_Entity_Srch_TaxId.Location = new System.Drawing.Point(14, 25);
            this.rb_Entity_Srch_TaxId.Name = "rb_Entity_Srch_TaxId";
            this.rb_Entity_Srch_TaxId.Size = new System.Drawing.Size(64, 17);
            this.rb_Entity_Srch_TaxId.TabIndex = 5;
            this.rb_Entity_Srch_TaxId.TabStop = true;
            this.rb_Entity_Srch_TaxId.Text = "Tax ID";
            this.rb_Entity_Srch_TaxId.UseVisualStyleBackColor = true;
            // 
            // gbEmployeeType
            // 
            this.gbEmployeeType.Controls.Add(this.cmb_entityType);
            this.gbEmployeeType.Controls.Add(this.rb_Entity_srch_byEntType);
            this.gbEmployeeType.Controls.Add(this.rb_Entity_srch_allEnt);
            this.gbEmployeeType.Location = new System.Drawing.Point(7, 7);
            this.gbEmployeeType.Name = "gbEmployeeType";
            this.gbEmployeeType.Size = new System.Drawing.Size(467, 69);
            this.gbEmployeeType.TabIndex = 1;
            this.gbEmployeeType.TabStop = false;
            this.gbEmployeeType.Text = "Type";
            // 
            // cmb_entityType
            // 
            this.cmb_entityType.BackColor = System.Drawing.SystemColors.Info;
            this.cmb_entityType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmb_entityType.FormattingEnabled = true;
            this.cmb_entityType.Location = new System.Drawing.Point(279, 38);
            this.cmb_entityType.Name = "cmb_entityType";
            this.cmb_entityType.Size = new System.Drawing.Size(157, 21);
            this.cmb_entityType.TabIndex = 2;
            this.cmb_entityType.Visible = false;
            // 
            // rb_Entity_srch_byEntType
            // 
            this.rb_Entity_srch_byEntType.AutoSize = true;
            this.rb_Entity_srch_byEntType.Location = new System.Drawing.Point(13, 43);
            this.rb_Entity_srch_byEntType.Name = "rb_Entity_srch_byEntType";
            this.rb_Entity_srch_byEntType.Size = new System.Drawing.Size(107, 17);
            this.rb_Entity_srch_byEntType.TabIndex = 1;
            this.rb_Entity_srch_byEntType.Text = "by Entity Type";
            this.rb_Entity_srch_byEntType.UseVisualStyleBackColor = true;
            this.rb_Entity_srch_byEntType.CheckedChanged += new System.EventHandler(this.rb_Entity_srch_byEntType_CheckedChanged);
            // 
            // rb_Entity_srch_allEnt
            // 
            this.rb_Entity_srch_allEnt.AutoSize = true;
            this.rb_Entity_srch_allEnt.Checked = true;
            this.rb_Entity_srch_allEnt.Location = new System.Drawing.Point(13, 20);
            this.rb_Entity_srch_allEnt.Name = "rb_Entity_srch_allEnt";
            this.rb_Entity_srch_allEnt.Size = new System.Drawing.Size(83, 17);
            this.rb_Entity_srch_allEnt.TabIndex = 0;
            this.rb_Entity_srch_allEnt.TabStop = true;
            this.rb_Entity_srch_allEnt.Text = "all Entities";
            this.rb_Entity_srch_allEnt.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(385, 315);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.lblCriteria);
            this.tabResults.Controls.Add(this.btnDisplay);
            this.tabResults.Controls.Add(this.ctlPRGrid);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(487, 343);
            this.tabResults.TabIndex = 1;
            this.tabResults.Text = "2. Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteria.Location = new System.Drawing.Point(18, 6);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(50, 13);
            this.lblCriteria.TabIndex = 4;
            this.lblCriteria.Text = "Criteria";
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(391, 315);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(75, 23);
            this.btnDisplay.TabIndex = 1;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // ctlPRGrid
            // 
            this.ctlPRGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPRGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ctlPRGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ctlPRGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPRGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gLastName,
            this.gFirstName,
            this.gTaxID,
            this.EntityType,
            this.NoofDuplicates,
            this.EntityTableId});
            this.ctlPRGrid.Location = new System.Drawing.Point(19, 26);
            this.ctlPRGrid.MultiSelect = false;
            this.ctlPRGrid.Name = "ctlPRGrid";
            this.ctlPRGrid.Size = new System.Drawing.Size(447, 283);
            this.ctlPRGrid.TabIndex = 0;
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.cmbState);
            this.tabDetail.Controls.Add(this.datetimepicker1);
            this.tabDetail.Controls.Add(this.btnSave);
            this.tabDetail.Controls.Add(this.btnEdit);
            this.tabDetail.Controls.Add(this.lblCriteriaDet);
            this.tabDetail.Controls.Add(this.lblStateZip);
            this.tabDetail.Controls.Add(this.label2);
            this.tabDetail.Controls.Add(this.label1);
            this.tabDetail.Controls.Add(this.txtAge);
            this.tabDetail.Controls.Add(this.txtBirthDate);
            this.tabDetail.Controls.Add(this.lblPayeeDetailCity);
            this.tabDetail.Controls.Add(this.lblPayeeDetailEMail);
            this.tabDetail.Controls.Add(this.lblPayeeDetailTaxId);
            this.tabDetail.Controls.Add(this.lblPayeeDetailEntityType);
            this.tabDetail.Controls.Add(this.cmbET);
            this.tabDetail.Controls.Add(this.txtEmailDet);
            this.tabDetail.Controls.Add(this.txttax);
            this.tabDetail.Controls.Add(this.txtZip);
            this.tabDetail.Controls.Add(this.txtCity);
            this.tabDetail.Controls.Add(this.txtAddr2);
            this.tabDetail.Controls.Add(this.lblPayeeDetailAddress);
            this.tabDetail.Controls.Add(this.txtAddr1);
            this.tabDetail.Controls.Add(this.txtFirstName);
            this.tabDetail.Controls.Add(this.lblPayeeDetailName);
            this.tabDetail.Controls.Add(this.txtLastName);
            this.tabDetail.Controls.Add(this.txtHomPhn);
            this.tabDetail.Controls.Add(this.lblPayeeDetailPhone);
            this.tabDetail.Controls.Add(this.txtOffPhn);
            this.tabDetail.Controls.Add(this.lblPayeeDetailAbbr);
            this.tabDetail.Controls.Add(this.txtAbbr);
            this.tabDetail.Controls.Add(this.btnProcess);
            this.tabDetail.Controls.Add(this.ctlPGridDet);
            this.tabDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDetail.Location = new System.Drawing.Point(4, 22);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Size = new System.Drawing.Size(487, 343);
            this.tabDetail.TabIndex = 2;
            this.tabDetail.Text = "3. Detail";
            this.tabDetail.UseVisualStyleBackColor = true;
            // 
            // cmbState
            // 
            this.cmbState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbState.Enabled = false;
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(52, 283);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(45, 21);
            this.cmbState.TabIndex = 64;
            // 
            // datetimepicker1
            // 
            this.datetimepicker1.CustomFormat = "yyyyMMdd";
            this.datetimepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimepicker1.Location = new System.Drawing.Point(23, 315);
            this.datetimepicker1.Name = "datetimepicker1";
            this.datetimepicker1.Size = new System.Drawing.Size(77, 20);
            this.datetimepicker1.TabIndex = 63;
            this.datetimepicker1.Visible = false;
            this.datetimepicker1.CloseUp += new System.EventHandler(this.datetimepicker1_CloseUp);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(298, 317);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 21);
            this.btnSave.TabIndex = 62;
            this.btnSave.Text = "Save Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(216, 317);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(72, 21);
            this.btnEdit.TabIndex = 61;
            this.btnEdit.Text = "Edit Entity";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblCriteriaDet
            // 
            this.lblCriteriaDet.AutoSize = true;
            this.lblCriteriaDet.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteriaDet.Location = new System.Drawing.Point(13, 2);
            this.lblCriteriaDet.Name = "lblCriteriaDet";
            this.lblCriteriaDet.Size = new System.Drawing.Size(39, 13);
            this.lblCriteriaDet.TabIndex = 60;
            this.lblCriteriaDet.Text = "Criteria";
            // 
            // lblStateZip
            // 
            this.lblStateZip.AutoSize = true;
            this.lblStateZip.Location = new System.Drawing.Point(9, 290);
            this.lblStateZip.Name = "lblStateZip";
            this.lblStateZip.Size = new System.Drawing.Size(40, 13);
            this.lblStateZip.TabIndex = 58;
            this.lblStateZip.Text = "St/Zip:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(373, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Age :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Birth Date :";
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAge.Enabled = false;
            this.txtAge.Location = new System.Drawing.Point(414, 281);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(43, 20);
            this.txtAge.TabIndex = 55;
            // 
            // txtBirthDate
            // 
            this.txtBirthDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtBirthDate.Enabled = false;
            this.txtBirthDate.Location = new System.Drawing.Point(282, 282);
            this.txtBirthDate.Name = "txtBirthDate";
            this.txtBirthDate.Size = new System.Drawing.Size(71, 20);
            this.txtBirthDate.TabIndex = 54;
            // 
            // lblPayeeDetailCity
            // 
            this.lblPayeeDetailCity.AutoSize = true;
            this.lblPayeeDetailCity.Location = new System.Drawing.Point(20, 266);
            this.lblPayeeDetailCity.Name = "lblPayeeDetailCity";
            this.lblPayeeDetailCity.Size = new System.Drawing.Size(27, 13);
            this.lblPayeeDetailCity.TabIndex = 53;
            this.lblPayeeDetailCity.Text = "City:";
            // 
            // lblPayeeDetailEMail
            // 
            this.lblPayeeDetailEMail.AutoSize = true;
            this.lblPayeeDetailEMail.Location = new System.Drawing.Point(237, 262);
            this.lblPayeeDetailEMail.Name = "lblPayeeDetailEMail";
            this.lblPayeeDetailEMail.Size = new System.Drawing.Size(38, 13);
            this.lblPayeeDetailEMail.TabIndex = 52;
            this.lblPayeeDetailEMail.Text = "Email :";
            // 
            // lblPayeeDetailTaxId
            // 
            this.lblPayeeDetailTaxId.AutoSize = true;
            this.lblPayeeDetailTaxId.Location = new System.Drawing.Point(206, 195);
            this.lblPayeeDetailTaxId.Name = "lblPayeeDetailTaxId";
            this.lblPayeeDetailTaxId.Size = new System.Drawing.Size(45, 13);
            this.lblPayeeDetailTaxId.TabIndex = 51;
            this.lblPayeeDetailTaxId.Text = "Tax ID :";
            // 
            // lblPayeeDetailEntityType
            // 
            this.lblPayeeDetailEntityType.AutoSize = true;
            this.lblPayeeDetailEntityType.Location = new System.Drawing.Point(206, 174);
            this.lblPayeeDetailEntityType.Name = "lblPayeeDetailEntityType";
            this.lblPayeeDetailEntityType.Size = new System.Drawing.Size(66, 13);
            this.lblPayeeDetailEntityType.TabIndex = 50;
            this.lblPayeeDetailEntityType.Text = "Entity Type :";
            // 
            // cmbET
            // 
            this.cmbET.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmbET.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbET.Enabled = false;
            this.cmbET.FormattingEnabled = true;
            this.cmbET.Location = new System.Drawing.Point(282, 170);
            this.cmbET.Name = "cmbET";
            this.cmbET.Size = new System.Drawing.Size(188, 21);
            this.cmbET.TabIndex = 49;
            // 
            // txtEmailDet
            // 
            this.txtEmailDet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmailDet.Enabled = false;
            this.txtEmailDet.Location = new System.Drawing.Point(281, 258);
            this.txtEmailDet.Name = "txtEmailDet";
            this.txtEmailDet.Size = new System.Drawing.Size(189, 20);
            this.txtEmailDet.TabIndex = 48;
            // 
            // txttax
            // 
            this.txttax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txttax.Enabled = false;
            this.txttax.Location = new System.Drawing.Point(282, 191);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(189, 20);
            this.txttax.TabIndex = 47;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtZip.Enabled = false;
            this.txtZip.Location = new System.Drawing.Point(102, 284);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(92, 20);
            this.txtZip.TabIndex = 46;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(50, 261);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(148, 20);
            this.txtCity.TabIndex = 45;
            // 
            // txtAddr2
            // 
            this.txtAddr2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr2.Enabled = false;
            this.txtAddr2.Location = new System.Drawing.Point(51, 237);
            this.txtAddr2.Name = "txtAddr2";
            this.txtAddr2.Size = new System.Drawing.Size(301, 20);
            this.txtAddr2.TabIndex = 44;
            // 
            // lblPayeeDetailAddress
            // 
            this.lblPayeeDetailAddress.AutoSize = true;
            this.lblPayeeDetailAddress.Location = new System.Drawing.Point(2, 218);
            this.lblPayeeDetailAddress.Name = "lblPayeeDetailAddress";
            this.lblPayeeDetailAddress.Size = new System.Drawing.Size(48, 13);
            this.lblPayeeDetailAddress.TabIndex = 43;
            this.lblPayeeDetailAddress.Text = "Address:";
            // 
            // txtAddr1
            // 
            this.txtAddr1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAddr1.Enabled = false;
            this.txtAddr1.Location = new System.Drawing.Point(52, 214);
            this.txtAddr1.Name = "txtAddr1";
            this.txtAddr1.Size = new System.Drawing.Size(301, 20);
            this.txtAddr1.TabIndex = 42;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFirstName.Enabled = false;
            this.txtFirstName.Location = new System.Drawing.Point(52, 192);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(148, 20);
            this.txtFirstName.TabIndex = 41;
            // 
            // lblPayeeDetailName
            // 
            this.lblPayeeDetailName.AutoSize = true;
            this.lblPayeeDetailName.Location = new System.Drawing.Point(12, 174);
            this.lblPayeeDetailName.Name = "lblPayeeDetailName";
            this.lblPayeeDetailName.Size = new System.Drawing.Size(38, 13);
            this.lblPayeeDetailName.TabIndex = 40;
            this.lblPayeeDetailName.Text = "Name:";
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtLastName.Enabled = false;
            this.txtLastName.Location = new System.Drawing.Point(52, 170);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(148, 20);
            this.txtLastName.TabIndex = 39;
            // 
            // txtHomPhn
            // 
            this.txtHomPhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtHomPhn.Enabled = false;
            this.txtHomPhn.Location = new System.Drawing.Point(359, 149);
            this.txtHomPhn.Name = "txtHomPhn";
            this.txtHomPhn.Size = new System.Drawing.Size(111, 20);
            this.txtHomPhn.TabIndex = 38;
            // 
            // lblPayeeDetailPhone
            // 
            this.lblPayeeDetailPhone.AutoSize = true;
            this.lblPayeeDetailPhone.Location = new System.Drawing.Point(206, 153);
            this.lblPayeeDetailPhone.Name = "lblPayeeDetailPhone";
            this.lblPayeeDetailPhone.Size = new System.Drawing.Size(73, 13);
            this.lblPayeeDetailPhone.TabIndex = 37;
            this.lblPayeeDetailPhone.Text = "Ph Off/Home:";
            // 
            // txtOffPhn
            // 
            this.txtOffPhn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtOffPhn.Enabled = false;
            this.txtOffPhn.Location = new System.Drawing.Point(281, 149);
            this.txtOffPhn.Name = "txtOffPhn";
            this.txtOffPhn.Size = new System.Drawing.Size(72, 20);
            this.txtOffPhn.TabIndex = 36;
            // 
            // lblPayeeDetailAbbr
            // 
            this.lblPayeeDetailAbbr.AutoSize = true;
            this.lblPayeeDetailAbbr.Location = new System.Drawing.Point(13, 152);
            this.lblPayeeDetailAbbr.Name = "lblPayeeDetailAbbr";
            this.lblPayeeDetailAbbr.Size = new System.Drawing.Size(32, 13);
            this.lblPayeeDetailAbbr.TabIndex = 35;
            this.lblPayeeDetailAbbr.Text = "Abbr:";
            // 
            // txtAbbr
            // 
            this.txtAbbr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtAbbr.Enabled = false;
            this.txtAbbr.Location = new System.Drawing.Point(52, 148);
            this.txtAbbr.Name = "txtAbbr";
            this.txtAbbr.Size = new System.Drawing.Size(148, 20);
            this.txtAbbr.TabIndex = 34;
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(394, 317);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 22;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // ctlPGridDet
            // 
            this.ctlPGridDet.AllowUserToAddRows = false;
            this.ctlPGridDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPGridDet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Keep,
            this.Del,
            this.S1099,
            this.PDLastName,
            this.PDFirstName,
            this.PDEntityID,
            this.PDEntityType,
            this.PDEntType});
            this.ctlPGridDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ctlPGridDet.Location = new System.Drawing.Point(9, 19);
            this.ctlPGridDet.MultiSelect = false;
            this.ctlPGridDet.Name = "ctlPGridDet";
            this.ctlPGridDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ctlPGridDet.Size = new System.Drawing.Size(475, 117);
            this.ctlPGridDet.TabIndex = 0;
            this.ctlPGridDet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlPGridDet_CellClick);
            // 
            // Keep
            // 
            this.Keep.FalseValue = "0";
            this.Keep.HeaderText = "Keep";
            this.Keep.Name = "Keep";
            this.Keep.TrueValue = "-1";
            this.Keep.Width = 45;
            // 
            // Del
            // 
            this.Del.FalseValue = "0";
            this.Del.HeaderText = "Del";
            this.Del.Name = "Del";
            this.Del.TrueValue = "-1";
            this.Del.Width = 45;
            // 
            // S1099
            // 
            this.S1099.FalseValue = "0";
            this.S1099.HeaderText = "1099";
            this.S1099.Name = "S1099";
            this.S1099.ReadOnly = true;
            this.S1099.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.S1099.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.S1099.TrueValue = "-1";
            this.S1099.Width = 45;
            // 
            // PDLastName
            // 
            this.PDLastName.HeaderText = "Last Name";
            this.PDLastName.Name = "PDLastName";
            this.PDLastName.ReadOnly = true;
            // 
            // PDFirstName
            // 
            this.PDFirstName.HeaderText = "First Name";
            this.PDFirstName.Name = "PDFirstName";
            this.PDFirstName.ReadOnly = true;
            // 
            // PDEntityID
            // 
            this.PDEntityID.HeaderText = "Entity ID";
            this.PDEntityID.Name = "PDEntityID";
            this.PDEntityID.ReadOnly = true;
            // 
            // PDEntityType
            // 
            this.PDEntityType.HeaderText = "Type";
            this.PDEntityType.Name = "PDEntityType";
            this.PDEntityType.ReadOnly = true;
            this.PDEntityType.Visible = false;
            // 
            // PDEntType
            // 
            this.PDEntType.HeaderText = "Ent Type";
            this.PDEntType.Name = "PDEntType";
            this.PDEntType.ReadOnly = true;
            // 
            // txtEID
            // 
            this.txtEID.Location = new System.Drawing.Point(13, 424);
            this.txtEID.Name = "txtEID";
            this.txtEID.Size = new System.Drawing.Size(43, 20);
            this.txtEID.TabIndex = 14;
            this.txtEID.Visible = false;
            // 
            // txtStateID
            // 
            this.txtStateID.Location = new System.Drawing.Point(62, 424);
            this.txtStateID.Name = "txtStateID";
            this.txtStateID.Size = new System.Drawing.Size(43, 20);
            this.txtStateID.TabIndex = 15;
            this.txtStateID.Visible = false;
            // 
            // txtSexID
            // 
            this.txtSexID.Location = new System.Drawing.Point(111, 424);
            this.txtSexID.Name = "txtSexID";
            this.txtSexID.Size = new System.Drawing.Size(43, 20);
            this.txtSexID.TabIndex = 16;
            this.txtSexID.Visible = false;
            // 
            // gLastName
            // 
            this.gLastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gLastName.DefaultCellStyle = dataGridViewCellStyle2;
            this.gLastName.HeaderText = "Last Name";
            this.gLastName.Name = "gLastName";
            this.gLastName.ReadOnly = true;
            this.gLastName.Width = 92;
            // 
            // gFirstName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gFirstName.DefaultCellStyle = dataGridViewCellStyle3;
            this.gFirstName.HeaderText = "First Name";
            this.gFirstName.Name = "gFirstName";
            this.gFirstName.ReadOnly = true;
            this.gFirstName.Width = 120;
            // 
            // gTaxID
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gTaxID.DefaultCellStyle = dataGridViewCellStyle4;
            this.gTaxID.HeaderText = "TaxID/SSN";
            this.gTaxID.Name = "gTaxID";
            this.gTaxID.ReadOnly = true;
            this.gTaxID.Width = 120;
            // 
            // EntityType
            // 
            this.EntityType.HeaderText = "EntityType";
            this.EntityType.Name = "EntityType";
            this.EntityType.ReadOnly = true;
            // 
            // NoofDuplicates
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.NoofDuplicates.DefaultCellStyle = dataGridViewCellStyle5;
            this.NoofDuplicates.HeaderText = "Count";
            this.NoofDuplicates.Name = "NoofDuplicates";
            this.NoofDuplicates.ReadOnly = true;
            this.NoofDuplicates.Width = 60;
            // 
            // EntityTableId
            // 
            this.EntityTableId.HeaderText = "EntityTableId";
            this.EntityTableId.Name = "EntityTableId";
            this.EntityTableId.ReadOnly = true;
            this.EntityTableId.Visible = false;
            // 
            // frmEntityCleanUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(542, 452);
            this.Controls.Add(this.txtSexID);
            this.Controls.Add(this.txtStateID);
            this.Controls.Add(this.txtEID);
            this.Controls.Add(this.ctlTabs);
            this.Controls.Add(this.lblEntity);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmEntityCleanUp";
            this.Text = "frmEmployee";
            this.Load += new System.EventHandler(this.frmEntityCleanUp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ctlTabs.ResumeLayout(false);
            this.tabSearch.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbEmployeeType.ResumeLayout(false);
            this.gbEmployeeType.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.tabResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPGridDet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblEntity;
        internal System.Windows.Forms.TabControl ctlTabs;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDisplay;
        internal System.Windows.Forms.DataGridView ctlPRGrid;
        internal System.Windows.Forms.DataGridView ctlPGridDet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox txtEID;
        private System.Windows.Forms.TextBox txtStateID;
        private System.Windows.Forms.TextBox txtSexID;
        private System.Windows.Forms.GroupBox gbEmployeeType;
        private System.Windows.Forms.RadioButton rb_Entity_srch_byEntType;
        private System.Windows.Forms.RadioButton rb_Entity_srch_allEnt;
        private System.Windows.Forms.RadioButton rb_Entity_Srch_TaxIDLN;
        private System.Windows.Forms.RadioButton rb_Entity_Srch_LNFN;
        private System.Windows.Forms.RadioButton rb_Entity_Srch_LNS;
        private System.Windows.Forms.RadioButton rb_Entity_Srch_TaxId;
        private System.Windows.Forms.Label lblStateZip;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.TextBox txtBirthDate;
        private System.Windows.Forms.Label lblPayeeDetailCity;
        private System.Windows.Forms.Label lblPayeeDetailEMail;
        private System.Windows.Forms.Label lblPayeeDetailTaxId;
        private System.Windows.Forms.Label lblPayeeDetailEntityType;
        private System.Windows.Forms.ComboBox cmbET;
        private System.Windows.Forms.TextBox txtEmailDet;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtAddr2;
        private System.Windows.Forms.Label lblPayeeDetailAddress;
        private System.Windows.Forms.TextBox txtAddr1;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblPayeeDetailName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtHomPhn;
        private System.Windows.Forms.Label lblPayeeDetailPhone;
        private System.Windows.Forms.TextBox txtOffPhn;
        private System.Windows.Forms.Label lblPayeeDetailAbbr;
        private System.Windows.Forms.TextBox txtAbbr;
        private System.Windows.Forms.TextBox txtLastN;
        private System.Windows.Forms.ComboBox cmb_entityType;
        private System.Windows.Forms.Label lblCriteria;
        private System.Windows.Forms.Label lblCriteriaDet;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker datetimepicker1;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Keep;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Del;
        private System.Windows.Forms.DataGridViewCheckBoxColumn S1099;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntityType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDEntType;
        private System.Windows.Forms.DataGridViewTextBoxColumn gLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gTaxID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityType;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoofDuplicates;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityTableId;

    }
}