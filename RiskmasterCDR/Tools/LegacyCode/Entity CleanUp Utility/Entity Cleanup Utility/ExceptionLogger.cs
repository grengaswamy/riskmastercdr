﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;



namespace Entity_Cleanup_Utility
{
    /// <summary>
    /// The Class Writes Exception and Error information into a log file named ECU_ExceptionLogger.txt.
    /// </summary>
    public static class ExceptionLogger
    {
        #region Add Log to file

        /// <summary>
        /// Writes error occured in log file,if log file does not exist,it creates the file first.
        /// </summary>
        /// <param name="exception">Exception</param>
        /// 
        public static void AddtoLogFile(Exception ex)
        {
            
            StreamWriter logWriter;
            try
            {
                string LogPath = Path.GetDirectoryName(Application.ExecutablePath);
                string filename = ConfigurationManager.AppSettings["filename"].ToString();
                string path = LogPath + filename;
                if (File.Exists(path))
                {
                     logWriter = File.AppendText(path);
                }
                else
                {
                    logWriter = File.CreateText(path);
                }

                logWriter.WriteLine("------------------------------------------------------------------------------------------------");
                logWriter.WriteLine("=> Exception Encountered at---" + DateTime.Now + "\n");
                logWriter.WriteLine("=> Source :" + ex.Source + "\n");
                logWriter.WriteLine("=> Exception StackTrace : " + ex.StackTrace + "\n");
                logWriter.WriteLine("=> Exception Message : " + ex.Message + "\n");
                logWriter.WriteLine("=> Exception InnerException : " + ex.InnerException + "\n");
                logWriter.WriteLine("=> Exception Description : " + ex.ToString() + "\n");
                logWriter.WriteLine("=> End of Exception--" + "\n");               
                logWriter.WriteLine("------------------------------------------------------------------------------------------------");
                logWriter.Close();                
               
            }
            catch
            {
               // MessageBox.Show("Error ocurred while logging exception. Please contact admin."," Entity CleanUp Says:");
            }
            finally
            {
               // MessageBox.Show("Error ocurred. Please contact admin.", " Entity CleanUp Says:");
            }
        }

        #endregion
        
    }
}
