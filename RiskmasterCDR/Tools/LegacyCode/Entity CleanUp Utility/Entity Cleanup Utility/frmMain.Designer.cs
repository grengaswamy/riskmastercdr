﻿using System.Drawing;
using System.Windows.Forms;
namespace Entity_Cleanup_Utility
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolExitbutton = new System.Windows.Forms.ToolStripButton();
            this.toolPrintbutton = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.piclogo = new System.Windows.Forms.PictureBox();
            this.ctlTree = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.childPanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.piclogo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(756, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.printGridToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Image = global::Entity_Cleanup_Utility.Properties.Resources.Settings;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // printGridToolStripMenuItem
            // 
            this.printGridToolStripMenuItem.Image = global::Entity_Cleanup_Utility.Properties.Resources.print_icon;
            this.printGridToolStripMenuItem.Name = "printGridToolStripMenuItem";
            this.printGridToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.printGridToolStripMenuItem.Text = "Print Grid";
            this.printGridToolStripMenuItem.Click += new System.EventHandler(this.printGridToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Entity_Cleanup_Utility.Properties.Resources.Exit1;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Image = global::Entity_Cleanup_Utility.Properties.Resources.undo;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExitbutton,
            this.toolPrintbutton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(756, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolExitbutton
            // 
            this.toolExitbutton.Image = global::Entity_Cleanup_Utility.Properties.Resources.Exit1;
            this.toolExitbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolExitbutton.Name = "toolExitbutton";
            this.toolExitbutton.Size = new System.Drawing.Size(45, 22);
            this.toolExitbutton.Text = "Exit";
            this.toolExitbutton.Click += new System.EventHandler(this.toolExitbutton_Click);
            // 
            // toolPrintbutton
            // 
            this.toolPrintbutton.Image = global::Entity_Cleanup_Utility.Properties.Resources.print_icon;
            this.toolPrintbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolPrintbutton.Name = "toolPrintbutton";
            this.toolPrintbutton.Size = new System.Drawing.Size(77, 22);
            this.toolPrintbutton.Text = "Print Grid";
            this.toolPrintbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolPrintbutton.Click += new System.EventHandler(this.toolPrintbutton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.piclogo);
            this.splitContainer1.Panel1.Controls.Add(this.ctlTree);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.childPanel);
            this.splitContainer1.Size = new System.Drawing.Size(756, 451);
            this.splitContainer1.SplitterDistance = 252;
            this.splitContainer1.TabIndex = 3;
            // 
            // piclogo
            // 
            this.piclogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("piclogo.BackgroundImage")));
            this.piclogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.piclogo.Location = new System.Drawing.Point(0, 262);
            this.piclogo.Name = "piclogo";
            this.piclogo.Size = new System.Drawing.Size(251, 164);
            this.piclogo.TabIndex = 5;
            this.piclogo.TabStop = false;
            // 
            // ctlTree
            // 
            this.ctlTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlTree.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctlTree.ImageIndex = 0;
            this.ctlTree.ImageList = this.imageList1;
            this.ctlTree.Location = new System.Drawing.Point(0, 0);
            this.ctlTree.Name = "ctlTree";
            this.ctlTree.SelectedImageIndex = 0;
            this.ctlTree.Size = new System.Drawing.Size(252, 451);
            this.ctlTree.TabIndex = 4;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "RootIcon.png");
            this.imageList1.Images.SetKeyName(1, "secUsersNode.gif");
            this.imageList1.Images.SetKeyName(2, "Entities.png");
            // 
            // childPanel
            // 
            this.childPanel.Location = new System.Drawing.Point(3, 3);
            this.childPanel.Name = "childPanel";
            this.childPanel.Size = new System.Drawing.Size(497, 426);
            this.childPanel.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 500);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Data Clean (Entities)";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.piclogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printGridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolExitbutton;
        private System.Windows.Forms.ToolStripButton toolPrintbutton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView ctlTree;
        public System.Windows.Forms.PictureBox piclogo;
        public System.Windows.Forms.Panel childPanel;

        public StatusBar statusBar1 = new StatusBar();
        // Create two StatusBarPanel objects to display in the StatusBar.
        public StatusBarPanel panel1 = new StatusBarPanel();
        public static StatusBarPanel panel2 = new StatusBarPanel();
        StatusBarPanel panel3 = new StatusBarPanel();
        StatusBarPanel panel4 = new StatusBarPanel();
        StatusBarPanel panel5 = new StatusBarPanel();
        StatusBarPanel panel6 = new StatusBarPanel();
        StatusBarPanel panel7 = new StatusBarPanel();
        private ImageList imageList1;


    }
}