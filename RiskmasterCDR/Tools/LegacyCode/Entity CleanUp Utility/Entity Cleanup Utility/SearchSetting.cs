﻿namespace Entity_Cleanup_Utility
{
    using System;
    using System.Windows.Forms;

    public partial class SearchSetting : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchSetting"/> class.
        /// </summary>
        public SearchSetting()
        {
            InitializeComponent();
            


        }

        /// <summary>
        /// Handles the Click event of the btnDone control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnDone_Click(object sender, EventArgs e)
        {
            try
            {
                // bpaskova JIRA 4412 added condition for showing the dialog
                if (hasChanges())
                {
                    DialogResult result = MessageBox.Show("Setting data has changed. Do you want to save changes?", "Entity Cleanup Confirmation:", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        Helper.gobjSettings.ChangePAYEEName = this.chk_funds.Checked;
                        Helper.gobjSettings.Show1099ParentColumn = this.chkParent_1099.Checked;
                        Helper.gobjSettings.RemoveTAXIDFormatting = this.chktax.Checked;
                        Helper.gobjSettings.SaveData();
                        MessageBox.Show("Setting data has saved.", "Entity Cleanup Confirmation:", MessageBoxButtons.OK);
                    }
                }
                // bpaskova JIRA 4412 close dialog without saving
                this.Close();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        /// <summary>
        /// Handles the Load event of the SearchSetting control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void SearchSetting_Load(object sender, EventArgs e)
        {
            this.chk_funds.Checked = Helper.gobjSettings.ChangePAYEEName;
            this.chkParent_1099.Checked = Helper.gobjSettings.Show1099ParentColumn;
            this.chktax.Checked = Helper.gobjSettings.RemoveTAXIDFormatting;
        }

        // bpaskova JIRA 4412 start
        /// <summary>
        /// Checks for settings changes.
        /// </summary>
        /// <returns>true - if there are changes in the settings, otherwise - false.</returns>
        private bool hasChanges()
        {
            return (this.chk_funds.Checked != Helper.gobjSettings.ChangePAYEEName ||
                this.chkParent_1099.Checked != Helper.gobjSettings.Show1099ParentColumn ||
                this.chktax.Checked != Helper.gobjSettings.RemoveTAXIDFormatting);
        }
        // bpaskova JIRA 4412 end
    }
}
