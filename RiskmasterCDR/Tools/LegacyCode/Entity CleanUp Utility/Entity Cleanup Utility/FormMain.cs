﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;

namespace Entity_Cleanup_Utility
{
    /// <summary>
    /// Main Form.
    /// </summary>
    public partial class FormMain : Form
    {

        #region Fields
        //static string m_sDataSource = string.Empty;
        //static string m_sLoginName = string.Empty;
        //static string m_sLoginPwd = string.Empty;
        //public static string m_sDbConnstring = string.Empty;
        //static string m_sDSNID = string.Empty;
        //static string m_sDBOUserId = string.Empty;
        //static eDatabaseType m_sDatabaseType = 0;
        #endregion

        #region Properties
        ///// <summary>
        ///// Gets or sets the dsnid.
        ///// </summary>
        ///// <value>
        ///// The dsnid.
        ///// </value>
        //public string DSNID { get; set; }

        ///// <summary>
        ///// Gets or sets the dbo user identifier.
        ///// </summary>
        ///// <value>
        ///// The dbo user identifier.
        ///// </value>
        //public string DBOUserId { get; set; }

        ///// <summary>
        ///// Gets or sets the database connstring.
        ///// </summary>
        ///// <value>
        ///// The database connstring.
        ///// </value>
        //public string DbConnstring { get; set; }

        ///// <summary>
        ///// Gets or sets the type of the database.
        ///// </summary>
        ///// <value>
        ///// The type of the database.
        ///// </value>
        //public eDatabaseType DatabaseType { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            try
            {
                //int num1 = 0;
                //int num2 = 5;
                //int div = num2 / num1;
                InitializeComponent();
                
                this.Text +="  "+ Helper.fGetClientName().ToUpper();

            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region Functions
        #region FormMain_Load
        /// <summary>
        /// Handles the Load event of the FormMain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// 

        private void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                //Login objLogin = new Login();
                //UserLogin oUserLogin = null;
                //bool bCon = false;
                //bCon = objLogin.RegisterApplication(0, 0, ref oUserLogin);
                //if (bCon)
                //{
                //    this.Initalize(oUserLogin);
                this.CreateMyStatusBar();
                this.FillTree();
                //}
                //else
                //{
                //    return;
                //}
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region FillTree
        /// <summary>
        /// Fills the tree.
        /// </summary>
        private void FillTree()
        {

            try
            {

                // Adding Entity Clean Up (Duplicates) node and childs
                ctlTree.Nodes.Add("E", "");
                ctlTree.Nodes["E"].NodeFont = new Font("verdana", 10);
                ctlTree.Nodes["E"].ForeColor = Color.Red;
                ctlTree.Nodes["E"].Text = "Entity Clean Up (Duplicates)";
                ctlTree.Nodes["E"].Nodes.Add("P", "1.) Payees");
                ctlTree.Nodes["E"].Nodes.Add("EM", "2.) Employees");
                ctlTree.Nodes["E"].Nodes.Add("EN", "3.) Entities");
                ctlTree.Nodes["E"].Expand();

                // Adding Entity Clean Up (Utilities) node and childs
                ctlTree.Nodes.Add("EU", "");
                ctlTree.Nodes["EU"].NodeFont = new Font("verdana", 10);
                ctlTree.Nodes["EU"].ForeColor = Color.Red;
                ctlTree.Nodes["EU"].Text = "Entity Clean Up (Utilities)";
                ctlTree.Nodes["EU"].Nodes.Add("CT", "1.) Change Entity Types");
                ctlTree.ExpandAll();
                ctlTree.NodeMouseClick += new TreeNodeMouseClickEventHandler(ctlTree_NodeMouseClick);
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }



        }
        #endregion

        #region ctlTree_NodeMouseClick
        void ctlTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                this.childPanel.Controls.Clear();
                switch ((e.Node as TreeNode).Name)
                {

                    case "P":
                        FormPayee formPayee = new FormPayee();
                        formPayee.TopLevel = false;
                        formPayee.AutoScroll = true;
                        this.childPanel.Controls.Add(formPayee);
                        formPayee.Show();
                        break;

                    case "EM":
                        frmEmployee frmEmp = new frmEmployee();
                        frmEmp.TopLevel = false;
                        frmEmp.AutoScroll = true;
                        this.childPanel.Controls.Add(frmEmp);
                        frmEmp.Show();
                        break;
                    case "EN":
                        frmEntityCleanUp frmEnt = new frmEntityCleanUp();
                        frmEnt.TopLevel = false;
                        frmEnt.AutoScroll = true;
                        this.childPanel.Controls.Add(frmEnt);
                        frmEnt.Show();
                        break;

                    case "CT":
                        frmChangeEntity frmChEnt = new frmChangeEntity();
                        frmChEnt.TopLevel = false;
                        frmChEnt.AutoScroll = true;
                        this.childPanel.Controls.Add(frmChEnt);
                        frmChEnt.Show();
                        break;

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }



        }
        #endregion

        #region CreateMyStatusBar
        private void CreateMyStatusBar()
        {
            try
            {
                panel1.BorderStyle = StatusBarPanelBorderStyle.None;
                panel1.Text = "Status:";
                panel1.AutoSize = StatusBarPanelAutoSize.Contents;

                panel2.BorderStyle = StatusBarPanelBorderStyle.Sunken;
                panel2.AutoSize = StatusBarPanelAutoSize.Spring;

                panel3.Text = DateTime.Now.ToShortTimeString();
                panel3.AutoSize = StatusBarPanelAutoSize.Contents;

                panel4.Text = string.Format("USER: {0}", Helper.gsUser);
                panel4.AutoSize = StatusBarPanelAutoSize.Contents;

                panel5.Text = "NUM";
                panel5.Style = StatusBarPanelStyle.Text;
                panel5.AutoSize = StatusBarPanelAutoSize.Contents;

                statusBar1.ShowPanels = true;

                statusBar1.Panels.Add(panel1);
                statusBar1.Panels.Add(panel2);
                statusBar1.Panels.Add(panel3);
                statusBar1.Panels.Add(panel4);
                statusBar1.Panels.Add(panel5);
                statusBar1.Panels.Add(panel6);
                statusBar1.Panels.Add(panel7);
                // Add the StatusBar to the form. 
                this.Controls.Add(statusBar1);
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }

        #endregion

        #region FormMain_KeyDown
        private void FormMain_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode & Keys.KeyCode) == Keys.NumLock)
                {
                    if (Control.IsKeyLocked(Keys.NumLock))
                        this.panel5.Text = "CapsLock is activated.";
                    else
                        this.panel5.Text = null;
                }

                if ((e.KeyCode & Keys.KeyCode) == Keys.CapsLock)
                {
                    if (Control.IsKeyLocked(Keys.CapsLock))

                        this.panel6.Text = "NumLock is activated.";
                    else
                        this.panel6.Text = null;
                }

                if ((e.KeyCode & Keys.KeyCode) == Keys.Insert)
                {
                    if (Control.IsKeyLocked(Keys.Insert))
                        this.panel6.Text = "NumLock is activated.";
                    else
                        this.panel6.Text = null;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region ExitApp
        private void ExitApp()
        {
            try
            {
                if (MessageBox.Show("Are you sure you want to exit?", "Entity clean up exit confirmation :", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {

                }
                else
                {
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        #region Menu Options Controls
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitApp();
        }
        #endregion

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.childPanel.Controls.Clear();
                frmUndo frmundo = new frmUndo();
                frmundo.TopLevel = false;
                frmundo.AutoScroll = true;
                this.childPanel.Controls.Add(frmundo);
                frmundo.Show();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SearchSetting searchSetting = new SearchSetting();              
                searchSetting.Show();
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddtoLogFile(ex);
            }
        }
        #endregion

        
    }
}
