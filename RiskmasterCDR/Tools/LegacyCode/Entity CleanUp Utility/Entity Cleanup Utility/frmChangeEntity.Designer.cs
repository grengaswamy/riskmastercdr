﻿namespace Entity_Cleanup_Utility
{
    partial class frmChangeEntity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblCET = new System.Windows.Forms.Label();
            this.tabCET = new System.Windows.Forms.TabControl();
            this.TabEt = new System.Windows.Forms.TabPage();
            this.gbCET = new System.Windows.Forms.GroupBox();
            this.ctlPRGrid = new System.Windows.Forms.DataGridView();
            this.gr_prLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gr_prEntityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblRowSel = new System.Windows.Forms.Label();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cmbEntityTo = new System.Windows.Forms.ComboBox();
            this.lblChangeto = new System.Windows.Forms.Label();
            this.cmbEntityFrom = new System.Windows.Forms.ComboBox();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabCET.SuspendLayout();
            this.TabEt.SuspendLayout();
            this.gbCET.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCET
            // 
            this.lblCET.AutoSize = true;
            this.lblCET.BackColor = System.Drawing.SystemColors.Control;
            this.lblCET.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCET.ForeColor = System.Drawing.Color.Maroon;
            this.lblCET.Location = new System.Drawing.Point(26, 16);
            this.lblCET.Name = "lblCET";
            this.lblCET.Size = new System.Drawing.Size(158, 17);
            this.lblCET.TabIndex = 1;
            this.lblCET.Text = "Change Entity Types";
            // 
            // tabCET
            // 
            this.tabCET.Controls.Add(this.TabEt);
            this.tabCET.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCET.Location = new System.Drawing.Point(29, 49);
            this.tabCET.Name = "tabCET";
            this.tabCET.SelectedIndex = 0;
            this.tabCET.Size = new System.Drawing.Size(449, 357);
            this.tabCET.TabIndex = 2;
            // 
            // TabEt
            // 
            this.TabEt.Controls.Add(this.gbCET);
            this.TabEt.Controls.Add(this.lblCriteria);
            this.TabEt.Location = new System.Drawing.Point(4, 22);
            this.TabEt.Name = "TabEt";
            this.TabEt.Padding = new System.Windows.Forms.Padding(3);
            this.TabEt.Size = new System.Drawing.Size(441, 331);
            this.TabEt.TabIndex = 0;
            this.TabEt.Text = "Entity Types";
            this.TabEt.UseVisualStyleBackColor = true;
            // 
            // gbCET
            // 
            this.gbCET.Controls.Add(this.ctlPRGrid);
            this.gbCET.Controls.Add(this.lblRowSel);
            this.gbCET.Controls.Add(this.btnProcess);
            this.gbCET.Controls.Add(this.cmbEntityTo);
            this.gbCET.Controls.Add(this.lblChangeto);
            this.gbCET.Controls.Add(this.cmbEntityFrom);
            this.gbCET.Location = new System.Drawing.Point(3, 25);
            this.gbCET.Name = "gbCET";
            this.gbCET.Size = new System.Drawing.Size(432, 300);
            this.gbCET.TabIndex = 2;
            this.gbCET.TabStop = false;
            this.gbCET.Text = "Select Entity Types";
            // 
            // ctlPRGrid
            // 
            this.ctlPRGrid.AllowUserToAddRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Blue;
            this.ctlPRGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.ctlPRGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ctlPRGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ctlPRGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctlPRGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gr_prLastName,
            this.gr_prFirstName,
            this.gr_prEntityID});
            this.ctlPRGrid.Location = new System.Drawing.Point(9, 75);
            this.ctlPRGrid.Name = "ctlPRGrid";
            this.ctlPRGrid.Size = new System.Drawing.Size(415, 193);
            this.ctlPRGrid.TabIndex = 2;
            this.ctlPRGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlPRGrid_CellClick);
            // 
            // gr_prLastName
            // 
            this.gr_prLastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prLastName.DefaultCellStyle = dataGridViewCellStyle6;
            this.gr_prLastName.FillWeight = 150F;
            this.gr_prLastName.HeaderText = "First Name";
            this.gr_prLastName.Name = "gr_prLastName";
            this.gr_prLastName.Width = 93;
            // 
            // gr_prFirstName
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prFirstName.DefaultCellStyle = dataGridViewCellStyle7;
            this.gr_prFirstName.FillWeight = 150F;
            this.gr_prFirstName.HeaderText = "Last Name";
            this.gr_prFirstName.Name = "gr_prFirstName";
            // 
            // gr_prEntityID
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gr_prEntityID.DefaultCellStyle = dataGridViewCellStyle8;
            this.gr_prEntityID.FillWeight = 150F;
            this.gr_prEntityID.HeaderText = "Entity ID";
            this.gr_prEntityID.Name = "gr_prEntityID";
            // 
            // lblRowSel
            // 
            this.lblRowSel.AutoSize = true;
            this.lblRowSel.ForeColor = System.Drawing.Color.Maroon;
            this.lblRowSel.Location = new System.Drawing.Point(16, 274);
            this.lblRowSel.Name = "lblRowSel";
            this.lblRowSel.Size = new System.Drawing.Size(104, 13);
            this.lblRowSel.TabIndex = 6;
            this.lblRowSel.Text = "Row Selected : 0";
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(339, 274);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmbEntityTo
            // 
            this.cmbEntityTo.FormattingEnabled = true;
            this.cmbEntityTo.Location = new System.Drawing.Point(256, 35);
            this.cmbEntityTo.Name = "cmbEntityTo";
            this.cmbEntityTo.Size = new System.Drawing.Size(164, 21);
            this.cmbEntityTo.TabIndex = 1;
            // 
            // lblChangeto
            // 
            this.lblChangeto.AutoSize = true;
            this.lblChangeto.Location = new System.Drawing.Point(186, 41);
            this.lblChangeto.Name = "lblChangeto";
            this.lblChangeto.Size = new System.Drawing.Size(71, 13);
            this.lblChangeto.TabIndex = 1;
            this.lblChangeto.Text = "Change to:";
            // 
            // cmbEntityFrom
            // 
            this.cmbEntityFrom.FormattingEnabled = true;
            this.cmbEntityFrom.Location = new System.Drawing.Point(16, 34);
            this.cmbEntityFrom.Name = "cmbEntityFrom";
            this.cmbEntityFrom.Size = new System.Drawing.Size(164, 21);
            this.cmbEntityFrom.TabIndex = 0;
            this.cmbEntityFrom.SelectedIndexChanged += new System.EventHandler(this.cmbEntityFrom_SelectedIndexChanged);
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.ForeColor = System.Drawing.Color.Maroon;
            this.lblCriteria.Location = new System.Drawing.Point(14, 7);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(50, 13);
            this.lblCriteria.TabIndex = 7;
            this.lblCriteria.Text = "Criteria";
            this.lblCriteria.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(476, 406);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // frmChangeEntity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(503, 452);
            this.Controls.Add(this.tabCET);
            this.Controls.Add(this.lblCET);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmChangeEntity";
            this.Text = "FormPayee";
            this.Load += new System.EventHandler(this.FormPayee_Load);
            this.tabCET.ResumeLayout(false);
            this.TabEt.ResumeLayout(false);
            this.TabEt.PerformLayout();
            this.gbCET.ResumeLayout(false);
            this.gbCET.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctlPRGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCET;
        private System.Windows.Forms.TabControl tabCET;
        private System.Windows.Forms.TabPage TabEt;
        private System.Windows.Forms.GroupBox gbCET;
        private System.Windows.Forms.ComboBox cmbEntityFrom;
        private System.Windows.Forms.Label lblChangeto;
        private System.Windows.Forms.ComboBox cmbEntityTo;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Label lblRowSel;
        private System.Windows.Forms.Label lblCriteria;
        internal System.Windows.Forms.DataGridView ctlPRGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gr_prEntityID;

    }
}