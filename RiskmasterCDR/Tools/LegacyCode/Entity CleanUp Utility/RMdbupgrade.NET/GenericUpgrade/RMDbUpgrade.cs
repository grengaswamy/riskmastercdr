﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

using Riskmaster.ExceptionTypes;

namespace GenericUpgrade
{
    public partial class RMDbUpgrade : Form
    {

        private LoginHelper loginHelper;

        public RMDbUpgrade()
        {
            InitializeComponent();
        }

        private void btnUpgrade_Click(object sender, EventArgs e)
        {
            if (this.lstDBs.SelectedItem != null)
            {
                KeyValuePair<Int32, String> chosenDb = (KeyValuePair<Int32, String>)this.lstDBs.SelectedItem;

                DialogResult dRes = MessageBox.Show(this, StringResources.UpgradeDbWarningMessage, StringResources.UpgradeDbWarningCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dRes == DialogResult.Yes)
                {
                    Upgrading frmUpgrading = null;
                    try
                    {

                        Cursor.Current = Cursors.WaitCursor;

                        frmUpgrading = new Upgrading();
                        RmUpgrader upgrader = new RmUpgrader(chosenDb.Key, loginHelper.SecurityDsn, frmUpgrading);
                        frmUpgrading.Show(this);
                        frmUpgrading.CloseDisabled = true;
                        upgrader.Upgrade();
                        frmUpgrading.Progress = "100";
                        frmUpgrading.Refresh();
                        MessageBox.Show(frmUpgrading, StringResources.SuccessfullyUpgradedMessage, StringResources.RiskmasterCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmUpgrading.CloseDisabled = false;
                        frmUpgrading.Close();
                        Application.DoEvents();
                        if (!this.chkMultiDB.Checked)
                        {
                            this.Close();
                        }

                    }
                    catch (ECUException ecuExc)
                    {
                        ExceptionLogger.AddException(ecuExc);
                        MessageBox.Show(this, ecuExc.Message, ecuExc.Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (RMAppException exc)
                    {
                        ExceptionLogger.AddException(exc); 
                        MessageBox.Show(this, exc.Message, StringResources.SelectDbCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.AddException(ex); 
                        MessageBox.Show(this, StringResources.Upgrade_GeneralError, StringResources.SelectDbCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        if (frmUpgrading != null)
                        {
                            frmUpgrading.CloseDisabled = false;
                            frmUpgrading.Close();
                        }
                        Cursor.Current = Cursors.Arrow;
                    }
                }
                else
                {
                    MessageBox.Show(this, StringResources.UpgradeCanceledMessage, StringResources.RiskmasterCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(this, StringResources.SelectDbMessage, StringResources.SelectDbCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RMDbUpgrade_Load(object sender, EventArgs e)
        {
            try
            {
                btnUpgrade.Enabled = false;
                chkMultiDB.Enabled = false;
                loginHelper = new LoginHelper();
         
                Dictionary<Int32,String> dbList = loginHelper.GetDatabases();
                this.lstDBs.DataSource = new BindingSource(dbList, null);
                this.lstDBs.DisplayMember = "Value";
                this.lstDBs.ValueMember = "Key";
                if (dbList.Count > 0)
                {
                    btnUpgrade.Enabled = true;
                }
                if (dbList.Count > 1)
                {
                    chkMultiDB.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.AddException(ex); 
                MessageBox.Show(this, ex.Message, StringResources.CriticalErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1); // exit application
            }
        }
    }
}
