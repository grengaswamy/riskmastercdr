﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenericUpgrade
{
    public partial class Upgrading : Form
    {

        private Boolean isCloseDisabled = false;
        public Boolean CloseDisabled
        {
            set { isCloseDisabled = value; }
        }


        public Upgrading()
        {
            InitializeComponent();
        }

        private void Upgrading_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isCloseDisabled)
            {
                e.Cancel = true;
            }
        }

        public String Progress
        {
            set { this.lblProgressMeter.Text = value; }
        }
    }
}
