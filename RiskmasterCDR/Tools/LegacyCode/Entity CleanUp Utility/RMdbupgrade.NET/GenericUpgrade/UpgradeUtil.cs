﻿using System;
using System.Text;
using System.IO;
using System.Windows.Forms;

using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace GenericUpgrade
{
    internal class UpgradeUtil
    {
        
        private string[] sParmArray = new string[10];   // used by execute sql - these values may be initialized by the call to bExecuteSQLScript
        // They may be changed in the script by [ASSIGN statements in the script file.

        private String m_connectionString;
        private TextWriter m_logWriter;
        private eDatabaseType m_dbType; 
        private DbUpgrade objDbUpgrade;

        public int g_IfNest;    // JP 2/1/2002    Level of IF nesting (only executes lines if == 0)

        public UpgradeUtil(string connectionString, TextWriter logWriter)
        {
	        m_connectionString = connectionString;
	        m_logWriter = logWriter;
	        objDbUpgrade = new DbUpgrade(m_connectionString);
	        m_dbType = objDbUpgrade.DbType;
        }

        public event ChangeProgressEventHandler ChangeProgress;
        public delegate void ChangeProgressEventHandler(object sender, ProgressEventArgs e);

        protected virtual void OnChangeProgress(ProgressEventArgs e)
        {
	        if (ChangeProgress != null) {
		        ChangeProgress(this, e);
	        }
        }


        //Public Event ChangeProgress(ByVal sender As Object, ByVal e As ProgressEventArgs)

        //Protected Overridable Sub OnChangeProgress(ByVal e As ProgressEventArgs)
        //    RaiseEvent ChangeProgress(Me, e)
        //End Sub

    public bool ExecuteSQLScriptExt(string sScriptFilename, string sParms)
    {
	    ////Technical Info-----------------------------------------------------------------------------------------------------------
	    //
	    ////Description
	    //This routine returns the next unique id for the given table.
	    //And also creates the next unique id and updates the database for the given table
	    //
	    ////Arguments
	    // sScriptFilename   | String     | Input    | Filename of script file. Does not include path
	    //                                              (assumed to be in program directory).
	    // sParms            | String     | Input    | String of substitution parameters. Separated by tabs.
	    //                                              If not enough parms are supplied for the script,
	    //                                              blanks will be inserted. Up to 9 parameters are
	    //                                              supported for the entire file.
	    //
	    //--------------------------------------------------------------------------------------------------------------------------
	    bool bRetVal = false;
	    string sScriptPath = string.Empty;
	    string sSQL = "";
	    bool bForgive = false;
	    int iDBSpecific = 0;
	    // JP 8/15/2001
	    string sTmp = string.Empty;
	    int iCount = 0;
	    bool bExecDirect = false;
	    bool bGoMode = false;
	    string sLine = string.Empty;
	    bool bProceed = false;
	    string sNativeError = "";
	    string sODBCError = "";

	    try {
		    g_IfNest = 0;   // Set as IF blocks are encountered that are designed to stop SQL from executing under certain circumstances
		    bGoMode = false;   
 
            // parse out initial parms into a global array
		    for (iCount = 1; iCount <= 9; iCount++) {
			    sParmArray[iCount] = ""; // blank out all parms first
		    }

            char separator = Convert.ToChar(9);

		    sTmp = sParms;
		    iCount = 1;

            if (sParms.IndexOf(separator) > 0)
            {
                string[] sTmpArr = sParms.Split(separator);

                foreach (string strParm in sTmpArr)
                {
                    if (String.IsNullOrEmpty(strParm))
                    {
                        continue;
                    }//if

                    sParmArray[iCount] = strParm;
                    iCount++;
                }//foreach
            }//if
            else
            {
                sParmArray[iCount] = String.Empty;
            }//else



		    // prepare to execute script
		    sScriptPath = GetSystemPath() + sScriptFilename;

		    OnChangeProgress(new ProgressEventArgs(40, sScriptPath));

		    // see if script exists
		    if (!File.Exists(sScriptPath)) {
			    m_logWriter.WriteLine("--- Error Msg: Could not find database upgrade script");
			    m_logWriter.WriteLine(" file-" + sScriptFilename);
			    m_logWriter.Flush();
			    throw new ECUException("File Missing", "Could not find database upgrade script. Upgrade program may be damaged. Please contact Computer Sciences Corporation Tech support.");
		    }

		    // open text file and execute statements
            using (TextReader objScriptReader = new StreamReader(sScriptPath))
            {
                iCount = 0;
                while (true)
                {
                    iCount = iCount + 1;
                    sLine = objScriptReader.ReadLine();
                    if (sLine == null)
                    {
                        break;
                    }
                    // FTP & some mail progs strip LF chars
                    if (sLine.Contains("\r") || sLine.Contains("\n"))
                    {
                        m_logWriter.WriteLine("--- Error Msg: Database upgrade script may be damaged");
                        m_logWriter.WriteLine(" file-" + sScriptFilename + " line-" + iCount + ") " + sLine);
                        m_logWriter.Flush();
                        throw new ECUException("Bad Script", "Database upgrade script may be damaged. Please contact Computer Sciences Corporation Tech support.");
                    }
                    // ignore comment lines (begin with ;)
                    if ((sLine.Trim().Substring(0, 1) != ";") && (sLine.Trim().Substring(0, 1) != "'") && ((sLine.Trim().Substring(0, 3)).ToUpper() != "REM") && (!string.IsNullOrEmpty(sLine.Trim())))
                    {
                        if (sLine.Trim() == "[GO_DELIMETER]")
                        {
                            bGoMode = true;
                            sSQL = "";
                        }
                        else if (bIsPseudoStatement(sLine.Trim()))
                        {
                            // SPECIFIC FUNCTION PROCESSING or error
                            if (!bProcessPseudoStatement(sLine))
                            {
                                return bRetVal;
                            }
                            sSQL = "";
                        }
                        else
                        {
                            bProceed = true;
                            // collect lines until a GO is spotted
                            if (bGoMode)
                            {
                                if (sLine.ToUpper().Trim() != "GO")
                                {
                                    bProceed = false;
                                    sSQL = sSQL + sLine;
                                }
                            }
                            else
                            {
                                sSQL = sLine;
                                // single line per statement mode
                            }

                            if (bProceed)
                            {
                                // subst parms if available and subst in any carriage returns
                                sSQL = sFormatMsgString(sSQL);

                                // Check and see if line if db specific
                                iDBSpecific = -1;
                                sSQL = sCheckDBSpecific(sSQL, ref iDBSpecific);

                                // Execute line only if for all databases or for this specific one    JP 8/15/2001
                                if (iDBSpecific == -1 || iDBSpecific == (int)m_dbType)
                                {
                                    // check for upgrade SQL extensions
                                    sSQL = sCheckForgive(sSQL, ref bForgive, ref sNativeError, ref sODBCError);

                                    // check to see if we need to directly execute statement (NOTE: all statements are now direct anyhow - since ROCKET was integrated)
                                    sSQL = sCheckExecDirect(sSQL, ref bExecDirect);
                                    //UI reference
                                    OnChangeProgress(new ProgressEventArgs(-1, "Executing: " + sSQL));


                                    if (m_dbType == eDatabaseType.DBMS_IS_ACCESS)
                                        sSQL = sSQL.Replace("|", "' + chr(124) + '");

                                    // Only execute if we're not excluded by an IF statement
                                    if (g_IfNest == 0)
                                    {
                                        if (!bForgive)
                                        {
                                            DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
                                        }
                                        else
                                        {
                                            try
                                            {
                                                DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
                                            }
                                            catch (Exception ex)
                                            {
                                                // todo bpa code changed to use ex.Message instead of Conversion.ErrorToString(); verify logging patterns 
                                                sTmp = ex.Message;
                                                // this come with CrLf

                                                if (sNativeError.Length > 0 && sTmp.IndexOf("[Native Error=" + sNativeError + "]") >= 0)
                                                {
                                                    // Do Nothing for now
                                                }
                                                else if (sODBCError.Length > 0 && sTmp.IndexOf("[ODBC Error=" + sODBCError + "]") >= 0)
                                                {
                                                    // Do Nothing for now
                                                }
                                                else
                                                {
                                                    // Log the error in the FORGIVE_ERR.LOG
                                                    if (sTmp.IndexOf("already") >= 0 || sTmp.IndexOf("duplic") >= 0 || sTmp.IndexOf("unique") >= 0)
                                                    {
                                                        //                                        Print #5, "- Error Msg: " & sTmp
                                                        sTmp = "- Error Msg: " + sTmp;
                                                    }
                                                    else if (sTmp.IndexOf("Syntax") >= 0 || sTmp.IndexOf("syntax") >= 0)
                                                    {
                                                        //                                        Print #5, "--- Error Msg: " & sTmp
                                                        sTmp = "--- Error Msg: " + sTmp;
                                                    }
                                                    else
                                                    {
                                                        //                                        Print #5, "-- Error Msg: " & sTmp
                                                        sTmp = "-- Error Msg: " + sTmp;
                                                    }
                                                    //                                    Print #5, "file: " & sScriptFilename & ", line: " & Trim$(Str$(iCount)) & ": " & sSQL
                                                    if (sTmp.Substring(sTmp.Length - 2, 2) != Environment.NewLine)
                                                        sTmp = sTmp + Environment.NewLine;
                                                    sTmp = sTmp + "file: " + sScriptFilename;
                                                    sTmp = sTmp + ", line: " + iCount;

                                                    m_logWriter.WriteLine(sTmp + ": " + sSQL + Environment.NewLine);
                                                    m_logWriter.Flush();
                                                }

                                            }
                                        }
                                    }
                                }

                                sSQL = "";
                            }
                        }
                    }
                }
            }   // close script file
		    bRetVal = true;
	    } catch (RMAppException) {
		    throw;
	    } catch (Exception ex) {
		    ExceptionLogger.AddMessage("UpgradeUtil.ExecuteSQLScriptExt", ex.Message);
		    throw new ECUException("Tech Support Error Information", "Upgrade Failed on the Following Script Line:" + Environment.NewLine + sScriptFilename + ": " + iCount.ToString() + " " + sSQL, ex);
	    }
	    return bRetVal;

    }

        public bool bIsPseudoStatement(string sSQL)
        {
	        bool bRetVal = false;
	        if (sSQL.Substring(0, 7).ToUpper() == "[ASSIGN") {
		        bRetVal = true;
	        } else if (sSQL.Substring(0, 15).ToUpper() == "[UPDT_TBL_STAMP") {
		        bRetVal = true;
	        } else if (sSQL.Substring(0, 4).ToUpper() == "[IF ") {
		        bRetVal = true;
	        } else if (sSQL.Substring(0, 6).ToUpper() == "[ENDIF") {
		        bRetVal = true;
	        } else {
		        bRetVal = false;
	        }
	        return bRetVal;
        }


        public bool bProcessPseudoStatement(string sSQL)
        {
	        bool bGroupAssoc = false;
	        //jtodd22 01/31/2008 ADD_STATE_FIELD_2
	        string sAssocField = string.Empty;
	        //jtodd22 01/31/2008 ADD_STATE_FIELD_2
	        string sListOrgHierarchyIDs = string.Empty;
	        //jtodd22 02/03/2010
	        string sWork = string.Empty;
	        string sTemp2 = string.Empty;
	        int lTemp = 0;
	        short nRegister = 0;
	        string sParm = string.Empty;
	        int nPos = 0;
	        string sFunction = string.Empty;
	        int nNumParms = 0;
	        string[] sTMParray = new string[1];

	        short iStateRowID = 0;
	        short iFormCategory = 0;
	        string sFormTitle = string.Empty;
	        string sFormName = string.Empty;
	        string sFileName = string.Empty;
	        short iActiveFlag = 0;
	        string sHashCRC = string.Empty;
	        string sPrimaryFormFlag = string.Empty;
	        short iLineOfBusCode = 0;

	        string sSC = string.Empty;
	        short iNLSCode = 0;
	        string sDescription = string.Empty;
	        string sSysCodeTable = string.Empty;
	        int lRelCodeID = 0;

	        string sStateID = string.Empty;
	        string sStateName = string.Empty;
	        double dEmployerLiability = 0;
	        double dMaxClaimAmount = 0;
	        short iRatingMethodCode = 0;
	        short iPrimaryFormFlag = 0;
	        int iUSLLimit = 0;
	        int iWeightingStepVal = 0;
	        int iDeletedFlag = 0;

	        string sStateTable = string.Empty;
	        string sUserPrompt = string.Empty;
	        string sSysFieldName = string.Empty;
	        int lFieldType = 0;
	        int lFieldSize = 0;
	        bool bRequired = false;
	        bool bDeleted = false;
	        bool bLookup = false;
	        bool bIsPatterned = false;
	        string sPattern = string.Empty;
	        string sCodeTable = string.Empty;
	        short iCatID = 0;
	        string sFieldName = string.Empty;
	        string sFieldDesc = string.Empty;
	        string sFieldTable = string.Empty;
	        string sFieldType = string.Empty;
	        string sOptMask = string.Empty;
	        string sDisplayCat = string.Empty;
	        bool bRetVal = false;


	        try {
		        sWork = sSQL.Trim();
		        // UI reference
		        OnChangeProgress(new ProgressEventArgs(-1, "Executing: " + sSQL));

		        // remove brackets from statement
		        if (!sWork.StartsWith("[") || !sWork.EndsWith("]")) {
			        // SYNTAX ERROR - incorrect delimeter for pseudo statement
			        throw new ECUException("PSEUDO STATEMENT PARSE ERROR", "Incorrect Backeting of Pseudo Statement");

                } else {
                    sWork = sWork.Substring(1, sWork.Length - 2);
                }

		        // determine which pseudo statement this line is
		        if (sWork.Substring(0, 7).ToUpper() == "ASSIGN ") {
			        if (g_IfNest > 0) {
				        return true;
				        // JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
			        }

			        // ASSIGN  -  assigns a value to a register (%%1 through %%9)
			        sWork = sWork.Substring(7).Trim();
			        // strip off assign pseudo statement keyword

			        // pick off register to assign
                    sTemp2 = sWork.Substring(0, 4);
			        if (!sTemp2.StartsWith("%%") || !Char.IsDigit(sTemp2, 2) || !sTemp2.EndsWith("=")) {
				        // SYNTAX ERROR - unexpected format
				        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Register Assignment Syntax");

			        } else {
				        nRegister = Int16.Parse(sTemp2.Substring(2, 1));
                        sWork = sWork.Substring(4).Trim();    // strip off register assignment portion

				        // process remainder through parameter substitution function
				        sWork = sFormatMsgString(sWork);

				        // determine assignment function
				        if (!sWork.ToUpper().StartsWith("QUERY(") && !sWork.Contains("(")) {
					        // SYNTAX ERROR - function format incorrect
					        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Pseudo Function Is Missing Open Parenthesis");

				        }

                        if (!sWork.ToUpper().StartsWith("QUERY(")) {
					        nPos = sWork.IndexOf("(");
					        sFunction = sWork.Substring(0, nPos).Trim().ToUpper();
					        sWork = sWork.Substring(nPos + 1).Trim();
				        // special processing for QUERY directive
				        } else {
					        sFunction = "QUERY";
                            sWork = sWork.Substring(6).Trim();
				        }

				        if (!sWork.EndsWith(")")) {
					        // SYNTAX ERROR - invalid function syntax - missing closing paren
					        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Pseudo Function Is Missing Close Parenthesis");

				        } else {
                            sWork = sWork.Substring(0, sWork.Length - 1).Trim();     // strip off closing paren

					        if (sFunction != "QUERY") {
						        // tear out parameters to function
                                string[] sTmpArr = sWork.Split(',');
                                nNumParms = sTmpArr.Length;
                                Array.Resize(ref sTMParray, nNumParms + 1);
                                int ind = 1;
                                foreach(string sTmp in sTmpArr)
                                {
                                    sTMParray[ind++] = sTmp;
                                }

					        } else {  // special processing for query function
						        nNumParms = 1;
						        sTMParray = new string[2];
						        sTMParray[1] = sWork;
					        }

					        // SPECIFIC FUNCTION PROCESSING   -    *** BEGIN ***
					        switch (sFunction) {
						        case "GET_TABLEID":
							        if (nNumParms != 1) {
								        // ERROR - incorrect number of parameters to GETTABLEID function
								        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to GET_TABLEID Function (1 is required)");

							        } else {
								        sParm = sTMParray[1].Trim();
								        lTemp = lGetTableID(sParm);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
									        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "System Table Name Specified in GET_TABLEID call is Invalid");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ATTACH_TABLE":
							        // ERROR - not supported
							        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "ATTACH TABLE macro no longer supported");

						        case "REATTACH_TABLE":
							        // ERROR - not supported
                                    throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "REATTACH TABLE macro no longer supported");

						        case "NEXT_UID":
							        if (nNumParms != 1) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to NEXT_UID Function (1 is required)");

							        } else {
								        sParm = sTMParray[1].Trim();
								        lTemp = objDbUpgrade.lGetNextUID(sParm);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "System Table Name Specified in NEXT_UID call is Invalid OR Unable to Obtain Glossary Unique ID");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "QUERY":
							        if (nNumParms != 1) {
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to Query Function (1 is required)");

                                    } else {
								        sParm = sTMParray[1].Trim();

								        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sParm)) {
									        if (objReader.Read()) {
										        sParmArray[nRegister] = objReader.GetString(0);
									        } else {
										        sParmArray[nRegister] = "";
									        }
								        }
							        }

							        break;
						        case "ADD_CODE":
							        if (nNumParms != 5) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_CODE Function (5 are required)");

                                    } else {

								        sSC = sTMParray[1].Trim();
								        iNLSCode = Int16.Parse(sTMParray[2]);
								        sDescription = sTMParray[3].Trim();
								        sSysCodeTable = sTMParray[4].Trim();
								        lRelCodeID = Int32.Parse(sTMParray[5]);

								        lTemp = lAddCode(sSC, iNLSCode, sDescription, sSysCodeTable, lRelCodeID);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_CODE call failed OR the call failed.");
								        }

								        // if successful, place value in designated register
                                        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_JURISDICTION":
							        if (nNumParms != 8) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_JURISDICTION Function (8 are required)");

							        } else {
								        //STATE_ROW_ID

								        sStateID = sTMParray[1].Trim();
								        sStateName = sTMParray[2].Trim();
								        dEmployerLiability = Convert.ToDouble(sTMParray[3]);
								        dMaxClaimAmount = Convert.ToDouble(sTMParray[4]);
								        iRatingMethodCode = Convert.ToInt16(sTMParray[5]);
								        iUSLLimit = (Convert.ToInt16(sTMParray[6]) != 0) ? -1: 0;
								        iWeightingStepVal = (Convert.ToInt16(sTMParray[7]) != 0) ? -1: 0;
								        iDeletedFlag = (Convert.ToInt16(sTMParray[8]) != 0) ? -1: 0;

								        lTemp = lAddJurisdiction(sStateID, sStateName, dEmployerLiability, dMaxClaimAmount, iRatingMethodCode, iUSLLimit, iWeightingStepVal, iDeletedFlag);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
                                        sParmArray[nRegister] = lTemp.ToString();

							        }

							        break;
						        case "ADD_JURISDICTIONAL_FROI":
							        if (nNumParms != 7) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_JURISDICTION_FROI Function (7 are required)");

							        } else {
								        //(1)STATE_ROW_ID,(2)FORM_NAME,(3)PDF_FILE_NAME,(4)ACTIVE_FLAG,(5)HASH_CRC,(6)PRIMARY_FORM_FLAG,(7)LINE_OF_BUS_CODE

								        sStateID = sTMParray[1].Trim();
								        sFormName = sTMParray[2].Trim();
								        sFileName = sTMParray[3].Trim();
								        iActiveFlag = Convert.ToInt16(sTMParray[4]);
								        sHashCRC = sTMParray[5].Trim();
								        iPrimaryFormFlag = Convert.ToInt16(sTMParray[6]);
								        iLineOfBusCode = Convert.ToInt16(sTMParray[7]);

								        lTemp = lAddJurisdictionalFROI(Convert.ToInt16(sStateID), sFormName, sFileName, iActiveFlag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
                                        sParmArray[nRegister] = lTemp.ToString();


							        }
							        break;
						        case "ADD_STATE_FIELD":
							        if (nNumParms != 11) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_STATE_FIELD Function (11 are required)");

							        } else {
								        sStateTable = sTMParray[1].Trim();
								        sUserPrompt = sTMParray[2].Trim();
								        sSysFieldName = sTMParray[3].Trim();
								        lFieldType = Int32.Parse(sTMParray[4]);
								        lFieldSize = Int32.Parse(sTMParray[5]);
								        bRequired = Convert.ToInt16(sTMParray[6]) != 0;
								        bDeleted = Convert.ToInt16(sTMParray[7]) != 0;
								        bLookup = Convert.ToInt16(sTMParray[8]) != 0;
								        bIsPatterned = Convert.ToInt16(sTMParray[9]) != 0;
								        sPattern = sTMParray[10].Trim();
								        sCodeTable = sTMParray[11].Trim();

								        lTemp = lAddStateField(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern,
								        sCodeTable);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_STATE_FIELD_2":
							        if (nNumParms != 13) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_STATE_FIELD_2 Function (13 are required)");

							        } else {
								        sStateTable = sTMParray[1].Trim();
								        sUserPrompt = sTMParray[2].Trim();
								        sSysFieldName = sTMParray[3].Trim();
								        lFieldType = Int32.Parse(sTMParray[4]);
								        lFieldSize = Int32.Parse(sTMParray[5]);
								        bRequired = Convert.ToInt16(sTMParray[6]) != 0;
								        bDeleted = Convert.ToInt16(sTMParray[7]) != 0;
								        bLookup = Convert.ToInt16(sTMParray[8]) != 0;
								        bIsPatterned = Convert.ToInt16(sTMParray[9]) != 0;
								        sPattern = sTMParray[10].Trim();
								        sCodeTable = sTMParray[11].Trim();
								        bGroupAssoc = Convert.ToInt16(sTMParray[12]) != 0;
								        sAssocField = sTMParray[13].Trim();


								        lTemp = lAddStateField_2(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern,
								        sCodeTable, bGroupAssoc, sAssocField);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_STATE_FIELD_2 call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_STATE_FIELD_ORGHIERARCHYASSOC":
							        if (nNumParms != 14) {
								        // ERROR - incorrect number of parameters to NEXTUID function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_STATE_FIELD_ORGHIERARCHYASSOC Function (14 are required)");

							        } else {
								        sStateTable = sTMParray[1].Trim();
								        sUserPrompt = sTMParray[2].Trim();
								        sSysFieldName = sTMParray[3].Trim();
								        lFieldType = Int32.Parse(sTMParray[4]);
								        lFieldSize = Int32.Parse(sTMParray[5]);
								        bRequired = Convert.ToInt16(sTMParray[6]) != 0;
								        bDeleted = Convert.ToInt16(sTMParray[7]) != 0;
								        bLookup = Convert.ToInt16(sTMParray[8]) != 0;
								        bIsPatterned = Convert.ToInt16(sTMParray[9]) != 0;
								        sPattern = sTMParray[10].Trim();
								        sCodeTable = sTMParray[11].Trim();
								        bGroupAssoc = Convert.ToInt16(sTMParray[12]) != 0;
								        sAssocField = sTMParray[13].Trim();
								        sListOrgHierarchyIDs = sTMParray[14].Trim();

								        lTemp = lAddStateField_OrgHierarchyAssoc(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern,
								        sCodeTable, bGroupAssoc, sAssocField, sListOrgHierarchyIDs);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the lAddStateField_OrgHierarchyAssoc call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_MERGE_DICTIONARY":
							        if (nNumParms != 8) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_MERGE_DICTIONARY Function 8 are required)");

							        } else {
								        iCatID = Int16.Parse(sTMParray[1]);
								        sFieldName = sTMParray[2].Trim();
								        sFieldDesc = sTMParray[3].Trim();
								        sFieldTable = sTMParray[4].Trim();
								        sFieldType = sTMParray[5];
								        sOptMask = sTMParray[6].Trim();
								        sDisplayCat = sTMParray[7].Trim();
								        sCodeTable = sTMParray[8];

								        lTemp = lAddMergeDictionaryLine(iCatID, sTMParray[2], sFieldDesc, sFieldTable, sTMParray[5].Trim(), sOptMask, sDisplayCat, sTMParray[8].Trim());
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_MERGE_DICTIONARY call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
                                        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;

						        case "ADD_WCP_FORM":
							        if (nNumParms != 9) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required)");

							        } else {
								        iStateRowID = Int16.Parse(sTMParray[1]);
								        iFormCategory = Int16.Parse(sTMParray[2]);
								        sFormTitle = sTMParray[3].Trim();
								        sFormName = sTMParray[4].Trim();
								        sFileName = sTMParray[5].Trim();
								        iActiveFlag = Int16.Parse(sTMParray[6]);
								        if (string.IsNullOrEmpty(sTMParray[7]))
									        sTMParray[7] = "no value";
								        sHashCRC = sTMParray[7].Trim();
								        if (string.IsNullOrEmpty(sHashCRC))
									        sHashCRC = "no value";
								        sPrimaryFormFlag = sTMParray[8].Trim();
								        iLineOfBusCode = Int16.Parse(sTMParray[9]);

								        //jtodd22 01/08/2008 trap for iStateRowID being 0 (zero)
								        if (iStateRowID < 1) {
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "The jurisdition ID value in ADD_WCP_FORM is invalid (zero or less).  The STATES table may have bad data.");
								        }

								        lTemp = lAddWCPForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_WCP_FORM call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
                                        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_CL_FORM":

							        if (nNumParms != 9) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required).");

							        } else {

								        iStateRowID = Int16.Parse(sTMParray[1]);
								        iFormCategory = Int16.Parse(sTMParray[2]);
								        sFormTitle = sTMParray[3].Trim();
								        sFormName = sTMParray[4].Trim();
								        sFileName = sTMParray[5].Trim();
								        iActiveFlag = Int16.Parse(sTMParray[6]);
								        if (string.IsNullOrEmpty(sTMParray[7]))
									        sTMParray[7] = "no value";
								        sHashCRC = sTMParray[7].Trim();
								        if (string.IsNullOrEmpty(sHashCRC))
									        sHashCRC = "no value";
								        sPrimaryFormFlag = sTMParray[8].Trim();
								        iLineOfBusCode = Int16.Parse(sTMParray[9]);

								        lTemp = lAddCLForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_WCP_FORM call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }
							        break;
						        case "ADD_EV_FORM":

							        if (nNumParms != 9) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_EV_FORM Function (9 are required)");

							        } else {

								        iStateRowID = Int16.Parse(sTMParray[1]);
								        iFormCategory = Int16.Parse(sTMParray[2]);
								        sFormTitle = sTMParray[3].Trim();
								        sFormName = sTMParray[4].Trim();
								        sFileName = sTMParray[5].Trim();
								        iActiveFlag = Int16.Parse(sTMParray[6]);
								        if (string.IsNullOrEmpty(sTMParray[7]))
									        sTMParray[7] = "no value";
								        sHashCRC = sTMParray[7].Trim();
								        if (string.IsNullOrEmpty(sHashCRC))
									        sHashCRC = "no value";
								        sPrimaryFormFlag = sTMParray[8].Trim();
								        iLineOfBusCode = Int16.Parse(sTMParray[9]);

								        lTemp = lAddEVForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_EV_FORM call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_GLOSSARY":
							        if (nNumParms != 4) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_GLOSSARY Function (4 are required)");

							        } else {
								        lTemp = lAddGlossary(sTMParray[1].Trim(), sTMParray[2].Trim(), Int16.Parse(sTMParray[3]), Int16.Parse(sTMParray[4]), 0);
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_GLOSSARY call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "ADD_GLOSSARY_EX":
							        if (nNumParms != 5) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to ADD_GLOSSARY_EX Function (5 are required)");

							        } else {
								        lTemp = lAddGlossary(sTMParray[1].Trim(), sTMParray[2].Trim(), Int16.Parse(sTMParray[3]), Int16.Parse(sTMParray[4]), Int16.Parse(sTMParray[5]));
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the ADD_GLOSSARY_EX call failed OR the call failed.");

								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }

							        break;
						        case "GET_COUNT":
							        if (nNumParms != 1) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to GET_COUNT Function (1 are required).");

							        } else {
								        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sTMParray[1].Trim())) {
									        if (objReader.Read()) {
										        lTemp = objReader.GetInt32(0);
									        } else {
                                                throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the GET_COUNT call failed OR the call failed.");
									        }
								        }

							        }
							        // if successful, place value in designated register
							        sParmArray[nRegister] = lTemp.ToString();

							        break;
						        case "DATABASE_UPGRADE_A":
							        if (nNumParms != 1) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to DATABASE_UPGRADE_A Function (1 are required).");

                                    } else {
								        lTemp = lDataBaseUpgrade_A();
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the DATABASE_UPGRADE_A call failed OR the call failed.");
								        }
							        }

							        break;
						        case "WCPFORMS_DUALCASEOFF":
							        if (nNumParms != 1) {
								        // ERROR - incorrect number of parameters to function
                                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to WCPFORMS_DUALCASEOFF Function (5 are required)");

							        } else {
								        lTemp = lWCPFormsDualCaseOff(sTMParray[1].Trim());
								        if (lTemp == 0) {
									        // ERROR - Error in script - System Table Name Not Found
                                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "One or more parameters to the WCPFORMS_DUALCASEOFF call failed OR the call failed.");
								        }

								        // if successful, place value in designated register
								        sParmArray[nRegister] = lTemp.ToString();
							        }
							        break;
						        default:
							        // ERROR - UNSUPPORTED FUNCTION
                                    throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "This ASSIGN subfunction is unsupported");

					        }
					        // SPECIFIC FUNCTION PROCESSING   -    ***  END  ***


					        // destroy parms array
					        System.Array.Clear(sTMParray, 0, sTMParray.Length);
				        }
			        }


		        } else if (sWork.ToUpper().StartsWith("IF ")) {
			        if (g_IfNest > 0) {
				        g_IfNest = g_IfNest + 1;
				        return true;
				        // JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
			        }

			        // IF  -  compares a value to a register (%%1 through %%9)
			        sWork = sWork.Substring(3).Trim();
			        // strip off assign pseudo statement keyword

			        // pick off register to assign
                    sTemp2 = sWork.Substring(0, 3);
                    if (!sTemp2.StartsWith("%%") || !Char.IsDigit(sTemp2, 2)) {
				        // SYNTAX ERROR - unexpected format
                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect IF Function Syntax");

			        } else {
                        nRegister = Int16.Parse(sTemp2.Substring(2, 1));
                        sWork = sWork.Substring(3);    // strip off register

				        // Pick off operator
				        if (sWork.StartsWith("<>")) {
					        if (!(sWork.Substring(2) != sParmArray[nRegister])) {
						        g_IfNest = g_IfNest + 1;
					        }
				        } else if (sWork.StartsWith("=")) {
                            if (!(sWork.Substring(1) == sParmArray[nRegister])) {
						        g_IfNest = g_IfNest + 1;
					        }
				        } else if (sWork.StartsWith("<")) {
                            if (!(String.Compare(sWork.Substring(1),sParmArray[nRegister]) < 0)) {
						        g_IfNest = g_IfNest + 1;
					        }
				        } else if (sWork.StartsWith(">")) {
                            if (!(String.Compare(sWork.Substring(1), sParmArray[nRegister]) > 0)) {
						        g_IfNest = g_IfNest + 1;
					        }
				        } else {
                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Invalid IF operator.");
				        }

				        // destroy parms array
				        System.Array.Clear(sTMParray, 0, sTMParray.Length);
			        }
		        } else if (sWork.ToUpper().StartsWith("ENDIF")) {
			        if (g_IfNest > 0)
				        g_IfNest = g_IfNest - 1;

		        } else if (sWork.ToUpper().StartsWith("UPDT_TBL_STAMP")) {
			        if (g_IfNest > 0) {
				        return true;
				        // JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
			        }

			        sWork = sWork.Substring(14).Trim();
			        // strip off statement keyword

			        // process remainder through parameter substitution function
			        sWork = sFormatMsgString(sWork);

			        // determine assignment function
			        if (!sWork.Contains("(")) {
				        // SYNTAX ERROR - function format incorrect
                        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Pseudo Function Is Missing Open Parenthesis.");

			        } else {
				        nPos = sWork.IndexOf("(");
				        sWork = sWork.Substring(nPos + 1).Trim();
				        if (!sWork.EndsWith(")")) {
					        // SYNTAX ERROR - invalid function syntax - missing closing paren
                            throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Pseudo Function Is Missing Close Parenthesis");

				        } else {
					        sWork = sWork.Substring(0, sWork.Length - 1).Trim();
					        // strip off closing paren

					        // tear out parameters to function
                            string[] sTmpArr = sWork.Split(',');
                            nNumParms = sTmpArr.Length;
                            Array.Resize(ref sTMParray, nNumParms + 1);
                            int ind = 1;
                            foreach (string sTmp in sTmpArr)
                            {
                                sTMParray[ind++] = sTmp;
                            }

					        // SPECIFIC FUNCTION PROCESSING   -    *** BEGIN ***
					        if (nNumParms != 1) {
						        // ERROR - incorrect number of parameters to UPDT_TBL_STAMP Sub
                                throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Incorrect Number of Parameters Given to UPDT_TBL_STAMP Subroutine (1 is required)");

					        } else {
						        sParm = sTMParray[1].Trim();
						        subTblUpdtStmp(sParm);
					        }

					        // SPECIFIC FUNCTION PROCESSING   -    ***  END  ***


					        // destroy parms array
					        System.Array.Clear(sTMParray, 0, sTMParray.Length);
				        }
			        }
		        } else {
			        // unsupported pseudo statement
		        }

		        bRetVal = true;
		        // success
            }
            catch (RMAppException e) {
		        throw e;
	        } catch (Exception ex) {
                ExceptionLogger.AddMessage("UpgradeUtil.bProcessPseudoStatement", ex.Message);
	        }
	        return bRetVal;
        }
    
        public int lAddCode(string sShortCode, short iNLSCode, string sDescription, string sSysCodeTable, int lRelatedCodeID)
        {
	        string sSQL = string.Empty;
	        int lCodeID = 0;
	        int lTableID = 0;
	        bool bJustAddDesc = false;
	        int lRetVal = 0;

	        try {
		        // get table id of code table to add to
		        lTableID = lGetTableID(sSysCodeTable);
		        if (lTableID <= 0) {
                    throw new ECUException(sSysCodeTable + " is not a Valid Table Name");
		        }

		        if (sSysCodeTable.ToUpper() == "STATES") {
			        // see if this code already exists
			        sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" + sSQLStringLiteral(sShortCode.Trim()) + "'";
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
				        if (objReader.Read()) {
					        lRetVal = objReader.GetInt32(0);
				        }
			        }

			        // get a unique id for the state
			        lCodeID = objDbUpgrade.lGetNextUID("STATES");
			        if (lCodeID <= 0) {
                        throw new ECUException("Unable to assign next ID for STATES Table");
			        }

			        // insert the values into the codes text table
			        sSQL = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, DELETED_FLAG)";
			        sSQL = sSQL + " VALUES (" + lCodeID + ",";
			        sSQL = sSQL + "'" + sSQLStringLiteral(sShortCode.Trim()) + "','" + sSQLStringLiteral(sDescription.Trim()) + "', 0)";
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

			        // stamp codes for recache
			        subTblUpdtStmp("STATES");
		        } else {
			        bool bHasCodeId = false;
			        // see if this code already exists
			        sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + lTableID + " AND SHORT_CODE='" + sSQLStringLiteral(sShortCode.Trim()) + "'";
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
				        if (objReader.Read()) {
					        bHasCodeId = true;

					        lCodeID = objReader.GetInt32("CODE_ID");
				        }
			        }
			        if (bHasCodeId) {
				        sSQL = "SELECT CODE_ID FROM CODES_TEXT WHERE CODE_ID = " + lCodeID;
				        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
					        if (objReader.Read()) {
						        return lCodeID;


					        } else if (lCodeID <= 0) {
                                throw new ECUException("Invalid Code ID for CODES Table: " + sShortCode + " " + lTableID);
					        } else {
						        bJustAddDesc = true;
					        }
				        }
			        }


			        if (!bJustAddDesc) {
				        // get a unique id for the code
				        lCodeID = objDbUpgrade.lGetNextUID("CODES");
				        if (lCodeID <= 0) {
                            throw new ECUException("Unable to assign next ID for CODES Table");
				        }

				        // insert the values into the codes table
				        sSQL = "INSERT INTO CODES (CODE_ID,TABLE_ID,SHORT_CODE,RELATED_CODE_ID,DELETED_FLAG,LINE_OF_BUS_CODE)";
				        sSQL = sSQL + " VALUES (" + lCodeID + "," + lTableID + ",";
				        sSQL = sSQL + "'" + sSQLStringLiteral(sShortCode.Trim()) + "'," + lRelatedCodeID + ",0,0)";
				        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
			        }

			        // insert the values into the codes text table
			        sSQL = "INSERT INTO CODES_TEXT (CODE_ID, LANGUAGE_CODE, SHORT_CODE, CODE_DESC)";
			        sSQL = sSQL + " VALUES (" + lCodeID + "," + iNLSCode + ",";
			        sSQL = sSQL + "'" + sSQLStringLiteral(sShortCode.Trim()) + "','" + sSQLStringLiteral(sDescription.Trim()) + "')";
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

			        // stamp codes for recache
			        subTblUpdtStmp("CODES");
		        }

		        // return code id
		        lRetVal = lCodeID;
	        } catch (RMAppException e) {
		        throw e;
	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddCode", ex.Message);
	        }
	        return lRetVal;
        }



        public int lAddGlossary(string sSysTableName, string sUserTableName, short iGlossType, short iNLSCode, int lParentTableID)
        {
	        string sSQL = string.Empty;
	        int lTableID = 0;
	        int lGlossaryTypeCode = 0;
	        int lRetVal = 0;


	        try {
		        // find actual glossary type CODE of the glossary type number
		        lGlossaryTypeCode = lGetCodeIDWithShort(iGlossType.ToString(), lGetTableID("GLOSSARY_TYPES"));
		        if (lGlossaryTypeCode == 0) {
			        return lRetVal;
		        }

		        // see if this table already exists
		        sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sSysTableName.Trim() + "'";
		        // sSQL = sSQL & " AND GLOSSARY_TYPE_CODE=" & Trim$(Str$(lGlossaryTypeCode))
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        return objReader.GetInt32("TABLE_ID");
				        // return existing ID if existing record found
			        }

		        }

		        // get a unique id for the glossary entry
		        lTableID = objDbUpgrade.lGetNextUID("GLOSSARY");
		        if (lTableID <= 0) {
			        return lRetVal;
		        }

		        // insert the values into the glossary table
		        if (lParentTableID == 0) {
			        sSQL = "INSERT INTO GLOSSARY (TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,NEXT_UNIQUE_ID," + "DELETED_FLAG,REQD_REL_TABL_FLAG,ATTACHMENTS_FLAG,REQD_IND_TABL_FLAG,LINE_OF_BUS_FLAG)";
			        sSQL = sSQL + " VALUES (" + lTableID + ",";
			        sSQL = sSQL + "'" + sSysTableName.Trim() + "'," + lGlossaryTypeCode + ", 1,0,0,0,0,0)";
		        } else {
			        sSQL = "INSERT INTO GLOSSARY (TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,NEXT_UNIQUE_ID,RELATED_TABLE_ID," + "DELETED_FLAG,REQD_REL_TABL_FLAG,ATTACHMENTS_FLAG,REQD_IND_TABL_FLAG,LINE_OF_BUS_FLAG)";
			        sSQL = sSQL + " VALUES (" + lTableID + ",";
			        sSQL = sSQL + "'" + sSysTableName.Trim() + "'," + lGlossaryTypeCode + ", 1,";
			        sSQL = sSQL + lParentTableID + ",0,-1,0,0,0)";
		        }
		        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

		        // insert the values into the glossary text table
		        sSQL = "INSERT INTO GLOSSARY_TEXT (TABLE_ID, TABLE_NAME, LANGUAGE_CODE)";
		        sSQL = sSQL + " VALUES (" + lTableID + ", '" + sUserTableName.Trim() + "',";
		        sSQL = sSQL + iNLSCode + ")";
		        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

		        // stamp glossary for recache
		        subTblUpdtStmp("GLOSSARY");

		        // return new table id
		        lRetVal = lTableID;
	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddGlossary", ex.Message);
	        }
	        return lRetVal;
        }


        public int lGetCodeIDWithShort(string sShortCode, int lTableID)
        {
	        int lIDTmp = 0;

	        lIDTmp = 0;
	        try {
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID=" + lTableID + " AND SHORT_CODE='" + sShortCode.Trim() + "'")) {
			        if (objReader.Read()) {
				        lIDTmp = objReader.GetInt32("CODE_ID");
			        }
		        }
	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lGetCodeIDWithShort", ex.Message);
	        }
	        return lIDTmp;
        }

        public int lGetTableID(string sSysTableName)
        {
            // returns 0 on error
	        int lIDTmp = 0;

	        try {
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sSysTableName + "'")) {
			        if (objReader.Read()) {
				        lIDTmp = objReader.GetInt32("TABLE_ID");
			        }
		        }

	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lGetTableID", ex.Message);
	        }
	        return lIDTmp;
        }

        public int lWCPFormsDualCaseOff(string sFileName)
        {
	        //WCPFORMS_DUALCASEOFF
	        string sSQL = string.Empty;
	        int lResult = 0;
	        string sFileNameLCase = string.Empty;


	        try {
		        //jtodd22 01/24/2006 to stop issues with case of file extension
		        //jtodd22 04/23/2007 to stop mixed case issues with Oracle
		        //jtodd22 04/23/2007 to stop issues with file name/extension in RMX with Apache Tomcat
		        //jtodd22 04/23/2007 convention is to use lower case only

		        sFileName = sFileName.Trim();
		        sFileNameLCase = sFileName.ToLower();

		        sSQL = "";
		        sSQL = sSQL + "UPDATE WCP_FORMS";
		        sSQL = sSQL + " SET FILE_NAME = '" + sFileNameLCase + "'";
		        switch (m_dbType) {
			        case eDatabaseType.DBMS_IS_ORACLE:
				        sSQL = sSQL + " WHERE LOWER(FILE_NAME) = '" + sFileNameLCase + "'";
				        break;
			        default:
				        sSQL = sSQL + " WHERE FILE_NAME = '" + sFileNameLCase + "'";
				        break;
		        }

		        //jtodd22 04/23/2007 because of older upgrades the same file name can be in the database twice
		        try {
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
		        } catch {
			        //save vb6 functionality
		        }
		        lResult = -1;
	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lWCPFormsDualCaseOff", ex.Message);
	        }
	        return lResult;
        }


        public string sCheckExecDirect(string sSQL, ref bool bExecDirect)
        {
	        string sRetVal = String.Empty;
	        string sTemp = String.Empty;


	        try {
		        sTemp = sSQL.Trim();
		        if (sTemp.Substring(0, 8).ToUpper() == "[DIRECT]") {
                    sTemp = sTemp.Substring(8).Trim();
                    sTemp = sTemp.Substring(8).Trim();
                    bExecDirect = true;
		        } else {
			        bExecDirect = false;
		        }

		        sRetVal = sTemp;
	        } catch (Exception) {
		        bExecDirect = false;
		        sRetVal = sSQL;
	        }
	        return sRetVal;
        }


        public string sCheckForgive(string sSQL, ref bool bForgive, ref string sNative, ref string sODBC)
        {
	        string sRetVal = String.Empty;
	        string sTemp = String.Empty;


	        try {
                sTemp = (sSQL.Replace(Convert.ToChar(9), ' ')).Trim();
		        sTemp = sTemp.Replace(" (", "(");

		        if (sTemp.ToUpper().StartsWith("[FORGIVE]")) {
			        sTemp = sTemp.Substring(9).Trim();

			        if (sTemp.ToUpper().StartsWith("ALTER TABLE ")) {
				        sTemp = "[FORGIVE_ALTER] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("CREATE TABLE ")) {
				        sTemp = "[FORGIVE_CREATETABLE] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("CREATE INDEX ")) {
				        sTemp = "[FORGIVE_CREATEINDEX] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("CREATE UNIQUE INDEX ")) {
				        sTemp = "[FORGIVE_CREATEINDEX] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("DROP INDEX ")) {
				        sTemp = "[FORGIVE_DROPINDEX] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("DROP TABLE ")) {
				        sTemp = "[FORGIVE_DROPTABLE] " + sTemp;
			        } else if (sTemp.ToUpper().StartsWith("INSERT INTO ")) {
				        sTemp = "[FORGIVE_INSERT] " + sTemp;
			        } else {
				        sTemp = "[FORGIVE] " + sTemp;
			        }
		        }

		        // Process different forgive scenarios
		        if (sTemp.ToUpper().StartsWith("[FORGIVE]")) {
			        sTemp = sTemp.Substring(9).Trim();
			        bForgive = true;
			        // traditional - FORGIVE EVERYTHING
			        sNative = "";
			        sODBC = "";
		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_ALTER]")) {
                    sTemp = sTemp.Substring(15).Trim();
			        bForgive = true;
			        // FORGIVE EVERYTHING - log if not a duplicate
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1508";
					        sODBC = "S0021";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "2705";
					        sODBC = "S0021";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "2705";
					        sODBC = "S0021";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-328";
					        sODBC = "S0021";
					        break;
				        case eDatabaseType.DBMS_IS_DB2: // TODO / Needs testing dcm 1/2003
					        //DB2, Msg Ref: SQL0612N "<name>" is a duplicate name.
					        //Explanation: A statement was issued with the same name appearing more than once where duplicates are not allowed. Where
					        // these names appear varies depending on the type of statement.
					        //ALTER TABLE statement cannot add a column to a table using the name of a column that already exists or is the same as
					        // another column being added. Furthermore, a column name can only be referenced in one ADD or ALTER COLUMN clause in a
					        // single ALTER TABLE statement.
					        //User Response: Specify unique names as appropriate for the type of statement.
					        //sqlcode: -612, sqlstate: 42711
					        sNative = "-612";   // <own.cname> is a duplicate name.
					        sODBC = "S0021";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "1430";
					        sODBC = "";
					        break;
				        default:
					        sNative = "";
					        sODBC = "S0021";
					        break;
			        }

		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_CREATETABLE]")) {
                    sTemp = sTemp.Substring(21).Trim();
			        bForgive = true;    // FORGIVE EVERYTHING - log if not a duplicate
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1303";
					        sODBC = "S0001";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "2714";
					        sODBC = "S0001";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "2714";
					        sODBC = "S0001";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-310";
					        sODBC = "S0001";
					        break;
				        case eDatabaseType.DBMS_IS_DB2:
					        //DB2, Msg Ref: SQL0601N The name of the object to be created is identical to the existing name "<name>" of type "<type>".
					        //Explanation: The CREATE or ALTER statement tried to create or add an object "<name>" when an object of type "<type>"
					        // already exists with that name on the application server or in the same statement.
					        //If "<type>" is FOREIGN KEY, PRIMARY KEY, UNIQUE, or CHECK CONSTRAINT, the "<name>" is the constraint name specified in the
					        // CREATE or ALTER TABLE statement or generated by the system.
					        //Federated system users: some data sources do not provide the appropriate values for the "<name>" and "<type>" message
					        // tokens. In these cases, "<name>" and "<type>" will have the following format: "OBJECT:<data source> TABLE/VIEW", and
					        // "UNKNOWN" indicating that the actual values at the specified data source are not known.
					        //The statement cannot be processed. No new object is created, and the existing object is not altered or modified.
					        //User Response: Either drop the existing object or choose another name for the new object.
					        //Federated system users: if the statement is a CREATE FUNCTION MAPPING or a CREATE TYPE MAPPING statement, the user can
					        // also consider not supplying a type mapping name and the system will automatically generate a unique name for this mapping.
					        //sqlcode: -601, sqlstate: 42710
					        sNative = "-601";   // is identical to existing name
					        sODBC = "S0001";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "955";
					        sODBC = "S0001";
					        break;
				        default:
					        sNative = "";
					        sODBC = "S0001";
					        break;
			        }

		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_CREATEINDEX]")) {
                    sTemp = sTemp.Substring(21).Trim();
			        bForgive = true;    // FORGIVE EVERYTHING - log if not a duplicate
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1403";
					        sODBC = "S0011";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "1913";
					        sODBC = "S0011";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "1913";
					        sODBC = "S0011";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-316";
					        sODBC = "S0011";
					        break;
				        case eDatabaseType.DBMS_IS_DB2:     // TODO / Needs testing dcm 1/2003
					        //DB2, Msg Ref: SQL0605W The index was not created because the index "<name>" already exists with the required description.
					        //Explanation: A CREATE INDEX operation attempted to create a new index and the indicated index matches the required index.
					        //For CREATE INDEX, two index descriptions match if they identify the same columns in the same order with the same ascending
					        // or descending specifications, and are both specified as unique or the new index is specified as non-unique. Also, two
					        // index descriptions match if they identify the same columns in the same order with the same or reverse ascending or
					        // descending specifications, and at least one description includes the ALLOW REVERSE SCANS parameter.
					        //The new index was not created.
					        //User Response: No action is required unless the existing index "<name>" is not a suitable index. For example, the existing
					        // index "<name>" is not a suitable index if it does not allow reverse scans, and the required one does (or vice versa). In
					        // this case, the index "<name>" must be dropped before the required index can be created.
					        //sqlcode: +605, sqlstate: 01550
					        sNative = "605";    // the index <own.iname> already exists   : 4 of 5
					        //              sNative = "-601"   ' is identical to existing name   : this has happened on 1 of 5, why?  dcm 1/2003
					        sODBC = "S0011";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "955";
					        sODBC = "S0001";
					        break;
				        default:
					        sNative = "";
					        sODBC = "S0011";
					        break;
			        }

		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_DROPTABLE]")) {
                    sTemp = sTemp.Substring(19).Trim();
			        bForgive = true; // FORGIVE EVERYTHING - log if not already dropped
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1305";
					        sODBC = "S0002";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "3701";
					        sODBC = "S0002";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "3701";
					        sODBC = "S0002";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-206";
					        sODBC = "S0002";
					        break;
				        case eDatabaseType.DBMS_IS_DB2: 	// TODO / Needs testing dcm 1/2003
					        //DB2, Msg Ref: SQL0204N "<own.tname>" is an undefined name.
					        // see below
					        sNative = "-204";   // no such table
					        sODBC = "S0002";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "942";
					        sODBC = "S0002";
					        break;
				        default:
					        sNative = "";
					        sODBC = "S0002";
					        break;
			        }

		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_DROPINDEX]")) {
                    sTemp = sTemp.Substring(19).Trim();
			        bForgive = true; 	// FORGIVE EVERYTHING - log if not already dropped
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1404";
					        sODBC = "S0012";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "3703";
					        sODBC = "S0012";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "3703";
					        sODBC = "S0012";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-319";
					        sODBC = "S0012";
					        break;
				        case eDatabaseType.DBMS_IS_DB2: 	// TODO / Needs testing dcm 1/2003
					        //DB2, Msg Ref: SQL0204N "<own.iname>" is an undefined name.
					        // see below
					        sNative = "-204"; 	// no such index
					        sODBC = "S0012";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "1418";
					        sODBC = "";
					        break;
				        default:
					        sNative = "";
					        sODBC = "S0012";
					        break;
			        }

		        } else if (sTemp.ToUpper().StartsWith("[FORGIVE_INSERT]")) {
                    sTemp = sTemp.Substring(16).Trim();
			        bForgive = true; 	// FORGIVE EVERYTHING - log if not a duplicate
			        switch (m_dbType) {
				        case eDatabaseType.DBMS_IS_ACCESS:
					        sNative = "-1605";
					        sODBC = "23000";
					        break;
				        case eDatabaseType.DBMS_IS_SQLSRVR:
					        sNative = "2601";
					        sODBC = "23000";
					        break;
				        case eDatabaseType.DBMS_IS_SYBASE:
					        sNative = "2601";
					        sODBC = "23000";
					        break;
				        case eDatabaseType.DBMS_IS_INFORMIX:
					        sNative = "-239";
					        sODBC = "23000";
					        break;
				        case eDatabaseType.DBMS_IS_DB2:
					        //DB2, Msg Ref: SQL0803N One or more values in the INSERT statement are not valid because they would produce duplicate rows
					        // for a table with a primary key, unique constraint, or unique index.
					        //Explanation: The INSERT object table is constrained by one or more UNIQUE indexes to have unique values in certain columns
					        // or groups of columns. The unique index may exist to support a primary key or unique constraint defined on the table.
					        // Completing the requested insert or update results in duplicates of the column values.
					        //Alternatively, if a view is the object of the INSERT statement, it is the table on which the view is defined that is
					        // constrained.
					        //The statement cannot be processed. The table remains unchanged.
					        //User Response: Examine the definitions for all UNIQUE indexes defined on the object table to determine the uniqueness
					        // constraints those indexes impose.
					        //For an INSERT statement, examine the object table content to determine which of the values in the specified value list
					        // violates the uniqueness constraint. Alternatively, if the INSERT statement contains a subquery, the object table contents
					        // addressed by that subquery must be matched against the object table contents to determine the cause of the problem.
					        //sqlcode: -803, sqlstate: 23505
					        sNative = "-803"; 	// constrains table <own.tname> from having duplicate rows
					        sODBC = "23000";
					        break;
				        case eDatabaseType.DBMS_IS_ORACLE:
					        sNative = "1";
					        sODBC = "23000";
					        break;
				        default:
					        sNative = "";
					        sODBC = "23000";
					        break;
			        }

		        } else {
			        bForgive = false;
		        }

		        sRetVal = sTemp;

		        // Move explanation of messages to a reference file.
		        // Other DB2 messages seen. W-Warning(+), N-Notification(-), and C-Critical(-)
		        // ie. SQL0104N is Notification of -104, and SQL0605W is Warning of 605
		        // Native Error = sqlcode and ODBC Error is sometimes sqlstate.  dcm 1/2003.
		        //
		        //DB2, Msg Ref: SQL0104N An unexpected token "<token>" was found following "<text>". Expected tokens may include: "<token-list>".
		        //Explanation: A syntax error in the SQL statement was detected at the specified token following the text "<text>".
		        // The "<text>" field indicates the 20 characters of the SQL statement that preceded the token that is not valid.
		        //As an aid to the programmer, a partial list of valid tokens is provided in the SQLERRM field of the SQLCA as
		        // "<token-list>". This list assumes the statement is correct to that point.
		        //The statement cannot be processed.
		        //User Response: Examine and correct the statement in the area of the specified token.
		        //sqlcode: -104, sqlstate: 42601

		        //DB2, Msg Ref: SQL0107N The name "<name>" is too long. The maximum length is "<length>".
		        //Explanation: The name returned as "<name>" is too long. The maximum length permitted for names of that type is indicated
		        // by "<length>".
		        //The names for indexes and constraints can be a maximum length of 18 bytes. The names for columns can be a maximum length
		        // of 30 bytes. The names for savepoints, tables, views and aliases can be a maximum length of 128 bytes. (This does not
		        // include any escape characters, if present.)
		        //A maximum of 30 bytes is permitted for a schema name (object qualifier), with the exception of user-defined types, which
		        // allow a maximum of 8 bytes for a schema name.
		        //Host variable names must not exceed 30 bytes in length.
		        //For the SQL CONNECT statement, an application server name of up to 18 characters in length will be accepted at
		        // pre-compilation time. However, at runtime, an application server name which is greater than 8 characters in length will
		        // cause an error.
		        //Also, a password of up to 18 characters in length and an authorization ID of up to 8 characters in length will be accepted
		        // in the SQL CONNECT statement.
		        //Federated system users: if in a pass-through session, a data source-specific limit might have been exceeded.
		        //The statement cannot be processed. Note: Where character data conversions are performed for applications and databases
		        // running under different codepages, the result of the conversion is exceeding the length limit.
		        //User Response: Choose a shorter name or correct the spelling of the object name.
		        //Federated system users: for a pass-through session, determine what data source is causing the error (see the problem
		        // determination guide for the failing data sources). Examine the SQL dialect for that data source to determine which
		        // specific limit has been exceeded, and adjust the failing statement as needed.
		        //sqlcode: -107, sqlstate: 42622

		        //DB2, Msg Ref: SQL0204N "<name>" is an undefined name.
		        //Explanation: This error is caused by one of the following:
		        //The object identified by "<name>" is not defined in the database.
		        //A data type is being used. This error can occur for the following reasons:
		        //If "<name>" is qualified, then a data type with this name does not exist in the database.
		        //If "<name>" is unqualified, then the user's function path does not contain the schema to which the desired data type belongs.
		        //The data type does not exist in the database with a create timestamp earlier than the time the package was bound
		        // (applies to static statements).
		        //If the data type is in the UNDER clause of a CREATE TYPE statement, the type name may be the same as the type being
		        // defined, which is not valid.
		        //A function is being referenced in one of:
		        //a DROP FUNCTION statement
		        //a COMMENT ON FUNCTION statement
		        //the SOURCE clause of a CREATE FUNCTION statement
		        //If "<name>" is qualified, then the function does not exist. If "<name>" is unqualified, then a function of this name does
		        // not exist in any schema of the current function path. Note that a function cannot be sourced on the COALESCE, NULLIF, or
		        // VALUE built-in functions.
		        //This return code can be generated for any type of database object.
		        //Federated system users: the object identified by "<name>" is not defined in the database or "<name>" is not a nickname in
		        // a DROP NICKNAME statement.
		        //Some data sources do not provide the appropriate values for "<name>". In these cases, the message token will have the
		        // following format: "OBJECT:<data source> TABLE/VIEW", indicating that the actual value for the specified data source is unknown.
		        //The statement cannot be processed.
		        //User Response: Ensure that the object name (including any required qualifiers) is correctly specified in the SQL statement
		        // and it exists. For missing data type or function in SOURCE clause, it may be that the object does not exist, OR it may be
		        // that the object does exist in some schema, but the schema is not present in your function path.
		        //Federated system users: if the statement is DROP NICKNAME, make sure the object is actually a nickname. The object might
		        // not exist in the federated database or at the data source. Verify the existence of the federated database objects (if
		        // any) and the data source objects (if any).
		        //sqlcode: -204, sqlstate: 42704

		        //DB2, Msg Ref: SQL0206N "<name>" is not valid in the context where it is used.
		        //Explanation: This error can occur in the following cases:
		        //For an INSERT or UPDATE statement, the specified column is not a column of the table, or view that was specified as the
		        // object of the insert or update.
		        //For a SELECT or DELETE statement, the specified column is not a column of any of the tables or views identified in a FROM
		        // clause in the statement.
		        //For an ORDER BY clause, the specified column is a correlated column reference in a subselect, which is not allowed.
		        //For a CREATE TRIGGER statement:
		        //A reference is made to a column of the subject table without using an OLD or NEW correlation name.
		        //The left hand side of an assignment in the SET transition-variable statement in the triggered action specifies an old
		        // transition variable where only a new transition variable is supported.
		        //For a CREATE FUNCTION statement:
		        //The RETURN statement of the SQL function references a variable that is not a parameter or other variable that is in the
		        // scope of the RETURN statement.
		        //The FILTER USING clause references a variable that is not a parameter name or an expression name in the WHEN clause.
		        //The search target in an index exploitation rule does not match some parameter name of the function that is being created.
		        //A search argument in an index exploitation rule does not match either an expression name in the EXPRESSION AS clause or a
		        // parameter name of the function being created.
		        //For a CREATE INDEX EXTENSION statement, the RANGE THROUGH clause or the FILTER USING clause references a variable that is
		        // not a parameter name that can be used in the clause.
		        //The statement cannot be processed.
		        //User Response: Verify that the names are specified correctly in the SQL statement. For a SELECT statement, ensure that all
		        // the required tables are named in the FROM clause. For a subselect in an ORDER BY clause, ensure that there are no
		        // correlated column references. If a correlation name is used for a table, verify that subsequent references use the
		        // correlation name and not the table name.
		        //For a CREATE TRIGGER statement, ensure that only new transition variables are specified on the left hand side of
		        // assignments in the SET transition-variable statement and that any reference to columns of the subject table have a
		        // correlation name specified.
		        //sqlcode: -206, sqlstate: 42703

		        //DB2, Msg Ref: SQL0401N The data types of the operands for the operation "<operator>" are not compatible.
		        //Explanation: The operation "<operator>" appearing within the SQL statement has a mixture of numeric and nonnumeric
		        // operands, or the operation operands are not compatible.
		        //Federated system users: this data type violation can be at the data source or at the federated server.
		        //Some data sources do not provide the appropriate values for "<operator>". In these cases the message token will have the
		        // following format: "<data source>:UNKNOWN", indicating that the actual value for the specified data source is unknown.
		        //The statement cannot be processed.
		        //User Response: Check all operand data types to ensure that they are comparable and compatible with the statement usage.
		        //If all the SQL statement operands are correct and accessing a view, check the data types of all the view operands.
		        //Federated system users: if the reason is unknown, isolate the problem to the data source failing the request (see the
		        // Problem Determination Guide for procedures to follow to identify the failing data source) and examine the data type
		        // restrictions for that data source.
		        //sqlcode: -401, sqlstate: 42818

		        //DB2, Msg Ref:SQL0433N Value "<value>" is too long.
		        //Explanation: The value "<value>" required truncation by a system (built-in) cast or adjustment function, which was called
		        // to transform the value in some way. The truncation is not allowed where this value is used.
		        //The value being transformed is one of the following:
		        //an argument to a user defined function (UDF)
		        //an input to the SET clause of an UPDATE statement
		        //a value being INSERTed into a table
		        //an input to a cast or adjustment function in some other context.
		        //a recursively referenced column whose data type and length is determined by the initialization part of recursion and may
		        // grow in the iterative part of the recursion.
		        //The statement has failed.
		        //User Response: If "<value>" is a literal string in the SQL statement, it is too long for its intended use.
		        //If "<value>" is not a literal string, examine the SQL statement to determine where the transformation is taking place.
		        // Either the input to the transformation is too long, or the target is too short.
		        //Correct the problem and rerun the statement.
		        //sqlcode: -433, sqlstate: 22001
	        } catch (Exception) {
		        bForgive = false;
		        sRetVal = sSQL;
	        }
	        return sRetVal;

        }


        public string sCheckDBSpecific(string sSQL, ref int iDBMake)
        {
	        string sRetVal = String.Empty;
	        string sTemp = String.Empty;


	        try {
		        sTemp = sSQL.Trim();
		        if (sTemp.Substring(0, 12).ToUpper() == "[SQL SERVER]") {
                    sTemp = sTemp.Substring(12).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                }
                else if (sTemp.Substring(0, 9).ToUpper() == "[SQLSRVR]") {
                    sTemp = sTemp.Substring(9).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                }
                else if (sTemp.Substring(0, 8).ToUpper() == "[SYBASE]") {
                    sTemp = sTemp.Substring(8).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_SYBASE;
                }
                else if (sTemp.Substring(0, 8).ToUpper() == "[ACCESS]") {
                    sTemp = sTemp.Substring(8).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_ACCESS;
                }
                else if (sTemp.Substring(0, 8).ToUpper() == "[ORACLE]") {
                    sTemp = sTemp.Substring(8).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_ORACLE;
                }
                else if (sTemp.Substring(0, 10).ToUpper() == "[INFORMIX]") {
                    sTemp = sTemp.Substring(10).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_INFORMIX;
                }
                else if (sTemp.Substring(0, 5).ToUpper() == "[DB2]") {
                    sTemp = sTemp.Substring(5).Trim();
                    iDBMake = (int)eDatabaseType.DBMS_IS_DB2;
		        } else {
			        iDBMake = -1;
			        // line not db specific
		        }

		        sRetVal = sTemp;

	        } catch (Exception) {
		        iDBMake = -1;
		        sRetVal = sSQL;
	        }
	        return sRetVal;
        }

        public string sFormatMsgString(string sText)
        {
	        string sRetVal = "";
	        string sWork = string.Empty;
	        short i = 0;
            string sFirstPart = string.Empty;
            string sSecondPart = string.Empty;
	        int nPos = 0;
	        //    ReDim sTMParray[0] As String

	        try {
		        // plug in carriage returns
		        nPos = sText.IndexOf("\\n");
		        sWork = sText;
		        while (nPos != -1) {
			        sFirstPart = sWork.Substring(0, nPos);
			        sSecondPart = sWork.Substring(nPos + 2); 

			        sWork = sFirstPart + Environment.NewLine + sSecondPart;
                    nPos = sText.IndexOf("\\n");
		        }

		        // plug in parameters
		        //    If Trim$(sMsgArguments) <> "" Then
		        //        sTmp = sMsgArguments
		        //        Do While sTmp <> ""
		        //            iCount = iCount + 1
		        //            ReDim Preserve sTMParray[iCount]  As String
		        //            If InStr(sTmp, Chr$(9)) Then
		        //                sTMParray[iCount] = Left(sTmp, InStr(sTmp, Chr$(9)) - 1)
		        //                sTmp = Mid(sTmp, InStr(sTmp, Chr$(9)) + 1)
		        //            Else
		        //                sTMParray[iCount] = sTmp
		        //                sTmp = ""
		        //            End If
		        //        Loop

		        for (i = 1; i <= 9; i++) {
			        while (sWork.Contains("%%" + i.ToString())) {
				        nPos = sWork.IndexOf("%%" + i.ToString());
				        sWork = sWork.Substring(0, nPos) + sParmArray[i] + sWork.Substring(nPos + 3);
			        }
		        }
		        //    End If

		        sRetVal = sWork;

	        } catch (Exception ex) {
		        throw new ECUException("Critical Error- " + ex.Message + " in {sFormatMsgString}");

	        }
	        return sRetVal;
        }


        public static string GetSystemPath()
        {
            string sRetVal = Path.GetDirectoryName(Application.ExecutablePath);
            DirectoryInfo dirInfo = Directory.GetParent(Application.ExecutablePath);
            sRetVal = dirInfo.FullName;
            if (!sRetVal.EndsWith("\\")) {
                sRetVal += "\\";
	        } 
	        return sRetVal;
        }

        public void subTblUpdtStmp(string sTableName)
        {
	        string sTmp = string.Empty;

	        try {
		        sTmp = DateTime.Now.ToString("yyyyMMddhhmmss");
                sTmp = sTmp.Substring(0, sTmp.Length - 2) + "00";
                DbFactory.ExecuteNonQuery(m_connectionString, "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + sTmp + "'  WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'");
	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.subTblUpdtStmp", ex.Message);
	        }

        }

        public int lGetStateID(string sState)
        {
	        int lIDTmp = 0;

	        try {
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID ='" + sState.Trim() + "'")) {
			        if (objReader.Read()) {
				        lIDTmp = objReader.GetInt32(0);
			        }
		        }

	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lGetStateID", ex.Message);
	        }

	        return lIDTmp;
        }

        public int lAddStateField(string sStateTable, string sUserPrompt, string sSysFieldName, int lFieldType, int lFieldSize, bool bRequired, 
                                    bool bDeleted,  bool bLookup, bool bIsPatterned, string sPattern, string sCodeTable)
        {
	        string sSQL = string.Empty;
	        int lRetVal = 0;
	        int lID = 0;
	        DbWriter objWriter = null;
            string sSysFieldNameUp = string.Empty;

	        try {
		        sSysFieldNameUp = sSysFieldName.ToUpper();

		        switch (lFieldType) {
			        // Now enforce field size  JP 4/4/2003, dcm 5/2/2003
			        case 0:
				        // normal string
				        if (lFieldSize == 0)
					        lFieldSize = 50;
				        break;
			        case 3:
				        // date (string 8)
				        lFieldSize = 8;
				        break;
			        case 4:
				        // time (string 6)
				        lFieldSize = 6;
				        break;
			        case 10:
			        case 12:
				        // claim number lookup, event number lookup (string 25)
				        lFieldSize = 25;
				        break;
			        case 13:
				        // vehicle lookup - VIN (string 20)
				        lFieldSize = 20;
				        break;
			        default:
				        // number, id, etc
				        if (lFieldSize != 0)
					        lFieldSize = 0;
				        break;
		        }

		        sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sStateTable + "'" + " AND SYS_FIELD_NAME = '" + sSysFieldNameUp + "'";

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lID = objReader.GetInt32("FIELD_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY");
				        objWriter.Fields["FIELD_ID"].Value = lID;

			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["SEQ_NUM"].Value = lID;
			        objWriter.Fields["HELP_CONTEXT_ID"].Value = objDbUpgrade.lGetNextUID("HELP_DEF");

			        objWriter.Fields["USER_PROMPT"].Value = sUserPrompt;
			        // this allows user to change prompts.  dcm 6/2003.
			        objWriter.Fields["SUPP_TABLE_NAME"].Value = sStateTable;
			        objWriter.Fields["SYS_FIELD_NAME"].Value = sSysFieldNameUp;
			        objWriter.Fields["FIELD_TYPE"].Value = lFieldType;
			        objWriter.Fields["FIELD_SIZE"].Value = lFieldSize;
			        objWriter.Fields["REQUIRED_FLAG"].Value = bRequired;
			        objWriter.Fields["DELETE_FLAG"].Value = 0;
			        objWriter.Fields["LOOKUP_FLAG"].Value = bLookup;
			        objWriter.Fields["IS_PATTERNED"].Value = bIsPatterned;
			        objWriter.Fields["PATTERN"].Value = sPattern;
			        objWriter.Fields["CODE_FILE_ID"].Value = lGetTableID(sCodeTable);
			        objWriter.Fields["GRP_ASSOC_FLAG"].Value = 0;
			        objWriter.Execute();
		        }

		        // Add field to database table if needed
		        if (lFieldType != 14 && lFieldType != 15 && lFieldType != 16) {
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " + sStateTable + " WHERE 0=1")) {
				        if (!objReader.ExistField(sSysFieldNameUp)) {
					        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize);
					        // includes NULL 6.0 dcm

					        sSQL = "ALTER TABLE " + sStateTable + " ADD " + sSysFieldNameUp + " " + sSQL;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        }
			        }
		        }

		        lRetVal = lID;

	        } catch (Exception ex) {
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddStateField", ex.Message);
	        }

	        return lRetVal;
        }


        public int lAddStateField_2(string sStateTable, string sUserPrompt, string sSysFieldName, int lFieldType, int lFieldSize,  bool bRequired, bool bDeleted, 
            bool bLookup, bool bIsPatterned,  string sPattern, string sCodeTable, bool bGroupAssoc, string sAssocField)
        {
	        string sSQL = string.Empty;
	        DbWriter objWriter = null;
	        int lID = 0;


	        try {
		        sSysFieldName = sSysFieldName.ToUpper();

		        switch (lFieldType) {
			        // Now enforce field size  JP 4/4/2003, dcm 5/2/2003
			        case 0:
				        // normal string
				        if (lFieldSize == 0)
					        lFieldSize = 50;
				        break;
			        case 3:
				        // date (string 8)
				        lFieldSize = 8;
				        break;
			        case 4:
				        // time (string 6)
				        lFieldSize = 6;
				        break;
			        case 10:
			        case 12:
				        // claim number lookup, event number lookup (string 25)
				        lFieldSize = 25;
				        break;
			        case 13:
				        // vehicle lookup - VIN (string 20)
				        lFieldSize = 20;
				        break;
			        default:
				        // number, id, etc
				        if (lFieldSize != 0)
					        lFieldSize = 0;
				        break;
		        }

		        sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sStateTable + "'" + " AND SYS_FIELD_NAME = '" + sSysFieldName + "'";

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lID = objReader.GetInt32("FIELD_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY");
				        objWriter.Fields["FIELD_ID"].Value = lID;
				        objWriter.Fields["SUPP_TABLE_NAME"].Value = sStateTable;
				        objWriter.Fields["SYS_FIELD_NAME"].Value = sSysFieldName;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["SEQ_NUM"].Value = lID;
			        objWriter.Fields["HELP_CONTEXT_ID"].Value = objDbUpgrade.lGetNextUID("HELP_DEF");

			        objWriter.Fields["USER_PROMPT"].Value = sUserPrompt;
			        // this allows user to change prompts.  dcm 6/2003.
			        objWriter.Fields["SUPP_TABLE_NAME"].Value = sStateTable;
			        objWriter.Fields["SYS_FIELD_NAME"].Value = sSysFieldName;
			        objWriter.Fields["FIELD_TYPE"].Value = lFieldType;
			        objWriter.Fields["FIELD_SIZE"].Value = lFieldSize;
			        objWriter.Fields["REQUIRED_FLAG"].Value = bRequired;
			        objWriter.Fields["DELETE_FLAG"].Value = 0;
			        objWriter.Fields["LOOKUP_FLAG"].Value = bLookup;
			        objWriter.Fields["IS_PATTERNED"].Value = bIsPatterned;
			        objWriter.Fields["PATTERN"].Value = sPattern;
			        objWriter.Fields["CODE_FILE_ID"].Value = lGetTableID(sCodeTable);
			        objWriter.Fields["GRP_ASSOC_FLAG"].Value = bGroupAssoc;
			        objWriter.Fields["ASSOC_FIELD"].Value = sAssocField;
			        objWriter.Execute();
		        }

		        // Add field to database table if needed
		        if (lFieldType != 14 && lFieldType != 15 && lFieldType != 16) {
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " + sStateTable + " WHERE 0=1")) {
				        if (!objReader.ExistField(sSysFieldName)) {
					        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize);
					        // includes NULL 6.0 dcm

					        sSQL = "ALTER TABLE " + sStateTable + " ADD " + sSysFieldName + " " + sSQL;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        }
			        }
		        }

	        } catch (Exception ex) {
		        lID = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddStateField_2", ex.Message);
	        }
	        return lID;
        }   
        
        //---------------------------------------------------------------------------------------
        // Procedure : lAddStateField_OrgHierarchyAssoc
        // Author    : jtodd22
        // Date      : 2/3/2010
        // Purpose   : MITS 19176
        //---------------------------------------------------------------------------------------
        //
        public int lAddStateField_OrgHierarchyAssoc(string sStateTable, string sUserPrompt, string sSysFieldName, int lFieldType, int lFieldSize, bool bRequired, bool bDeleted, bool bLookup, bool bIsPatterned, string sPattern,
        string sCodeTable, bool bGroupAssoc, string sAssocField, string sListOrgHierarchyIDs)
        {
	        string sSQL = string.Empty;
	        string sSQLToExe = string.Empty;
	        DbWriter objWriter = null;
	        int lID = 0;
	        string sNow = string.Empty;
	        string sValues = string.Empty;
	        int lRetVal = 0;

	        try {
		        sNow = DateTime.Now.ToString("yyyyMMddhhmmss");
		        sSysFieldName = sSysFieldName.ToUpper();
		        switch (lFieldType) {    // Now enforce field size  JP 4/4/2003, dcm 5/2/2003
			        case 0:    // normal string
				        if (lFieldSize == 0)
					        lFieldSize = 50;
				        break;
			        case 3:     // date (string 8)
				        lFieldSize = 8;
				        break;
			        case 4:     // time (string 6)
				        lFieldSize = 6;
				        break;
			        case 10:
			        case 12:     // claim number lookup, event number lookup (string 25)
				        lFieldSize = 25;
				        break;
			        case 13:     // vehicle lookup - VIN (string 20)
				        lFieldSize = 20;
				        break;
			        default:     // number, id, etc
				        if (lFieldSize != 0)
					        lFieldSize = 0;
				        break;
		        }

		        sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sStateTable + "'" + " AND SYS_FIELD_NAME = '" + sSysFieldName + "'";
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lID = objReader.GetInt32("FIELD_ID");
				        return lID;
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY");
				        objWriter.Fields["FIELD_ID"].Value = lID;
				        objWriter.Fields["SUPP_TABLE_NAME"].Value = sStateTable;
				        objWriter.Fields["SYS_FIELD_NAME"].Value = sSysFieldName;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["SEQ_NUM"].Value = lID;
			        objWriter.Fields["HELP_CONTEXT_ID"].Value = objDbUpgrade.lGetNextUID("HELP_DEF");

			        objWriter.Fields["USER_PROMPT"].Value = sUserPrompt;     // this allows user to change prompts.  dcm 6/2003.
			        objWriter.Fields["SUPP_TABLE_NAME"].Value = sStateTable;
			        objWriter.Fields["SYS_FIELD_NAME"].Value = sSysFieldName;
			        objWriter.Fields["FIELD_TYPE"].Value = lFieldType;
			        objWriter.Fields["FIELD_SIZE"].Value = lFieldSize;
			        objWriter.Fields["REQUIRED_FLAG"].Value = bRequired;
			        objWriter.Fields["DELETE_FLAG"].Value = 0;
			        objWriter.Fields["LOOKUP_FLAG"].Value = bLookup;
			        objWriter.Fields["IS_PATTERNED"].Value = bIsPatterned;
			        objWriter.Fields["PATTERN"].Value = sPattern;
			        objWriter.Fields["CODE_FILE_ID"].Value = lGetTableID(sCodeTable);
			        objWriter.Fields["GRP_ASSOC_FLAG"].Value = bGroupAssoc;
			        objWriter.Fields["ASSOC_FIELD"].Value = sAssocField;
		        }


		        // Add field to database table if needed
		        if (lFieldType != 14 && lFieldType != 15 && lFieldType != 16) {
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " + sStateTable + " WHERE 0=1")) {
				        if (!objReader.ExistField(sSysFieldName)) {
					        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize);     // includes NULL 6.0 dcm

					        sSQL = "ALTER TABLE " + sStateTable + " ADD " + sSysFieldName + " " + sSQL;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        }
			        }
		        }

		        lRetVal = lID;

		        if (!string.IsNullOrEmpty(sListOrgHierarchyIDs.Trim())) {
                    string[] sArray = sListOrgHierarchyIDs.Split(';');
			        sSQL = "";
			        sSQL = sSQL + "INSERT INTO SUPP_ASSOC";
			        sSQL = sSQL + " (FIELD_ID,ASSOC_CODE_ID,UPDATED_BY_USER,DTTM_RCD_LAST_UPD)";
			        sSQL = sSQL + " VALUES";
                    foreach(string sTmp in sArray) {
				        sValues = "(" + lID + ", " + sTmp + ", 'CSC_dbupgrade', '" + sNow + "')";
				        sSQLToExe = sSQL + sValues;
				        try {
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQLToExe);
				        } catch (Exception) {
					        sValues = "(" + lID + ", " + sTmp + ", 'dbupgrad', '" + sNow + "')";
					        sSQLToExe = sSQL + sValues;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQLToExe);
				        }
			        }

		        }
	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddStateField_OrgHierarchyAssoc", ex.Message);
	        }
	        return lRetVal;
        }



        public int lAddJurisdiction(string sStateID, string sStateName, double dEmployerLiability, double dMaxClaimAmount, short iRatingMethodCode, int iUSLLimit, int iWeightingStepVal, int iDeletedFlag)
        {
	        string sSQL = string.Empty;
	        int lID = 0;
	        int lRetVal = 0;
	        DbWriter objWriter = null;


	        try {
		        sStateID = sStateID.ToUpper();

		        sSQL = "SELECT * FROM STATES WHERE STATE_ID  = '" + sStateID + "'";
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        objWriter = DbFactory.GetDbWriter(objReader, true);
				        lRetVal = objReader.GetInt32("STATE_ROW_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("STATES");
				        objWriter.Fields["STATE_ROW_ID"].Value = lID;
				        objWriter.Fields["STATE_NAME"].Value = sStateName;
				        objWriter.Fields["EMPLOYER_LIABILITY"].Value = dEmployerLiability;
				        objWriter.Fields["MAX_CLAIM_AMOUNT"].Value = dMaxClaimAmount;
				        objWriter.Fields["RATING_METHOD_CODE"].Value = iRatingMethodCode;
				        objWriter.Fields["USL_LIMIT"].Value = iUSLLimit;
				        objWriter.Fields["WEIGHTING_STEP_VAL"].Value = iWeightingStepVal;
				        objWriter.Fields["DELETED_FLAG"].Value = iDeletedFlag;
				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["STATE_ID"].Value = sStateID;
			        objWriter.Execute();
		        }


	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddJurisdiction", ex.Message);
	        }
	        return lRetVal;
        }

        public int lAddWCPForm(short iStateRowID, short iFormCategory, string sFormTitle, string sFormName, string sFileName, short iActiveFlag, string sHashCRC, string sPrimaryFormFlag, short iLineOfBusCode)
        {

	        short iOriginalJurisRowID = 0;
	        string sSQL = string.Empty;
	        int lID = 0;
	        string sNow = string.Empty;
	        string sText = string.Empty;
	        DbWriter objWriter = null;
	        int lRetVal = 0;


	        try {
		        sNow = DateTime.Now.ToString("yyyyMMddhhmmss");

		        //jtodd22 12/19/2005 to stop issues with case of file extension
		        //jtodd22 04/23/2007 to stop issues with Informix, Oracle and RMX (Apache Tomcat) file names must be lower case
		        sFileName = sFileName.Trim();
		        sFileName = sFileName.ToLower();

		        //jtodd22 01/08/2007 to trap bad jurisdiction IDs from STATES table causing duplication errors
		        sSQL = "SELECT * FROM WCP_FORMS";

		        switch (m_dbType) {
			        case eDatabaseType.DBMS_IS_ACCESS:
				        sSQL = sSQL + " WHERE LCASE(FILE_NAME) = '" + sFileName + "'";

				        break;
			        case eDatabaseType.DBMS_IS_ORACLE:
				        sSQL = sSQL + " WHERE LOWER(FILE_NAME) = '" + sFileName + "'";

				        break;
			        default:
				        sSQL = sSQL + " WHERE LOWER(FILE_NAME) = '" + sFileName + "'";
				        break;
		        }

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        iOriginalJurisRowID = objReader.GetInt16("STATE_ROW_ID");
			        }
		        }

		        //jtodd 01/08/2007 a 0 (zero) iOriginalJurisRowID means the form does not exist in WCP_FORMS
		        if (iOriginalJurisRowID != 0) {
			        if (iOriginalJurisRowID == iStateRowID) {
				        //do nothing
			        } else {
				        sText = "";
				        sText = sText + "The application is attempting to insert a duplicate record ";
				        sText = sText + "that will cause an error." + Environment.NewLine;
				        sText = sText + "The STATES table may contain bad data." + Environment.NewLine;
				        sText = sText + "The original jurisdiction ID is " + iOriginalJurisRowID + "." + Environment.NewLine;
				        sText = sText + "The new jurisdiction ID is " + iStateRowID + "." + Environment.NewLine;

                        throw new ECUException(sText + "UpgradeUtil.lAddWCPForm" + Environment.NewLine);
			        }
		        }

		        sSQL = "SELECT * FROM WCP_FORMS";
		        sSQL = sSQL + " WHERE STATE_ROW_ID  = " + iStateRowID;
		        sSQL = sSQL + " AND FORM_CATEGORY = " + iFormCategory;

		        switch (m_dbType) {
			        case eDatabaseType.DBMS_IS_ACCESS:
				        sSQL = sSQL + " AND LCASE(FILE_NAME) = '" + sFileName + "'";

				        break;
			        case eDatabaseType.DBMS_IS_ORACLE:
				        sSQL = sSQL + " AND LOWER(FILE_NAME) = '" + sFileName + "'";

				        break;
			        default:
				        sSQL = sSQL + " AND LOWER(FILE_NAME) = '" + sFileName + "'";
				        break;
		        }

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lRetVal = objReader.GetInt32("FORM_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("WCP_FORMS");
				        objWriter.Fields["FORM_ID"].Value = lID;
				        objWriter.Fields["STATE_ROW_ID"].Value = iStateRowID;
				        objWriter.Fields["FORM_CATEGORY"].Value = iFormCategory;
				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["FORM_TITLE"].Value = sFormTitle;
			        objWriter.Fields["FORM_NAME"].Value = sFormName;
			        objWriter.Fields["FILE_NAME"].Value = sFileName;
			        objWriter.Fields["ACTIVE_FLAG"].Value = iActiveFlag;
			        objWriter.Fields["HASH_CRC"].Value = sHashCRC;
			        objWriter.Fields["PRIMARY_FORM_FLAG"].Value = sPrimaryFormFlag;
			        objWriter.Fields["LINE_OF_BUS_CODE"].Value = iLineOfBusCode;

			        if (objWriter.Fields.ExistField("UPDATED_BY_USER")) {
				        objWriter.Fields["ADDED_BY_USER"].Value = "CSCdbUpG";
				        objWriter.Fields["DTTM_RCD_ADDED"].Value = sNow;
				        objWriter.Fields["DTTM_RCD_LAST_UPD"].Value = sNow;
				        objWriter.Fields["UPDATED_BY_USER"].Value = "CSCdbUpG";
			        }

			        objWriter.Execute();
		        }
	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddWCPForm", ex.Message);
	        }
	        return lRetVal;
        }

        public int lAddMergeDictionaryLine(short iCatID, string sFieldName, string sFieldDesc, string sFieldTable, string sFieldType, string sOptMask, string sDisplayCat, string sCodeTable)
        {
	        string sSQL = string.Empty;
	        int lRetVal = 0;
	        int lID = 0;
	        DbWriter objWriter = null;


	        try {
		        sSQL = "SELECT * FROM MERGE_DICTIONARY WHERE FIELD_NAME " + (string.IsNullOrEmpty(sFieldName) ? "IS NULL" : "= '" + sFieldName + "'") + " AND FIELD_TABLE = '" + sFieldTable + "' AND CAT_ID  = " + iCatID + " AND FIELD_TYPE = " + sFieldType;
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lRetVal = objReader.GetInt32("FIELD_ID");
			        } else {
				        lID = objDbUpgrade.lGetNextUID("MERGE_DICTIONARY");
				        if (lID == 0) {
					        throw new ECUException("PSEUDO STATEMENT SYNTAX ERROR", "Could not determine the next unique field ID for MERGE_DICTIONARY.");
				        }
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        objWriter.Fields["FIELD_NAME"].Value = sFieldName;
				        objWriter.Fields["CAT_ID"].Value = iCatID;
				        objWriter.Fields["FIELD_ID"].Value = lID;

				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["FIELD_DESC"].Value = sFieldDesc;
			        objWriter.Fields["FIELD_TABLE"].Value = sFieldTable;
			        objWriter.Fields["FIELD_TYPE"].Value = sFieldType;
			        objWriter.Fields["OPT_MASK"].Value = sOptMask;
			        objWriter.Fields["DISPLAY_CAT"].Value = sDisplayCat;
			        objWriter.Fields["CODE_TABLE"].Value = sCodeTable;

			        objWriter.Execute();
		        }

	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddMergeDictionaryLine", ex.Message);
	        }
	        return lRetVal;
        }

        public int lGetMaxFieldID(ref string sTableName)
        {

	        int lResVal = 0;

	        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM " + sTableName)) {
		        if (objReader.Read()) {
			        if (objReader.IsDBNull(0)) {
				        //MsgBox "The supplemental dictionary has already been updated", 64, "UPDSUPP"
				        lResVal = 0;
			        } else {
				        lResVal = objReader.GetInt32(0) + 1;
			        }
		        }
	        }

	        return lResVal;
        }


        public int lAddCLForm(short iStateRowID, short iFormCategory, string sFormTitle, string sFormName, string sFileName, short iActiveFlag, string sHashCRC, string sPrimaryFormFlag, short iLineOfBusCode)
        {
	        //mwc 05/18/2001

	        string sSQL = string.Empty;
	        int lRetVal = 0;
	        int lID = 0;
	        DbWriter objWriter = null;


	        try {
		        sSQL = "SELECT * FROM CL_FORMS" + " WHERE STATE_ROW_ID  = " + iStateRowID + " AND FORM_CATEGORY = " + iFormCategory + " AND FILE_NAME = '" + sFileName + "'";

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lRetVal = objReader.GetInt32("FORM_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("CL_FORMS");

				        objWriter.Fields["STATE_ROW_ID"].Value = iStateRowID;
				        objWriter.Fields["FORM_CATEGORY"].Value = iFormCategory;
				        objWriter.Fields["FORM_ID"].Value = lID;
				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["FORM_TITLE"].Value = sFormTitle;
			        objWriter.Fields["FORM_NAME"].Value = sFormName;
			        objWriter.Fields["FILE_NAME"].Value = sFileName;
			        objWriter.Fields["ACTIVE_FLAG"].Value = iActiveFlag;
			        objWriter.Fields["HASH_CRC"].Value = sHashCRC;
			        objWriter.Fields["PRIMARY_FORM_FLAG"].Value = sPrimaryFormFlag;
			        objWriter.Fields["LINE_OF_BUS_CODE"].Value = iLineOfBusCode;

			        objWriter.Execute();
		        }
	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddCLForm", ex.Message);
	        }
	        return lRetVal;
        }


        public int lAddJurisdictionalFROI(short iStateRowID, string sFormName, string sFileName, short iActiveFlag, string sHashCRC, short iPrimaryFormFlag, short iLineOfBusCode)
        {

	        string sSQL = string.Empty;
	        int lRetVal = 0;
	        int lID = 0;
	        DbWriter objWriter = null;


	        try {
		        sSQL = "SELECT * FROM JURIS_FORMS" + " WHERE STATE_ROW_ID  = " + iStateRowID + " AND PDF_FILE_NAME = '" + sFileName + "'";

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lRetVal = objReader.GetInt32("FORM_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("JURIS_FORMS");

				        objWriter.Fields["STATE_ROW_ID"].Value = iStateRowID;
				        objWriter.Fields["FORM_ID"].Value = lID;
				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["FORM_NAME"].Value = sFormName;
			        objWriter.Fields["PDF_FILE_NAME"].Value = sFileName;
			        objWriter.Fields["ACTIVE_FLAG"].Value = iActiveFlag;
			        objWriter.Fields["HASH_CRC"].Value = sHashCRC;
			        objWriter.Fields["PRIMARY_FORM_FLAG"].Value = iPrimaryFormFlag;
			        objWriter.Fields["LINE_OF_BUS_CODE"].Value = iLineOfBusCode;

			        objWriter.Execute();
		        }

	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddJurisdictionalFROI", ex.Message);
	        }

	        return lRetVal;
        }

        public int lAddEVForm(short iStateRowID, short iFormCategory, string sFormTitle, string sFormName, string sFileName, short iActiveFlag, string sHashCRC, string sPrimaryFormFlag, short iLineOfBusCode)
        {
	        //mwc 05/18/2001

	        string sSQL = string.Empty;
	        int lRetVal = 0;
	        int lID = 0;
	        DbWriter objWriter = null;


	        try {
		        sSQL = "SELECT * FROM EV_FORMS" + " WHERE STATE_ROW_ID  = " + iStateRowID + " AND FORM_CATEGORY = " + iFormCategory + " AND FILE_NAME = '" + sFileName + "'";

		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
			        if (objReader.Read()) {
				        lRetVal = objReader.GetInt32("FORM_ID");
			        } else {
				        objWriter = DbFactory.GetDbWriter(objReader, false);
				        objWriter.Where.Clear();
				        lID = objDbUpgrade.lGetNextUID("EV_FORMS");

				        objWriter.Fields["STATE_ROW_ID"].Value = iStateRowID;
				        objWriter.Fields["FORM_CATEGORY"].Value = iFormCategory;
				        objWriter.Fields["FORM_ID"].Value = lID;
				        lRetVal = lID;
			        }
		        }
		        if ((objWriter != null)) {
			        objWriter.Fields["FORM_TITLE"].Value = sFormTitle;
			        objWriter.Fields["FORM_NAME"].Value = sFormName;
			        objWriter.Fields["FILE_NAME"].Value = sFileName;
			        objWriter.Fields["ACTIVE_FLAG"].Value = iActiveFlag;
			        objWriter.Fields["HASH_CRC"].Value = sHashCRC;
			        objWriter.Fields["PRIMARY_FORM_FLAG"].Value = sPrimaryFormFlag;
			        objWriter.Fields["LINE_OF_BUS_CODE"].Value = iLineOfBusCode;

			        objWriter.Execute();
		        }

	        } catch (Exception ex) {
		        lRetVal = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lAddEVForm", ex.Message);
	        }
	        return lRetVal;
        }
        public string sSQLStringLiteral(string sText)
        {
	        return sText.Replace("'", "''");
        }


        public int lDataBaseUpgrade_A()
        {
	        int functionReturnValue = 0;
	        string sSQL = string.Empty;
	        int result = 0;


	        try {
		        sSQL = "DELETE FROM WCP_FORMS WHERE FILE_NAME IS NULL";
		        try {
			        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
		        } catch (Exception ex) {
                    if (isCriticalError(ex.Message))
                    {
				        throw ex;
			        }
		        }
		        //rpandey20 02/27/2009  -- MITS 14446 Removing duplicate rows from table ******
		        sSQL = " DELETE  FROM WCP_FORMS WHERE FORM_ID NOT IN ( ";
		        sSQL = sSQL + " SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 GROUP BY FILE_NAME ";
		        sSQL = sSQL + " UNION ";
		        sSQL = sSQL + "SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=-1 GROUP BY FILE_NAME)";
		        try {
			        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
		        } catch (Exception ex) {
                    if (isCriticalError(ex.Message))
                    {
				        throw ex;
			        }
		        }
		        //-------------------------------------------------------------
		        sSQL = " DELETE FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 AND FILE_NAME IN  ";
		        sSQL = sSQL + " (SELECT FILE_NAME FROM WCP_FORMS GROUP BY FILE_NAME HAVING COUNT(FILE_NAME)> 1)  ";
		        try {
			        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
		        } catch (Exception ex) {
                    if (isCriticalError(ex.Message))
                    {
				        throw ex;
			        }
		        }
		        //****************************************

		        switch (m_dbType) {
			        //npradeepshar 09/28/2010 --MITS 22243Commented as we no longer support these datbsses
			        // Case DBMS_IS_ACCESS
			        //jtodd22 03/18/2008 --MITS 11819
			        // On Error Resume Next
			        //  sSQL = "DROP INDEX RM_WCP_FORMS ON WCP_FORMS"
			        // result = DB_SQLExecute(dbLookup, sSQL)
			        //  If Err.Number > 0 Then Err.Clear
			        // On Error GoTo hError
			        // sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME TEXT(64) NOT NULL"
			        // result = DB_SQLExecute(dbLookup, sSQL)
			        // sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
			        // result = DB_SQLExecute(dbLookup, sSQL)

			        //Case DBMS_IS_DB2
			        //jtodd22 03/18/2008 --MITS 11819
			        //On Error Resume Next
			        //sSQL = "DROP INDEX RM_WCP_FORMS"
			        //result = DB_SQLExecute(dbLookup, sSQL)
			        //If Err.Number > 0 Then Err.Clear
			        //On Error GoTo hError
			        //sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME VARCHAR(64) NOT NULL"
			        //result = DB_SQLExecute(dbLookup, sSQL)
			        //sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
			        //result = DB_SQLExecute(dbLookup, sSQL)

			        //Case DBMS_IS_INFORMIX
			        //jtodd22 03/18/2008 --MITS 11819
			        //On Error Resume Next
			        //sSQL = "DROP INDEX RM_WCP_FORMS"
			        //result = DB_SQLExecute(dbLookup, sSQL)
			        //If Err.Number > 0 Then Err.Clear
			        //On Error GoTo hError
			        //sSQL = "ALTER TABLE WCP_FORMS MODIFY (FILE_NAME VARCHAR(64) NOT NULL)"
			        //result = DB_SQLExecute(dbLookup, sSQL)
			        //sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
			        //result = DB_SQLExecute(dbLookup, sSQL)

			        case eDatabaseType.DBMS_IS_ORACLE:
				        //jtodd22 03/18/2008 --MITS 11819_
				        //npradeepshar 09/28/2010 --MITS 22243 Drop two indexes and created the new one only
				        sSQL = "DROP INDEX RM_WCP_FORMS";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch {
					        // The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
				        }
				        sSQL = "ALTER TABLE WCP_FORMS DROP CONSTRAINT IDX_WF_FILE_NAME";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch {
					        // The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
				        }
				        sSQL = "ALTER TABLE WCP_FORMS MODIFY (FILE_NAME VARCHAR2(64) NOT NULL)";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch (Exception ex) {
                            if (isCriticalError(ex.Message))
                            {
						        throw ex;
					        }
				        }
				        sSQL = "ALTER TABLE WCP_FORMS ADD CONSTRAINT IDX_WF_FILE_NAME UNIQUE (FILE_NAME)";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch (Exception ex) {
                            if (isCriticalError(ex.Message))
                            {
						        throw ex;
					        }
				        }

				        break;
			        default:
				        //npradeepshar 09/28/2010 --MITS 22243 Dropped two indexes and created the new one only
				        sSQL = "DROP INDEX WCP_FORMS.RM_WCP_FORMS";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch {
					        // The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
				        }
				        sSQL = "ALTER TABLE WCP_FORMS DROP CONSTRAINT IDX_WF_FILE_NAME";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch {
					        // The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
				        }
				        sSQL = "ALTER TABLE WCP_FORMS ALTER COLUMN FILE_NAME VARCHAR(64) NOT NULL";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch (Exception ex) {
                            if (isCriticalError(ex.Message))
                            {
						        throw ex;
					        }
				        }
				        sSQL = "ALTER TABLE WCP_FORMS ADD CONSTRAINT IDX_WF_FILE_NAME UNIQUE (FILE_NAME)";
				        try {
					        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        } catch (Exception ex) {
                            if (isCriticalError(ex.Message))
                            {
						        throw ex;
					        }
				        }

				        break;
		        }
		        functionReturnValue = 1;


	        } catch (Exception ex) {
		        functionReturnValue = 0;
		        //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.lDataBaseUpgrade_A", ex.Message);
	        }
	        return functionReturnValue;

        }

        public bool isCriticalError(string sError)
        {
            bool bRetVal = true;

            switch (m_dbType)
            {
                case eDatabaseType.DBMS_IS_ACCESS:
                    break;
                case eDatabaseType.DBMS_IS_DB2:
                    break;
                case eDatabaseType.DBMS_IS_INFORMIX:
                    break;
                case eDatabaseType.DBMS_IS_ORACLE:
                    if (sError.StartsWith("[ODBC Error=S1000] [Native Error=955] [Oracle][ODBC][Ora]ORA-00955:", StringComparison.Ordinal))
                    {
                        //name is already used by an existing object duplcate value
                        //jtodd22 01/25/2008--let it crash
                    }
                    if (sError.StartsWith("[ODBC Error=S1000] [Native Error=1430] [Oracle][ODBC][Ora]ORA-01430:", StringComparison.Ordinal))
                    {
                        //column being added already exists in table
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=S1000] [Native Error=1442] [Oracle][ODBC][Ora]ORA-01442:", StringComparison.Ordinal))
                    {
                        //column to be modified to NOT NULL is already NOT NULL
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=42S11] [Native Error=1408] [Oracle][ODBC][Ora]ORA-01408:", StringComparison.Ordinal))
                    {
                        //such column list already indexed
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=42S12] [Native Error=1418] [Oracle][ODBC][Ora]ORA-01418:", StringComparison.Ordinal))
                    {
                        //specified index does not exist
                        return false;
                    }

                    break;
                default:
                    if (sError.StartsWith("[ODBC Error=S0011] [Native Error=1913]", StringComparison.Ordinal))
                    {
                        //There is already an index on table
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=S0012] [Native Error=3703]", StringComparison.Ordinal))
                    {
                        //There is already an index on table
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=S0021] [Native Error=2705]", StringComparison.Ordinal))
                    {
                        //Column names in each table must be unique.
                        return false;
                    }
                    if (sError.StartsWith("[ODBC Error=23000] [Native Error=1505] [Microsoft][ODBC SQL Server Driver][SQL Server]", StringComparison.Ordinal))
                    {
                        //CREATE UNIQUE INDEX terminated because a duplicate key was found for index ID 13. Most significant primary key is
                        //jtodd22 01/25/2008--let it crash
                    }

                    break;
            }
            return bRetVal;
        }

    }
}
