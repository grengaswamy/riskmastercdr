﻿using System;
using System.Text;
using System.IO;
using System.Configuration;
using System.Windows.Forms;

namespace GenericUpgrade
{
    public static class ExceptionLogger
    {
        /// <summary>
        /// Adds the occured exception information in the log file. If the log file does not exist, it creates the file first.
        /// </summary>
        /// <param name="ex">Exception information.</param>
        /// 
        public static void AddException(Exception ex)
        {
            try
            {
                string fileName = getLogFileName();
                using (StreamWriter logFile = new StreamWriter(fileName, true))
                {
                    logFile.WriteLine(new string('=', 80));
                    logFile.WriteLine("Exception date: " + DateTime.Now.ToString("[yyyyMMdd hh:mm:ss] "));
                    logFile.WriteLine("Message: " + ex.Message);
                    logFile.WriteLine("StackTrace: " + ex.StackTrace);
                    logFile.Flush();
                }
            }
            catch (Exception)
            {
                // error on saving exception; nothing to do
            }
        }

        /// <summary>
        /// Adds the provided information in the log file. If the log file does not exist, it creates the file first.
        /// </summary>
        /// <param name="sMethod">Method that exception occured.</param>
        /// <param name="sMessage">Exception message.</param>
        public static void AddMessage(string sMethod, string sMessage)
        {
            try
            {
                string fileName = getLogFileName();
                using (StreamWriter logFile = new StreamWriter(fileName, true))
                {
                    logFile.WriteLine(new string('=', 80));
                    logFile.WriteLine("Exception date: " + DateTime.Now.ToString("[yyyyMMdd hh:mm:ss] "));
                    logFile.WriteLine("Method Name: " + sMethod);
                    logFile.WriteLine("Message: " + sMessage);
                    logFile.Flush();
                }
            }
            catch (Exception) {
                // error on saving exception; nothing to do
            }

        }

        private static string getLogFileName()
        {
            string logFilePath = Path.GetDirectoryName(Application.ExecutablePath);
            string fileName = ConfigurationManager.AppSettings["LogFileName"].ToString();
            return Path.Combine(logFilePath, fileName);
        }
    }
}
