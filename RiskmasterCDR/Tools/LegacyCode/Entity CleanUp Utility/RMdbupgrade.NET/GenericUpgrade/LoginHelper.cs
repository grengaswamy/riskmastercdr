﻿using System;
using System.Text;
using System.Collections.Generic;

using System.Configuration;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Db;

namespace GenericUpgrade
{



    public class LoginHelper
    {
        private Riskmaster.Security.Login objLogin;
        private String securityDSN;

        public LoginHelper()
        {
            try
            {
                String sSecurityConnStr = ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
                objLogin = new Riskmaster.Security.Login(sSecurityConnStr);
                securityDSN = objLogin.SecurityDsn;
            }
            catch (Exception ex)
            {
                throw new RMAppException(StringResources.LoginHelper_Login_Exception, ex);
            }
        }

        public Dictionary<Int32,String> GetDatabases()
        {
            Dictionary<Int32,String> dsnList = new Dictionary<Int32,string>(); 
            try
            {
                String sSql = "SELECT * FROM DATA_SOURCE_TABLE ORDER BY DSN";

                using (DbReader objReader = DbFactory.GetDbReader(securityDSN, sSql))
                {
                    while (objReader.Read())
                    {
                        Int32 dsnId = objReader.GetInt32("DSNID");
                        String dsn = objReader.GetString("DSN");
                        dsnList.Add(dsnId, dsn);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new RMAppException(StringResources.LoginHelper_GetDatabases_Exception, ex);
            }
            return dsnList;
        }

        public String SecurityDsn
        {
            get { return securityDSN; } 
        }

    }
}
