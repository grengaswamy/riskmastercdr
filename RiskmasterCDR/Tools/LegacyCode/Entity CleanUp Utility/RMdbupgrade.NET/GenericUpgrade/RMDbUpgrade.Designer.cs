﻿namespace GenericUpgrade
{
    partial class RMDbUpgrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RMDbUpgrade));
            this.lblPickDb = new System.Windows.Forms.Label();
            this.lstDBs = new System.Windows.Forms.ListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.chkMultiDB = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblPickDb
            // 
            this.lblPickDb.AutoSize = true;
            this.lblPickDb.Location = new System.Drawing.Point(12, 9);
            this.lblPickDb.Name = "lblPickDb";
            this.lblPickDb.Size = new System.Drawing.Size(136, 13);
            this.lblPickDb.TabIndex = 0;
            this.lblPickDb.Text = "Pick Database to Upgrade:";
            // 
            // lstDBs
            // 
            this.lstDBs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDBs.FormattingEnabled = true;
            this.lstDBs.Location = new System.Drawing.Point(9, 30);
            this.lstDBs.Name = "lstDBs";
            this.lstDBs.Size = new System.Drawing.Size(305, 199);
            this.lstDBs.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(183, 245);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(131, 30);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpgrade.Location = new System.Drawing.Point(9, 245);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(131, 30);
            this.btnUpgrade.TabIndex = 4;
            this.btnUpgrade.Text = "Upgrade";
            this.btnUpgrade.UseVisualStyleBackColor = true;
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // chkMultiDB
            // 
            this.chkMultiDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkMultiDB.AutoSize = true;
            this.chkMultiDB.Checked = true;
            this.chkMultiDB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMultiDB.Location = new System.Drawing.Point(16, 291);
            this.chkMultiDB.Name = "chkMultiDB";
            this.chkMultiDB.Size = new System.Drawing.Size(154, 17);
            this.chkMultiDB.TabIndex = 5;
            this.chkMultiDB.Text = "Update Multiple Databases";
            this.chkMultiDB.UseVisualStyleBackColor = true;
            // 
            // RMDbUpgrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 317);
            this.Controls.Add(this.chkMultiDB);
            this.Controls.Add(this.btnUpgrade);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lstDBs);
            this.Controls.Add(this.lblPickDb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(340, 345);
            this.Name = "RMDbUpgrade";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RM Database Upgrade";
            this.Load += new System.EventHandler(this.RMDbUpgrade_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPickDb;
        private System.Windows.Forms.ListBox lstDBs;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnUpgrade;
        private System.Windows.Forms.CheckBox chkMultiDB;
    }
}

