﻿namespace GenericUpgrade
{
    partial class Upgrading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Upgrading));
            this.lblCompletePer = new System.Windows.Forms.Label();
            this.lblProgressMeter = new System.Windows.Forms.Label();
            this.lblExecuting = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblDb = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCompletePer
            // 
            this.lblCompletePer.AutoSize = true;
            this.lblCompletePer.Location = new System.Drawing.Point(15, 17);
            this.lblCompletePer.Name = "lblCompletePer";
            this.lblCompletePer.Size = new System.Drawing.Size(65, 13);
            this.lblCompletePer.TabIndex = 0;
            this.lblCompletePer.Text = "% Complete:";
            // 
            // lblProgressMeter
            // 
            this.lblProgressMeter.AutoSize = true;
            this.lblProgressMeter.Location = new System.Drawing.Point(101, 17);
            this.lblProgressMeter.Name = "lblProgressMeter";
            this.lblProgressMeter.Size = new System.Drawing.Size(13, 13);
            this.lblProgressMeter.TabIndex = 1;
            this.lblProgressMeter.Text = "0";
            // 
            // lblExecuting
            // 
            this.lblExecuting.AutoSize = true;
            this.lblExecuting.Location = new System.Drawing.Point(19, 44);
            this.lblExecuting.Name = "lblExecuting";
            this.lblExecuting.Size = new System.Drawing.Size(57, 13);
            this.lblExecuting.TabIndex = 2;
            this.lblExecuting.Text = "Executing:";
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfo.BackColor = System.Drawing.SystemColors.Window;
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblInfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblInfo.Location = new System.Drawing.Point(12, 81);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblInfo.Size = new System.Drawing.Size(310, 192);
            this.lblInfo.TabIndex = 1;
            // 
            // lblDb
            // 
            this.lblDb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDb.Location = new System.Drawing.Point(101, 44);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(221, 13);
            this.lblDb.TabIndex = 4;
            // 
            // lblFileName
            // 
            this.lblFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFileName.Location = new System.Drawing.Point(101, 62);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(221, 13);
            this.lblFileName.TabIndex = 5;
            // 
            // Upgrading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 282);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblExecuting);
            this.Controls.Add(this.lblProgressMeter);
            this.Controls.Add(this.lblCompletePer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(350, 320);
            this.Name = "Upgrading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RM Database Upgrade";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Upgrading_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCompletePer;
        private System.Windows.Forms.Label lblProgressMeter;
        private System.Windows.Forms.Label lblExecuting;
        public System.Windows.Forms.Label lblInfo;
        public System.Windows.Forms.Label lblDb;
        public System.Windows.Forms.Label lblFileName;
    }
}