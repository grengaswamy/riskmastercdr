﻿using System;
using System.Text;

namespace GenericUpgrade
{

    public class ProgressEventArgs : System.EventArgs
    {

	    private int m_progressStatus;
	    public int ProgressStatus {
		    get { return this.m_progressStatus; }
		    set { m_progressStatus = value; }
	    }

	    private string m_infoLine;
	    public string InfoLine {
		    get { return this.m_infoLine; }
		    set { m_infoLine = value; }
	    }

	    public ProgressEventArgs(int status, string line)
	    {
		    m_progressStatus = status;
		    m_infoLine = line;
	    }
    }

}
