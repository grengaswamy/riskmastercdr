﻿using System;
using System.Text;

using System.Configuration;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Db;

namespace GenericUpgrade
{
    public class DbUpgrade
    {
        
        private String m_connectionString;

        // db manufacturer type
        public eDatabaseType DbType;

        public DbUpgrade(String connectionString)
        {
            m_connectionString = connectionString;
            DbType = DbFactory.GetDatabaseType(m_connectionString);
        }

        public int lGetNextUID(string p_sTableNameSystem)
        {
	        int iNextId = 0;
	        try {
		        using (DbConnection dbConnection = DbFactory.GetDbConnection(m_connectionString)) {
			        dbConnection.Open();
			        using (DbTransaction dbTrans = dbConnection.BeginTransaction()) {
				        //Get current Id
				        int iCurrentId = 0;
				        using (DbReader objReader = dbConnection.ExecuteReader("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableNameSystem + "'", dbTrans)) {
					        objReader.Read();
					        iCurrentId = objReader.GetInt32(0);
				        }
				        iNextId = iCurrentId + 1;

				        //Save new Id
				        string sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextId;
				        sSQL = sSQL + " WHERE SYSTEM_TABLE_NAME = '" + p_sTableNameSystem + "'";
				        sSQL = sSQL + " AND NEXT_UNIQUE_ID = " + iCurrentId;
				        dbConnection.ExecuteNonQuery(sSQL, dbTrans);
				        dbTrans.Commit();
			        }
		        }
	        } catch (Exception ex) {
                ExceptionLogger.AddMessage("UpgradeUtil.lGetNextUID", ex.Message); 
		        throw;
	        }

	        return iNextId;
        }

        public string sTrimNull(ref string sArg)
        {
	        string functionReturnValue = String.Empty;
	        int index = 0;
	        index = sArg.IndexOf('\0');
	        if (index > 0) {
		        functionReturnValue = sArg.Substring(0, index - 1);
	        } else {
		        functionReturnValue = sArg;
	        }
	        return functionReturnValue;
        }

        //***************************************************************************************
        // Procedure : StorageClause
        // Date      : 9//99
        // Author    : Dave McAninch (dcm)
        // Purpose   : This routine creates a storage clause (sSQL) for a Create Table statement
        //             based on a similar Table (sTableName).
        //***************************************************************************************
        public void StorageClause(ref string sSQL, ref string sTableName)
        {
	        string sTableSpace = string.Empty;
	        int lInitExt = 0;
	        int lNextExt = 0;

	        switch (DbType) {
		        case eDatabaseType.DBMS_IS_ORACLE:
			        string localSql = "SELECT TABLESPACE_NAME, INITIAL_EXTENT, NEXT_EXTENT FROM USER_TABLES WHERE TABLE_NAME = '" + sTableName + "'";
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, localSql)) {
				        if (objReader.Read()) {
					        sTableSpace = objReader.GetString("TABLESPACE_NAME");
					        lInitExt = objReader.GetInt32("INITIAL_EXTENT");
					        lNextExt = objReader.GetInt32("NEXT_EXTENT");
					        sSQL = " PCTFREE 10 PCTUSED 40 TABLESPACE " + sTableSpace;
					        sSQL = sSQL + " STORAGE (INITIAL " + lInitExt + " NEXT " + lNextExt;
					        sSQL = sSQL + " MINEXTENTS 1 MAXEXTENTS 121 PCTINCREASE 1)";
				        }
			        }
			        break;
	        }
        }


        //***************************************************************************************
        // Procedure : sAddField
        // Purpose   : Construct field descriptors
        //***************************************************************************************
        public string sAddField(ref string sTableName, ref DbReader objReader)
        {
	        string sSQL = string.Empty;
	        int lfieldid = 0;
	        string sFieldName = string.Empty;
	        short nFieldType = 0;
	        short nFieldSize = 0;
	        DbWriter objWriter = default(DbWriter);
	        string sRetValue = string.Empty;


	        try {
		        using (DbReader objReader1 = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM SUPP_DICTIONARY WHERE 0=1")) {
			        objReader1.Read();
			        objWriter = DbFactory.GetDbWriter(objReader1, false);
			        objWriter.Where.Clear();
			        // Clear Where: 0 = 1
		        }
		        // add database entry for table
		        lfieldid = lGetNextUID("SUPP_DICTIONARY");

		        // automatically assign new field to end of supp. list
		        objWriter.Fields["FIELD_ID"].Value = lfieldid;
		        objWriter.Fields["SEQ_NUM"].Value = lfieldid;

		        sFieldName = objReader.GetString("SYS_FIELD_NAME");
		        nFieldType = objReader.GetInt16("FIELD_TYPE");
		        nFieldSize = objReader.GetInt16("FIELD_SIZE");

		        // plug in data
		        objWriter.Fields["SUPP_TABLE_NAME"].Value = sTableName;
		        objWriter.Fields["USER_PROMPT"].Value = objReader.GetString("USER_PROMPT");
		        objWriter.Fields["SYS_FIELD_NAME"].Value = sFieldName;
		        objWriter.Fields["FIELD_TYPE"].Value = nFieldType;
		        objWriter.Fields["FIELD_SIZE"].Value = nFieldSize;
		        objWriter.Fields["REQUIRED_FLAG"].Value = objReader.GetInt16("REQUIRED_FLAG");

		        objWriter.Fields["DELETE_FLAG"].Value = 0;
		        objWriter.Fields["IS_PATTERNED"].Value = objReader.GetInt16("IS_PATTERNED");
		        objWriter.Fields["PATTERN"].Value = objReader.GetString("PATTERN");
		        objWriter.Fields["LOOKUP_FLAG"].Value = objReader.GetInt16("LOOKUP_FLAG");

		        objWriter.Fields["CODE_FILE_ID"].Value = objReader.GetInt32("CODE_FILE_ID");
		        objWriter.Fields["GRP_ASSOC_FLAG"].Value = objReader.GetInt16("GRP_ASSOC_FLAG");
		        objWriter.Fields["ASSOC_FIELD"].Value = objReader.GetString("ASSOC_FIELD");
		        objWriter.Fields["HELP_CONTEXT_ID"].Value = objReader.GetInt32("HELP_CONTEXT_ID");

		        objWriter.Execute();

		        //change supp_assoc old field_id (for this state) to new field_id
		        if (sFieldName != "CLAIM_ID") {
			        sSQL = "UPDATE SUPP_ASSOC SET FIELD_ID = " + lfieldid;
			        sSQL = sSQL + " WHERE FIELD_ID = " + objReader.GetString("FIELD_ID");
			        sSQL = sSQL + " AND ASSOC_CODE_ID = " + objReader.GetString("STATE_ROW_ID");
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
		        }

		        sSQL = sFieldName.Trim().ToUpper() + " " + sTypeMap(nFieldType, nFieldSize);
		        // includes NULL 6.0 dcm
		        sRetValue = sSQL;
	        } catch (Exception ex) {
                ExceptionLogger.AddMessage("UpgradeUtil.sAddField", ex.Message); 
                throw new ECUException("Index Creation Error", "Index Creation Error: An error occurred creating the field. The database server could " + "not create the field. Possible causes may be that the field already exists, the field name is already taken, the field name includes illegal characters or whitespace, or the table of the field is in use by another user. The database server error message will follow (click OK).");
	        }
	        return sRetValue;
        }

       //***************************************************************************************
    // Procedure : sTypeMap
    // Purpose   : Translates RM supplemental type to DB type string
    // added NULL in version 6.0 dcm
    //***************************************************************************************
    public string sTypeMap(int nFieldType, int nSize)
    {
	    string sType = string.Empty;

	    // Map type
	    switch (nFieldType) {
		    case 0:
		    case 3:
		    case 4:
		    case 10:
		    case 12:
		    case 13:
			    // string, date, time, claim lookup, event lookup, vehicle lookup,
			    switch (DbType) {
				    case eDatabaseType.DBMS_IS_SQLSRVR:
				    case eDatabaseType.DBMS_IS_SYBASE:
				    case eDatabaseType.DBMS_IS_INFORMIX:
				    case eDatabaseType.DBMS_IS_DB2:
					    sType = "VARCHAR";
					    break;
				    case eDatabaseType.DBMS_IS_ORACLE:
					    sType = "VARCHAR2";
					    break;
				    case eDatabaseType.DBMS_IS_ACCESS:
					    sType = "TEXT";
					    break;
			    }

			    // Add size
			    sType = sType + "(" + nSize + ")";

			    break;
		    case 6:
		    case 7:
		    case 8:
		    case 9:
		    case 14:
		    case 15:
		    case 16:
			    // code, primary key, entity, state, multi-code, multi-entity, multi-state
			    switch (DbType) {
				    case eDatabaseType.DBMS_IS_INFORMIX:
				    case eDatabaseType.DBMS_IS_DB2:
					    sType = "INTEGER";
					    break;
				    case eDatabaseType.DBMS_IS_SQLSRVR:
				    case eDatabaseType.DBMS_IS_SYBASE:
					    sType = "INT";
					    break;
				    case eDatabaseType.DBMS_IS_ORACLE:
					    sType = "NUMBER(10,0)";
					    break;
				    case eDatabaseType.DBMS_IS_ACCESS:
					    sType = "LONG";
					    break;
			    }

			    break;
		    case 1:
		    case 2:
			    // number, currency (float)
			    switch (DbType) {
				    case eDatabaseType.DBMS_IS_SQLSRVR:
				    case eDatabaseType.DBMS_IS_SYBASE:
				    case eDatabaseType.DBMS_IS_INFORMIX:
				    case eDatabaseType.DBMS_IS_DB2:
					    sType = "FLOAT";
					    break;
				    case eDatabaseType.DBMS_IS_ORACLE:
					    sType = "NUMBER";
					    break;
				    case eDatabaseType.DBMS_IS_ACCESS:
					    sType = "DOUBLE";
					    break;
			    }

			    break;
		    case 5:
		    case 11:
			    // free text (and old multi-valued)
			    switch (DbType) {
				    case eDatabaseType.DBMS_IS_SQLSRVR:
				    case eDatabaseType.DBMS_IS_SYBASE:
				    case eDatabaseType.DBMS_IS_INFORMIX:
					    sType = "TEXT";
					    break;
				    case eDatabaseType.DBMS_IS_DB2:
					    sType = "CLOB(10M)";
					    break;
				    case eDatabaseType.DBMS_IS_ORACLE:
					    sType = "VARCHAR2(2000)";
					    break;
				    case eDatabaseType.DBMS_IS_ACCESS:
					    sType = "MEMO";
					    break;
			    }
			    break;
	    }

	    // INFORMIX allows NULL by default and doesn't allow "NULL" in statement
	    // added NULL to this routine in 6.0. dcm 1/3/03
	    if (DbType == eDatabaseType.DBMS_IS_SQLSRVR || DbType == eDatabaseType.DBMS_IS_SYBASE || DbType == eDatabaseType.DBMS_IS_ORACLE) {
		    sType = sType + " NULL";
	    }

	    return sType;
    }

        //***************************************************************************************
        // Procedure : subUpdateSupp
        // Date      : 1/3/2003
        // Author    : This routine is from a program Jim Partin (jap) and Jainagaraj Raju Naragaran (rjn).
        // Purpose   : Run the UpdateSupp/SuppUpdate on all databases
        //***************************************************************************************
        public void subUpdateSupp()
        {
	        int newFieldID = 0;
	        string sSQL = string.Empty;
	        int recdsUpdated = 1;
	        int lTmp = 0;
	        int lTmp2 = 0;
	        bool bHasGreater = false;

	        try {
		        //first check if there are supp fields with ID > 20000
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM SUPP_DICTIONARY WHERE FIELD_ID >= 20000")) {
			        if (objReader.Read()) {
				        if (!objReader.IsDBNull(0)) {
					        bHasGreater = true;
				        }
			        }
		        }
		        if (bHasGreater) {
			        //get the max id before 20000
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM SUPP_DICTIONARY WHERE FIELD_ID < 20000")) {
				        if (objReader.Read()) {
					        newFieldID = objReader.GetInt32(0);
				        }
			        }
			        newFieldID = newFieldID + 1;

			        //start updating supp fields
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE FIELD_ID >= 20000 ORDER BY FIELD_ID")) {
				        while (objReader.Read()) {
					        int oldFieldId = objReader.GetInt32(0);
					        sSQL = "UPDATE SUPP_DICTIONARY SET FIELD_ID = " + newFieldID + " WHERE FIELD_ID = " + oldFieldId;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

					        sSQL = "UPDATE SUPP_ASSOC SET FIELD_ID = " + newFieldID + " WHERE FIELD_ID = " + oldFieldId;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

					        sSQL = "UPDATE MERGE_FORM_DEF SET FIELD_ID = " + newFieldID + " WHERE FIELD_ID = " + oldFieldId;
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

					        newFieldID = newFieldID + 1;

					        recdsUpdated = recdsUpdated + 1;
				        }
			        }

			        //update glossary to set next unique id
			        sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + newFieldID + " WHERE SYSTEM_TABLE_NAME = 'SUPP_DICTIONARY'";
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
			        sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + newFieldID + " WHERE SYSTEM_TABLE_NAME = 'SUPP_ASSOC'";
			        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

		        }

		        // delete extra records.
		        DbFactory.ExecuteNonQuery(m_connectionString, "DELETE FROM SUPP_ASSOC WHERE FIELD_ID NOT IN (SELECT FIELD_ID FROM SUPP_DICTIONARY)");

		        //MJH 11/9/99
		        DbFactory.ExecuteNonQuery(m_connectionString, "DELETE FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='WC_STATE_SUPP' AND SYS_FIELD_NAME='CLAIM_ID')");

		        bool hasMaxHelpContext = false;
		        // Now, check help context ID's
		        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(HELP_CONTEXT_ID) FROM SUPP_DICTIONARY WHERE HELP_CONTEXT_ID < 0")) {
			        if (objReader.Read()) {
				        hasMaxHelpContext = true;
				        lTmp = objReader.GetInt32(0);
			        }
		        }

		        if (hasMaxHelpContext) {
			        lTmp = lTmp + 1;

			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'HELP_DEF'")) {
				        if (objReader.Read()) {
					        lTmp2 = objReader.GetInt32(0);
				        }
			        }

			        // ... now, fix any supp_dictionary items with duplicate help ids
			        if (DbType == eDatabaseType.DBMS_IS_ORACLE) {
				        sSQL = "create or replace function getSuppCount (lhelpid IN NUMBER) RETURN NUMBER IS suppCount NUMBER;";
				        sSQL = sSQL + Environment.NewLine + "BEGIN";
				        sSQL = sSQL + Environment.NewLine + "SELECT COUNT(*) INTO suppCount FROM SUPP_DICTIONARY where HELP_CONTEXT_ID = lhelpid and DELETE_FLAG = 0;";

				        sSQL = sSQL + Environment.NewLine + "return(suppCount);";
				        sSQL = sSQL + Environment.NewLine + "EXCEPTION";
				        sSQL = sSQL + Environment.NewLine + "WHEN OTHERS";
				        sSQL = sSQL + Environment.NewLine + "THEN return(0);";
				        sSQL = sSQL + Environment.NewLine + "END;";
				        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
				        sSQL = "select FIELD_ID,HELP_CONTEXT_ID,REQUIRED_FLAG from SUPP_DICTIONARY SD1 where getSuppCount(SD1.HELP_CONTEXT_ID) > 1 and DELETE_FLAG = 0 and HELP_CONTEXT_ID <> 0 order by HELP_CONTEXT_ID";
			        } else {
				        sSQL = "select FIELD_ID,HELP_CONTEXT_ID,REQUIRED_FLAG from SUPP_DICTIONARY SD1 where (SELECT COUNT(*) FROM SUPP_DICTIONARY SD2 where SD2.HELP_CONTEXT_ID = SD1.HELP_CONTEXT_ID and SD2.DELETE_FLAG = 0 ) > 1 and DELETE_FLAG = 0 and HELP_CONTEXT_ID <> 0 order by HELP_CONTEXT_ID";
			        }
			        using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL)) {
				        while (objReader.Read()) {
					        sSQL = "UPDATE SUPP_DICTIONARY SET HELP_CONTEXT_ID = " + lTmp + " WHERE FIELD_ID = " + objReader.GetInt32("FIELD_ID");
					        DbFactory.ExecuteNonQuery(m_connectionString, sSQL);

					        // ... make sure required field entry gets moved
					        //sSQL = "INSERT INTO REQ_DEF(LABEL_ID,REQUIRED_FLAG) VALUES (" & lTmp & "," & IIf(iAnyVarToInt(vDB_GetData(rs, "REQUIRED_FLAG")), -1, 0) & ")"
					        //result = DB_SQLExecute(dbLookup, sSQL)

					        lTmp = lTmp + 1;

				        }
			        }

			        DbFactory.ExecuteNonQuery(m_connectionString, "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + lTmp + 1 + " WHERE SYSTEM_TABLE_NAME = 'HELP_DEF'");
		        }

		        return;
	        } catch (Exception ex) {
                ExceptionLogger.AddMessage("UpgradeUtil.supUpdateSupp", ex.Message); 
	        }
        }

        //***************************************************************************************
        // Procedure : subUpdateGlossary_NEXT_UNIQUE_ID
        // Purpose   :
        // 08/29/2001 jlt fixed error handling caused by missing JURIS_FORMS table
        //               (skipped upgrade from 3.6.20 to 4.0)
        //***************************************************************************************
        public void subUpdateGlossary_NEXT_UNIQUE_ID(string sTableName)
        {
            int lMaxValue = 0;
            string sSQL = string.Empty;

            try
            {
                sSQL = "";
                //    Select Case sTableName
                //        Case "STATES"
                //            sSQL = "SELECT MAX(STATE_ROW_ID) AS MAX_FORM_ID FROM " & sTableName
                //        Case Else
                //            sSQL = "SELECT MAX(FORM_ID) AS MAX_FORM_ID FROM " & sTableName
                //    End Select
                switch (sTableName)
                {
                    case "STATES":
                        sSQL = "SELECT MAX(STATE_ROW_ID) FROM " + sTableName;
                        break;
                    default:
                        sSQL = "SELECT MAX(FORM_ID) FROM " + sTableName;
                        break;
                }
                if (DbType != eDatabaseType.DBMS_IS_ACCESS)
                    sSQL = sSQL.Replace(" AS ", " ");
                //Remove alias As(notneeded except for access)

                using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        lMaxValue = objReader.GetInt32(0) + 1;
                    }
                    else
                    {
                        lMaxValue = 0;
                    }
                }
                DbWriter objWriter = null;
                if (lMaxValue > 0)
                {
                    sSQL = "SELECT * FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'";
                    using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            objWriter = DbFactory.GetDbWriter(objReader, true);
                            objWriter.Fields["NEXT_UNIQUE_ID"].Value = lMaxValue;
                            objWriter.Execute();
                        }
                        else
                        {
                            //should never happen
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //iGeneralError
                ExceptionLogger.AddMessage("UpgradeUtil.subUpdateGlossary_NEXT_UNIQUE_ID", ex.Message); 
            }

        }
    }
}
