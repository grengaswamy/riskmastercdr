﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Riskmaster.Db;
using Riskmaster.Security.Encryption;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Windows.Forms;
using System.Configuration;

namespace GenericUpgrade
{
    public struct DbDescr
    {
        public Int32 DsnId;
        public String DSN;
        public String ConnectionString;
    }

    public class RmUpgrader
    {
        private DbDescr dbDescr = new DbDescr();
        private String scriptFileName;

        public DbDescr DBDescr 
        {
            get { return dbDescr; } 
        }

        Upgrading progressFrm;

        public RmUpgrader(Int32 pDbDsnId, String pSecurityDSN, Upgrading pProgressFrm)
        {
            dbDescr.DsnId = pDbDsnId;
            PrepareConnectionString(pSecurityDSN);
            progressFrm = pProgressFrm;
            scriptFileName = ConfigurationManager.AppSettings["ScriptFileName"];
        }

        private void PrepareConnectionString(String securityDSN)
        {
            String  sSql = "SELECT * FROM DATA_SOURCE_TABLE WHERE DSNID = " + dbDescr.DsnId;
            using (DbReader objReader = DbFactory.GetDbReader(securityDSN, sSql))
            {
                if (objReader.Read())
                {
                    dbDescr.DSN = objReader.GetString("DSN");

                    String userId = RMCryptography.DecryptString(objReader.GetString("RM_USERID"));
                    String password = RMCryptography.DecryptString(objReader.GetString("RM_PASSWORD"));

                    // Now, form a connection string so we can connect and do rest of upgrade
                    String connString = objReader.GetString("CONNECTION_STRING");
                    //tkatsarski: 05/18/15 Start changes for RMA-10177: Connection string format was causing problems with Oracle.
                    if (connString == "")
                    {
                        dbDescr.ConnectionString = String.Format("DSN={0};UID={1};PWD={2};", dbDescr.DSN, userId, password);
                        //dbDescr.ConnectionString = String.Format("DSN={0};UID={1};PWD={2}", dbDescr.DSN, userId, password);
                    }
                    else
                    {
                        dbDescr.ConnectionString = String.Format("{0}UID={1};PWD={2};", connString, userId, password);
                        //dbDescr.ConnectionString = String.Format("{0};UID={1};PWD={2}", connString, userId, password);
                    }
                    //tkatsarski: 05/18/15 End changes for RMA-10177
                }
            }
        }

        private void progress_Changed(object m, ProgressEventArgs e)
        {
            if (e.ProgressStatus != -1)
            {
                progressFrm.Progress = e.ProgressStatus.ToString();
            }
            progressFrm.lblInfo.Text = e.InfoLine;
            Application.DoEvents();
        }

        public void Upgrade()
        {
            string currentFolder = string.Empty;

            // -- open error log file (logs errors that occur on [FORGIVE] lines
            currentFolder = UpgradeUtil.GetSystemPath();
            string sHeader = new string('=', 80);
            string sTmp = "Update '" + dbDescr.DSN + "' Database for RISKMASTER...";
            using (StreamWriter outfile = new StreamWriter(currentFolder + "forgive_err_" + dbDescr.DSN + ".log", true))
            {
                outfile.WriteLine(sHeader);
                outfile.WriteLine(sTmp);
                outfile.WriteLine("      " + currentFolder + " " + DateTime.Now.ToString());

                progressFrm.lblDb.Text = dbDescr.DSN;
                progressFrm.lblFileName.Text = scriptFileName;
                progressFrm.lblInfo.Text = "Update " + dbDescr.DSN + " Database for RISKMASTER...";
                progressFrm.Progress = "10";
                progressFrm.Refresh();
                System.Windows.Forms.Application.DoEvents();

                UpgradeUtil upgrManager = new UpgradeUtil(dbDescr.ConnectionString, outfile);
                upgrManager.ChangeProgress += new UpgradeUtil.ChangeProgressEventHandler(this.progress_Changed);
                try
                {
                    upgrManager.ExecuteSQLScriptExt(scriptFileName, "");

                    progressFrm.Progress = Convert.ToString(80);
                    progressFrm.Refresh();
                    System.Windows.Forms.Application.DoEvents();
                }
                finally
                {
                    upgrManager.ChangeProgress -= new UpgradeUtil.ChangeProgressEventHandler(this.progress_Changed);
                }
                outfile.WriteLine("Update '" + dbDescr.DSN + "' Database was successfully finished." + Environment.NewLine);
                outfile.Flush();
                outfile.Close();
            }
            // Forgive Err Log

        }
    }
}
