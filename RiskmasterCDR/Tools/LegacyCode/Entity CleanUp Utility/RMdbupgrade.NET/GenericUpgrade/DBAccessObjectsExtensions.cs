﻿using System;
using System.Text;

using Riskmaster.Db;

namespace GenericUpgrade
{

    public static class MyExtensionMethods
    {
        #region DbReader
        public static bool ExistField(this DbReader reader, string sFieldName)
        {
            bool bResult = false;
            try {
                Object fld = reader[sFieldName];
                bResult =  true;
            } catch (Exception) {
                //Do nothing
                //bResult already set to false
            }
            return bResult;
        }
        #endregion

        #region DbWriter
        public static bool ExistField(this DbFieldList fieldList, string sFieldName)
        {
            bool bResult = false;
            if (fieldList.Count > 0) {
                for (int idx = 0; idx <= fieldList.Count - 1; idx++)
                {
                    DbField fld = fieldList[idx];
                    bResult = (fld.Name == sFieldName);
                    if (bResult) break;
                }
                return bResult;
            }
            return bResult;
        }
        #endregion
    }
}
