﻿using System;
using System.Text;
using System.Runtime.Serialization;

using Riskmaster.ExceptionTypes;

namespace GenericUpgrade
{
    public class ECUException : RMAppException
    {
        private string caption = string.Empty;
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }
        
        /// <summary>
        /// Initializes a new instance of the ECU exception class.
		/// </summary>
        public ECUException()
            : base("DB upgrade exception.")
        { }

        /// <summary>
        /// Initializes a new instance of the ECU exception class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ECUException(string message)
            : base(message)
        {
            this.caption = "DB upgrade";
        }
        
        /// <summary>
        /// Initializes a new instance of the ECU exception class with a specified error message.
        /// </summary>
        /// <param name="caption">The message for the caption of message popup.</param>
        /// <param name="message">The message that describes the error.</param>
        public ECUException(string caption, string message)
            : base(message)
        {
            this.caption = caption;
        }

        /// <summary>
        /// Initializes a new instance of the ECU exception class with a specified error message.
        /// </summary>
        /// <param name="caption">The message for the caption of message popup.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference if no inner exception is specified.</param>
        public ECUException(string caption, string message, Exception innerException)
            : base(message, innerException)
        {
            this.caption = caption;
        }

    }
}
