﻿Option Strict Off
Option Explicit On

Imports System.IO
Imports Riskmaster.Db
Imports Riskmaster.ExceptionTypes
Imports EDI.CommonUtils

Public Class UpgradeUtil

    ' THIS MODULE IS FOR ANY GENERAL ROUTINES THAT ALL
    ' UPGRADE MODULES MAY WANT TO USE. IN GENERAL,
    ' DO EVERYTHING POSSIBLE FROM YOUR ROUTINES SINCE
    ' THESE ROUTINES AREN'T GUARANTEED TO WORK ON FUTURE
    ' VERSIONS OF RM/WIN.
    '
    Dim sParmArray(9) As String ' used by execute sql - these values may be initialized by the call to bExecuteSQLScript
    ' They may be changed in the script by [ASSIGN statements in the script file.

    Private m_connectionString As String
    Private m_logWriter As TextWriter
    Private m_dbType As eDatabaseType
    Private objDbUpgrade As DbUpgrade

    Public g_IfNest As Integer ' JP 2/1/2002    Level of IF nesting (only executes lines if == 0)

    Public Sub New(connectionString As String, logWriter As TextWriter)
        m_connectionString = connectionString
        m_logWriter = logWriter
        objDbUpgrade = New DbUpgrade(m_connectionString)
        m_dbType = objDbUpgrade.DbType
    End Sub

    Public Event ChangeProgress(ByVal sender As Object, ByVal e As ProgressEventArgs)

    Protected Overridable Sub OnChangeProgress(ByVal e As ProgressEventArgs)
        RaiseEvent ChangeProgress(Me, e)
    End Sub

    Public Function ExecuteSQLScriptExt(sScriptFilename As String, sParms As String) As Boolean
        '//Technical Info-----------------------------------------------------------------------------------------------------------
        '
        '//Description
        'This routine returns the next unique id for the given table.
        'And also creates the next unique id and updates the database for the given table
        '
        '//Arguments
        ' sScriptFilename   | String     | Input    | Filename of script file. Does not include path
        '                                              (assumed to be in program directory).
        ' sParms            | String     | Input    | String of substitution parameters. Separated by tabs.
        '                                              If not enough parms are supplied for the script,
        '                                              blanks will be inserted. Up to 9 parameters are
        '                                              supported for the entire file.
        '
        '--------------------------------------------------------------------------------------------------------------------------
        Dim sScriptPath As String
        Dim nFileNo As Integer
        Dim sSQL As String = ""
        Dim bHasParms As Boolean
        Dim bForgive As Boolean
        Dim iDBSpecific As Short ' JP 8/15/2001
        Dim sTmp As String
        Dim iCount As Integer
        Dim bExecDirect As Boolean
        Dim bGoMode As Boolean
        Dim sLine As String
        Dim bProceed As Boolean
        Dim sNativeError As String = ""
        Dim sODBCError As String = ""

        Try
            g_IfNest = 0 ' Set as IF blocks are encountered that are designed to stop SQL from executing under certain circumstances
            bGoMode = False
            ' parse out initial parms into a global array
            For iCount = 1 To 9
                sParmArray(iCount) = "" ' blank out all parms first
            Next iCount

            Dim separator As Char = Chr(9)

            sTmp = sParms
            iCount = 0
            Do While sTmp <> ""
                iCount = iCount + 1
                If InStr(sTmp, separator) > 0 Then
                    sParmArray(iCount) = Left(sTmp, InStr(sTmp, separator) - 1)
                    sTmp = Mid(sTmp, InStr(sTmp, separator) + 1)
                Else
                    sParmArray(iCount) = sTmp
                    sTmp = ""
                End If
            Loop


            ' prepare to execute script
            sScriptPath = GetSystemPath() & sScriptFilename

            OnChangeProgress(New ProgressEventArgs(40, sScriptPath))


            If String.IsNullOrEmpty(sParms) Or sParms.Trim() = "" Then
                bHasParms = False
            Else
                bHasParms = True
            End If

            ' see if script exists
            If Not File.Exists(sScriptPath) Then
                m_logWriter.WriteLine("--- Error Msg: Could not find database upgrade script")
                m_logWriter.WriteLine(" file-" & sScriptFilename)
                m_logWriter.Flush()
                ' todo message - throw
                Throw New RMAppException("File Missing: Could not find database upgrade script. Upgrade program may be damaged. Please contact Computer Sciences Corporation Tech support.")

                ExecuteSQLScriptExt = False
                Exit Function
            End If

            ' open text file and execute statements
            nFileNo = FreeFile()
            FileOpen(nFileNo, sScriptPath, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

            iCount = 0
            Do While Not EOF(nFileNo)
                iCount = iCount + 1
                sLine = LineInput(nFileNo)
                If InStr(sLine, vbLf) > 0 Or InStr(sLine, vbCr) > 0 Then ' FTP & some mail progs strip LF chars
                    m_logWriter.WriteLine("--- Error Msg: Database upgrade script may be damaged")
                    m_logWriter.WriteLine(" file-" & sScriptFilename & " line-" & iCount & ") " & sLine)
                    m_logWriter.Flush()
                    ' todo message - throw
                    Throw New RMAppException("Bad Script: Database upgrade script may be damaged. Please contact Computer Sciences Corporation Tech support.")

                    ExecuteSQLScriptExt = False
                    Exit Function
                End If
                If (Left(Trim(sLine), 1) <> ";") And (Left(Trim(sLine), 1) <> "'") And (UCase(Left(Trim(sLine), 3)) <> "REM") And (Trim(sLine) <> "") Then ' ignore comment lines (begin with ;)
                    If Trim(sLine) = "[GO_DELIMETER]" Then
                        bGoMode = True
                        sSQL = ""
                    ElseIf bIsPseudoStatement(sLine) Then
                        If Not bProcessPseudoStatement(sLine) Then ' SPECIFIC FUNCTION PROCESSING or error
                            ExecuteSQLScriptExt = False
                            FileClose(nFileNo)
                            Exit Function
                        End If
                        sSQL = ""
                    Else
                        bProceed = True
                        If bGoMode Then ' collect lines until a GO is spotted
                            If Trim(UCase(sLine)) <> "GO" Then
                                bProceed = False
                                sSQL = sSQL & sLine
                            End If
                        Else
                            sSQL = sLine ' single line per statement mode
                        End If

                        If bProceed Then
                            ' subst parms if available and subst in any carriage returns
                            sSQL = sFormatMsgString(sSQL)

                            ' Check and see if line if db specific
                            iDBSpecific = -1
                            sSQL = sCheckDBSpecific(sSQL, iDBSpecific)

                            ' Execute line only if for all databases or for this specific one    JP 8/15/2001
                            If iDBSpecific = -1 Or iDBSpecific = m_dbType Then
                                ' check for upgrade SQL extensions
                                sSQL = sCheckForgive(sSQL, bForgive, sNativeError, sODBCError)

                                ' check to see if we need to directly execute statement (NOTE: all statements are now direct anyhow - since ROCKET was integrated)
                                sSQL = sCheckExecDirect(sSQL, bExecDirect)
                                'UI reference
                                OnChangeProgress(New ProgressEventArgs(-1, "Executing: " & sSQL))


                                If m_dbType = eDatabaseType.DBMS_IS_ACCESS Then sSQL = Replace(sSQL, "|", "' + chr(124) + '")

                                If g_IfNest = 0 Then ' Only execute if we're not excluded by an IF statement
                                    If Not bForgive Then
                                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                                    Else
                                        Try
                                            DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                                        Catch ex As Exception
                                            ' todo use ex.Message  and verify logging patterns 
                                            sTmp = ErrorToString() ' this come with CrLf

                                            If Len(sNativeError) > 0 And InStr(sTmp, "[Native Error=" & sNativeError & "]") > 0 Then
                                                ' Do Nothing for now
                                            ElseIf Len(sODBCError) > 0 And InStr(sTmp, "[ODBC Error=" & sODBCError & "]") > 0 Then
                                                ' Do Nothing for now
                                            Else
                                                ' Log the error in the FORGIVE_ERR.LOG
                                                If InStr(sTmp, "already") > 0 Or InStr(sTmp, "duplic") > 0 Or InStr(sTmp, "unique") > 0 Then
                                                    '                                        Print #5, "- Error Msg: " & sTmp
                                                    sTmp = "- Error Msg: " & sTmp
                                                ElseIf InStr(sTmp, "Syntax") > 0 Or InStr(sTmp, "syntax") > 0 Then
                                                    '                                        Print #5, "--- Error Msg: " & sTmp
                                                    sTmp = "--- Error Msg: " & sTmp
                                                Else
                                                    '                                        Print #5, "-- Error Msg: " & sTmp
                                                    sTmp = "-- Error Msg: " & sTmp
                                                End If
                                                '                                    Print #5, "file: " & sScriptFilename & ", line: " & Trim$(Str$(iCount)) & ": " & sSQL
                                                If Right(sTmp, 2) <> vbCrLf Then sTmp = sTmp & vbCrLf
                                                sTmp = sTmp & "file: " & sScriptFilename
                                                sTmp = sTmp & ", line: " & Trim(Str(iCount))

                                                m_logWriter.WriteLine(sTmp & ": " & sSQL & Environment.NewLine)
                                                m_logWriter.Flush()
                                            End If

                                        End Try
                                    End If
                                End If
                            End If

                            sSQL = ""
                        End If
                    End If
                End If
            Loop

            FileClose(nFileNo)

            ExecuteSQLScriptExt = True
        Catch e As RMAppException
            Throw
        Catch ex As Exception
            LogWriter.LogError("UpgradeUtil.ExecuteSQLScriptExt", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
            Throw New RMAppException("Upgrade Failed on the Following Script Line:" & Environment.NewLine & sScriptFilename & ": " & Str(iCount) & " " & sSQL, ex)
            ' caption "Tech Support Error Information"
        End Try
        Return ExecuteSQLScriptExt

    End Function

    '    Function bLoadSystemScript(ByRef dbDB As Short, ByRef sScriptFilename As String, ByRef sScriptName As String) As Boolean
    '        On Error GoTo hError
    '        Dim sScriptPath As String
    '        Dim nFileNo As Short
    '        Dim sSQL As String
    '        Dim iCount As Short
    '        Dim sLine As String
    '        Dim sScript As String
    '        Dim rs As Short

    '        ' prepare to execute script
    '        sScriptPath = sGetSystemPath() & sScriptFilename

    '        ' see if script exists
    '        If Not File.Exists(sScriptPath) Then
    '            MsgBox("Could not find database upgrade scripts. Upgrade program may be damaged. Please contact Computer Sciences Corporation Tech support.", 48, "File Missing")
    '            PrintLine(5, "--- Error Msg: Could not find database upgrade script")
    '            PrintLine(5, " file-" & sScriptFilename)
    '            bLoadSystemScript = False
    '            Exit Function
    '        End If

    '        ' open text file and retrieve script
    '        nFileNo = FreeFile()
    '        FileOpen(nFileNo, sScriptPath, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

    '        iCount = 0
    '        Do While Not EOF(nFileNo)
    '            iCount = iCount + 1
    '            sLine = LineInput(nFileNo)
    '            sScript = sScript & sLine & vbCrLf
    '        Loop

    '        FileClose(nFileNo)

    '        ' Write system script to sys script table
    '        rs = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbDB, "SELECT * FROM VBS_SYS_SCRIPTS WHERE KEY_NAME = '" & sScriptName & "'", ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
    '        If Not ROCKETCOMLibDTGRocket_definst.DB_EOF(rs) Then
    '            ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_CLOSE)
    '            ROCKETCOMLibDTGRocket_definst.DB_Edit(rs)
    '        Else
    '            ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_CLOSE)
    '            ROCKETCOMLibDTGRocket_definst.DB_AddNew(rs)
    '            ROCKETCOMLibDTGRocket_definst.DB_PutData(rs, "KEY_NAME", sScriptName)
    '        End If
    '        ROCKETCOMLibDTGRocket_definst.DB_PutData(rs, "SCRIPT_TEXT", sScript)

    '        ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP)

    '        bLoadSystemScript = True
    '        Exit Function

    'hError:
    '        Dim EResult As Short

    '        bLoadSystemScript = False
    '        EResult = iGeneralError(Err.Number, ErrorToString(), "UpgradeUtil.bLoadSystemScript")
    '        MsgBox("Upgrade Failed on the Following Script Line:" & vbCrLf & sScriptFilename & ": " & Str(iCount) & " " & sSQL, 48, "Tech Support Error Information")
    '        Select Case EResult
    '            Case MsgBoxResult.Retry
    '                Resume
    '            Case Else
    '                Exit Function
    '        End Select

    '    End Function



    Function bIsPseudoStatement(sSQL As String) As Boolean
        If sSQL.Trim().Substring(0, 7).ToUpper() = "[ASSIGN" Then
            bIsPseudoStatement = True
        ElseIf sSQL.Trim().Substring(0, 15).ToUpper() = "[UPDT_TBL_STAMP" Then
            bIsPseudoStatement = True
        ElseIf sSQL.Trim().Substring(0, 4).ToUpper() = "[IF " Then
            bIsPseudoStatement = True
        ElseIf sSQL.Trim().Substring(0, 6).ToUpper() = "[ENDIF" Then
            bIsPseudoStatement = True
        Else
            bIsPseudoStatement = False
        End If
    End Function

    Function bProcessPseudoStatement(ByRef sSQL As String) As Boolean

        Dim bGroupAssoc As Boolean 'jtodd22 01/31/2008 ADD_STATE_FIELD_2
        Dim sAssocField As String 'jtodd22 01/31/2008 ADD_STATE_FIELD_2
        Dim sListOrgHierarchyIDs As String 'jtodd22 02/03/2010
        Dim sWork As String
        Dim sTemp2 As String
        Dim lTemp As Integer
        Dim nRegister As Short
        Dim sParm As String
        Dim nPos As Integer
        Dim sFunction As String
        Dim bContinue As Boolean
        Dim nNumParms As Integer
        Dim sTMParray(0) As String

        Dim iStateRowID As Short
        Dim iFormCategory As Short
        Dim sFormTitle As String
        Dim sFormName As String
        Dim sFileName As String
        Dim iActiveFlag As Short
        Dim sHashCRC As String
        Dim sPrimaryFormFlag As String
        Dim iLineOfBusCode As Short

        Dim sSC As String
        Dim iNLSCode As Short
        Dim sDescription As String
        Dim sSysCodeTable As String
        Dim lRelCodeID As Integer

        Dim sStateID As String
        Dim sStateName As String
        Dim dEmployerLiability As Double
        Dim dMaxClaimAmount As Double
        Dim iRatingMethodCode As Short
        Dim iPrimaryFormFlag As Short
        Dim iUSLLimit As Short
        Dim iWeightingStepVal As Short
        Dim iDeletedFlag As Short

        Dim sStateTable As String
        Dim sUserPrompt As String
        Dim sSysFieldName As String
        Dim lFieldType As Integer
        Dim lFieldSize As Integer
        Dim bRequired As Boolean
        Dim bDeleted As Boolean
        Dim bLookup As Boolean
        Dim bIsPatterned As Boolean
        Dim sPattern As String
        Dim sCodeTable As String
        Dim iCatID As Short
        Dim sFieldName As String
        Dim sFieldDesc As String
        Dim sFieldTable As String
        Dim sFieldType As String
        Dim sOptMask As String
        Dim sDisplayCat As String
        Dim bRetVal As Boolean = False

        Try

            sWork = sSQL.Trim()
            ' UI reference
            OnChangeProgress(New ProgressEventArgs(-1, "Executing: " & sSQL))

            ' remove brackets from statement
            If Left(sWork, 1) <> "[" Or Right(sWork, 1) <> "]" Then
                ' SYNTAX ERROR - incorrect delimeter for pseudo statement
                ' todo message - throw
                Throw New RMAppException("PSEUDO STATEMENT PARSE ERROR: Incorrect Backeting of Pseudo Statement")
                'MsgBox("Incorrect Backeting of Pseudo Statement", 48, "PSEUDO STATEMENT PARSE ERROR")

                Return False ' take the easy way out
            Else
                sWork = Mid(sWork, 2, Len(sWork) - 2)
            End If

            ' determine which pseudo statement this line is
            If UCase(Left(sWork, 7)) = "ASSIGN " Then
                If g_IfNest > 0 Then
                    Return True ' JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
                End If

                ' ASSIGN  -  assigns a value to a register (%%1 through %%9)
                sWork = Trim(Mid(sWork, 8)) ' strip off assign pseudo statement keyword

                ' pick off register to assign
                sTemp2 = Left(sWork, 4)
                If Left(sTemp2, 2) <> "%%" Or (Asc(Mid(sTemp2, 3, 1)) > Asc("9")) Or (Asc(Mid(sTemp2, 3, 1)) < Asc("0")) Or Right(sTemp2, 1) <> "=" Then
                    ' SYNTAX ERROR - unexpected format
                    ' todo message - throw
                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Register Assignment Syntax")
                    'MsgBox("Incorrect Register Assignment Syntax", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                    Return False 'take the easy way out
                Else
                    nRegister = Int16.Parse(Mid(sTemp2, 3, 1))
                    sWork = Trim(Mid(sWork, 5)) ' strip off register assignment portion

                    ' process remainder through parameter substitution function
                    sWork = sFormatMsgString(sWork)

                    ' determine assignment function
                    If Left(UCase(sWork), 6) <> "QUERY(" And InStr(sWork, "(") = 0 Then
                        ' SYNTAX ERROR - function format incorrect
                        ' todo message - throw
                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Pseudo Function Is Missing Open Parenthesis")
                        'MsgBox("Pseudo Function Is Missing Open Parenthesis", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                        Return False 'take the easy way out
                    End If

                    If Left(UCase(sWork), 6) <> "QUERY(" Then
                        nPos = InStr(sWork, "(")
                        sFunction = UCase(Trim(Left(sWork, nPos - 1)))
                        sWork = Trim(Mid(sWork, nPos + 1))
                    Else ' special processing for QUERY directive
                        sFunction = "QUERY"
                        sWork = Trim(Mid(sWork, 7))
                    End If

                    If Right(sWork, 1) <> ")" Then
                        ' SYNTAX ERROR - invalid function syntax - missing closing paren
                        ' todo message - throw
                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Pseudo Function Is Missing Close Parenthesis")
                        'MsgBox("Pseudo Function Is Missing Close Parenthesis", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                        Return False 'take the easy way out
                    Else
                        sWork = Trim(Left(sWork, Len(sWork) - 1)) ' strip off closing paren

                        If sFunction <> "QUERY" Then
                            ' tear out parameters to function
                            nNumParms = 0
                            bContinue = True
                            ' Do While sWork <> ""
                            Do While bContinue
                                nNumParms = nNumParms + 1
                                ReDim Preserve sTMParray(nNumParms)

                                If InStr(sWork, ",") > 0 Then
                                    sTMParray(nNumParms) = Left(sWork, InStr(sWork, ",") - 1)
                                    sWork = Mid(sWork, InStr(sWork, ",") + 1)
                                Else
                                    sTMParray(nNumParms) = sWork
                                    sWork = ""
                                    bContinue = False
                                End If
                            Loop
                        Else ' special processing for query function
                            nNumParms = 1
                            ReDim sTMParray(1)
                            sTMParray(1) = sWork
                        End If

                        ' SPECIFIC FUNCTION PROCESSING   -    *** BEGIN ***
                        Select Case sFunction
                            Case "GET_TABLEID"
                                If nNumParms <> 1 Then
                                    ' ERROR - incorrect number of parameters to GETTABLEID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to GET_TABLEID Function (1 is required)")
                                    'MsgBox("Incorrect Number of Parameters Given to GET_TABLEID Function (1 is required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    sParm = Trim(sTMParray(1))
                                    lTemp = lGetTableID(sParm)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: System Table Name Specified in GET_TABLEID call is Invalid")
                                        'MsgBox("System Table Name Specified in GET_TABLEID call is Invalid", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ATTACH_TABLE"
                                ' ERROR - not supported
                                Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: ATTACH TABLE macro no longer supported")
                                ' todo message - throw
                                'MsgBox("ATTACH TABLE macro no longer supported", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                Return False 'take the easy way out

                            Case "REATTACH_TABLE"
                                ' ERROR - not supported
                                ' todo message - throw
                                Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: REATTACH TABLE macro no longer supported")
                                'MsgBox("REATTACH TABLE macro no longer supported", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                Return False 'take the easy way out

                            Case "NEXT_UID"
                                If nNumParms <> 1 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to NEXT_UID Function (1 is required)")
                                    'MsgBox("Incorrect Number of Parameters Given to NEXT_UID Function (1 is required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    sParm = Trim(sTMParray(1))
                                    lTemp = objDbUpgrade.lGetNextUID(sParm)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: System Table Name Specified in NEXT_UID call is Invalid OR Unable to Obtain Glossary Unique ID")
                                        'MsgBox("System Table Name Specified in NEXT_UID call is Invalid OR Unable to Obtain Glossary Unique ID", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "QUERY"
                                If nNumParms <> 1 Then
                                    ' todo message - throw
                                    'MsgBox("Incorrect Number of Parameters Given to Query Function (1 is required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    sParm = Trim(sTMParray(1))

                                    Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sParm)
                                        If objReader.Read() Then
                                            sParmArray(nRegister) = objReader.GetString(0)
                                        Else
                                            sParmArray(nRegister) = ""
                                        End If
                                    End Using
                                End If

                            Case "ADD_CODE"
                                If nNumParms <> 5 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_CODE Function (5 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_CODE Function (5 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else


                                    sSC = Trim(sTMParray(1))
                                    iNLSCode = Int16.Parse(sTMParray(2))
                                    sDescription = Trim(sTMParray(3))
                                    sSysCodeTable = Trim(sTMParray(4))
                                    lRelCodeID = Int32.Parse(sTMParray(5))

                                    lTemp = lAddCode(sSC, iNLSCode, sDescription, sSysCodeTable, lRelCodeID)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_CODE call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_CODE call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_JURISDICTION"
                                If nNumParms <> 8 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_JURISDICTION Function (8 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_JURISDICTION Function (8 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    'STATE_ROW_ID

                                    sStateID = sTMParray(1).Trim()
                                    sStateName = sTMParray(2).Trim()
                                    dEmployerLiability = Convert.ToDouble(sTMParray(3))
                                    dMaxClaimAmount = Convert.ToDouble(sTMParray(4))
                                    iRatingMethodCode = Convert.ToInt16(sTMParray(5))
                                    iUSLLimit = Convert.ToInt16(sTMParray(6)) <> 0
                                    iWeightingStepVal = Convert.ToInt16(sTMParray(7)) <> 0
                                    iDeletedFlag = Convert.ToInt16(sTMParray(8)) <> 0

                                    lTemp = lAddJurisdiction(sStateID, sStateName, dEmployerLiability, dMaxClaimAmount, iRatingMethodCode, iUSLLimit, iWeightingStepVal, iDeletedFlag)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))

                                End If

                            Case "ADD_JURISDICTIONAL_FROI"
                                If nNumParms <> 7 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_JURISDICTION_FROI Function (7 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_JURISDICTION_FROI Function (7 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    '(1)STATE_ROW_ID,(2)FORM_NAME,(3)PDF_FILE_NAME,(4)ACTIVE_FLAG,(5)HASH_CRC,(6)PRIMARY_FORM_FLAG,(7)LINE_OF_BUS_CODE

                                    sStateID = Trim(sTMParray(1))
                                    sFormName = Trim(sTMParray(2))
                                    sFileName = Trim(sTMParray(3))
                                    iActiveFlag = Convert.ToInt16(sTMParray(4))
                                    sHashCRC = Trim(sTMParray(5))
                                    iPrimaryFormFlag = Convert.ToInt16(sTMParray(6))
                                    iLineOfBusCode = Convert.ToInt16(sTMParray(7))

                                    lTemp = lAddJurisdictionalFROI(CShort(sStateID), sFormName, sFileName, iActiveFlag, sHashCRC, iPrimaryFormFlag, iLineOfBusCode)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))


                                End If
                            Case "ADD_STATE_FIELD"
                                If nNumParms <> 11 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_STATE_FIELD Function (11 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_STATE_FIELD Function (11 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else

                                    sStateTable = Trim(sTMParray(1))
                                    sUserPrompt = Trim(sTMParray(2))
                                    sSysFieldName = Trim(sTMParray(3))
                                    lFieldType = Int32.Parse(sTMParray(4))
                                    lFieldSize = Int32.Parse(sTMParray(5))
                                    bRequired = Convert.ToInt16(sTMParray(6)) <> 0
                                    bDeleted = Convert.ToInt16(sTMParray(7)) <> 0
                                    bLookup = Convert.ToInt16(sTMParray(8)) <> 0
                                    bIsPatterned = Convert.ToInt16(sTMParray(9)) <> 0
                                    sPattern = Trim(sTMParray(10))
                                    sCodeTable = Trim(sTMParray(11))

                                    lTemp = lAddStateField(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern, sCodeTable)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_STATE_FIELD call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_STATE_FIELD_2"
                                If nNumParms <> 13 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_STATE_FIELD_2 Function (13 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_STATE_FIELD_2 Function (13 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else

                                    sStateTable = Trim(sTMParray(1))
                                    sUserPrompt = Trim(sTMParray(2))
                                    sSysFieldName = Trim(sTMParray(3))
                                    lFieldType = Int32.Parse(sTMParray(4))
                                    lFieldSize = Int32.Parse(sTMParray(5))
                                    bRequired = Convert.ToInt16(sTMParray(6)) <> 0
                                    bDeleted = Convert.ToInt16(sTMParray(7)) <> 0
                                    bLookup = Convert.ToInt16(sTMParray(8)) <> 0
                                    bIsPatterned = Convert.ToInt16(sTMParray(9)) <> 0
                                    sPattern = Trim(sTMParray(10))
                                    sCodeTable = Trim(sTMParray(11))
                                    bGroupAssoc = Convert.ToInt16(sTMParray(12)) <> 0
                                    sAssocField = Trim(sTMParray(13))


                                    lTemp = lAddStateField_2(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern, sCodeTable, bGroupAssoc, sAssocField)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_STATE_FIELD_2 call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_STATE_FIELD_2 call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_STATE_FIELD_ORGHIERARCHYASSOC"
                                If nNumParms <> 14 Then
                                    ' ERROR - incorrect number of parameters to NEXTUID function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_STATE_FIELD_ORGHIERARCHYASSOC Function (14 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_STATE_FIELD_ORGHIERARCHYASSOC Function (14 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else

                                    sStateTable = Trim(sTMParray(1))
                                    sUserPrompt = Trim(sTMParray(2))
                                    sSysFieldName = Trim(sTMParray(3))
                                    lFieldType = Int32.Parse(sTMParray(4))
                                    lFieldSize = Int32.Parse(sTMParray(5))
                                    bRequired = Convert.ToInt16(sTMParray(6)) <> 0
                                    bDeleted = Convert.ToInt16(sTMParray(7)) <> 0
                                    bLookup = Convert.ToInt16(sTMParray(8)) <> 0
                                    bIsPatterned = Convert.ToInt16(sTMParray(9)) <> 0
                                    sPattern = Trim(sTMParray(10))
                                    sCodeTable = Trim(sTMParray(11))
                                    bGroupAssoc = Convert.ToInt16(sTMParray(12)) <> 0
                                    sAssocField = Trim(sTMParray(13))
                                    sListOrgHierarchyIDs = Trim(sTMParray(14))

                                    lTemp = lAddStateField_OrgHierarchyAssoc(sStateTable, sUserPrompt, sSysFieldName, lFieldType, lFieldSize, bRequired, bDeleted, bLookup, bIsPatterned, sPattern, sCodeTable, bGroupAssoc, sAssocField, sListOrgHierarchyIDs)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the lAddStateField_OrgHierarchyAssoc call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the lAddStateField_OrgHierarchyAssoc call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_MERGE_DICTIONARY"
                                If nNumParms <> 8 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_MERGE_DICTIONARY Function 8 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_MERGE_DICTIONARY Function 8 are required)", 4, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else

                                    iCatID = Int16.Parse(sTMParray(1))
                                    sFieldName = Trim(sTMParray(2))
                                    sFieldDesc = Trim(sTMParray(3))
                                    sFieldTable = Trim(sTMParray(4))
                                    sFieldType = sTMParray(5)
                                    sOptMask = Trim(sTMParray(6))
                                    sDisplayCat = Trim(sTMParray(7))
                                    sCodeTable = sTMParray(8)

                                    lTemp = lAddMergeDictionaryLine(Int16.Parse(sTMParray(1)), sTMParray(2), Trim(sTMParray(3)), Trim(sTMParray(4)), Trim(sTMParray(5)), Trim(sTMParray(6)), Trim(sTMParray(7)), Trim(sTMParray(8)))
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_MERGE_DICTIONARY call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_MERGE_DICTIONARY call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If


                            Case "ADD_WCP_FORM"
                                If nNumParms <> 9 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    iStateRowID = Int16.Parse(sTMParray(1))
                                    iFormCategory = Int16.Parse(sTMParray(2))
                                    sFormTitle = Trim(sTMParray(3))
                                    sFormName = Trim(sTMParray(4))
                                    sFileName = Trim(sTMParray(5))
                                    iActiveFlag = Int16.Parse(sTMParray(6))
                                    If Trim(sTMParray(7)) = "" Then sTMParray(7) = "no value"
                                    sHashCRC = Trim(sTMParray(7))
                                    If sHashCRC = "" Then sHashCRC = "no value"
                                    sPrimaryFormFlag = Trim(sTMParray(8))
                                    iLineOfBusCode = Int16.Parse(sTMParray(9))

                                    'jtodd22 01/08/2008 trap for iStateRowID being 0 (zero)
                                    If iStateRowID < 1 Then
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: The jurisdition ID value in ADD_WCP_FORM is invalid (zero or less).  The STATES table may have bad data.")
                                        'MsgBox("The jurisdition ID value in ADD_WCP_FORM is invalid (zero or less).  The STATES table may have bad data.", 48, "PSEUDO STATEMENT SYNTAX ERROR")
                                        Return False 'take the easy way out
                                    End If

                                    lTemp = lAddWCPForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_WCP_FORM call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_WCP_FORM call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_CL_FORM"

                                If nNumParms <> 9 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_WCP_FORM Function (9 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else


                                    iStateRowID = Int16.Parse(sTMParray(1))
                                    iFormCategory = Int16.Parse(sTMParray(2))
                                    sFormTitle = Trim(sTMParray(3))
                                    sFormName = Trim(sTMParray(4))
                                    sFileName = Trim(sTMParray(5))
                                    iActiveFlag = Int16.Parse(sTMParray(6))
                                    If Trim(sTMParray(7)) = "" Then sTMParray(7) = "no value"
                                    sHashCRC = Trim(sTMParray(7))
                                    If sHashCRC = "" Then sHashCRC = "no value"
                                    sPrimaryFormFlag = Trim(sTMParray(8))
                                    iLineOfBusCode = Int16.Parse(sTMParray(9))

                                    lTemp = lAddCLForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        'MsgBox("One or more parameters to the ADD_WCP_FORM call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If
                            Case "ADD_EV_FORM"

                                If nNumParms <> 9 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_EV_FORM Function (9 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_EV_FORM Function (9 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else


                                    iStateRowID = Int16.Parse(sTMParray(1))
                                    iFormCategory = Int16.Parse(sTMParray(2))
                                    sFormTitle = Trim(sTMParray(3))
                                    sFormName = Trim(sTMParray(4))
                                    sFileName = Trim(sTMParray(5))
                                    iActiveFlag = Int16.Parse(sTMParray(6))
                                    If Trim(sTMParray(7)) = "" Then sTMParray(7) = "no value"
                                    sHashCRC = Trim(sTMParray(7))
                                    If sHashCRC = "" Then sHashCRC = "no value"
                                    sPrimaryFormFlag = Trim(sTMParray(8))
                                    iLineOfBusCode = Int16.Parse(sTMParray(9))

                                    lTemp = lAddEVForm(iStateRowID, iFormCategory, sFormTitle, sFormName, sFileName, iActiveFlag, sHashCRC, sPrimaryFormFlag, iLineOfBusCode)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_EV_FORM call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_EV_FORM call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_GLOSSARY"
                                If nNumParms <> 4 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_GLOSSARY Function (4 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_GLOSSARY Function (4 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    lTemp = lAddGlossary(Trim(sTMParray(1)), Trim(sTMParray(2)), Int16.Parse(sTMParray(3)), Int16.Parse(sTMParray(4)), 0)
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw 
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_GLOSSARY call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_GLOSSARY call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "ADD_GLOSSARY_EX"
                                If nNumParms <> 5 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw 
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to ADD_GLOSSARY_EX Function (5 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to ADD_GLOSSARY_EX Function (5 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    lTemp = lAddGlossary(Trim(sTMParray(1)), Trim(sTMParray(2)), Int16.Parse(sTMParray(3)), Int16.Parse(sTMParray(4)), Int16.Parse(sTMParray(5)))
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw  
                                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the ADD_GLOSSARY_EX call failed OR the call failed.")
                                        'MsgBox("One or more parameters to the ADD_GLOSSARY_EX call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If

                            Case "GET_COUNT"
                                If nNumParms <> 1 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw MsgBox("Incorrect Number of Parameters Given to GET_COUNT Function (1 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sTMParray(1).Trim())
                                        If objReader.Read() Then
                                            lTemp = objReader.GetInt32(0)
                                        Else
                                            ' todo message - throw 
                                            Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: One or more parameters to the GET_COUNT call failed OR the call failed.")
                                            'MsgBox("One or more parameters to the GET_COUNT call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")
                                            Return False 'take the easy way out
                                        End If
                                    End Using

                                End If
                                ' if successful, place value in designated register
                                sParmArray(nRegister) = Trim(Str(lTemp))

                            Case "DATABASE_UPGRADE_A"
                                If nNumParms <> 1 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw MsgBox("Incorrect Number of Parameters Given to DATABASE_UPGRADE_A Function (1 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    lTemp = lDataBaseUpgrade_A()
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw MsgBox("One or more parameters to the DATABASE_UPGRADE_A call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If
                                End If

                            Case "WCPFORMS_DUALCASEOFF"
                                If nNumParms <> 1 Then
                                    ' ERROR - incorrect number of parameters to function
                                    ' todo message - throw 
                                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to WCPFORMS_DUALCASEOFF Function (5 are required)")
                                    'MsgBox("Incorrect Number of Parameters Given to WCPFORMS_DUALCASEOFF Function (5 are required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                    Return False 'take the easy way out
                                Else
                                    lTemp = lWCPFormsDualCaseOff(Trim(sTMParray(1)))
                                    If lTemp = 0 Then
                                        ' ERROR - Error in script - System Table Name Not Found
                                        ' todo message - throw MsgBox("One or more parameters to the WCPFORMS_DUALCASEOFF call failed OR the call failed.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                        Return False 'take the easy way out
                                    End If

                                    ' if successful, place value in designated register
                                    sParmArray(nRegister) = Trim(Str(lTemp))
                                End If
                            Case Else
                                ' ERROR - UNSUPPORTED FUNCTION
                                ' todo message - throw 
                                Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: This ASSIGN subfunction is unsupported")
                                'MsgBox("This ASSIGN subfunction is unsupported", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                                Return False 'take the easy way out
                        End Select
                        ' SPECIFIC FUNCTION PROCESSING   -    ***  END  ***


                        ' destroy parms array
                        System.Array.Clear(sTMParray, 0, sTMParray.Length)
                    End If
                End If


            ElseIf UCase(Left(sWork, 3)) = "IF " Then
                If g_IfNest > 0 Then
                    g_IfNest = g_IfNest + 1
                    Return True ' JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
                End If

                ' IF  -  compares a value to a register (%%1 through %%9)
                sWork = Trim(Mid(sWork, 4)) ' strip off assign pseudo statement keyword

                ' pick off register to assign
                sTemp2 = Left(sWork, 3)
                If Left(sTemp2, 2) <> "%%" Or (Asc(Mid(sTemp2, 3, 1)) > Asc("9")) Or (Asc(Mid(sTemp2, 3, 1)) < Asc("0")) Then
                    ' SYNTAX ERROR - unexpected format
                    ' todo message - throw 
                    Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect IF Function Syntax")
                    'MsgBox("Incorrect IF Function Syntax", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                    Return False 'take the easy way out
                Else
                    nRegister = Int16.Parse(Mid(sTemp2, 3, 1))
                    sWork = Trim(Mid(sWork, 4)) ' strip off register

                    ' Pick off operator
                    If Left(sWork, 2) = "<>" Then
                        If Not (Mid(sWork, 3) <> sParmArray(nRegister)) Then
                            g_IfNest = g_IfNest + 1
                        End If
                    ElseIf Left(sWork, 1) = "=" Then
                        If Not (Mid(sWork, 2) = sParmArray(nRegister)) Then
                            g_IfNest = g_IfNest + 1
                        End If
                    ElseIf Left(sWork, 1) = "<" Then
                        If Not (Mid(sWork, 2) < sParmArray(nRegister)) Then
                            g_IfNest = g_IfNest + 1
                        End If
                    ElseIf Left(sWork, 1) = ">" Then
                        If Not (Mid(sWork, 2) > sParmArray(nRegister)) Then
                            g_IfNest = g_IfNest + 1
                        End If
                    Else
                        ' todo message - throw 
                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Invalid IF operator.")
                        'MsgBox("Invalid IF operator.", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                        Return False 'take the easy way out
                    End If

                    ' destroy parms array
                    System.Array.Clear(sTMParray, 0, sTMParray.Length)
                End If
            ElseIf UCase(Left(sWork, 5)) = "ENDIF" Then
                If g_IfNest > 0 Then g_IfNest = g_IfNest - 1

            ElseIf UCase(Left(sWork, 14)) = "UPDT_TBL_STAMP" Then
                If g_IfNest > 0 Then
                    Return True  ' JP 2/1/2002    Leave if we're in an IF block that shouldn't be executing
                End If

                sWork = Trim(Mid(sWork, 15)) ' strip off statement keyword

                ' process remainder through parameter substitution function
                sWork = sFormatMsgString(sWork)

                ' determine assignment function
                If InStr(sWork, "(") = 0 Then
                    ' SYNTAX ERROR - function format incorrect
                    ' todo message - throw MsgBox("Pseudo Function Is Missing Open Parenthesis", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                    Return False 'take the easy way out
                Else
                    nPos = InStr(sWork, "(")
                    sWork = Trim(Mid(sWork, nPos + 1))
                    If Right(sWork, 1) <> ")" Then
                        ' SYNTAX ERROR - invalid function syntax - missing closing paren
                        ' todo message - throw 
                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Pseudo Function Is Missing Close Parenthesis")
                        'MsgBox("Pseudo Function Is Missing Close Parenthesis", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                        Exit Function ' take the easy way out
                    Else
                        sWork = Trim(Left(sWork, Len(sWork) - 1)) ' strip off closing paren

                        ' tear out parameters to function
                        nNumParms = 0
                        Do While sWork <> ""
                            nNumParms = nNumParms + 1
                            ReDim Preserve sTMParray(nNumParms)

                            If InStr(sWork, ",") > 0 Then
                                sTMParray(nNumParms) = Left(sWork, InStr(sWork, ",") - 1)
                                sWork = Mid(sWork, InStr(sWork, ",") + 1)
                            Else
                                sTMParray(nNumParms) = sWork
                                sWork = ""
                            End If
                        Loop


                        ' SPECIFIC FUNCTION PROCESSING   -    *** BEGIN ***
                        If nNumParms <> 1 Then
                            ' ERROR - incorrect number of parameters to UPDT_TBL_STAMP Sub
                            ' todo message - throw 
                            Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Incorrect Number of Parameters Given to UPDT_TBL_STAMP Subroutine (1 is required)")
                            'MsgBox("Incorrect Number of Parameters Given to UPDT_TBL_STAMP Subroutine (1 is required)", 48, "PSEUDO STATEMENT SYNTAX ERROR")

                            Exit Function ' take the easy way out
                        Else
                            sParm = Trim(sTMParray(1))
                            Call subTblUpdtStmp(sParm)
                        End If

                        ' SPECIFIC FUNCTION PROCESSING   -    ***  END  ***


                        ' destroy parms array
                        System.Array.Clear(sTMParray, 0, sTMParray.Length)
                    End If
                End If
            Else
                ' unsupported pseudo statement
            End If

            bRetVal = True ' success
        Catch e As RMAppException
            Throw e
        Catch ex As Exception
            LogWriter.LogError("UpgradeUtil.bProcessPseudoStatement", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try
        Return bRetVal
    End Function

    Function lAddCode(ByRef sShortCode As String, ByRef iNLSCode As Short, ByRef sDescription As String, ByRef sSysCodeTable As String, ByRef lRelatedCodeID As Integer) As Integer
        Dim sSQL As String
        Dim lCodeID As Integer
        Dim lTableID As Integer
        Dim bJustAddDesc As Boolean
        Dim lRetVal As Integer = 0
        Try

            ' get table id of code table to add to
            lTableID = lGetTableID(sSysCodeTable)
            If lTableID <= 0 Then
                ' todo message - throw 
                Throw New RMAppException(sSysCodeTable & " is not a Valid Table Name")
                'MsgBox(sSysCodeTable & " is not a Valid Table Name")
                Return 0
            End If

            If UCase(sSysCodeTable) = "STATES" Then
                ' see if this code already exists
                sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" & sSQLStringLiteral(Trim(sShortCode)) & "'"
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                    If objReader.Read() Then
                        lRetVal = objReader.GetInt32(0)
                    End If
                End Using

                ' get a unique id for the state
                lCodeID = objDbUpgrade.lGetNextUID("STATES")
                If lCodeID <= 0 Then
                    ' todo message - throw 
                    Throw New RMAppException("Unable to assign next ID for STATES Table")
                    'MsgBox("Unable to assign next ID for STATES Table")
                    Return 0
                End If

                ' insert the values into the codes text table
                sSQL = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, DELETED_FLAG)"
                sSQL = sSQL & " VALUES (" & Trim(Str(lCodeID)) & ","
                sSQL = sSQL & "'" & sSQLStringLiteral(Trim(sShortCode)) & "','" & sSQLStringLiteral(Trim(sDescription)) & "', 0)"
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                ' stamp codes for recache
                Call subTblUpdtStmp("STATES")
            Else
                Dim bHasCodeId As Boolean = False
                ' see if this code already exists
                sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " & lTableID & " AND SHORT_CODE='" & sSQLStringLiteral(Trim(sShortCode)) & "'"
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                    If objReader.Read() Then
                        bHasCodeId = True

                        lCodeID = objReader.GetInt32("CODE_ID")
                    End If
                End Using
                If bHasCodeId Then
                    sSQL = "SELECT CODE_ID FROM CODES_TEXT WHERE CODE_ID = " & lCodeID
                    Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                        If objReader.Read() Then
                            Return lCodeID

                        ElseIf lCodeID <= 0 Then

                            ' todo message - throw 
                            Throw New RMAppException("Invalid Code ID for CODES Table: " & sShortCode & " " & Str(lTableID))
                            'MsgBox("Invalid Code ID for CODES Table: " & sShortCode & " " & Str(lTableID))
                            Return 0
                        Else
                            bJustAddDesc = True
                        End If
                    End Using
                End If


                If Not bJustAddDesc Then
                    ' get a unique id for the code
                    lCodeID = objDbUpgrade.lGetNextUID("CODES")
                    If lCodeID <= 0 Then
                        ' todo message - throw 
                        Throw New RMAppException("Unable to assign next ID for CODES Table")
                        'MsgBox("Unable to assign next ID for CODES Table")
                        Return 0
                    End If

                    ' insert the values into the codes table
                    sSQL = "INSERT INTO CODES (CODE_ID,TABLE_ID,SHORT_CODE,RELATED_CODE_ID,DELETED_FLAG,LINE_OF_BUS_CODE)"
                    sSQL = sSQL & " VALUES (" & Trim(Str(lCodeID)) & "," & Trim(Str(lTableID)) & ","
                    sSQL = sSQL & "'" & sSQLStringLiteral(Trim(sShortCode)) & "'," & Trim(Str(lRelatedCodeID)) & ",0,0)"
                    DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                End If

                ' insert the values into the codes text table
                sSQL = "INSERT INTO CODES_TEXT (CODE_ID, LANGUAGE_CODE, SHORT_CODE, CODE_DESC)"
                sSQL = sSQL & " VALUES (" & Trim(Str(lCodeID)) & "," & Trim(Str(iNLSCode)) & ","
                sSQL = sSQL & "'" & sSQLStringLiteral(Trim(sShortCode)) & "','" & sSQLStringLiteral(Trim(sDescription)) & "')"
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                ' stamp codes for recache
                Call subTblUpdtStmp("CODES")
            End If

            ' return code id
            lRetVal = lCodeID
        Catch e As RMAppException
            Throw e
        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddCode", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try
        Return lRetVal
    End Function

    Function lAddGlossary(ByRef sSysTableName As String, ByRef sUserTableName As String, ByRef iGlossType As Short, ByRef iNLSCode As Short, ByRef lParentTableID As Integer) As Integer
        Dim sSQL As String
        Dim lTableID As Integer
        Dim lGlossaryTypeCode As Integer
        Dim lRetVal As Integer = 0

        Try

            ' find actual glossary type CODE of the glossary type number
            lGlossaryTypeCode = lGetCodeIDWithShort(Trim(Str(iGlossType)), lGetTableID("GLOSSARY_TYPES"))
            If lGlossaryTypeCode = 0 Then
                Return lRetVal
            End If

            ' see if this table already exists
            sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" & Trim(sSysTableName) & "'"
            ' sSQL = sSQL & " AND GLOSSARY_TYPE_CODE=" & Trim$(Str$(lGlossaryTypeCode))
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    Return objReader.GetInt32("TABLE_ID") ' return existing ID if existing record found
                End If

            End Using

            ' get a unique id for the glossary entry
            lTableID = objDbUpgrade.lGetNextUID("GLOSSARY")
            If lTableID <= 0 Then
                Return lRetVal
            End If

            ' insert the values into the glossary table
            If lParentTableID = 0 Then
                sSQL = "INSERT INTO GLOSSARY (TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,NEXT_UNIQUE_ID," & "DELETED_FLAG,REQD_REL_TABL_FLAG,ATTACHMENTS_FLAG,REQD_IND_TABL_FLAG,LINE_OF_BUS_FLAG)"
                sSQL = sSQL & " VALUES (" & Trim(Str(lTableID)) & ","
                sSQL = sSQL & "'" & Trim(sSysTableName) & "'," & Trim(Str(lGlossaryTypeCode)) & ", 1,0,0,0,0,0)"
            Else
                sSQL = "INSERT INTO GLOSSARY (TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,NEXT_UNIQUE_ID,RELATED_TABLE_ID," & "DELETED_FLAG,REQD_REL_TABL_FLAG,ATTACHMENTS_FLAG,REQD_IND_TABL_FLAG,LINE_OF_BUS_FLAG)"
                sSQL = sSQL & " VALUES (" & Trim(Str(lTableID)) & ","
                sSQL = sSQL & "'" & Trim(sSysTableName) & "'," & Trim(Str(lGlossaryTypeCode)) & ", 1,"
                sSQL = sSQL & lParentTableID & ",0,-1,0,0,0)"
            End If
            DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

            ' insert the values into the glossary text table
            sSQL = "INSERT INTO GLOSSARY_TEXT (TABLE_ID, TABLE_NAME, LANGUAGE_CODE)"
            sSQL = sSQL & " VALUES (" & Trim(Str(lTableID)) & ", '" & Trim(sUserTableName) & "',"
            sSQL = sSQL & Trim(Str(iNLSCode)) & ")"
            DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

            ' stamp glossary for recache
            Call subTblUpdtStmp("GLOSSARY")

            ' return new table id
            lRetVal = lTableID
        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddGlossary", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function

    Function lGetCodeIDWithShort(ByRef sShortCode As String, ByRef lTableID As Integer) As Integer
        Dim lIDTmp As Integer

        lIDTmp = 0
        Try
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID=" & Trim(Str(lTableID)) & " AND SHORT_CODE='" & Trim(sShortCode) & "'")
                If objReader.Read() Then
                    lIDTmp = objReader.GetInt32("CODE_ID")
                End If
            End Using
        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lGetCodeIDWithShort", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lIDTmp
    End Function

    Function lGetTableID(ByRef sSysTableName As String) As Integer
        ' returns 0 on error
        Dim lIDTmp As Integer = 0

        Try
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sSysTableName & "'")
                If objReader.Read() Then
                    lIDTmp = objReader.GetInt32("TABLE_ID")
                End If
            End Using
            lGetTableID = lIDTmp

        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lGetTableID", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lIDTmp
    End Function

    Public Function lWCPFormsDualCaseOff(ByRef sFileName As String) As Integer
        'WCPFORMS_DUALCASEOFF
        Dim sSQL As String
        Dim lResult As Integer = 0
        Dim sFileNameLCase As String

        Try

            'jtodd22 01/24/2006 to stop issues with case of file extension
            'jtodd22 04/23/2007 to stop mixed case issues with Oracle
            'jtodd22 04/23/2007 to stop issues with file name/extension in RMX with Apache Tomcat
            'jtodd22 04/23/2007 convention is to use lower case only

            sFileName = sFileName.Trim()
            sFileNameLCase = sFileName.ToLower()

            sSQL = ""
            sSQL = sSQL & "UPDATE WCP_FORMS"
            sSQL = sSQL & " SET FILE_NAME = '" & sFileNameLCase & "'"
            Select Case m_dbType
                Case eDatabaseType.DBMS_IS_ORACLE
                    sSQL = sSQL & " WHERE LOWER(FILE_NAME) = '" & sFileNameLCase & "'"
                Case Else
                    sSQL = sSQL & " WHERE FILE_NAME = '" & sFileNameLCase & "'"
            End Select

            'jtodd22 04/23/2007 because of older upgrades the same file name can be in the database twice
            Try
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
            Catch
                'save vb6 functionality
            End Try
            lResult = -1
        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lWCPFormsDualCaseOff", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lResult
    End Function


    Function sCheckExecDirect(ByRef sSQL As String, ByRef bExecDirect As Boolean) As String
        Dim sTemp As String

        Try

            sTemp = sSQL.Trim()
            If sTemp.Substring(0, 8).ToUpper() = "[DIRECT]" Then
                sTemp = Trim(Mid(sTemp, 9))
                bExecDirect = True
            Else
                bExecDirect = False
            End If

            sCheckExecDirect = sTemp
        Catch ex As Exception
            bExecDirect = False
            sCheckExecDirect = sSQL
        End Try
    End Function

    Function sCheckForgive(ByRef sSQL As String, ByRef bForgive As Boolean, ByRef sNative As String, ByRef sODBC As String) As String
        Dim sTemp As String

        Try

            sTemp = Trim(Replace(sSQL, Chr(9), " "))
            sTemp = Replace(sTemp, " (", "(")

            If UCase(Left(sTemp, 9)) = "[FORGIVE]" Then
                sTemp = Trim(Mid(sTemp, 10))

                If Left(UCase(sTemp), 12) = "ALTER TABLE " Then
                    sTemp = "[FORGIVE_ALTER] " & sTemp
                ElseIf Left(UCase(sTemp), 13) = "CREATE TABLE " Then
                    sTemp = "[FORGIVE_CREATETABLE] " & sTemp
                ElseIf Left(UCase(sTemp), 13) = "CREATE INDEX " Then
                    sTemp = "[FORGIVE_CREATEINDEX] " & sTemp
                ElseIf Left(UCase(sTemp), 20) = "CREATE UNIQUE INDEX " Then
                    sTemp = "[FORGIVE_CREATEINDEX] " & sTemp
                ElseIf Left(UCase(sTemp), 11) = "DROP INDEX " Then
                    sTemp = "[FORGIVE_DROPINDEX] " & sTemp
                ElseIf Left(UCase(sTemp), 11) = "DROP TABLE " Then
                    sTemp = "[FORGIVE_DROPTABLE] " & sTemp
                ElseIf Left(UCase(sTemp), 12) = "INSERT INTO " Then
                    sTemp = "[FORGIVE_INSERT] " & sTemp
                Else
                    sTemp = "[FORGIVE] " & sTemp
                End If
            End If

            ' Process different forgive scenarios
            If UCase(Left(sTemp, 9)) = "[FORGIVE]" Then
                sTemp = Trim(Mid(sTemp, 10))
                bForgive = True ' traditional - FORGIVE EVERYTHING
                sNative = ""
                sODBC = ""
            ElseIf UCase(Left(sTemp, 15)) = "[FORGIVE_ALTER]" Then
                sTemp = Trim(Mid(sTemp, 16))
                bForgive = True ' FORGIVE EVERYTHING - log if not a duplicate
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1508"
                        sODBC = "S0021"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "2705"
                        sODBC = "S0021"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "2705"
                        sODBC = "S0021"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-328"
                        sODBC = "S0021"
                    Case eDatabaseType.DBMS_IS_DB2 ' TODO / Needs testing dcm 1/2003
                        'DB2, Msg Ref: SQL0612N "<name>" is a duplicate name.
                        'Explanation: A statement was issued with the same name appearing more than once where duplicates are not allowed. Where
                        ' these names appear varies depending on the type of statement.
                        'ALTER TABLE statement cannot add a column to a table using the name of a column that already exists or is the same as
                        ' another column being added. Furthermore, a column name can only be referenced in one ADD or ALTER COLUMN clause in a
                        ' single ALTER TABLE statement.
                        'User Response: Specify unique names as appropriate for the type of statement.
                        'sqlcode: -612, sqlstate: 42711
                        sNative = "-612" ' <own.cname> is a duplicate name.
                        sODBC = "S0021"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "1430"
                        sODBC = ""
                    Case Else
                        sNative = ""
                        sODBC = "S0021"
                End Select

            ElseIf UCase(Left(sTemp, 21)) = "[FORGIVE_CREATETABLE]" Then
                sTemp = Trim(Mid(sTemp, 22))
                bForgive = True ' FORGIVE EVERYTHING - log if not a duplicate
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1303"
                        sODBC = "S0001"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "2714"
                        sODBC = "S0001"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "2714"
                        sODBC = "S0001"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-310"
                        sODBC = "S0001"
                    Case eDatabaseType.DBMS_IS_DB2
                        'DB2, Msg Ref: SQL0601N The name of the object to be created is identical to the existing name "<name>" of type "<type>".
                        'Explanation: The CREATE or ALTER statement tried to create or add an object "<name>" when an object of type "<type>"
                        ' already exists with that name on the application server or in the same statement.
                        'If "<type>" is FOREIGN KEY, PRIMARY KEY, UNIQUE, or CHECK CONSTRAINT, the "<name>" is the constraint name specified in the
                        ' CREATE or ALTER TABLE statement or generated by the system.
                        'Federated system users: some data sources do not provide the appropriate values for the "<name>" and "<type>" message
                        ' tokens. In these cases, "<name>" and "<type>" will have the following format: "OBJECT:<data source> TABLE/VIEW", and
                        ' "UNKNOWN" indicating that the actual values at the specified data source are not known.
                        'The statement cannot be processed. No new object is created, and the existing object is not altered or modified.
                        'User Response: Either drop the existing object or choose another name for the new object.
                        'Federated system users: if the statement is a CREATE FUNCTION MAPPING or a CREATE TYPE MAPPING statement, the user can
                        ' also consider not supplying a type mapping name and the system will automatically generate a unique name for this mapping.
                        'sqlcode: -601, sqlstate: 42710
                        sNative = "-601" ' is identical to existing name
                        sODBC = "S0001"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "955"
                        sODBC = "S0001"
                    Case Else
                        sNative = ""
                        sODBC = "S0001"
                End Select

            ElseIf UCase(Left(sTemp, 21)) = "[FORGIVE_CREATEINDEX]" Then
                sTemp = Trim(Mid(sTemp, 22))
                bForgive = True ' FORGIVE EVERYTHING - log if not a duplicate
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1403"
                        sODBC = "S0011"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "1913"
                        sODBC = "S0011"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "1913"
                        sODBC = "S0011"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-316"
                        sODBC = "S0011"
                    Case eDatabaseType.DBMS_IS_DB2 ' TODO / Needs testing dcm 1/2003
                        'DB2, Msg Ref: SQL0605W The index was not created because the index "<name>" already exists with the required description.
                        'Explanation: A CREATE INDEX operation attempted to create a new index and the indicated index matches the required index.
                        'For CREATE INDEX, two index descriptions match if they identify the same columns in the same order with the same ascending
                        ' or descending specifications, and are both specified as unique or the new index is specified as non-unique. Also, two
                        ' index descriptions match if they identify the same columns in the same order with the same or reverse ascending or
                        ' descending specifications, and at least one description includes the ALLOW REVERSE SCANS parameter.
                        'The new index was not created.
                        'User Response: No action is required unless the existing index "<name>" is not a suitable index. For example, the existing
                        ' index "<name>" is not a suitable index if it does not allow reverse scans, and the required one does (or vice versa). In
                        ' this case, the index "<name>" must be dropped before the required index can be created.
                        'sqlcode: +605, sqlstate: 01550
                        sNative = "605" ' the index <own.iname> already exists   : 4 of 5
                        '              sNative = "-601"   ' is identical to existing name   : this has happened on 1 of 5, why?  dcm 1/2003
                        sODBC = "S0011"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "955"
                        sODBC = "S0001"
                    Case Else
                        sNative = ""
                        sODBC = "S0011"
                End Select

            ElseIf UCase(Left(sTemp, 19)) = "[FORGIVE_DROPTABLE]" Then
                sTemp = Trim(Mid(sTemp, 20))
                bForgive = True ' FORGIVE EVERYTHING - log if not already dropped
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1305"
                        sODBC = "S0002"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "3701"
                        sODBC = "S0002"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "3701"
                        sODBC = "S0002"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-206"
                        sODBC = "S0002"
                    Case eDatabaseType.DBMS_IS_DB2 ' TODO / Needs testing dcm 1/2003
                        'DB2, Msg Ref: SQL0204N "<own.tname>" is an undefined name.
                        ' see below
                        sNative = "-204" ' no such table
                        sODBC = "S0002"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "942"
                        sODBC = "S0002"
                    Case Else
                        sNative = ""
                        sODBC = "S0002"
                End Select

            ElseIf UCase(Left(sTemp, 19)) = "[FORGIVE_DROPINDEX]" Then
                sTemp = Trim(Mid(sTemp, 20))
                bForgive = True ' FORGIVE EVERYTHING - log if not already dropped
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1404"
                        sODBC = "S0012"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "3703"
                        sODBC = "S0012"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "3703"
                        sODBC = "S0012"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-319"
                        sODBC = "S0012"
                    Case eDatabaseType.DBMS_IS_DB2 ' TODO / Needs testing dcm 1/2003
                        'DB2, Msg Ref: SQL0204N "<own.iname>" is an undefined name.
                        ' see below
                        sNative = "-204" ' no such index
                        sODBC = "S0012"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "1418"
                        sODBC = ""
                    Case Else
                        sNative = ""
                        sODBC = "S0012"
                End Select

            ElseIf UCase(Left(sTemp, 16)) = "[FORGIVE_INSERT]" Then
                sTemp = Trim(Mid(sTemp, 17))
                bForgive = True ' FORGIVE EVERYTHING - log if not a duplicate
                Select Case m_dbType
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sNative = "-1605"
                        sODBC = "23000"
                    Case eDatabaseType.DBMS_IS_SQLSRVR
                        sNative = "2601"
                        sODBC = "23000"
                    Case eDatabaseType.DBMS_IS_SYBASE
                        sNative = "2601"
                        sODBC = "23000"
                    Case eDatabaseType.DBMS_IS_INFORMIX
                        sNative = "-239"
                        sODBC = "23000"
                    Case eDatabaseType.DBMS_IS_DB2
                        'DB2, Msg Ref: SQL0803N One or more values in the INSERT statement are not valid because they would produce duplicate rows
                        ' for a table with a primary key, unique constraint, or unique index.
                        'Explanation: The INSERT object table is constrained by one or more UNIQUE indexes to have unique values in certain columns
                        ' or groups of columns. The unique index may exist to support a primary key or unique constraint defined on the table.
                        ' Completing the requested insert or update results in duplicates of the column values.
                        'Alternatively, if a view is the object of the INSERT statement, it is the table on which the view is defined that is
                        ' constrained.
                        'The statement cannot be processed. The table remains unchanged.
                        'User Response: Examine the definitions for all UNIQUE indexes defined on the object table to determine the uniqueness
                        ' constraints those indexes impose.
                        'For an INSERT statement, examine the object table content to determine which of the values in the specified value list
                        ' violates the uniqueness constraint. Alternatively, if the INSERT statement contains a subquery, the object table contents
                        ' addressed by that subquery must be matched against the object table contents to determine the cause of the problem.
                        'sqlcode: -803, sqlstate: 23505
                        sNative = "-803" ' constrains table <own.tname> from having duplicate rows
                        sODBC = "23000"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sNative = "1"
                        sODBC = "23000"
                    Case Else
                        sNative = ""
                        sODBC = "23000"
                End Select

            Else
                bForgive = False
            End If

            sCheckForgive = sTemp
            Exit Function

            ' Move explanation of messages to a reference file.
            ' Other DB2 messages seen. W-Warning(+), N-Notification(-), and C-Critical(-)
            ' ie. SQL0104N is Notification of -104, and SQL0605W is Warning of 605
            ' Native Error = sqlcode and ODBC Error is sometimes sqlstate.  dcm 1/2003.
            '
            'DB2, Msg Ref: SQL0104N An unexpected token "<token>" was found following "<text>". Expected tokens may include: "<token-list>".
            'Explanation: A syntax error in the SQL statement was detected at the specified token following the text "<text>".
            ' The "<text>" field indicates the 20 characters of the SQL statement that preceded the token that is not valid.
            'As an aid to the programmer, a partial list of valid tokens is provided in the SQLERRM field of the SQLCA as
            ' "<token-list>". This list assumes the statement is correct to that point.
            'The statement cannot be processed.
            'User Response: Examine and correct the statement in the area of the specified token.
            'sqlcode: -104, sqlstate: 42601

            'DB2, Msg Ref: SQL0107N The name "<name>" is too long. The maximum length is "<length>".
            'Explanation: The name returned as "<name>" is too long. The maximum length permitted for names of that type is indicated
            ' by "<length>".
            'The names for indexes and constraints can be a maximum length of 18 bytes. The names for columns can be a maximum length
            ' of 30 bytes. The names for savepoints, tables, views and aliases can be a maximum length of 128 bytes. (This does not
            ' include any escape characters, if present.)
            'A maximum of 30 bytes is permitted for a schema name (object qualifier), with the exception of user-defined types, which
            ' allow a maximum of 8 bytes for a schema name.
            'Host variable names must not exceed 30 bytes in length.
            'For the SQL CONNECT statement, an application server name of up to 18 characters in length will be accepted at
            ' pre-compilation time. However, at runtime, an application server name which is greater than 8 characters in length will
            ' cause an error.
            'Also, a password of up to 18 characters in length and an authorization ID of up to 8 characters in length will be accepted
            ' in the SQL CONNECT statement.
            'Federated system users: if in a pass-through session, a data source-specific limit might have been exceeded.
            'The statement cannot be processed. Note: Where character data conversions are performed for applications and databases
            ' running under different codepages, the result of the conversion is exceeding the length limit.
            'User Response: Choose a shorter name or correct the spelling of the object name.
            'Federated system users: for a pass-through session, determine what data source is causing the error (see the problem
            ' determination guide for the failing data sources). Examine the SQL dialect for that data source to determine which
            ' specific limit has been exceeded, and adjust the failing statement as needed.
            'sqlcode: -107, sqlstate: 42622

            'DB2, Msg Ref: SQL0204N "<name>" is an undefined name.
            'Explanation: This error is caused by one of the following:
            'The object identified by "<name>" is not defined in the database.
            'A data type is being used. This error can occur for the following reasons:
            'If "<name>" is qualified, then a data type with this name does not exist in the database.
            'If "<name>" is unqualified, then the user's function path does not contain the schema to which the desired data type belongs.
            'The data type does not exist in the database with a create timestamp earlier than the time the package was bound
            ' (applies to static statements).
            'If the data type is in the UNDER clause of a CREATE TYPE statement, the type name may be the same as the type being
            ' defined, which is not valid.
            'A function is being referenced in one of:
            'a DROP FUNCTION statement
            'a COMMENT ON FUNCTION statement
            'the SOURCE clause of a CREATE FUNCTION statement
            'If "<name>" is qualified, then the function does not exist. If "<name>" is unqualified, then a function of this name does
            ' not exist in any schema of the current function path. Note that a function cannot be sourced on the COALESCE, NULLIF, or
            ' VALUE built-in functions.
            'This return code can be generated for any type of database object.
            'Federated system users: the object identified by "<name>" is not defined in the database or "<name>" is not a nickname in
            ' a DROP NICKNAME statement.
            'Some data sources do not provide the appropriate values for "<name>". In these cases, the message token will have the
            ' following format: "OBJECT:<data source> TABLE/VIEW", indicating that the actual value for the specified data source is unknown.
            'The statement cannot be processed.
            'User Response: Ensure that the object name (including any required qualifiers) is correctly specified in the SQL statement
            ' and it exists. For missing data type or function in SOURCE clause, it may be that the object does not exist, OR it may be
            ' that the object does exist in some schema, but the schema is not present in your function path.
            'Federated system users: if the statement is DROP NICKNAME, make sure the object is actually a nickname. The object might
            ' not exist in the federated database or at the data source. Verify the existence of the federated database objects (if
            ' any) and the data source objects (if any).
            'sqlcode: -204, sqlstate: 42704

            'DB2, Msg Ref: SQL0206N "<name>" is not valid in the context where it is used.
            'Explanation: This error can occur in the following cases:
            'For an INSERT or UPDATE statement, the specified column is not a column of the table, or view that was specified as the
            ' object of the insert or update.
            'For a SELECT or DELETE statement, the specified column is not a column of any of the tables or views identified in a FROM
            ' clause in the statement.
            'For an ORDER BY clause, the specified column is a correlated column reference in a subselect, which is not allowed.
            'For a CREATE TRIGGER statement:
            'A reference is made to a column of the subject table without using an OLD or NEW correlation name.
            'The left hand side of an assignment in the SET transition-variable statement in the triggered action specifies an old
            ' transition variable where only a new transition variable is supported.
            'For a CREATE FUNCTION statement:
            'The RETURN statement of the SQL function references a variable that is not a parameter or other variable that is in the
            ' scope of the RETURN statement.
            'The FILTER USING clause references a variable that is not a parameter name or an expression name in the WHEN clause.
            'The search target in an index exploitation rule does not match some parameter name of the function that is being created.
            'A search argument in an index exploitation rule does not match either an expression name in the EXPRESSION AS clause or a
            ' parameter name of the function being created.
            'For a CREATE INDEX EXTENSION statement, the RANGE THROUGH clause or the FILTER USING clause references a variable that is
            ' not a parameter name that can be used in the clause.
            'The statement cannot be processed.
            'User Response: Verify that the names are specified correctly in the SQL statement. For a SELECT statement, ensure that all
            ' the required tables are named in the FROM clause. For a subselect in an ORDER BY clause, ensure that there are no
            ' correlated column references. If a correlation name is used for a table, verify that subsequent references use the
            ' correlation name and not the table name.
            'For a CREATE TRIGGER statement, ensure that only new transition variables are specified on the left hand side of
            ' assignments in the SET transition-variable statement and that any reference to columns of the subject table have a
            ' correlation name specified.
            'sqlcode: -206, sqlstate: 42703

            'DB2, Msg Ref: SQL0401N The data types of the operands for the operation "<operator>" are not compatible.
            'Explanation: The operation "<operator>" appearing within the SQL statement has a mixture of numeric and nonnumeric
            ' operands, or the operation operands are not compatible.
            'Federated system users: this data type violation can be at the data source or at the federated server.
            'Some data sources do not provide the appropriate values for "<operator>". In these cases the message token will have the
            ' following format: "<data source>:UNKNOWN", indicating that the actual value for the specified data source is unknown.
            'The statement cannot be processed.
            'User Response: Check all operand data types to ensure that they are comparable and compatible with the statement usage.
            'If all the SQL statement operands are correct and accessing a view, check the data types of all the view operands.
            'Federated system users: if the reason is unknown, isolate the problem to the data source failing the request (see the
            ' Problem Determination Guide for procedures to follow to identify the failing data source) and examine the data type
            ' restrictions for that data source.
            'sqlcode: -401, sqlstate: 42818

            'DB2, Msg Ref:SQL0433N Value "<value>" is too long.
            'Explanation: The value "<value>" required truncation by a system (built-in) cast or adjustment function, which was called
            ' to transform the value in some way. The truncation is not allowed where this value is used.
            'The value being transformed is one of the following:
            'an argument to a user defined function (UDF)
            'an input to the SET clause of an UPDATE statement
            'a value being INSERTed into a table
            'an input to a cast or adjustment function in some other context.
            'a recursively referenced column whose data type and length is determined by the initialization part of recursion and may
            ' grow in the iterative part of the recursion.
            'The statement has failed.
            'User Response: If "<value>" is a literal string in the SQL statement, it is too long for its intended use.
            'If "<value>" is not a literal string, examine the SQL statement to determine where the transformation is taking place.
            ' Either the input to the transformation is too long, or the target is too short.
            'Correct the problem and rerun the statement.
            'sqlcode: -433, sqlstate: 22001
        Catch ex As Exception
            bForgive = False
            sCheckForgive = sSQL
        End Try

    End Function

    Function sCheckDBSpecific(ByRef sSQL As String, ByRef iDBMake As Short) As String
        Dim sTemp As String

        Try

            sTemp = Trim(sSQL)
            If UCase(Left(sTemp, 12)) = "[SQL SERVER]" Then
                sTemp = Trim(Mid(sTemp, 13))
                iDBMake = eDatabaseType.DBMS_IS_SQLSRVR
            ElseIf UCase(Left(sTemp, 9)) = "[SQLSRVR]" Then
                sTemp = Trim(Mid(sTemp, 10))
                iDBMake = eDatabaseType.DBMS_IS_SQLSRVR
            ElseIf UCase(Left(sTemp, 8)) = "[SYBASE]" Then
                sTemp = Trim(Mid(sTemp, 9))
                iDBMake = eDatabaseType.DBMS_IS_SYBASE
            ElseIf UCase(Left(sTemp, 8)) = "[ACCESS]" Then
                sTemp = Trim(Mid(sTemp, 9))
                iDBMake = eDatabaseType.DBMS_IS_ACCESS
            ElseIf UCase(Left(sTemp, 8)) = "[ORACLE]" Then
                sTemp = Trim(Mid(sTemp, 9))
                iDBMake = eDatabaseType.DBMS_IS_ORACLE
            ElseIf UCase(Left(sTemp, 10)) = "[INFORMIX]" Then
                sTemp = Trim(Mid(sTemp, 11))
                iDBMake = eDatabaseType.DBMS_IS_INFORMIX
            ElseIf UCase(Left(sTemp, 5)) = "[DB2]" Then
                sTemp = Trim(Mid(sTemp, 6))
                iDBMake = eDatabaseType.DBMS_IS_DB2
            Else
                iDBMake = -1 ' line not db specific
            End If

            sCheckDBSpecific = sTemp

        Catch ex As Exception
            iDBMake = -1
            sCheckDBSpecific = sSQL
        End Try
    End Function

    Function sFormatMsgString(ByRef sText As String) As String
        Dim sRetVal As String = ""
        Dim sWork As String
        Dim i As Short
        Dim sFirstPart As String
        Dim sSecondPart As String
        Dim nPos As Integer
        '    ReDim sTmpArray(0) As String
        Try

            ' plug in carriage returns
            nPos = InStr(sText, "\n")
            sWork = sText
            Do While nPos <> 0
                sFirstPart = Left(sWork, nPos - 1)
                sSecondPart = Mid(sWork, nPos + 2)

                sWork = sFirstPart & vbCrLf & sSecondPart
                nPos = InStr(sWork, "\n")
            Loop

            ' plug in parameters
            '    If Trim$(sMsgArguments) <> "" Then
            '        sTmp = sMsgArguments
            '        Do While sTmp <> ""
            '            iCount = iCount + 1
            '            ReDim Preserve sTmpArray(iCount)  As String
            '            If InStr(sTmp, Chr$(9)) Then
            '                sTmpArray(iCount) = Left(sTmp, InStr(sTmp, Chr$(9)) - 1)
            '                sTmp = Mid(sTmp, InStr(sTmp, Chr$(9)) + 1)
            '            Else
            '                sTmpArray(iCount) = sTmp
            '                sTmp = ""
            '            End If
            '        Loop

            For i = 1 To 9
                Do While InStr(sWork, "%%" & Trim(CStr(i))) > 0
                    nPos = InStr(sWork, "%%" & Trim(CStr(i)))
                    sWork = Left(sWork, nPos - 1) & sParmArray(i) & Mid(sWork, nPos + 3)
                Loop
            Next i
            '    End If

            sRetVal = sWork

        Catch ex As Exception
            Throw New RMAppException("Critical Error- " & ex.Message & " in {sFormatMsgString}")

        End Try
        Return sRetVal
    End Function

    Public Shared Function GetSystemPath() As String
        If Right(My.Application.Info.DirectoryPath, 1) = "\" Then
            GetSystemPath = My.Application.Info.DirectoryPath
        Else
            GetSystemPath = My.Application.Info.DirectoryPath & "\"
        End If
    End Function

    Sub subTblUpdtStmp(ByRef sTableName As String)
        Dim sTmp As String

        Try
            sTmp = Now.ToString("yyyyMMddhhmmss")
            Mid(sTmp, Len(sTmp) - 1, 2) = "00"
            DbFactory.ExecuteNonQuery(m_connectionString, "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" & sTmp & "'  WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'")
        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.subTblUpdtStmp", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try

    End Sub

    ' ''    Public Function sSpecialFunctions(ByRef sStr As String) As String
    ' ''        On Error GoTo ET_sSpecialFunctions

    ' ''        Dim iPos As Short
    ' ''        Dim iPos2 As Short
    ' ''        Dim iPos3 As Short
    ' ''        Dim iPos4 As Short
    ' ''        Dim sTmp As String
    ' ''        Dim sTmp2 As String
    ' ''        Dim sTmp3 As String
    ' ''        Dim lStateID As Integer
    ' ''        Dim lCodeID As Integer
    ' ''        Dim lTableID As Integer

    ' ''        sTmp = sStr
    ' ''        iPos = InStr(sTmp, "[STATE=")
    ' ''        While iPos
    ' ''            iPos2 = InStr(iPos, sTmp, "]")
    ' ''            sTmp2 = Trim(Mid(sTmp, iPos + 7, iPos2 - (iPos + 7)))

    ' ''            lStateID = lGetStateID(sTmp2)

    ' ''            sTmp = Left(sTmp, iPos - 1) & Trim(Str(lStateID)) & Mid(sTmp, iPos2 + 1)

    ' ''            iPos = InStr(sTmp, "[STATE=")
    ' ''        End While

    ' ''        iPos = InStr(sTmp, "[CODE:")
    ' ''        While iPos
    ' ''            iPos2 = InStr(iPos, sTmp, "]")
    ' ''            iPos3 = InStr(iPos, sTmp, ":")
    ' ''            iPos4 = InStr(iPos, sTmp, "=")
    ' ''            sTmp2 = Trim(Mid(sTmp, iPos4 + 1, iPos2 - (iPos4 + 1)))
    ' ''            sTmp3 = Trim(Mid(sTmp, iPos3 + 1, iPos4 - (iPos3 + 1)))

    ' ''            lTableID = lGetTableID(sTmp3)
    ' ''            lCodeID = lGetCodeIDWithShort(sTmp2, lTableID)

    ' ''            sTmp = Left(sTmp, iPos - 1) & Trim(Str(lCodeID)) & Mid(sTmp, iPos2 + 1)

    ' ''            iPos = InStr(sTmp, "[CODE:")
    ' ''        End While

    ' ''        sSpecialFunctions = sTmp
    ' ''        Exit Function

    ' ''ET_sSpecialFunctions:
    ' ''        Dim EResult As Short

    ' ''        bAborted = True
    ' ''        EResult = iGeneralError(Err.Number, ErrorToString(), "UpgradeUtil.sSpecialFunctions")
    ' ''        Select Case EResult
    ' ''            Case MsgBoxResult.Retry
    ' ''                Resume
    ' ''            Case Else
    ' ''                Exit Function
    ' ''        End Select
    ' ''    End Function

    Public Function lGetStateID(sState As String) As Integer
        Dim lIDTmp As Integer = 0

        Try
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID ='" & sState.Trim() & "'")
                If objReader.Read() Then
                    lIDTmp = objReader.GetInt32(0)
                End If
            End Using

        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lGetStateID", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try

        Return lIDTmp
    End Function

    Function lAddStateField(ByRef sStateTable As String, ByRef sUserPrompt As String, ByRef sSysFieldName As String, ByRef lFieldType As Integer, ByRef lFieldSize As Integer, ByRef bRequired As Boolean, ByRef bDeleted As Boolean, ByRef bLookup As Boolean, ByRef bIsPatterned As Boolean, ByRef sPattern As String, ByRef sCodeTable As String) As Integer
        Dim sSQL As String
        Dim lRetVal As Integer = 0
        Dim lID As Integer
        Dim objWriter As DbWriter = Nothing

        Try

            sSysFieldName = sSysFieldName.ToUpper()

            Select Case lFieldType ' Now enforce field size  JP 4/4/2003, dcm 5/2/2003
                Case 0 ' normal string
                    If lFieldSize = 0 Then lFieldSize = 50
                Case 3 ' date (string 8)
                    lFieldSize = 8
                Case 4 ' time (string 6)
                    lFieldSize = 6
                Case 10, 12 ' claim number lookup, event number lookup (string 25)
                    lFieldSize = 25
                Case 13 ' vehicle lookup - VIN (string 20)
                    lFieldSize = 20
                Case Else ' number, id, etc
                    If lFieldSize <> 0 Then lFieldSize = 0
            End Select

            sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" & sStateTable & "'" & " AND SYS_FIELD_NAME = '" & sSysFieldName & "'"

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lID = objReader.GetInt32("FIELD_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY")
                    objWriter.Fields("FIELD_ID").Value = lID

                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("SEQ_NUM").Value = lID
                objWriter.Fields("HELP_CONTEXT_ID").Value = objDbUpgrade.lGetNextUID("HELP_DEF")

                objWriter.Fields("USER_PROMPT").Value = sUserPrompt ' this allows user to change prompts.  dcm 6/2003.
                objWriter.Fields("SUPP_TABLE_NAME").Value = sStateTable
                objWriter.Fields("SYS_FIELD_NAME").Value = sSysFieldName
                objWriter.Fields("FIELD_TYPE").Value = lFieldType
                objWriter.Fields("FIELD_SIZE").Value = lFieldSize
                objWriter.Fields("REQUIRED_FLAG").Value = bRequired
                objWriter.Fields("DELETE_FLAG").Value = 0
                objWriter.Fields("LOOKUP_FLAG").Value = bLookup
                objWriter.Fields("IS_PATTERNED").Value = bIsPatterned
                objWriter.Fields("PATTERN").Value = sPattern
                objWriter.Fields("CODE_FILE_ID").Value = lGetTableID(sCodeTable)
                objWriter.Fields("GRP_ASSOC_FLAG").Value = 0
                objWriter.Execute()
            End If

            ' Add field to database table if needed
            If lFieldType <> 14 And lFieldType <> 15 And lFieldType <> 16 Then
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " & sStateTable & " WHERE 0=1")
                    If Not objReader.ExistField(sSysFieldName) Then
                        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize) ' includes NULL 6.0 dcm

                        sSQL = "ALTER TABLE " & sStateTable & " ADD " & sSysFieldName & " " & sSQL
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    End If
                End Using
            End If

            lRetVal = lID

        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddStateField", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try

        Return lRetVal
    End Function

    Function lAddStateField_2(ByRef sStateTable As String, ByRef sUserPrompt As String, ByRef sSysFieldName As String, ByRef lFieldType As Integer, ByRef lFieldSize As Integer, ByRef bRequired As Boolean, ByRef bDeleted As Boolean, ByRef bLookup As Boolean, ByRef bIsPatterned As Boolean, ByRef sPattern As String, ByRef sCodeTable As String, ByRef bGroupAssoc As Boolean, ByRef sAssocField As String) As Integer
        Dim sSQL As String
        Dim objWriter As DbWriter = Nothing
        Dim lID As Integer = 0

        Try

            sSysFieldName = UCase(sSysFieldName)

            Select Case lFieldType ' Now enforce field size  JP 4/4/2003, dcm 5/2/2003
                Case 0 ' normal string
                    If lFieldSize = 0 Then lFieldSize = 50
                Case 3 ' date (string 8)
                    lFieldSize = 8
                Case 4 ' time (string 6)
                    lFieldSize = 6
                Case 10, 12 ' claim number lookup, event number lookup (string 25)
                    lFieldSize = 25
                Case 13 ' vehicle lookup - VIN (string 20)
                    lFieldSize = 20
                Case Else ' number, id, etc
                    If lFieldSize <> 0 Then lFieldSize = 0
            End Select

            sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" & sStateTable & "'" & " AND SYS_FIELD_NAME = '" & sSysFieldName & "'"

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lID = objReader.GetInt32("FIELD_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY")
                    objWriter.Fields("FIELD_ID").Value = lID
                    objWriter.Fields("SUPP_TABLE_NAME").Value = sStateTable
                    objWriter.Fields("SYS_FIELD_NAME").Value = sSysFieldName
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("SEQ_NUM").Value = lID
                objWriter.Fields("HELP_CONTEXT_ID").Value = objDbUpgrade.lGetNextUID("HELP_DEF")

                objWriter.Fields("USER_PROMPT").Value = sUserPrompt ' this allows user to change prompts.  dcm 6/2003.
                objWriter.Fields("SUPP_TABLE_NAME").Value = sStateTable
                objWriter.Fields("SYS_FIELD_NAME").Value = sSysFieldName
                objWriter.Fields("FIELD_TYPE").Value = lFieldType
                objWriter.Fields("FIELD_SIZE").Value = lFieldSize
                objWriter.Fields("REQUIRED_FLAG").Value = bRequired
                objWriter.Fields("DELETE_FLAG").Value = 0
                objWriter.Fields("LOOKUP_FLAG").Value = bLookup
                objWriter.Fields("IS_PATTERNED").Value = bIsPatterned
                objWriter.Fields("PATTERN").Value = sPattern
                objWriter.Fields("CODE_FILE_ID").Value = lGetTableID(sCodeTable)
                objWriter.Fields("GRP_ASSOC_FLAG").Value = bGroupAssoc
                objWriter.Fields("ASSOC_FIELD").Value = sAssocField
                objWriter.Execute()
            End If

            ' Add field to database table if needed
            If lFieldType <> 14 And lFieldType <> 15 And lFieldType <> 16 Then
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " & sStateTable & " WHERE 0=1")
                    If Not objReader.ExistField(sSysFieldName) Then
                        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize) ' includes NULL 6.0 dcm

                        sSQL = "ALTER TABLE " & sStateTable & " ADD " & sSysFieldName & " " & sSQL
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    End If
                End Using
            End If

        Catch ex As Exception
            lID = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddStateField_2", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try
        Return lID
    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : lAddStateField_OrgHierarchyAssoc
    ' Author    : jtodd22
    ' Date      : 2/3/2010
    ' Purpose   : MITS 19176
    '---------------------------------------------------------------------------------------
    '
    Function lAddStateField_OrgHierarchyAssoc(ByRef sStateTable As String, ByRef sUserPrompt As String, ByRef sSysFieldName As String, ByRef lFieldType As Integer, ByRef lFieldSize As Integer, ByRef bRequired As Boolean, ByRef bDeleted As Boolean, ByRef bLookup As Boolean, ByRef bIsPatterned As Boolean, ByRef sPattern As String, ByRef sCodeTable As String, ByRef bGroupAssoc As Boolean, ByRef sAssocField As String, ByRef sListOrgHierarchyIDs As String) As Integer
        Dim i As Integer
        Dim sSQL As String
        Dim sSQLToExe As String
        Dim objWriter As DbWriter = Nothing
        Dim lID As Integer
        Dim sNow As String
        Dim sValues As String
        Dim lRetVal As Integer = 0
        Try

20:         sNow = DateTime.Now.ToString("yyyyMMddhhmmss")
30:         sSysFieldName = sSysFieldName.ToUpper()
40:         Select Case lFieldType ' Now enforce field size  JP 4/4/2003, dcm 5/2/2003
                Case 0 ' normal string
50:                 If lFieldSize = 0 Then lFieldSize = 50
60:             Case 3 ' date (string 8)
70:                 lFieldSize = 8
80:             Case 4 ' time (string 6)
90:                 lFieldSize = 6
100:            Case 10, 12 ' claim number lookup, event number lookup (string 25)
110:                lFieldSize = 25
120:            Case 13 ' vehicle lookup - VIN (string 20)
130:                lFieldSize = 20
140:            Case Else ' number, id, etc
150:                If lFieldSize <> 0 Then lFieldSize = 0
160:                End Select

            sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" & sStateTable & "'" & " AND SYS_FIELD_NAME = '" & sSysFieldName & "'"
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lID = objReader.GetInt32("FIELD_ID")
                    Return lID
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("SUPP_DICTIONARY")
                    objWriter.Fields("FIELD_ID").Value = lID
                    objWriter.Fields("SUPP_TABLE_NAME").Value = sStateTable
                    objWriter.Fields("SYS_FIELD_NAME").Value = sSysFieldName
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("SEQ_NUM").Value = lID
                objWriter.Fields("HELP_CONTEXT_ID").Value = objDbUpgrade.lGetNextUID("HELP_DEF")

                objWriter.Fields("USER_PROMPT").Value = sUserPrompt ' this allows user to change prompts.  dcm 6/2003.
                objWriter.Fields("SUPP_TABLE_NAME").Value = sStateTable
                objWriter.Fields("SYS_FIELD_NAME").Value = sSysFieldName
                objWriter.Fields("FIELD_TYPE").Value = lFieldType
                objWriter.Fields("FIELD_SIZE").Value = lFieldSize
                objWriter.Fields("REQUIRED_FLAG").Value = bRequired
                objWriter.Fields("DELETE_FLAG").Value = 0
                objWriter.Fields("LOOKUP_FLAG").Value = bLookup
                objWriter.Fields("IS_PATTERNED").Value = bIsPatterned
                objWriter.Fields("PATTERN").Value = sPattern
                objWriter.Fields("CODE_FILE_ID").Value = lGetTableID(sCodeTable)
                objWriter.Fields("GRP_ASSOC_FLAG").Value = bGroupAssoc
                objWriter.Fields("ASSOC_FIELD").Value = sAssocField
            End If


            ' Add field to database table if needed
            If lFieldType <> 14 And lFieldType <> 15 And lFieldType <> 16 Then
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM " & sStateTable & " WHERE 0=1")
                    If Not objReader.ExistField(sSysFieldName) Then
                        sSQL = objDbUpgrade.sTypeMap(lFieldType, lFieldSize) ' includes NULL 6.0 dcm

                        sSQL = "ALTER TABLE " & sStateTable & " ADD " & sSysFieldName & " " & sSQL
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    End If
                End Using
            End If

            lRetVal = lID

590:        Dim sArray() As String
            If Trim(sListOrgHierarchyIDs) <> "" Then
600:            sArray = Split(sListOrgHierarchyIDs, ";")
610:            sSQL = ""
620:            sSQL = sSQL & "INSERT INTO SUPP_ASSOC"
630:            sSQL = sSQL & " (FIELD_ID,ASSOC_CODE_ID,UPDATED_BY_USER,DTTM_RCD_LAST_UPD)"
640:            sSQL = sSQL & " VALUES"
650:            For i = 0 To UBound(sArray, 1)
660:                sValues = "(" & lID & ", " & sArray(i) & ", 'CSC_dbupgrade', '" & sNow & "')"
670:                sSQLToExe = sSQL & sValues
                    Try
690:                    DbFactory.ExecuteNonQuery(m_connectionString, sSQLToExe)
                    Catch ex As Exception
730:                    sValues = "(" & lID & ", " & sArray(i) & ", 'dbupgrad', '" & sNow & "')"
740:                    sSQLToExe = sSQL & sValues
750:                    DbFactory.ExecuteNonQuery(m_connectionString, sSQLToExe)
                    End Try
780:            Next i

            End If
        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddStateField_OrgHierarchyAssoc", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
        End Try
        Return lRetVal
    End Function


    Public Function lAddJurisdiction(ByVal sStateID As String, ByVal sStateName As String, ByVal dEmployerLiability As Double, ByVal dMaxClaimAmount As Double, ByVal iRatingMethodCode As Short, ByVal iUSLLimit As Short, ByVal iWeightingStepVal As Short, ByVal iDeletedFlag As Short) As Integer
        Dim sSQL As String
        Dim lID As Integer
        Dim lRetVal As Integer = 0
        Dim objWriter As DbWriter = Nothing

        Try

            sStateID = sStateID.ToUpper()

            sSQL = "SELECT * FROM STATES WHERE STATE_ID  = '" & sStateID & "'"
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    objWriter = DbFactory.GetDbWriter(objReader, True)
                    lRetVal = objReader.GetInt32("STATE_ROW_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("STATES")
                    objWriter.Fields("STATE_ROW_ID").Value = lID
                    objWriter.Fields("STATE_NAME").Value = sStateName
                    objWriter.Fields("EMPLOYER_LIABILITY").Value = dEmployerLiability
                    objWriter.Fields("MAX_CLAIM_AMOUNT").Value = dMaxClaimAmount
                    objWriter.Fields("RATING_METHOD_CODE").Value = iRatingMethodCode
                    objWriter.Fields("USL_LIMIT").Value = iUSLLimit
                    objWriter.Fields("WEIGHTING_STEP_VAL").Value = iWeightingStepVal
                    objWriter.Fields("DELETED_FLAG").Value = iDeletedFlag
                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("STATE_ID").Value = sStateID
                objWriter.Execute()
            End If


        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddJurisdiction", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function
    Public Function lAddWCPForm(ByVal iStateRowID As Short, ByVal iFormCategory As Short, ByVal sFormTitle As String, ByVal sFormName As String, ByVal sFileName As String, ByVal iActiveFlag As Short, ByVal sHashCRC As String, ByVal sPrimaryFormFlag As String, ByVal iLineOfBusCode As Short) As Integer

        Dim iOriginalJurisRowID As Short
        Dim sSQL As String
        Dim lID As Integer
        Dim sNow As String
        Dim sText As String
        Dim objWriter As DbWriter = Nothing
        Dim lRetVal As Integer = 0

        Try

            sNow = Now.ToString("yyyyMMddhhmmss")

            'jtodd22 12/19/2005 to stop issues with case of file extension
            'jtodd22 04/23/2007 to stop issues with Informix, Oracle and RMX (Apache Tomcat) file names must be lower case
            sFileName = sFileName.Trim()
            sFileName = sFileName.ToLower()

            'jtodd22 01/08/2007 to trap bad jurisdiction IDs from STATES table causing duplication errors
            sSQL = "SELECT * FROM WCP_FORMS"

            Select Case m_dbType
                Case eDatabaseType.DBMS_IS_ACCESS
                    sSQL = sSQL & " WHERE LCASE(FILE_NAME) = '" & sFileName & "'"

                Case eDatabaseType.DBMS_IS_ORACLE
                    sSQL = sSQL & " WHERE LOWER(FILE_NAME) = '" & sFileName & "'"

                Case Else
                    sSQL = sSQL & " WHERE LOWER(FILE_NAME) = '" & sFileName & "'"
            End Select

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    iOriginalJurisRowID = objReader.GetInt16("STATE_ROW_ID")
                End If
            End Using

            'jtodd 01/08/2007 a 0 (zero) iOriginalJurisRowID means the form does not exist in WCP_FORMS
            If iOriginalJurisRowID <> 0 Then
                If iOriginalJurisRowID = iStateRowID Then
                    'do nothing
                Else
                    sText = ""
                    sText = sText & "The application is attempting to insert a duplicate record "
                    sText = sText & "that will cause an error." & vbCrLf
                    sText = sText & "The STATES table may contain bad data." & vbCrLf
                    sText = sText & "The original jurisdiction ID is " & iOriginalJurisRowID & "." & vbCrLf
                    sText = sText & "The new jurisdiction ID is " & iStateRowID & "." & vbCrLf

                    ' todo  review again should popup GUI message or just log it !!!
                    Throw New RMAppException(sText)
                    'Err.Raise(vbObjectError + 100, "UpgradeUtil.lAddWCPForm", sText)
                End If
            End If

            sSQL = "SELECT * FROM WCP_FORMS"
            sSQL = sSQL & " WHERE STATE_ROW_ID  = " & iStateRowID
            sSQL = sSQL & " AND FORM_CATEGORY = " & iFormCategory

            Select Case m_dbType
                Case eDatabaseType.DBMS_IS_ACCESS
                    sSQL = sSQL & " AND LCASE(FILE_NAME) = '" & sFileName & "'"

                Case eDatabaseType.DBMS_IS_ORACLE
                    sSQL = sSQL & " AND LOWER(FILE_NAME) = '" & sFileName & "'"

                Case Else
                    sSQL = sSQL & " AND LOWER(FILE_NAME) = '" & sFileName & "'"
            End Select

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lRetVal = objReader.GetInt32("FORM_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("WCP_FORMS")
                    objWriter.Fields("FORM_ID").Value = lID
                    objWriter.Fields("STATE_ROW_ID").Value = iStateRowID
                    objWriter.Fields("FORM_CATEGORY").Value = iFormCategory
                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("FORM_TITLE").Value = sFormTitle
                objWriter.Fields("FORM_NAME").Value = sFormName
                objWriter.Fields("FILE_NAME").Value = sFileName
                objWriter.Fields("ACTIVE_FLAG").Value = iActiveFlag
                objWriter.Fields("HASH_CRC").Value = sHashCRC
                objWriter.Fields("PRIMARY_FORM_FLAG").Value = sPrimaryFormFlag
                objWriter.Fields("LINE_OF_BUS_CODE").Value = iLineOfBusCode

                If objWriter.Fields.ExistField("UPDATED_BY_USER") Then
                    objWriter.Fields("ADDED_BY_USER").Value = "CSCdbUpG"
                    objWriter.Fields("DTTM_RCD_ADDED").Value = sNow
                    objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
                    objWriter.Fields("UPDATED_BY_USER").Value = "CSCdbUpG"
                End If

                objWriter.Execute()
            End If
        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddWCPForm", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function

    Public Function lAddMergeDictionaryLine(ByVal iCatID As Short, ByVal sFieldName As String, ByVal sFieldDesc As String, ByVal sFieldTable As String, ByVal sFieldType As String, ByVal sOptMask As String, ByVal sDisplayCat As String, ByVal sCodeTable As String) As Integer
        Dim sSQL As String
        Dim lRetVal As Integer = 0
        Dim lID As Integer
        Dim objWriter As DbWriter = Nothing

        Try

            sSQL = "SELECT * FROM MERGE_DICTIONARY WHERE FIELD_NAME " & IIf(sFieldName = "", "IS NULL", "= '" & sFieldName & "'") & " AND FIELD_TABLE = '" & sFieldTable & "' AND CAT_ID  = " & iCatID & " AND FIELD_TYPE = " & sFieldType
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lRetVal = objReader.GetInt32("FIELD_ID")
                Else
                    lID = objDbUpgrade.lGetNextUID("MERGE_DICTIONARY")
                    If lID = 0 Then
                        ' todo !!!
                        ' todo message - throw
                        Throw New RMAppException("PSEUDO STATEMENT SYNTAX ERROR: Could not determine the next unique field ID for MERGE_DICTIONARY.")
                        'MsgBox("Could not determine the next unique field ID for MERGE_DICTIONARY.", 48, "PSEUDO STATEMENT SYNTAX ERROR")
                    End If
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    objWriter.Fields("FIELD_NAME").Value = sFieldName
                    objWriter.Fields("CAT_ID").Value = iCatID
                    objWriter.Fields("FIELD_ID").Value = lID

                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("FIELD_DESC").Value = sFieldDesc
                objWriter.Fields("FIELD_TABLE").Value = sFieldTable
                objWriter.Fields("FIELD_TYPE").Value = sFieldType
                objWriter.Fields("OPT_MASK").Value = sOptMask
                objWriter.Fields("DISPLAY_CAT").Value = sDisplayCat
                objWriter.Fields("CODE_TABLE").Value = sCodeTable

                objWriter.Execute()
            End If

        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddMergeDictionaryLine", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function

    Public Function lGetMaxFieldID(ByRef sTableName As String) As Integer

        Dim lResVal As Integer = 0

        Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM " & sTableName)
            If objReader.Read() Then
                If objReader.IsDBNull(0) Then
                    'MsgBox "The supplemental dictionary has already been updated", 64, "UPDSUPP"
                    lResVal = 0
                Else
                    lResVal = objReader.GetInt32(0) + 1
                End If
            End If
        End Using

        Return lResVal
    End Function


    Public Function lAddCLForm(ByVal iStateRowID As Short, ByVal iFormCategory As Short, ByVal sFormTitle As String, ByVal sFormName As String, ByVal sFileName As String, ByVal iActiveFlag As Short, ByVal sHashCRC As String, ByVal sPrimaryFormFlag As String, ByVal iLineOfBusCode As Short) As Integer 'mwc 05/18/2001

        Dim sSQL As String
        Dim lRetVal As Integer = 0
        Dim lID As Integer
        Dim objWriter As DbWriter = Nothing

        Try

            sSQL = "SELECT * FROM CL_FORMS" & " WHERE STATE_ROW_ID  = " & iStateRowID & " AND FORM_CATEGORY = " & iFormCategory & " AND FILE_NAME = '" & sFileName & "'"

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lRetVal = objReader.GetInt32("FORM_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("CL_FORMS")

                    objWriter.Fields("STATE_ROW_ID").Value = iStateRowID
                    objWriter.Fields("FORM_CATEGORY").Value = iFormCategory
                    objWriter.Fields("FORM_ID").Value = lID
                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("FORM_TITLE").Value = sFormTitle
                objWriter.Fields("FORM_NAME").Value = sFormName
                objWriter.Fields("FILE_NAME").Value = sFileName
                objWriter.Fields("ACTIVE_FLAG").Value = iActiveFlag
                objWriter.Fields("HASH_CRC").Value = sHashCRC
                objWriter.Fields("PRIMARY_FORM_FLAG").Value = sPrimaryFormFlag
                objWriter.Fields("LINE_OF_BUS_CODE").Value = iLineOfBusCode

                objWriter.Execute()
            End If
        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddCLForm", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function

    Public Function lAddJurisdictionalFROI(ByVal iStateRowID As Short, ByVal sFormName As String, ByVal sFileName As String, ByVal iActiveFlag As Short, ByVal sHashCRC As String, ByVal iPrimaryFormFlag As Short, ByVal iLineOfBusCode As Short) As Integer

        Dim sSQL As String
        Dim lRetVal As Integer = 0
        Dim lID As Integer
        Dim objWriter As DbWriter = Nothing

        Try

            sSQL = "SELECT * FROM JURIS_FORMS" & " WHERE STATE_ROW_ID  = " & iStateRowID & " AND PDF_FILE_NAME = '" & sFileName & "'"

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lRetVal = objReader.GetInt32("FORM_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("JURIS_FORMS")

                    objWriter.Fields("STATE_ROW_ID").Value = iStateRowID
                    objWriter.Fields("FORM_ID").Value = lID
                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("FORM_NAME").Value = sFormName
                objWriter.Fields("PDF_FILE_NAME").Value = sFileName
                objWriter.Fields("ACTIVE_FLAG").Value = iActiveFlag
                objWriter.Fields("HASH_CRC").Value = sHashCRC
                objWriter.Fields("PRIMARY_FORM_FLAG").Value = iPrimaryFormFlag
                objWriter.Fields("LINE_OF_BUS_CODE").Value = iLineOfBusCode

                objWriter.Execute()
            End If

        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddJurisdictionalFROI", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try

        Return lRetVal
    End Function

    Public Function lAddEVForm(ByVal iStateRowID As Short, ByVal iFormCategory As Short, ByVal sFormTitle As String, ByVal sFormName As String, ByVal sFileName As String, ByVal iActiveFlag As Short, ByVal sHashCRC As String, ByVal sPrimaryFormFlag As String, ByVal iLineOfBusCode As Short) As Integer 'mwc 05/18/2001

        Dim sSQL As String
        Dim lRetVal As Integer = 0
        Dim lID As Integer
        Dim objWriter As DbWriter = Nothing

        Try

            sSQL = "SELECT * FROM EV_FORMS" & " WHERE STATE_ROW_ID  = " & iStateRowID & " AND FORM_CATEGORY = " & iFormCategory & " AND FILE_NAME = '" & sFileName & "'"

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lRetVal = objReader.GetInt32("FORM_ID")
                Else
                    objWriter = DbFactory.GetDbWriter(objReader, False)
                    objWriter.Where.Clear()
                    lID = objDbUpgrade.lGetNextUID("EV_FORMS")

                    objWriter.Fields("STATE_ROW_ID").Value = iStateRowID
                    objWriter.Fields("FORM_CATEGORY").Value = iFormCategory
                    objWriter.Fields("FORM_ID").Value = lID
                    lRetVal = lID
                End If
            End Using
            If Not objWriter Is Nothing Then
                objWriter.Fields("FORM_TITLE").Value = sFormTitle
                objWriter.Fields("FORM_NAME").Value = sFormName
                objWriter.Fields("FILE_NAME").Value = sFileName
                objWriter.Fields("ACTIVE_FLAG").Value = iActiveFlag
                objWriter.Fields("HASH_CRC").Value = sHashCRC
                objWriter.Fields("PRIMARY_FORM_FLAG").Value = sPrimaryFormFlag
                objWriter.Fields("LINE_OF_BUS_CODE").Value = iLineOfBusCode

                objWriter.Execute()
            End If

        Catch ex As Exception
            lRetVal = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lAddEVForm", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
        Return lRetVal
    End Function
    Function sSQLStringLiteral(ByRef sText As String) As String
        sSQLStringLiteral = Replace(sText, "'", "''")
    End Function

    Public Function lDataBaseUpgrade_A() As Integer
        Dim sSQL As String
        Dim result As Integer

        Try

            sSQL = "DELETE FROM WCP_FORMS WHERE FILE_NAME IS NULL"
            Try
                result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
            Catch ex As Exception
                If isCriticalError() Then
                    Throw ex
                End If
            End Try
            'rpandey20 02/27/2009  -- MITS 14446 Removing duplicate rows from table ******
            sSQL = " DELETE  FROM WCP_FORMS WHERE FORM_ID NOT IN ( "
            sSQL = sSQL & " SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 GROUP BY FILE_NAME "
            sSQL = sSQL & " UNION "
            sSQL = sSQL & "SELECT MAX(FORM_ID) FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=-1 GROUP BY FILE_NAME)"
            Try
                result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
            Catch ex As Exception
                If isCriticalError() Then
                    Throw ex
                End If
            End Try
            '-------------------------------------------------------------
            sSQL = " DELETE FROM WCP_FORMS WHERE PRIMARY_FORM_FLAG=0 AND FILE_NAME IN  "
            sSQL = sSQL & " (SELECT FILE_NAME FROM WCP_FORMS GROUP BY FILE_NAME HAVING COUNT(FILE_NAME)> 1)  "
            Try
                result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
            Catch ex As Exception
                If isCriticalError() Then
                    Throw ex
                End If
            End Try
            '****************************************

            Select Case m_dbType
                'npradeepshar 09/28/2010 --MITS 22243Commented as we no longer support these datbsses
                ' Case DBMS_IS_ACCESS
                'jtodd22 03/18/2008 --MITS 11819
                ' On Error Resume Next
                '  sSQL = "DROP INDEX RM_WCP_FORMS ON WCP_FORMS"
                ' result = DB_SQLExecute(dbLookup, sSQL)
                '  If Err.Number > 0 Then Err.Clear
                ' On Error GoTo hError
                ' sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME TEXT(64) NOT NULL"
                ' result = DB_SQLExecute(dbLookup, sSQL)
                ' sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
                ' result = DB_SQLExecute(dbLookup, sSQL)

                'Case DBMS_IS_DB2
                'jtodd22 03/18/2008 --MITS 11819
                'On Error Resume Next
                'sSQL = "DROP INDEX RM_WCP_FORMS"
                'result = DB_SQLExecute(dbLookup, sSQL)
                'If Err.Number > 0 Then Err.Clear
                'On Error GoTo hError
                'sSQL = "ALTER TABLE WCP_FORMS ADD COLUMN FILE_NAME VARCHAR(64) NOT NULL"
                'result = DB_SQLExecute(dbLookup, sSQL)
                'sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
                'result = DB_SQLExecute(dbLookup, sSQL)

                'Case DBMS_IS_INFORMIX
                'jtodd22 03/18/2008 --MITS 11819
                'On Error Resume Next
                'sSQL = "DROP INDEX RM_WCP_FORMS"
                'result = DB_SQLExecute(dbLookup, sSQL)
                'If Err.Number > 0 Then Err.Clear
                'On Error GoTo hError
                'sSQL = "ALTER TABLE WCP_FORMS MODIFY (FILE_NAME VARCHAR(64) NOT NULL)"
                'result = DB_SQLExecute(dbLookup, sSQL)
                'sSQL = "CREATE UNIQUE INDEX RM_WCP_FORMS ON WCP_FORMS (FILE_NAME)"
                'result = DB_SQLExecute(dbLookup, sSQL)

                Case eDatabaseType.DBMS_IS_ORACLE
                    'jtodd22 03/18/2008 --MITS 11819_
                    'npradeepshar 09/28/2010 --MITS 22243 Drop two indexes and created the new one only
                    sSQL = "DROP INDEX RM_WCP_FORMS"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch
                        ' The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS DROP CONSTRAINT IDX_WF_FILE_NAME"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch
                        ' The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS MODIFY (FILE_NAME VARCHAR2(64) NOT NULL)"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch ex As Exception
                        If isCriticalError() Then
                            Throw ex
                        End If
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS ADD CONSTRAINT IDX_WF_FILE_NAME UNIQUE (FILE_NAME)"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch ex As Exception
                        If isCriticalError() Then
                            Throw ex
                        End If
                    End Try

                Case Else
                    'npradeepshar 09/28/2010 --MITS 22243 Dropped two indexes and created the new one only
                    sSQL = "DROP INDEX WCP_FORMS.RM_WCP_FORMS"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch
                        ' The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS DROP CONSTRAINT IDX_WF_FILE_NAME"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch
                        ' The exception is absorbed, so the VB.NET functionality is same as the original vb6 code
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS ALTER COLUMN FILE_NAME VARCHAR(64) NOT NULL"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch ex As Exception
                        If isCriticalError() Then
                            Throw ex
                        End If
                    End Try
                    sSQL = "ALTER TABLE WCP_FORMS ADD CONSTRAINT IDX_WF_FILE_NAME UNIQUE (FILE_NAME)"
                    Try
                        result = DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    Catch ex As Exception
                        If isCriticalError() Then
                            Throw ex
                        End If
                    End Try

            End Select
            lDataBaseUpgrade_A = 1

        Catch ex As Exception

            lDataBaseUpgrade_A = 0
            'iGeneralError
            LogWriter.LogError("UpgradeUtil.lDataBaseUpgrade_A", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try

    End Function
    Function isCriticalError() As Boolean
        Dim bRetVal As Boolean = True

        Dim sError As String
        sError = Err.Description
        Select Case m_dbType
            Case eDatabaseType.DBMS_IS_ACCESS
            Case eDatabaseType.DBMS_IS_DB2
            Case eDatabaseType.DBMS_IS_INFORMIX
            Case eDatabaseType.DBMS_IS_ORACLE
                If InStr(1, sError, "[ODBC Error=S1000] [Native Error=955] [Oracle][ODBC][Ora]ORA-00955:", CompareMethod.Binary) = 1 Then
                    'name is already used by an existing object duplcate value
                    'jtodd22 01/25/2008--let it crash
                End If
                If InStr(1, sError, "[ODBC Error=S1000] [Native Error=1430] [Oracle][ODBC][Ora]ORA-01430:", CompareMethod.Binary) = 1 Then
                    'column being added already exists in table
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=S1000] [Native Error=1442] [Oracle][ODBC][Ora]ORA-01442:", CompareMethod.Binary) = 1 Then
                    'column to be modified to NOT NULL is already NOT NULL
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=42S11] [Native Error=1408] [Oracle][ODBC][Ora]ORA-01408:", CompareMethod.Binary) = 1 Then
                    'such column list already indexed
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=42S12] [Native Error=1418] [Oracle][ODBC][Ora]ORA-01418:", CompareMethod.Binary) = 1 Then
                    'specified index does not exist
                    Return False
                End If

            Case Else
                If InStr(1, sError, "[ODBC Error=S0011] [Native Error=1913]", CompareMethod.Binary) = 1 Then
                    'There is already an index on table
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=S0012] [Native Error=3703]", CompareMethod.Binary) = 1 Then
                    'There is already an index on table
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=S0021] [Native Error=2705]", CompareMethod.Binary) = 1 Then
                    'Column names in each table must be unique.
                    Return False
                End If
                If InStr(1, sError, "[ODBC Error=23000] [Native Error=1505] [Microsoft][ODBC SQL Server Driver][SQL Server]", CompareMethod.Binary) = 1 Then
                    'CREATE UNIQUE INDEX terminated because a duplicate key was found for index ID 13. Most significant primary key is
                    'jtodd22 01/25/2008--let it crash
                End If

        End Select
        Return bRetVal
    End Function
End Class

