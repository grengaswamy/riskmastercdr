﻿
Option Strict On
Option Explicit On

Imports System.Configuration
Imports Riskmaster.ExceptionTypes
Imports Riskmaster.Security
Imports Riskmaster.Db
Imports EDI.CommonUtils

Public Class DbUpgrade

    'Public objLogin As RMLoginLib.RMLogin
    'Global objUser As RMUser   
    Friend objLogin As Riskmaster.Security.Login
    Public objUser As Riskmaster.Security.UserLogin

    Private m_connectionString As String

    ' db manufacturer type
    Public DbType As eDatabaseType

    Public Sub New(connectionString As String)
        m_connectionString = connectionString
        DbType = DbFactory.GetDatabaseType(m_connectionString)
    End Sub

    Function lGetNextUID(ByVal p_sTableNameSystem As String) As Integer
        Dim iNextId As Integer
        Try
            Using dbConnection As DbConnection = DbFactory.GetDbConnection(m_connectionString)
                dbConnection.Open()
                Using dbTrans As DbTransaction = dbConnection.BeginTransaction()
                    'Get current Id
                    Dim iCurrentId As Integer
                    Using objReader As DbReader = dbConnection.ExecuteReader("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & p_sTableNameSystem & "'", dbTrans)
                        objReader.Read()
                        iCurrentId = objReader.GetInt32(0)
                    End Using
                    iNextId = iCurrentId + 1

                    'Save new Id
                    Dim sSQL As String = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & iNextId
                    sSQL = sSQL & " WHERE SYSTEM_TABLE_NAME = '" & p_sTableNameSystem & "'"
                    sSQL = sSQL & " AND NEXT_UNIQUE_ID = " & iCurrentId
                    dbConnection.ExecuteNonQuery(sSQL, dbTrans)
                    dbTrans.Commit()
                End Using
            End Using
        Catch Ex As Exception
            LogWriter.LogError("UpgradeUtil.lGetNextUID", 0, 0, Ex.Source.ToString(), Ex.Message & Environment.NewLine)
            Throw
        End Try

        Return iNextId
    End Function


    Function sTrimNull(ByRef sArg As String) As String
        Dim index As Integer
        index = InStr(1, sArg, Chr(0))
        If index > 0 Then
            sTrimNull = Left(sArg, index - 1)
        Else
            sTrimNull = sArg
        End If
    End Function

    '***************************************************************************************
    ' Procedure : StorageClause
    ' Date      : 9//99
    ' Author    : Dave McAninch (dcm)
    ' Purpose   : This routine creates a storage clause (sSQL) for a Create Table statement
    '             based on a similar Table (sTableName).
    '***************************************************************************************
    Public Sub StorageClause(ByRef sSQL As String, ByRef sTableName As String)
        Dim sTableSpace As String
        Dim lInitExt As Integer
        Dim lNextExt As Integer

        Select Case DbType
            Case eDatabaseType.DBMS_IS_ORACLE
                Dim localSql As String = "SELECT TABLESPACE_NAME, INITIAL_EXTENT, NEXT_EXTENT FROM USER_TABLES WHERE TABLE_NAME = '" & sTableName & "'"
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, localSql)
                    If objReader.Read() Then
                        sTableSpace = objReader.GetString("TABLESPACE_NAME")
                        lInitExt = objReader.GetInt32("INITIAL_EXTENT")
                        lNextExt = objReader.GetInt32("NEXT_EXTENT")
                        sSQL = " PCTFREE 10 PCTUSED 40 TABLESPACE " & sTableSpace
                        sSQL = sSQL & " STORAGE (INITIAL " & lInitExt & " NEXT " & lNextExt
                        sSQL = sSQL & " MINEXTENTS 1 MAXEXTENTS 121 PCTINCREASE 1)"
                    End If
                End Using
        End Select
    End Sub

    '***************************************************************************************
    ' Procedure : sAddField
    ' Purpose   : Construct field descriptors
    '***************************************************************************************
    Function sAddField(ByRef sTableName As String, ByRef objReader As DbReader) As String
        Dim sSQL As String
        Dim lfieldid As Integer
        Dim sFieldName As String
        Dim nFieldType As Short
        Dim nFieldSize As Short
        Dim objWriter As DbWriter
        Dim sRetValue As String = String.Empty

        Try

            Using objReader1 As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT * FROM SUPP_DICTIONARY WHERE 0=1")
                objReader1.Read()
                objWriter = DbFactory.GetDbWriter(objReader1, False)
                objWriter.Where.Clear() ' Clear Where: 0 = 1
            End Using
            ' add database entry for table
            lfieldid = lGetNextUID("SUPP_DICTIONARY")

            ' automatically assign new field to end of supp. list
            objWriter.Fields("FIELD_ID").Value = lfieldid
            objWriter.Fields("SEQ_NUM").Value = lfieldid

            sFieldName = objReader.GetString("SYS_FIELD_NAME")
            nFieldType = objReader.GetInt16("FIELD_TYPE")
            nFieldSize = objReader.GetInt16("FIELD_SIZE")

            ' plug in data
            objWriter.Fields("SUPP_TABLE_NAME").Value = sTableName
            objWriter.Fields("USER_PROMPT").Value = objReader.GetString("USER_PROMPT")
            objWriter.Fields("SYS_FIELD_NAME").Value = sFieldName
            objWriter.Fields("FIELD_TYPE").Value = nFieldType
            objWriter.Fields("FIELD_SIZE").Value = nFieldSize
            objWriter.Fields("REQUIRED_FLAG").Value = objReader.GetInt16("REQUIRED_FLAG")

            objWriter.Fields("DELETE_FLAG").Value = 0
            objWriter.Fields("IS_PATTERNED").Value = objReader.GetInt16("IS_PATTERNED")
            objWriter.Fields("PATTERN").Value = objReader.GetString("PATTERN")
            objWriter.Fields("LOOKUP_FLAG").Value = objReader.GetInt16("LOOKUP_FLAG")

            objWriter.Fields("CODE_FILE_ID").Value = objReader.GetInt32("CODE_FILE_ID")
            objWriter.Fields("GRP_ASSOC_FLAG").Value = objReader.GetInt16("GRP_ASSOC_FLAG")
            objWriter.Fields("ASSOC_FIELD").Value = objReader.GetString("ASSOC_FIELD")
            objWriter.Fields("HELP_CONTEXT_ID").Value = objReader.GetInt32("HELP_CONTEXT_ID")

            objWriter.Execute()

            'change supp_assoc old field_id (for this state) to new field_id
            If sFieldName <> "CLAIM_ID" Then
                sSQL = "UPDATE SUPP_ASSOC SET FIELD_ID = " & lfieldid
                sSQL = sSQL & " WHERE FIELD_ID = " & objReader.GetString("FIELD_ID")
                sSQL = sSQL & " AND ASSOC_CODE_ID = " & objReader.GetString("STATE_ROW_ID")
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
            End If

            sSQL = sFieldName.Trim().ToUpper() & " " & sTypeMap(nFieldType, nFieldSize) ' includes NULL 6.0 dcm
            sRetValue = sSQL
        Catch ex As Exception
            LogWriter.LogError("DbUpgrade.sAddField", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)
            ' todo message - throw
            Throw New RMAppException("Index Creation Error: An error occurred creating the field. The database server could " & "not create the field. Possible causes may be that the field already exists, the field name is already taken, the field name includes illegal characters or whitespace, or the table of the field is in use by another user. The database server error message will follow (click OK).")
            'MsgBox("An error occurred creating the field. The database server could " & "not create the field. Possible causes may be that the field already " & "exists, the field name is already taken, the field name includes " & "illegal characters or whitespace, or the table of the field is in " & "use by another user. The database server error message will follow " & "(click OK).", MsgBoxStyle.Exclamation, "Index Creation Error")
        End Try
        Return sRetValue
    End Function

    '***************************************************************************************
    ' Procedure : sTypeMap
    ' Purpose   : Translates RM supplemental type to DB type string
    ' added NULL in version 6.0 dcm
    '***************************************************************************************
    Function sTypeMap(ByVal nFieldType As Integer, ByVal nSize As Integer) As String
        Dim sType As String = String.Empty

        ' Map type
        Select Case nFieldType
            Case 0, 3, 4, 10, 12, 13 ' string, date, time, claim lookup, event lookup, vehicle lookup,
                Select Case DbType
                    Case eDatabaseType.DBMS_IS_SQLSRVR, eDatabaseType.DBMS_IS_SYBASE, eDatabaseType.DBMS_IS_INFORMIX, eDatabaseType.DBMS_IS_DB2
                        sType = "VARCHAR"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sType = "VARCHAR2"
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sType = "TEXT"
                End Select

                ' Add size
                sType = sType & "(" & nSize & ")"

            Case 6, 7, 8, 9, 14, 15, 16 ' code, primary key, entity, state, multi-code, multi-entity, multi-state
                Select Case DbType
                    Case eDatabaseType.DBMS_IS_INFORMIX, eDatabaseType.DBMS_IS_DB2
                        sType = "INTEGER"
                    Case eDatabaseType.DBMS_IS_SQLSRVR, eDatabaseType.DBMS_IS_SYBASE
                        sType = "INT"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sType = "NUMBER(10,0)"
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sType = "LONG"
                End Select

            Case 1, 2 ' number, currency (float)
                Select Case DbType
                    Case eDatabaseType.DBMS_IS_SQLSRVR, eDatabaseType.DBMS_IS_SYBASE, eDatabaseType.DBMS_IS_INFORMIX, eDatabaseType.DBMS_IS_DB2
                        sType = "FLOAT"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sType = "NUMBER"
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sType = "DOUBLE"
                End Select

            Case 5, 11 ' free text (and old multi-valued)
                Select Case DbType
                    Case eDatabaseType.DBMS_IS_SQLSRVR, eDatabaseType.DBMS_IS_SYBASE, eDatabaseType.DBMS_IS_INFORMIX
                        sType = "TEXT"
                    Case eDatabaseType.DBMS_IS_DB2
                        sType = "CLOB(10M)"
                    Case eDatabaseType.DBMS_IS_ORACLE
                        sType = "VARCHAR2(2000)"
                    Case eDatabaseType.DBMS_IS_ACCESS
                        sType = "MEMO"
                End Select
        End Select

        ' INFORMIX allows NULL by default and doesn't allow "NULL" in statement
        ' added NULL to this routine in 6.0. dcm 1/3/03
        If DbType = eDatabaseType.DBMS_IS_SQLSRVR Or DbType = eDatabaseType.DBMS_IS_SYBASE Or DbType = eDatabaseType.DBMS_IS_ORACLE Then
            sType = sType & " NULL"
        End If

        Return sType
    End Function

    '***************************************************************************************
    ' Procedure : subUpdateSupp
    ' Date      : 1/3/2003
    ' Author    : This routine is from a program Jim Partin (jap) and Jainagaraj Raju Naragaran (rjn).
    ' Purpose   : Run the UpdateSupp/SuppUpdate on all databases
    '***************************************************************************************
    Sub subUpdateSupp()
        Dim newFieldID As Integer
        Dim sSQL As String
        Dim recdsUpdated As Integer = 1
        Dim lTmp As Integer
        Dim lTmp2 As Integer
        Dim bHasGreater As Boolean = False

        Try
            'first check if there are supp fields with ID > 20000
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM SUPP_DICTIONARY WHERE FIELD_ID >= 20000")
                If objReader.Read() Then
                    If Not objReader.IsDBNull(0) Then
                        bHasGreater = True
                    End If
                End If
            End Using
            If bHasGreater Then
                'get the max id before 20000
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(FIELD_ID) FROM SUPP_DICTIONARY WHERE FIELD_ID < 20000")
                    If objReader.Read() Then
                        newFieldID = objReader.GetInt32(0)
                    End If
                End Using
                newFieldID = newFieldID + 1

                'start updating supp fields
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE FIELD_ID >= 20000 ORDER BY FIELD_ID")
                    While objReader.Read()
                        Dim oldFieldId As Integer = objReader.GetInt32(0)
                        sSQL = "UPDATE SUPP_DICTIONARY SET FIELD_ID = " & newFieldID & " WHERE FIELD_ID = " & oldFieldId
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                        sSQL = "UPDATE SUPP_ASSOC SET FIELD_ID = " & newFieldID & " WHERE FIELD_ID = " & oldFieldId
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                        sSQL = "UPDATE MERGE_FORM_DEF SET FIELD_ID = " & newFieldID & " WHERE FIELD_ID = " & oldFieldId
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                        newFieldID = newFieldID + 1

                        recdsUpdated = recdsUpdated + 1
                    End While
                End Using

                'update glossary to set next unique id
                sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & newFieldID & " WHERE SYSTEM_TABLE_NAME = 'SUPP_DICTIONARY'"
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & newFieldID & " WHERE SYSTEM_TABLE_NAME = 'SUPP_ASSOC'"
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

            End If

            ' delete extra records.
            DbFactory.ExecuteNonQuery(m_connectionString, "DELETE FROM SUPP_ASSOC WHERE FIELD_ID NOT IN (SELECT FIELD_ID FROM SUPP_DICTIONARY)")

            'MJH 11/9/99
            DbFactory.ExecuteNonQuery(m_connectionString, "DELETE FROM SUPP_ASSOC WHERE FIELD_ID IN (SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME='WC_STATE_SUPP' AND SYS_FIELD_NAME='CLAIM_ID')")

            Dim hasMaxHelpContext As Boolean = False
            ' Now, check help context ID's
            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT MAX(HELP_CONTEXT_ID) FROM SUPP_DICTIONARY WHERE HELP_CONTEXT_ID < 0")
                If objReader.Read() Then
                    hasMaxHelpContext = True
                    lTmp = objReader.GetInt32(0)
                End If
            End Using

            If hasMaxHelpContext Then
                lTmp = lTmp + 1

                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'HELP_DEF'")
                    If objReader.Read() Then
                        lTmp2 = objReader.GetInt32(0)
                    End If
                End Using

                ' ... now, fix any supp_dictionary items with duplicate help ids
                If DbType = eDatabaseType.DBMS_IS_ORACLE Then
                    sSQL = "create or replace function getSuppCount (lhelpid IN NUMBER) RETURN NUMBER IS suppCount NUMBER;"
                    sSQL = sSQL & vbCrLf & "BEGIN"
                    sSQL = sSQL & vbCrLf & "SELECT COUNT(*) INTO suppCount FROM SUPP_DICTIONARY where HELP_CONTEXT_ID = lhelpid and DELETE_FLAG = 0;"

                    sSQL = sSQL & vbCrLf & "return(suppCount);"
                    sSQL = sSQL & vbCrLf & "EXCEPTION"
                    sSQL = sSQL & vbCrLf & "WHEN OTHERS"
                    sSQL = sSQL & vbCrLf & "THEN return(0);"
                    sSQL = sSQL & vbCrLf & "END;"
                    DbFactory.ExecuteNonQuery(m_connectionString, sSQL)
                    sSQL = "select FIELD_ID,HELP_CONTEXT_ID,REQUIRED_FLAG from SUPP_DICTIONARY SD1 where getSuppCount(SD1.HELP_CONTEXT_ID) > 1 and DELETE_FLAG = 0 and HELP_CONTEXT_ID <> 0 order by HELP_CONTEXT_ID"
                Else
                    sSQL = "select FIELD_ID,HELP_CONTEXT_ID,REQUIRED_FLAG from SUPP_DICTIONARY SD1 where (SELECT COUNT(*) FROM SUPP_DICTIONARY SD2 where SD2.HELP_CONTEXT_ID = SD1.HELP_CONTEXT_ID and SD2.DELETE_FLAG = 0 ) > 1 and DELETE_FLAG = 0 and HELP_CONTEXT_ID <> 0 order by HELP_CONTEXT_ID"
                End If
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                    While objReader.Read()
                        sSQL = "UPDATE SUPP_DICTIONARY SET HELP_CONTEXT_ID = " & lTmp & " WHERE FIELD_ID = " & objReader.GetInt32("FIELD_ID")
                        DbFactory.ExecuteNonQuery(m_connectionString, sSQL)

                        ' ... make sure required field entry gets moved
                        'sSQL = "INSERT INTO REQ_DEF(LABEL_ID,REQUIRED_FLAG) VALUES (" & lTmp & "," & IIf(iAnyVarToInt(vDB_GetData(rs, "REQUIRED_FLAG")), -1, 0) & ")"
                        'result = DB_SQLExecute(dbLookup, sSQL)

                        lTmp = lTmp + 1

                    End While
                End Using

                DbFactory.ExecuteNonQuery(m_connectionString, "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & lTmp + 1 & " WHERE SYSTEM_TABLE_NAME = 'HELP_DEF'")
            End If

            Exit Sub
        Catch ex As Exception
            LogWriter.LogError("DbUpgrade.supUpdateSupp", 0, 0, ex.Source.ToString(), ex.Message & Environment.NewLine)

        End Try
    End Sub

    '***************************************************************************************
    ' Procedure : subUpdateGlossary_NEXT_UNIQUE_ID
    ' Purpose   :
    ' 08/29/2001 jlt fixed error handling caused by missing JURIS_FORMS table
    '               (skipped upgrade from 3.6.20 to 4.0)
    '***************************************************************************************
    Public Sub subUpdateGlossary_NEXT_UNIQUE_ID(ByVal sTableName As String)
        Dim lMaxValue As Integer
        Dim sSQL As String

        Try
            sSQL = ""
            '    Select Case sTableName
            '        Case "STATES"
            '            sSQL = "SELECT MAX(STATE_ROW_ID) AS MAX_FORM_ID FROM " & sTableName
            '        Case Else
            '            sSQL = "SELECT MAX(FORM_ID) AS MAX_FORM_ID FROM " & sTableName
            '    End Select
            Select Case sTableName
                Case "STATES"
                    sSQL = "SELECT MAX(STATE_ROW_ID) FROM " & sTableName
                Case Else
                    sSQL = "SELECT MAX(FORM_ID) FROM " & sTableName
            End Select
            If DbType <> eDatabaseType.DBMS_IS_ACCESS Then sSQL = sSQL.Replace(" AS ", " ") 'Remove alias As(notneeded except for access)

            Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                If objReader.Read() Then
                    lMaxValue = objReader.GetInt32(0) + 1
                Else
                    lMaxValue = 0
                End If
            End Using
            Dim objWriter As DbWriter = Nothing
            If lMaxValue > 0 Then
                sSQL = "SELECT * FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"
                Using objReader As DbReader = DbFactory.GetDbReader(m_connectionString, sSQL)
                    If objReader.Read() Then
                        objWriter = DbFactory.GetDbWriter(objReader, True)
                        objWriter.Fields("NEXT_UNIQUE_ID").Value = lMaxValue
                        objWriter.Execute()
                    Else
                        'should never happen
                    End If
                End Using
            End If
        Catch ex As Exception
            'iGeneralError
            LogWriter.LogError("DbUpgrade.subUpdateGlossary_NEXT_UNIQUE_ID", 0, 0, " table: " & sTableName & " ", ex.Message & Environment.NewLine)
        End Try

    End Sub

End Class
