﻿
Option Strict On
Option Explicit On

Public Class ProgressEventArgs
    Inherits System.EventArgs

    Private m_progressStatus As Integer
    Public Property ProgressStatus() As Integer
        Set(value As Integer)
            m_progressStatus = value
        End Set
        Get
            Return Me.m_progressStatus
        End Get
    End Property

    Private m_infoLine As String
    Public Property InfoLine() As String
        Set(value As String)
            m_infoLine = value
        End Set
        Get
            Return Me.m_infoLine
        End Get
    End Property

    Public Sub New(status As Integer, line As String)
        m_progressStatus = status
        m_infoLine = line
    End Sub
End Class
