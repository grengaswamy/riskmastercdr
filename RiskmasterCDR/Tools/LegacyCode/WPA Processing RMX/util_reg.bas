Attribute VB_Name = "UTIL_REG"
Option Explicit


' Declare all Registration Database API's

'---------------------------------------------------------
' Registration database constants
'----------------------------------------------------------
Global Const REG_ROOT_KEY = "SOFTWARE\DTG"
Global Const REG_SZ = 1
Global Const KEY_LENGTH = 256
Global Const REG_SUCCESS = 0&
Global Const REG_BADKEY = 2&
Global Const HKEY_LOCAL_MACHINE = &H80000002

'Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
'Declare Function RegSetValue Lib "advapi32.dll" Alias "RegSetValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
'Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
'Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Declare Function RegQueryValue Lib "advapi32.dll" Alias "RegQueryValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal lpValue As String, lpcbValue As Long) As Long

'*****************************************************************
'*
'*  bRegDBGetValue (sKey As String, sValue As String) As Integer
'*
'*  <Enter Function Description Here>
'*
'*  Inputs: <Enter Input Values Here>
'*
'*  Returns: <Enter Return Values Here>
'*
'*  Date Written:             By:
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Function bRegDBGetValue(sKey As String, sValue As String) As Integer

On Error GoTo ERROR_TRAP_bRegDBGetValue

    Dim sItem As String * KEY_LENGTH
    Dim lrtn As Long
    bRegDBGetValue = False
    Dim sRoot As String
    Dim sTmp As String

    sRoot = REG_ROOT_KEY & "\" & Trim(App.Title)
    If sKey <> "" And Left(sKey, 1) <> "\" Then
        sTmp = sRoot & "\" & sKey
    Else
        sTmp = sRoot & sKey
    End If
    lrtn = RegQueryValue(HKEY_LOCAL_MACHINE, sTmp, sItem, KEY_LENGTH)

    If lrtn = REG_SUCCESS Then
        bRegDBGetValue = True
        sValue = sTrimNull(sItem)
    Else
        bRegDBGetValue = False
    End If

    Exit Function

ERROR_TRAP_bRegDBGetValue:
    If iGeneralErrorExt(Err, Error$, "UTIL_REG.BAS\bRegDBGetValue") = IDRETRY Then Resume Else bRegDBGetValue = False
    Exit Function

End Function

