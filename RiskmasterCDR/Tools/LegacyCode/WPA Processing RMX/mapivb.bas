Attribute VB_Name = "MAPIVB"

Option Explicit

'****************************************************************************'
'                                                                            '
' Visual Basic declaration for the MAPI functions.                           '
'                                                                            '                                                                           '
'****************************************************************************'


'***************************************************
'   MAPI Message holds information about a message
'***************************************************

Public Type MAPIMessage
    Reserved As Long
    Subject As String
    NoteText As String
    MessageType As String
    DateReceived As String
    ConversationID As String
    flags As Long
    RecipCount As Long
    FileCount As Long
End Type

'***************************************************
'   DTGMAPISUM Message holds the summary  information about a message
'  ( We greatly simplyfied the summary structure,because this is only a walkaround wchen 3/18/97 )
'***************************************************
'Type DTGMAPISUM
'  MsgReference As String
'  Subject As String
'End Type
Type MsgReference
  length As Integer
  CharArray(0 To 24) As Integer
End Type

'************************************************
'   MAPIRecip holds information about a message
'   originator or recipient
'************************************************
Public Type MapiRecip
    Reserved As Long
    RecipClass As Long
    Name As String
    Address As String
    EIDSize As Long
    EntryID As String
End Type

'******************************************************
'   MapiFile holds information about file attachments
'******************************************************
Public Type MapiFile
    Reserved As Long
    flags As Long
    Position As Long
    PathName As String
    FileName As String
    FileType As String
End Type

'***************************
'   FUNCTION Declarations
'***************************
Declare Function MAPILogon Lib "MAPI32.DLL" (ByVal UIParam&, ByVal User$, ByVal Password$, ByVal flags&, ByVal Reserved&, Session&) As Long
Declare Function MAPILogoff Lib "MAPI32.DLL" (ByVal Session&, ByVal UIParam&, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIReadMail Lib "MAPI32.DLL" (lMsg&, nRecipients&, nFiles&, ByVal Session&, ByVal UIParam&, MessageID$, ByVal Flag&, ByVal Reserved&) As Long
Declare Function BMAPIGetReadMail Lib "MAPI32.DLL" (ByVal lMsg&, Message As MAPIMessage, Recip() As MapiRecip, File() As MapiFile, Originator As MapiRecip) As Long
Declare Function MAPIFindNext Lib "MAPI32.DLL" Alias "BMAPIFindNext" (ByVal Session&, ByVal UIParam&, MsgType$, SeedMsgID$, ByVal Flag&, ByVal Reserved&, MsgID$) As Long
Declare Function MAPIDeleteMail Lib "MAPI32.DLL" (ByVal Session&, ByVal UIParam&, ByVal MsgID$, ByVal flags&, ByVal Reserved&) As Long
Declare Function MAPISendMail Lib "MAPI32.DLL" Alias "BMAPISendMail" (ByVal Session&, ByVal UIParam&, Message As MAPIMessage, Recipient() As MapiRecip, File() As MapiFile, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIAddress Lib "MAPI32.DLL" (lInfo&, ByVal Session&, ByVal UIParam&, Caption$, ByVal nEditFields&, label$, nRecipients&, Recip() As MapiRecip, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIGetAddress Lib "MAPI32.DLL" (ByVal lInfo&, ByVal nRecipients&, Recipients() As MapiRecip) As Long
Declare Function MAPIResolveName Lib "MAPI32.DLL" Alias "BMAPIResolveName" (ByVal Session&, ByVal UIParam&, ByVal UserName$, ByVal flags&, ByVal Reserved&, Recipient As MapiRecip) As Long

'**************************
'   CONSTANT Declarations
'**************************
'CMC Constant
 Global Const LIST_ALL = 0
 Global Const LIST_UNREAD = 1
 Global Const LIST_COUNT = 4

'*********88

'MAPI Constant

Global Const SUCCESS_SUCCESS = 0
'Global Const MAPI_USER_ABORT = 1
'Global Const MAPI_E_FAILURE = 2
'Global Const MAPI_E_LOGIN_FAILURE = 3
'Global Const MAPI_E_DISK_FULL = 4
'Global Const MAPI_E_INSUFFICIENT_MEMORY = 5
'Global Const MAPI_E_BLK_TOO_SMALL = 6
'Global Const MAPI_E_TOO_MANY_SESSIONS = 8
'Global Const MAPI_E_TOO_MANY_FILES = 9
'Global Const MAPI_E_TOO_MANY_RECIPIENTS = 10
'Global Const MAPI_E_ATTACHMENT_NOT_FOUND = 11
'Global Const MAPI_E_ATTACHMENT_OPEN_FAILURE = 12
'Global Const MAPI_E_ATTACHMENT_WRITE_FAILURE = 13
'Global Const MAPI_E_UNKNOWN_RECIPIENT = 14
'Global Const MAPI_E_BAD_RECIPTYPE = 15
Global Const MAPI_E_NO_MESSAGES = 16
'Global Const MAPI_E_INVALID_MESSAGE = 17
'Global Const MAPI_E_TEXT_TOO_LARGE = 18
'Global Const MAPI_E_INVALID_SESSION = 19
'Global Const MAPI_E_TYPE_NOT_SUPPORTED = 20
'Global Const MAPI_E_AMBIGUOUS_RECIPIENT = 21

'Global Const MAPI_ORIG = 0
Global Const MAPI_TO = 1
Global Const MAPI_CC = 2
'Global Const MAPI_BCC = 3


'***********************
'   FLAG Declarations
'***********************

Global Const MAPI_LOGON_UI = &H1
Global Const MAPI_NEW_SESSION = &H2
Global Const MAPI_DIALOG = &H8
Global Const MAPI_UNREAD_ONLY = &H20
Global Const MAPI_ENVELOPE_ONLY = &H40
Global Const MAPI_PEEK = &H80
Global Const MAPI_GUARANTEE_FIFO = &H100
Global Const MAPI_BODY_AS_FILE = &H200
Global Const MAPI_AB_NOMODIFY = &H400
Global Const MAPI_SUPPRESS_ATTACH = &H800
Global Const MAPI_FORCE_DOWNLOAD = &H1000

'Global Const MAPI_OLE = &H1
'Global Const MAPI_OLE_STATIC = &H2


'*****************************************
' Global variables used by MapiDemo only
'*****************************************

Dim MsgID$
'Dim Recips() As MapiRecip, Files() As MapiFile


'*********************************************
' Define global message, recipient and file
' structures for use within the view form
'*********************************************

Dim M As MAPIMessage
Dim Mo As MapiRecip
Dim Mr() As MapiRecip
Dim Mf() As MapiFile

Dim ref() As MsgReference    'CMC reference

Dim toR()  As String
Dim ccR()  As String

Global Const RMWIN_SUBJECT_PREFIX = "[RM/win]"
Global Const RMWIN_REF_PREFIX = "RMREF"
'Global Const RMS_EM_SIZE = 64                   ' Max size of users e-mail address

Sub subMailLogon()


On Error GoTo ET_subMailLogon

    Dim lError As Long


    Rem load main MDI mail form
    Load frmMail

    Rem bring MAPI engine online (login if necessary)
    lError = lWPAInitMAPI()
    If lError <> 0 Then
        Call subDisplayMsg(lError, "")
       Exit Sub
    End If

    Rem load list with new mail
    Call subMailRefreshList(False)
    'Call subCMCMailRefreshList(2) 'New messages as default
    ' frmMDIParent.labMailStatus.Caption = frmMail.SubjectList.ListCount & " Messages"   ' update status line

    Rem show the main mail window
    frmMail.Show

  Exit Sub

ET_subMailLogon:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailLogon")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailRead()


On Error GoTo ET_subMailRead

   Rem see if any message is selected
   If (frmMail.SubjectList.ListCount = 0) Or (frmMail.SubjectList.ListIndex = -1) Then
      Exit Sub
   End If

   Rem load mdi mail view form
   Call subLoadViewForm

  Exit Sub

ET_subMailRead:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailRead")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailRefreshList(boolRMWINMessagesOnly As Integer)
On Error GoTo ET_subMailRefreshList

    Dim r() As MapiRecip
    Dim f() As MapiFile
    Dim rc As Long
    Dim sSubject As String
    Dim sTable As String
    Dim lRecID As Long
    Dim bIsReferenced As Integer
    Dim lRecID2, lfrmCode, lFrmParentCode, lSecurity As Long
    Dim sCaption As String

    Rem initialize listboxes
    frmMail.SubjectList.Clear
    frmMail.IdList.Clear

    MsgID$ = ""

    Rem ***********************************************
    Rem  Retrieve the user's mail and display the
    Rem  subject line from each message. Only the
    Rem  messages whose subjects begin with [RM/win]
    Rem  will be displayed. The message
    Rem  mail id is store in the IdList, and the
    Rem  Subject is stored in the SubjectList
    Rem ***********************************************
    Dim start As Long, finish As Long, i As Long
    start = Timer
    i = 0
    rc = MAPIFindNext(WPAMapiSession, 0&, "", "", 0&, 0&, MsgID$)
    Do While (rc& = SUCCESS_SUCCESS)
       If (rc& = SUCCESS_SUCCESS) Then
          rc& = MAPIReadMail(WPAMapiSession, MsgID$, 0&, 0&, M, Mo, r(), f())
          If (rc& = SUCCESS_SUCCESS) Then
             sSubject = M.Subject
             If boolRMWINMessagesOnly Then
               If Left$(Trim(sSubject), Len(RMWIN_SUBJECT_PREFIX)) = RMWIN_SUBJECT_PREFIX Then
                  frmMail.SubjectList.AddItem sSubject
                  frmMail.IdList.AddItem MsgID$
               End If
             Else
               frmMail.SubjectList.AddItem sSubject
               frmMail.IdList.AddItem MsgID$
             End If
          Else
             Call subDisplayMsg(90, "")
          End If
       ElseIf rc& = MAPI_E_NO_MESSAGES Then

       Else
               Call subDisplayMsg(96, "")
       End If
       i = i + 1
       rc& = MAPIFindNext(WPAMapiSession, 0&, "", MsgID$, 0&, 0&, MsgID$)
    Loop
    finish = Timer
    'MsgBox "For " & i & "time = " & (finish - start)
    Rem initially select first item in list
    If (frmMail.SubjectList.ListCount > 0) Then
           frmMail.SubjectList.ListIndex = 0
    End If

    Rem display # of messages
    'dbas 1/19/98  frmMDIParent.labMailStatus.Caption = frmMail.SubjectList.ListCount & " Messages"
  Exit Sub

ET_subMailRefreshList:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailRefreshList")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailSend()


On Error GoTo ET_subMailSend
    Dim M As MAPIMessage
    Dim sRecipStr As String
    Dim lRecID As Long
    Dim iTo As Integer
    Dim iPos As Integer
    Dim iCC As Integer
    Dim i As Integer
    Dim sSubject As String
    Dim sTable As String
    ReDim Mr(0 To 0) As MapiRecip
    ReDim Mf(0 To 0) As MapiFile
    Dim bNoSubject As Integer
    Dim code1, code2, code3, code5, code7 As String

    subHourGlass True

    Rem read all TO's (including EMAIL and FAX)
    iTo = 0

    If Trim(frmCompose.txtTo.Text) <> "" Then
       iPos = 1
       Do While (iPos <> 0)
           ReDim Preserve toR(0 To iTo)        ' allocate more space in array

           toR(iTo) = StringToken(iPos, (frmCompose.txtTo.Text), ";")
           iTo = iTo + 1
       Loop
    End If

    If Trim(frmCompose.txtFaxAddress.Text) <> "" Then
       iPos = 1
       Do While (iPos <> 0)
           ReDim Preserve toR(0 To iTo)        ' allocate more space in array

           toR(iTo) = StringToken(iPos, (frmCompose.txtFaxAddress.Text), ";")
           iTo = iTo + 1
       Loop
    End If

    Rem if no recipients, error out
    If (iTo = 0) Then
        Call subDisplayMsg(101, "")
        subHourGlass False
        Exit Sub
    End If

    Rem read all CC's (just EMAIL)
    iCC = 0
    If (Len(Trim(frmCompose.txtCC.Text)) <> 0) Then
        iPos% = 1
        Do While (iPos <> 0)
            ReDim Preserve ccR(0 To iCC)        ' allocate more space in array

            ccR(iCC) = StringToken(iPos, (frmCompose.txtCC.Text), ";")
            iCC = iCC + 1
        Loop
    End If

    Rem setup TO: recipient structures
    ReDim Mr(0 To iTo + iCC)

    If (iTo <> 0) Then
        For i = 0 To iTo - 1
            Mr(i).Name = toR(i)
            Mr(i).RecipClass = MAPI_TO
        Next i
    End If

    Rem setup CC: recipients
    If (iCC <> 0) Then
        For i = 0 To iCC - 1
            Mr(iTo + i).Name = ccR(i)
            Mr(iTo + i).RecipClass = MAPI_CC
        Next i
    End If

    Rem setup any attachments (currently unsupported)
    M.FileCount = 0&

    'If (AttachCount > 0) Then
    '    ReDim Mf(0 To AttachCount - 1)

    '    For i = 0 To AttachCount - 1
    '        Mf(i).Filename = ""
    '        Mf(i).PathName = ""
    '        Mf(i).Position = -1
    '        Mf(i).FileType = ""
    '    Next i

    '    M.FileCount = AttachCount

    'End If

    Rem format the subject (place any prefixes on it that are neeed)
    bNoSubject = False
    If (Len(frmCompose.txtSubject.Text) = 0) Then
       bNoSubject = True
    End If

    sSubject = RMWIN_SUBJECT_PREFIX
    'sTable = Trim(frmCompose.labSysTable)
    'lRecID = Val(frmCompose.labSysRecID)
       
    ' JP 11/18/97   NOT USED    code6 = Trim$(Str$(frmCompose.labFormSecurity))
    ' JP 11/18/97   NOT USED    code4 = Trim$(Str$(frmCompose.labFormCode))
    ' JP 11/18/97   NOT USED    code5 = Trim$(Str$(frmCompose.labParentFormCode))
    code2 = Trim$(Str$(frmCompose.labAttachRecordID))
    code3 = Trim$(Str$(frmCompose.labAttachSecondRecordID))
    code1 = Trim$(frmCompose.sAttachTableName)
    code7 = Trim$(frmCompose.sAttachCaption)

    'If (sTable <> "") And (lRecID <> 0) Then
       ' add record reference prefix
       'sSubject = sSubject & "[" & RMWIN_REF_PREFIX & ":" & sTable & ":" & Trim$(Str$(lRecID)) & "]"
    If (Val(frmCompose.labRefered) And (frmCompose.labAttachRecordID) <> 0) Then
        sSubject = sSubject & "[" & RMWIN_REF_PREFIX & ":" & code1 & ":" & code2 & ":" & code3 & ":0:0:0:" & code7 & "]" & Chr(9)
    End If
    'End If

    If Not bNoSubject Then
       sSubject = sSubject & " " & frmCompose.txtSubject.Text
    Else
       sSubject = sSubject & " " & frmCompose.labNoSubject
    End If

    Rem setup the MAPI mail message
    M.Reserved = 0&
    M.Subject = sSubject
    M.NoteText = frmCompose.txtBody.Text
    M.MessageType = ""
    M.DateReceived = ""
    M.flags = 0&
    M.RecipCount = iTo + iCC


    Rem send the message via MAPI
    sRecipStr = frmCompose.txtTo.Text & ";" & frmCompose.txtCC.Text
    If (MAPISendMail(WPAMapiSession, 0&, M, Mr(), Mf(), 0&, 0&) = SUCCESS_SUCCESS) Then
'        Rem if successful, log the event
'        If InStr(sRecipStr, "fax:") Then
'            nEventType = 19   ' fax event
'        Else
'            nEventType = 17   ' mail event
'        End If

'        sTableName = frmCompose.labSysTable
'        If frmCompose.labSysRecID <> "" Then
'           lRecID = Val(frmCompose.labSysRecID)
'        Else
'           lRecID = 0
'        End If

        'Call subLogMAPIEvent(nEventType, sRecipStr, sTableName, lRecID, (frmCompose.txtBody.Text))

        Rem unload the form
        Unload frmCompose
    Else
        Rem send failed, complain to user
        Call subDisplayMsg(100, "")
    End If


    Rem clean up
    Erase toR
    Erase ccR
    Erase Mr
    Erase Mf

    subHourGlass False
    Exit Sub

ET_subMailSend:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailSend")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMAPICheckNewMail()
'************************************************
'* Check for new mail(s) and inform user if any *
'* new mail is arrived.                         *
'* Author: Denis Basaric, 11/11/1997            *
'* Proc. based on subMailRead()                 *
'************************************************
On Error GoTo Err_subMAPICheckNewMail

   Dim r() As MapiRecip
   Dim f() As MapiFile
   Dim rc As Long
   Dim sSubject As String
   Dim lMessageCount As Long, lRMWINMessageCount As Long
   Dim lError As Long

   ' Let user know what we're doing
   subSetSystemStatus "Checking New Mail ..."
   ' Try to init. mapi
   lError = lWPAInitMAPI()
   If lError <> 0 Then
      Call subDisplayMsg(lError, "")
      Exit Sub
   End If
    
   MsgID$ = ""

   '************************************************
   '* This procedure just check is there's any new *
   '* mails and show that to user                  *
   '************************************************
    
   lMessageCount = 0
   
   ' Always find only unreaded messages
   rc = MAPIFindNext(WPAMapiSession, 0&, "", "", MAPI_UNREAD_ONLY, 0&, MsgID$)
   Do While (rc& = SUCCESS_SUCCESS)
      lMessageCount = lMessageCount + 1
      rc& = MAPIFindNext(WPAMapiSession, 0&, "", MsgID$, 0&, 0&, MsgID$)
   Loop

   If lMessageCount > 0 Then
      ' Inform user about new mails
      Beep
      MsgBox "You have " & lMessageCount & " new message(s).", vbInformation
      'dbas 1/19/98  frmMDIParent.labMailStatus.Caption = lMessageCount & " New Message(s)"
   End If

   Exit Sub
Err_subMAPICheckNewMail:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMAPICheckNewMail")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select

End Sub

Sub subRouteMailTo(frm As Form, n As Integer, sUser() As String)

On Error GoTo ET_subRouteMailTo
    Dim M As MAPIMessage
    Dim sRecipStr As String
    Dim lRecID As Long
    Dim iTo As Integer
    Dim iPos As Integer
    Dim iCC As Integer
    Dim i As Integer
    Dim sSubject As String
    Dim sTable As String
    ReDim Mr(0 To 0) As MapiRecip
    ReDim Mf(0 To 0) As MapiFile
    Dim bNoSubject As Integer
    Dim code1, code2, code3, code5, code7 As String

    subHourGlass True
    
    For i = 1 To n
        ReDim Preserve toR(1 To i)
        toR(i) = sUser(i)
    Next i
    

   
    ReDim Mr(0 To n)

    If (n <> 0) Then
        For i = 0 To n - 1
            Mr(i).Name = toR(i + 1)
            Mr(i).RecipClass = MAPI_TO
        Next i
    End If

    Rem setup any attachments (currently unsupported)
    M.FileCount = 0&

    Rem format the subject (place any prefixes on it that are neeed)
    bNoSubject = False
    If (Len(frm.Subject_EDIT.Text) = 0) Then
       bNoSubject = True
    End If

    sSubject = RMWIN_SUBJECT_PREFIX
       
    
    code1 = Trim$(frm.sAttachTableName)
    code2 = Trim$(Str$(frm.labAttachRecordID))
    code3 = Trim$(Str$(frm.labAttachSecondRecordID))
    ' JP 11/18/97   NOT USED    code4 = Trim$(Str$(frm.labFormCode))
    ' JP 11/18/97   NOT USED    code5 = Trim$(Str$(frm.labParentFormCode))
    ' JP 11/18/97   NOT USED    code6 = Trim$(Str$(frm.labFormSecurity))
    code7 = Trim$(frm.sAttachCaption)

    If (Val(frm.bRefered) And (frm.labAttachRecordID) <> 0) Then
        sSubject = sSubject & "[" & RMWIN_REF_PREFIX & ":" & code1 & ":" & code2 & ":" & code3 & ":0:0:0:" & code7 & "]" & Chr(9)
    End If
    

    If Not bNoSubject Then
       sSubject = sSubject & " " & frm.Subject_EDIT.Text
    Else
       sSubject = sSubject & " " & "<No Subject>"
    End If

    Rem setup the MAPI mail message
    M.Reserved = 0&
    M.Subject = sSubject
    M.NoteText = frm.Message_EDIT.Text
    M.MessageType = ""
    M.DateReceived = ""
    M.flags = 0&
    M.RecipCount = n
    
    Rem send the message via MAPI
    For i = 0 To n - 1
        If (MAPISendMail(WPAMapiSession, 0&, M, Mr(), Mf(), 0&, 0&) = SUCCESS_SUCCESS) Then
            Rem unload the form
        Else
            Rem send failed, complain to user
            Call subDisplayMsg(100, "")
        End If
    Next i

    Rem clean up
    Erase toR
    Erase ccR
    Erase Mr
    Erase Mf

    subHourGlass False
    Exit Sub

ET_subRouteMailTo:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subRouteMailTo")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select

End Sub

Function bMailGetAttachInfo(frmForm As Form, sTable As String, lRecID As Long, lRecID2 As Long, sCaption As String) As Integer
On Error GoTo ET_bMailGetAttachInfo

  Dim bSupported As Integer
  Dim lLOBCode As Long
  Dim sLOBShortCode As String
  Dim lEntTableID As Long
  Dim sSecondTableName As String
  Dim lATTableID As Long


  If Not IsNull(frmForm.TableName(0)) Then
    sTable = sAnyVarToString(frmForm.TableName(0))
  Else
    sTable = ""
  End If

  If (Not IsNull(frmForm.RecordID(0)) And (frmForm.RecordID(0) <> "")) Then
     lRecID = Val(frmForm.RecordID(0))
  Else
     lRecID = 0
  End If

  If Not IsNull(frmForm.Caption) Then
    sCaption = frmForm.Caption
  Else
    sCaption = ""
  End If

  ' JP 11/18/97  lSecurity = Val(frmForm.labSecurityBase)
  ' JP 11/18/97  lfrmCode = Val(frmForm.Tag)
  ' JP 11/18/97  lFrmParentCode = lGetParentFormCode(frmForm)
  lRecID2 = Val(frmForm.SecondRecordID(0))
  sCaption = sCaption
  
  
  
  
  Rem check for admin tracking tables
  'If Val(sGetShortCode(lGetGlssryTypCd(lGetTableID(sTable)))) = 8 Then
  '   lATTableID = lGetTableID(sTable)
  '   sTable = "ADMINTRACK"
  'End If

  Rem see if mail sys supports the table
  bSupported = True
 'Select Case sTable
  '   Case "EVENT"
   '     bSupported = True
    ' Case "CLAIM"
   '     ' claim is special because LOC is critical (all share same CLAIM screen)
   '     On Error Resume Next
   '     lLOBCode = Val(frmForm.labLineOfBusiness)
   '     On Error GoTo ET_bMailGetAttachInfo
   '
   '     sLOBShortCode = Trim$(sGetShortCode(lLOBCode))
   '     If (sLOBShortCode = "WC") Or (sLOBShortCode = "GC") Or (sLOBShortCode = "VA") Then
   '        bSupported = True
   '        sTable = sLOBShortCode
   '     End If
   '  Case "ENTITY"
   '     On Error Resume Next
   '
   '     sSecondTableName = ""
   '     sSecondTableName = frmForm.TableName(1)
    '
   '     On Error GoTo ET_bMailGetAttachInfo
   '
   '     sSecondTableName = Trim$(sSecondTableName)
   '     If sSecondTableName = "EMPLOYEE" Then
   '         sTable = "EMPLOYEE"
  '          bSupported = True
 '       Else   ' general entity
 '           ' encode entity table id into entity name
 '           lEntTableID = Val(frmForm.SecondRecordID(0))
 '           If lEntTableID <> 0 Then
 '              bSupported = True
 '
 '              sTable = "ENTITY" & Str$(lEntTableID)
  '          End If
  '      End If
 '    Case "ADMINTRACK"
 '       sTable = "ADMINTRACK" & Str$(lATTableID)
 '       bSupported = True
 '    Case Else
 '       bSupported = True 'wchen : now we try to support everything
 ' End Select
 '
  bMailGetAttachInfo = bSupported
  Exit Function

ET_bMailGetAttachInfo:
  bMailGetAttachInfo = False
  Exit Function

End Function

Function bMailIsRMWINMsg(sSubject, bIsReferenced, sTable, lRecID, lRecID2, lfrmCode, lFrmParentCode, lSecurity, sCaption) As Integer
On Error GoTo ET_bMailIsRMWINMsg

    Dim sTempSubject As String
    Dim iPos As Integer
    'Dim iPos2 As Integer
    Dim sTmp  As String


    If Left$(Trim(sSubject), 8) = RMWIN_SUBJECT_PREFIX Then   ' show only if RM win message
       bMailIsRMWINMsg = True
       bIsReferenced = False

       Rem strictly parse out reference header
       sTempSubject = Mid$(Trim(sSubject), 9)
       If sTempSubject <> "" Then
          sTempSubject = Trim$(sTempSubject)
          If Left$(sTempSubject, 7) = ("[" & RMWIN_REF_PREFIX & ":") Then
             sTempSubject = Mid$(sTempSubject, 8)
             sTempSubject = Trim$(sTempSubject)
             
             iPos = InStr(sTempSubject, ":")
             If iPos > 0 Then
                 sTable = Left$(sTempSubject, iPos - 1)
                 sTempSubject = Mid$(sTempSubject, Len(sTable) + 2)
                 sTempSubject = Trim$(sTempSubject)
                   
                 iPos = InStr(sTempSubject, ":")
                 If iPos > 0 Then
                     sTmp = Left$(sTempSubject, iPos - 1)
                     sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                     sTempSubject = Trim$(sTempSubject)
                     lRecID = Val(sTmp)
             
                     iPos = InStr(sTempSubject, ":")
                     If iPos > 0 Then
                         sTmp = Left$(sTempSubject, iPos - 1)
                         sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                         sTempSubject = Trim$(sTempSubject)
                         lRecID2 = Val(sTmp)
                 
                         iPos = InStr(sTempSubject, ":")
                         If iPos > 0 Then
                             sTmp = Left$(sTempSubject, iPos - 1)
                             sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                             sTempSubject = Trim$(sTempSubject)
                             lfrmCode = Val(sTmp)
                             
                             iPos = InStr(sTempSubject, ":")
                             If iPos > 0 Then
                                 sTmp = Left$(sTempSubject, iPos - 1)
                                 sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                                 sTempSubject = Trim$(sTempSubject)
                                 lFrmParentCode = Val(sTmp)
                 
                                 iPos = InStr(sTempSubject, ":")
                                 If iPos > 0 Then
                                     sTmp = Left$(sTempSubject, iPos - 1)
                                     sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                                     sTempSubject = Trim$(sTempSubject)
                                     lSecurity = Val(sTmp)
                 
                                     iPos = InStr(sTempSubject, "]" & Chr(9))
                                     If iPos > 0 Then
                                         sCaption = Left$(sTempSubject, iPos - 1)
                                         sTempSubject = "Re:" & sCaption
                                         bIsReferenced = True
                                     End If
                                 End If
                            End If
                         End If
                     End If
                 End If
             End If
          End If
       End If
       sSubject = Trim$(sTempSubject)   ' return trimmed subject
    Else
       bMailIsRMWINMsg = False
    End If

    Exit Function

ET_bMailIsRMWINMsg:
     bMailIsRMWINMsg = False
     Exit Function
End Function

Function CopyFiles(MfIn As MapiFile, MfOut As MapiFile) As Long

    MfOut.FileName = MfIn.FileName
    MfOut.PathName = MfIn.PathName
    MfOut.Reserved = MfIn.Reserved
    MfOut.flags = MfIn.flags
    MfOut.Position = MfIn.Position
    MfOut.FileType = MfIn.FileType
    CopyFiles = 1&

End Function

Function CopyRecipient(MrIn As MapiRecip, MrOut As MapiRecip) As Long

    MrOut.Name = MrIn.Name
    MrOut.Address = MrIn.Address
    MrOut.EIDSize = MrIn.EIDSize
    MrOut.EntryID = MrIn.EntryID
    MrOut.Reserved = MrIn.Reserved
    MrOut.RecipClass = MrIn.RecipClass

    CopyRecipient = 1&

End Function

Function MAPIReadMail(Session As Long, MessageID As String, flags As Long, Reserved As Long, Message As MAPIMessage, orig As MapiRecip, RecipsOut() As MapiRecip, FilesOut() As MapiFile) As Long

    Dim Info&
    Dim nFiles&
    Dim nRecips&
    Dim rc&
    Dim i As Integer
    Dim ignore As Long

    rc& = BMAPIReadMail(Info&, nRecips, nFiles, Session, 0, MessageID, flags, Reserved)

    If (rc& = SUCCESS_SUCCESS) Then

        'Message is now read into the handles array.  We have to redim the arrays and read
        'the stuff in

        If (nRecips = 0) Then nRecips = 1
        If (nFiles = 0) Then nFiles = 1

        ReDim Recips(0 To nRecips - 1) As MapiRecip
        ReDim Files(0 To nFiles - 1) As MapiFile

        rc = BMAPIGetReadMail(Info&, Message, Recips(), Files(), orig)
        If rc = SUCCESS_SUCCESS Then

           '*******************************************
           ' Copy Recipient and File structures from
           ' Local structures to those passed as
           ' parameters
           '*******************************************

           ReDim FilesOut(0 To nFiles - 1) As MapiFile
           ReDim RecipsOut(0 To nRecips - 1) As MapiRecip

           For i = 0 To nRecips - 1
               ignore = CopyRecipient(Recips(i), RecipsOut(i))
           Next i

           For i = 0 To nFiles - 1
               ignore = CopyFiles(Files(i), FilesOut(i))
           Next i
        Else
           'Call subDisplayMsg(90, "")
        End If
    Else
      'Call subDisplayMsg(90, "")
    End If

    MAPIReadMail = rc

End Function

Function sGetEmailByLoginName(sUser As String) As String
On Error GoTo ET_sGetEmailByLoginName

    Dim result As Integer
    Dim sTmp As String * 255
    Dim sEmailAddr As String
    Dim nTemp As Integer
    Dim bFound As Integer
    Dim lTemp As Long
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String
    Dim sDSN As String

    'result = GetSecurityDSN(sTmp)
    sTmp = objLogin.SecurityDSN
    sDSN = sTrimNull(sTmp)

    Rem Connect to security DB
    db = DB_OpenDatabase(henv, sDSN, 0)

    Rem Query
    sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" & sUser & "'"
    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       sEmailAddr = vDB_GetData(rs, 1) & ""
       bFound = True
    Else
       sEmailAddr = ""
       bFound = False
    End If

    Rem Shut down
    result = DB_CloseRecordset(rs, DB_DROP)
    result = DB_CloseDatabase(db)
    
    Rem return email address
    sGetEmailByLoginName = sEmailAddr
    Exit Function

ET_sGetEmailByLoginName:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "sGetEmailByLoginName")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            result = DB_CloseRecordset(rs, DB_DROP)
            result = DB_CloseDatabase(db)
            sGetEmailByLoginName = ""
            Exit Function
    End Select
End Function

Function sMailConvertToFaxAddress(sName As String, sNumber As String) As String
' This function turns a name and fax number into a valid fax
'  gateway address. Currently, this routine is hard-coded
'  to convert to Microsoft At Work fax format (included with
'  Microsoft Windows for Workgroups 3.11 and Windows 95).
'  This could be rewritten to be configurable in the future.

On Error GoTo ET_sMailConvertToFaxAddress


    sMailConvertToFaxAddress = "[fax:" & Trim$(sName) & "@" & Trim$(sNumber) & "]"

    Exit Function

ET_sMailConvertToFaxAddress:

     sMailConvertToFaxAddress = ""
  Exit Function


End Function

Function sMailGetEmailName(lUserID As Long) As String

On Error GoTo ET_sMailGetEmailName

    Dim result As Integer
    Dim sTmp As String * 255
    Dim sEmailAddr As String
    Dim nTemp As Integer
    Dim bFound As Integer
    Dim lTemp As Long
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String
    Dim sDSN As String

    'result = GetSecurityDSN(sTmp)
    sTmp = objLogin.SecurityDSN
    sDSN = sTrimNull(sTmp)

    Rem Connect to security DB
    db = DB_OpenDatabase(henv, sDSN, 0)

    Rem Query
    sSQL = "SELECT EMAIL_ADDR FROM USER_TABLE WHERE USER_ID = " & lUserID
    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       sEmailAddr = vDB_GetData(rs, 1) & ""
       bFound = True
    Else
       sEmailAddr = ""
       bFound = False
    End If

    Rem Shut down
    result = DB_CloseRecordset(rs, DB_DROP)
    result = DB_CloseDatabase(db)
    
    Rem return email address
    sMailGetEmailName = sEmailAddr
    Exit Function

ET_sMailGetEmailName:

     sMailGetEmailName = ""
  Exit Function

End Function

Function StringToken(iPos%, str1$, delim$) As String

  Dim iPos2 As Integer

'*******************************************************
'   Returns a string from string position "iPos" upto
'   a delimeter character
'*******************************************************

    If (Len(str1$) <> 0) Then

        iPos2% = iPos%

        iPos2% = InStr(iPos%, str1$, delim$) 'Find next delimeter
        If (iPos2% = 0) Then
            StringToken$ = Right$(str1$, Len(str1) - iPos% + 1)
            iPos% = 0
            Exit Function
        Else
            StringToken$ = Mid$(str1$, iPos%, iPos2% - iPos%)
        End If

        iPos% = iPos2% + 1

    Else

        iPos% = 0

    End If

End Function

Sub subLoadViewForm()
On Error GoTo ET_subLoadViewForm

    ReDim Mr(0 To 0) As MapiRecip
    ReDim Mf(0 To 0) As MapiFile
    Dim DateStr1 As String
    Dim DateStr2 As String
    Dim DateStr3 As String
    Dim sMonth As String
    Dim sDay As String
    Dim sYear As String
    Dim sTime As String
    Dim li As Integer
    Dim rc As Long
    Dim i As Integer
    Dim sTable As String
    Dim lRecID As Long
    Dim bIsReferenced As Integer
    Dim sSubject As String
    Dim bDummy As Integer
    Dim lRecID2, lfrmCode, lFrmParentCode, lSecurity As Long
    Dim sCaption As String

    Rem read in selected message
    li = frmMail.SubjectList.ListIndex
    MsgID$ = frmMail.IdList.List(li)

    rc = MAPIReadMail(WPAMapiSession, MsgID$, 0&, 0&, M, Mo, Mr(), Mf())
    If (rc& <> SUCCESS_SUCCESS) Then

        Call subDisplayMsg(90, "")
        Unload frmView
        Exit Sub
    End If

    Rem display message
    If (rc = SUCCESS_SUCCESS) Then

        frmView.To_EDIT.Text = Mo.Name

        sSubject = M.Subject
        bDummy = bMailIsRMWINMsg(sSubject, bIsReferenced, sTable, lRecID, lRecID2, lfrmCode, lFrmParentCode, lSecurity, sCaption)
        frmView.Subject_EDIT.Text = sSubject
        If bIsReferenced Then
           frmView.labReference = sTable & ":" & Format$(lRecID, "0000000000")
           frmView.btnGotoReference.Enabled = True

           frmView.labSysTable = sTable
           frmView.labSysRecID = lRecID
           
           ' JP 4/3/98
           frmView.sAttachTableName = sTable
           frmView.labAttachRecordID = lRecID
           frmView.labAttachSecondRecordID = lRecID2
        Else
           frmView.btnGotoReference.Enabled = False

           frmView.labSysTable = ""
           frmView.labSysRecID = "0"
        
           ' JP 4/3/98
           frmView.sAttachTableName = ""
           frmView.labAttachRecordID = 0
           frmView.labAttachSecondRecordID = 0
        End If

        frmView.Caption = sSubject

        DateStr1 = M.DateReceived
        sMonth = Trim$(Mid$(DateStr1, 6, 2))
        sDay = Trim$(Mid$(DateStr1, 9, 2))
        sYear = Trim$(Left$(DateStr1, 4))
        sTime = Trim$(Right$(DateStr1, 5))
        DateStr2 = sMonth & "/" & sDay & "/" & sYear
        DateStr3 = Format$(DateStr2, "Long Date")
        frmView.Date_EDIT.Text = DateStr3 & "   " & Format$(sTime, "Medium Time")
        frmView.Message_EDIT.Text = M.NoteText

        '******************************************
        ' Display any file attachment information
        '******************************************

        If (M.FileCount > 0) Then
            For i = 0 To M.FileCount - 1
                frmView.Message_EDIT.Text = frmView.Message_EDIT.Text + "<" + Mf(i).FileName + ">"
            Next i
        End If

    End If

  Exit Sub

ET_subLoadViewForm:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subLoadViewForm")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailCompose(bSendAttached As Integer)
On Error GoTo ET_subMailCompose

    Dim bIsReferencable As Integer
    Dim sTable As String
    Dim lRecID As Long, lRecID2 As Long
    Dim sCaption As String
    Dim lError As Long
    Dim nError As Integer

    Rem get attachment info if available (may be stand alone)
    If bSendAttached Then
       bIsReferencable = bMailGetAttachInfo(frmMDIParent.ActiveForm, sTable, lRecID, lRecID2, sCaption)
       'If bIsReferencable And ((lRecID = 0) Or (sTable = "")) Then bIsReferencable = False

       If Not bIsReferencable Then
          Call subDisplayMsg(150, sTable)
          Exit Sub
       End If
    End If

    Rem bring MAPI engine online (login if necessary)
    lError = lWPAInitMAPI()
    If lError <> 0 Then
       Call subDisplayMsg(lError, "")
       Exit Sub
    End If

    Rem load compose form
    Load frmCompose

    Rem if attached (Send Regarding...), attach info about attachment
    If (bIsReferencable) And (bSendAttached) Then
       frmCompose.labRefered = True
       frmCompose.labRegarding = sCaption
       frmCompose.labRegarding.Visible = True
       frmCompose.labRegardingHeader.Visible = True

       Rem enable insert summary ONLY if EVENT or CLAIM
       'If (Trim$(sTable) = "EVENT") Or (Trim$(sTable) = "CLAIM") Then
       '   frmCompose.btnInsertSummary.Visible = True
       'Else
       '   frmCompose.btnInsertSummary.Visible = False
       'End If

       Rem set the window caption
       frmCompose.Caption = frmCompose.labCaption(0)

       Rem plug in a default subject
       frmCompose.txtSubject = frmCompose.labRe & " " & sCaption

       Rem stuff in reference form table name and rec id
       frmCompose.labSysTable = sTable
       frmCompose.labSysRecID = lRecID
       
       ' JP 11/18/97   NOT USED    frmCompose.labFormSecurity = lSecurity
       ' JP 11/18/97   NOT USED    frmCompose.labFormCode = lfrmCode
       ' JP 11/18/97   NOT USED    frmCompose.labParentFormCode = lFrmParentCode
       frmCompose.labAttachRecordID = lRecID
       frmCompose.labAttachSecondRecordID = lRecID2
       frmCompose.sAttachTableName = sTable
       frmCompose.sAttachCaption = sCaption
    
    
    Else
       frmCompose.labRefered = False
       frmCompose.labRegarding.Visible = False
       frmCompose.labRegardingHeader.Visible = False

       frmCompose.btnInsertSummary.Visible = False

       frmCompose.Caption = frmCompose.labCaption(1)

       Rem blank out reference form table name and rec id
       frmCompose.labSysTable = ""
       frmCompose.labSysRecID = "0"
    End If

  Exit Sub

ET_subMailCompose:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailCompose")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailDelete()
On Error GoTo ET_subMailDelete

    Dim li As Integer
    Dim rc As Long


    '**********************************
    ' If there's no mail listed, then
    ' just exit this subroutine
    '**********************************

    If (frmMail.SubjectList.ListCount = 0) Then
        Exit Sub
    End If

    '*************************************
    ' Get the list index of the selected
    ' item, then retrive it's id from
    ' an associated msgid list.
    '*************************************

    li = frmMail.SubjectList.ListIndex
    MsgID$ = frmMail.IdList.List(li)

    '********************************************
    ' Call MAPIDeleteMail with the msgid of the
    ' mail to be deleted.
    '********************************************

    rc = MAPIDeleteMail(WPAMapiSession, 0&, MsgID$, 0&, 0&)

    '*********************************************
    ' if the call was successful, then we delete
    ' the msgid from IdList and the subject from
    ' the SubjectList.
    '*********************************************

    If (rc& = SUCCESS_SUCCESS) Then
        frmMail.SubjectList.RemoveItem li
        frmMail.IdList.RemoveItem li
    Else
        Call subDisplayMsg(92, "")
    End If

   'dbas 1/19/98 frmMDIParent.labMailStatus.Caption = frmMail.SubjectList.ListCount & " Messages"
   Exit Sub

ET_subMailDelete:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailDelete")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

Sub subMailLaunchDEForm(sTable As String, lRecID As Long)

On Error GoTo ET_subMailLaunchDEForm

    Dim sTmp As String
    Dim lTmp As Long
    Dim lEntTableID As Long
    Dim lATTableID As Long


    If Left$(sTable, 6) = "ENTITY" Then
       lEntTableID = Val(Mid$(sTable, 7))

       sTable = "ENTITY"

    ElseIf Left$(sTable, 10) = "ADMINTRACK" Then
       lATTableID = Val(Mid$(sTable, 11))

       sTable = "ADMINTRACK"

    End If

    Select Case UCase$(sTable)
        Case "GC"    ' general claim
            sTmp = "GC"
            lTmp = lGetCodeIDWithShort(sTmp, lGetTableID("LINE_OF_BUSINESS"))
            Call subDEClaim(0, lRecID, lTmp)
        Case "WC"    ' workers comp
            sTmp = "WC"
            lTmp = lGetCodeIDWithShort(sTmp, lGetTableID("LINE_OF_BUSINESS"))
            Call subDEClaim(0, lRecID, lTmp)
        Case "VA"    ' vehicle accidents
            sTmp = "VA"
            lTmp = lGetCodeIDWithShort(sTmp, lGetTableID("LINE_OF_BUSINESS"))
            Call subDEClaim(0, lRecID, lTmp)

            ' result = bSelectRecord(frmDEClaim, SELECT_RECORD_ID, lRecID, 0)
        Case "EVENT"
            Call subDEEvent(lRecID)

        Case "ENTITY"
            Call subDEEntityMaint(lRecID, lEntTableID, 0)

        Case "EMPLOYEE"
            Call subDEEmployee(lRecID)

        Case "ADMINTRACK"
            Call subATShowDE(sGetSystemTableName(lATTableID), sGetTableName(lATTableID), lRecID)
  '      Case 4    ' policy
  '          Call subDEPolicy
'        Case 7   ' administrative tracking
'            ' Call subShowTableMaint(0)
'        Case 10
'            Call subDEVehicle
'        Case 14  ' employee
'            Call subDEEmployee(0)
        Case Else
            Call subDisplayMsg(150, sTable)
            Exit Sub
    End Select
'    If sTmp <> "" Then
'        lTmp = lGetCodeIDWithShort(DB_GetHDBC(dbLookup), sTmp, lGetTableID("LINE_OF_BUSINESS"))
'        Call subDEClaim(0, 0, lTmp)
'    End If

    Exit Sub

ET_subMailLaunchDEForm:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "MAPIVB.BAS/subMailLaunchDEForm")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

