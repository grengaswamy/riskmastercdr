Attribute VB_Name = "UTIL_CDS"
Option Explicit



Function bInitCodes(bCheckSyncFlag As Integer, bLogin As Integer) As Integer

    Static ignore As Integer
    Dim bDoCache As Integer
    Static sLastRefresh As String

    bInitCodes = False
    bDoCache = bLogin  ' flags whether caching should occur on this call or not

    Rem if sync checking is desired, check cache option settings to see what to do
    If Not ignore And bCheckSyncFlag And Not bLogin Then bDoCache = bIsCacheNeeded("CODES", sLastRefresh, ignore)

    If bDoCache Then
        'subsetsystemstatus "Downloading Codes from Database..."
        If bCodesInit(DB_GetHdbc(dbLookup)) Then sLastRefresh = Format(Date & " " & Time, "YYYYMMDDHHNNSS")
    End If

    bInitCodes = True

End Function

Function sGetArrayCodeDesc(CodeAry() As Long, iCodes As Integer) As String

    Dim sDesc As String
    Dim i As Integer

    sDesc = ""
    For i = 0 To iCodes - 2
        sDesc = sDesc & sGetCodeDesc(CodeAry(i)) & ", "
    Next i
    sDesc = sDesc & sGetCodeDesc(CodeAry(iCodes - 1))

    sGetArrayCodeDesc = sDesc

End Function

Function sGetCodeDesc(lCode As Long) As String

    Dim sCode As String * 128
    Dim ln As Long

    ln = 0
    sGetCodeDesc = ""
    If bInitCodes(False, False) Then
        If bGetCodeDesc(DB_GetHdbc(dbLookup), lCode, sCode, ln) Then If ln > 0 Then sGetCodeDesc = Trim$(sTrimNull(sCode))
    End If

End Function

Function sGetListCodeDesc(sCodes As String, iCodes As Integer) As String

    Dim sDesc As String
    Dim i As Integer
    Dim sSel As String
    Dim iPtr As Integer

    sSel = sCodes
    sDesc = ""
    For i = 1 To iCodes
        iPtr = InStr(sSel, ",")
        sDesc = sDesc & sGetCodeDesc(Val(Left$(sSel, iPtr - 1))) & ", "
        sSel = Mid$(sSel, iPtr + 1)
    Next i
    sGetListCodeDesc = Left$(sDesc, Len(sDesc) - 1)

End Function

Function sGetShortCode(lCode As Long) As String

    Dim sCode As String * 128
    Dim ln As Long

    ln = 0
    sGetShortCode = ""
    If bInitCodes(False, False) Then
        If bGetShortCode(DB_GetHdbc(dbLookup), lCode, sCode, ln) Then If ln > 0 Then sGetShortCode = Trim$(sTrimNull(sCode))
    End If

End Function

