Attribute VB_Name = "UTIL_ERR"
'*****************************************************************
'*
'*  UTIL_ERR.BAS
'*
'*  <Enter Module Description Here>
'*
'*  Date Written:             By:
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Option Explicit

'Trapped System Errors
Const ERR_UNKNOWN = 1
Const ERR_VB_RUN_TIME = 2
Const ERR_PRINTER = 3
Const ERR_GRID_CONTROL = 4
Const ERR_OLE = 5
Const ERR_COMMON_DIALOG = 6
Const ERR_ODBC = 7
Const ERR_DATA_ACCESS = 8

Global Const IDCANCEL = 2
Global Const IDRETRY = 4

Function iGeneralErrorExt(ByVal ECode As Long, sErrorString As String, sErrArguments As String) As Integer

On Error GoTo ERROR_TRAP_iGeneralErrorExt

    Dim sTitle As String
    Dim HoldCode As Long
    Dim iFile As Integer
    Dim sOutStr As String

    HoldCode = ECode

    Call subWriteErrorLog(sErrArguments, ECode, sErrorString)
        
    'Shruti for integration with TM Service in RMX
    sOutStr = Chr$(34) & xRMUserInfo.sUserLoginName & Chr$(34) & "," & Chr$(34) & xRMInfo.sODBCName & Chr$(34) & "," & Chr$(34) & Trim$(Format$(Now, "YYYYMMDDHHNNSS")) & Chr$(34)
    sOutStr = sOutStr & "," & Chr$(34) & "WPA Processing:" & sErrArguments & Chr$(34) & "," & Chr$(34) & Trim$(Str$(ECode)) & Chr$(34) & "," & Chr$(34) & sErrorString & Chr$(34)

    'Call WriteToStdOut(ECode, ReplaceStr(sOutStr, Chr$(13) & Chr$(10), "  ") & Chr$(13) & Chr$(10))
    'Shruti for integration with TM Service in RMX ends
    
    Select Case ECode
        Case 482
            ECode = ERR_PRINTER

        Case 5 To 481, 483 To 521
            ECode = ERR_VB_RUN_TIME

        Case 600 To 648
            ECode = ERR_ODBC

        Case 3001 To 3299
            'If ECode = 3186 Or Err.Number = 3260 Then FreeLocks    ' if locking error, do FreeLocks (may help)

            ' invisible retries
            If ECode = 3197 Then ' DATA CHANGED
                iGeneralErrorExt = IDRETRY
                Exit Function
            ElseIf ECode = 3146 Then ' ODBC ERROR
                If InStr(UCase$(sErrorString), "LOCK") Then
                    Dim dStart As Double
    
                    'FreeLocks
                    dStart = Timer
                    While (Timer - dStart) < 120
                        DoEvents
                    Wend
                    'FreeLocks
                    iGeneralErrorExt = IDRETRY
                    Exit Function
                End If
            End If
            ECode = ERR_DATA_ACCESS

        Case 20476 To 28671, 32751 To 32765
            If ECode = 32755 Then
                Exit Function
            Else
                ECode = ERR_COMMON_DIALOG
            End If

        Case 29999
            ECode = ERR_ODBC
            'sErrorString = Err.Description

        Case 30000 To 30019
            ECode = ERR_GRID_CONTROL

        Case 31001 To 31039
            ECode = ERR_OLE

        Case Else
            ECode = ERR_UNKNOWN

    End Select

    Select Case ECode
        Case 2: sTitle = "VB Run-Time Error"
        Case 3: sTitle = "Printer Error"
        Case 4: sTitle = "Grid Control Error"
        Case 5: sTitle = "OLE Error"
        Case 6: sTitle = "Common Dialog Error"
        Case 7: sTitle = "ODBC Error"
        Case 8: sTitle = "Data Access Error"
        Case Else: sTitle = "Unknown error"
    End Select
    
    sErrArguments = "ERROR CODE = " & LTrim$(Str$(HoldCode)) & Chr$(13) & Chr$(10) & "ERROR DESCRIPTION = " & sErrorString & Chr$(13) & Chr$(10) & "MODULE\PROCEDURE = " & sErrArguments
    If bBackground = 0 Then
        Beep
        iGeneralErrorExt = MsgBox(sErrArguments, 53, sTitle)
    Else
        iFile = FreeFile
        Open sGetSystemPath() & "wpaproc.log" For Append As iFile
        Print #iFile, "[" & Now & "] " & sErrArguments
        Close iFile
        'Shruti for integration with TM Service in RMX
        Call WriteToStdOut(ECode, ReplaceStr(sErrArguments, Chr$(13) & Chr$(10), "  ") & Chr$(13) & Chr$(10))
        'Shruti for integration with TM Service in RMX ends
        iGeneralErrorExt = IDCANCEL
    End If

    Exit Function

ERROR_TRAP_iGeneralErrorExt:
    Dim EResult As Integer
    If bBackground = 0 Then
        Beep
        EResult = MsgBox(" Critical System Error- " & Error$(Err) & " in {iGeneralErrorExt}" & Chr(13) & Chr(10) & "System will shut down!", 21, "Error- " & Err)
    Else
        EResult = IDCANCEL
    End If
    Select Case EResult
        Case IDRETRY
            Resume
        Case IDCANCEL
            End
        Case Else
            Exit Function
    End Select
End Function

Sub subWriteErrorLog(sModule As String, lErrCode As Long, sErrString As String)
On Error GoTo ET_subWriteErrorLog

    Dim sLogPath As String
    Dim sItem As String * KEY_LENGTH
    Dim sKey As String
    Dim nRetry As Integer
    Dim bDone As Integer
    Dim nFileNo As Integer
    Dim dStart As Double
    Dim sOutStr As String

    Rem determine location of error log
    sKey = REG_ROOT_KEY & "\RISKMASTER\Security\Directory"
    If RegQueryValue(HKEY_LOCAL_MACHINE, sKey, sItem, KEY_LENGTH) = 0 Then
        sLogPath = Trim$(sTrimNull(sItem))
    Else
        ' couldn't find security directory entry in reg database - put in program directory instead
        sLogPath = Trim$(sGetSystemPath())
    End If
    If Right$(sLogPath, 1) = "\" Then sLogPath = sLogPath & "RMERR.LOG" Else sLogPath = sLogPath & "\RMERR.LOG"

    Rem open error log for writing (retry up to 10 times if needed)
    nRetry = 0
    bDone = False
    
    
    nFileNo = FreeFile

    While Not bDone And (nRetry < 10)
        On Error Resume Next
        Open sLogPath For Append Lock Write As nFileNo
        If Err = 0 Then bDone = True
        On Error GoTo ET_subWriteErrorLog
    
        Rem if couldn't open, wait 5 seconds and try again
        If Not bDone Then
            dStart = Timer
            While (Timer - dStart) < 5
                DoEvents
            Wend
        End If
    
        nRetry = nRetry + 1
    Wend

    If bDone Then
        Rem write error statement to file
        sOutStr = Chr$(34) & xRMUserInfo.sUserLoginName & Chr$(34) & "," & Chr$(34) & xRMInfo.sODBCName & Chr$(34) & "," & Chr$(34) & Trim$(Format$(Now, "YYYYMMDDHHNNSS")) & Chr$(34)
        sOutStr = sOutStr & "," & Chr$(34) & "WPA Processing:" & sModule & Chr$(34) & "," & Chr$(34) & Trim$(Str$(lErrCode)) & Chr$(34) & "," & Chr$(34) & sErrString & Chr$(34)
        Print #nFileNo, sOutStr
        Rem close error log
        Close #nFileNo
    End If
    Exit Sub

ET_subWriteErrorLog:
    Dim EResult As Integer
    If bBackground = 0 Then
        Beep
        EResult = MsgBox(" System Error- " & Error$ & " :: While Writing Error Log", 21, "Error- " & Err)
    Else
        EResult = IDCANCEL
    End If
    Select Case EResult
        Case IDRETRY
            Resume
        Case IDCANCEL
            Exit Sub
        Case Else
            Exit Sub
    End Select
End Sub

