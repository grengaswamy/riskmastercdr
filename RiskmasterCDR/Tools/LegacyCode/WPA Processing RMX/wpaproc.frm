VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmWPAProc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "WPA Auto Diary Processing"
   ClientHeight    =   4260
   ClientLeft      =   1725
   ClientTop       =   2220
   ClientWidth     =   6915
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "wpaproc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4260
   ScaleWidth      =   6915
   Begin ComctlLib.ProgressBar pnlAutoDef 
      Height          =   315
      Left            =   1800
      TabIndex        =   9
      Top             =   2340
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   556
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   3
      Top             =   360
      Width           =   6630
   End
   Begin VB.CommandButton cmdStartStop 
      Caption         =   "&Start"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   1500
      TabIndex        =   0
      Tag             =   "0"
      Top             =   3720
      Width           =   1215
   End
   Begin VB.CommandButton cmdSuspendResume 
      Caption         =   "S&uspend"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2820
      TabIndex        =   1
      Tag             =   "0"
      Top             =   3720
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   4140
      TabIndex        =   2
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Label lblTotDiaries 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   8
      Top             =   2940
      Width           =   1095
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Process Status"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   1065
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Definitions Processed"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   2400
      Width           =   1530
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total Diaries Created"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   2940
      Width           =   1485
   End
   Begin VB.Label lblDefsProc 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   5520
      TabIndex        =   7
      Top             =   2460
      Width           =   1215
   End
End
Attribute VB_Name = "frmWPAProc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub cmdStartStop_Click()

    If cmdStartStop.Tag = "0" Then

        lstStatus.Clear
        pnlAutoDef.Value = 0
        lblDefsProc.Caption = "0 of 0"
        lblTotDiaries.Caption = "0"

        cmdStartStop.Tag = "1"
        cmdStartStop.Caption = "&Stop"
        cmdSuspendResume.Enabled = True
        cmdExit.Enabled = False

        gAskStop = False
        gProcessStatus = PS_GO
        Call ProcessAuto
        'Merged by Nitin for ReserveWorksheet in R6 Starts
            'Start: Mohit Yadav: 01/23/2009: Safeway: Payments/Reserve Worksheets diary creation
        Call GenerateTimeLapseDiaries
            'End: Mohit Yadav: 01/23/2009: Safeway: Payments/Reserve Worksheets diary creation
        'End: Merged by Nitin for ReserveWorksheet in R6
        If gProcessStatus <> PS_STOP Then UpdateStatus "Auto Diary Processing completed " & Format$(Now, "MM/DD/YYYY") & " @ " & Format$(Now, "HH:MM:SSam/pm")
        gProcessStatus = PS_END

        cmdStartStop.Tag = "0"
        cmdStartStop.Caption = "&Start"
        cmdSuspendResume.Tag = "0"
        cmdSuspendResume.Caption = "S&uspend"
        cmdSuspendResume.Enabled = False
        cmdExit.Enabled = True

    Else
        cmdStartStop.Tag = "0"
        cmdStartStop.Caption = "&Start"
        cmdSuspendResume.Tag = "0"
        cmdSuspendResume.Caption = "S&uspend"
        cmdSuspendResume.Enabled = False
        cmdExit.Enabled = True

        gProcessStatus = PS_STOP
        UpdateStatus "Auto Diary Processing stopped " & Format$(Now, "MM/DD/YYYY") & " @ " & Format$(Now, "HH:MM:SSam/pm")
    End If

End Sub

Private Sub cmdSuspendResume_Click()

    If cmdSuspendResume.Tag = "0" Then
        cmdSuspendResume.Tag = "1"
        cmdSuspendResume.Caption = "&Resume"
        cmdStartStop.Enabled = True
        cmdExit.Enabled = True
        gProcessStatus = PS_SUSPEND
        UpdateStatus "Auto Diary Processing suspended " & Format$(Now, "MM/DD/YYYY") & " @ " & Format$(Now, "HH:MM:SSam/pm")
    Else
        cmdSuspendResume.Tag = "0"
        cmdSuspendResume.Caption = "S&uspend"
        cmdStartStop.Enabled = True
        cmdExit.Enabled = False
        gProcessStatus = PS_GO
        UpdateStatus "Auto Diary Processing resumed " & Format$(Now, "MM/DD/YYYY") & " @ " & Format$(Now, "HH:MM:SSam/pm")
    End If

End Sub

Private Sub Form_Load()

    gProcessStatus = PS_END

    Call subCenterForm(Me)

End Sub

Private Sub Form_Unload(Cancel As Integer)

   ' Dim rc As Integer

    If gProcessStatus <> PS_END Then
        Cancel = True
        Exit Sub
    End If

    DB_CloseDatabase dbCreate
    DB_CloseDatabase dbLookup
     DB_CloseDatabase dbGlobalConnect
    DB_CloseDatabase dbGlobalConnect2
     DB_FreeEnvironment hEnv
    Set objUser = Nothing
    Set objLogin = Nothing
    'rc = UnRegisterApplication()
   
   Set frmWPAProc = Nothing
End Sub

