Attribute VB_Name = "UTILITY"
Option Explicit


Function bAreFormsActive(Frm As MDIForm) As Integer
    On Error Resume Next
    Dim sTmp As String
    sTmp = Frm.ActiveForm.Caption
    If Err Then
        bAreFormsActive = False
    Else
        bAreFormsActive = True
    End If
End Function

Function bIsDate(ByVal sDate As String) As Integer
  If IsNumeric(sDate) And Len(sDate) = 8 Then
    bIsDate = True
  Else
    bIsDate = False
  End If
End Function

Function dAnyVarToDouble(a As Variant) As Double
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'1 a | Variant | Input | No information at this time
'
'****************************************************************
    Select Case VarType(a)
        Case V_NULL, V_EMPTY
            dAnyVarToDouble = 0
            Exit Function
        Case V_INTEGER, V_LONG, V_SINGLE, V_DOUBLE
            dAnyVarToDouble = a
            Exit Function
        Case V_STRING
            dAnyVarToDouble = Val(a)
            Exit Function
    End Select


End Function

Function dDateTimeValue(sDate As String, sTime As String) As Double
    Dim dtmp As Double
    'Dim sTmp As String
    'dTmp = Second(sTime) * 60
    'dTmp = dTmp + Minute(sTime) * 60
    'dTmp = dTmp + Hour(sTime) * 3600
    'dTmp = Val("." & Str(dTmp))
    'dTmp = dTmp + DateValue(sDate)
    dtmp = DateValue(sDate) + TimeValue(sTime)

    dDateTimeValue = dtmp

End Function

Function dDTTMToDouble(sDTTM As String) As Double
    Dim dtmp As Double
    Dim sDate As String
    Dim sTime As String

    If Trim(sDTTM) <> "" Then
        sDate = Mid(sDTTM, 5, 2) & "-" & Mid(sDTTM, 7, 2) & "-" & Left(sDTTM, 4)
        sTime = Mid(sDTTM, 9, 2) & ":" & Mid(sDTTM, 11, 2) & ":" & Mid(sDTTM, 13, 2)
        dtmp = DateValue(sDate) + TimeValue(sTime)
    End If
    dDTTMToDouble = dtmp

End Function

Function dFilterCurrency(dAmt As Double) As Double
' PURPOSE: Rounds a number to zero if it is very, very small.

   If dAmt < 0.005 And dAmt > -0.005 Then dFilterCurrency = 0# Else dFilterCurrency = dAmt
End Function

Function HIBYTE(ShortInt As Integer) As Integer
    HIBYTE% = ShortInt% \ 256
End Function

Function iAnyVarToInt(a As Variant) As Integer
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'1 a | Variant | Input | No information at this time
'
'****************************************************************
    Select Case VarType(a)
        Case V_NULL, V_EMPTY
            iAnyVarToInt = 0
            Exit Function
        Case V_INTEGER, V_LONG
            iAnyVarToInt = a
        Case V_SINGLE, V_DOUBLE
            iAnyVarToInt = Int(a)
            Exit Function
        Case V_STRING
            iAnyVarToInt = Val(a)
            Exit Function
    End Select
End Function

Function iCalculateAge(sDate1 As String, sDate2 As String) As Integer
    Dim lTmp As Long

    If IsDate(sDate1) And IsDate(sDate2) Then
        lTmp = Int(DateDiff("y", sDate1, sDate2) / 365.25)
        If lTmp > 32000 Then lTmp = 0
        If lTmp < 0 Then lTmp = 0
        iCalculateAge = lTmp
    Else
        iCalculateAge = 0
    End If


End Function

Function iFindItemInListbox(lst As Control, lItemData As Long) As Integer
   Dim nCount As Integer
   Dim i As Integer

   nCount = lst.ListCount - 1
   For i = 0 To nCount
      If lst.ItemData(i) = lItemData Then
         iFindItemInListbox = i
         Exit Function
      End If
   Next i

   iFindItemInListbox = 0
End Function

Function IsTableInSQLFrom(ByVal sSQLFrom As String, ByVal sTableName As String) As Integer
'******************************************************
'* Function check does table already exists in SQL    *
'* FROM clause and if it exist return TRUE, otherwise *
'* function return false.                             *
'* sSQLFrom   - SQL From clause                       *
'* sTableName - Table name to check for.              *
'*                                                    *
'* Author: Denis Basaric, 10/27/1997                  *
'******************************************************
Const SQL_FROM = "FROM "
Dim i As Integer

' Default is false
IsTableInSQLFrom = False

If sSQLFrom = "" Or sTableName = "" Then GoTo hExit

sSQLFrom = Trim$(UCase$(sSQLFrom))
sTableName = Trim$(UCase$(sTableName))

' Remember Length of sTableName
i = Len(sTableName)

' Check is Table name at begining of sSQLFrom
If Left$(sSQLFrom, i) = sTableName Then
   IsTableInSQLFrom = True
ElseIf Right$(sSQLFrom, i) = sTableName Then ' Is it at the end ?
   IsTableInSQLFrom = True
ElseIf Left$(sSQLFrom, i + Len(SQL_FROM)) = SQL_FROM & sSQLFrom Then ' Is it at begining with FROM
   IsTableInSQLFrom = True
ElseIf InStr(sSQLFrom, sTableName & ",") > 0 Then ' Is it inside with ,
   IsTableInSQLFrom = True
End If

hExit:

End Function


Function lAnyVarToLong(a As Variant) As Long
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'1 a | Variant | Input | No information at this time
'
'****************************************************************
    Select Case VarType(a)
        Case V_NULL, V_EMPTY
            lAnyVarToLong = 0
            Exit Function
        Case V_INTEGER, V_LONG
            lAnyVarToLong = a
        Case V_SINGLE, V_DOUBLE
            lAnyVarToLong = Int(a)
            Exit Function
        Case V_STRING
            lAnyVarToLong = Val(a)
            Exit Function
    End Select
End Function

Function LOBYTE(ShortInt As Integer) As Integer
    LOBYTE% = ShortInt% Mod 256
End Function

Function LOWORD(LongInt As Long) As Integer
    LOWORD% = LongInt& Mod 65536
End Function

Sub PutPrinter(ByVal a, ByVal X As Single, ByVal Y As Single, Justify As Integer)
On Error GoTo ERROR_TRAP_subPutPrinter
    'Global Const LEFT_JUSTIFY = 0  ' 0 - Left Justify
    'Global Const RIGHT_JUSTIFY = 1 ' 1 - Right Justify
    'Global Const CENTER = 2        ' 2 - Center
    'Global Const JUSTIFY = 3       ' 3 - JUSTIFY preforms line wrap not available
    Dim length As Single
    Dim PtText As String
    Dim Text As String
    Select Case VarType(a)
        Case V_NULL, V_EMPTY
            Text = ""
        Case V_INTEGER, V_LONG, V_SINGLE, V_DOUBLE
            Text = Str(a)
        Case V_STRING
            Text = a
    End Select

    PtText = Trim(Text)
    length = Printer.TextWidth(PtText)
    Select Case Justify
        Case 0
            Printer.CurrentY = Y
            Printer.CurrentX = X
            Printer.Print PtText

        Case 1
            Printer.CurrentY = Y
            Printer.CurrentX = X - length
            Printer.Print PtText

        Case 2
            Printer.CurrentY = Y
            Printer.CurrentX = X - (length / 2)
            Printer.Print PtText
    End Select
Exit Sub
ERROR_TRAP_subPutPrinter:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "UTIL.BAS/subPutPrinter")
    Select Case EResult
        Case IDCANCEL
            Exit Sub
        Case IDRETRY
            Resume
        Case Else
            Exit Sub
    End Select

End Sub

Function sAnyVarToString(a As Variant)
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'1 a | Variant | Input | No information at this time
'
'****************************************************************
    Select Case VarType(a)
        Case V_NULL, V_EMPTY
            sAnyVarToString = ""
            Exit Function
        Case V_INTEGER, V_LONG, V_SINGLE, V_DOUBLE
            sAnyVarToString = Str(a)
            Exit Function
        Case V_STRING
            sAnyVarToString = a
            Exit Function
    End Select
End Function

Function sDateTimeValue(sDate As String, sTime As String) As String
    Dim sTmp As String
    sTmp = Format(sDate & " " & sTime, "YYYYMMDDHHNNSS")
    sDateTimeValue = sTmp
End Function

Function sDBDateFormat(sDate As String, sFormat As String) As String
    Dim sTmp As String
    sDBDateFormat = ""
    If Trim(sDate) <> "" Then
        sTmp = Mid(sDate, 5, 2) & "-" & Mid(sDate, 7, 2) & "-" & Left(sDate, 4)
        sDBDateFormat = Format(sTmp, sFormat)
    End If
End Function

Function sDBDTTMFormat(sDTTM As String, sDateFormat As String, sTimeFormat As String) As String
    Dim sTmp As String
    Dim sDateTmp As String
    sTmp = Mid(sDTTM, 5, 2) & "-" & Mid(sDTTM, 7, 2) & "-" & Left(sDTTM, 4)
    sDateTmp = Format(sTmp, sDateFormat)
    'mjh 7/25/96  I can not believe that this has never worked and no one has reported that
    'their time is always related to the year.  before it waould create time off of the year
    sTmp = Mid(sDTTM, 9, 2) & ":" & Mid(sDTTM, 11, 2) & ":" & Mid(sDTTM, 13, 2)
    sDateTmp = sDateTmp & " " & Format(sTmp, sTimeFormat)

    sDBDTTMFormat = sDateTmp

End Function

Function sDBTimeFormat(sTime As String, sFormat As String) As String
    Dim sTmp As String
    sDBTimeFormat = ""
    If Trim(sTime) <> "" Then
        sTmp = Left(sTime, 2) & ":" & Mid(sTime, 3, 2) & ":" & Mid(sTime, 5, 2)
        sDBTimeFormat = Format(sTmp, sFormat)
    End If

End Function

Function sGetSystemPath() As String
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'
'****************************************************************
    If Right(App.Path, 1) = "\" Then
        sGetSystemPath = App.Path
    Else
        sGetSystemPath = App.Path & "\"
    End If

End Function

Function sGetTablesNotInSQLFrom(ByVal sSQLFrom As String, ByVal sTableNames As String) As String
'******************************************************
'* Function check does Table/Tables separated with    *
'* comma exist(s) in SQL From clause and return all   *
'* table(s) that not exist there, comma separated.    *
'* sSQLFrom    - SQL From clause                      *
'* sTableNames - Table(s) name to check for.          *
'* Return: String with all table(s) that don't exists *
'*         in FROM clause, comma separated.           *
'* Author: Denis Basaric, 10/27/1997                  *
'******************************************************
Dim i As Integer, iLastPosition As Integer
Dim s As String
Dim sReturn As String

Const SEPARATOR$ = ","

sTableNames = Trim$(sTableNames)
sSQLFrom = Trim$(sSQLFrom)

If sTableNames = "" Then GoTo hSGTNT_Exit

If sSQLFrom = "" Then
   sReturn = sTableNames
   GoTo hSGTNT_Exit
End If

' Check did we receive just one table
i = InStr(sTableNames, SEPARATOR)
If i = 0 Then
   ' It's just one table
   If Not IsTableInSQLFrom(sSQLFrom, sTableNames) Then
      ' Table not in FROM
      sReturn = sTableNames
   End If
   GoTo hSGTNT_Exit
End If

' Otherwise Parse all tables from string and check does they exists in SQL FROM clause
iLastPosition = 1
Do While i > 0
   s = Mid$(sTableNames, iLastPosition, i - iLastPosition)
   If Not IsTableInSQLFrom(sSQLFrom, s) Then
      If sReturn <> "" Then sReturn = sReturn & SEPARATOR
      sReturn = sReturn & s
   End If
   iLastPosition = i + 1
   i = InStr(iLastPosition, sTableNames, SEPARATOR)

   If i = 0 Then
      ' Get the last one
      If iLastPosition < Len(sTableNames) Then
         s = Mid$(sTableNames, iLastPosition)
         If Not IsTableInSQLFrom(sSQLFrom, s) Then
            If sReturn <> "" Then sReturn = sReturn & SEPARATOR
               sReturn = sReturn & s
            End If
         End If
   End If
Loop

' Function Exit
hSGTNT_Exit:
sGetTablesNotInSQLFrom = sReturn

End Function

' FUNCTION: sSQLTextArg
' PURPOSE : Process varchar arguments for use directly in SQL statements.
'           Quotes the string if not empty. Double quotes any single quotes so they will work.
'           Returns the string NULL if string is empty.
Function sSQLTextArg(sArg As String) As String
   Dim sTmp As String

   If sArg = "" Then
      sTmp = "NULL"
   Else
      sTmp = ReplaceStr(sArg, "'", "''")
      sTmp = "'" & ReplaceStr(sTmp, """", """""") & "'"
   End If
   sSQLTextArg = sTmp
End Function

Function sTrimNull(sArg As String) As String
    Dim Index As Integer
    Index = InStr(1, sArg, Chr$(0))
    If Index Then
        sTrimNull = Left$(sArg, Index - 1)
    Else
        sTrimNull = sArg
    End If
End Function

Sub subCenterForm(pForm As Form)
'//Technical Info************************************************
'
'//Description
'This needs a description
'
'//Arguments
'1 pForm | Form | Input | No information at this time
'
'****************************************************************

    pForm.Left = (Screen.Width - pForm.Width) \ 2
    pForm.Top = (Screen.Height - pForm.Height) \ 2

End Sub

Sub subCopyCombo(cb1 As ComboBox, cb2 As ComboBox)
    ' Copies list contents of cb1 to cb2
    cb2.Clear
    Dim i%
    For i = 0 To cb1.ListCount - 1
        cb2.AddItem cb1.List(i)
        cb2.ItemData(i) = cb1.ItemData(i)
    Next i
End Sub

Sub subDB_CloseRecordset(rs As Integer, iType As Integer)
   Dim result As Integer

   
   If rs <> 0 Then
      result = DB_CloseRecordset(rs, iType)
      If iType = DB_DROP Then rs = 0  ' clear rs var once freed
   End If
End Sub

Sub subHourGlass(bDisplay As Integer)

    If bBackground <> 0 Then Exit Sub

    If bDisplay Then
        Screen.MousePointer = 11
    Else
        Screen.MousePointer = 0
    End If

End Sub

Function vDB_GetData(rs As Integer, vField As Variant) As Variant
   Dim vData As Variant
   

   DB_GetData rs, vField, vData

   vDB_GetData = vData
End Function

