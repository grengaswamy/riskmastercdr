VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COSHAItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'OSHA 300
Private m_lEventId As Long
Private m_sCaseNumber As String 'local copy
Private m_sOccupation As String 'local copy
Private m_sDateOfEvent As String 'local copy
Private m_sTimeOfEvent As String 'local copy
Private m_sEventLocation As String 'local copy
Private m_sInjuryDesc As String 'local copy
Private m_lRecordAsInjury As Long 'local copy
Private m_lRecordAsMuscSkel As Long 'local copy
Private m_lRecordAsSkinDisor As Long 'local copy
Private m_lRecordAsRespCond As Long 'local copy
Private m_lRecordAsHearLoss As Long 'local copy
Private m_lRecordAsOther As Long 'local copy
Private m_sChkBoxDeath As String 'local copy
Private m_sChkBoxRestriction As String 'local copy
Private m_sChkBoxWorkLoss As String 'local copy
Private m_sChkBoxOtherRecord As String 'local copy
Private m_lDaysWorkLoss As Long 'local copy
Private m_lDaysRestriction As Long 'local copy
Private m_lRecordAsPoison As Long 'local copy
Private m_lPrivacyCase As Long 'local copy jlt 01/10/2002
Private m_sDateOfDeath As String
Private m_sDateOfHire As String
Private m_sEmployeeStartTime As String
Private m_sEmployeeAddress As String
Private m_sEmployeeCity As String
Private m_sEmployeeState As String
Private m_sEmployeeZipCode As String
Private m_sEmployeeDateOfBirth As String
Private m_sEmployeeSex As String
Private m_sEmployeeFullName As String
Private m_sSortOn As String
Private m_sEmployeeOccupation As String
Private m_sActivityWhenInjured As String
Private m_sDisabilityCode As String
Private m_sPIRowId As String
Private m_sIllnessDesc As String
Private m_sObjectSubstanceThatInjured As String
Private m_lSharpsObjectId As Long
Private m_lSharpsMakeId As Long
Private m_sHowEventOccurred As String
Private m_sHealthCareEntityFacility As String
Private m_sHealthCareEntityAddress As String
Private m_sHealthCareEntityCity As String
Private m_sHealthCareEntityState As String
Private m_sHealthCareEntityZipCode As String
Private m_iDaysOfWeek() As Integer
Private m_sHealthCareProf As String
Private m_bEmergencyRoomTreatment As Boolean
Private m_bHospitalizedOverNight As Boolean
' MITS 9101 Manish C Jha 08/21/2008
Private m_sLocDescription As String

Public Property Get CaseNumber() As String

    CaseNumber = m_sCaseNumber

End Property

Public Property Let CaseNumber(ByVal sCaseNumber As String)

    m_sCaseNumber = sCaseNumber

End Property

Public Property Get EmployeeFullName() As String

    EmployeeFullName = m_sEmployeeFullName

End Property

Public Property Let EmployeeFullName(ByVal sEmployeeFullName As String)

    m_sEmployeeFullName = sEmployeeFullName

End Property
' MITS 9101 Manish C Jha 08/21/2008
Public Property Get LocDescription() As String

    LocDescription = m_sLocDescription

End Property

Public Property Let LocDescription(ByVal LocDescription As String)

    m_sLocDescription = LocDescription

End Property

Public Property Get Occupation() As String

    Occupation = m_sOccupation

End Property

Public Property Let Occupation(ByVal sOccupation As String)

    m_sOccupation = sOccupation

End Property

Public Property Get DateOfEvent() As String

    DateOfEvent = m_sDateOfEvent

End Property

Public Property Let DateOfEvent(ByVal sDateOfEvent As String)

    m_sDateOfEvent = sDateOfEvent

End Property

Public Property Get TimeOfEvent() As String

    TimeOfEvent = m_sTimeOfEvent

End Property

Public Property Let TimeOfEvent(ByVal sTimeOfEvent As String)

    m_sTimeOfEvent = sTimeOfEvent

End Property

Public Property Get EventLocation() As String

    EventLocation = m_sEventLocation

End Property

Public Property Let EventLocation(ByVal sEventLocation As String)

    m_sEventLocation = sEventLocation

End Property

Public Property Get InjuryDesc() As String

    InjuryDesc = m_sInjuryDesc

End Property

Public Property Let InjuryDesc(ByVal sInjuryDesc As String)

    m_sInjuryDesc = sInjuryDesc

End Property

Public Property Get RecordAsInjury() As Long

    RecordAsInjury = m_lRecordAsInjury

End Property

Public Property Let RecordAsInjury(ByVal lRecordAsInjury As Long)

    m_lRecordAsInjury = lRecordAsInjury

End Property

Public Property Get RecordAsMuscSkel() As Long

    RecordAsMuscSkel = m_lRecordAsMuscSkel

End Property

Public Property Let RecordAsMuscSkel(ByVal lRecordAsMuscSkel As Long)

    m_lRecordAsMuscSkel = lRecordAsMuscSkel

End Property

Public Property Get RecordAsSkinDisor() As Long

    RecordAsSkinDisor = m_lRecordAsSkinDisor

End Property

Public Property Let RecordAsSkinDisor(ByVal lRecordAsSkinDisor As Long)

    m_lRecordAsSkinDisor = lRecordAsSkinDisor

End Property

Public Property Get RecordAsRespCond() As Long

    RecordAsRespCond = m_lRecordAsRespCond

End Property

Public Property Let RecordAsRespCond(ByVal lRecordAsRespCond As Long)

    m_lRecordAsRespCond = lRecordAsRespCond

End Property

Public Property Get RecordAsHearLoss() As Long

    RecordAsHearLoss = m_lRecordAsHearLoss

End Property

Public Property Let RecordAsHearLoss(ByVal lRecordAsHearLoss As Long)

    m_lRecordAsHearLoss = lRecordAsHearLoss

End Property

Public Property Get RecordAsOther() As Long

    RecordAsOther = m_lRecordAsOther

End Property

Public Property Let RecordAsOther(ByVal lRecordAsOther As Long)

    m_lRecordAsOther = lRecordAsOther

End Property

Public Property Get ChkBoxDeath() As String

    ChkBoxDeath = m_sChkBoxDeath

End Property

Public Property Let ChkBoxDeath(ByVal sChkBoxDeath As String)

    m_sChkBoxDeath = sChkBoxDeath

End Property

Public Property Get ChkBoxRestriction() As String

    ChkBoxRestriction = m_sChkBoxRestriction

End Property

Public Property Let ChkBoxRestriction(ByVal sChkBoxRestriction As String)

    m_sChkBoxRestriction = sChkBoxRestriction

End Property

Public Property Get ChkBoxWorkLoss() As String

    ChkBoxWorkLoss = m_sChkBoxWorkLoss

End Property

Public Property Let ChkBoxWorkLoss(ByVal sChkBoxWorkLoss As String)

    m_sChkBoxWorkLoss = sChkBoxWorkLoss

End Property

Public Property Get ChkBoxOtherRecord() As String

    ChkBoxOtherRecord = m_sChkBoxOtherRecord

End Property

Public Property Let ChkBoxOtherRecord(ByVal sChkBoxOtherRecord As String)

    m_sChkBoxOtherRecord = sChkBoxOtherRecord

End Property

Public Property Get DaysWorkLoss() As Long

    DaysWorkLoss = m_lDaysWorkLoss

End Property

Public Property Let DaysWorkLoss(ByVal lDaysWorkLoss As Long)

    m_lDaysWorkLoss = lDaysWorkLoss

End Property

Public Property Get DaysRestriction() As Long

    DaysRestriction = m_lDaysRestriction

End Property

Public Property Let DaysRestriction(ByVal lDaysRestriction As Long)

    m_lDaysRestriction = lDaysRestriction

End Property

Public Property Get RecordAsPoison() As Long

    RecordAsPoison = m_lRecordAsPoison

End Property

Public Property Let RecordAsPoison(ByVal lRecordAsPoison As Long)

    m_lRecordAsPoison = lRecordAsPoison

End Property

Public Property Get PrivacyCase() As Long

    PrivacyCase = m_lPrivacyCase

End Property

Public Property Let PrivacyCase(ByVal lPrivacyCase As Long)

    m_lPrivacyCase = lPrivacyCase

End Property

Public Property Get DateOfDeath() As String

    DateOfDeath = m_sDateOfDeath

End Property

Public Property Let DateOfDeath(ByVal sDateOfDeath As String)

    m_sDateOfDeath = sDateOfDeath

End Property

Public Property Get DateOfHire() As String

    DateOfHire = m_sDateOfHire

End Property

Public Property Let DateOfHire(ByVal sDateOfHire As String)

    m_sDateOfHire = sDateOfHire

End Property

Public Property Get EmployeeStartTime() As String

    EmployeeStartTime = m_sEmployeeStartTime

End Property

Public Property Let EmployeeStartTime(ByVal sEmployeeStartTime As String)

    m_sEmployeeStartTime = sEmployeeStartTime

End Property

Public Property Get EmployeeAddress() As String

    EmployeeAddress = m_sEmployeeAddress

End Property

Public Property Let EmployeeAddress(ByVal sEmployeeAddress As String)

    m_sEmployeeAddress = sEmployeeAddress

End Property

Public Property Get EmployeeCity() As String

    EmployeeCity = m_sEmployeeCity

End Property

Public Property Let EmployeeCity(ByVal sEmployeeCity As String)

    m_sEmployeeCity = sEmployeeCity

End Property

Public Property Get EmployeeState() As String

    EmployeeState = m_sEmployeeState

End Property

Public Property Let EmployeeState(ByVal sEmployeeState As String)

    m_sEmployeeState = sEmployeeState

End Property

Public Property Get EmployeeZipCode() As String

    EmployeeZipCode = m_sEmployeeZipCode

End Property

Public Property Let EmployeeZipCode(ByVal sEmployeeZipCode As String)

    m_sEmployeeZipCode = sEmployeeZipCode

End Property

Public Property Get EmployeeDateOfBirth() As String

    EmployeeDateOfBirth = m_sEmployeeDateOfBirth

End Property

Public Property Let EmployeeDateOfBirth(ByVal sEmployeeDateOfBirth As String)

    m_sEmployeeDateOfBirth = sEmployeeDateOfBirth

End Property

Public Property Get EmployeeSex() As String

    EmployeeSex = m_sEmployeeSex

End Property

Public Property Let EmployeeSex(ByVal sEmployeeSex As String)

    m_sEmployeeSex = sEmployeeSex

End Property


Public Property Get EmployeeOccupation() As String

    EmployeeOccupation = m_sEmployeeOccupation

End Property

Public Property Let EmployeeOccupation(ByVal sEmployeeOccupation As String)

    m_sEmployeeOccupation = sEmployeeOccupation

End Property

Public Property Get ActivityWhenInjured() As String

    ActivityWhenInjured = m_sActivityWhenInjured

End Property

Public Property Let ActivityWhenInjured(ByVal sActivityWhenInjured As String)

    m_sActivityWhenInjured = sActivityWhenInjured

End Property

Public Property Get DisabilityCode() As String

    DisabilityCode = m_sDisabilityCode

End Property

Public Property Let DisabilityCode(ByVal sDisabilityCode As String)

    m_sDisabilityCode = sDisabilityCode

End Property

Public Property Get PiRowId() As String

    PiRowId = m_sPIRowId

End Property

Public Property Let PiRowId(ByVal sPIRowId As String)

    m_sPIRowId = sPIRowId

End Property

Public Property Get IllnessDesc() As String

    IllnessDesc = m_sIllnessDesc

End Property

Public Property Let IllnessDesc(ByVal sIllnessDesc As String)

    m_sIllnessDesc = sIllnessDesc

End Property

Public Property Get ObjectSubstanceThatInjured() As String

    ObjectSubstanceThatInjured = m_sObjectSubstanceThatInjured

End Property

Public Property Let ObjectSubstanceThatInjured(ByVal sObjectSubstanceThatInjured As String)

    m_sObjectSubstanceThatInjured = sObjectSubstanceThatInjured

End Property

Public Property Get HowEventOccurred() As String

    HowEventOccurred = m_sHowEventOccurred

End Property

Public Property Let HowEventOccurred(ByVal sHowEventOccurred As String)

    m_sHowEventOccurred = sHowEventOccurred

End Property


Public Property Get SharpsObjectId() As Long

    SharpsObjectId = m_lSharpsObjectId

End Property

Public Property Let SharpsObjectId(ByVal lSharpsObjectId As Long)

    m_lSharpsObjectId = lSharpsObjectId

End Property

Public Property Get SharpsMakeId() As Long

    SharpsMakeId = m_lSharpsMakeId

End Property

Public Property Let SharpsMakeId(ByVal lSharpsMakeId As Long)

    m_lSharpsMakeId = lSharpsMakeId

End Property

Public Property Get HealthCareEntityFacility() As String

    HealthCareEntityFacility = m_sHealthCareEntityFacility

End Property

Public Property Let HealthCareEntityFacility(ByVal sHealthCareEntityFacility As String)

    m_sHealthCareEntityFacility = sHealthCareEntityFacility

End Property

Public Property Get HealthCareEntityAddress() As String

    HealthCareEntityAddress = m_sHealthCareEntityAddress

End Property

Public Property Let HealthCareEntityAddress(ByVal sHealthCareEntityAddress As String)

    m_sHealthCareEntityAddress = sHealthCareEntityAddress

End Property

Public Property Get HealthCareEntityCity() As String

    HealthCareEntityCity = m_sHealthCareEntityCity

End Property

Public Property Let HealthCareEntityCity(ByVal sHealthCareEntityCity As String)

    m_sHealthCareEntityCity = sHealthCareEntityCity

End Property

Public Property Get HealthCareEntityState() As String

    HealthCareEntityState = m_sHealthCareEntityState

End Property

Public Property Let HealthCareEntityState(ByVal sHealthCareEntityState As String)

    m_sHealthCareEntityState = sHealthCareEntityState

End Property

Public Property Get HealthCareEntityZipCode() As String

    HealthCareEntityZipCode = m_sHealthCareEntityZipCode

End Property

Public Property Let HealthCareEntityZipCode(ByVal sHealthCareEntityZipCode As String)

    m_sHealthCareEntityZipCode = sHealthCareEntityZipCode

End Property

Public Property Get DaysOfWeek() As Integer()
    DaysOfWeek = m_iDaysOfWeek()
End Property

Public Property Let DaysOfWeek(iDaysOfWeek() As Integer)
    m_iDaysOfWeek = iDaysOfWeek()
End Property

Public Property Get HealthCareProf() As String

    HealthCareProf = m_sHealthCareProf

End Property

Public Property Let HealthCareProf(ByVal sHealthCareProf As String)

    m_sHealthCareProf = sHealthCareProf

End Property

Public Property Get EmergencyRoomTreatment() As Boolean

    EmergencyRoomTreatment = m_bEmergencyRoomTreatment

End Property

Public Property Let EmergencyRoomTreatment(ByVal bEmergencyRoomTreatment As Boolean)

    m_bEmergencyRoomTreatment = bEmergencyRoomTreatment

End Property

Public Property Get HospitalizedOverNight() As Boolean

    HospitalizedOverNight = m_bHospitalizedOverNight

End Property

Public Property Let HospitalizedOverNight(ByVal bHospitalizedOverNight As Boolean)

    m_bHospitalizedOverNight = bHospitalizedOverNight

End Property

Public Property Get EventId() As Long

    EventId = m_lEventId

End Property

Public Property Let EventId(ByVal lEventId As Long)

    m_lEventId = lEventId

End Property

Public Property Get SortOn() As String

    SortOn = m_sSortOn

End Property

Public Property Let SortOn(ByVal sSortOn As String)

    m_sSortOn = sSortOn

End Property
