VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsZIPUtil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Win32 File Functions
Const GENERIC_WRITE = &H40000000
Const GENERIC_READ = &H80000000
Const FILE_ATTRIBUTE_NORMAL = &H80
Const CREATE_ALWAYS = 2
Const OPEN_ALWAYS = 4
Const INVALID_HANDLE_VALUE = -1

Private Declare Function ReadFile Lib "kernel32" (ByVal hFile As Long, _
   ByVal lpBuffer As String, ByVal nNumberOfBytesToRead As Long, _
   lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long

Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Declare Function CreateFile Lib "kernel32" _
  Alias "CreateFileA" (ByVal lpFileName As String, _
  ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, _
  ByVal lpSecurityAttributes As Long, _
  ByVal dwCreationDisposition As Long, _
  ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) _
  As Long

Private Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Private Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" (ByVal lpszPath As String, ByVal lpPrefixString As String, ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

Private Function sGetTempPath() As String
   Dim sTmpPath As String * 512
   Dim sTmpName As String * 576
   Dim nRet As Long

   nRet = GetTempPath(512, sTmpPath)
   If (nRet > 0 And nRet < 512) Then
         sGetTempPath = Left$(sTmpPath, InStr(sTmpPath, vbNullChar) - 1)
   
         If Right$(sGetTempPath, 1) <> "\" Then sGetTempPath = sGetTempPath & "\"
   End If
End Function

Private Function sGetTempFilename() As String
    Dim szTmpName As String * 576
    Dim sTmp As String
    
    GetTempFileName sGetTempPath(), "OA", 0, szTmpName
    sTmp = Left$(szTmpName, InStr(szTmpName, vbNullChar) - 1)

    sGetTempFilename = sTmp
End Function

Private Function ConvertValueToHexStr(ValIn As Long, digits As Integer) As String
Dim s As String
Dim i As Long
Dim Val As Long
Dim h As String

h = Hex(ValIn)
s = ""
While (Len(s) + Len(h)) < digits
  s = s & "0"
Wend
s = s & h
ConvertValueToHexStr = s
End Function
Private Function EncodeBinaryString(aBuffer() As Byte) As String
Dim s As String
Dim i As Long
Dim j As Long
Dim c1 As Integer
Dim l1 As Long
Dim sLookup As String

  sLookup = "000102030405060708090A0B0C0D0E0F"
  sLookup = sLookup & "101112131415161718191A1B1C1D1E1F"
  sLookup = sLookup & "202122232425262728292A2B2C2D2E2F"
  sLookup = sLookup & "303132333435363738393A3B3C3D3E3F"
  sLookup = sLookup & "404142434445464748494A4B4C4D4E4F"
  sLookup = sLookup & "505152535455565758595A5B5C5D5E5F"
  sLookup = sLookup & "606162636465666768696A6B6C6D6E6F"
  sLookup = sLookup & "707172737475767778797A7B7C7D7E7F"
  sLookup = sLookup & "808182838485868788898A8B8C8D8E8F"
  sLookup = sLookup & "909192939495969798999A9B9C9D9E9F"
  sLookup = sLookup & "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
  sLookup = sLookup & "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
  sLookup = sLookup & "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
  sLookup = sLookup & "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
  sLookup = sLookup & "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
  sLookup = sLookup & "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"
  
  i = LBound(aBuffer)
  j = 1
  l1 = UBound(aBuffer)
  s = Space((l1 - i + 1) * 2)
  While i <= l1
    'get next BYTE
    c1 = 2 * aBuffer(i)
    'Lookup Ascii Hex equivalent and append to string
    Mid$(s, j, 2) = Mid$(sLookup, c1 + 1, 2)
    j = j + 2
    i = i + 1
  Wend
  
EncodeBinaryString = s

End Function
Private Sub DecodeBinaryString(str1 As String, aBuffer() As Byte)
Dim s As String
Dim i As Long
Dim j As Long
Dim c1 As Integer
Dim c2 As Integer
Dim hex9 As Integer
Dim hexA As Integer
Dim hex0 As Integer
Dim l1 As Long
  
  ReDim aBuffer(1 To Len(str1) / 2) As Byte
  
  hex9 = Asc("9")
  hexA = Asc("A")
  hex0 = Asc("0")
  i = 1
  j = 1
  l1 = Len(str1)
  s = Space(l1 / 2)
  While i < l1
    ' An encoded BYTE is formaed by two ASCII Hex values
    ' for example "56" means the MS part is 5 and the LS part is 6
    ' Get MS part of this Encoded Character
    c2 = Asc(Mid$(str1, i, 1))
    ' Decode the MS part
    If (c2 > hex9) Then
      c2 = c2 - hexA + 10
    Else
      c2 = c2 - hex0
    End If
    ' Get LS part of this Encoded Character
    i = i + 1
    c1 = Asc(Mid$(str1, i, 1))
    ' Decode the LS part
    If (c1 > hex9) Then
      c1 = c1 - hexA + 10
    Else
      c1 = c1 - hex0
    End If
    ' Put the Decoded MS and LS parts together into one BINARY value
    i = i + 1
    
    aBuffer(j) = (c2 * 16) + c1
    
    j = j + 1
  Wend
End Sub

Public Function ZIPFile(sFileName As String) As String
    Dim aBuffer() As Byte
    Dim lTotalBytes As Long
    Dim xComp As New XceedCompression
    Dim vaDest As Variant
    Dim aDest() As Byte
    Dim xErr As xcdCompressionError
    Dim i As Long
    
    ' We set properties that will affect the way we compress
    ' xComp.EncryptionPassword = "a"    ' "cscriskmasterdtg"
    xComp.CompressionLevel = xclHigh
    
    ' Read file into byte array in memory
    Open sFileName For Binary As #5
    
    lTotalBytes = LOF(5)
    ReDim aBuffer(1 To lTotalBytes) As Byte
    
    Get 5, , aBuffer
    
    Close #5
    
    ' Compress the buffer
    
    xErr = xComp.Compress(aBuffer, vaDest, True)
    
    If xErr <> xceSuccess Then
        Exit Function
    Else
        aDest = vaDest
  
        ZIPFile = EncodeBinaryString(aDest)
    End If

    
End Function

Public Sub UnZIPBuffer(sStr As String, aBuffer() As Byte)
    Dim lError As Long
    Dim lTotalBytes As Long
    Dim xComp As New XceedCompression
    Dim vaDest As Variant
    Dim aDest() As Byte
    Dim xErr As xcdCompressionError
    Dim i As Long
    
    ' We set properties that will affect the way we compress
    ' xComp.EncryptionPassword = "cscriskmasterdtg"
    ' xComp.EncryptionPassword = "a"
    
    ' Compress the buffer
    DecodeBinaryString sStr, aDest
    
    xErr = xComp.Uncompress(aDest, vaDest, True)
    
    
    
    If xErr <> xceSuccess Then
        Exit Sub
    Else
        aBuffer = vaDest
    End If

End Sub

Public Sub UnZIPBufferToFile(sStr As String, sOutFilename As String)
    Dim aImage() As Byte

    UnZIPBuffer sStr, aImage
    
    Open sOutFilename For Binary Access Write As #5
    
    Put 5, , aImage
    
    Close #5
End Sub

Public Function UnZIPBufferToTempFile(sStr As String) As String
    Dim aImage() As Byte
    Dim sOutName As String
    
    UnZIPBuffer sStr, aImage
    
    sOutName = sGetTempFilename()
    
    Open sOutName For Binary Access Write As #5
    
    Put 5, , aImage
    
    Close #5
    
    UnZIPBufferToTempFile = sOutName
End Function

