Attribute VB_Name = "Declarations"
Option Explicit

Global Const ERR_CONN_IN_USE = 29999
Global Const ERR_ABORT_REQUESTED = 39999
Global Const DBMS_IS_ACCESS = 0
Global Const DBMS_IS_SQLSRVR = 1
Global Const DBMS_IS_SYBASE = 2
Global Const DBMS_IS_INFORMIX = 3
Global Const DBMS_IS_ODBC = 4
Global Const WORK_LOSS_RULE_CHANGE_DATE As String = "20011231"
'*****************************
' Note could more simply use the RegTool reference but
' will stay with the existing code...
'*****************************
Global Const REG_SZ = 1
Global Const KEY_LENGTH = 128
Global Const REG_ROOT_KEY = "SOFTWARE\DTG"
Global Const REG_SUCCESS = 0&
Global Const REG_BADKEY = 2&
Global Const HKEY_LOCAL_MACHINE = &H80000002
Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) As Long
Declare Function RegQueryValue Lib "advapi32.dll" Alias "RegQueryValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal lpValue As String, lpcbValue As Long) As Long
Declare Function RegSetValue Lib "advapi32.dll" Alias "RegSetValueA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal dwType As Long, ByVal lpData As String, ByVal cbData As Long) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal ByteLen As Long)

Type EscapeStructure
  EscValue As Long
  Pad As String * 16
End Type

Enum SortOrder
    SORT_EVENTDATE
    SORT_EVENTNUMBER
    SORT_PERSONNAME
End Enum

Enum ColumnESource
    SRC_E_PRIMARY_LOCATION
    SRC_E_LOCATION_DESCRIPTION
    SRC_E_DEPARTMENT_ADDRESS
End Enum

Enum ColumnFSource
    SRC_DEFAULT
    SRC_EVENTDESC
    SRC_OSHAHOWACC
    SRC_OSHAACCDESC
End Enum


'Internal Application Objects
Public g_objRocket As DTGRocket
Public g_hEnv As Long
Public g_fso As scripting.FileSystemObject
Public g_objBroker As CBroker
Public g_objErr As CError
'Safeway Atul - Global variable which hold the Privacy Flag value
Public g_bPrivacyCaseFlag As String
#If BB_DUMP_SQL Then
Public f As scripting.TextStream
#End If

Public Sub DebugMsg(s As String)
#If BB_DEBUG Then 'write it out to the application event log
    DebugLogError "Debug Message", Erl, 0, "", s
#End If
End Sub
'BSB 02.19.2003 Needed this near duplicate becuase debug error logging was
' clearing the bubble flag on the error object.
Public Sub DebugLogError(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String, Optional bBubbleErr As Boolean = False)
On Error GoTo hError
Dim iFile As Integer
Dim sFileName As String

iFile = FreeFile
sFileName = App.Path
If Right$(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
sFileName = sFileName & "error.log"

Open sFileName For Append Shared As #iFile
Print #iFile, """" & Format$(Now, "yyyymmddhhnnss") & """," & """" & sProcedure & """," & """" & ErrLine & """," & """" & ErrNumber & """," & """" & ErrSource & """," & """" & ErrDescription & ""","
Close #iFile
iFile = 0

hExit:
   On Error Resume Next
   If iFile <> 0 Then Close iFile
   On Error GoTo 0
   Exit Sub

hError:
   GoTo hExit
End Sub



'Treat MultiValued "values" as csv strings.
Public Function GetXMLValue(sName As String, objXML As DOMDocument) As String
    On Error Resume Next
    Dim objNode As IXMLDOMElement
    Dim objChild As IXMLDOMElement
    Set objNode = objXML.selectSingleNode("//control[@name='" & sName & "']")
    Select Case objNode.getAttribute("type")
        Case "radio"
            Set objNode = objXML.selectSingleNode("//control[@name='" & sName & "' && @checked]")
            GetXMLValue = objNode.getAttribute("value")
        Case "date"
            GetXMLValue = objNode.getAttribute("value")
            If IsDate(GetXMLValue) Then
                GetXMLValue = Format$(GetXMLValue, "yyyymmdd")
            ElseIf Not IsDate(sDBDateFormat(GetXMLValue, "Short Date")) Then
            'Elseif IsDate(sDBDAteformat(Left(Getxmlvalue,4) & "\" & mid(getxmlvalue,5,2) & "\" & right(getxmlvalue,2),"Short Date")
                GetXMLValue = ""
            End If
        Case "combobox"
            GetXMLValue = objNode.getAttribute("codeid")
        Case "multiclaimnumberlookup"
            For Each objChild In objNode.childNodes
                AddDelimited GetXMLValue, CStr(objChild.getAttribute("value"))
            Next
        Case "multieventnumberlookup"
            For Each objChild In objNode.childNodes
                AddDelimited GetXMLValue, CStr(objChild.getAttribute("value"))
            Next
        Case "multiorgh"
            For Each objChild In objNode.childNodes
                AddDelimited GetXMLValue, CStr(objChild.getAttribute("value"))
            Next
        Case "numeric"
            GetXMLValue = objNode.getAttribute("value")
        Case "checkbox"
            Set objNode = Nothing
            Set objNode = objXML.selectSingleNode("//control[@name='" & sName & "' && @checked]")
            GetXMLValue = IIf(Not objNode Is Nothing, "-1", "0")
    End Select
End Function

'Note: This function runs when the DLL is first loaded.
Sub Main()
    On Error GoTo hErr
    DebugMsg "Begin Main()"

    Set g_objErr = New CError
    Set g_fso = New FileSystemObject
    Set g_objRocket = New DTGRocket
    Set g_objBroker = New CBroker
    g_hEnv = g_objRocket.DB_InitEnvironment()
   
hExit:
    DebugMsg "Exit Main()"
    Exit Sub
hErr:
    LogError "Declarations.Main", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Sub

Public Sub AbortRequested(src As String)
    LogError "Declarations.AbortRequested", Erl, ERR_ABORT_REQUESTED, src, "Abort Requested", True
    g_objErr.Bubble
End Sub
'*********************************
' The safe GETDATA functions.
'*********************************
Public Function GetDataBool(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As Boolean
Dim v As Variant
GetDataBool = False

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

If "" & v <> "" Then
   On Error Resume Next
   If Val(v) <> 0 Then GetDataBool = True
   On Error GoTo 0
End If

End Function

Public Function GetDataLong(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As Long
Dim v As Variant
GetDataLong = 0

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

If "" & v <> "" Then
   On Error Resume Next
   GetDataLong = CLng(v)
   On Error GoTo 0
End If

End Function


Public Function GetDataInteger(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As Integer
Dim v As Variant
GetDataInteger = 0

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

If "" & v <> "" Then
   On Error Resume Next
   GetDataInteger = CInt(v)
   On Error GoTo 0
End If

End Function
Public Function GetDataDouble(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As Double
Dim v As Variant
GetDataDouble = 0

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

If "" & v <> "" Then
   On Error Resume Next
   GetDataDouble = CDbl(v)
   On Error GoTo 0
End If

End Function
Public Function GetDataSingle(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As Single
Dim v As Variant
GetDataSingle = 0

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

If "" & v <> "" Then
   On Error Resume Next
   GetDataSingle = CSng(v)
   On Error GoTo 0
End If

End Function
Public Function GetDataString(ByVal iRS As Integer, ByVal vFieldNameIdx As Variant) As String
Dim v As Variant

g_objRocket.DB_GetData iRS, vFieldNameIdx, v

GetDataString = "" & v

End Function


Public Sub SafeCloseRecordset(ByRef iRS As Integer)
    DebugMsg "Closing Recordset: " & iRS
    If iRS <> 0 Then
       On Error Resume Next
       g_objRocket.DB_CloseRecordset iRS, DB_DROP
       On Error GoTo 0
    End If
End Sub
Public Sub SafeCloseConnection(ByRef hDb As Integer)
If hDb <> 0 Then
   On Error Resume Next
    g_objRocket.DB_CloseDatabase hDb
   On Error GoTo 0
End If
End Sub

Public Function CloseApp()
    DebugMsg "Begin CloseApp()"

    On Error Resume Next
    g_objRocket.DB_FreeEnvironment g_hEnv
    Set g_fso = Nothing
    Set g_objBroker = Nothing
    Set g_objRocket = Nothing
    DebugMsg "End CloseApp()"

End Function
    
Public Function IsEmptyVariantArray(vArray)
        On Error Resume Next
        Dim trash
        trash = UBound(vArray)
        If Err.Number <> 0 Then
            IsEmptyVariantArray = True
        End If
End Function

Public Function ServerUniqueFileName() As String
   On Error GoTo GetServerUniqueFileName_Error
    ServerUniqueFileName = Replace(g_fso.GetTempName(), ".tmp", ".rtf")
GetServerUniqueFileName_Exit:
   Exit Function

GetServerUniqueFileName_Error:
    LogError "GetServerUniqueFileName", Erl, Err.Number, Err.Source, Err.Description
    GoTo GetServerUniqueFileName_Exit
End Function

Sub AddUnique(sArray() As String, sItem As String)
   Dim ub As Integer
   Dim i As Integer

   ub = UBound(sArray)
   For i = 1 To ub
      If sArray(i) = sItem Then
          Exit Sub
      End If
   Next i

   ' add item if we get to this point
   ReDim Preserve sArray(ub + 1)
   sArray(ub + 1) = sItem
End Sub

Function ExistsInArray(sArray() As String, sItem As String) As Integer
   Dim i As Integer
   For i = LBound(sArray) To UBound(sArray)
      If sArray(i) = sItem Then
          ExistsInArray = True
          Exit Function
      End If
   Next i
End Function
Function IndexOf(sArray() As String, sItem As String) As Integer
   Dim i As Integer
   
   For i = LBound(sArray) To UBound(sArray)
      If sArray(i) = sItem Then
          IndexOf = i
          Exit Function
      End If
   Next i
   IndexOf = 0
End Function
' FUNCTION: sSQLTextArg
' PURPOSE : Process varchar arguments for use directly in SQL statements.
'           Quotes the string if not empty. Double quotes any single quotes so they will work.
'           Returns the string NULL if string is empty.
Function sSQLTextArg(sArg As String) As String
   Dim sTmp As String

   If sArg = "" Then
      sTmp = "NULL"
   Else
         sTmp = "'" & Replace(sArg, "'", "''") & "'"
   End If
   sSQLTextArg = sTmp
End Function
Function sDBDateFormat(sDate As String, sFormat As String) As String
    On Error GoTo sdbDateFormat_Error
    Dim sTmp As String

    sDBDateFormat = ""
    sTmp = Trim$(sDate)
    If sTmp = "" Then Exit Function ' Added by Denis Basaric 11/04/1997
    If Len(sTmp) < 8 Then sTmp = String$(8 - Len(sTmp), "0") & sTmp
    If Len(sTmp) > 8 Then sTmp = Left$(sTmp, 8)
    sTmp = DateSerial(Val(Left(sTmp, 4)), Val(Mid(sTmp, 5, 2)), Val(Mid(sTmp, 7, 2)))
    sDBDateFormat = Format(sTmp, sFormat)  ' Changed by Denis Basaric 10/31/97
    
sdbDateFormat_Exit:
    Exit Function
    
sdbDateFormat_Error:
    LogError "sdbDateFormat", Erl, Err.Number, Err.Source, Err.Description
    Resume sdbDateFormat_Exit
End Function

Function sDBTimeFormat(sTime As String, sFormat As String) As String
    Dim sTmp As String
    sDBTimeFormat = ""
    If Trim(sTime) <> "" Then
        sTmp = Left(sTime, 2) & ":" & Mid(sTime, 3, 2) & ":" & Mid(sTime, 5, 2)
        sDBTimeFormat = Format(sTmp, sFormat)
    End If

End Function
Function sDBDTTMFormat(sDTTM As String, sDateFormat As String, sTimeFormat As String) As String
    Dim sTmp As String
    Dim sDateTmp As String
    sTmp = Mid(sDTTM, 5, 2) & "-" & Mid(sDTTM, 7, 2) & "-" & Left(sDTTM, 4)
    sDateTmp = Format(sTmp, sDateFormat)
    'mjh 7/25/96  I can not believe that this has never worked and no one has reported that
    'their time is always related to the year.  before it waould create time off of the year
    sTmp = Mid(sDTTM, 9, 2) & ":" & Mid(sDTTM, 11, 2) & ":" & Mid(sDTTM, 13, 2)
    sDateTmp = sDateTmp & " " & Format(sTmp, sTimeFormat)

    sDBDTTMFormat = sDateTmp

End Function
'BSB 01.15.2003 Fix for 12 Noon.
Public Function f_sGetTimeHHMM_AM_PM(ByVal s As String) As String
Dim iPart1 As Integer
Dim iPart2 As Integer
Dim sAMPM As String
On Error GoTo hError
    f_sGetTimeHHMM_AM_PM = ""
    DebugMsg "f_sGetTimeHHMM_AM_PM param s:" & s
  If Trim$(s & "") = "" Then Exit Function
    If Len(s) < 4 Then Exit Function
    If Not IsDate(Mid$(s, 1, 2) & ":" & Mid$(s, 3, 2)) Then Exit Function
    sAMPM = ""
    iPart1 = CInt(Mid$(s, 1, 2))
    iPart2 = CInt(Mid$(s, 3, 2))
    If iPart1 = 12 Then '12 Noon (PM but no hour adjustment.)
        sAMPM = "PM"
    ElseIf iPart1 > 12 Then 'Afternoon (PM with hour adjustment.)
        iPart1 = iPart1 - 12
        sAMPM = "PM"
    Else
        sAMPM = "AM" 'Morning (AM no hour adjustment)
    End If
    s = Right$("00" & Trim$(CStr(iPart1)), 2) & ":" & Right$("00" & Trim$(CStr(iPart2)), 2) & " " & sAMPM
    If s = "00:00 AM" Then s = "12:00 AM"
    f_sGetTimeHHMM_AM_PM = s
    DebugMsg "f_sGetTimeHHMM_AM_PM result:" & f_sGetTimeHHMM_AM_PM
hExit:
    Exit Function
hError:
    GoTo hExit
End Function

Function AddSep(s, Optional Sep = ",") As String
    AddSep = IIf(s <> "", s & Sep, s)
End Function

Function Append(s) As String
    Append = IIf(s <> "", " ," & s, " " & s)
End Function
Sub AddDelimited(ByRef sOriginal As String, sToAppend As String, Optional Sep As String = ",")
    sOriginal = AddSep(sOriginal, Sep) & sToAppend
End Sub
Public Function sOrgLevelToEIDColumn(lReportLevel As Long) As String
    
    Select Case lReportLevel
            Case 1005
                sOrgLevelToEIDColumn = "CLIENT_EID"
            Case 1006
                sOrgLevelToEIDColumn = "COMPANY_EID"
            Case 1007
                sOrgLevelToEIDColumn = "OPERATION_EID"
            Case 1008
                sOrgLevelToEIDColumn = "REGION_EID"
            Case 1009
                sOrgLevelToEIDColumn = "DIVISION_EID"
            Case 1010
                sOrgLevelToEIDColumn = "LOCATION_EID"
            Case 1011
                sOrgLevelToEIDColumn = "FACILITY_EID"
            Case 1012
                sOrgLevelToEIDColumn = "DEPARTMENT_EID"
        End Select
  
End Function
Public Function IsChecked(objNode As IXMLDOMNode) As Boolean
    IsChecked = Not (objNode.Attributes.getNamedItem("checked") Is Nothing)
End Function

