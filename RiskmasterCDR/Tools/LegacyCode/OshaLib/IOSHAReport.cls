VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IOSHAReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'Completely Public Properties
Public UserFileName As String
Public StartTime As Date
Public BeginDate As String
Public DriveByEvent As Boolean                          'MITS 21930  jtodd22
Public EndDate As String
Public EventBasedFlag As Boolean
Public PrimaryLocationFlag As Boolean
Public PrintOSHADescFlag As Boolean
Public AllEntitiesFlag As Boolean
Public UseReportLevel As Long
Public ReportLevel As Long
Public ReportOn As Long
Public YearOfReport As String
'Added Functionality 07.01.2002 BB
Public Enforce180DayRule As Boolean
Public AsOfDate As String
Public ItemSortOrder As Long
Public ColumnESource As Long
Public ColumnFSource As Long
Public EstablishmentNamePrefix As String
Public EstablishmentNamePrefixOrgLevel As Long          'MITS 21727
Public RetainPageTotals As Boolean
' MITS 9101 Manish C Jha 08/21/2008
Public LocDescription As Boolean

'jlt 09/17/2003
Public ByOSHAEstablishmentFlag As Boolean

''Read Only Properties
Public Property Get Errors() As CErrors
End Property
Public Property Get Company() As CEntityItem
End Property
Public Property Get Notify() As IWorkerNotifications
End Property
Public Property Set Notify(obj As IWorkerNotifications)
End Property
Public Property Get Context() As CInstanceUtilities
End Property
Public Property Get PITypeEmployee() As Long
End Property
Public Property Get sUseReportLevel() As String
End Property
Public Property Get sReportLevel() As String
End Property
Public Property Get Items() As Collection
End Property
'Read - Write Object Properties
Public Property Get CriteriaXmlDom() As DOMDocument
End Property
Public Property Set CriteriaXmlDom(objDoc As DOMDocument)
End Property
Public Property Get SelectedEntities() As Collection
End Property
Public Property Set SelectedEntities(col As Collection)
End Property

' PUBLIC METHODS
'- Both create the report in the file specified thru UserFileName
'Return value arbitrary.
Public Function ResultEMF() As String
End Function
'End Function
Public Function ResultPDF() As String
End Function
Public Sub LogClassError(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String, Optional bBubbleErr As Boolean = False)
End Sub

