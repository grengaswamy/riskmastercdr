VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDispatchIReportEngine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IReportEngine
'Hack to allow Dispatch access to secondary interfaces of the COSHAManager object.
Private m_objInner As IReportEngine
Private pInner As Long
Public Property Get InnerObject() As IReportEngine
        If pInner = 0 Then Exit Property
        CopyMemory m_objInner, pInner, 4
        Set InnerObject = m_objInner
        CopyMemory m_objInner, 0&, 4
End Property

'BSB Cut the circular reference back to "Parent" object - using a "soft reference".
Public Property Set InnerObject(obj As IReportEngine)
    If obj Is Nothing Then Exit Property
    pInner = ObjPtr(obj)
'    Set m_objInner = obj 'Replaced with hack noted above.
End Property
Public Property Get CriteriaHtml() As String
    CriteriaHtml = InnerObject.CriteriaHtml
End Property

Public Property Set CriteriaXmlDom(RHS As Variant)
    Set InnerObject.CriteriaXmlDom = RHS
End Property

Public Property Get CriteriaXmlDom() As Variant
    Set CriteriaXmlDom = InnerObject.CriteriaXmlDom
End Property

Public Property Get HtmlCriteriaSupported() As Boolean
    HtmlCriteriaSupported = InnerObject.HtmlCriteriaSupported()
End Property

Public Property Let OutputDirectory(ByVal RHS As String)
    InnerObject.OutputDirectory = RHS
End Property

Public Property Get OutputDirectory() As String
    OutputDirectory = InnerObject.OutputDirectory()
End Property

Public Property Let OutputFormat(ByVal RHS As RMNetIReport.eReportFormat)
    InnerObject.OutputFormat = RHS
End Property

Public Property Get OutputFormat() As RMNetIReport.eReportFormat
    OutputFormat = InnerObject.OutputFormat()
End Property

Public Property Get OutputsSupported() As Long
OutputsSupported = InnerObject.OutputsSupported()
End Property

Public Property Get ReportOutput() As RMNetIReport.CReportFiles
    Set ReportOutput = InnerObject.ReportOutput()
End Property

Public Property Let ReportType(ByVal RHS As Long)
    InnerObject.ReportType = RHS
End Property

'Hack to allow script access to the secondary
'interfaces of a COSHAManager object.
Public Property Get ReportType() As Long
    ReportType = InnerObject.ReportType
End Property

Public Sub RunReport()
    InnerObject.RunReport
End Sub

Public Property Set WorkerNotifications(ByVal RHS As RMNetIReport.IWorkerNotifications)
    Set InnerObject.WorkerNotifications = RHS
End Property

Public Property Get WorkerNotifications() As RMNetIReport.IWorkerNotifications
    Set WorkerNotifications = InnerObject.WorkerNotifications
End Property

Public Property Get XmlCriteriaSupported() As Boolean
    XmlCriteriaSupported = InnerObject.XmlCriteriaSupported
End Property

'HACK TO IMPLEMENT INTERFACE FOR REAL CLIENTS
Private Property Get IReportEngine_CriteriaHtml() As String
    IReportEngine_CriteriaHtml = InnerObject.CriteriaHtml
End Property

Private Property Set IReportEngine_CriteriaXmlDom(ByVal RHS As Object)
    Set InnerObject.CriteriaXmlDom = RHS
End Property

Private Property Get IReportEngine_CriteriaXmlDom() As Object
    Set IReportEngine_CriteriaXmlDom = InnerObject.CriteriaXmlDom
End Property

Private Property Get IReportEngine_HtmlCriteriaSupported() As Boolean
    IReportEngine_HtmlCriteriaSupported = InnerObject.HtmlCriteriaSupported()
End Property

Private Property Let IReportEngine_OutputDirectory(ByVal RHS As String)
    InnerObject.OutputDirectory = RHS
End Property

Private Property Get IReportEngine_OutputDirectory() As String
    IReportEngine_OutputDirectory = InnerObject.OutputDirectory()
End Property

Private Property Let IReportEngine_OutputFormat(ByVal RHS As RMNetIReport.eReportFormat)
    InnerObject.OutputFormat = RHS
End Property

Private Property Get IReportEngine_OutputFormat() As RMNetIReport.eReportFormat
    IReportEngine_OutputFormat = InnerObject.OutputFormat()
End Property

Private Property Get IReportEngine_OutputsSupported() As Long
IReportEngine_OutputsSupported = InnerObject.OutputsSupported()
End Property

Private Property Get IReportEngine_ReportOutput() As RMNetIReport.CReportFiles
    Set IReportEngine_ReportOutput = InnerObject.ReportOutput()
End Property

Private Property Let IReportEngine_ReportType(ByVal RHS As Long)
    InnerObject.ReportType = RHS
End Property

'Hack to allow script access to the secondary
'interfaces of a COSHAManager object.
Private Property Get IReportEngine_ReportType() As Long
    IReportEngine_ReportType = InnerObject.ReportType
End Property

Private Sub IReportEngine_RunReport()
    InnerObject.RunReport
End Sub

Private Property Set IReportEngine_WorkerNotifications(ByVal RHS As RMNetIReport.IWorkerNotifications)
    Set InnerObject.WorkerNotifications = RHS
End Property

Private Property Get IReportEngine_WorkerNotifications() As RMNetIReport.IWorkerNotifications
    Set IReportEngine_WorkerNotifications = InnerObject.WorkerNotifications
End Property

Private Property Get IReportEngine_XmlCriteriaSupported() As Boolean
    IReportEngine_XmlCriteriaSupported = m_objInner.XmlCriteriaSupported
End Property
