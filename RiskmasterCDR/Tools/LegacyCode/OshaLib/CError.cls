VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "Error Object since Microsoft won't let us re-use theirs."
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'---------------------------------------------------------------------------------------
' Module    : CError
' DateTime  : 1/8/2002 11:32
' Author    : bsb
' Purpose   : Simple Error Object since MS won't let us use theirs.
'---------------------------------------------------------------------------------------

Option Explicit
Public Number As Long
Public Description As String
Public Source As String
Public Line As Long
Public BubbleFlag As Boolean
Public Sub Bubble()
DebugMsg "Attempting to Bubble"
    If Me.BubbleFlag Then
      DebugMsg "Bubbling Error"
      Me.Fire
    End If
End Sub
Public Sub Fire()
    Dim num As Long, src As String, desc As String
    num = Me.Number
    src = Me.Source
    desc = Me.Description
    Me.Clear
    On Error GoTo 0
    Err.Raise num, src, desc
End Sub
Public Sub Clear()
With Me
    .Number = 0
    .Source = ""
    .Description = ""
    .BubbleFlag = False
End With
End Sub
