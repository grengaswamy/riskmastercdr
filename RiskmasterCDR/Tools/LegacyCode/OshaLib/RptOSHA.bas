Attribute VB_Name = "RptOSHA"
Option Explicit
Option Base 1

'*****************************************************************
'*
'*  RptOSHA_2001.BAS
'*
'*  built from older rptOSHA*.bas for 2001 requirements
'*
'*  Date Written: 09/2001           By: jlt, Jim Todd,
'*  OO redesign by:                     bsb Brian Battah
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*

Dim colWorkLosses As CWorkLosses                    'new jtodd22 12/03/2004
Dim colRestrictions As CRestrictions                'new jtodd22 12/03/2004
'*****************************************************************

'Fetch all necessary records from the DB.
'Fixed the logic so that this object is only a SINGLE REPORT.
'Inputs:    SharpsOnlyFlag
'   rpt obj (assumes the following properties:)
'            Context
'            sUseReportLevel
'            m_sReportLevel
'            ReportOn
'            BeginDate
'            EndDate
'            AllEntitiesFlag,
'            SelectedEntities,
'            EventBasedFlag
' Note: May in the future formalize this into an "IOhsaReport" interface
' Note: All "parallell queries" MUST BE ORDERED to increase search efficiency.
Function FetchOshaData(rpt As IOSHAReport, lFilterEventID As Long, Optional SharpsOnlyFlag As Boolean = False) As Collection
    Dim bDone As Boolean
    Dim lRetry As Long       ' BSB 02.19.2003 Allow retry for network timeout errors during data retrieval.
    Dim SQL As String
    Dim SQLFrom As String       ' JP 1/30/2003   Support for spliting into full and count(*) queries.
    Dim SQLJoin As String       ' XHU 02/15/2006 join sql statement
    Dim SQLWhere As String      ' JP 1/30/2003   Support for spliting into full and count(*) queries.
    Dim SQLCount As String      ' JP 1/30/2003   Support for spliting into full and count(*) queries.
    Dim hDB_SNAPSHOT As Integer ' used to hold connection for SNAPSHOT use.
    Dim rs As Integer 'holds the one to one data
    Dim rsTreatments As Integer
    Dim rsInjuries As Integer
    Dim rsWorkLosses As Integer
    Dim rsBodyParts As Integer
    Dim rsRestrictions As Integer
    Dim rsPhysician As Integer
    Dim rsHospital As Integer
'    Dim rsIllnesses As Integer 'Only use the Illness code directly on the PI record.
    Dim RecordCount As Long
    Dim ElapsedSeconds As Long
    Dim objItem As COSHAItem
    Dim s As String, sBodyParts As String
    Dim sPrev As String, sCurr As String
    Dim sInDeptClause As String
    Dim sInPeopleInvolvedClause As String
    Dim DaysOfWeek() As Integer

    Dim Items As Collection
    Dim tmpItem As COSHAItem
    
    Dim s180LimitDateCalr As String                     'new jtodd22 12/03/2004
    Dim s180LimitDateDTG As String                      'new jtodd22 12/03/2004
    Dim l As Long                                       'new jtodd22 12/03/2004
    Dim GrossTotalWorkLossDays As Long                                    'new jtodd22 12/03/2004
    Dim sTmp As String                                  'new jtodd22 12/03/2004
    Dim sDateDTG As String                              'new jtodd22 12/03/2004
    Dim i As Long
    Dim iDaysOff As Integer                             'new jtodd22 12/03/2004
    Dim iDaysRestrict As Integer                        'new jtodd22 12/03/2004
    Dim sTestDate As String                             'new jtodd22 06/03/2005
    
    Dim sTimeOfEvent As String                          'new jtodd22 01/15/2008, MITS 6107
    
    Dim lRestrictCount As Long
    Dim lWorkLossCount As Long
    Dim lRestrictPlace As Long
    Dim lWorkLossPlace As Long
    Dim sRestrictDateDTG As String
    Dim sWorkLossDateDTG As String
    Dim l_180DayCount As Long
    
    On Error GoTo hErr
    Set Items = New Collection
    Set objItem = New COSHAItem

    DebugMsg "FetchData Begin"

    If rpt.Notify.CheckAbort Then AbortRequested ("FetchOshaData")
    
    rpt.StartTime = Now
    
    sInDeptClause = BuildDeptClause(rpt)
    
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchOshaData")
    DebugMsg "First FetchData SQL beginning."
   
    '************************************
    ' This SQL Code is best handled outside of VB...
    ' Look for sql script file called "OSHA Data Queries.sql"
    '************************************
    'Query for all one to one data. (or multi-record data that we only want the frst occurence of.)
    ' MITS 9101 Manish C Jha 08/21/2008
    SQL = "SELECT EVENT.EVENT_ID, EVENT.DATE_OF_EVENT, EVENT.TIME_OF_EVENT, EVENT.DEPT_EID,EVENT.LOCATION_AREA_DESC,"
    SQL = SQL & " EVENT.EVENT_NUMBER, EVENT.EVENT_DESCRIPTION,EVENT.PRIMARY_LOC_CODE,"
    SQL = SQL & " EVENT.ADDR1 | EV_ADDR1, EVENT.ADDR2 | EV_ADDR2, EVENT.CITY | EV_CITY,EVENT.STATE_ID"
    SQL = SQL & " | EV_STATE_ID, EVENT.ZIP_CODE | EV_ZIP_CODE, EV_DEPT_ENTITY.LAST_NAME | EV_DEPT_LAST_NAME, "
    SQL = SQL & " EV_DEPT_ENTITY.ADDR1 | EV_DEPT_ADDR1, EV_DEPT_ENTITY.ADDR2 | EV_DEPT_ADDR2, EV_DEPT_ENTITY.CITY "
    SQL = SQL & " | EV_DEPT_CITY, EV_DEPT_ENTITY.STATE_ID | EV_DEPT_STATE_ID, EV_DEPT_ENTITY.ZIP_CODE "
    SQL = SQL & " | EV_DEPT_ZIP_CODE, PI_ENTITY.FIRST_NAME | PI_FIRST_NAME, PI_ENTITY.MIDDLE_NAME "
    SQL = SQL & " | PI_MIDDLE_NAME, PI_ENTITY.LAST_NAME | PI_LAST_NAME,  PI_ENTITY.ADDR1 | PI_ADDR1,"
    SQL = SQL & " PI_ENTITY.ADDR2 | PI_ADDR2, PI_ENTITY.CITY | PI_CITY,  PI_ENTITY.STATE_ID | PI_STATE_ID, "
    SQL = SQL & " PI_ENTITY.ZIP_CODE | PI_ZIP_CODE, PI_ENTITY.BIRTH_DATE | PI_BIRTH_DATE, "
    SQL = SQL & " PI_ENTITY.SEX_CODE | PI_SEX_CODE, PERSON_INVOLVED.DEPT_ASSIGNED_EID, "
    SQL = SQL & " PERSON_INVOLVED.POSITION_CODE, PERSON_INVOLVED.DISABILITY_CODE, "
    SQL = SQL & " PERSON_INVOLVED.PI_ROW_ID, PERSON_INVOLVED.DATE_OF_DEATH,"
    SQL = SQL & " PERSON_INVOLVED.DATE_HIRED, PERSON_INVOLVED.WORKDAY_START_TIME, "
    SQL = SQL & " PERSON_INVOLVED.WORK_SUN_FLAG, PERSON_INVOLVED.WORK_MON_FLAG,  "
    SQL = SQL & " PERSON_INVOLVED.WORK_TUE_FLAG, PERSON_INVOLVED.WORK_WED_FLAG,  "
    SQL = SQL & " PERSON_INVOLVED.WORK_THU_FLAG, PERSON_INVOLVED.WORK_FRI_FLAG,  "
    SQL = SQL & " PERSON_INVOLVED.WORK_SAT_FLAG,"
    SQL = SQL & " PERSON_INVOLVED.OSHA_ACC_DESC , PERSON_INVOLVED.ILLNESS_CODE,"
    SQL = SQL & " EVENT_X_OSHA.*"
        
    'Table Joins
    ' JP 1/30/2003   Support for count(*) query.
    SQLFrom = " FROM EVENT, EVENT_X_OSHA, PERSON_INVOLVED, ENTITY PI_ENTITY, ENTITY EV_DEPT_ENTITY  "

    'Wizard issue 1768 MITS 4835 OSHA for PI based but no record in EVENT_X_OSHA
    'XHU Oracle 8 and lower does not support LEFT OUTER JOIN.
    hDB_SNAPSHOT = g_objRocket.DB_OpenDatabase(g_hEnv, rpt.Context.DSN, 0)
    If rpt.EventBasedFlag Then
        If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_ORACLE Then
        'mjha3 : MITS 13273 : 10/24/2008
        SQLWhere = " WHERE  EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID(+) AND "
        Else
            SQLJoin = " FROM EVENT LEFT OUTER JOIN EVENT_X_OSHA ON EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID "
        End If
    Else
        If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_ORACLE Then
            SQLWhere = " WHERE EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID(+) AND "
        Else
            SQLJoin = " FROM EVENT LEFT OUTER JOIN EVENT_X_OSHA ON EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID "
        End If
    End If
    If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_ORACLE Then
        SQLWhere = SQLWhere & " EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND     "
        SQLWhere = SQLWhere & " PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID AND  "
        SQLWhere = SQLWhere & " EVENT.DEPT_EID = EV_DEPT_ENTITY.ENTITY_ID "
    Else
        SQLJoin = SQLJoin & " INNER JOIN PERSON_INVOLVED ON EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID "
        SQLJoin = SQLJoin & " INNER JOIN ENTITY PI_ENTITY ON PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID "
        SQLJoin = SQLJoin & " INNER JOIN ENTITY EV_DEPT_ENTITY ON EVENT.DEPT_EID = EV_DEPT_ENTITY.ENTITY_ID "
    End If

    'abisht MITS 8639 1/25/2007 SQLJoin Query changed for handling access database
    If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_ACCESS Then
        If rpt.EventBasedFlag Then
            SQLJoin = " FROM (((EVENT INNER JOIN EVENT_X_OSHA ON EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID)INNER JOIN PERSON_INVOLVED ON EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID)INNER JOIN ENTITY AS PI_ENTITY ON PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID)INNER JOIN ENTITY AS EV_DEPT_ENTITY ON EVENT.DEPT_EID = EV_DEPT_ENTITY.ENTITY_ID "
        Else
            SQLJoin = " FROM (((EVENT LEFT OUTER JOIN EVENT_X_OSHA ON EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID)INNER JOIN PERSON_INVOLVED ON EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID)INNER JOIN ENTITY AS PI_ENTITY ON PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID)INNER JOIN ENTITY AS EV_DEPT_ENTITY ON EVENT.DEPT_EID = EV_DEPT_ENTITY.ENTITY_ID "
        End If
    End If
    'abisht MITS 8639 1/25/2007 changes ends here
   
    'jlt 09/17/2003  Beware of reporting methods.  (2) is by employer (1) is by OSHA Establishment
    'Add the WHERE clauses limiting the report (previously just set up joins.)
    If SQLWhere <> "" Then
        SQLWhere = SQLWhere & " AND "
    End If
    If rpt.ByOSHAEstablishmentFlag Then             '(1) is by OSHA Establishment
        SQLWhere = SQLWhere & " EVENT_X_OSHA.OSHA_ESTAB_EID IN " & sInDeptClause & _
        IIf(rpt.BeginDate = "", "", " AND EVENT.DATE_OF_EVENT >= '" & rpt.BeginDate & "'") & _
        IIf(rpt.EndDate = "", "", " AND EVENT.DATE_OF_EVENT <= '" & rpt.EndDate & "'")
    Else                                            '(2) is by employer
        
        If rpt.DriveByEvent Then                                                                'MITS 21930   jtodd22
            SQLWhere = SQLWhere & " EVENT.DEPT_EID IN " & sInDeptClause
        Else
            SQLWhere = SQLWhere & " PERSON_INVOLVED.DEPT_ASSIGNED_EID IN " & sInDeptClause
        End If
        SQLWhere = SQLWhere & IIf(rpt.BeginDate = "", "", " AND EVENT.DATE_OF_EVENT >= '" & rpt.BeginDate & "'")
        SQLWhere = SQLWhere & IIf(rpt.EndDate = "", "", " AND EVENT.DATE_OF_EVENT <= '" & rpt.EndDate & "'")
    
        ' Previously filtered so that only accidents that happened in the employee's
        ' assigned department were reported. Now show any that happened in the
        ' department regardless. BB 07.05.2002
        '" AND PERSON_INVOLVED.DEPT_ASSIGNED_EID IN " & sInDeptClause &
    End If
    SQLWhere = SQLWhere & " AND PERSON_INVOLVED.PI_TYPE_CODE = " & rpt.PITypeEmployee
    
    If SharpsOnlyFlag Then 'Sharps log
        SQLWhere = SQLWhere & " AND EVENT_X_OSHA.SHARPS_OBJECT > 0"
    End If
    If Not rpt.EventBasedFlag Then 'is person involved
        SQLWhere = SQLWhere & " AND PERSON_INVOLVED.OSHA_REC_FLAG <> 0"
    Else    'is event based
        SQLWhere = SQLWhere & " AND EVENT_X_OSHA.RECORDABLE_FLAG <> 0"
    End If
    
    
    'jtodd22 08/20/2007  MITS 8757
    If lFilterEventID > 0 Then
        SQLWhere = SQLWhere & " AND EVENT.EVENT_ID = " & lFilterEventID
    End If
    
    'Add order by to accomodate skipping duplicates
'    SQL = SQL & " ORDER BY EVENT_NUMBER, PERSON_INVOLVED.PI_ROW_ID"
'*********************************************************
' Watch this nasty efficiency gainer... :) Order the recordset by PI_ROW_ID,
' then in the "inner loops" which perform the "left join" functionality (AND ARE
'   ALSO SORTED BY PI_ROW_ID BUT MAY CONTAIN ZERO OR MORE RECORDS)
'   DO NOT RESET THEM TO THE FIRST RECORD!
'*********************************************************
    If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_ORACLE Then
        SQL = SQL & SQLFrom & SQLWhere & " ORDER BY  PERSON_INVOLVED.PI_ROW_ID"
    Else
        SQL = SQL & SQLJoin & " WHERE " & SQLWhere & " ORDER BY  PERSON_INVOLVED.PI_ROW_ID"
    End If
   
#If BB_DUMP_SQL Then
    f.WriteLine rpt.Context.SafeSQL(SQL)
#End If
    
    rpt.Notify.UpdateProgress "Retrieving Osha Incident Records", 0, 1, 0
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchOshaData")
    
' BSB 20030213 Test all databases with non-snapshot database
' trying to debug a flaky sql timeout error on this query.
    If g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_INFORMIX Or _
       g_objRocket.DB_GetDBMake(hDB_SNAPSHOT) = DBMS_IS_DB2 Then      ' JP 1/30/2003   DB2 and Informix can't deal with scrollable cursor with large text (CLOB) data in it
 
        ' Execute count query to get total records first
        SQLCount = "SELECT COUNT(*) " & SQLFrom & SQLWhere
        rs = g_objRocket.DB_CreateRecordset(hDB_SNAPSHOT, rpt.Context.SafeSQL(SQLCount), DB_FORWARD_ONLY, 0)
        RecordCount = GetDataLong(rs, 1)
        g_objRocket.DB_CloseRecordset rs, DB_DROP
  
        ' Open query on complete dataset
        If RecordCount > 0 Then rs = g_objRocket.DB_CreateRecordset(hDB_SNAPSHOT, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
     Else   ' other databases (Access, SQL Server, Oracle) don't have issue with scrollable cursors
           rs = g_objRocket.DB_CreateRecordset(hDB_SNAPSHOT, rpt.Context.SafeSQL(SQL), DB_SNAPSHOT, 0)
              'Hack for Record Count Fix, we almost got away with the movelast-movefirst thing except in
              'Access Databases.
              'Just loop thru and sum for a count.
           While (Not g_objRocket.DB_EOF(rs)): RecordCount = RecordCount + 1: g_objRocket.DB_MoveNext rs: Wend
           g_objRocket.DB_MoveFirst rs
     End If
    
    If RecordCount = 0 Then Set FetchOshaData = New Collection: GoTo hExit  'no incidents to log
    
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchOshaData")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Incident Records", ElapsedSeconds, (ElapsedSeconds / IIf(Items.Count, Items.Count, 1) * (RecordCount - Items.Count)), Items.Count / RecordCount
    
    'Get the full set of PI_ROW_ID's to use in clause for multiples queries.
    'Build a list of PI's for a nice "IN () " clause
    SQL = Mid(SQL, InStr(SQL, " FROM "))
    SQL = Left(SQL, InStr(SQL, " ORDER BY"))
    sInPeopleInvolvedClause = "(SELECT DISTINCT PERSON_INVOLVED.PI_ROW_ID " & SQL & ")"
    'sInPeopleInvolvedClause = BuildPIClause(rs)
    
    '*******************************************************
    'Some data is aggregated - I don't want to hog memory by including them
    'as duplicates result records in the main query so I get them seperately.
    '*******************************************************
    'Get Multiple Treatment Codes
    rsTreatments = FetchTreatments(rpt, sInPeopleInvolvedClause)
    DebugMsg "Last FetchData SQL junk done"
    'Get Multiple Work Loss Records
    rsWorkLosses = FetchWorkLosses(rpt, sInPeopleInvolvedClause)
    'Get Multiple Restriction Records
    rsRestrictions = FetchRestrictions(rpt, sInPeopleInvolvedClause)
    'Get Multiple Body Part Codes.
    rsBodyParts = FetchBodyParts(rpt, sInPeopleInvolvedClause)
    'Get (Possibly Missing) Hospital Info.
    rsHospital = FetchHospitals(rpt, sInPeopleInvolvedClause)
    'Get (Possibly Missing) Physician Info.
    rsPhysician = FetchPhysicians(rpt, sInPeopleInvolvedClause)
    'Get Multiple Injury Codes.
    rsInjuries = FetchInjuries(rpt, sInPeopleInvolvedClause)
    
    '********************************
    ' Only the illness code in Person Involved is Used.
    '********************************
    '***********************************************
    'Data Retrieval Completed....
    'Now Construct a collection of COSHAItems to hold it.
    '***********************************************
    While Not g_objRocket.DB_EOF(rs)
        If rpt.Notify.CheckAbort Then AbortRequested ("FetchOshaData")
        'Book-keep the while loop to avoid duplicate COSHAItems
        sPrev = sCurr
        sCurr = CStr(GetDataLong(rs, "PI_ROW_ID"))
        If sPrev <> sCurr Then 'not a duplicate record.
            s180LimitDateDTG = "99999999"                       'jtodd22 12/03/2004  must be set for each record
            Set objItem = New COSHAItem
            With objItem
                .EventId = GetDataLong(rs, "EVENT_ID")
                .CaseNumber = GetDataString(rs, "EVENT_NUMBER")
                'Can't "Guess" twelve am (midnight).
                'jtodd22 01/15/2008 MITS 6107
                'mjha3 : 02/11/2009 : MITS 13980
                'Previous code for TimeOfEvent has been modified.
                'Time of Event will be treated same whether it is NULL or 000000.
                sTimeOfEvent = GetDataString(rs, "TIME_OF_EVENT")
                If (sTimeOfEvent = "000000" Or sTimeOfEvent = "") Then
                    sTimeOfEvent = ""
                Else
                    .TimeOfEvent = f_sGetTimeHHMM_AM_PM(sTimeOfEvent)
                End If
                .DateOfDeath = GetDataString(rs, "DATE_OF_DEATH")
                .ChkBoxDeath = IIf(Trim(.DateOfDeath) = "", "NO", "YES")
                .DateOfHire = GetDataString(rs, "DATE_HIRED")
                .EmployeeStartTime = f_sGetTimeHHMM_AM_PM(GetDataString(rs, "WORKDAY_START_TIME"))
                .EmployeeAddress = GetDataString(rs, "PI_ADDR1") & " " & GetDataString(rs, "PI_ADDR2")
                .EmployeeCity = GetDataString(rs, "PI_CITY")
                .EmployeeState = rpt.Context.sGetStateCode(GetDataLong(rs, "PI_STATE_ID"))
                .EmployeeZipCode = GetDataString(rs, "PI_ZIP_CODE")
                .EmployeeDateOfBirth = GetDataString(rs, "PI_BIRTH_DATE")
                .EmployeeSex = rpt.Context.sGetShortCode(GetDataLong(rs, "PI_SEX_CODE"))
    
                'Pick the Field(s) to sort by.
                'Choose a default (2 - Person Name Then Event Number)
               DebugMsg "Item Sort Order field value (0 based):" & rpt.ItemSortOrder
                .SortOn = GetDataString(rs, "PI_LAST_NAME") & GetDataString(rs, "PI_FIRST_NAME") & GetDataString(rs, "PI_MIDDLE_NAME") & GetDataString(rs, "EVENT_NUMBER")
                Select Case rpt.ItemSortOrder
                    Case SORT_EVENTDATE 'Event Date Then Event Number
                        .SortOn = GetDataString(rs, "DATE_OF_EVENT") & GetDataString(rs, "EVENT_NUMBER")
                    Case SORT_EVENTNUMBER 'Event Number Then Person Name
                        .SortOn = GetDataString(rs, "EVENT_NUMBER") & GetDataString(rs, "PI_LAST_NAME") & GetDataString(rs, "PI_FIRST_NAME") & GetDataString(rs, "PI_MIDDLE_NAME")
                    Case SORT_PERSONNAME 'Is default - leave it alone....
                End Select
    
                .EmployeeFullName = GetDataString(rs, "PI_FIRST_NAME") & " " & GetDataString(rs, "PI_MIDDLE_NAME") & " " & GetDataString(rs, "PI_LAST_NAME")
                .EmployeeFullName = Replace(.EmployeeFullName, "  ", " ")
                .Occupation = rpt.Context.sGetCodeDesc(GetDataLong(rs, "POSITION_CODE"))
                If Trim$(.Occupation) = "" Then .Occupation = "(No Data)"
                .DateOfEvent = GetDataString(rs, "DATE_OF_EVENT")
                
                'mjha3 : 01/23/2009 : MITS 14360
                Select Case rpt.ColumnESource
                    Case SRC_E_PRIMARY_LOCATION
                    .EventLocation = rpt.Context.sGetCodeDesc(GetDataLong(rs, "PRIMARY_LOC_CODE"))
                    Case SRC_E_LOCATION_DESCRIPTION
                        .EventLocation = GetDataString(rs, "LOCATION_AREA_DESC")
                        .EventLocation = f_sRemoveDateTimeStampUserName(.EventLocation)
                        .EventLocation = Replace(.EventLocation, vbCrLf, " ")
                    Case SRC_E_DEPARTMENT_ADDRESS
                        s = IIf(GetDataString(rs, "EV_ADDR1") <> "", "EV_", "EV_DEPT_")
                        If s = "EV_DEPT_" Then .EventLocation = GetDataString(rs, "EV_DEPT_LAST_NAME") & ", "
                        .EventLocation = .EventLocation & GetDataString(rs, s & "ADDR1") & _
                                    GetDataString(rs, s & "ADDR2") & ", " & _
                                    GetDataString(rs, s & "CITY") & ", " & _
                                    Trim$(rpt.Context.sGetStateCode(GetDataLong(rs, s & "STATE_ID"))) & ", " & _
                                    GetDataString(rs, s & "ZIP_CODE")
                        If .EventLocation = ", , , " Then .EventLocation = "(no data)"
                        .EventLocation = Replace(.EventLocation, ", , ", "")
                        If Right(.EventLocation, 2) = ", " Then .EventLocation = Left(.EventLocation, Len(.EventLocation) - 2)
                End Select
                If Trim$(.EventLocation) = "" Then
                    s = IIf(GetDataString(rs, "EV_ADDR1") <> "", "EV_", "EV_DEPT_")
                    If s = "EV_DEPT_" Then .EventLocation = GetDataString(rs, "EV_DEPT_LAST_NAME") & ", "
                    .EventLocation = .EventLocation & GetDataString(rs, s & "ADDR1") & _
                                GetDataString(rs, s & "ADDR2") & ", " & _
                                GetDataString(rs, s & "CITY") & ", " & _
                                Trim$(rpt.Context.sGetStateCode(GetDataLong(rs, s & "STATE_ID"))) & ", " & _
                                GetDataString(rs, s & "ZIP_CODE")
                    If .EventLocation = ", , , " Then .EventLocation = "(no data)"
                    .EventLocation = Replace(.EventLocation, ", , ", "")
                    If Right(.EventLocation, 2) = ", " Then .EventLocation = Left(.EventLocation, Len(.EventLocation) - 2)
                End If
                ' MITS 9101 Manish C Jha 08/21/2008
                .LocDescription = GetDataString(rs, "LOCATION_AREA_DESC")
                .LocDescription = f_sRemoveDateTimeStampUserName(.LocDescription)
                If rpt.LocDescription = True Then
                        .LocDescription = Replace(.LocDescription, vbCrLf, " ")
                        .EventLocation = .LocDescription
                End If
                'Had this logic incorrect... BB 03/29/02
                .ActivityWhenInjured = IIf(rpt.PrintOSHADescFlag, GetDataString(rs, "ACTIVITY_WHEN_INJ"), GetDataString(rs, "EVENT_DESCRIPTION"))
                If Not rpt.EventBasedFlag Then .ActivityWhenInjured = GetDataString(rs, "OSHA_ACC_DESC")
    
                .ActivityWhenInjured = f_sRemoveDateTimeStamp(.ActivityWhenInjured)
    
                .DisabilityCode = rpt.Context.sGetShortCode(GetDataLong(rs, "DISABILITY_CODE"))
                If Trim$(.DisabilityCode & Chr$(32)) <> "INJ" And Trim$(.DisabilityCode & Chr$(32)) <> "ILL" Then
                    rpt.Errors.Add "FetchOshaData", Erl, "3100", "", SoftErrFormat(.CaseNumber, "Unknown Disability Code:'" & .DisabilityCode & " '  (Expected 'ILL' or 'INJ')")
                End If
                .PiRowId = GetDataLong(rs, "PI_ROW_ID")
    
                '*********************
                ' These functions check the .DisabilityCode
                ' (Hence it MUST be set prior to calling them.)
                '*********************
                'LoadIllnesses objItem, rsIllnesses 'no longer using pi_x_illness bb 01/2002
                'MITS 5311 jtodd22 08/31/2005
                If .DisabilityCode = "ILL" Then
                    .IllnessDesc = rpt.Context.sGetCodeDesc(GetDataLong(rs, "ILLNESS_CODE"))
                    RecordIllnessAs objItem, rpt.Context.GetSingleString("PARENT.SHORT_CODE", "CODES CHILD, CODES PARENT", "CHILD.CODE_ID =" & GetDataLong(rs, "ILLNESS_CODE") & " AND CHILD.RELATED_CODE_ID = PARENT.CODE_ID")
                End If
                sBodyParts = GetBodyParts(objItem, rsBodyParts)
                LoadInjuries objItem, rsInjuries
                
                'jlt 02/20/2004 added missing LoadTreatments
                LoadTreatments objItem, rsTreatments
                
                .DaysOfWeek = GetDaysOfWeek(rs)
                'OSHA has published a procedure change for counting the 180 day rule, jtodd22 12/03/2004
                Set colWorkLosses = New CWorkLosses
                Set colRestrictions = New CRestrictions
                
                If Not rpt.Enforce180DayRule Then
                    'jtodd22 12/03/2004--LoadWorkLoss objItem, rsWorkLosses, rpt
                    LoadWorkLoss objItem, rsWorkLosses, rpt
                    If .DaysWorkLoss = -1 Then rpt.Errors.Add "FetchOshaData", Erl, "3100", "", SoftErrFormat(.CaseNumber, "Invalid date range for work loss.")
                    'jtodd22 12/03/2004--LoadRestrictedDays objItem, rsRestrictions, rpt
                    LoadRestrictedDays objItem, rsRestrictions, rpt
                    If .DaysRestriction = -1 Then rpt.Errors.Add "FetchOshaData", Erl, "3100", "", SoftErrFormat(.CaseNumber, "Invalid date range for work restriction.")
                Else
                    LoadWorkLoss objItem, rsWorkLosses, rpt
                    LoadRestrictedDays objItem, rsRestrictions, rpt
                    GrossTotalWorkLossDays = 0
                    If .DaysWorkLoss > 0 Then
                        GrossTotalWorkLossDays = .DaysWorkLoss
                    End If
                    
                    If .DaysRestriction + .DaysWorkLoss > 180 Then
                        l_180DayCount = 0
                        lRestrictCount = colRestrictions.Count
                        lWorkLossCount = colWorkLosses.Count
                        lRestrictPlace = 0
                        lWorkLossPlace = 0
                        sRestrictDateDTG = "99999999"
                        sWorkLossDateDTG = "99999999"
                        .DaysRestriction = 0
                        .DaysWorkLoss = 0
                        
                        'Get first Records
                        If lRestrictCount > lRestrictPlace Then
                            lRestrictPlace = lRestrictPlace + 1
                            sRestrictDateDTG = colRestrictions.Item(lRestrictPlace).DateFirstRestrct
                        Else
                            sRestrictDateDTG = "99999999"
                        End If
                        If lWorkLossCount > lWorkLossPlace Then
                            lWorkLossPlace = lWorkLossPlace + 1
                            sWorkLossDateDTG = colWorkLosses.Item(lWorkLossPlace).DateLastWorked
                        Else
                            sWorkLossDateDTG = "99999999"
                        End If
                        bDone = False
                        While Not bDone
                            If sRestrictDateDTG > sWorkLossDateDTG Then
                                'workloss record
                                l_180DayCount = l_180DayCount + colWorkLosses.Item(lWorkLossPlace).OSHAWorkLossDays
                                If l_180DayCount > 180 Then
                                    .DaysWorkLoss = .DaysWorkLoss + (colWorkLosses.Item(lWorkLossPlace).OSHAWorkLossDays - (l_180DayCount - 180))
                                    sRestrictDateDTG = "99999999"
                                    sWorkLossDateDTG = "99999999"
                                Else
                                    .DaysWorkLoss = .DaysWorkLoss + colWorkLosses.Item(lWorkLossPlace).OSHAWorkLossDays
                                    If lWorkLossCount > lWorkLossPlace Then
                                        lWorkLossPlace = lWorkLossPlace + 1
                                        sWorkLossDateDTG = colWorkLosses.Item(lWorkLossPlace).DateLastWorked
                                    Else
                                        sWorkLossDateDTG = "99999999"
                                    End If
                                End If
                            Else
                                If sRestrictDateDTG < sWorkLossDateDTG Then
                                    'restriction record
                                    l_180DayCount = l_180DayCount + colRestrictions.Item(lRestrictPlace).OSHARestrictionDays
                                    If l_180DayCount > 180 Then
                                        .DaysRestriction = .DaysRestriction + (colRestrictions.Item(lRestrictPlace).OSHARestrictionDays - (l_180DayCount - 180))
                                        sRestrictDateDTG = "99999999"
                                        sWorkLossDateDTG = "99999999"
                                    Else
                                        .DaysRestriction = .DaysRestriction + colRestrictions.Item(lRestrictPlace).OSHARestrictionDays
                                        If lRestrictCount > lRestrictPlace Then
                                            lRestrictPlace = lRestrictPlace + 1
                                            sRestrictDateDTG = colRestrictions.Item(lRestrictPlace).DateFirstRestrct
                                        Else
                                            sRestrictDateDTG = "99999999"
                                        End If
                                    End If
                                End If
                            End If
                            If sRestrictDateDTG = sWorkLossDateDTG Then bDone = True
                        Wend
                        
                        'jtodd22 01/30/2009 to cover the condition where there is a WorkLoss and a Restriction of 180 days
                        If .DaysWorkLoss = 0 And GrossTotalWorkLossDays > 0 Then .DaysWorkLoss = 1
                        
                    End If
                End If
                Debug.Print "Lost Day Count:  " & .DaysWorkLoss
                Debug.Print "Restricted Count:  " & .DaysRestriction
                Set colWorkLosses = Nothing
                Set colRestrictions = Nothing
                If .DaysWorkLoss = -1 Then .DaysWorkLoss = 0
                If .DaysRestriction = -1 Then .DaysRestriction = 0
                LoadPhysician objItem, rsPhysician
                LoadHospital objItem, rsHospital, rpt.Context
    
    
                .ObjectSubstanceThatInjured = f_sRemoveDateTimeStamp(GetDataString(rs, "OBJ_SUBST_THAT_INJ"))
                .HowEventOccurred = f_sRemoveDateTimeStamp(GetDataString(rs, "HOW_ACC_OCCURRED"))
                '.InjuryDesc = .HowEventOccurred & "; " & .ObjectSubstanceThatInjured & ";"
                If Trim$(.InjuryDesc) > "" Then
                    .InjuryDesc = f_sRemoveDateTimeStamp(.InjuryDesc) & "; "
                End If
                If Trim$(.IllnessDesc) > "" Then
                    .InjuryDesc = .InjuryDesc & f_sRemoveDateTimeStamp(.IllnessDesc) & "; "
                End If
                If Trim$(sBodyParts) > "" Then
                    .InjuryDesc = .InjuryDesc & f_sRemoveDateTimeStamp(sBodyParts) & "; "
                End If
                'jlt 11/20/2002 Me.InjuryDesc does not always need Me.ObjectSubstanceThatInjured, see MITS 3734
'                If Trim$(.ObjectSubstanceThatInjured) > "" Then
'                    .InjuryDesc = .InjuryDesc & f_sRemoveDateTimeStamp(.ObjectSubstanceThatInjured) & "; "
'                End If
                DebugMsg "Column F Source field value (0 based):" & rpt.ColumnFSource
                Select Case rpt.ColumnFSource
                    Case SRC_DEFAULT
                        'Do nothing - already set.
                    Case SRC_EVENTDESC 'Just use event description.
                        .InjuryDesc = f_sRemoveDateTimeStamp(GetDataString(rs, "EVENT_DESCRIPTION"))
                    Case SRC_OSHAHOWACC 'Just use osha how acc. occurred field.
                        .InjuryDesc = f_sRemoveDateTimeStamp(.HowEventOccurred)
                    Case SRC_OSHAACCDESC
                        .InjuryDesc = f_sRemoveDateTimeStamp(GetDataString(rs, "OSHA_ACC_DESC"))
                End Select
                If .InjuryDesc = "" Then .InjuryDesc = "(No Data: Selection=" & rpt.ColumnFSource + 1 & " )"

                .PrivacyCase = GetDataLong(rs, "PRIVACY_CASE_FLAG")
  
                .SharpsObjectId = GetDataLong(rs, "SHARPS_OBJECT")
                .SharpsMakeId = GetDataLong(rs, "SHARPS_BRAND_MAKE")
            End With

            'For "Insertion Sort based on 'SortOn'" find the proper collection "position"
            i = 1
            For Each tmpItem In Items
                If tmpItem.SortOn > objItem.SortOn Then Exit For
                i = i + 1
            Next
            'Add before selected position
            If Items.Count = 0 Then 'First entry
                Items.Add objItem, "Key:" & objItem.PiRowId
            ElseIf Items.Count >= i Then 'Found valid position
                Items.Add objItem, "Key:" & objItem.PiRowId, i
            Else 'Add to end of collection
                Items.Add objItem, "Key:" & objItem.PiRowId, , Items.Count
            End If
        End If 'if not a duplicate
        ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
        rpt.Notify.UpdateProgress "Constructing Osha Incident (" & Items.Count & " of " & RecordCount & ")", ElapsedSeconds, (ElapsedSeconds / IIf(Items.Count, Items.Count, 1) * (RecordCount - Items.Count)), Items.Count / RecordCount
        g_objRocket.DB_MoveNext rs
    Wend
    Set FetchOshaData = Items
    
hExit:
    SafeCloseRecordset rs
    SafeCloseRecordset rsTreatments
    SafeCloseRecordset rsInjuries
    SafeCloseRecordset rsWorkLosses
    SafeCloseRecordset rsBodyParts
    SafeCloseRecordset rsRestrictions
    SafeCloseRecordset rsPhysician
    SafeCloseRecordset rsHospital
    SafeCloseConnection hDB_SNAPSHOT
    Set colWorkLosses = Nothing
    Set colRestrictions = Nothing
    g_objErr.Bubble
    Exit Function
hErr:

    LogError "RptOSHA.FetchOshaData", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.FetchOshaData", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Function
' MITS 9101 Manish C Jha 08/21/2008
Private Function f_sRemoveDateTimeStampUserName(ByVal str_test As String) As String
Dim iPos
    'always trim the incoming string, a date/time stamp may have a leading space
    str_test = Trim(str_test & "")
    f_sRemoveDateTimeStampUserName = str_test
    If str_test = "" Then
        Exit Function
    End If
    '***********
    'remove date
    iPos = InStr(str_test, " ")
    If IsDate(Left(str_test, iPos)) Then
        str_test = Trim(Mid(str_test, iPos + 1))
        f_sRemoveDateTimeStampUserName = str_test
    'Else
    '    Exit Function
    End If
    '***********
    'remove time
    'remember that a user friendly date/time will have the format of "hh:mm ?M" or "h:mm ?M"
    'and the ?M may have a period after it, so start the search at the seventh character for the trailing space
    iPos = InStr(7, str_test, " ")
    If IsDate(Left(str_test, iPos)) Then
        str_test = Trim(Mid(str_test, iPos + 1))
        f_sRemoveDateTimeStampUserName = str_test
    'Else
    '    Exit Function
    End If
    '**************
    'Remove user ID
    If Len(str_test) > 0 Then
       If Left(str_test, 1) = "(" Then
           iPos = 0
           iPos = InStr(1, str_test, ")")
           If iPos > 0 Then
               str_test = Trim(Mid(str_test, iPos + 1))
           End If
       End If
    End If
    f_sRemoveDateTimeStampUserName = Trim(str_test)
End Function
Public Sub LoadReportOnCompanyData(Company As CEntityItem, rpt As IOSHAReport)
    Dim SQL As String
    Dim rs As Integer
    On Error GoTo hErr
    DebugMsg "Rpt (" & TypeName(rpt) & ") Is Nothing? " & (rpt Is Nothing)
    SQL = SQL & " SELECT"
    SQL = SQL & " REPORT_ON_ENTITY.LAST_NAME | RPT_LAST_NAME, REPORT_ON_ENTITY.ABBREVIATION | RPT_ABBREVIATION,"
    SQL = SQL & " REPORT_ON_ENTITY.ADDR1 | RPT_ADDR1, REPORT_ON_ENTITY.ADDR2 | RPT_ADDR2,"
    SQL = SQL & " REPORT_ON_ENTITY.CITY | RPT_CITY,REPORT_ON_ENTITY.STATE_ID | RPT_STATE_ID,"
    SQL = SQL & " REPORT_ON_ENTITY.ZIP_CODE | RPT_ZIP_CODE,REPORT_ON_ENTITY.NATURE_OF_BUSINESS | RPT_NATURE_OF_BUSINESS,"
    SQL = SQL & " REPORT_ON_ENTITY.SIC_CODE | RPT_SIC_CODE,"
    SQL = SQL & " REPORT_ON_ENTITY.NAICS_CODE | RPT_NAICS_CODE"             'jlt 11/04/2003
    SQL = SQL & " FROM"
    SQL = SQL & " ENTITY REPORT_ON_ENTITY"
    SQL = SQL & " WHERE REPORT_ON_ENTITY.ENTITY_ID = " & rpt.ReportOn
    DebugMsg "Formulated query: " & SQL
    DebugMsg "Context is nothing?: " & (rpt.Context Is Nothing)
    DebugMsg "Formulated query: " & rpt.Context.SafeSQL(SQL)
    
    rs = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
    DebugMsg "Created Recordset"
    If Not g_objRocket.DB_EOF(rs) Then
            Company.Name = GetDataString(rs, "RPT_LAST_NAME") & "  " & GetDataString(rs, "RPT_ABBREVIATION")
            Company.Address = GetDataString(rs, "RPT_ADDR1") & "  " & GetDataString(rs, "RPT_ADDR2")
            Company.City = Trim$(GetDataString(rs, "RPT_CITY"))
            Company.State = rpt.Context.sGetStateCode(GetDataLong(rs, "RPT_STATE_ID"))
            Company.ZipCode = Trim$(GetDataString(rs, "RPT_ZIP_CODE"))
            Company.NatureOfBusiness = Trim$(GetDataString(rs, "RPT_NATURE_OF_BUSINESS"))
            Company.SICCode = rpt.Context.sGetShortCode(GetDataLong(rs, "RPT_SIC_CODE"))
            Company.NAICSText = rpt.Context.sGetShortCode(GetDataLong(rs, "RPT_NAICS_CODE"))
            'jlt 01/28/2003     Company.SICCode = GetDataLong(rs, "RPT_SIC_CODE")
    End If
    SafeCloseRecordset rs
    
    If Val(rpt.EstablishmentNamePrefix) <> 0 Then Company.Name = rpt.Context.GetSingleString("LAST_NAME", "ORG_HIERARCHY, ENTITY", rpt.sReportLevel & " = " & rpt.ReportOn & " AND ENTITY.ENTITY_ID = " & sOrgLevelToEIDColumn(Val(rpt.EstablishmentNamePrefix))) & "; " & Company.Name
    
    DebugMsg "leaving loadreportoncompanydata"

hExit:
    SafeCloseRecordset rs
    Exit Sub
hErr:
    LogError "RptOSHA.LoadReportOnCompanyData", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
End Sub

 Private Sub RecordIllnessAs(objItem As COSHAItem, sParentShortCode As String)
    
    
    With objItem
        If .DisabilityCode <> "ILL" Then Exit Sub
        Select Case sParentShortCode
            Case "01"
                .RecordAsSkinDisor = -1
            Case "02"
                .RecordAsRespCond = -1
            Case "03"
                .RecordAsPoison = -1
            Case "04"
                .RecordAsPoison = -1
            Case "05"
                .RecordAsOther = -1
    '                Case "06"
    '                    .RecordAsMuscSkel = -1
            Case "07"
                .RecordAsOther = -1
            Case "SD"
                .RecordAsSkinDisor = -1
            Case "HL"
                'new for 2004  jlt
                If .DateOfEvent > "20032131" Then
                    .RecordAsHearLoss = -1
                Else
                    .RecordAsOther = -1
                End If
    '                            Case "MD"
    '                                .RecordAsMuscSkel = -1
            Case "MISC"
                .RecordAsOther = -1
            Case "PO"
                .RecordAsPoison = -1
            Case "RC"
                .RecordAsRespCond = -1
            Case Else
                .RecordAsOther = -1
        End Select
    End With
End Sub
Private Function FetchPhysicians(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    On Error GoTo hErr
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchPhysicians")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Physician Info", ElapsedSeconds, 0, 0
    SQL = " SELECT PI_ROW_ID,"
    SQL = SQL & " PHYS_ENTITY.FIRST_NAME | PHYS_FIRST_NAME,PHYS_ENTITY.MIDDLE_NAME | PHYS_MIDDLE_NAME,"
    SQL = SQL & " PHYS_ENTITY.LAST_NAME | PHYS_LAST_NAME, PHYS_ENTITY.ADDR1 | PHYS_ADDR1,"
    SQL = SQL & " PHYS_ENTITY.ADDR2 | PHYS_ADDR2, PHYS_ENTITY.CITY | PHYS_CITY, PHYS_ENTITY.STATE_ID |"
    SQL = SQL & " PHYS_STATE_ID, PHYS_ENTITY.ZIP_CODE | PHYS_ZIP_CODE FROM"
    SQL = SQL & " PI_X_PHYSICIAN, ENTITY PHYS_ENTITY"
    SQL = SQL & " WHERE PI_ROW_ID IN " & sInPeopleInvolvedClause
    SQL = SQL & " AND PHYS_ENTITY.ENTITY_ID = PI_X_PHYSICIAN.PHYSICIAN_EID ORDER BY PI_ROW_ID"
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchPhysicians = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchPhysicians", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit
End Function

Private Sub LoadPhysician(objItem As COSHAItem, rsPhysician As Integer)
    Dim lRetry As Long
    Dim lRowId As Long
    'Prime the While Loop
   On Error GoTo hErr

    If Not g_objRocket.DB_EOF(rsPhysician) Then lRowId = GetDataLong(rsPhysician, "PI_ROW_ID")
    
    Do While Not g_objRocket.DB_EOF(rsPhysician) And lRowId <= objItem.PiRowId
        If (lRowId = objItem.PiRowId) Then
            objItem.HealthCareProf = GetDataString(rsPhysician, "PHYS_FIRST_NAME") & " " & GetDataString(rsPhysician, "PHYS_MIDDLE_NAME") & " " & GetDataString(rsPhysician, "PHYS_LAST_NAME")
            Exit Sub ' only want the first record found.
        End If
        g_objRocket.DB_MoveNext rsPhysician
        If Not g_objRocket.DB_EOF(rsPhysician) Then lRowId = GetDataLong(rsPhysician, "PI_ROW_ID")
    Loop

hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadPhysician", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadPhysician", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Sub
Private Function FetchHospitals(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchHospitals")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Hospital Info", ElapsedSeconds, 0, 0
    
    SQL = " SELECT  PI_ROW_ID, HOSP_ENTITY.LAST_NAME | HOSP_LAST_NAME, HOSP_ENTITY.ADDR1 | HOSP_ADDR1,"
    SQL = SQL & " HOSP_ENTITY.ADDR2 | HOSP_ADDR2,HOSP_ENTITY.CITY | HOSP_CITY, HOSP_ENTITY.STATE_ID"
    SQL = SQL & " | HOSP_STATE_ID, HOSP_ENTITY.ZIP_CODE | HOSP_ZIP_CODE FROM"
    SQL = SQL & " PI_X_HOSPITAL, ENTITY HOSP_ENTITY"
    SQL = SQL & " WHERE PI_ROW_ID IN " & sInPeopleInvolvedClause
    SQL = SQL & " AND HOSP_ENTITY.ENTITY_ID = PI_X_HOSPITAL.HOSPITAL_EID ORDER BY PI_ROW_ID"
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchHospitals = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchHospitals", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit

End Function


Private Sub LoadHospital(objItem As COSHAItem, rsHospital As Integer, Context As CInstanceUtilities)
    On Error GoTo hErr
    Dim lRetry As Long
    Dim lRowId As Long
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsHospital) Then lRowId = GetDataLong(rsHospital, "PI_ROW_ID")

    Do While Not g_objRocket.DB_EOF(rsHospital) And lRowId <= objItem.PiRowId
        If (lRowId = objItem.PiRowId) Then
                objItem.HealthCareEntityFacility = GetDataString(rsHospital, "HOSP_LAST_NAME")
                objItem.HealthCareEntityAddress = GetDataString(rsHospital, "HOSP_ADDR1") & " " & GetDataString(rsHospital, "HOSP_ADDR2")
                objItem.HealthCareEntityCity = GetDataString(rsHospital, "HOSP_CITY")
                objItem.HealthCareEntityState = (Context.sGetStateCode(GetDataLong(rsHospital, "HOSP_STATE_ID")))
                objItem.HealthCareEntityZipCode = GetDataString(rsHospital, "HOSP_ZIP_CODE")
                Exit Sub ' only want the first record found.
        End If
        g_objRocket.DB_MoveNext rsHospital
        If Not g_objRocket.DB_EOF(rsHospital) Then lRowId = GetDataLong(rsHospital, "PI_ROW_ID")
    Loop
hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadHospital", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadHospital", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If

End Sub

  Private Function FetchInjuries(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchInjuries")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Injury Info", ElapsedSeconds, 0, 0
    
    ' JP 1/21/2004   Not Informix compatible.   SQL = "SELECT PI_ROW_ID, CODES_TEXT.CODE_DESC"
    SQL = "SELECT PI_ROW_ID, CODES_TEXT.CODE_DESC,MAIN_CODE.CODE_ID "   ' JP 1/21/2004
    SQL = SQL & " FROM CODES MAIN_CODE, CODES_TEXT, PI_X_INJURY "
    SQL = SQL & " WHERE PI_X_INJURY.INJURY_CODE = MAIN_CODE.CODE_ID"
    SQL = SQL & " AND CODES_TEXT.CODE_ID = MAIN_CODE.CODE_ID"
    SQL = SQL & " AND  PI_ROW_ID IN " & sInPeopleInvolvedClause & " ORDER BY PI_ROW_ID, MAIN_CODE.CODE_ID"
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchInjuries = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchInjuries", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit

End Function
  
Private Sub LoadInjuries(objItem As COSHAItem, rsInjury As Integer)
    On Error GoTo hErr
    Dim lRetry As Long
    Dim sTemp As String
    Dim lRowId As Long
    
    If objItem.DisabilityCode <> "INJ" Then Exit Sub
    
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsInjury) Then lRowId = GetDataLong(rsInjury, "PI_ROW_ID")

'    g_objRocket.DB_MoveFirst rsInjury
    Do While Not g_objRocket.DB_EOF(rsInjury) And lRowId <= objItem.PiRowId
        If lRowId = objItem.PiRowId Then
            AddDelimited sTemp, Trim(GetDataString(rsInjury, "CODE_DESC"))
        End If
        g_objRocket.DB_MoveNext rsInjury
        If Not g_objRocket.DB_EOF(rsInjury) Then lRowId = GetDataLong(rsInjury, "PI_ROW_ID")
    Loop
    
    objItem.RecordAsInjury = -1
    objItem.InjuryDesc = sTemp
hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadInjuries", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadInjuries", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If

End Sub
Private Sub LoadTreatments(objItem As COSHAItem, rsTreatments As Integer)
    Dim sTemp As String
    Dim lRowId As Long
    Dim lRetry As Long
    On Error GoTo hErr
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsTreatments) Then lRowId = GetDataLong(rsTreatments, "PI_ROW_ID")

    Do While Not g_objRocket.DB_EOF(rsTreatments) And lRowId <= objItem.PiRowId
        If lRowId = objItem.PiRowId Then
            'Treatments are "cummulative" in nature...
            Select Case GetDataLong(rsTreatments, "SHORT_CODE")
                Case 3
                    objItem.EmergencyRoomTreatment = -1
                Case 4
                    objItem.EmergencyRoomTreatment = -1
                    objItem.HospitalizedOverNight = -1
                Case 5
                    objItem.EmergencyRoomTreatment = -1
                    objItem.HospitalizedOverNight = -1
            End Select
        End If
        g_objRocket.DB_MoveNext rsTreatments
        If Not g_objRocket.DB_EOF(rsTreatments) Then lRowId = GetDataLong(rsTreatments, "PI_ROW_ID")
    Loop
hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadTreatments", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadTreatments", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
   
End Sub
Private Function FetchBodyParts(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchBodyParts")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Body Part Info", ElapsedSeconds, 0, 0
    
    SQL = "SELECT PI_ROW_ID, CODE_DESC FROM CODES_TEXT, PI_X_BODY_PART "
    SQL = SQL & " WHERE PI_ROW_ID IN " & sInPeopleInvolvedClause
    SQL = SQL & " AND PI_X_BODY_PART.BODY_PART_CODE = CODES_TEXT.CODE_ID "
    SQL = SQL & " ORDER BY PI_ROW_ID"
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchBodyParts = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchBodyParts", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit
End Function
Private Function GetBodyParts(objItem As COSHAItem, rsBodyParts As Integer) As String
    Dim lNumParts As Long
    Dim sTemp As String
    Dim lRowId As Long
    
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsBodyParts) Then lRowId = GetDataLong(rsBodyParts, "PI_ROW_ID")
    Do While Not g_objRocket.DB_EOF(rsBodyParts) And lRowId <= objItem.PiRowId
        If (lRowId = objItem.PiRowId) Then
            AddDelimited sTemp, Trim(GetDataString(rsBodyParts, "CODE_DESC"))
            lNumParts = lNumParts + 1
        End If
        g_objRocket.DB_MoveNext rsBodyParts
        If Not g_objRocket.DB_EOF(rsBodyParts) Then lRowId = GetDataLong(rsBodyParts, "PI_ROW_ID")
    Loop
    GetBodyParts = sTemp
 End Function
 
Private Function FetchWorkLosses(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchWorkLosses")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Work Loss Info", ElapsedSeconds, 0, 0
    
    'MITS 14372
    SQL = ""
    SQL = SQL & "SELECT PI_ROW_ID, PI_X_WORK_LOSS.DATE_RETURNED, PI_X_WORK_LOSS.DATE_LAST_WORKED, PI_X_WORK_LOSS.DURATION"
    SQL = SQL & " FROM PI_X_WORK_LOSS WHERE PI_X_WORK_LOSS.PI_ROW_ID IN " & sInPeopleInvolvedClause
    SQL = SQL & " ORDER BY PI_ROW_ID,PI_X_WORK_LOSS.DATE_LAST_WORKED"      'must have PI_ROW_ID for efficiency and PI_X_WORK_LOSS.DATE_LAST_WORKED for 180 day rule.

#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchWorkLosses = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchWorkLoss", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit
End Function
'NOTE: Bad data bug found against Kaiser (ORACLE) where a date returned
' is found with no corresponding date last worked!
' Added Armor. BSB 2/20/2002
'
Private Sub LoadWorkLossCollection(objItem As COSHAItem, rsWorkLosses As Integer, rpt As IOSHAReport)
    Dim iDaysOff As Integer
    Dim lOSHAWorkLossDays As Long               'MITS 14372
    Dim lRowId As Long
    Dim AsOfDate As String                      'jlt 01/24/2003
    Dim AsOfDatePlusOne As String               'jtodd22 012/03/2004
    Dim sAsOfDatePlusOneDTG As String           'jtodd22 012/03/2004
    Dim sAsOfDateDTG As String                  'jlt 01/27/2003
    Dim sDateLastWorkedDTG As String            'jlt 01/27/2003
    Dim sDateReturnedDTG As String              'jlt 01/27/2003
    Dim lRetry As Long
    On Error GoTo hErr
    'If No "as of" date specified then use today.
    'jlt 01/24/2003     If Not IsDate(sAsOfDate) Then sAsOfDate = Date
    If Not IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then      'jlt 01/24/2003
        AsOfDate = Date
        sAsOfDateDTG = Format(Date, "yyyymmdd")
    Else
        AsOfDate = sDBDateFormat(rpt.AsOfDate, "Short Date")
        sAsOfDateDTG = rpt.AsOfDate
    End If
    AsOfDatePlusOne = DateAdd("d", 1, AsOfDate)
    sAsOfDatePlusOneDTG = Format(AsOfDatePlusOne, "yyyymmdd")
    
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsWorkLosses) Then lRowId = GetDataLong(rsWorkLosses, "PI_ROW_ID")
    While Not g_objRocket.DB_EOF(rsWorkLosses) And lRowId <= objItem.PiRowId
        
        sDateLastWorkedDTG = Trim$(GetDataString(rsWorkLosses, "DATE_LAST_WORKED") & "")
        'jlt 12/22/2003 enforce first WL day after event date (earliest sDateLastWorkedDTG is objItem.DateOfEvent)
        If sDateLastWorkedDTG < objItem.DateOfEvent Then
            rpt.Errors.Add "LoadWorkLoss", Erl, "3201", "", SoftErrFormat(objItem.CaseNumber, "'Date Lasted Worked' is before 'Date of Event', 'Date Lasted Worked' was changed for Calculation Only")
            sDateLastWorkedDTG = objItem.DateOfEvent
        End If
        sDateReturnedDTG = Trim$(GetDataString(rsWorkLosses, "DATE_RETURNED") & "")
        'jtodd22 12/03/2004  we can treat a NULL or empty Returned date as today (or As of Date)plus one
        If sDateReturnedDTG & "" = "" Then sDateReturnedDTG = sAsOfDatePlusOneDTG
        
        'MITS 23694 jtodd22 01/24/2011
        'if the user has entered a cut-off date use it
            'jtodd 12/03/2004  If the Returned Date is after an "As Of Date" setting it to "As Of Date" plus one simplies the calculation
            If sDateReturnedDTG > sAsOfDatePlusOneDTG Then sDateReturnedDTG = sAsOfDatePlusOneDTG
        
        
        'jlt 01/27/2003 Besure to avoid records that start after the "As Of" date
        If sDateLastWorkedDTG <= sAsOfDateDTG Then
            'jlt 12/22/2003 by changing sDateLastWorkedDTG we can get neg days off because we have by-passed RISKMASTER's date validation
            'jlt 12/22/2003  If sDateReturnedDTG <> sDateLastWorkedDTG Then
            If (sDateReturnedDTG > sDateLastWorkedDTG) Or (Trim(sDateReturnedDTG) = "") Then
                'jlt the dates are the same there is no OSHA workloss
                If (lRowId = objItem.PiRowId) Then
                    'AFTER RULE CHANGE
                    If objItem.DateOfEvent > WORK_LOSS_RULE_CHANGE_DATE Then
                        iDaysOff = iDaysOff + (DateDiff("d", sDBDateFormat(sDateLastWorkedDTG, "Short Date"), sDBDateFormat(sDateReturnedDTG, "Short Date")) - 1)
                    Else 'BEFORE RULE CHANGE
                        If Trim(GetDataString(rsWorkLosses, "DURATION")) > "" Then
                            iDaysOff = iDaysOff + GetDataLong(rsWorkLosses, "DURATION")
                        Else
                            iDaysOff = iDaysOff + f_iGetDaysOffWork(sDBDateFormat(GetDataString(rsWorkLosses, "DATE_LAST_WORKED") & "", "Short Date"), rpt.AsOfDate, objItem.DaysOfWeek())
                        End If
                    End If
                End If      'If sDateReturnedDTG <> sDateLastWorkedDTG Then
            End If          'If sDateLastWorkedDTG <= sAsOfDateDTG Then
            lOSHAWorkLossDays = iDaysOff
            colWorkLosses.Add2 0, 0, sDateReturnedDTG, sDateLastWorkedDTG, lRowId, 0, lOSHAWorkLossDays
        End If
        
        g_objRocket.DB_MoveNext rsWorkLosses
        If Not g_objRocket.DB_EOF(rsWorkLosses) Then lRowId = GetDataLong(rsWorkLosses, "PI_ROW_ID")
    Wend
    objItem.DaysWorkLoss = iDaysOff
    If iDaysOff > 0 Then
        objItem.ChkBoxWorkLoss = "YES"
    End If
hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadWorkLoss", Erl, Err.Number, Err.Source, Err.Description, True
    DebugLogError "RptOsha.LoadWorkLoss", 0, 0, "Extra Info", "Load Work Loss for PI:" & objItem.PiRowId & vbCrLf & g_objRocket.DB_EnumRecordsets()
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      rpt.Errors.Add "LoadWorkLoss", Erl, g_objErr.Number, g_objErr.Source, SoftErrFormat(objItem.CaseNumber, "Work Loss Days calculation failed due to bad WorkLoss record(s) for " & objItem.EmployeeFullName & ".")
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadWorkLoss", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : LoadWorkLoss
' DateTime  : 12/3/2004 11:46
' Author    : jtodd22
' Purpose   :
'---------------------------------------------------------------------------------------
' Note      : Bad data bug found against Kaiser (ORACLE) where a date returned
' ..........: is found with no corresponding date last worked!
' ..........: Added Armor. BSB 2/20/2002
' Note      : Altered for change in 180 day rule.
' ..........: jtodd22  12/03/2004
' Note      : Found that "As Of Date" as not applied to records with a Returned to Work Date
' ..........: jtodd22  12/03/2004

Private Sub LoadWorkLoss(objItem As COSHAItem, rsWorkLosses As Integer, rpt As IOSHAReport)
    Dim iDaysOff As Integer
    Dim lRowId As Long
    Dim lOSHAWorkLossDays As Long               'MITS 14372
    Dim AsOfDate As String                      'jlt 01/24/2003
    Dim sAsOfDateDTG As String                  'jlt 01/27/2003
    Dim AsOfDatePlusOne As String               'jtodd22 012/03/2004
    Dim sAsOfDatePlusOneDTG As String           'jtodd22 012/03/2004
    Dim sDateLastWorkedDTG As String            'jlt 01/27/2003
    Dim sDateReturnedDTG As String              'jlt 01/27/2003
    Dim lRetry As Long
    On Error GoTo hErr
    'If No "as of" date specified then use today.
    'jlt 01/24/2003     If Not IsDate(sAsOfDate) Then sAsOfDate = Date
    If Not IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then      'jlt 01/24/2003
        AsOfDate = Date
        sAsOfDateDTG = Format(Date, "yyyymmdd")
    Else
        AsOfDate = sDBDateFormat(rpt.AsOfDate, "Short Date")
        sAsOfDateDTG = rpt.AsOfDate
    End If
    AsOfDatePlusOne = DateAdd("d", 1, AsOfDate)
    sAsOfDatePlusOneDTG = Format(AsOfDatePlusOne, "yyyymmdd")
    
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsWorkLosses) Then lRowId = GetDataLong(rsWorkLosses, "PI_ROW_ID")
    While Not g_objRocket.DB_EOF(rsWorkLosses) And lRowId <= objItem.PiRowId
        sDateLastWorkedDTG = GetDataString(rsWorkLosses, "DATE_LAST_WORKED") & ""
        If Trim$(sDateLastWorkedDTG) <> "" Then     'MITS 14372
            'jlt 12/22/2003 enforce first WL day after event date (earliest sDateLastWorkedDTG is objItem.DateOfEvent)
            If sDateLastWorkedDTG < objItem.DateOfEvent Then
                rpt.Errors.Add "LoadWorkLoss", Erl, "3201", "", SoftErrFormat(objItem.CaseNumber, "'Date Lasted Worked' is before 'Date of Event', 'Date Lasted Worked' was changed for Calculation Only")
                sDateLastWorkedDTG = objItem.DateOfEvent
            End If
            sDateReturnedDTG = Trim$(GetDataString(rsWorkLosses, "DATE_RETURNED") & "")
            'jtodd22 12/03/2004  we can treat a NULL or empty Returned date as today (or As of Date)plus one
            If sDateReturnedDTG & "" = "" Then sDateReturnedDTG = sAsOfDatePlusOneDTG
            'jtodd22 12/03/2004  If the Returned Date is after an "As Of Date" setting it to "As Of Date" plus one simplies the calculation
            'rsushilaggar 03/07/2011
            'If sDateReturnedDTG > sAsOfDatePlusOneDTG Then sDateReturnedDTG = sAsOfDatePlusOneDTG
            If IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then
                'jtodd22 12/03/2004  If the Returned Date is after an "As Of Date" setting it to "As Of Date" plus one simplies the calculation
                If sDateReturnedDTG > sAsOfDatePlusOneDTG Then sDateReturnedDTG = sAsOfDatePlusOneDTG
            End If

            
            'jlt 01/27/2003 Besure to avoid records that start after the "As Of" date
            If sDateLastWorkedDTG <= sAsOfDateDTG Then
                'jlt 12/22/2003 by changing sDateLastWorkedDTG we can get neg days off because we have by-passed RISKMASTER's date validation
                'jlt 12/22/2003  If sDateReturnedDTG <> sDateLastWorkedDTG Then
                If (sDateReturnedDTG > sDateLastWorkedDTG) Or (Trim(sDateReturnedDTG) = "") Then
                    'jlt the dates are the same there is no OSHA workloss
                    If (lRowId = objItem.PiRowId) Then
                        'AFTER RULE CHANGE
                        If objItem.DateOfEvent > WORK_LOSS_RULE_CHANGE_DATE Then
                            If sDateReturnedDTG <> "" Then
                                lOSHAWorkLossDays = (DateDiff("d", sDBDateFormat(sDateLastWorkedDTG, "Short Date"), sDBDateFormat(sDateReturnedDTG, "Short Date")) - 1)
                                iDaysOff = iDaysOff + lOSHAWorkLossDays
                            End If
                        Else 'BEFORE RULE CHANGE
                            If Trim(GetDataString(rsWorkLosses, "DURATION")) > "" Then
                                iDaysOff = iDaysOff + GetDataLong(rsWorkLosses, "DURATION")
                            Else
                                iDaysOff = iDaysOff + f_iGetDaysOffWork(sDBDateFormat(GetDataString(rsWorkLosses, "DATE_LAST_WORKED") & "", "Short Date"), rpt.AsOfDate, objItem.DaysOfWeek())
                            End If
                        End If
                        colWorkLosses.Add2 0, 0, sDateReturnedDTG, sDateLastWorkedDTG, lRowId, 0, lOSHAWorkLossDays
                    End If      'If sDateReturnedDTG <> sDateLastWorkedDTG Then
                End If          'If sDateLastWorkedDTG <= sAsOfDateDTG Then
            End If
        Else
            rpt.Errors.Add "LoadWorkLoss", Erl, "3204", "", SoftErrFormat(objItem.CaseNumber, "System required field 'Date Lasted Worked' is missing, record was not processed.")
        End If
        g_objRocket.DB_MoveNext rsWorkLosses
        If Not g_objRocket.DB_EOF(rsWorkLosses) Then lRowId = GetDataLong(rsWorkLosses, "PI_ROW_ID")
    Wend
    objItem.DaysWorkLoss = iDaysOff
    If iDaysOff > 0 Then
        objItem.ChkBoxWorkLoss = "YES"
    End If
hExit:
   g_objErr.Bubble
   Exit Sub
hErr:
    LogError "RptOSHA.LoadWorkLoss", Erl, Err.Number, Err.Source, Err.Description, True
    DebugLogError "RptOsha.LoadWorkLoss", 0, 0, "Extra Info", "Load Work Loss for PI:" & objItem.PiRowId & vbCrLf & g_objRocket.DB_EnumRecordsets()
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      rpt.Errors.Add "LoadWorkLoss", Erl, g_objErr.Number, g_objErr.Source, SoftErrFormat(objItem.CaseNumber, "Work Loss Days calculation failed due to bad WorkLoss record(s) for " & objItem.EmployeeFullName & ".")
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadWorkLoss", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Sub
Private Function FetchRestrictions(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchRestrictions")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Restriction Info", ElapsedSeconds, 0, 0
    
    'MITS 14372
    SQL = ""
    SQL = SQL & "SELECT PI_ROW_ID,"
    SQL = SQL & " PI_X_RESTRICT.DATE_FIRST_RESTRCT"
    SQL = SQL & ", PI_X_RESTRICT.DATE_LAST_RESTRCT"
    SQL = SQL & ", PI_X_RESTRICT.DURATION"
    SQL = SQL & " FROM PI_X_RESTRICT"
    SQL = SQL & " WHERE PI_ROW_ID IN " & sInPeopleInvolvedClause
    SQL = SQL & " ORDER BY PI_ROW_ID, PI_X_RESTRICT.DATE_FIRST_RESTRCT"     'must have order by PI_ROW_ID for efficiency and PI_X_RESTRICT.DATE_FIRST_RESTRCT for 180 day rule.
    
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchRestrictions = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
    Exit Function
hErr:
    rpt.Errors.Add "FetchRestrictions", Erl, Err.Number, Err.Source, SoftErrFormat("FetchRestrictions", "FetchRestrictions failed for people involved " & sInPeopleInvolvedClause & ".")
    GoTo hExit
    Resume
End Function
Private Function LoadRestrictedCollection(objItem As COSHAItem, rsRestrictions As Integer, rpt As IOSHAReport) As Long
    Dim lRetry As Long
    Dim iDaysRestrict As Integer
    Dim lRowId As Long
    Dim sDateFirstRestrictDTG As String         'jlt 01/27/2003
    Dim sDateLastRestrictDTG As String          'jlt 01/24/2003
    Dim AsOfDate As String                      'jlt 01/24/2003
    Dim sAsOfDateDTG As String                  'jlt 01/24/2003
    On Error GoTo hErr
    'If No "as of" date specified then use today.
    'jlt 01/24/2003 beware using "As Of Date" as it relates to first restrictied date
    If Not IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then      'jlt 01/24/2003
        AsOfDate = Date
        sAsOfDateDTG = Format(Date, "yyyymmdd")
    Else
        AsOfDate = sDBDateFormat(rpt.AsOfDate, "Short Date")
        sAsOfDateDTG = rpt.AsOfDate
    End If
    'jlt 01/24/2003 the varible AsOfDateDTG how contains the DTG date format of the current date or the Reports "As Of Date")
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsRestrictions) Then lRowId = GetDataLong(rsRestrictions, "PI_ROW_ID")
    
    While Not g_objRocket.DB_EOF(rsRestrictions) And lRowId <= objItem.PiRowId
        If (lRowId = objItem.PiRowId) Then
            sDateFirstRestrictDTG = GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & ""
            If Trim$(sDateFirstRestrictDTG) <> "" Then                  'MITS 14372
              If sDateFirstRestrictDTG = objItem.DateOfEvent Then       'MITS 14372
                  'jlt 01/27/2003 osha 300 rule says start counting days after date of event, not day of event.
                  rpt.Errors.Add "LoadRestrictedCollection", Erl, "3301", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is equal to 'Date of Event', 'Date First Restricted' was changed for Calculation Only")      'jlt 12/22/2003
                  sDateFirstRestrictDTG = Format(DateAdd("d", 1, sDBDateFormat(objItem.DateOfEvent, "Short Date")), "yyyymmdd")
              End If
              If sDateFirstRestrictDTG < objItem.DateOfEvent Then       'MITS 14372
                  'jlt 01/27/2003 osha 300 rule says start counting days after date of event, not day of event.
                  'jlt 02/11/2004 "Date Fired" changed to "Date First"
                  rpt.Errors.Add "LoadRestrictedCollection", Erl, "3301", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is before 'Date of Event', 'Date First Restricted' was changed for Calculation Only")      'jlt 12/22/2003
                  sDateFirstRestrictDTG = Format(DateAdd("d", 1, sDBDateFormat(objItem.DateOfEvent, "Short Date")), "yyyymmdd")
              End If
              sDateLastRestrictDTG = Trim$(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "")
              If IsDate(sDBDateFormat(sDateLastRestrictDTG, "Short Date")) Then
                  If sDateLastRestrictDTG < sDateFirstRestrictDTG Then
                      'jlt 01/27/2003 if dates are reversed then blow first restriction date passed "As Of" and computer's date so record is not used
                      rpt.Errors.Add "LoadRestrictedCollection", Erl, "3302", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is after 'Date Last Restricted', Record was not used for Calculation")                 'jlt 12/22/2203
                      sDateFirstRestrictDTG = Format(DateAdd("d", 10000, sDBDateFormat(sAsOfDateDTG, "Short Date")), "yyyymmdd")
                  End If
              End If
              If sDateFirstRestrictDTG <= sAsOfDateDTG Then   'jlt 01/24/2003
                  'jlt 01/24/2003 If the report uses an "As Of" date the later restriction records are filtered out
                  'AFTER RULE CHANGE
                  'sDateLastRestrictDTG
                  If objItem.DateOfEvent > WORK_LOSS_RULE_CHANGE_DATE Then
    
                      If Trim$(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT")) <> "" Then
                          'jlt 01/23/2003 iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & "", "Short Date"), sDBDateFormat(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "", "Short Date"))
                          If (GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "") <= sAsOfDateDTG Then
                              iDaysRestrict = iDaysRestrict + (DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), sDBDateFormat(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "", "Short Date")) + 1)
                          Else
                              iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), AsOfDate) + 1
                          End If
                      Else
                          'jlt 01/23/2003 iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & "", "Short Date"), Date) + 1
                          iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), AsOfDate) + 1
                      End If
                  Else 'BEFORE RULE CHANGE
                       If Trim(GetDataString(rsRestrictions, "DURATION") & "") > "" Then
                          iDaysRestrict = iDaysRestrict + GetDataInteger(rsRestrictions, "DURATION")
                      Else
                          iDaysRestrict = iDaysRestrict + f_iGetDaysOffWork(sDBDateFormat(GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & "", "Short Date"), Date$, objItem.DaysOfWeek())
                      End If
                  End If
              End If          'If sDateFirstRestrictDTG <= sAsOfDateDTG Then
            Else            'MITS 14372
                rpt.Errors.Add "LoadRestrictedCollection", Erl, "3301", "", SoftErrFormat(objItem.CaseNumber, "System required field 'Date First Restricted' is missing', record was not processed.")      'jtodd22 01/29/2009
            End If          'If Trim$(sDateFirstRestrictDTG) <> "" Then
        End If              'If (lRowId = objItem.PIRowId) Then
        
        g_objRocket.DB_MoveNext rsRestrictions
  
        If Not g_objRocket.DB_EOF(rsRestrictions) Then lRowId = GetDataLong(rsRestrictions, "PI_ROW_ID")
    Wend
    If iDaysRestrict > 0 Then
        objItem.ChkBoxRestriction = "YES"
    Else
        objItem.ChkBoxRestriction = "NO"
    End If
    objItem.DaysRestriction = iDaysRestrict
hExit:
   g_objErr.Bubble
   Exit Function
hErr:
    LogError "RptOSHA.LoadRestrictedCollection", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      rpt.Errors.Add "LoadRestrictedDays", Erl, Err.Number, Err.Source, SoftErrFormat(objItem.CaseNumber, "Restricted Days calculation failed due to bad restriction record(s) for " & objItem.EmployeeFullName & ".")
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadRestrictedCollection", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : LoadRestrictedDays
' DateTime  : 12/3/2004 13:49
' Author    : jtodd22
' Purpose   :
' Note      : Altered for 180 day rule procedure change.
' ..........: jtodd22 12/03/2004
'---------------------------------------------------------------------------------------
'
Private Function LoadRestrictedDays(objItem As COSHAItem, rsRestrictions As Integer, rpt As IOSHAReport) As Long
    Dim lRetry As Long
    Dim iDaysRestrict As Integer
    Dim lRowId As Long
    Dim sDateFirstRestrictDTG As String         'jlt 01/27/2003
    Dim sDateLastRestrictDTG As String          'jlt 01/24/2003
    Dim AsOfDate As String                      'jlt 01/24/2003
    Dim sAsOfDateDTG As String                  'jlt 01/24/2003
    Dim lOSHARestrictionDays As Long
    On Error GoTo hErr
    'If No "as of" date specified then use today.
    'jlt 01/24/2003 beware using "As Of Date" as it relates to first restrictied date
    If Not IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then      'jlt 01/24/2003
        AsOfDate = Date
        sAsOfDateDTG = Format(Date, "yyyymmdd")
    Else
        AsOfDate = sDBDateFormat(rpt.AsOfDate, "Short Date")
        sAsOfDateDTG = rpt.AsOfDate
    End If
    'jlt 01/24/2003 the varible AsOfDateDTG how contains the DTG date format of the current date or the Reports "As Of Date")
    'Prime the While Loop
    If Not g_objRocket.DB_EOF(rsRestrictions) Then lRowId = GetDataLong(rsRestrictions, "PI_ROW_ID")
    
    While Not g_objRocket.DB_EOF(rsRestrictions) And lRowId <= objItem.PiRowId
        If (lRowId = objItem.PiRowId) Then
            sDateFirstRestrictDTG = GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & ""
            If Trim$(sDateFirstRestrictDTG) <> "" Then              'MITS 14372
              If sDateFirstRestrictDTG = objItem.DateOfEvent Then   'MITS 14372
                  'jlt 01/27/2003 osha 300 rule says start counting days after date of event, not day of event.
                  'jlt 02/11/2004 "Date Fired" changed to "Date First"
                  rpt.Errors.Add "LoadRestrictedDays", Erl, "3301", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is equal to 'Date of Event', 'Date First Restricted' was changed for Calculation Only")      'jlt 12/22/2003
                  sDateFirstRestrictDTG = Format(DateAdd("d", 1, sDBDateFormat(objItem.DateOfEvent, "Short Date")), "yyyymmdd")
              End If
              If sDateFirstRestrictDTG <= objItem.DateOfEvent Then
                  'jlt 01/27/2003 osha 300 rule says start counting days after date of event, not day of event.
                  'jlt 02/11/2004 "Date Fired" changed to "Date First"
                  rpt.Errors.Add "LoadRestrictedDays", Erl, "3301", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is before 'Date of Event', 'Date First Restricted' was changed for Calculation Only")      'jlt 12/22/2003
                  sDateFirstRestrictDTG = Format(DateAdd("d", 1, sDBDateFormat(objItem.DateOfEvent, "Short Date")), "yyyymmdd")
              End If
              sDateLastRestrictDTG = Trim$(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "")
              If IsNull(sDateLastRestrictDTG) Or sDateLastRestrictDTG = "" Then
                  sDateLastRestrictDTG = sAsOfDateDTG
              End If
              If IsDate(sDBDateFormat(sDateLastRestrictDTG, "Short Date")) Then
                  If sDateLastRestrictDTG < sDateFirstRestrictDTG Then
                      'jlt 01/27/2003 if dates are reversed then blow first restriction date passed "As Of" and computer's date so record is not used
                      rpt.Errors.Add "LoadRestrictedDays", Erl, "3302", "", SoftErrFormat(objItem.CaseNumber, "'Date First Restricted' is after 'Date Last Restricted', Record was not used for Calculation")                 'jlt 12/22/2203
                      sDateFirstRestrictDTG = Format(DateAdd("d", 10000, sDBDateFormat(sAsOfDateDTG, "Short Date")), "yyyymmdd")
                  End If
              End If
              If sDateFirstRestrictDTG <= sAsOfDateDTG Then   'jlt 01/24/2003
                  'jlt 01/24/2003 If the report uses an "As Of" date the later restriction records are filtered out
                  'AFTER RULE CHANGE
                  'sDateLastRestrictDTG
                  If objItem.DateOfEvent > WORK_LOSS_RULE_CHANGE_DATE Then
    
                      If sDateLastRestrictDTG <> "" Then
                          'jlt 01/23/2003 iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & "", "Short Date"), sDBDateFormat(GetDataString(rsRestrictions, "DATE_LAST_RESTRCT") & "", "Short Date"))
                          If sDateLastRestrictDTG <= sAsOfDateDTG Then
                            lOSHARestrictionDays = (DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), sDBDateFormat(sDateLastRestrictDTG, "Short Date")) + 1)
                              iDaysRestrict = iDaysRestrict + lOSHARestrictionDays
                          Else
                            'MITS 23694 jtodd22 01/24/2011
                            'if the user has entered a cut-off date use it
                            If IsDate(sDBDateFormat(rpt.AsOfDate, "Short Date")) Then
                                'jtodd22 12/03/2004  If the Returned Date is after an "As Of Date" setting it to "As Of Date" plus one simplies the calculation
                                If sDateLastRestrictDTG > sAsOfDateDTG Then sDateLastRestrictDTG = sAsOfDateDTG
                            End If
                            lOSHARestrictionDays = (DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), sDBDateFormat(sDateLastRestrictDTG, "Short Date")) + 1)
                            iDaysRestrict = iDaysRestrict + lOSHARestrictionDays
                          End If
                      Else
                          'jlt 01/23/2003 iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(GetDataString(rsRestrictions, "DATE_FIRST_RESTRCT") & "", "Short Date"), Date) + 1
                          iDaysRestrict = iDaysRestrict + DateDiff("d", sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), AsOfDate) + 1
                      End If
                  Else 'BEFORE RULE CHANGE
                       If Trim(GetDataString(rsRestrictions, "DURATION") & "") > "" Then
                          iDaysRestrict = iDaysRestrict + GetDataInteger(rsRestrictions, "DURATION")
                      Else
                          iDaysRestrict = iDaysRestrict + f_iGetDaysOffWork(sDBDateFormat(sDateFirstRestrictDTG, "Short Date"), Date$, objItem.DaysOfWeek())
                      End If
                  End If
                  colRestrictions.Add2 0, 0, sDateLastRestrictDTG, 0, sDateFirstRestrictDTG, lRowId, 0, lOSHARestrictionDays
                  
              End If      'If sDateFirstRestrictDTG <= sAsOfDateDTG Then
            Else        'MITS 14372
                  rpt.Errors.Add "LoadRestrictedDays", Erl, "3303", "", SoftErrFormat(objItem.CaseNumber, "System required field 'Date First Restricted' is missing, record was not processed.")      'jlt 12/22/2003
            End If
        End If          'If (lRowId = objItem.PIRowId) Then
        g_objRocket.DB_MoveNext rsRestrictions
  
        If Not g_objRocket.DB_EOF(rsRestrictions) Then lRowId = GetDataLong(rsRestrictions, "PI_ROW_ID")
    Wend
    If iDaysRestrict > 0 Then
        objItem.ChkBoxRestriction = "YES"
    Else
        objItem.ChkBoxRestriction = "NO"
    End If
    objItem.DaysRestriction = iDaysRestrict
hExit:
   g_objErr.Bubble
   Exit Function
hErr:
    LogError "RptOSHA.LoadRestrictedDays", Erl, Err.Number, Err.Source, Err.Description, True
    'BSB 02.19.2003 Timeout Error Retry Logic
    If lRetry > 15 Or InStr(UCase(g_objErr.Description), "TIMEOUT") = 0 Then
      rpt.Errors.Add "LoadRestrictedDays", Erl, Err.Number, Err.Source, SoftErrFormat(objItem.CaseNumber, "Restricted Days calculation failed due to bad restriction record(s) for " & objItem.EmployeeFullName & ".")
      GoTo hExit
    Else
      lRetry = lRetry + 1
      LogError "RptOSHA.LoadRestrictedDays", 0, 0, "Error Handler", "Make Retry Attempt # " & lRetry
      g_objErr.Clear
      Resume
    End If
End Function

Private Function FetchTreatments(rpt As Object, sInPeopleInvolvedClause As String) As Integer
    Dim ElapsedSeconds As Long
    Dim SQL As String
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchTreatments")
    '**************************************
    ' Check for Abort and Placate the Client with some Status
    '**************************************
    If rpt.Notify.CheckAbort Then AbortRequested ("FetchTreatments")
    ElapsedSeconds = DateDiff("s", rpt.StartTime, Now)
    rpt.Notify.UpdateProgress "Retrieving Osha Treatment Info", ElapsedSeconds, 0, 0
    
    SQL = "SELECT PI_ROW_ID, SHORT_CODE FROM PI_X_TREATMENT, CODES WHERE PI_X_TREATMENT.PI_ROW_ID IN " & sInPeopleInvolvedClause & "  AND PI_X_TREATMENT.TREATMENT_CODE=CODES.CODE_ID ORDER BY PI_ROW_ID"
#If BB_DUMP_SQL Then
    f.WriteLine SQL
#End If
    FetchTreatments = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, rpt.Context.SafeSQL(SQL), DB_FORWARD_ONLY, 0)
hExit:
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchTreatments", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit

End Function
Public Function BuildDeptClause(rpt As Object)
    Dim SQL As String
    Dim s As String
    Dim vTmp As Variant
    On Error GoTo hErr
    
    s = ""
    If rpt.ByOSHAEstablishmentFlag Then
        For Each vTmp In rpt.SelectedEntities
            AddDelimited s, CStr(vTmp)
        Next
        SQL = " (" & s & ")"
    Else
        If rpt.AllEntitiesFlag Then
             'Get all entities for everything under the "Use report"
             'In theory, the "all entities" flag will end up including all departments
             'whether we start with the "use report at" level or the "Report On" level.
             SQL = "SELECT DISTINCT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE "
             SQL = SQL & rpt.sUseReportLevel & "=" & rpt.ReportOn
             'SQL = SQL & rpt.sUseReportLevel & "=" & rpt.ReportOn
         Else
             For Each vTmp In rpt.SelectedEntities
                 AddDelimited s, CStr(vTmp)
             Next
             If Trim(s) = "" Or Val(s) = 0 Then
                LogError "RptOSHA.BuildDeptClause", Erl, 3032, "RptOSHA", "Department Clause is empty."
             End If
             s = " (" & s & ")"
             SQL = "SELECT DISTINCT DEPARTMENT_EID FROM ORG_HIERARCHY "
             SQL = SQL & " WHERE ORG_HIERARCHY." & rpt.sReportLevel & " IN " & s
         End If
    End If
hExit:
    BuildDeptClause = " (" & SQL & ")"
    DebugMsg (BuildDeptClause)
    Exit Function
hErr:
    LogError "RptOSHA.BuildDeptClause", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function


'To Do - cover case of next day return. Call it what?  One day off? or No days off?
'Talked w/ JLT: A same day return is 0, a next day return is also 0, Mon-Wed would be 1... etc.
'NOTES: I believe this function to be wholly innacurate. 02/21/02 BSB
' However, since it is no longer in use for real OSHA300 reports and is only used
' to run comparison reports against previous years OSHA200 reports. It should
' not be changed. (No point in making the results look different w/o good reason.)
Private Function f_iGetDaysOffWork(date1 As String, date2 As String, DaysOfWeek() As Integer) As Integer
Dim Days As Integer
Dim Newdays As Integer
'Dim DayOfWeekStart As Integer
'Dim DayOfWeekEnd As Integer
Dim i As Integer

    On Error GoTo hErr
    f_iGetDaysOffWork = 0
    'Check valid parameters. (Return -1 to indicate bad data)
    If Not IsDate(date1) Then Err.Raise 3030, , "Invalid date last worked."
    If Not IsDate(date2) Then Err.Raise 3031, , "Invalid date returned to work."
    
    If date1 = date2 Then Exit Function 'return zero - same day return.
    If DateAdd("d", 1, date1) = date2 Then Exit Function 'return zero - following day return.
    
    date1 = DateAdd("d", -1, date1)
    Days = DateDiff("d", date1, date2)
'    DayOfWeekStart = Weekday(date1)
'    DayOfWeekEnd = Weekday(date2)
    Newdays = 0
    For i = 1 To Days
        If DaysOfWeek(Weekday(DateAdd("d", i - 1, date1))) Then Newdays = Newdays + 1
    Next i
    f_iGetDaysOffWork = Newdays
hExit:
    Exit Function
hErr:
    LogError "RptOSHA_2001.f_iGetDaysOffWork", Erl, Err.Number, Err.Source, Err.Description
    f_iGetDaysOffWork = -1 '(Return -1 to indicate problem data)
    GoTo hExit
End Function

Private Function f_sRemoveDateTimeStamp(ByVal s As String) As String
Dim iPos As Integer
Dim sTemp As String
    On Error GoTo hErr
    f_sRemoveDateTimeStamp = ""
    sTemp = Trim$(s)
    iPos = InStr(1, sTemp, " ", vbTextCompare)
    'remove date
    If IsDate(Left$(sTemp, iPos)) Then
        sTemp = Trim$(Mid$(sTemp, iPos))
    End If
    'remove time
    If IsDate(Left$(sTemp, 8)) Then
        sTemp = Trim$(Mid$(sTemp, 9))
    End If
    f_sRemoveDateTimeStamp = sTemp
hExit:
    Exit Function
hErr:
    LogError "RptOSHA_2001.f_sRemoveDateTimeStamp", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function

Private Function GetDaysOfWeek(rsMain As Integer) As Integer()
    Dim iDaysOfWeek() As Integer
    ReDim iDaysOfWeek(1 To 7)
    '******************************
    ' rsMain is expected to remain on the appropriate
    ' person involved record.
    '******************************
    iDaysOfWeek(1) = GetDataInteger(rsMain, "WORK_SUN_FLAG")
    iDaysOfWeek(2) = GetDataInteger(rsMain, "WORK_MON_FLAG")
    iDaysOfWeek(3) = GetDataInteger(rsMain, "WORK_TUE_FLAG")
    iDaysOfWeek(4) = GetDataInteger(rsMain, "WORK_WED_FLAG")
    iDaysOfWeek(5) = GetDataInteger(rsMain, "WORK_THU_FLAG")
    iDaysOfWeek(6) = GetDataInteger(rsMain, "WORK_FRI_FLAG")
    iDaysOfWeek(7) = GetDataInteger(rsMain, "WORK_SAT_FLAG")
    GetDaysOfWeek = iDaysOfWeek
End Function

Public Function SoftErrFormat(CaseNumber As String, desc As String) As String
    SoftErrFormat = "Case Number:" & CaseNumber & vbTab & "Message: " & desc
End Function

Public Function FetchSignature(rpt As IOSHAReport) As IPictureDisp
    On Error GoTo hErr
    ' JP 12/15/2002   Print signature instead of text name if signature is specified
    Dim sSignature As String
    Dim rs As Integer
    Dim oZip As New clsZIPUtil
    Dim sImageFile As String
    Dim vData As Variant
    Set FetchSignature = Nothing
    
    sSignature = ""
    rs = g_objRocket.DB_CreateRecordset(rpt.Context.hDb, "SELECT * FROM OSHA_OPTIONS WHERE ROW_ID = 1", DB_FORWARD_ONLY, 0)
    If Not g_objRocket.DB_EOF(rs) Then
        'BSB 02.06.2003  Even though this didn't cause an error, it logs to the error log.
        'Let's do it the old way and eat any errors. (This field will be missing on non-6.0 data bases.)
        'sSignature = GetDataString(rs, "OSHA300A_SIGNATURE") & ""
        On Error Resume Next
        g_objRocket.DB_GetData rs, "OSHA300A_SIGNATURE", vData
        sSignature = "" & vData
        If sSignature = "" Then GoTo hExit
        On Error GoTo hErr
    End If
    
        sImageFile = oZip.UnZIPBufferToTempFile(sSignature)
        Set FetchSignature = LoadPicture(sImageFile)
        Kill sImageFile    ' clean up temp file when we're done with it
hExit:
  SafeCloseRecordset rs
  g_objErr.Bubble
  Exit Function
hErr:
  LogError "RptOSHA.FetchSignature", Erl, Err.Number, Err.Source, Err.Description, True
  GoTo hExit

End Function

'BSB 01.13.2003
Public Function LoadExposureInfo(rpt As COSHA300AReport) As Boolean
Dim iRS As Integer
Dim lDays As Long, lTotalDays As Long
Dim lg As Long
Dim lHours As Long
Dim lPersonDays As Long
Dim lTotalHours As Long
Dim lTotalPersonDays As Long
Dim lRecordCount As Long
Dim sSQL As String
Dim lPrevEID As Long
Dim bAdd As Boolean
On Error GoTo hErr
    
    Set rpt = rpt
    
    LoadExposureInfo = False
    'select fields
    sSQL = "SELECT DISTINCT" & _
           " ENTITY_EXPOSURE.EXPOSURE_ROW_ID" & _
           ", ENTITY_EXPOSURE.ENTITY_ID" & _
           ", ENTITY_EXPOSURE.NO_OF_EMPLOYEES" & _
           ", ENTITY_EXPOSURE.NO_OF_WORK_HOURS" & _
           ", ENTITY_EXPOSURE.START_DATE" & _
           ", ENTITY_EXPOSURE.END_DATE" & _
           " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard) & _
           " AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID" & _
           " AND ENTITY_EXPOSURE.START_DATE >= '" & rpt.Standard.BeginDate & "'" & _
           " AND ENTITY_EXPOSURE.END_DATE <= '" & rpt.Standard.EndDate & "'" & _
           " AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" & _
           " AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL" & _
           " ORDER BY ENTITY_EXPOSURE.ENTITY_ID"
    
    iRS = g_objRocket.DB_CreateRecordset(rpt.Standard.Context.hDb, rpt.Standard.Context.SafeSQL(sSQL), DB_FORWARD_ONLY, 0)
    lg = 0
    lTotalDays = 0     ' JP 12/16/2002
    lRecordCount = 0
    lPrevEID = 0
    rpt.ExposureAvgEmployeeCount = 0
    rpt.ExposureTotalHoursWorked = 0
    Do While Not g_objRocket.DB_EOF(iRS)
        lPrevEID = GetDataLong(iRS, "ENTITY_ID")
  
        lRecordCount = lRecordCount + 1
 
        lDays = DateDiff("d", sDBDateFormat(GetDataString(iRS, "START_DATE"), "Short Date"), sDBDateFormat(GetDataString(iRS, "END_DATE"), "Short Date")) + 1
        lTotalDays = lTotalDays + lDays
  
        lPersonDays = lDays * GetDataLong(iRS, "NO_OF_EMPLOYEES")
        lTotalPersonDays = lTotalPersonDays + lPersonDays
  
  
        lHours = GetDataLong(iRS, "NO_OF_WORK_HOURS")
        lTotalHours = lTotalHours + lHours
  
        g_objRocket.DB_MoveNext iRS
  
        ' Now, add average into total if we're off the department
        bAdd = False
        If g_objRocket.DB_EOF(iRS) Then
            bAdd = True
        ElseIf (GetDataLong(iRS, "ENTITY_ID") <> lPrevEID) Then
            bAdd = True
        End If
  
        If bAdd Then
            If lTotalDays > 0 Then
               rpt.ExposureAvgEmployeeCount = rpt.ExposureAvgEmployeeCount + lTotalPersonDays / lTotalDays     ' JP 12/16/2002
            End If

            lTotalPersonDays = 0
            lTotalDays = 0
            bAdd = False
        End If
    Loop
    
    rpt.ExposureTotalHoursWorked = lTotalHours
    DebugMsg "rpt.ExposureAvgEmployeeCount: " & rpt.ExposureAvgEmployeeCount & " rpt.ExposureTotalHoursWorked:" & rpt.ExposureTotalHoursWorked
    LoadExposureInfo = True
hExit:
    SafeCloseRecordset iRS
    g_objErr.Bubble
    Exit Function
hErr:
    LogError "RptOSHA.LoadExposureInfo", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
End Function
Public Function LoadExposureInfo2(rpt As COSHA300AReport) As Boolean
Dim iConn As Integer
Dim iDBMake As Integer
Dim iRS As Integer
Dim lDays As Long, lTotalDays As Long
Dim lg As Long
Dim lHours As Long
Dim lPersons As Long
Dim lTotalHours As Long
Dim lTotalPersons As Long
Dim lRecordCount As Long
Dim sSQL2 As String
Dim sSQL3 As String
Dim sSQL4 As String
Dim sSQL5 As String
Dim sSQL6 As String
Dim lPrevEID As Long

Dim col As Collection
Dim vItem As Variant

On Error GoTo hErr
    
    Set rpt = rpt
    
    LoadExposureInfo2 = False
    lg = 0
    lTotalDays = 0     ' JP 12/16/2002
    lRecordCount = 0
    lPrevEID = 0
    rpt.ExposureAvgEmployeeCount = 0
    rpt.ExposureTotalHoursWorked = 0
    
    iDBMake = g_objRocket.DB_GetDBMake(rpt.Standard.Context.hDb)
    '**************************************************************************************************
    'classify records by days in record
    sSQL2 = "SELECT DISTINCT" & _
            " ENTITY_EXPOSURE.EXPOSURE_ROW_ID" & _
            ", ENTITY_EXPOSURE.ENTITY_ID" & _
            ", ENTITY_EXPOSURE.START_DATE" & _
            ", ENTITY_EXPOSURE.END_DATE" & _
           " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard) & _
           " AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID" & _
           " AND ENTITY_EXPOSURE.START_DATE >= '" & rpt.Standard.BeginDate & "'" & _
           " AND ENTITY_EXPOSURE.END_DATE <= '" & rpt.Standard.EndDate & "'" & _
           " AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" & _
           " AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL"

    iRS = g_objRocket.DB_CreateRecordset(rpt.Standard.Context.hDb, rpt.Standard.Context.SafeSQL(sSQL2), DB_FORWARD_ONLY, 0)
    Do While Not g_objRocket.DB_EOF(iRS)
        lDays = DateDiff("d", sDBDateFormat(GetDataString(iRS, "START_DATE"), "Short Date"), sDBDateFormat(GetDataString(iRS, "END_DATE"), "Short Date")) + 1
        sSQL3 = "UPDATE ENTITY_EXPOSURE SET DAY_COUNT = " & lDays & " WHERE EXPOSURE_ROW_ID = " & GetDataLong(iRS, "EXPOSURE_ROW_ID")
        If (iDBMake = 1 Or iDBMake = 2) Then
            iConn = g_objRocket.DB_OpenDatabase(g_hEnv, rpt.Standard.Context.DSN, 0)
            g_objRocket.DB_SQLExecute iConn, sSQL3
            g_objRocket.DB_CloseDatabase (iConn)
        Else
            g_objRocket.DB_SQLExecute rpt.Standard.Context.hDb, sSQL3
        End If
        g_objRocket.DB_MoveNext iRS
    Loop
    SafeCloseRecordset iRS
    '**********************************************************************************************
    'make collection of record types
    sSQL4 = "SELECT DISTINCT" & _
            " ENTITY_EXPOSURE.DAY_COUNT" & _
            " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard) & _
           " AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID" & _
           " AND ENTITY_EXPOSURE.START_DATE >= '" & rpt.Standard.BeginDate & "'" & _
           " AND ENTITY_EXPOSURE.END_DATE <= '" & rpt.Standard.EndDate & "'" & _
           " AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" & _
           " AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL"
    iRS = g_objRocket.DB_CreateRecordset(rpt.Standard.Context.hDb, rpt.Standard.Context.SafeSQL(sSQL4), DB_FORWARD_ONLY, 0)
    Set col = New Collection
    Do While Not g_objRocket.DB_EOF(iRS)
        col.Add GetDataLong(iRS, 1)
        g_objRocket.DB_MoveNext iRS
    Loop
    SafeCloseRecordset iRS
    '**************************************************************************************************************
    'make and sum employee counts
    sSQL5 = "SELECT DISTINCT" & _
           "  SUM(ENTITY_EXPOSURE.NO_OF_EMPLOYEES)  | S_EMPLOYEES" & _
           ", SUM(ENTITY_EXPOSURE.NO_OF_WORK_HOURS) | S_HOURS" & _
           ", MIN(ENTITY_EXPOSURE.START_DATE) | M_START_DATE" & _
           ", MAX(ENTITY_EXPOSURE.END_DATE)  | M_END_DATE" & _
           " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard) & _
           " AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID" & _
           " AND ENTITY_EXPOSURE.START_DATE >= '" & rpt.Standard.BeginDate & "'" & _
           " AND ENTITY_EXPOSURE.END_DATE <= '" & rpt.Standard.EndDate & "'" & _
           " AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" & _
           " AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL"
    For Each vItem In col
       sSQL6 = sSQL5 & " AND ENTITY_EXPOSURE.DAY_COUNT = " & vItem
       iRS = g_objRocket.DB_CreateRecordset(rpt.Standard.Context.hDb, rpt.Standard.Context.SafeSQL(sSQL6), DB_FORWARD_ONLY, 0)
       Do While Not g_objRocket.DB_EOF(iRS)
           lRecordCount = lRecordCount + 1
           lDays = DateDiff("d", sDBDateFormat(GetDataString(iRS, "M_START_DATE"), "Short Date"), sDBDateFormat(GetDataString(iRS, "M_END_DATE"), "Short Date")) + 1
           lPersons = GetDataLong(iRS, "S_EMPLOYEES")
           lTotalPersons = lTotalPersons + lPersons / (lDays / vItem)
           lHours = GetDataLong(iRS, "S_HOURS")
           lTotalHours = lTotalHours + lHours
           g_objRocket.DB_MoveNext iRS
        Loop
        SafeCloseRecordset iRS
        rpt.ExposureAvgEmployeeCount = rpt.ExposureAvgEmployeeCount + lTotalPersons
        rpt.ExposureTotalHoursWorked = rpt.ExposureTotalHoursWorked + lTotalHours
        lTotalPersons = 0
   Next
    
    DebugMsg "rpt.ExposureAvgEmployeeCount: " & rpt.ExposureAvgEmployeeCount & " rpt.ExposureTotalHoursWorked:" & rpt.ExposureTotalHoursWorked
    LoadExposureInfo2 = True
hExit:
    SafeCloseRecordset iRS
    g_objErr.Bubble
    Exit Function
hErr:
    LogError "RptOSHA.LoadExposureInfo2", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
    Resume
End Function
Public Function LoadExposureInfo3(rpt As COSHA300AReport) As Boolean
Dim bPresent As Boolean
Dim bHave24Pays As Boolean
Dim bHave26Pays As Boolean
Dim l As Long
Dim ll As Long
Dim iDBMake As Integer
Dim iRS As Integer
Dim lCount As Long
Dim lDays As Long, lTotalDays As Long
Dim lg As Long
Dim lPayRange As Long
Dim lPersons As Long
Dim lTotalHours As Long
Dim lTotalPersons As Long
Dim lRecordCount As Long
Dim sEndDate As String
Dim sLastDate As String
Dim sStartDate As String
Dim sSQL2 As String
Dim vItem As Variant
Dim lPrevEID As Long
Dim colPayRanges As Collection
Dim objExposure As New CExposure
Dim colExposures As New CExposures
Dim sTmp As String
On Error GoTo hErr
    
    Set rpt = rpt
    
    LoadExposureInfo3 = False
    lg = 0
    lTotalDays = 0     ' JP 12/16/2002
    lRecordCount = 0
    lPrevEID = 0
    rpt.ExposureAvgEmployeeCount = 0
    rpt.ExposureTotalHoursWorked = 0
    sLastDate = ""
    
    sTmp = "Reading Exposure Records"
    rpt.Standard.Notify.UpdateProgress sTmp, 0, (0 / 1), 0

    iDBMake = g_objRocket.DB_GetDBMake(rpt.Standard.Context.hDb)
    '**************************************************************************************************
    'classify records by days in record
    sSQL2 = "SELECT DISTINCT" & _
            " ENTITY_EXPOSURE.EXPOSURE_ROW_ID" & _
            ", ENTITY_EXPOSURE.NO_OF_EMPLOYEES" & _
            ", ENTITY_EXPOSURE.NO_OF_WORK_HOURS" & _
            ", ENTITY_EXPOSURE.ENTITY_ID" & _
            ", ENTITY_EXPOSURE.START_DATE" & _
            ", ENTITY_EXPOSURE.END_DATE"
    If rpt.Standard.ByOSHAEstablishmentFlag <> True Then
        sSQL2 = sSQL2 & " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard) & _
           " AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID"
    Else
        sSQL2 = sSQL2 & " FROM ENTITY_EXPOSURE" & _
           " WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " & BuildDeptClause(rpt.Standard)
    End If
    sSQL2 = sSQL2 & " AND ENTITY_EXPOSURE.START_DATE >= '" & rpt.Standard.BeginDate & "'" & _
           " AND ENTITY_EXPOSURE.END_DATE <= '" & rpt.Standard.EndDate & "'" & _
           " AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" & _
           " AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL"
    sSQL2 = sSQL2 & " ORDER BY ENTITY_EXPOSURE.START_DATE"          'need to resolve 24 or 26 pays per year
    
    iRS = g_objRocket.DB_CreateRecordset(rpt.Standard.Context.hDb, rpt.Standard.Context.SafeSQL(sSQL2), DB_FORWARD_ONLY, 0)
    bHave24Pays = False
    bHave26Pays = False
    
    sTmp = "Calculating Exposure"
    rpt.Standard.Notify.UpdateProgress sTmp, 0, (lRecordCount / 1), 0
    
    Do While Not g_objRocket.DB_EOF(iRS)
        With objExposure
            lDays = DateDiff("d", sDBDateFormat(GetDataString(iRS, "START_DATE"), "Short Date"), sDBDateFormat(GetDataString(iRS, "END_DATE"), "Short Date")) + 1
            If lDays > 88 And lDays < 94 Then lDays = 90            'fix for days in quarter, beware Feburary
            If lDays > 27 And lDays < 32 Then lDays = 30            'fix for days in month, beware Feburary
            If lDays = 13 Or lDays = 15 Or lDays = 16 Then          'fix for 24 pays per year, beware Feburary
                lDays = 15
                bHave24Pays = True
            End If
            'fix for 24 pays per year during leap years
            If Left(GetDataString(iRS, "END_DATE"), 4) = "0229" And lDays = 14 And bHave24Pays = True Then
                'Programmer assumes two-week pay start on Sun or Mon
                'This could be a real two-week pay in 2004 Mon-Sun, 01/01/2004 is Thu
                'This could be a real two-week pay in 2020 Sun-Sat, 01/01/2020 is Wed
                'this could be a real two-week pay in 2032 Mon-Sun, 01/01/2032 is Thu
                If bHave26Pays = False Then
                    lDays = 15
                Else
                    'we have a mix of "two-week" and "twice-a-month" pays
                    If InStr(1, Right(GetDataString(iRS, "END_DATE"), 4), "2004,2020,2032") = 0 Then
                        lDays = 15
                    Else
                        'we have an issue, too many "assumed" conditions
                        'employees for 26 pay per year will inflate, for 24 pay per year will deflate
                        'net employees on 300A is not predicted
                        'leave lDays alone
                    End If
                End If
            Else
                'do nothing lDays is good at 14, based on not having any 24 pays-per-year in the
                'first six (6) weeks of the year
            End If
            .DayCount = lDays
            .EndDate = GetDataString(iRS, "END_DATE")
            If .EndDate < Left(.EndDate, 4) & "0228" And .DayCount = 14 Then bHave26Pays = True
            .Employees = GetDataLong(iRS, "NO_OF_EMPLOYEES")
            .Hours = GetDataLong(iRS, "NO_OF_WORK_HOURS")
            .RowID = GetDataLong(iRS, "EXPOSURE_ROW_ID")
            .StartDate = GetDataString(iRS, "START_DATE")
            colExposures.Add .DayCount, .EndDate, .StartDate, .Hours, .Employees, .RowID, "k" & CStr(.RowID)
        End With
        g_objRocket.DB_MoveNext iRS
    Loop
    SafeCloseRecordset iRS
    '**********************************************************************************************
    'make collection of record types
    Set colPayRanges = New Collection
    For l = 1 To colExposures.Count
        bPresent = False
        For ll = 1 To colPayRanges.Count
            If colExposures.Item(l).DayCount = colPayRanges.Item(ll) Then bPresent = True
        Next
        If bPresent = False Then
            colPayRanges.Add colExposures.Item(l).DayCount
        End If
    Next
    '**************************************************************************************************************
    'make and sum employee counts
    sTmp = "Begin Exposure Summing"
    rpt.Standard.Notify.UpdateProgress sTmp, 0, (0 / 1), 0
    lCount = 0
    For Each vItem In colPayRanges
        lCount = lCount + 1
        lPayRange = vItem
        lTotalPersons = 0
        lTotalHours = 0
        sStartDate = "21000000"             'jlt someting to start date check
        For l = 1 To colExposures.Count
        sTmp = "Exposure Summing; " & lCount & " of " & colPayRanges.Count & "; " & l & " of " & colExposures.Count
        rpt.Standard.Notify.UpdateProgress sTmp, 0, 0, 0
            If colExposures.Item(l).DayCount = lPayRange Then
                If colExposures.Item(l).EndDate > sEndDate Then sEndDate = colExposures.Item(l).EndDate
                If colExposures.Item(l).StartDate < sStartDate Then sStartDate = colExposures.Item(l).StartDate
                lTotalPersons = lTotalPersons + colExposures.Item(l).Employees
                lTotalHours = lTotalHours + colExposures.Item(l).Hours
            End If
        Next
        lDays = DateDiff("d", sDBDateFormat(sStartDate, "Short Date"), sDBDateFormat(sEndDate, "Short Date")) + 1
        'Because of the monthly and quarterly pay types force lDays to single data type
        lTotalPersons = lTotalPersons / (CSng(lDays) / vItem)
        rpt.ExposureAvgEmployeeCount = rpt.ExposureAvgEmployeeCount + lTotalPersons
        rpt.ExposureTotalHoursWorked = rpt.ExposureTotalHoursWorked + lTotalHours
    Next
    
    DebugMsg "rpt.ExposureAvgEmployeeCount: " & rpt.ExposureAvgEmployeeCount & " rpt.ExposureTotalHoursWorked:" & rpt.ExposureTotalHoursWorked
    LoadExposureInfo3 = True
hExit:
    SafeCloseRecordset iRS
    g_objErr.Bubble
    Exit Function
hErr:
    LogError "RptOSHA.LoadExposureInfo3", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
    Resume
End Function
Public Function sPadDateWithZeros(sDate) As String
'new jtodd22 12/03/2004
Dim Seperator As String
    'expected mm/dd/ccyy
    'will fix m/d/ccyy
    sDate = Replace(sDate, " ", "")
    sDate = Trim(sDate)
    sPadDateWithZeros = sDate
    If Not IsDate(sDate) Then
        'not a valid date, go away
        Exit Function
    End If
    If Len(sDate) = 10 Then
        sPadDateWithZeros = sDate
        Exit Function
    End If
    
    Seperator = ""
    If InStr(1, sDate, "/") > 0 Then Seperator = "/"
    If InStr(1, sDate, "-") > 0 Then Seperator = "-"
    If Seperator = "" Then Exit Function                    'unknown seperator
    
    If InStr(1, sDate, Seperator) = 2 Then
        sDate = "0" & sDate
        ' is 0m/d/* or 0m/dd/*
    End If
    'days are done
    If InStr(4, sDate, Seperator) = 5 Then
        sDate = Left(sDate, 3) & "0" & Mid(sDate, 4)
    End If
    'months are done
    Select Case Len(sDate)
        Case 8
            'century is missing, OSHA 300 applies to events after 2000, therefore century is 20
            If InStr(4, sDate, Seperator) = 6 Then
                sDate = Left(sDate, 6) & "20" & Right(sDate, 2)
            End If
        Case 10
            'good thing, expected
        Case Else
            'bad thing
    End Select
    sPadDateWithZeros = sDate
End Function


