VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CInstanceUtilities"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'*************************************
' Wrap the "general" Db functions inside of this
' Per Instance Utility class to be able to pick
' which DSN is used rather than have a single "global DSN"
' I suspect that a global DSN can cause problems if
' one web server is hosting this dll and it creates more than
' one object instance which previously reset the g_hdb
' and g_hdbExtra handles to use a new DSN.
' (Possibly while in use by existing objs.)
'*************************************
Private m_sDSN As String
'Private m_re As RegExp

Function GetSingleString(sFieldName As String, sTableName As String, sCriteria As String) As String
    Dim rs As Integer
    
    On Error GoTo hErr
    GetSingleString = ""
    
    If sFieldName = "" Or sTableName = "" Then Exit Function
    
    rs = g_objRocket.DB_CreateRecordset(Me.hDb, SafeSQL("SELECT " & sFieldName & " FROM " & sTableName & IIf(sCriteria <> "", " WHERE " & sCriteria, "")), DB_FORWARD_ONLY, 0)
    If Not g_objRocket.DB_EOF(rs) Then
        GetSingleString = GetDataString(rs, 1)
    End If
hExit:
    SafeCloseRecordset rs
    Exit Function
hErr:
    LogError "Declarations.GetSingleString", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function
'**********************************************************************************************
' Note..:  301 failed to generate when run from a claim or event, traced to CreateRecordset
' ......:  returning multiple rows when
' ......:  sFieldName = 'DEPT_ASSIGNED_EID' and there are multiple people in PERSON_INVOLVED
' ......:  and one of the people is not an employee (DEPT_ASSIGNED_EID = 0)
' ......:  jtodd22 12/07/2004
'**********************************************************************************************
Function GetSingleLong(sFieldName As String, sTableName As String, sCriteria As String) As Long
    Dim rs As Integer
    Dim sSQL As String
    
    On Error GoTo hErr
    GetSingleLong = 0
    
    If sFieldName = "" Or sTableName = "" Then Exit Function
    
    sSQL = SafeSQL("SELECT " & sFieldName & " FROM " & sTableName & IIf(sCriteria <> "", " WHERE " & sCriteria, ""))
    
    If sFieldName = "DEPT_ASSIGNED_EID" Then
        sSQL = sSQL & " AND DEPT_ASSIGNED_EID > 0"
        End If
        rs = g_objRocket.DB_CreateRecordset(Me.hDb, sSQL, DB_FORWARD_ONLY, 0)
        If Not g_objRocket.DB_EOF(rs) Then
            GetSingleLong = GetDataLong(rs, 1)
        End If
hExit:
    SafeCloseRecordset rs
    Exit Function
hErr:
    LogError "Declarations.GetSingleLong", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function

'**************************************
' Simple Single String Fetch Functions
'**************************************
Function sGetStateCode(lStateID As Long) As String
    sGetStateCode = GetSingleString("STATE_ID", "STATES", "STATE_ROW_ID=" & lStateID)
End Function
Function sGetStateName(lStateID As Long) As String
    sGetStateName = GetSingleString("STATE_NAME", "STATES", "STATE_ROW_ID=" & lStateID)
End Function
Function sGetShortCode(lCode As Long) As String
    sGetShortCode = GetSingleString("SHORT_CODE", "CODES", "CODE_ID=" & lCode)
End Function
Function sGetCodeDesc(lCode As Long) As String
    sGetCodeDesc = GetSingleString("CODE_DESC", "CODES_TEXT", "CODE_ID=" & lCode)
End Function
Function sGetTableName(lCode As Long) As String
    sGetTableName = GetSingleString("SYSTEM_TABLE_NAME", "GLOSSARY", "TABLE_ID=" & lCode)
End Function
Function lGetTableID(sTableName As String) As Long
    lGetTableID = GetSingleLong("TABLE_ID", "GLOSSARY", "SYSTEM_TABLE_NAME = " & sSQLTextArg(sTableName))
End Function
Function lGetCodeIDWithShort(Scode As String, lTableId As Long) As Long
    lGetCodeIDWithShort = GetSingleLong("CODE_ID", "CODES", "TABLE_ID=" & lTableId & " AND SHORT_CODE=" & sSQLTextArg(Scode))
End Function
Function sGetSuppTableKeyColName(sSuppTableName As String) As String
    sGetSuppTableKeyColName = GetSingleString("SYS_FIELD_NAME", "SUPP_DICTIONARY", "SUPP_TABLE_NAME='" & sSuppTableName & "' AND FIELD_TYPE=7")
End Function
Function sGetOrgAbbreviation(lEntityId As Long) As String
    sGetOrgAbbreviation = GetSingleString("ABBREVIATION", "ENTITY", "ENTITY_ID=" & lEntityId)
End Function
Function sGetOrgName(lEntityId As Long) As String
    sGetOrgName = GetSingleString("LAST_NAME", "ENTITY", "ENTITY_ID=" & lEntityId)
End Function
Public Function GetNextUniqueTableID(sTableName As String) As Long
On Error GoTo hError

    Dim rs As Integer
    Dim sSQL As String
    Dim lTableId As Long
    Dim lNextUID As Long
    Dim lOrigUID As Long
    Dim lRows As Long
    Dim lCollisionRetryCount As Long
    Dim lErrRetryCount As Long
    Dim sSaveError As String
    
    Do
        rs = g_objRocket.DB_CreateRecordset(Me.hDb, SafeSQL("SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"), DB_FORWARD_ONLY, 0)
        If g_objRocket.DB_EOF(rs) Then
            g_objRocket.DB_CloseRecordset rs, DB_DROP
            Err.Raise 32001, "GetNextUniqueTableID(" & sTableName & ")", "Specified table does not exist in glossary."
        End If
  
        lTableId = GetDataLong(rs, 1)
        lNextUID = GetDataLong(rs, 2)

        g_objRocket.DB_CloseRecordset rs, DB_DROP
     
        ' Compute next id
        lOrigUID = lNextUID
        If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2
  
        ' try to reserve id (searched update)
        sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & lNextUID & " WHERE TABLE_ID = " & lTableId
    
        ' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
        If lOrigUID Then sSQL = sSQL & " AND NEXT_UNIQUE_ID = " & lOrigUID
     
        ' Try update
        On Error Resume Next
        lRows = g_objRocket.DB_SQLExecute(Me.hDb, sSQL)
  
        If Err.Number <> 0 Then
            sSaveError = Err.Description

            lErrRetryCount = lErrRetryCount + 1

            If lErrRetryCount >= 5 Then
                On Error GoTo hError
                Err.Raise 29999, "GetNextUniqueTableID(" & sTableName & ")", sSaveError
            End If
        Else
            ' if success, return
            If lRows = 1 Then
                GetNextUniqueTableID = lNextUID - 1
                Exit Function   ' success
            Else
                 lCollisionRetryCount = lCollisionRetryCount + 1   ' collided with another user - try again (up to 1000 times)
            End If
        End If
  
        On Error GoTo hError
  
    Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)
     
    If lCollisionRetryCount >= 1000 Then
        On Error GoTo hError
        Err.Raise 32001, "GetNextUniqueTableID(" & sTableName & ")", "Collision timeout. Server load too high. Please wait and try again."
    End If

    GetNextUniqueTableID = 0
hExit:
    SafeCloseRecordset rs
    Exit Function
    
hError:
    LogError "GetNextUniqueTableId", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function

Function sFindEntity(lEntityId As Long) As String
On Error GoTo ET_sFindEntity

    Dim sEntName As String
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String

    db = g_objBroker.GetConn(m_sDSN)
    sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " & lEntityId
    rs = g_objRocket.DB_CreateRecordset(db, SafeSQL(sSQL), DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       sEntName = GetDataString(rs, "LAST_NAME")
       If GetDataString(rs, "FIRST_NAME") & "" <> "" Then sEntName = sEntName & ", " & GetDataString(rs, "FIRST_NAME")
    Else
       sEntName = ""
    End If
    SafeCloseRecordset rs

    sFindEntity = sEntName
hExit:
    SafeCloseRecordset rs
    Exit Function

ET_sFindEntity:
    LogError "CInstanceUtilities.sFindEntity", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function
'**************************************************
' replace proprietary SQL (depending on DB)
Public Function SafeSQL(sSQL As String) As String
On Error GoTo hError
   Dim iPos As Integer
   Dim iPos2 As Integer
   Dim sTemp As String
   Dim sTempName As String
   Dim rs As Integer
   Dim sCols As String
   Dim i As Long
   Dim sSQL2 As String
   Dim sTable As String
   Dim sUID As String
   Dim dbMake As Long
   
    dbMake = g_objRocket.DB_GetDBMake(Me.hDb)
    sUID = Mid(m_sDSN, InStr(m_sDSN, "UID=") + 4)
    If InStr(sUID, ";") Then sUID = Left(sUID, InStr(sUID, ";") - 1)

   sTemp = sSQL

   ' replace temp table create (depending on DB)
   iPos = InStr(sTemp, "[INTO ")
   If iPos Then
      iPos2 = InStr(iPos + 6, sTemp, "]")
      sTempName = Trim$(Mid$(sTemp, iPos + 6, iPos2 - iPos - 6))

      Select Case dbMake
         Case DBMS_IS_ACCESS
            sTemp = Left$(sTemp, iPos - 1) & "INTO " & sTempName & "_" & sUID & Mid$(sTemp, iPos2 + 1)
         Case DBMS_IS_SQLSRVR, DBMS_IS_SYBASE
            sTemp = Left$(sTemp, iPos - 1) & " INTO ##" & sTempName & " " & Mid$(sTemp, iPos2 + 1)
         Case DBMS_IS_INFORMIX
            sTemp = Left$(sTemp, iPos - 1) & " " & Mid$(sTemp, iPos2 + 1) & " INTO TEMP " & sTempName ' JP 7/11/2001   NOT RELIABLE ON ALL VERSIONS OF INFORMIX   & " WITH NO LOG"
         Case DBMS_IS_ORACLE     ' Oracle Support   JP 4/15/98
            ' sTemp = Left$(sTemp, iPos - 1) & " INTO " & sTempName & "_" & Me.Manager.UID() & " " & Mid$(sTemp, iPos2 + 1)
            On Error Resume Next
            g_objRocket.DB_SQLExecute Me.hDb, "DROP TABLE " & sTempName & "_" & sUID
            On Error GoTo hError

            sTemp = "CREATE TABLE " & sTempName & "_" & sUID & " NOLOGGING AS " & Left$(sTemp, iPos - 1) & " " & Mid$(sTemp, iPos2 + 1)
         Case DBMS_IS_DB2
            sSQL2 = Left$(sTemp, iPos - 1) & " " & Mid$(sTemp, iPos2 + 1)
            sSQL2 = Replace(sSQL2, "|", " ")
            rs = g_objRocket.DB_CreateRecordset(Me.hDb, SafeSQL(sSQL2 & " AND 0=1"), DB_FORWARD_ONLY, 0)
            g_objRocket.DB_CloseRecordset rs, DB_CLOSE
            sCols = ""
            For i = 1 To g_objRocket.DB_ColumnCount(rs)
                If Len(sCols) Then sCols = sCols & ","
                sCols = sCols & g_objRocket.DB_ColumnName(rs, i)
                Select Case g_objRocket.DB_ColumnType(rs, i)
                    Case 1                ' SQL_C_CHAR
                        If DB_SQLColumnType(rs, i) = 1 Or DB_SQLColumnType(rs, i) = 12 Then
                            sCols = sCols & " VARCHAR(" & g_objRocket.DB_ColumnSize(rs, i) & ")"
                        Else
                            sCols = sCols & " CLOB(10M)"
                        End If
                    Case -7, 5, 4, -16    ' SQL_C_BIT, SQL_C_SHORT, SQL_C_LONG, SQL_C_SLONG
                        sCols = sCols & " INTEGER"
                    Case 7, 8             ' SQL_C_FLOAT, SQL_C_DOUBLE
                        sCols = sCols & " FLOAT"
                End Select
            Next i
            g_objRocket.DB_CloseRecordset rs, DB_DROP

            sTable = Left$(sTempName & "_" & sUID, 18)

            sSQL2 = "CREATE TABLE " & sTable & "(" & sCols & ")"
            g_objRocket.DB_SQLExecute Me.hDb, sSQL2

            sTemp = "INSERT INTO " & sTable & " " & Left$(sTemp, iPos - 1) & " " & Mid$(sTemp, iPos2 + 1)
      End Select
   End If

   ' Replace temp table names (depending on db)
   iPos = InStr(sTemp, "[")
   While iPos <> 0
      iPos2 = InStr(sTemp, "]")
      sTempName = Trim$(Mid$(sTemp, iPos + 1, iPos2 - iPos - 1))
      Select Case dbMake
         Case DBMS_IS_ACCESS
            sTempName = sTempName & "_" & sUID
         Case DBMS_IS_SQLSRVR, DBMS_IS_SYBASE
            sTempName = "##" & sTempName
         Case DBMS_IS_INFORMIX
            ' nothing
         Case DBMS_IS_ORACLE    ' Oracle Support JP 4/15/98
            sTempName = sTempName & "_" & sUID
         Case DBMS_IS_DB2       ' JP 2/8/2000
            sTempName = Left$(sTempName & "_" & sUID, 18)

      End Select
      sTemp = Left$(sTemp, iPos - 1) & sTempName & Mid$(sTemp, iPos2 + 1)

      iPos = InStr(sTemp, "[")
   Wend

   ' Replace | with AS or blank (depending on db)
   If dbMake = DBMS_IS_ACCESS Then
      sTemp = Replace(sTemp, "|", "AS")
   Else
      sTemp = Replace(sTemp, "| ", "")
   End If

   SafeSQL = sTemp

hExit:
   SafeCloseRecordset rs
   g_objErr.Bubble
   Exit Function

hError:
    LogError "SafeSQL", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
End Function



Friend Property Get DSN() As String

    DSN = m_sDSN

End Property

Friend Property Let DSN(ByVal sDSN As String)

    m_sDSN = sDSN

End Property

Public Property Get hDb() As Integer

    hDb = g_objBroker.GetConn(m_sDSN)

End Property

Private Sub Class_Initialize()
                                        'Set m_re = New RegExp
End Sub
