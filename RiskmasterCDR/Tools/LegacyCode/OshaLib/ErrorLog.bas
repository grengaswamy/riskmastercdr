Attribute VB_Name = "ErrorLog"
Option Explicit

Public Sub LogError(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String, Optional bBubbleErr As Boolean = False)
On Error GoTo hError
Dim iFile As Integer
Dim sFileName As String

iFile = FreeFile
sFileName = App.Path
If Right$(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
sFileName = sFileName & "error.log"

Open sFileName For Append Shared As #iFile
Print #iFile, """" & Format$(Now, "yyyymmddhhnnss") & """," & """" & sProcedure & """," & """" & ErrLine & """," & """" & ErrNumber & """," & """" & ErrSource & """," & """" & ErrDescription & ""","
Close #iFile
iFile = 0

#If BB_DEBUG Then
    ' Note: in this project (Possibly because of the form or the third party control)
    ' the setting for "non-interactive" is greyed out.
    ' Additionally any MessageBoxes will NOT FIRE and ARE NOT ROUTED TO THE EVENT LOG
    'LIKE THEY NORMALLY WOULD BE.  They cause the process to lock up requiring a
    ' bounce of the web-server.
    'MsgBox "Error " & Err.Number & " (" & ErrDescription & ") in procedure " & sProcedure & " of " & ErrSource & "."
#End If
    'Object Based Error Handling
    g_objErr.Clear
    g_objErr.BubbleFlag = bBubbleErr
    g_objErr.Source = ErrSource
    g_objErr.Number = ErrNumber
    g_objErr.Description = ErrDescription
    g_objErr.Line = ErrLine
    
hExit:
   On Error Resume Next
   If iFile <> 0 Then Close iFile
   On Error GoTo 0
   'If bBubbleErr Then Err.Raise ErrNumber, ErrSource & ":" & sProcedure, ErrDescription
   Exit Sub

hError:
   GoTo hExit
End Sub



