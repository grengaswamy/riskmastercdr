VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVSPrintWrapper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**************************************************
'* Wrap calls to VideoSoft vsPrinter control      *
'* vsPrn must be set to the desired vsPrinterControl         *
'*                                                                           *
'* Author: Denis Basaric, 11/04/1997                            *
'* Adapted to .cls module by Brian Battah, 02/01/2002
'**************************************************


Private m_ctlvsPrn As VSPrinter

' Text / Paragraph alignment constants
 Const VSALIGN_LEFT_TOP = 0        ' - Left Top
 Const VSALIGN_CENTER_TOP = 1      ' - Center Top
 Const VSALIGN_RIGHT_TOP = 2       ' - Right Top
 Const VSALIGN_LEFT_BOTTOM = 3     ' - Left Bottom
 Const VSALIGN_CENTER_BOTTOM = 4   ' - Center Bottom
 Const VSALIGN_RIGHT_BOTTOM = 5    ' - Right Bottom
 Const VSALIGN_LEFT_BASELINE = 6   ' - Left Baseline
 Const VSALIGN_CENTER_BASELINE = 7 ' - Center Baseline
 Const VSALIGN_RIGHT_BASELINE = 8  ' - Right Baseline
 Const VSALIGN_JUST_TOP = 9        ' - Justified Top
 Const VSALIGN_JUST_BOTTOM = 10    ' - Justified Bottom
 Const VSALIGN_JUST_BASELINE = 11  ' - Justified Baseline

Private Sub Class_Terminate()

   Set m_ctlvsPrn = Nothing

End Sub


'Declare Function bGetCodeDesc Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal codeid As Long, ByVal DESC As String, length As Long) As Long
'Declare Function bCodesInit Lib "DTGCSR32.DLL" (ByVal hdbc As Long) As Long

'Public g_sConnect As String
'Public henv As Long
'Public rsConnections As New Collection

Private Function OffsetX(i As Variant) As Variant
    On Error GoTo OffsetX_Error
' Used only Internaly, it mimic Object Property
Static m_OffsetX

If i = -1 Then
   If "" & m_OffsetX = "" Or Not IsNumeric(m_OffsetX) Then m_OffsetX = 0
   OffsetX = m_OffsetX
Else
   m_OffsetX = i
   OffsetX = i
End If
OffsetX_Exit:
    Exit Function
OffsetX_Error:
    LogError "OffsetX", Erl, Err.Number, Err.Source, Err.Description
    Resume OffsetX_Exit
End Function

Private Function OffsetY(i As Variant) As Variant
    On Error GoTo OffsetY_Error
' Used only Internaly, it mimic Object Property
Static m_OffsetY

If i = -1 Then
   If "" & m_OffsetY = "" Or Not IsNumeric(m_OffsetY) Then m_OffsetY = 0
   OffsetY = m_OffsetY
Else
   m_OffsetY = i
   OffsetY = i
End If
OffsetY_Exit:
    Exit Function
    
OffsetY_Error:
    LogError "OffsetY", Erl, Err.Number, Err.Source, Err.Description
    Resume OffsetY_Exit
End Function

Private Function ScaleMulti(i As Integer) As Integer
    On Error GoTo ScaleMulti_Error
' Scale multiplicator from Twips ScaleMulti(0) Twips are 1 inch etc.
' if I=0 function return value of static variable
' otherwise it sets static variable to i
Static m_ScaleMulti As Integer

If i = 0 Then
   If m_ScaleMulti = 0 Then m_ScaleMulti = 1
   ScaleMulti = m_ScaleMulti
Else
   m_ScaleMulti = i
   ScaleMulti = i
End If

ScaleMulti_Exit:
    Exit Function

ScaleMulti_Error:
    LogError "ScaleMulti", Erl, Err.Number, Err.Source, Err.Description
    Resume ScaleMulti_Exit
End Function

Sub vsDrawBox(ByVal X1, ByVal Y1, ByVal X2, ByVal Y2)
    On Error GoTo vsDrawBox_Error
' Draw Box same as Line (x1,y1)-(x2,y2),,B
X1 = CLng(X1 * ScaleMulti(0) + OffsetX(-1))
Y1 = CLng(Y1 * ScaleMulti(0) + OffsetY(-1))
X2 = CLng(X2 * ScaleMulti(0) + OffsetX(-1))
Y2 = CLng(Y2 * ScaleMulti(0) + OffsetY(-1))

vsPrn.X1 = X1
vsPrn.Y1 = Y1
vsPrn.X2 = X2
vsPrn.Y2 = Y2

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2

vsPrn.Draw = 2
vsDrawBox_Exit:
    Exit Sub
vsDrawBox_Error:
    LogError "vsDrawBox", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawBox_Exit
End Sub

Sub vsDrawBoxRelative(ByVal x, ByVal Y)
    On Error GoTo vsDrawBoxRelative_Error
' Draw box same as Line -Step(x,y)
x = CLng(x * ScaleMulti(0) + OffsetX(-1))
Y = CLng(Y * ScaleMulti(0) + OffsetY(-1))

vsPrn.X1 = vsPrn.CurrentX
vsPrn.Y1 = vsPrn.CurrentY
vsPrn.X2 = x + vsPrn.CurrentX
vsPrn.Y2 = Y + vsPrn.CurrentY

vsPrn.Draw = 2

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2
vsDrawBoxRelative_Exit:
    Exit Sub
vsDrawBoxRelative_Error:
    LogError "vsDrawBoxRelative", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawBoxRelative_Exit
End Sub

Sub vsDrawBoxStep(ByVal X1, ByVal Y1, ByVal X2, ByVal Y2)
    On Error GoTo vsDrawBoxStep_Error
' Draw Box same as Line (x1,y1)-Step(x2,y2),,B

X1 = CLng(X1 * ScaleMulti(0) + OffsetX(-1))
Y1 = CLng(Y1 * ScaleMulti(0) + OffsetY(-1))

X2 = CLng(X2 * ScaleMulti(0)) + X1
Y2 = CLng(Y2 * ScaleMulti(0)) + Y1

vsPrn.X1 = X1
vsPrn.Y1 = Y1
vsPrn.X2 = X2
vsPrn.Y2 = Y2

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2

vsPrn.Draw = 2

vsDrawBoxStep_Exit:
    Exit Sub
vsDrawBoxStep_Error:
    LogError "vsDrawBoxStep", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawBoxStep_Exit
End Sub

Sub vsDrawLine(ByVal X1, ByVal Y1, ByVal X2, ByVal Y2)
    On Error GoTo vsDrawLine_Error
    ' Draw Line same as Line (x1,y1)-(x2,y2)

X1 = CLng(X1 * ScaleMulti(0) + OffsetX(-1))
Y1 = CLng(Y1 * ScaleMulti(0) + OffsetY(-1))
X2 = CLng(X2 * ScaleMulti(0) + OffsetX(-1))
Y2 = CLng(Y2 * ScaleMulti(0) + OffsetY(-1))

vsPrn.X1 = X1
vsPrn.Y1 = Y1
vsPrn.X2 = X2
vsPrn.Y2 = Y2

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2

vsPrn.Draw = 1

vsDrawLine_Exit:
    Exit Sub
vsDrawLine_Error:
    LogError "vsDrawLine", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawLine_Exit
End Sub

Sub vsDrawLineRelative(ByVal x, ByVal Y)
    On Error GoTo vsDrawLineRelative_Error
' Draw Line same as Line -Step(x,y)

x = CLng(x * ScaleMulti(0) + OffsetX(-1))
Y = CLng(Y * ScaleMulti(0) + OffsetY(-1))

vsPrn.X1 = vsPrn.CurrentX
vsPrn.Y1 = vsPrn.CurrentY
vsPrn.X2 = x + vsPrn.CurrentX
vsPrn.Y2 = Y + vsPrn.CurrentY

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2

vsPrn.Draw = 1

vsDrawLineRelative_Exit:
    Exit Sub
vsDrawLineRelative_Error:
    LogError "vsDrawLineRelative", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawLineRelative_Exit
End Sub

Sub vsDrawLineStep(ByVal X1, ByVal Y1, ByVal X2, ByVal Y2)
    On Error GoTo vsDrawLineStep_Error
' Draw line same as Line (x1,y1)-Step(x2,y2)

X1 = CLng(X1 * ScaleMulti(0) + OffsetX(-1))
Y1 = CLng(Y1 * ScaleMulti(0) + OffsetY(-1))
X2 = X1 + CLng(X2 * ScaleMulti(0))
Y2 = Y1 + CLng(Y2 * ScaleMulti(0))

vsPrn.X1 = X1
vsPrn.Y1 = Y1
vsPrn.X2 = X2
vsPrn.Y2 = Y2

vsPrn.CurrentX = vsPrn.X2
vsPrn.CurrentY = vsPrn.Y2

vsPrn.Draw = 1

vsDrawLineStep_Exit:
    Exit Sub
vsDrawLineStep_Error:
    LogError "vsDrawLineStep", Erl, Err.Number, Err.Source, Err.Description
    Resume vsDrawLineStep_Exit
End Sub

Sub vsEndDoc()
    On Error GoTo vsEndDoc_Error
' End document
vsPrn.Action = paEndDoc

Dim r
r = OffsetX(0)
r = OffsetY(0)
r = ScaleMulti(1)

vsEndDoc_Exit:
    Exit Sub
vsEndDoc_Error:
    LogError "vsEndDoc", Erl, Err.Number, Err.Source, Err.Description
    Resume vsEndDoc_Exit
End Sub

Function vsGetCurrentLine() As Integer
vsGetCurrentLine = vsPrn.CurrentLine
End Function

Function vsGetCurrentPage() As Integer
vsGetCurrentPage = vsPrn.CurrentPage
End Function

Function vsGetCurrentX() As Variant

vsGetCurrentX = vsPrn.CurrentX / ScaleMulti(0) - OffsetX(-1)

End Function

Function vsGetCurrentY() As Variant
vsGetCurrentY = vsPrn.CurrentY / ScaleMulti(0) - OffsetY(-1)
End Function

Function vsGetFontName() As String
vsGetFontName = vsPrn.FontName
End Function

Function vsGetFontSize() As Integer

vsGetFontSize = vsPrn.FontSize

End Function

Function vsGetLineSpacing() As Integer
' Line spacing is expressed as a percentage of the current font height.
vsGetLineSpacing = vsPrn.LineSpacing
End Function

Function vsGetParagraphHeight(s As String) As Variant
vsPrn.CalcParagraph = s
vsGetParagraphHeight = vsPrn.TextHei / ScaleMulti(0)
End Function

Function vsGetPrintHeight() As Long
' Return paper height
vsPrn.PhysicalPage = True
vsGetPrintHeight = vsPrn.PageHeight
vsPrn.PhysicalPage = False
End Function

Function vsGetPrintWidth() As Long
' Return paper width
vsPrn.PhysicalPage = True
vsGetPrintWidth = vsPrn.PageWidth
vsPrn.PhysicalPage = False
End Function

Function vsGetScaleHeight() As Long
' Return just Paper client area
vsGetScaleHeight = vsPrn.PageHeight

End Function

Function vsGetScaleWidth() As Long
' Return just Paper client area
vsGetScaleWidth = vsPrn.PageWidth
End Function

Function vsGetTextAlign() As Integer
vsGetTextAlign = vsPrn.TextAlign
End Function

Function vsGetTextHeight(s As String) As Variant
vsPrn.Measure = s
vsGetTextHeight = vsPrn.TextHei / ScaleMulti(0)
End Function

Function vsGetTextWidth(s As String) As Variant
vsPrn.Measure = s
vsGetTextWidth = vsPrn.TextWid / ScaleMulti(0)
End Function

Public Sub vsKillDoc()
vsPrn.KillDoc
Unload frmPrint
End Sub

Sub vsNewPage()
vsPrn.Action = 4
End Sub

Sub vsPrint(s As Variant)
vsPrn.Text = CStr("" & s) & Chr$(13)
End Sub

' evs , sets up the font, field spacing and text for a print
Sub vsFormatPrint(s As Variant, iFieldLen As Integer, iFontSize As Integer)
    On Error GoTo VSFormatPrint_Error
    Dim iOldFontSize As Integer, iStrLen As Integer
    ' the string s cannot be longer than the field len, and if shorter then
    ' it must be padded with spaces
    iStrLen = Len(s)
    
    If (iStrLen < iFieldLen) Then ' string is smaller so pad with spaces
        While (iStrLen < iFieldLen)
            s = s & " "
            iStrLen = iStrLen + 1
        Wend
    Else                                    ' truncate string
        s = Left(s, iFieldLen)
    End If
    
    iOldFontSize = vsGetFontSize
    vsSetFontSize iFontSize
    vsPrint (s)
    vsSetFontSize iOldFontSize

VSFormatPrint_Exit:
    Exit Sub
VSFormatPrint_Error:
    LogError "VSFormatPrint", Erl, Err.Number, Err.Source, Err.Description
    Resume VSFormatPrint_Exit
End Sub

Sub vsPrintNoCr(s As Variant)
vsPrn.Text = CStr("" & s)
End Sub

Sub vsPrintParagraph(s As String)
   vsPrn.Paragraph = s
End Sub

Sub vsSetBrushColor(l As Long)
vsPrn.BrushColor = l
End Sub

Sub vsSetBrushStyle(i As Integer)
vsPrn.BrushStyle = i
End Sub

Sub vsSetCurrentX(x As Variant)
vsPrn.CurrentX = CLng(x * ScaleMulti(0)) + OffsetX(-1)
End Sub

Sub vsSetCurrentY(Y As Variant)
vsPrn.CurrentY = CLng(Y * ScaleMulti(0)) + OffsetY(-1)
End Sub

Sub vsSetFontBold(b As Integer)
vsPrn.FontBold = b
End Sub

Sub vsSetFontItalic(b As Integer)
vsPrn.FontItalic = b
End Sub

Sub vsSetFontName(s As String)
vsPrn.FontName = s
End Sub

Sub vsSetFontSize(i As Integer)
vsPrn.FontSize = i
End Sub

Sub vsSetFontUnderLine(b As Integer)
vsPrn.FontUnderline = b
End Sub

Sub vsSetLineSpacing(i As Integer)
' Line spacing is expressed as a percentage of the current font height.
vsPrn.LineSpacing = i
End Sub

Sub vsSetMarginLeft(i As Integer)
vsPrn.MarginLeft = i * ScaleMulti(0)
End Sub

Sub vsSetMarginRight(i As Integer)
vsPrn.MarginRight = i * ScaleMulti(0)
End Sub

Function vsSetPaperOrientation(i As Integer) As Integer
    On Error GoTo vsSetPaperOrientation_Error
'************************************************
'* Function set papaer orientation and return   *
'* TRUE if orientation is set.                  *
'* i = 0  - Portrait                            *
'* i = 1  - Landscape                           *
'* i <> 0 , 1 - Raise error inv. prop. value    *
'************************************************
vsSetPaperOrientation = False
If i = 0 Or i = 1 Then
   vsPrn.Orientation = i ' Landscape
   ' Test it
   If vsPrn.Orientation = i Then
      vsSetPaperOrientation = True
   End If
Else
   Error 380 ' Invalid property value
End If
vsSetPaperOrientation_Exit:
    Exit Function
vsSetPaperOrientation_Error:
    LogError "vsSetPaperOrientation", Erl, Err.Number, Err.Source, Err.Description
    Resume vsSetPaperOrientation_Exit
End Function

Sub vsSetPaperSize(i As Integer)

vsPrn.PaperSize = i

End Sub

Sub vsSetPenStyle(i As Integer)
vsPrn.PenStyle = i
End Sub

Sub vsSetPenWidth(i As Integer)
' Pen Width in Twips
vsPrn.PenWidth = i
End Sub

Sub vsSetScaleLeft(v As Variant)
Dim r As Variant
If IsNumeric(v) Then
   r = OffsetX(-(v * ScaleMulti(0)))
Else
   Error 380 ' Invalid property value
End If
End Sub

Sub vsSetScaleMode(i As Integer)
Dim r As Integer
If i = 1 Then ' Twips
   r = ScaleMulti(1)
ElseIf i = 5 Then ' Inches
   r = ScaleMulti(1440)
Else
   Error 380  ' Invalid Property Value
End If

End Sub

Sub vsSetScaleTop(v As Variant)
Dim r As Variant
If IsNumeric(v) Then
   r = OffsetY(-(v * ScaleMulti(0)))
Else
   Error 380 ' Invalid property value
End If
End Sub

Sub vsSetTextAlign(i As Integer)
vsPrn.TextAlign = i
End Sub

Sub vsStartDoc(boolPreview As Integer)
    On Error GoTo vsStartDoc_Error
   
    With vsPrn
        If boolPreview Then
            ' Set control to preview mode
            .Preview = True
            .AbortWindow = True
            .ColorMode = cmMonochrome ' Force monochrome preview mode to consume resources
        Else
            ' Send output directly to printer
            .Preview = False
            .AbortWindow = False
        End If
    
  
        .PhysicalPage = False ' True
        .MarginBottom = 0
        .MarginTop = 0
        .MarginLeft = 0
        .MarginRight = 0
        .BrushStyle = bsTransparent
        .PageBorder = 0
        .Action = paStartDoc
    End With
    
vsStartDoc_Exit:
    Exit Sub

vsStartDoc_Error:
    LogError "vsStartDoc", Erl, Err.Number, Err.Source, Err.Description
    Resume vsStartDoc_Exit
End Sub




Friend Property Get vsPrn() As VSPrinter

    Set vsPrn = m_ctlvsPrn

End Property

'BE SURE TO SET THIS BEFORE USING ANY MEMBER FUNCTIONS....
Friend Property Set vsPrn(ctlvsPrn As VSPrinter)

    Set m_ctlvsPrn = ctlvsPrn

End Property
