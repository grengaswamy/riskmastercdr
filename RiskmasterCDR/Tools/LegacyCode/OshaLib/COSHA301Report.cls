VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COSHA301Report"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Implements IOSHAReport

'Data Directly Supporting IOSHAReport
Private m_bDriveByEvent As Boolean          'MITS 21930     jtodd22
Private m_sYearOfReport As String
Private m_sBeginDate As String
Private m_sEndDate As String
Private m_lReportLevel As Long
Private m_sReportLevel As String
Private m_lUseReportLevel As Long
Private m_sUseReportLevel As String
Private m_lReportOn As Long
Private m_sUserFileName As String
Private m_bAllEntitiesFlag As Boolean
Private m_bEventBasedFlag As Boolean
Private m_lPrintOSHADesc As Long
Private m_colEntities As Collection
Private m_objCompany As CEntityItem
Private m_objCriteriaXML As DOMDocument
Private m_dStartTime As Date
Private m_bPrimaryLocationFlag As Boolean
' BB Additions to support enhancements as of 7.01.2002
Private m_bEnforce180DayRule As Boolean
Private m_sAsOfDate As String
Private m_lItemSortOrder As Long
Private m_sEstablishmentNamePrefix As String
Private m_lEstablishmentNamePrefixOrgLevel As Long          'MITS 21727
Private m_lColumnESource As Long
Private m_lColumnFSource As Long
Private m_bRetainPageTotals As Boolean
' MITS 9101 Manish C Jha 08/21/2008
Private m_bLocDescription As Boolean

' BB 7.15.2002 Additions to support filtering to a specific event\claimant
Private m_lFilterEventId As Long
Private m_lFilterClaimId As Long
Private m_bByOSHAEstablishmentFlag As Boolean   'jlt 09/18/2003

Private m_objInstance As CInstanceUtilities
Private m_objNotify As IWorkerNotifications
Private m_lPITypeEmployee As Long
Private m_colItems As Collection
Private m_colErrors As CErrors

Private m_iTotalEntityPagesPrinted As Integer
Private m_iTotalEntityPagesExpected As Integer
Private m_frm As Form
Private m_bPrintSoftErrLog As Boolean
Private m_objPreparer As CPreparer
'General Object Properties.
Private m_sDSN As String
Private m_sRawDSN As String

Private Sub Class_Initialize()
    On Error GoTo hErr
    Set m_objInstance = New CInstanceUtilities
    Set m_colErrors = New CErrors
    Set m_objCompany = New CEntityItem
    Set m_objPreparer = New CPreparer
    
hExit:
    Exit Sub
hErr:
    LogError "COSHA301Report.Class_Initialize", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit

End Sub
Private Sub Class_Terminate()
    If Not m_frm Is Nothing Then Unload m_frm
   Set m_objPreparer = Nothing
   Set m_frm = Nothing
   Set m_objNotify = Nothing
   Set m_objCompany = Nothing
   Set m_colErrors = Nothing
   Set m_colItems = Nothing
   Set m_objCriteriaXML = Nothing
   Set m_objInstance = Nothing
End Sub

'This would be a poor assumption in an Oracle or Access DB case...
Public Property Let DSN(ByVal sDSN As String)
    m_sRawDSN = sDSN
    If InStr(sDSN, "UID=") Then
        m_sDSN = sDSN
    Else
        m_sDSN = "DSN=" & sDSN & ";UID=sa;PWD="
    End If
    'pass this along ....
    m_objInstance.DSN = m_sDSN
End Property

Public Sub Init()
   On Error GoTo hErr

#If BB_DEBUG Then
    If m_sDSN = "" Then Me.DSN = "RemoteQuest"
'    If m_sPwd = "" Then Me.Pwd = "dtg"
'    If m_sUID = "" Then Me.UID = "dtg"
#End If

#If BB_ROCKET Then
    DebugMsg "Starting New Osha Report - The following Recordsets are already open:" & vbCrLf & g_objRocket.DB_EnumRecordsets()
#End If
    
    If m_sDSN = "" Then
        Err.Raise 3001, "COSHA301.Init()", "Insufficient login information provided." & vbCrLf & "Set the  DSN property before calling this function."
    End If
    
    'Get one connection within the broker to test DB Connectivity.
    '(Will be re-used by broker for later requests.)
    
    If m_objInstance.hDb = 0 Then
        Err.Raise 30101, "OSHA301", "DB Connection Failed"
    End If
    
    m_lPITypeEmployee = m_objInstance.GetSingleLong("CODE_ID", "CODES", "SHORT_CODE='E' AND TABLE_ID=" & m_objInstance.lGetTableID("PERSON_INV_TYPE"))
    
    If m_lPITypeEmployee = 0 Then
        'error, PI type employee was not found
        Err.Raise 3015, "Class_Initialize()", "The Person Involved Type of Employee was not found." & vbCrLf & "This application will not continue."
   End If
   
hExit:
   Exit Sub

hErr:
    LogError "COSHA301.Init", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
End Sub
'Give back an IOSHAReport interface to this object.
' Used by "manager" and FetchOshaData procedure (across all report types.)
'Note: WILL NOT WORK FROM SCRIPT AFTER SRP1 FROM Microsoft.
Public Property Get Standard() As IOSHAReport
    Set Standard = Me
End Property

'Note will not check for "missing" criteria.
'Only "contradictory" criteria will fail this check.
'Thus defaults will be used if any criteria is missing and only if those defaults
' cause a conflict will an error be reported.
Private Function ValidateCriteria() As Boolean
    On Error GoTo hErr
        'Check that dates are within one year if used.
    On Error GoTo hErr
    
    'If requesting specific filtered reports for osha301 accept default criteria.
    If (Me.FilterClaimId Or Me.FilterEventId) Then
        Me.Standard.ReportLevel = 1012
        ValidateCriteria = True  'OSHA301
        Exit Function
    End If

    If Standard.YearOfReport = "" Then
        If Val(Trim(Standard.BeginDate)) = 0 Then Err.Raise 3023, "COSHA301Report", "Invalid BeginDate Specified"
        If Val(Trim(Standard.EndDate)) = 0 Then Err.Raise 3024, "COSHA301Report", "Invalid EndDate Specified"
        'Date Range Spans multiple years (not allowed)
        If Val(Left(Standard.BeginDate, 4)) <> Val(Left(Standard.EndDate, 4)) Then Err.Raise 3025, "COSHA301Report", "Date Range Spans multiple years (not allowed)"
        If Val(Standard.EndDate) - Val(Standard.BeginDate) <= 0 Then Err.Raise 3030, "COSHA301Report", "Invalid Date Range specified. (BeginDate MUST come before the EndDate.)"
        'Date Range Outside of OSHA300 Rule Applicability (Before Jan, 1 2002 not allowed)
        If Val(Left(Standard.BeginDate, 4)) < 2002 Then Err.Raise 3026, "COSHA301Report", "Date Range begins before OSHA 300 Rules were enacted - January 1, 2002.  (not allowed)"
    Else
        'Date Range Outside of OSHA300 Rule Applicability (Before Jan, 1 2002 not allowed)
        If Val(Standard.YearOfReport) < 2002 Then Err.Raise 3026, "COSHA301Report", "Date Range begins before OSHA 300 Rules were enacted - January 1, 2002.  (not allowed)"
    End If
    
    'Invalid Report Level
    If Standard.ReportLevel < 1005 Or Standard.ReportLevel > 1012 Then Err.Raise 3027, "COSHA301Report", "Invalid Report Level"
    'Invalid Use Report Level
    If Standard.UseReportLevel < 1005 Or Standard.UseReportLevel > 1012 Then Err.Raise 3028, "COSHA301Report", "Invalid Use Report Level"
    'Invalid Use vs Report Level (Must use at an equal or higher org level)
    'Note org heirarchy level numbers are  inverted (low number is higher level)
    If Standard.UseReportLevel > Standard.ReportLevel Then Err.Raise 3029, "COSHA301Report", "Invalid Use vs Report Level (Must use at an equal or higher org level)"
    
    'No Entities selected with AllEntitiesFlag = false.
    If Not Standard.AllEntitiesFlag And Standard.SelectedEntities.Count = 0 Then
        Err.Raise 3016, "ValidateCriteria()", "No Enitities Found at Given ""Report At"" Level"
    End If
    
    'TODO - other validity checks...
    ValidateCriteria = True
    
hExit:
    Exit Function
hErr:
    LogClassError "COSHA301Report.ValidateCriteria", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function

'Enforce Filter IFF provided.
'Hack to allow printing the 301 for a particular claim or the 301's specific to an event.
'BSB 07.15.2002
Private Sub FilterOshaData()
        Dim objItem As COSHAItem
        For Each objItem In m_colItems
            If Me.FilterClaimId Then
                If 0 = (m_objInstance.GetSingleLong("CLAIM_ID", "CLAIM, PERSON_INVOLVED", "CLAIM.CLAIM_ID =" & Me.FilterClaimId & " AND CLAIM.EVENT_ID=PERSON_INVOLVED.EVENT_ID AND PERSON_INVOLVED.PI_ROW_ID =" & objItem.PiRowId & " AND PERSON_INVOLVED.PI_TYPE_CODE = 237")) Then
                    m_colItems.Remove "Key:" & objItem.PiRowId
                End If
            ElseIf Me.FilterEventId Then
                If 0 = (m_objInstance.GetSingleLong("EVENT_ID", "PERSON_INVOLVED", "PERSON_INVOLVED.PI_ROW_ID =" & objItem.PiRowId & " AND PERSON_INVOLVED.EVENT_ID =" & Me.FilterEventId)) Then
                    m_colItems.Remove "Key:" & objItem.PiRowId
                End If
            End If
        Next

End Sub
'This is not entirely as I would have expected.
' The 301 is considered a different report for each incident.
' However, we're batching them together.
' Must come up with a simple way to print only one...
Private Sub CreateData()

    On Error GoTo hErr
    DebugMsg "Entered COSHA301Report.CreateData"
'
'    'Pick up the system default for whether or not to use "OSHA_DESC" on these reports.
'    'NOTE: This CURRENTLY over-rides the user selected value!!!!!!
'    m_lPrintOSHADesc = Val(m_objInstance.GetSingleString("PRINT_OSHA_DESC", "SYS_PARMS", ""))
    
    If Not ValidateCriteria Then Err.Raise 3040, "COSHA301Report.CreateData", "Failed Criteria Check. PDF not generated."
    DebugMsg "Validated Criteria COSHA301Report"
    'LoadReportOnCompanyData Standard.Company, Me.Standard
    DebugMsg "Loaded company data"
        
    Set m_colItems = RptOSHA.FetchOshaData(Me.Standard, Me.FilterEventId)
        
    FilterOshaData
    
    DebugMsg "Fetched OSHA data"
    
    m_iTotalEntityPagesExpected = Standard.Items.Count
    If m_iTotalEntityPagesExpected = 0 Then Me.Standard.UserFileName = "": Exit Sub
    m_iTotalEntityPagesPrinted = 0
     
    DebugMsg "Calculated Pages Expected"
    
    DebugMsg "Leaving COSHA301Report.CreateData"
    
hExit:
    g_objErr.Bubble
    Exit Sub
hErr:
    LogClassError "COSHA301Report.CreateData", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
    Resume
End Sub

'************************************
'Purpose: Inject LogError calls with code to fill this
' class object's error properties for use from ASP.
'************************************
Public Sub LogClassError(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String, Optional bBubbleErr As Boolean = False)
    Me.Standard.Errors.Add sProcedure, ErrLine, ErrNumber, ErrSource, ErrDescription
    LogError sProcedure, ErrLine, ErrNumber, ErrSource, ErrDescription, IIf(bBubbleErr Or ErrNumber = ERR_ABORT_REQUESTED, True, False)
End Sub

'******************************************************
'******************************************************
' Properties REQUIRED by the IOHSAREPORT interface
'******************************************************
'******************************************************
'
Private Property Get IOSHAReport_AllEntitiesFlag() As Boolean
    IOSHAReport_AllEntitiesFlag = m_bAllEntitiesFlag
End Property
Private Property Let IOSHAReport_AllEntitiesFlag(ByVal bAllEntitiesFlag As Boolean)
    m_bAllEntitiesFlag = bAllEntitiesFlag
End Property
Private Property Get IOSHAReport_ByOSHAEstablishmentFlag() As Boolean
    IOSHAReport_ByOSHAEstablishmentFlag = m_bByOSHAEstablishmentFlag
End Property
Private Property Let IOSHAReport_ByOSHAEstablishmentFlag(ByVal bByOSHAEstablishmentFlag As Boolean)
    If bByOSHAEstablishmentFlag Then
        Standard.EstablishmentNamePrefix = ""
        Standard.EstablishmentNamePrefixOrgLevel = 0
        Standard.ReportLevel = 1012
        Standard.UseReportLevel = 1012
    End If
    m_bByOSHAEstablishmentFlag = bByOSHAEstablishmentFlag
End Property

Private Property Get IOSHAReport_BeginDate() As String
    IOSHAReport_BeginDate = m_sBeginDate
End Property
Private Property Let IOSHAReport_BeginDate(ByVal sBeginDate As String)
    On Error Resume Next
    sBeginDate = Format$(sBeginDate, "yyyymmdd")
    m_sBeginDate = sBeginDate
    m_sYearOfReport = Left(sBeginDate, 4)
End Property

Private Property Get IOSHAReport_Context() As CInstanceUtilities
    Set IOSHAReport_Context = m_objInstance
End Property

Private Property Let IOSHAReport_DriveByEvent(ByVal RHS As Boolean)
    m_bDriveByEvent = RHS
End Property

Private Property Get IOSHAReport_DriveByEvent() As Boolean
    IOSHAReport_DriveByEvent = m_bDriveByEvent
End Property

Private Property Let IOSHAReport_EstablishmentNamePrefix(ByVal RHS As String)
RHS = IIf(m_bByOSHAEstablishmentFlag, "0", RHS)
m_sEstablishmentNamePrefix = RHS
End Property

Private Property Get IOSHAReport_EstablishmentNamePrefix() As String
IOSHAReport_EstablishmentNamePrefix = m_sEstablishmentNamePrefix
End Property

Private Property Let IOSHAReport_EstablishmentNamePrefixOrgLevel(ByVal vData As Long)
    vData = IIf(m_bByOSHAEstablishmentFlag, 0, vData)
    m_lEstablishmentNamePrefixOrgLevel = vData
End Property
Private Property Get IOSHAReport_EstablishmentNamePrefixOrgLevel() As Long
    IOSHAReport_EstablishmentNamePrefixOrgLevel = m_lEstablishmentNamePrefixOrgLevel
End Property

Private Sub IOSHAReport_LogClassError(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String, Optional bBubbleErr As Boolean = False)
    Call LogClassError(sProcedure, ErrLine, ErrNumber, ErrSource, ErrDescription, bBubbleErr)
End Sub

Private Property Get IOSHAReport_YearOfReport() As String
    IOSHAReport_YearOfReport = m_sYearOfReport
End Property

Private Property Let IOSHAReport_YearOfReport(ByVal sYearOfReport As String)
    If sYearOfReport = "" Then Exit Property
    If CLng(sYearOfReport) = 0 Then Exit Property
    m_sBeginDate = Trim$(sYearOfReport) & "0101"
    m_sEndDate = Trim$(sYearOfReport) & "1231"
    m_sYearOfReport = sYearOfReport
End Property
' MITS 9101 Manish C Jha 08/21/2008
Private Property Let IOSHAReport_LocDescription(ByVal RHS As Boolean)
m_bLocDescription = RHS
End Property
Private Property Get IOSHAReport_LocDescription() As Boolean
IOSHAReport_LocDescription = m_bLocDescription
End Property
Private Property Get IOSHAReport_EndDate() As String
    IOSHAReport_EndDate = m_sEndDate
End Property
Private Property Let IOSHAReport_EndDate(ByVal sEndDate As String)
    On Error Resume Next
    sEndDate = Format$(sEndDate, "yyyymmdd")
    On Error GoTo 0
    If Left(m_sBeginDate, 4) = Left(sEndDate, 4) Then
        m_sEndDate = sEndDate
    Else
        LogClassError "COSHA301.EndDate", Erl, 3013, "", "Invalid end date. Must be in the same calander year as the beginning date.", True
    End If
End Property
Private Property Get IOSHAReport_EventBasedFlag() As Boolean
    IOSHAReport_EventBasedFlag = m_bEventBasedFlag
End Property
Property Let IOSHAReport_EventBasedFlag(ByVal bEventBase As Boolean)
    m_bEventBasedFlag = bEventBase
End Property
Private Property Get IOSHAReport_Notify() As IWorkerNotifications
    Set IOSHAReport_Notify = m_objNotify
End Property
Private Property Set IOSHAReport_Notify(obj As IWorkerNotifications)
    Set m_objNotify = obj
End Property
Private Property Get IOSHAReport_PITypeEmployee() As Long
    IOSHAReport_PITypeEmployee = m_lPITypeEmployee
End Property
Private Property Get IOSHAReport_PrimaryLocationFlag() As Boolean
    IOSHAReport_PrimaryLocationFlag = m_bPrimaryLocationFlag
End Property
Private Property Let IOSHAReport_PrimaryLocationFlag(ByVal bPrimaryLocationFlag As Boolean)
    m_bPrimaryLocationFlag = bPrimaryLocationFlag
End Property
Private Property Get IOSHAReport_PrintOSHADescFlag() As Boolean
    IOSHAReport_PrintOSHADescFlag = IIf(m_lPrintOSHADesc = 0, False, True)
End Property
Private Property Let IOSHAReport_PrintOSHADescFlag(ByVal RHS As Boolean)
    m_lPrintOSHADesc = RHS
End Property
Private Property Get IOSHAReport_ReportOn() As Long
    IOSHAReport_ReportOn = m_lReportOn
End Property
'Now used to hold the top level Entity_EID for the report.
Private Property Let IOSHAReport_ReportOn(ByVal RHS As Long)
    If m_lReportOn <> RHS Then
        m_lReportOn = RHS
    End If
End Property
Private Property Get IOSHAReport_SelectedEntities() As Collection
    Set IOSHAReport_SelectedEntities = m_colEntities
End Property
Private Property Set IOSHAReport_SelectedEntities(ByRef colEntities As Collection)
    Set m_colEntities = colEntities
End Property
Private Property Let IOSHAReport_StartTime(ByVal RHS As Date)
    m_dStartTime = RHS
End Property
Private Property Get IOSHAReport_StartTime() As Date
    IOSHAReport_StartTime = m_dStartTime
End Property
Private Property Get IOSHAReport_ReportLevel() As Long
    IOSHAReport_ReportLevel = m_lReportLevel
End Property
Private Property Let IOSHAReport_ReportLevel(ByVal lReportLevel As Long)
    lReportLevel = IIf(m_bByOSHAEstablishmentFlag, 1012, lReportLevel)
    m_lReportLevel = lReportLevel
    m_sReportLevel = sOrgLevelToEIDColumn(lReportLevel)
End Property
Private Property Get IOSHAReport_UseReportLevel() As Long
    IOSHAReport_UseReportLevel = m_lUseReportLevel
End Property
Private Property Let IOSHAReport_UseReportLevel(ByVal lUseReportLevel As Long)
    lUseReportLevel = IIf(m_bByOSHAEstablishmentFlag, 1012, lUseReportLevel)
    m_lUseReportLevel = lUseReportLevel
    m_sUseReportLevel = sOrgLevelToEIDColumn(lUseReportLevel)
End Property
Private Property Get IOSHAReport_sReportLevel() As String
    IOSHAReport_sReportLevel = m_sReportLevel
End Property
Private Property Get IOSHAReport_sUseReportLevel() As String
    IOSHAReport_sUseReportLevel = m_sUseReportLevel
End Property
Private Property Let IOSHAReport_Enforce180DayRule(ByVal RHS As Boolean)
m_bEnforce180DayRule = RHS
End Property
Private Property Get IOSHAReport_Enforce180DayRule() As Boolean
IOSHAReport_Enforce180DayRule = m_bEnforce180DayRule
End Property
Private Property Let IOSHAReport_ItemSortOrder(ByVal RHS As Long)
m_lItemSortOrder = RHS
End Property
Private Property Get IOSHAReport_ItemSortOrder() As Long
IOSHAReport_ItemSortOrder = m_lItemSortOrder
End Property
'mjha3 : 01/28/2009 : MITS 14360
Private Property Let IOSHAReport_ColumnESource(ByVal RHS As Long)
    m_lColumnESource = RHS
End Property
Private Property Get IOSHAReport_ColumnESource() As Long
    IOSHAReport_ColumnESource = m_lColumnESource
End Property
Private Property Let IOSHAReport_ColumnFSource(ByVal RHS As Long)
    m_lColumnFSource = RHS
End Property
Private Property Get IOSHAReport_ColumnFSource() As Long
    IOSHAReport_ColumnFSource = m_lColumnFSource
End Property
Private Property Let IOSHAReport_AsOfDate(ByVal RHS As String)
    On Error Resume Next
    If IsDate(RHS) Then
        RHS = Format$(RHS, "yyyymmdd")
    End If
    m_sAsOfDate = RHS
End Property
Private Property Get IOSHAReport_AsOfDate() As String
    IOSHAReport_AsOfDate = m_sAsOfDate
End Property

Private Property Let IOSHAReport_UserFileName(ByVal RHS As String)
    m_sUserFileName = RHS
End Property
Private Property Get IOSHAReport_UserFileName() As String
    IOSHAReport_UserFileName = m_sUserFileName
End Property
Private Property Get IOSHAReport_Company() As CEntityItem
    Set IOSHAReport_Company = m_objCompany
End Property
Private Property Get IOSHAReport_Items() As Collection
    Set IOSHAReport_Items = m_colItems
End Property
Private Property Get IOSHAReport_Errors() As CErrors
    Set IOSHAReport_Errors = m_colErrors
End Property
Private Property Get IOSHAReport_CriteriaXMLDom() As DOMDocument
    Dim objXML As DOMDocument
    Dim objTmpElt As IXMLDOMElement
    Dim objEntityElt As IXMLDOMElement
    Dim ControlNodes As IXMLDOMNodeList
    Dim vTmp As Variant
    Dim fso As FileSystemObject
    On Error GoTo hErr
    Set fso = New FileSystemObject
    
    Set objXML = m_objCriteriaXML 'New DOMDocument
    Set ControlNodes = objXML.getElementsByTagName("control")
    
    'Clean off all "codeid" and  "checked" elts this should be a blank template.
    For Each objTmpElt In ControlNodes
        If Not objTmpElt.Attributes.getNamedItem("checked") Is Nothing Then objTmpElt.Attributes.removeNamedItem "checked"
        If Not objTmpElt.Attributes.getNamedItem("codeid") Is Nothing Then objTmpElt.Attributes.removeNamedItem "codeid"
    Next

    For Each objTmpElt In ControlNodes
        'DebugMsg objTmpElt.getAttribute("name")
        Select Case objTmpElt.getAttribute("name")
            Case "drivebyevent"                                              'MITS 21930
                objTmpElt.setAttribute "value", Me.Standard.DriveByEvent
            Case "reportlevel"
                objTmpElt.setAttribute "codeid", Me.Standard.ReportLevel 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.ReportLevel & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""
            Case "usereportlevel"
                objTmpElt.setAttribute "codeid", Me.Standard.UseReportLevel 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.UseReportLevel & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""
           Case "begindate"
                 objTmpElt.setAttribute "value", Me.Standard.BeginDate
            Case "enddate"
                 objTmpElt.setAttribute "value", Me.Standard.EndDate
            Case "allentitiesflag"
                 If Me.Standard.AllEntitiesFlag Then
                    Set objEntityElt = objXML.selectSingleNode("//control[@name='allentitiesflag' && @value='true']")
                Else
                    Set objEntityElt = objXML.selectSingleNode("//control[@name='allentitiesflag' && @value='false']")
                End If
                objEntityElt.setAttribute "checked", ""
            Case "selectedentities"
                For Each vTmp In m_colEntities
                    Set objEntityElt = objXML.createElement("option")
                    objEntityElt.setAttribute "value", CStr(vTmp)
                    objEntityElt.Text = m_objInstance.sGetOrgAbbreviation(CStr(vTmp)) & " - " & m_objInstance.sGetOrgName(CStr(vTmp))
                    objTmpElt.appendChild objEntityElt
                Next
            Case "yearofreport"
                objTmpElt.setAttribute "codeid", Me.Standard.YearOfReport
            Case "eventbasedflag"
                If Me.Standard.EventBasedFlag Then objTmpElt.setAttribute "checked", ""
            Case "printsofterrlog"
                If Me.PrintSoftErrLog Then objTmpElt.setAttribute "checked", ""
            Case "printoshadescflag"
                 If m_lPrintOSHADesc Then objTmpElt.setAttribute "checked", ""
            Case "primarylocationflag"
                 If m_bPrimaryLocationFlag Then objTmpElt.setAttribute "checked", ""
            Case "preparername"
                objTmpElt.setAttribute "value", Me.Preparer.Name
            Case "preparertitle"
                objTmpElt.setAttribute "value", Me.Preparer.Title
            Case "preparerphone"
                objTmpElt.setAttribute "value", Me.Preparer.Phone
            '********************************************
            ' Added functionality BB 07.01.2002
            ' 180 Day rule, as of date, sort order and column F source field selections
            '********************************************
            Case "enforce180dayrule"
                If Me.Standard.Enforce180DayRule Then objTmpElt.setAttribute "checked", ""
            Case "asofdate"
                objTmpElt.setAttribute "value", Me.Standard.AsOfDate
            Case "establishmentnameprefix"
                objTmpElt.setAttribute "codeid", Me.Standard.EstablishmentNamePrefix 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.EstablishmentNamePrefix & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""
            Case "sortorder"
                objTmpElt.setAttribute "codeid", Me.Standard.ItemSortOrder 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.ItemSortOrder & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""
            'mjha3 : 01/28/2009 : MITS 14360
            Case "columnesource"
                 objTmpElt.setAttribute "codeid", Me.Standard.ColumnESource 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.ColumnESource & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""

            Case "columnfsource"
                 objTmpElt.setAttribute "codeid", Me.Standard.ColumnFSource 'any numeric combo box is set as a "codeid" in RequestToXML()
                Set objEntityElt = objTmpElt.selectSingleNode("option[@value='" & Me.Standard.ColumnFSource & "']")
                If Not objEntityElt Is Nothing Then objEntityElt.setAttribute "selected", ""
            '********************************************
            ' Added functionality BB 07.17.2002
            ' Filter by Event or Claim Id
            '********************************************
            Case "filtereventid"
                objTmpElt.setAttribute "value", Me.FilterEventId
            Case "filterclaimid"
                objTmpElt.setAttribute "value", Me.FilterClaimId
            'new jlt 09/25/2003
            Case "byoshaestablishmentflag"
                 If Me.Standard.ByOSHAEstablishmentFlag Then
                    Set objEntityElt = objXML.selectSingleNode("//control[@name='byoshaestablishmentflag' && @value='true']")
                Else
                    Set objEntityElt = objXML.selectSingleNode("//control[@name='byoshaestablishmentflag' && @value='false']")
                End If
                objEntityElt.setAttribute "checked", ""

            End Select
    Next
    'Pick up any hidden internal tags (like report type)
    Set ControlNodes = objXML.getElementsByTagName("internal")
    For Each objTmpElt In ControlNodes
        Select Case objTmpElt.getAttribute("name")
            Case "reporttype"
                'ALWAYS set the report type.
                objTmpElt.setAttribute "value", 1
                Set objTmpElt = objXML.selectSingleNode("//report")
                objTmpElt.setAttribute "type", 1
        End Select
    Next
    
    DebugMsg "Done with Get CriteriaXmlDom."
    Set IOSHAReport_CriteriaXMLDom = objXML
hExit:
    Exit Property
hErr:
    LogClassError "COSHA301Report.CriteriaXML", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Property
Private Property Set IOSHAReport_CriteriaXMLDom(objXML As DOMDocument)
    Dim prevXML As DOMDocument
    Dim objCriteriaXML As DOMDocument
    Dim bUseYear As Boolean
    Dim objEntityElt As IXMLDOMElement
    Dim objElt As IXMLDOMElement
    Dim objTmpElt As IXMLDOMElement
    Dim ControlNodes As IXMLDOMNodeList
    
    DebugMsg "arrived in ParseCriteria"
    Set prevXML = m_objCriteriaXML
    Set objCriteriaXML = objXML
    
    Set objElt = objCriteriaXML.selectSingleNode("//control[@name = 'useyear']")
    If Not objElt Is Nothing Then
        bUseYear = IIf(LCase(objElt.Text) = "true", True, False)
    End If

    DebugMsg "set back up "
    
    Set ControlNodes = objCriteriaXML.getElementsByTagName("control")
    
    DebugMsg "set Elt list of criteria count: " & ControlNodes.length

    For Each objTmpElt In ControlNodes
        Select Case objTmpElt.getAttribute("name")
            Case "drivebyevent"                                              'MITS 21930
                Standard.DriveByEvent = objTmpElt.getAttribute("value")
            Case "reportlevel"
                Standard.ReportLevel = objTmpElt.getAttribute("codeid") 'any numeric combo box is set as a "codeid" in RequestToXML()
            Case "usereportlevel"
                Standard.UseReportLevel = objTmpElt.getAttribute("codeid") 'any numeric combo box is set as a "codeid" in RequestToXML()
            Case "begindate"
                If Not bUseYear Then Standard.BeginDate = objTmpElt.getAttribute("value")
            Case "enddate"
                If Not bUseYear Then Standard.EndDate = objTmpElt.getAttribute("value")
            Case "allentitiesflag"
                Set objTmpElt = objCriteriaXML.selectSingleNode("//control[@name='allentitiesflag' && @checked]")
                If Not objTmpElt Is Nothing Then Me.Standard.AllEntitiesFlag = objTmpElt.getAttribute("value")
            Case "selectedentities"
                    Set m_colEntities = New Collection
                For Each objEntityElt In objTmpElt.childNodes
                    Standard.SelectedEntities.Add CStr(objEntityElt.getAttribute("value"))
                Next
            Case "yearofreport"
                If bUseYear Then Me.Standard.YearOfReport = objTmpElt.getAttribute("codeid")
            Case "eventbasedflag"
                Standard.EventBasedFlag = IsChecked(objTmpElt)
            Case "printsofterrlog"
                Me.PrintSoftErrLog = IsChecked(objTmpElt)
            Case "printoshadescflag"
                m_lPrintOSHADesc = IsChecked(objTmpElt)
            Case "primarylocationflag"
                m_bPrimaryLocationFlag = IsChecked(objTmpElt)
            Case "preparername"
                Me.Preparer.Name = objTmpElt.getAttribute("value")
            Case "preparertitle"
                Me.Preparer.Title = objTmpElt.getAttribute("value")
            Case "preparerphone"
                Me.Preparer.Phone = objTmpElt.getAttribute("value")
            '********************************************
            ' Added functionality BB 07.01.2002
            ' 180 Day rule, as of date, sort order and column F source field selections
            '********************************************
            Case "enforce180dayrule"
                Me.Standard.Enforce180DayRule = GetXMLValue("enforce180dayrule", objCriteriaXML)
            Case "asofdate"
                 Me.Standard.AsOfDate = GetXMLValue("asofdate", objCriteriaXML)
            Case "establishmentnameprefix"
                Me.Standard.EstablishmentNamePrefix = GetXMLValue("establishmentnameprefix", objCriteriaXML)
                Me.Standard.EstablishmentNamePrefixOrgLevel = Val(Me.Standard.EstablishmentNamePrefix)
            Case "sortorder"
                Me.Standard.ItemSortOrder = GetXMLValue("sortorder", objCriteriaXML)
            'mjha3 : 01/28/2009 : MITS 14360
            Case "columnesource"
                Me.Standard.ColumnESource = GetXMLValue("columnesource", objCriteriaXML)
            Case "columnfsource"
                Me.Standard.ColumnFSource = GetXMLValue("columnfsource", objCriteriaXML)
            '********************************************
            ' Added functionality BB 07.17.2002
            ' Filter by Event or Claim Id
            '********************************************
            Case "filtereventid"
                 Me.FilterEventId = objTmpElt.getAttribute("value")
            Case "filterclaimid"
                Me.FilterClaimId = objTmpElt.getAttribute("value")
            '**********************************************
            'new jlt 09/25/2003
            Case "byoshaestablishmentflag"
                Set objTmpElt = objCriteriaXML.selectSingleNode("//control[@name='byoshaestablishmentflag' && @checked]")
                If Not objTmpElt Is Nothing Then Me.Standard.ByOSHAEstablishmentFlag = objTmpElt.getAttribute("value")
            End Select
    Next
    
    'RMX does not run though RMNetReportInterfaces.dll
    'MITS 21727 never worked in RMX
    'Does not apply to OSHA 301
'    If Me.Standard.EstablishmentNamePrefixOrgLevel > 1004 Then
'        If Me.Standard.ReportLevel > Me.Standard.UseReportLevel Then
'            Me.Standard.UseReportLevel = Me.Standard.ReportLevel
'        Else
'            Me.Standard.ReportLevel = Me.Standard.UseReportLevel
'        End If
'    End If
    
'    'Pick up any hidden internal tags (like report type)
'    Set ControlNodes = objCriteriaXML.getElementsByTagName("internal")
'    For Each objTmpElt In ControlNodes
'        Select Case objTmpElt.getAttribute("name")
''            Case "reporttype"
''                Me.ReportType = objTmpElt.getAttribute("value")
'        End Select
'    Next
    
    'Validate and store for property get.
    If Not ValidateCriteria Then Err.Raise 3040, "Property Set CriteriaXMLDom()", "Failed Criteria Check. (See previous error.)"
    Set m_objCriteriaXML = objXML
hExit:
    Exit Property
hErr:
    LogClassError "COSHA301Report.CriteriaXMLDom", Erl, Err.Number, Err.Source, Err.Description, True
    GoTo hExit
End Property


'Fetch all necessary records from the DB.
'Fixed the logic so that this object is only a SINGLE REPORT.
'Return File Name of PDF results.
Private Function IOSHAReport_ResultPDF() As String
    On Error GoTo hErr
    CreateData                                          'MITS 22253
    If Standard.UserFileName = "" Then Exit Function
hExit:
    Exit Function
hErr:
    LogClassError "COSHA301Report.IOSHAReport_ResultPDF", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function
Private Function IOSHAReport_ResultEMF() As String
On Error GoTo hErr
        
    'stub for Implements IOSHAReport        'MITS 22253
    
hExit:
    Exit Function
hErr:
    LogClassError "COSHA301Report.IOSHAReport_ResultEMF", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function

Public Property Get PrintSoftErrLog() As Boolean
    PrintSoftErrLog = m_bPrintSoftErrLog
End Property

Public Property Let PrintSoftErrLog(ByVal bPrintSoftErrLog As Boolean)
    m_bPrintSoftErrLog = bPrintSoftErrLog
End Property

Public Property Get Preparer() As CPreparer

    Set Preparer = m_objPreparer

End Property


Public Property Get FilterEventId() As Long

    FilterEventId = m_lFilterEventId

End Property

Public Property Let FilterEventId(ByVal lFilterEventID As Long)

    m_lFilterEventId = lFilterEventID

End Property

Public Property Get FilterClaimId() As Long

    FilterClaimId = m_lFilterClaimId

End Property

Public Property Let FilterClaimId(ByVal lFilterClaimId As Long)

    m_lFilterClaimId = lFilterClaimId

End Property

Public Sub FetchOshaData()
    Set m_colItems = RptOSHA.FetchOshaData(Me.Standard, Me.FilterEventId)
End Sub

Private Property Let IOSHAReport_RetainPageTotals(ByVal RHS As Boolean)
    m_bRetainPageTotals = RHS
End Property

Private Property Get IOSHAReport_RetainPageTotals() As Boolean
    IOSHAReport_RetainPageTotals = m_bRetainPageTotals
End Property
