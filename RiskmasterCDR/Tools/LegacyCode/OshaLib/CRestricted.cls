VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRestricted"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
' Object properties
Private m_PiRestrictRowId As Long
Private m_PiRowId As Long
Private m_DateFirstRestrct As String
Private m_PercentDisabled As Long
Private m_DateLastRestrct As String
Private m_Duration As Long
Private m_PositionCode As Long
Private m_OSHARestrictionDays As Long       'MITS 14372

Public Function ClearObject() As Long
    m_PiRestrictRowId = 0
    m_PiRowId = 0
    m_DateFirstRestrct = ""
    m_PercentDisabled = 0
    m_DateLastRestrct = ""
    m_Duration = 0
    m_PositionCode = 0
    m_OSHARestrictionDays = 0
End Function
Public Property Get PiRestrictRowId() As Long
   PiRestrictRowId = m_PiRestrictRowId
End Property
Public Property Let PiRestrictRowId(ByVal vData As Long)
    m_PiRestrictRowId = vData
End Property

Public Property Get PiRowId() As Long
   PiRowId = m_PiRowId
End Property
Public Property Let PiRowId(ByVal vData As Long)
    m_PiRowId = vData
End Property

Public Property Get DateFirstRestrct() As String
   DateFirstRestrct = m_DateFirstRestrct
End Property
Public Property Let DateFirstRestrct(ByVal vData As String)
    m_DateFirstRestrct = vData
End Property

Public Property Get PercentDisabled() As Long
   PercentDisabled = m_PercentDisabled
End Property
Public Property Let PercentDisabled(ByVal vData As Long)
    m_PercentDisabled = vData
End Property

Public Property Get DateLastRestrct() As String
   DateLastRestrct = m_DateLastRestrct
End Property
Public Property Let DateLastRestrct(ByVal vData As String)
      m_DateLastRestrct = vData
End Property

Public Property Get Duration() As Long
   Duration = m_Duration
End Property
Public Property Let Duration(ByVal vData As Long)
    m_Duration = vData
End Property

Public Property Get PositionCode() As Long
   PositionCode = m_PositionCode
End Property
Public Property Let PositionCode(ByVal vData As Long)
    m_PositionCode = vData
End Property

Public Property Get OSHARestrictionDays() As Long
    OSHARestrictionDays = m_OSHARestrictionDays
End Property
Public Property Let OSHARestrictionDays(ByVal vData As Long)
    m_OSHARestrictionDays = vData
End Property
