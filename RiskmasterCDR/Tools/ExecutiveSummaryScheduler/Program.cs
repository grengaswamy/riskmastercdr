﻿using System.Net.Mail;
using System;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Security;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security.Encryption;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using System.IO;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.ProgressNotes;
using Riskmaster.Application.ExecutiveSummaryReport;
using Riskmaster.Models;// sonali for jira- RMACLOUD 2515
using System.Configuration; // ash , jira- RMACLOUD 2515

namespace Riskmaster.Tools.ExecutiveSummaryScheduler
{
    /// Name		: Riskmaster.Tools.ExecutiveSummaryScheduler
    /// Author		: Shradha
    /// Date Created: 22 June 2011
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// This will run to Print Froi and acord in batch,it can run from taskmanager as well as silently
    /// </summary>
    /// <remarks>*	The tool can either be run directly or can be executed via taskmanager in silent mode.
    /// Command line parameters-
    /// BatchFroiAndAcord.exe uid pwd DSN adminUid,adminPwd</remarks>
    public class Program
    {
        #region declarations

        
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static int m_iLoginID = 0;
        static string m_sLoginPwd = string.Empty;
        static string m_sModuleName = string.Empty;
        static ModuleType m_iModuleType;
        static string m_sDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string m_strPDFTemplatePath = string.Empty;
        static string m_strPDFOutputPath = string.Empty;
        static string m_strAcordPDFOutputPath = string.Empty;
        static string m_strPDFUrlPath = string.Empty;
        static DbConnection m_objDbConnection = null;
        static DbCommand m_objCmd = null;
        static int m_iModuleId;
        static string m_sTableName = string.Empty;
        static int m_DocPathType;
        static string m_sPdfFileName = string.Empty;
        static string m_sAcordPdfFileName = string.Empty;
        static string m_sPdfBasePath = string.Empty;
        static string m_sTempDirPath = string.Empty;
        static string m_sAcordTempDirName = string.Empty;
        static string m_sExecSummaryTempDirName = string.Empty;
        static Riskmaster.Settings.SysSettings objSettings ;//= new SysSettings(AppGlobals.ConnectionString);


        private static int m_iEventId;
        private static  int m_iClaimId;
        private static bool m_bActivateFilter;
        private static  string m_sFilterSQL;
        private static  bool m_bPrintSelectedNotes;
        private static  int m_iClaimProgressNoteId;
        private static  int m_iNoteTypeCod;
        private static int m_iPolicyId; //averma62 MITS - 27826

        private static bool m_bIsDebugMode = false;

        private static int m_iClientId = 0;//sonali for jira- RMACLOUD 2515
        
        // added by atavaragiri for mits 25686 //
        public static RecordCollection rcEvent, rcClaim, rcPolicy; //averma62 MITS - 28002

        public enum ModuleType
        {
            events = 0,
            claims = 1,
            admintracking = 2,
            progressNotes = 3
        }

        public struct RecordCollection
        {
            public string EventNumber;
            public string PolicyNumber;
            public string ClaimNumber;

        }


        private static DbReader m_objDbReaderRetriveRc = null;

        //FileStorageManager objFileStorageManager = null;
        //FileStorageManager objFileStorageManagerIndividualUser = null;
        //static StorageType m_enmDocumentStorageType = 0;
        //static string m_sDestinationStoragePath = string.Empty;

        #endregion


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Login objLogin = null;
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            try
            {
                

                int argCount = (args == null) ? 0 : args.Length;
                if (argCount > 0)//Entry from TaskManager
                //if(1==1)
                {
                    GetCommandLineParameters(args);
                    /*   m_sDataSource="rmACloud";
                        m_sLoginName="cloud";
                        m_sLoginName = "cloud";
                        m_iClientId = 1;*/
                    if (string.IsNullOrEmpty(m_sDataSource)
                        || string.IsNullOrEmpty(m_sLoginName)
                        || string.IsNullOrEmpty(m_sLoginPwd))
                    {
                        throw new Exception("parameters missing.");
                    }
                    
                    writeLog("\nAuthenticating.... \n");
                    //Thread.Sleep(15000); //debug 

                    //AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sLoginPwd, m_sDataSource);
                    AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sDataSource,m_iClientId);

                    //AppGlobals.Userlogin.objUs
                    //AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sDataSource);
                    //objUserLogin == null || objUserLogin.LoginName == string.Empty || objUserLogin.DatabaseId == 0)
                    if (AppGlobals.Userlogin == null 
                        || AppGlobals.Userlogin.DatabaseId == 0 
                        || AppGlobals.Userlogin.LoginName == string.Empty )
                    {
                        throw new Exception("Authentication failure.");
                    }
                    Initalize(AppGlobals.Userlogin);
                }
                else//Entry for direct run
                {
                    // Direct run just for debug purposes right now... The cliam ID etc paremeter still need to be provided.

                    // If the application is not running from TaskManager, we pick the client id from config.
                    m_iClientId = Convert.ToInt32(ConfigurationManager.AppSettings["ClientId"].ToString()); 
                    objLogin = new Login(m_iClientId);//sonali for jira- RMACLOUD 2515
                    bool bCon = false;

                    bCon = objLogin.RegisterApplication(0, 0, ref AppGlobals.Userlogin, m_iClientId);//sonali for jira- RMACLOUD 2515
                    if (bCon)
                    {
                        Initalize(AppGlobals.Userlogin);
                    }
                    else
                    {
                        throw new Exception("Unauthenticated");
                    }

                }

                AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);    
                //nsachdeva2 MITS-27124 02/02/2012
                rcEvent = GetRecordValues("event", m_iEventId);
                rcClaim = GetRecordValues("claim", m_iClaimId);
                rcPolicy = GetRecordValues("policy", m_iPolicyId);//averma62 MITS - 28002
                //End MITS: 27124
                objSettings = new SysSettings(AppGlobals.ConnectionString, m_iClientId);//sonali for jira- RMACLOUD 2515

                //txtDocPathType.Text = (Convert.ToBoolean(AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType)) ? "Database" : "File System";

                // validating for upload locations here.
                if ((!objSettings.UseAcrosoftInterface))                
                {
                    if (!(Convert.ToBoolean(AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType)))
                    {
                        //File System 
                        if (AppGlobals.Userlogin.DocumentPath.Length > 0)
                        {
                            if (!Directory.Exists(AppGlobals.Userlogin.DocumentPath))
                            {
                                throw new Exception("\nUser doc path does not exists");
                            }
                            writeLog("\nUser Doc path :" + AppGlobals.Userlogin.DocumentPath);
                        }
                        else if (AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                        {
                            
                           if (!Directory.Exists(AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath))
                            {
                                throw new Exception("DSN doc path does not exists");
                            }
                            writeLog("\nDSN Doc path :" + AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath);
                        }
                        else
                        {
                            throw new Exception("\nDoc path not set");
                        }
                    }
                }
                
                // validating the PDF temp storage path here.
                if (!string.IsNullOrEmpty(Riskmaster.Common.RMConfigurator.BasePath))
                {
                    if (!Directory.Exists
                            (Path.Combine(Riskmaster.Common.RMConfigurator.BasePath, @"temp\ExecSummaryTemp"))
                        )
                    {
                        throw new Exception(@"Acess to temp\ExecSummaryTemp unavailable");
                    }
                }
                else
                {
                    throw new Exception(@"\nBasePath config not found");
                }


                StartExecSummSchedulerProcess();
                //aaggarwal29: MITS 35212 start
                if (AppGlobals.htErrors.Count > 0)
                {
                    smsg = " Application completed with errors";
                    throw new Exception(smsg);
                }
                //aaggarwal29: MITS 35212 end
                
            }
            catch (Exception ex)
            {
                //aaggarwal29: MITS 35212 start
                smsg = ex.Message;
                FormatAndDisplayExcMsg(smsg);
                //aaggarwal29: MITS 35212 start
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();

                }

                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();

            }
        }

        //aaggarwal29: MITS 35212 start
        /// <summary>
        /// Format Exception Message returned from Stored Procedure.
        /// </summary>
        /// <param name="smsg">Exception Message</param>
        private static void FormatAndDisplayExcMsg(string smsg)
        {
            string[] sMsgArray = smsg.Split('\n');

            int iLengthofArray = sMsgArray.Length;
            for (int i = 0; i < iLengthofArray; i++)
            {
                sMsgArray[i] = sMsgArray[i].Replace("'", "");
                sMsgArray[i] = sMsgArray[i].Replace("-", " ");
                Console.WriteLine("1001 ^*^*^ {0} ", sMsgArray[i]);
            }
        }
        //aaggarwal29: MITS 35212 end

        private static void GetCommandLineParameters(string[] p_args)
        {
            int iLength = 0;
            bool bResult = false;
            string sPrefix = string.Empty;
            string sRebuildAll = string.Empty;
            iLength = p_args.Length;

            for (int i = 0; i < iLength; i++)
            {
                sPrefix = p_args[i].Trim();

                if (sPrefix.Length > 3)
                {
                    sPrefix = sPrefix.Substring(0, 3);
                }
                switch (sPrefix.ToLower())
                {
                    case "-ds":
                        m_sDataSource = p_args[i].Trim().Substring(3);
                        break;
                    case "-ru":
                        m_sLoginName = p_args[i].Trim().Substring(3);
                        break;
                    case "-rp":
                        m_sLoginPwd = p_args[i].Trim().Substring(3);
                        m_sLoginPwd = XmlConvert.DecodeName(m_sLoginPwd);
                        m_sLoginPwd = RMCryptography.DecryptString(m_sLoginPwd);
                                //.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;")
                                //)
                            //);
                        break;
                    case "-ev":
                        m_iModuleType = ModuleType.events;
                        int.TryParse( p_args[i].Trim().Substring(3), out m_iModuleId ); 
                        break;
                    case "-cl":
                        m_iModuleType = ModuleType.claims;
                        int.TryParse(p_args[i].Trim().Substring(3), out m_iModuleId); 
                        break;
                    case "-at":
                        m_iModuleType = ModuleType.admintracking;
                        int.TryParse(p_args[i].Trim().Substring(3), out m_iModuleId); 
                        break;
                    case "-tb":
                        m_iModuleType = ModuleType.admintracking;
                        m_sTableName = p_args[i].Trim().Substring(3);
                        break;
                    case "-nt":
                        m_iModuleType = ModuleType.progressNotes;
                        //m_iTableName = p_args[i].Trim().Substring(3);
                        break;

                    case "-ne" : m_iEventId =  Conversion.CastToType<Int32>( p_args[i].Trim().Substring(3),out bResult);
                    //nsachdeva2 MITS-27124 02/02/2012    
                    //Commented and moved after this function call which db connection get initialized.
                        /* added by atavaragiri for mits 25686 : */ //rcEvent = GetRecordValues("event", m_iEventId);
                        break;
                    case "-nc" : m_iClaimId =  Conversion.CastToType<Int32>( p_args[i].Trim().Substring(3),out bResult);
                        //nsachdeva2 MITS-27124 02/02/2012
                        //Commented and moved after this function call which db connection get initialized.
                        /*added by atavaragiri for mits 25686 : */ //rcClaim = GetRecordValues("claim", m_iClaimId);
                        break;
                    case "-na" : m_bActivateFilter = Conversion.CastToType<bool>(p_args[i].Trim().Substring(3),out bResult);
                        break;
                    case "-nf" : m_sFilterSQL =p_args[i].Trim().Substring(3);
                        break;
                    case "-np": m_bPrintSelectedNotes = Conversion.CastToType<bool>(p_args[i].Trim().Substring(3), out bResult);
                        break;
                    case "-nn": m_iClaimProgressNoteId = Conversion.CastToType<Int32>(  p_args[i].Trim().Substring(3),out bResult);
                        break; ;
                    case "-ny": m_iNoteTypeCod = Conversion.CastToType<Int32>(p_args[i].Trim().Substring(3), out bResult);
                        break;
                        //Start averma62
                    case "-no": m_iPolicyId = Conversion.CastToType<Int32>(p_args[i].Trim().Substring(3), out bResult);
                        break;
                    case "-ci": m_iClientId = Conversion.CastToType<Int32>(p_args[i].Trim().Substring(3), out bResult);
                        break;
                    //End averma62
                }
            }
        }

        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            m_sDSNID = p_oUserLogin.DatabaseId.ToString();
            m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
            m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_DocPathType = p_oUserLogin.objRiskmasterDatabase.DocPathType;
            m_iLoginID = p_oUserLogin.UserId;
        }

        public string GetExecSummaryPdfName(string sPdfCreated)
        {
            string sExecSUmmaryPdfname = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;

            sTemp = "EXEC_SUMMARY.pdf";
            sFdfPathTemp = sPdfCreated.Remove(sPdfCreated.LastIndexOf("\\") + 1);
            sFdfPathTemp = sFdfPathTemp + sTemp;
            sExecSUmmaryPdfname = sFdfPathTemp;

            return sExecSUmmaryPdfname;
        }

        private static bool StartExecSummSchedulerProcess()
        {
            //ExecutiveSummary objExecSummary = null;
            string sFilePath = string.Empty;
            string sRequestHost = string.Empty;
            string sPdfPath = string.Empty;
            string sFdfPath = string.Empty;
            string sPdfCreated = string.Empty;
            string sFinalPdf = string.Empty;
            try
            {

                ReadConfigSettings();

                if (AppGlobals.bEmailFile)
                {
                    if (string.IsNullOrEmpty(AppGlobals.Userlogin.objUser.Email))
                    {
                        writeLog("Error: Sender Email address missing. Please make sure that the email addresses are provided in SMS");
                    }
                }

                //string Conn = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;                

                //m_iModuleType = ModuleType.progressNotes;

                if (m_iModuleType == ModuleType.progressNotes)
                {
                    writeLog("\nProcessing EnhancedNotes PDF");
                    sPdfPath = CreateEnhcNotesPdf();
                }
                else
                {
                    writeLog("\nProcessing Executive Summary");
                    sPdfPath = CreateExecSummaryPdf();
                }

                #region File lock check here 
                                
                
                //rsolanki2: sometimes the file still remains locked int he file system even when the ComponentnOne Pdf has created the file
                // calling GC may remove the file systen locks

                //writeLog("\nFileLock Detected.");
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //Thread.Sleep(10000);
                

                #endregion

                if (AppGlobals.bEmailFile)
                {
                    SendEmail(sPdfPath);
                }

                if (AppGlobals.bUploadFile)
                {
                    //Riskmaster.Settings.SysSettings objSettings = new SysSettings(AppGlobals.ConnectionString);
                    //objSettings
                    if ((!objSettings.UseAcrosoftInterface)
                        || m_iModuleType == ModuleType.admintracking)
                    {
                        //sPdfPath = @"C:\Program Files\CSC\RiskMaster\temp\ExecSummaryTemp\Exec_Summ_Claim_RecId_12226_DTTM_-8588898430497346391.pdf";
                        UploadFinalFileToRMDocManagement(sPdfPath);
                    }
                    else
                    {
                        writeLog("ACrosoft interface enabled. Uploading to MCM");
                        UploadFinalFileToMCM(sPdfPath);
                    }
                }               

                if (AppGlobals.bCreateDiary)
                {
                    GenerateDiary();
                    writeLog("\nCreate Diary process completed\n");
                }

                if (AppGlobals.bDeleteFileAfterUpload)
                {
                    writeLog("\nDeleting file from the temp location");
                    File.Delete(sPdfPath);                    
                }

            }

            catch (Exception ex)
            {   // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR: [StartExecSummSchedulerProcess]" +ex.Message + " " + ex.StackTrace);
                //throw (ex);
            }
          
            return true;

        }

        private static void ReadConfigSettings()
        {
            writeLog("reading config settings");

            NameValueCollection nvCollSettings = RMConfigurationManager.GetNameValueSectionSettings("ExecSummarySchedulerSettings", m_sDbConnstring,m_iClientId);

            bool.TryParse(nvCollSettings["DeleteFileAfterUpload"], out AppGlobals.bDeleteFileAfterUpload);
            bool.TryParse(nvCollSettings["UploadFile"], out AppGlobals.bUploadFile );
            bool.TryParse( nvCollSettings["EmailFile"], out AppGlobals.bEmailFile );
            bool.TryParse( nvCollSettings["CreateDiary"], out AppGlobals.bCreateDiary );
            //AppGlobals.sSmtpServer = nvCollSettings["SmtpServer"];
            //AppGlobals.sFromEmailAddress = nvCollSettings["FromEmailAddress"];
            //AppGlobals.sFromEmailName = nvCollSettings["FromEmailName"];


            if (AppGlobals.bEmailFile)
            {
                DbReader objReader = null;
                try
                {
                    objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", m_iClientId)//sonali for jira- RMACLOUD 2515
                        , "SELECT SMTP_SERVER, ADMIN_EMAIL_ADDR FROM SETTINGS");
                    if (m_bIsDebugMode) writeLog("ReadConfigSettings: executing sql :-SELECT SMTP_SERVER, ADMIN_EMAIL_ADDR FROM SETTINGS");
                    if (objReader.Read())
                    {
                        AppGlobals.sSmtpServer = Conversion.ConvertObjToStr(objReader[0]);
                        AppGlobals.sFromEmailAddress = Conversion.ConvertObjToStr(objReader[1]);
                    }
                }
                catch (Exception p_oException)
                {   // atavaragiri mits 25686 //
                    AppGlobals.htErrors.Add(p_oException.Message, p_oException.StackTrace);
                    writeLog(string.Format("ERROR:[ReadConfigSettings] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                    //throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
                }

                //AppGlobals.sSmtpServer = nvCollSettings["SmtpServer"];
                //AppGlobals.sFromEmailAddress = nvCollSettings["FromEmailAddress"];
                //AppGlobals.sFromEmailName = nvCollSettings["FromEmailName"];

                if (string.IsNullOrEmpty(AppGlobals.sSmtpServer)
                        || string.IsNullOrEmpty(AppGlobals.sFromEmailAddress))
                {
                    writeLog("Smtp server IP or Admin Email setting missing. Please set SMTP server details.");
                    //System.Windows.Forms.Application.Exit();                
                }
            }
           
        }

        private static void SendEmail(string sPdfPath)
        {
            string body = string.Empty;
            SmtpClient sc;
            MailMessage myMail = new MailMessage();
            MailAddress maFrom;
            Attachment mAttachment = new Attachment(sPdfPath);
            
            try
            {
                writeLog("\nEmailing Document");

                RecordCollection rcTemp;
                string sModuleNumber = string.Empty;

                sc = new SmtpClient(AppGlobals.sSmtpServer);
                //myMail = new MailMessage();//txtFrom.Text, txtTo.Text, txtSubject.Text, txtBody.Text);

                maFrom = new MailAddress(AppGlobals.sFromEmailAddress);//, AppGlobals.sFromEmailName);

                myMail.From = maFrom;
                myMail.To.Add(AppGlobals.Userlogin.objUser.Email);
                //myMail.CC.Add("rsolanki2@csc.com");

                //myMail.BodyFormat  = MailFormat.Html;
                //SmtpServer.Port = 587;
                ////SmtpServer.Credentials = new System.Net.NetworkCredential("username", "password");
                //SmtpServer.EnableSsl = true;
                //if (m_iModuleType == ModuleType.progressNotes)
                //{
                //    body = @"Please find attached the Enhanced Notes Pdf";
                //    myMail.Subject = "EnhcNotesPdf";
                //}
                //else
                //{
                //    body = @"Please find attached the ExecSummary report";
                //    myMail.Subject = "ExecSummaryReport";
                //}

                if (m_iModuleType == ModuleType.events)
                {
                    rcTemp = GetRecordValues("event", m_iModuleId);
                    sModuleNumber = rcTemp.EventNumber;

                    myMail.Subject = "ExecSummaryReport ( EVENT: " + sModuleNumber + " )";
                    body = @"Please find attached the ExecSummary report";

                }
                else if (m_iModuleType == ModuleType.claims)
                {
                    rcTemp = GetRecordValues("claim", m_iModuleId);
                    sModuleNumber = rcTemp.ClaimNumber;

                    myMail.Subject = "ExecSummaryReport ( CLAIM: " + sModuleNumber + " )";
                    body = @"Please find attached the ExecSummary report";

                }
                else if (m_iModuleType == ModuleType.admintracking)
                {
                    DataModelFactory objDataModelFactory = null;
                    objDataModelFactory = new DataModelFactory(m_sDataSource, m_sLoginName, m_sLoginPwd,m_iClientId);
                    ADMTable objADMTable = null;

                    objADMTable = (ADMTable)objDataModelFactory.GetDataModelObject("ADMTable", false);
                    objADMTable.MoveTo(m_iModuleId);
                    sModuleNumber = objADMTable.TableId.ToString();

                    myMail.Subject = "ExecSummaryReport (ADMT- " + objADMTable.TableName + " : " + sModuleNumber + " )";
                    body = @"Please find attached the ExecSummary report";

                    objADMTable.Dispose();
                    objDataModelFactory.Dispose();

                }
                else if (m_iModuleType == ModuleType.progressNotes)
                {
                    body = @"Please find attached the Enhanced Notes Pdf";
                    //Start averma62 MITS - 28002
                    //if(m_iPolicyId!=0)
                    //{
                    //    sModuleNumber = rcPolicy.PolicyNumber;
                    //    myMail.Subject = "EnhcNotesPdf ( POLICY: " + sModuleNumber + " )";
                    //}
                    //else
                    // {
                    //    if (m_iClaimId == 0)
                    //    {
                    //        //rcTemp = GetRecordValues("event", m_iEventId);
                    //        //sModuleNumber = rcTemp.EventNumber;

                    //        // commented above and added below for mits 25686 //
                    //          sModuleNumber = rcEvent.EventNumber;


                    //        myMail.Subject = "EnhcNotesPdf ( EVENT: " + sModuleNumber + " )";
                    //    }
                    //    else
                    //    {
                    //        //rcTemp = GetRecordValues("claim", m_iClaimId);
                    //        //sModuleNumber = rcTemp.ClaimNumber;
                        
                    //        // commented above and added below for mits 25686 //
                    //          sModuleNumber = rcClaim.ClaimNumber;
                       
                    //        myMail.Subject = "EnhcNotesPdf ( CLAIM: " + sModuleNumber + " )";

                    //    }
                    if(m_iEventId > 0|| m_iClaimId>0)
                    {
                        if (m_iClaimId == 0)
                        {
                          
                            sModuleNumber = rcEvent.EventNumber;


                            myMail.Subject = "EnhcNotesPdf ( EVENT: " + sModuleNumber + " )";
                        }
                        else
                        {
                            sModuleNumber = rcClaim.ClaimNumber;

                            myMail.Subject = "EnhcNotesPdf ( CLAIM: " + sModuleNumber + " )";

                        }

                    }
                    else
                    {
                        if (m_iPolicyId > 0)
                        {
                            sModuleNumber = rcPolicy.PolicyNumber;
                            myMail.Subject = "EnhcNotesPdf ( POLICY: " + sModuleNumber + " )";
                        }

                 }
                    //End averma62 MITS - 28002
                }

                myMail.Body = body; //set the body message

                //mAttachment = new Attachment(sPdfPath);
                myMail.Attachments.Add(mAttachment); //add the attachment

                sc.Send(myMail);

                writeLog("\nEmailing operation complete");
                writeLog("\nNote: Email sent to the ID configured in SMS. Please check this configured Email ID if no mail received");

            }
            catch (Exception ex)
            {   // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("Error: [SendEmail] " + ex.Message);
                //System.Windows.Forms.Application.Exit();                
            }
            finally 
            {
                mAttachment.Dispose();
                myMail.Dispose();
            }    

            //var fromAddress = new MailAddress("from@gmail.com", "From Name");
            //var toAddress = new MailAddress("to@example.com", "To Name");
            //const string fromPassword = "fromPassword";
            //const string subject = "Subject";
            //const string body = "Body";

            //var smtp = new SmtpClient
            //{
            //    Host = "smtp.gmail.com",
            //    Port = 587,
            //    EnableSsl = true,
            //    DeliveryMethod = SmtpDeliveryMethod.Network,
            //    UseDefaultCredentials = false,
            //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            //};
            //using (var message = new MailMessage(fromAddress, toAddress)
            //{
            //    Subject = subject,
            //    Body = body
            //})
            //{
            //    smtp.Send(message);
            //}

        }

        private static string GetPdfPath()
        {
            string sBasepath = string.Empty;
            string sTemp = @"Temp\ExecSummaryTemp\";

            try
            {
                //sTemp = @"riskmaster\ExecSummaryTemp\";
                //sBasepath = Riskmaster.Common.RMConfigurator.BasePath;
                //sBasepath = sBasepath.Remove((sBasepath.Length - 10), 10);
                //sBasepath = sBasepath + sTemp;
                sBasepath = Riskmaster.Common.RMConfigurator.BasePath.Remove((sBasepath.Length - 10), 10) + sTemp;

            }
            catch (Exception ex)
            {   // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR [getPdfPath] :" +ex.Message + " " + ex.StackTrace);
                //Log.Write(ex.Message, "CommonWebServiceLog");
                //throw ex;
            }
            return sBasepath;

        }

        public static string CreateExecSummaryPdf()
        {

            ExecutiveSummary objExec = new ExecutiveSummary(AppGlobals.Userlogin.UserId, m_sDbConnstring,m_iClientId);
            string sfilename = string.Empty;
            
            try
            {
                sfilename = Path.Combine(Riskmaster.Common.RMConfigurator.BasePath, @"temp\ExecSummaryTemp")
                         + @"\Exec_Summ_"
                         + (
                                (m_iModuleType == ModuleType.claims)
                                ? "Claim"
                                : ((m_iModuleType == ModuleType.events)
                                    ? "Events"
                                    : "AdminTracking"
                                        + "_table_"
                                        + m_sTableName
                                    )
                            )
                         + "_RecId_"
                         + m_iModuleId
                         + "_DTTM_"
                         + DateTime.Now.ToString("yyyyMMddHHmmss") 
                         + DateTime.Now.ToString().Replace("/","-").Replace(":","-").Replace(" ","-")
                         + ".pdf";
                writeLog("\nWriting to file: " +sfilename);

                objExec.m_UserLogin = AppGlobals.Userlogin;

                if (m_iModuleType == ModuleType.claims)
                {
                    writeLog("creating executive summary for claim now\n");
                    objExec.ClaimExecSumm(m_iModuleId, sfilename);
                }
                if (m_iModuleType == ModuleType.events)
                {
                    writeLog("creating executive summary for event now\n");
                    objExec.EventExecSumm(m_iModuleId, sfilename);
                }
                if (m_iModuleType == ModuleType.admintracking)
                {
                    writeLog("creating executive summary for admin tracking now\n");
                    objExec.AdmTrackingExecSumm(m_iModuleId, m_sTableName, sfilename);
                }

                //sfilename = sfilename + "\\EXEC_SUMMARY_" + DateTime.Now.ToBinary()+ ".pdf";
                //byte[] pdfbytes = objPDFMemoryStream.ToArray();
                //file = new FileStream(sfilename, FileMode.Create);
                //file.Write(pdfbytes, 0, pdfbytes.Length);
                //file.Close();


            }
            catch (Exception ex)
            {   // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR [CreateSummary]:" + ex.Message + " " + ex.StackTrace);
                //throw (ex);
            }
            return sfilename;

        }

        public static  string CreateEnhcNotesPdf() 
        {
            ProgressNotesManager objProgressNotesManager = null;
            bool bReturnValue = false;
            string sfilename = string.Empty;           
            string sPdfDocPath = string.Empty;

            try
            {

                objProgressNotesManager = new ProgressNotesManager
                    (AppGlobals.Userlogin.objRiskmasterDatabase.DataSourceName
                        , AppGlobals.Userlogin.LoginName
                        , AppGlobals.Userlogin.Password, m_iClientId);

                //bReturnValue = objProgressNotesManager.PrintProgressNotesReport
                //    (m_iEventId, m_iClaimId, m_bActivateFilter, m_sFilterSQL,
                //        ref sPdfDocPath, m_bPrintSelectedNotes, m_iClaimProgressNoteId, m_iNoteTypeCod,0); //rsolanki2:debug

                bReturnValue = objProgressNotesManager.PrintProgressNotesReport
                (m_iEventId, m_iClaimId, m_bActivateFilter, m_sFilterSQL,
                    ref sPdfDocPath, m_bPrintSelectedNotes, m_iClaimProgressNoteId, m_iNoteTypeCod, m_iPolicyId, -1); //averma62 MITS - 27826 - initially policy id was set to 0. Changed to pass policy id//Added by gbindra MITS#34104 WWIG GAP15

                writeLog("\nNotes generated at to file: " + sPdfDocPath);

            }
            catch (Exception ex)
            {    // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR [CreateNotes]:" + ex.Message + " " + ex.StackTrace);
                //throw (ex);
            }
            return sPdfDocPath;

        }

        public static void UploadFinalFileToRMDocManagement(string sSourceDir)
        {
            string sName = string.Empty;
            string sGlobalDocPath = string.Empty;
            //MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            //FileStorageManager objmanager = null;
            string sRMXUserName = string.Empty;
            string sDSNName = string.Empty;

            try
            {
                writeLog("\nAttaching Pdf to RMX Document Management Module\n");

                if (sSourceDir != null && sSourceDir.Trim() != "")
                {
                    sName = sSourceDir.Substring((sSourceDir.LastIndexOf("\\") + 1));

                    writeLog("\ncreating Riskmaster.FileStorageManager object\n");

                    //objmanager = new FileStorageManager((StorageType)AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType, AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath);

                    if (!File.Exists(sSourceDir))
                        throw new FileNotFoundException(Globalization.GetString("Function.FileNotFound.Error"));

                    objFileStream = File.Open(sSourceDir, FileMode.Open);

                    writeLog("\ncreating Riskmaster.DataStreamingServiceClient object\n");
                    DataStreamingService.DataStreamingServiceClient rmservice = null;
                    rmservice = new DataStreamingService.DataStreamingServiceClient();

                    DataStreamingService.StreamedDocumentType document = null;
                    document = new DataStreamingService.StreamedDocumentType();

                    writeLog("\ncreating Riskmaster.AuthenticationServiceClient object\n");
                    AuthenticationService.AuthenticationServiceClient auService = null;
                    auService = new AuthenticationService.AuthenticationServiceClient();

                    //sRMXUserName = AppGlobals.Userlogin.UserId.ToString();
                    //sDSNName = AppGlobals.Userlogin.objRiskmasterDatabase.DataSourceName;

                    sRMXUserName = m_sLoginName;
                    sDSNName = m_sDataSource;

                    document.Class = new DataStreamingService.StreamedCodeType();
                    document.Type = new DataStreamingService.StreamedCodeType();
                    document.Category = new DataStreamingService.StreamedCodeType();
                    document.Token = auService.GetUserSessionID(sRMXUserName, sDSNName);

                    if (m_iModuleType == ModuleType.events)
                    {
                        document.Title = "EVENT";
                        document.Subject = "EVENT ExecutiveSummary";
                        document.AttachTable = "EVENT";
                        document.AttachRecordId = m_iModuleId;
                    }
                    else if (m_iModuleType == ModuleType.claims)
                    {
                        document.Title = "CLAIM";
                        document.Subject = "CLAIM ExecutiveSummary";
                        document.AttachTable = "CLAIM";
                        document.AttachRecordId = m_iModuleId;
                    }
                    else if (m_iModuleType == ModuleType.admintracking)
                    {
                        document.Title = "ADMIN TRACKING";
                        document.Subject = "ADMIN TRACKING ExecutiveSummary";
                        document.AttachTable = "ADMIN_TRACKING";
                        document.AttachRecordId = m_iModuleId;
                    }
                    else if (m_iModuleType == ModuleType.progressNotes)
                    {
                        document.Title = "ENHANCED NOTES PDF";
                        document.Subject = "Enhc Notes";
                        //Start averma62 MITS - 27826 -- case to set table name for policy
                        //if (m_iPolicyId != 0)
                        //{
                        //    document.AttachTable = "POLICY";
                        //    document.AttachRecordId = m_iPolicyId;
                        //}
                        //else
                        //{
                        //    //End averma62 MITS - 27826
                        //    if (m_iClaimId == 0)
                        //    {
                        //        document.AttachTable = "EVENT";
                        //        document.AttachRecordId = m_iEventId;
                        //    }
                        //    else
                        //    {
                        //        document.AttachTable = "CLAIM";
                        //        document.AttachRecordId = m_iClaimId;
                        //    }
                        //}//averma62 MITS - 27826


                        if (m_iClaimId > 0 || m_iEventId >0)
                        {

                            if (m_iClaimId == 0)
                            {
                                document.AttachTable = "EVENT";
                                document.AttachRecordId = m_iEventId;
                            }
                            else
                            {
                                document.AttachTable = "CLAIM";
                                document.AttachRecordId = m_iClaimId;
                            }

                           
                        }
                        else
                        {
                            if (m_iPolicyId > 0)
                            {
                                document.AttachTable = "POLICY";
                                document.AttachRecordId = m_iPolicyId;
                            }
                        }//averma62 MITS - 27826
                    }                    

                    //document.FilePath = sSourceDir;
                    document.FilePath = string.Empty;
                    document.UserId = m_sLoginName;
                    document.UserName = m_sDBOUserId;


                    document.Notes = string.Empty;
                    document.Keywords = string.Empty;
                    document.FileName = sName;
                    document.fileStream = objFileStream;
                    document.FileSize = objFileStream.Length.ToString();

                    document.Copy = 1;
                    document.Create = 1;
                    document.CreateDate = DateTime.Now.ToString();
                    document.Delete = 0;
                    document.DocInternalType = string.Empty;
                    document.DocumentCategory = string.Empty;
                    document.DocumentClass = string.Empty;
                    document.DocumentsType = string.Empty;
                    document.Download = 1;
                    document.Edit = 1;
                    document.Email = 1;
                    document.Errors = string.Empty;
                    document.FolderId = string.Empty;
                    document.FolderName = string.Empty;
                    document.FormName = string.Empty;
                    document.Keywords = string.Empty;
                    document.Move = 0;
                    document.Pid = string.Empty;
                    document.PsId = 0;
                    document.Readonly = 0;
                    document.ScreenFlag = string.Empty;
                    //document.Subject = string.Empty;
                    //document.Title = string.Empty;
                    document.Transfer = 0;
                    document.Type = new DataStreamingService.StreamedCodeType();
                    document.View = 1;

                    writeLog("\nuploading Pdf...\n");

                    rmservice.CreateDocument(document.AttachRecordId,
                        document.AttachTable,
                        document.Category,
                        "", //claim number
                        document.Class,
                        document.ClientId,
                        document.Copy,
                        document.Create,
                        document.CreateDate,
                        document.Delete,
                        document.DocInternalType,
                        document.DocumentCategory,
                        document.DocumentClass,
                        document.DocumentId,
                        document.DocumentsType,
                        document.Download,
                        document.Edit,
                        document.Email,
                        document.Errors,
                        document.FileContents,
                        document.FileName,
                        document.FilePath,
                        document.FileSize,
                        document.FolderId,
                        document.FolderName,
                        document.FormName,
                        document.Keywords,
                        document.Move,
                        document.Notes,
                        document.Pid,
                        document.PsId,
                        document.Readonly,
                        document.ScreenFlag,
                        document.Subject,
                        document.Title,
                        document.Token,
                        document.Transfer,
                        document.Type,
                        document.UserId,
                        document.UserName,
                        document.View,
                        document.fileStream);

                    objFileStream.Close();


                    //todo: we would need to delete the PDf object post upload.
                    //File.Delete(sSourceDir);

                }
                else
                {
                    throw new Exception("PDF file not found for Upload");
                }
            }
            catch (Exception ex)
            {
                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);

                writeLog("ERROR [CopyFinalFileToDir]:" + ex.Message + " " + ex.StackTrace);
                //Log.Write(ex.Message, "CommonWebServiceLog");
                //throw (ex);
            }
            finally
            {

                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream.Dispose();
                }              
            }

        }


        protected static void GenerateDiary()
        {
            DataModelFactory objDataModelFactory = null;
            WpaDiaryEntry objDiary = null;
            String sAttachTable = string.Empty;
            String sDiaryNotes = string.Empty;
            String sRegarding = string.Empty;
            String sModuleNumber = string.Empty;
            //Event objEvent = null;
            //Claim objClaim = null;
            ADMTable objADMTable = null;

            try
            {
                writeLog("generating diaries");
                objDataModelFactory = new DataModelFactory(m_sDataSource, m_sLoginName, m_sLoginPwd, m_iClientId);//sonali for jira- RMACLOUD 2515
                //objDataModelFactory = new DataModelFactory(AppGlobals.Userlogin);
                objDiary = (WpaDiaryEntry)objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);

                RecordCollection rcTemp;
                if (m_iModuleType == ModuleType.events)
                {
                    rcTemp = GetRecordValues("event", m_iModuleId);
                    sModuleNumber = rcTemp.EventNumber;
                    //objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
                    //objEvent.MoveTo(m_iModuleId);
                    //sModuleNumber = objEvent.EventNumber;
                    //sAttachTable = objEvent.Table;
                    sAttachTable = "EVENT";
                    sDiaryNotes = "An Executive Summary Report has been generated for EVENT: " + sModuleNumber;
                    sRegarding = "EVENT: " + sModuleNumber;
                    objDiary.EntryName = "EXECUTIVE SUMMARY REPORT";
                    objDiary.IsAttached = true;
                    objDiary.AttachRecordid = m_iModuleId;

                }
                else if (m_iModuleType == ModuleType.claims)
                {
                    rcTemp = GetRecordValues("claim", m_iModuleId);
                    sModuleNumber = rcTemp.ClaimNumber;
                    //objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    ////objClaim.MoveTo(m_iModuleId);
                    //sModuleNumber = objClaim.ClaimNumber;
                    //sAttachTable = objClaim.Table;
                    sAttachTable = "CLAIM";
                    sDiaryNotes = "An Executive Summary Report has been generated for CLAIM: " + sModuleNumber;
                    sRegarding = "CLAIM: " + sModuleNumber;
                    objDiary.EntryName = "EXECUTIVE SUMMARY REPORT";
                    objDiary.IsAttached = true;
                    objDiary.AttachRecordid = m_iModuleId;
                }
                else if (m_iModuleType == ModuleType.admintracking)
                {
                    objADMTable = (ADMTable)objDataModelFactory.GetDataModelObject("ADMTable", false);
                    objADMTable.MoveTo(m_iModuleId);
                    sModuleNumber = objADMTable.TableId.ToString(); //todo:rsolanki2: check on this
                    sAttachTable = objADMTable.TableName;
                    sDiaryNotes = "An Executive Summary Report has been generated for ADMT Table: " + sModuleNumber;
                    sRegarding = "ADMT : " + sModuleNumber;
                    objDiary.EntryName = "EXECUTIVE SUMMARY REPORT";
                    objDiary.IsAttached = false;
                }
                else if (m_iModuleType == ModuleType.progressNotes)
                {
                    //Start averma62 MITS - 28002
                    //if (m_iPolicyId != 0)
                    //{
                    //    sModuleNumber = rcPolicy.PolicyNumber;
                    //    sAttachTable = "POLICY";
                    //    objDiary.IsAttached = true;
                    //    objDiary.AttachRecordid = m_iPolicyId;
                    //}
                    //else
                    //{
                    //    if (m_iClaimId == 0)
                    //    {
                    //        //rcTemp = GetRecordValues("event",  m_iEventId);
                    //        //sModuleNumber = rcTemp.EventNumber;

                    //        // commented above and added below for mits 25686//
                    //        sModuleNumber = rcEvent.EventNumber;

                    //        //sModuleNumber = m_iEventId.ToString();
                    //        sAttachTable = "EVENT";
                    //        objDiary.IsAttached = true;
                    //        objDiary.AttachRecordid = m_iEventId;

                    //    }
                    //    else
                    //    {
                    //        //rcTemp = GetRecordValues("claim", m_iClaimId);
                    //        //sModuleNumber = rcTemp.ClaimNumber;

                    //        // commented above and added below for mits 25686//
                    //        sModuleNumber = rcClaim.ClaimNumber;

                    //        //sModuleNumber = m_iClaimId.ToString();
                    //        sAttachTable = "CLAIM";
                    //        objDiary.IsAttached = true;
                    //        objDiary.AttachRecordid = m_iClaimId;
                    //    }
                    //}


                    if (m_iEventId  > 0 || m_iClaimId>0)
                    {

                        if (m_iClaimId == 0)
                        {
                            //rcTemp = GetRecordValues("event",  m_iEventId);
                            //sModuleNumber = rcTemp.EventNumber;

                            // commented above and added below for mits 25686//
                            sModuleNumber = rcEvent.EventNumber;

                            //sModuleNumber = m_iEventId.ToString();
                            sAttachTable = "EVENT";
                            objDiary.IsAttached = true;
                            objDiary.AttachRecordid = m_iEventId;

                        }
                        else
                        {
                            //rcTemp = GetRecordValues("claim", m_iClaimId);
                            //sModuleNumber = rcTemp.ClaimNumber;

                            // commented above and added below for mits 25686//
                            sModuleNumber = rcClaim.ClaimNumber;

                            //sModuleNumber = m_iClaimId.ToString();
                            sAttachTable = "CLAIM";
                            objDiary.IsAttached = true;
                            objDiary.AttachRecordid = m_iClaimId;
                        }
                      
                    }
                    else
                    {
                        if (m_iPolicyId > 0)
                        {
                            sModuleNumber = rcPolicy.PolicyNumber;
                            sAttachTable = "POLICY";
                            objDiary.IsAttached = true;
                            objDiary.AttachRecordid = m_iPolicyId;
                        }
                    }
                    //ENd averma62 MITS - 28002
                    sDiaryNotes = "An Enhanced Notes PDF has been generated for "+ sAttachTable + " : "+ sModuleNumber;
                    sRegarding = sAttachTable + " : " + sModuleNumber;

                    objDiary.EntryName = "ENHANCED NOTES PDF";
                    objDiary.EntryNotes = "";
                }

                //objDiary.EntryName = "EXECUTIVE SUMMARY REPORT";

                objDiary.EntryNotes = sRegarding;
                objDiary.Priority = 1;
                objDiary.CreateDate = Conversion.ToDbDateTime(DateTime.Now);
                objDiary.StatusOpen = true;
                objDiary.AutoConfirm = false;
                objDiary.AssignedUser = m_sLoginName;
                objDiary.AssigningUser = m_sLoginName;
                objDiary.Regarding = sDiaryNotes;
                objDiary.CompleteDate = Conversion.ToDbDate(DateTime.Now);
                objDiary.CompleteTime = Conversion.ToDbTime(DateTime.Now);
                //objDiary.IsAttached = true;
                objDiary.AttachTable = sAttachTable;
               
                // atavaragiri mits 25686 //
                if (AppGlobals.htErrors.Count > 0)
                {
                    foreach (DictionaryEntry de in AppGlobals.htErrors)
                    {
                        AppGlobals.sdiaryErrors += de.Key +  ";" ;
                    }
                    objDiary.EntryNotes = AppGlobals.sdiaryErrors;  // logging the error in the diary notes    
                }
                //end mits 25686

                //objDiary.AttachRecordid = m_iModuleId;

                objDiary.Save();
            }
            catch (Exception ex)
            {
                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR [GenerateDiary]:" + ex.Message + " " + ex.StackTrace);
                //Log.Write(ex.Message, "CommonWebServiceLog");
                //throw (ex);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objDiary != null)
                    objDiary = null;
                //if (objEvent != null) objEvent = null;
                //if (objClaim != null) objClaim = null;

            }
        }

        public  string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                //if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {

                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(p_oException.Message, p_oException.StackTrace);
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                //throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

        public static void ExecuteSql(string p_sSQL)
        {
            //if (m_bIsDebugMode) writeLog("ExecuteSql: executing sql " + p_sSQL);
            try
            {
                m_objDbConnection.ExecuteNonQuery(p_sSQL);
            }
            catch (Exception p_oException)
            {

                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(p_oException.Message, p_oException.StackTrace);
                writeLog(string.Format("ERROR:[ExecuteSql]\nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                //throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                //objDbConnection.Dispose();
            }
        }

        public static void writeLog(string p_strLogText)
        {
            //string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);
            //string sMessage = "\n0 ^*^*^ " + p_strLogText;
            //try
            //{
            //    m_sw1.WriteLine(sMessage);
            //Console.WriteLine(sMessage);
            Console.WriteLine("\n0 ^*^*^ " +  p_strLogText.Replace ("\n","\n0 ^*^*^ "));
            //    m_sw1.Flush();
            //}
            //catch (Exception exp)
            //{
            //    throw new Exception("Error Logging Exception :" + exp.Message.ToString(), exp.InnerException);
            //}

        }

        private static void UploadFinalFileToMCM(string sSourceDir)
        {
            #region Declarations
            //string sIndiUserPath = "";            
            MemoryStream objFileContent = null;
            string sSQL = string.Empty;
            //string sDocId = string.Empty;
            string sTableName = string.Empty;
            string sEventNumber = string.Empty;
            string sClaimNumber = string.Empty;
            string sPolicyName = string.Empty;
            string sEventId = string.Empty;
            string sClaimId = string.Empty;
            string sAcrosoftAttachmentsTypeKey = string.Empty;
            string sAcrosoftGeneralStorageTypeKey = string.Empty;
            string sAcrosoftPolicyTypeKey = string.Empty;
            string sAcrosoftUsersTypeKey = string.Empty;
            string sFileName = string.Empty;
            int iRetCd = 0;
            byte[] bFileContent = null;
            string sDocTitle = string.Empty;
            string sCreateTs = string.Empty;
            string sModifiedCreateTs = string.Empty;
            Acrosoft objAcrosoft = null;
            string sAppExcpXml = string.Empty;
            string sSessionId = string.Empty;
            string sReturnXmlstring = string.Empty;
          
            string sAttachedRecord = String.Empty;
            string sPIEmployeeId = string.Empty;
            string sPIPatientId = string.Empty;
            string sPIWitnessId = string.Empty;
            string sEventDatedText = string.Empty;
            string sClaimant = string.Empty;
            string sDefendant = string.Empty;
            string sPolicyId = string.Empty;
            string sFormName = string.Empty;
            string sFormFileName = string.Empty;
            string sAuthor = string.Empty;
            //skhare7 MITS 22743
            string sPlan = string.Empty;
            string sLitigation = string.Empty;
            string sExpert = string.Empty;
            string sAdjusterText = string.Empty;

            //skhare7 MITS   22743 end
            string sEventFolderFriendlyName = "";
            string sClaimFolderFriendlyName = "";
            string sPolicyFolderFriendlyName = "";
            string sUsersFolderFriendlyName = "";
            string sGeneralFolderFriendlyName = "";
            //string sUserDocLoc = "";
            ////string sMigrationStatus = "";
            //bool bMigrationStatus = false;

            //int iRowsCount = 1;
            //int i = 0;
            //long iSize = 0;

            #endregion

            try
            {

                RMConfigurationManager.GetAcrosoftSettings();

                sAcrosoftAttachmentsTypeKey = AcrosoftSection.AcrosoftAttachmentsTypeKey;
                sAcrosoftGeneralStorageTypeKey = AcrosoftSection.AcrosoftGeneralStorageTypeKey;
                sAcrosoftPolicyTypeKey = AcrosoftSection.AcrosoftPolicyTypeKey;
                sAcrosoftUsersTypeKey = AcrosoftSection.AcrosoftUsersTypeKey;
                sEventFolderFriendlyName = AcrosoftSection.EventFolderFriendlyName;
                sClaimFolderFriendlyName = AcrosoftSection.ClaimFolderFriendlyName;
                sPolicyFolderFriendlyName = AcrosoftSection.PolicyFolderFriendlyName;
                sUsersFolderFriendlyName = AcrosoftSection.UsersFolderFriendlyName;
                sGeneralFolderFriendlyName = AcrosoftSection.GeneralFolderFriendlyName;

                objFileContent = new MemoryStream();


                //rsolanki2 :  start updates for MCM mits 19200 
                Boolean bSuccess = false;
                string sTempAcrosoftUserId = AppGlobals.Userlogin.LoginName;
                string sTempAcrosoftPassword = AppGlobals.Userlogin.Password;

                if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                {
                    if (AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName] != null
                        && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].RmxUser))
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].AcrosoftUserId;
                        sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].AcrosoftPassword;
                    }
                    else
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                        sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                    }
                }
                // end 19200

                objAcrosoft = new Acrosoft(m_iClientId);//sonali
                if (objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml) != 0)
                {
                    writeLog("Acrosoft Authentication unsuccessfull. terminating.");
                    System.Windows.Forms.Application.Exit();
                }

                if (m_iModuleType == ModuleType.progressNotes)
                {
                    //Start averma62 - MITS - 28002
                    if (m_iPolicyId != 0)
                    {
                        sTableName = "Policy";
                    }
                    else
                    {
                        if (m_iClaimId == 0)
                        {
                            //sModuleNumber = m_iEventId.ToString();
                            sTableName = "Event";
                        }
                        else
                        {
                            //sModuleNumber = m_iClaimId.ToString();
                            sTableName = "Claim"; //todo:rsolanki2: check on this
                        }
                    }
                    //End averma62 - MITS - 28002
                }
                else if (m_iModuleType == ModuleType.events)
                {
                    sTableName = "Event";
                }
                else if (m_iModuleType == ModuleType.claims)
                {
                    sTableName = "Claim";
                }
                else if (m_iModuleType == ModuleType.admintracking)
                {
                    sTableName = m_sTableName;
                }

                sFileName = Path.GetFileName(sSourceDir);
                sDocTitle = sFileName;



                #region uploading attachment to MCM

                //writeLog("---------------");
                iRetCd = 1;
                //sTableName =  ;//AppGlobals.m_dsDocs.Tables[0].Rows[i]["TABLE_NAME"].ToString();
                sModifiedCreateTs = sCreateTs = DateTime.Now.ToString();
                //AppGlobals.m_dsDocs.Tables[0].Rows[i]["CREATE_DATE"].ToString();
                sAuthor = AppGlobals.Userlogin.LoginName;

                writeLog(string.Format("attempting file transfer for:{0}\n FileType:{1}\nDocument Name:{2}\nauthor:{4} ", sFileName, sTableName, sDocTitle, sCreateTs, sAuthor));

                sAttachedRecord = "";


                objFileContent = new MemoryStream(FileAsByteArray(sSourceDir));

                if (objFileContent.Length == 0)
                {
                    writeLog("Cannot read file. aborting upload..");
                    return;
                }

                RecordCollection rcTemp;
               

                switch (sTableName.ToLower())
                {
                    case "event": sEventId = m_iEventId.ToString();

                        //rcTemp = GetRecordValues("event", m_iEventId);
                        //sEventNumber = rcTemp.EventNumber;

                        // commented above and added below for mits 25686//
                        sEventNumber = rcEvent.EventNumber;

                        writeLog("\nCreating Folder in Mcm");
                        // sEventNumber = objEvent.EventNumber;
                        sAttachedRecord = sEventNumber;

                        if (iRetCd == 1)
                        {

                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }
                        break;
                    case "claim": sClaimId = m_iClaimId.ToString();// AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();


                        //rcTemp = GetRecordValues("claim", m_iClaimId);
                        //sEventNumber = rcTemp.EventNumber;
                        // commented above and added below for mits 25686//
                        sEventNumber = rcClaim.EventNumber;
                        sClaimNumber = rcClaim.ClaimNumber;

                        //sEventNumber = rcTemp.EventNumber;
                       // sClaimNumber = rcTemp.ClaimNumber;


                        writeLog("\nCreating Folder in Mcm");


                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft

                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "piemployee": sPIEmployeeId = m_iModuleId.ToString();

                        sEventNumber = GetRecordValues("piemployee", Conversion.ConvertStrToInteger(sPIEmployeeId)).EventNumber;

                        // sClaimNumber = GetRecordValues(sTableName, Conversion.ConvertStrToInteger(sClaimId)).ClaimNumber;
                        writeLog("\nCreating Folder in Mcm");
                        //sEventNumber = objEvent.EventNumber;
                        sAttachedRecord = sEventNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "pipatient": sPIPatientId = m_iModuleId.ToString();


                        rcTemp = GetRecordValues("pipatient", Conversion.ConvertStrToInteger(sPIPatientId));
                        sClaimNumber = rcTemp.ClaimNumber;
                        sEventNumber = rcTemp.EventNumber;

                        writeLog("\nCreating Folder in Mcm");
                        //  sEventNumber = objEvent.EventNumber;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sEventNumber;


                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "piwitness": sPIWitnessId = m_iModuleId.ToString();


                        rcTemp = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId));

                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;

                        //sEventNumber = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId)).EventNumber;
                        //sClaimNumber = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId)).ClaimNumber;

                        //skhare7 MITS 21874 End
                        writeLog("\nCreating Folder in Mcm");
                        //    sEventNumber = objEvent.EventNumber;
                        sAttachedRecord = sEventNumber;


                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "eventdatedtext": sEventDatedText = m_iModuleId.ToString();

                        sEventNumber = GetRecordValues("eventdatedtext", Conversion.ConvertStrToInteger(sEventId)).EventNumber;
                        //  sClaimNumber = GetRecordValues(sTableName, Conversion.ConvertStrToInteger(sPIWitnessId)).ClaimNumber;
                        writeLog("\nCreating Folder in Mcm");
                        //  sEventNumber = objEvent.EventNumber;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sEventNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "claimant": sClaimant = m_iModuleId.ToString();


                        rcTemp = GetRecordValues("claimant", Conversion.ConvertStrToInteger(sClaimant));
                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;


                        //  sEventNumber = objClaim.EventNumber;
                        //sClaimNumber = objClaim.ClaimNumber;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "defendant": sDefendant = m_iModuleId.ToString();


                        rcTemp = GetRecordValues("defendant", Conversion.ConvertStrToInteger(sDefendant));
                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;

                        writeLog("\nCreating Folder in Mcm");
                        // sEventNumber = objClaim.EventNumber;
                        //  sClaimNumber = objClaim.ClaimNumber;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    //skhare7:MITS 22743
                    case "expert": sExpert = m_iModuleId.ToString();


                        rcTemp = GetRecordValues("expert", Conversion.ConvertStrToInteger(sExpert));
                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;

                        writeLog("\nCreating Folder in Mcm");

                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    case "plan": sPlan = m_iModuleId.ToString();

                        //sEventNumber = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan)).EventNumber;
                        //sClaimNumber = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan)).ClaimNumber;

                        rcTemp = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan));
                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;

                        writeLog("\nCreating Folder in Mcm");

                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;
                    case "litigation": sLitigation = m_iModuleId.ToString();

                        rcTemp = GetRecordValues("litigation", Conversion.ConvertStrToInteger(sLitigation));

                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;


                        writeLog("\nCreating Folder in Mcm");

                        //skhare7 MITS 21874 End
                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;
                    case "adjuster dated text": sAdjusterText = m_iModuleId.ToString();

                        rcTemp = GetRecordValues("adjuster dated text", Conversion.ConvertStrToInteger(sAdjusterText));
                        sEventNumber = rcTemp.EventNumber;
                        sClaimNumber = rcTemp.ClaimNumber;

                        writeLog("\nCreating Folder in Mcm");

                        sAttachedRecord = sClaimNumber;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;
                    //skhare7:MITS 22743 End
                    case "policy": sPolicyId = m_iModuleId.ToString();
                        //skhare7 MITS 21874
                        //   writeLog("creating DM object-policy");
                        //   objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                        //  writeLog("navigating...");
                        // objPolicy.MoveTo(Conversion.ConvertStrToInteger(sPolicyId));
                        sPolicyName = GetRecordValues("policy", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;

                        writeLog("\nCreating Folder in Mcm");
                        //  sPolicyName = objPolicy.PolicyName;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sPolicyName;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreatePolicyFolder(sSessionId, sAcrosoftPolicyTypeKey, sPolicyName, sPolicyFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);

                                //iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;

                    //MITS 18136 : Raman Bhatia 10/14/2009
                    //skhare7 MITS 21889 :Enhanced policy added for different LOB's
                    //Adding MCM support for Policy Management
                    case "policyenh":
                    case "policyenhgl":
                    case "policyenhpc":
                    case "policyenhwc":
                    case "policyenhal":
                        sPolicyId = m_iModuleId.ToString();
                        //skhare7 MITS 21874
                        //  writeLog("creating DM object-policy");
                        //   objPolicyEnh = (PolicyEnh)m_objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                        //  writeLog("navigating...");
                        //  objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(sPolicyId));
                        sPolicyName = GetRecordValues("policyenh", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;
                        writeLog("\nCreating Folder in Mcm");
                        //sPolicyName = objPolicyEnh.PolicyName;
                        //skhare7 MITS 21874 End
                        sAttachedRecord = sPolicyName;

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreatePolicyFolder(sSessionId, sAcrosoftPolicyTypeKey, sPolicyName, sPolicyFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }


                        break;


                    //skhare7 MITS 21889 
                    default:

                        sTableName = "OrphanDocuments";

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            objAcrosoft.CreateUserFolder(sSessionId, sAcrosoftUsersTypeKey, sAuthor, sUsersFolderFriendlyName, out sAppExcpXml);
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sAuthor, sAcrosoftUsersTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftUsersTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                            }

                        }

                        break;
                } // switch

                writeLog("MCM upload output xml :-" + sAppExcpXml);
                //writeLog("Updating Doc transfer status in db");
                if (iRetCd == 0)
                {
                    //udating status in db
                    writeLog("File transfer to MCM successfull");
                }
                else
                {
                    writeLog("File transfer to MCM unsuccessfull");
                }

                #endregion

            }
            catch (Exception exp)
            {
                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(exp.Message, exp.StackTrace);
                writeLog(string.Format("ERROR:[UploadFinalFiletoMCM] \nmessage:{0}\nStackTrace{1}\nFileName{2}  ", exp.Message.ToString(), exp.StackTrace.ToString(), sFileName));
            }
            finally 
            {
                if (objFileContent!=null)
                {
                    objFileContent.Dispose();
                }
                if (objAcrosoft !=null)
                {                    
                    objAcrosoft = null;
                }
            }
        }

        //skhare7 MITS 21874
        public static RecordCollection GetRecordValues(string sTableName, int iPrimaryKey)
        {
            string sSQL = string.Empty;

            //RecordCollection rc = new RecordCollection();
            try
            {

                switch (sTableName.ToLower())
                {

                    case "event":
                        sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };

                        }

                        break;

                    case "claim":

                        sSQL = "SELECT E.EVENT_NUMBER,C.CLAIM_NUMBER FROM CLAIM C,EVENT E WHERE C.EVENT_ID=E.EVENT_ID AND C.CLAIM_ID=" + iPrimaryKey;

                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]), ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };


                        }
                        break;

                    case "pipatient":
                    case "piwitness":
                    case "piemployee":
                        sSQL = "select E.EVENT_NUMBER,C.CLAIM_NUMBER from PERSON_INVOLVED PI,EVENT E,CLAIM C "
                            + "where E.EVENT_ID=PI.EVENT_ID and C.EVENT_ID=E.EVENT_ID and PI.PI_ROW_ID = "
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                            };


                        }
                        break;

                    case "policyenh":
                        sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { PolicyNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };

                        }

                        break;

                    case "eventdatedtext":
                        sSQL = "select E.EVENT_NUMBER  from EVENT_X_DATED_TEXT ED,EVENT E,CLAIM C "
                            + "where ED.EVENT_ID=E.EVENT_ID and ED.EV_DT_ROW_ID="
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };

                        }

                        break;

                    case "claimant":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from CLAIMANT CL,EVENT E,CLAIM C where cl.CLAIM_ID=C.CLAIM_ID and "
                        + "E.EVENT_ID=C.EVENT_ID and CL.CLAIMANT_ROW_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;

                    case "defendant":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from DEFENDANT D,EVENT E,CLAIM C where D.CLAIM_ID=C.CLAIM_ID and "
                       + "E.EVENT_ID=C.EVENT_ID and D.DEFENDANT_ROW_ID =" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }
                        break;

                    case "policy":
                        sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { PolicyNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };

                        }

                        break;
                    //skhare7:MITS 22743
                    case "litigation":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E where C.CLAIM_ID=CL.CLAIM_ID and C.EVENT_ID=E.EVENT_ID and CL.LITIGATION_ROW_ID ="
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "expert":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from EXPERT EP ,CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E  where C.CLAIM_ID=CL.CLAIM_ID and E.EVENT_ID=C.EVENT_ID"
                                 + "and CL.LITIGATION_ROW_ID=EP.LITIGATION_ROW_ID and EP.EXPERT_ROW_ID ="
                                 + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "adjuster dated text":
                        sSQL = "select  C.CLAIM_NUMBER,E.EVENT_NUMBER from ADJUST_DATED_TEXT AT,CLAIM_ADJUSTER CA,CLAIM C,EVENT E where "
                                + "AT.ADJ_ROW_ID=CA.ADJ_ROW_ID and C.CLAIM_ID=CA.CLAIM_ID and C.EVENT_ID=E.EVENT_ID"
                                 + "and AT.ADJ_DTTEXT_ROW_ID ="
                                    + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "plan":
                        sSQL = "select E.EVENT_NUMBER,c.CLAIM_NUMBER from CLAIM c , DISABILITY_PLAN DP,EVENT E "
                               + "where  c.CLAIM_ID=DP.PLAN_ID and E.EVENT_ID=c.EVENT_ID and "
                               + "DP.PLAN_ID=" + iPrimaryKey;

                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                            };

                        }

                        break;
                    //skhare7:MITS 22743 End
                    default:
                        return new RecordCollection();

                }

                return new RecordCollection();


            }
            catch (Exception p_oException)
            {    // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(p_oException.Message, p_oException.StackTrace);
                writeLog(string.Format("ERROR:[GetRecordValues] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
          

        }

        private static byte[] FileAsByteArray(string p_sFileName)
        {
            byte[] arrRet = null;
            FileStream objFStream = null;
            BinaryReader objBReader = null;
            try
            {
                //
                //while (IsFileUsedbyAnotherProcess(p_sFileName))
                //{
                    
                //}

                objFStream = new FileStream(p_sFileName, FileMode.Open,FileAccess.Read,FileShare.Read);
                objBReader = new BinaryReader(objFStream);
                arrRet = objBReader.ReadBytes((int)objFStream.Length);
            }
            catch (Exception p_objException)
            {
                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(p_objException.Message, p_objException.StackTrace);
                writeLog("ERROR retriving file :" + p_objException.Message);
            }
            finally
            {
                if (objFStream != null)
                {
                    objFStream.Close();
                    objFStream = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
            return arrRet;
        }

        public static bool IsFileUsedbyAnotherProcess(string filename)
        {
            try
            {
                File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (System.IO.IOException exp)
            {
                // atavaragiri mits 25686 //
                AppGlobals.htErrors.Add(exp.Message, exp.StackTrace);
                return true;
            }
            return false;

        }
        
    }
}
