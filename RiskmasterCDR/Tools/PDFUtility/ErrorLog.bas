Attribute VB_Name = "ErrorLog"
Option Explicit
Public Sub Dbg(s As String)
#If BB_DEBUG Then
    LogError "Debug", 0, 9999, "Debug", s
#End If
End Sub
Public Sub LogMsg(s As String)
    LogError "Debug", 0, 9999, "Debug", s
End Sub
Public Sub LogError(ByVal sProcedure As String, ByVal ErrLine As Long, ByVal ErrNum As Long, ByVal ErrSrc As String, ByVal ErrDesc As String)
On Error GoTo hError
Dim iFile As Integer
Dim sFileName As String

iFile = FreeFile
sFileName = App.Path
If Right$(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
sFileName = sFileName & "error.log"

Open sFileName For Append Shared As #iFile
Print #iFile, """" & Format$(Now, "yyyymmddhhnnss") & """," & """" & sProcedure & """," & """" & ErrLine & """," & """" & ErrNum & """," & """" & ErrSrc & """," & """" & ErrDesc & ""","
Close #iFile
iFile = 0

hExit:
   On Error Resume Next
   If iFile <> 0 Then Close iFile
   Exit Sub

hError:
   Resume hExit
End Sub

Public Sub WriteToFile(ByVal sPDF As String)
On Error GoTo hError
Dim iFile As Integer
Dim sFileName As String

iFile = FreeFile
sFileName = App.Path
If Right$(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
sFileName = sFileName & "error.log"

Open sFileName For Append Shared As #iFile
Print #iFile, sPDF
Close #iFile
iFile = 0

hExit:
   On Error Resume Next
   If iFile <> 0 Then Close iFile
   Exit Sub

hError:
   Resume hExit
End Sub


