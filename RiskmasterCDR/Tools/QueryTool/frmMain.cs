using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Riskmaster.Db;
using System.Data.SqlClient;

namespace Riskmaster.Tools.QueryTool
{
	/// <summary>
	/// Summary description for frmMain.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		internal string m_connectionString;
		
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSQL;
		private System.Windows.Forms.TextBox txtResults;
		private System.Windows.Forms.Button cmdExecute;
		private System.Windows.Forms.RadioButton optReader;
		private System.Windows.Forms.RadioButton optDataSet;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGrid dgrResults;
		private System.Windows.Forms.Button cmdCustom;
		private System.Windows.Forms.Button cmdTestWriterOTF;
		private System.Windows.Forms.Button cmdTestWriterByReader;
		private System.Windows.Forms.Button cmdStoreBlob;
		private System.Windows.Forms.Button cmdWriterWithBlob;
		private System.Windows.Forms.Button cmdPerfTest;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

		
		/// <summary>
		/// TODO: Summary of frmMain.
		/// </summary>
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			//Go Get Connection Info from User
			dlgConnect dlg = new dlgConnect();
			dlg.ShowDialog(this);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtSQL = new System.Windows.Forms.TextBox();
			this.txtResults = new System.Windows.Forms.TextBox();
			this.cmdExecute = new System.Windows.Forms.Button();
			this.optReader = new System.Windows.Forms.RadioButton();
			this.optDataSet = new System.Windows.Forms.RadioButton();
			this.label2 = new System.Windows.Forms.Label();
			this.dgrResults = new System.Windows.Forms.DataGrid();
			this.cmdCustom = new System.Windows.Forms.Button();
			this.cmdTestWriterOTF = new System.Windows.Forms.Button();
			this.cmdTestWriterByReader = new System.Windows.Forms.Button();
			this.cmdStoreBlob = new System.Windows.Forms.Button();
			this.cmdWriterWithBlob = new System.Windows.Forms.Button();
			this.cmdPerfTest = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgrResults)).BeginInit();
			this.SuspendLayout();
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(456, 496);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(80, 24);
			this.cmdCancel.TabIndex = 0;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(152, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Query";
			// 
			// txtSQL
			// 
			this.txtSQL.AcceptsReturn = true;
			this.txtSQL.AcceptsTab = true;
			this.txtSQL.Location = new System.Drawing.Point(208, 16);
			this.txtSQL.Multiline = true;
			this.txtSQL.Name = "txtSQL";
			this.txtSQL.Size = new System.Drawing.Size(440, 136);
			this.txtSQL.TabIndex = 2;
			this.txtSQL.Text = "SELECT * FROM USER_DETAILS_TABLE WHERE USER_ID=~VALUE~";
			// 
			// txtResults
			// 
			this.txtResults.AcceptsReturn = true;
			this.txtResults.AcceptsTab = true;
			this.txtResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.txtResults.BackColor = System.Drawing.Color.LemonChiffon;
			this.txtResults.Location = new System.Drawing.Point(216, 232);
			this.txtResults.Multiline = true;
			this.txtResults.Name = "txtResults";
			this.txtResults.ReadOnly = true;
			this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtResults.Size = new System.Drawing.Size(440, 256);
			this.txtResults.TabIndex = 3;
			this.txtResults.Text = "Query Results Area";
			this.txtResults.WordWrap = false;
			// 
			// cmdExecute
			// 
			this.cmdExecute.Location = new System.Drawing.Point(464, 160);
			this.cmdExecute.Name = "cmdExecute";
			this.cmdExecute.Size = new System.Drawing.Size(64, 24);
			this.cmdExecute.TabIndex = 4;
			this.cmdExecute.Text = "Execute";
			this.cmdExecute.Click += new System.EventHandler(this.cmdExecute_Click);
			// 
			// optReader
			// 
			this.optReader.Checked = true;
			this.optReader.Location = new System.Drawing.Point(328, 160);
			this.optReader.Name = "optReader";
			this.optReader.Size = new System.Drawing.Size(128, 24);
			this.optReader.TabIndex = 5;
			this.optReader.TabStop = true;
			this.optReader.Text = "Results via Reader";
			// 
			// optDataSet
			// 
			this.optDataSet.Location = new System.Drawing.Point(328, 184);
			this.optDataSet.Name = "optDataSet";
			this.optDataSet.Size = new System.Drawing.Size(128, 24);
			this.optDataSet.TabIndex = 6;
			this.optDataSet.Text = "Results via DataSet";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Location = new System.Drawing.Point(152, 240);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 24);
			this.label2.TabIndex = 7;
			this.label2.Text = "Results";
			// 
			// dgrResults
			// 
			this.dgrResults.DataMember = "";
			this.dgrResults.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgrResults.Location = new System.Drawing.Point(216, 232);
			this.dgrResults.Name = "dgrResults";
			this.dgrResults.Size = new System.Drawing.Size(440, 256);
			this.dgrResults.TabIndex = 8;
			// 
			// cmdCustom
			// 
			this.cmdCustom.Location = new System.Drawing.Point(8, 64);
			this.cmdCustom.Name = "cmdCustom";
			this.cmdCustom.Size = new System.Drawing.Size(128, 24);
			this.cmdCustom.TabIndex = 9;
			this.cmdCustom.Text = "Param Direct";
			this.cmdCustom.Click += new System.EventHandler(this.cmdCustom_Click);
			// 
			// cmdTestWriterOTF
			// 
			this.cmdTestWriterOTF.Location = new System.Drawing.Point(8, 128);
			this.cmdTestWriterOTF.Name = "cmdTestWriterOTF";
			this.cmdTestWriterOTF.Size = new System.Drawing.Size(128, 24);
			this.cmdTestWriterOTF.TabIndex = 10;
			this.cmdTestWriterOTF.Text = "Writer On the Fly";
			this.cmdTestWriterOTF.Click += new System.EventHandler(this.cmdTestWriterOTF_Click);
			// 
			// cmdTestWriterByReader
			// 
			this.cmdTestWriterByReader.Location = new System.Drawing.Point(8, 160);
			this.cmdTestWriterByReader.Name = "cmdTestWriterByReader";
			this.cmdTestWriterByReader.Size = new System.Drawing.Size(128, 24);
			this.cmdTestWriterByReader.TabIndex = 11;
			this.cmdTestWriterByReader.Text = "Writer By Reader";
			// 
			// cmdStoreBlob
			// 
			this.cmdStoreBlob.Location = new System.Drawing.Point(8, 96);
			this.cmdStoreBlob.Name = "cmdStoreBlob";
			this.cmdStoreBlob.Size = new System.Drawing.Size(128, 24);
			this.cmdStoreBlob.TabIndex = 12;
			this.cmdStoreBlob.Text = "Store Blob Direct";
			this.cmdStoreBlob.Click += new System.EventHandler(this.cmdStoreBlob_Click);
			// 
			// cmdWriterWithBlob
			// 
			this.cmdWriterWithBlob.Location = new System.Drawing.Point(8, 192);
			this.cmdWriterWithBlob.Name = "cmdWriterWithBlob";
			this.cmdWriterWithBlob.Size = new System.Drawing.Size(128, 24);
			this.cmdWriterWithBlob.TabIndex = 13;
			this.cmdWriterWithBlob.Text = "Writer with Blob";
			// 
			// cmdPerfTest
			// 
			this.cmdPerfTest.Location = new System.Drawing.Point(8, 248);
			this.cmdPerfTest.Name = "cmdPerfTest";
			this.cmdPerfTest.Size = new System.Drawing.Size(128, 23);
			this.cmdPerfTest.TabIndex = 14;
			this.cmdPerfTest.Text = "Performance Test";
			this.cmdPerfTest.Click += new System.EventHandler(this.cmdPerfTest_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 280);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(144, 23);
			this.button1.TabIndex = 15;
			this.button1.Text = "SQL Client Disconnect?";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(672, 533);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.cmdPerfTest);
			this.Controls.Add(this.cmdWriterWithBlob);
			this.Controls.Add(this.cmdStoreBlob);
			this.Controls.Add(this.cmdTestWriterByReader);
			this.Controls.Add(this.cmdTestWriterOTF);
			this.Controls.Add(this.cmdCustom);
			this.Controls.Add(this.dgrResults);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.optDataSet);
			this.Controls.Add(this.optReader);
			this.Controls.Add(this.cmdExecute);
			this.Controls.Add(this.txtResults);
			this.Controls.Add(this.txtSQL);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cmdCancel);
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.Text = "Riskmaster Query Tool";
			((System.ComponentModel.ISupportInitialize)(this.dgrResults)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		
		/// <summary>
		/// TODO: Summary of cmdCancel_Click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		
		/// <summary>
		/// TODO: Summary of cmdExecute_Click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdExecute_Click(object sender, System.EventArgs e)
		{ 
			try
			{
				if (optReader.Checked)
				{
					this.txtResults.BringToFront();
					DbReader rdr = DbFactory.GetDbReader(m_connectionString,this.txtSQL.Text);
					ShowReaderResult(rdr);
				}
				else
				{
					this.dgrResults.BringToFront();
					System.Data.DataSet da = DbFactory.GetDataSet(m_connectionString, this.txtSQL.Text);
					dgrResults.DataSource = da;
					dgrResults.DataMember = "TABLE";
					dgrResults.Refresh();
				}
			}
			catch(Exception ex)
			{
				this.txtResults.BringToFront();
				this.txtResults.Text = ex.Source + "\r\n\r\n Error Message: "  + ex.Message +  "\r\n\r\n Stack Trace: " + ex.StackTrace;
			}
		}
		
		/// <summary>
		/// TODO: Summary of InfoHandler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void InfoHandler(DbConnection sender, Db.DbInfoMessageArgs e){System.Windows.Forms.MessageBox.Show("Got Info" + e.Source +  e.Message);}
		
		/// <summary>
		/// TODO: Summary of cmdCustom_Click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdCustom_Click(object sender, System.EventArgs e)
		{
			try
			{
				DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString);
				DbConn.InfoMessage += new DbInfoMessageEventHandler(InfoHandler);
				DbConn.Open();
				//DbTransaction DbTrans = DbConn.BeginTransaction();
				
				DbCommand DbCmd  = DbConn.CreateCommand();
				//DbCmd.Transaction = DbTrans;

				DbCmd.CommandText = this.txtSQL.Text;
				DbParameter DbParam = DbCmd.CreateParameter();
				DbParam.ParameterName = "VALUE"; //TODO Param Names are case insensitive?
				DbParam.Value = 2;
				DbCmd.Parameters.Add(DbParam);
				DbReader DbRdr = DbCmd.ExecuteReader();
				ShowReaderResult(DbRdr);
				//DbTrans.Commit();
				DbConn.Close();
			}
			catch(Exception ex)
			{
				this.txtResults.BringToFront();
				this.txtResults.Text = ex.Source + "\r\n\r\n Error Message: "  + ex.Message +  "\r\n\r\n Stack Trace: " + ex.StackTrace;
			}
		}

		
		/// <summary>
		/// TODO: Summary of ShowReaderResult.
		/// </summary>
		/// <param name="rdr"></param>
		private void ShowReaderResult(DbReader rdr)
		{
			string s;

			this.txtResults.BringToFront();
			this.txtResults.Text = "";
			object[] arrValues = new object[rdr.FieldCount];
			while(rdr.Read()) 
			{
				rdr.GetValues(arrValues);
				s = "";
				foreach( object obj in arrValues)
					s = s + "|" + obj.ToString();
				txtResults.AppendText(s + "\r\n");
			}
		}

		
		/// <summary>
		/// TODO: Summary of cmdStoreBlob_Click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdStoreBlob_Click(object sender, System.EventArgs e)
		{
			
			//TODO Change this connection string as appropriate before re-attempting this test.
			this.m_connectionString = "Driver=SQL Server;Database=DocumentStorage;UID=sa;PWD=riskmaster;Server=localhost;";
			this.txtSQL.Text  = "UPDATE  DOCUMENT_STORAGE SET DOC_BLOB = ~FILEBLOB~ WHERE STORAGE_ID=~ID~";
			try
			{
				DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString);
				DbConn.Open();
				//DbTransaction DbTrans = DbConn.BeginTransaction();
				
				DbCommand DbCmd  = DbConn.CreateCommand();
				//DbCmd.Transaction = DbTrans;

				DbCmd.CommandText = this.txtSQL.Text;
				DbParameter DbParam = DbCmd.CreateParameter();
				DbParam.ParameterName = "FILEBLOB"; //TODO Param Names are case insensitive?
				DbParam.Value = FileAsByteArray(@"c:\home\docs\6.00.zip");
				DbCmd.Parameters.Add(DbParam);
				DbParam = DbCmd.CreateParameter();
				DbParam.ParameterName = "ID"; 
				DbParam.Value = 8;
				DbCmd.Parameters.Add(DbParam);
				txtResults.Text = "Execute Non-Reader returned: " + DbCmd.ExecuteNonQuery();
				
				
				DbCmd.CommandText = "SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE STORAGE_ID=~ID~";
				DbCmd.Parameters.Clear();
				DbCmd.Parameters.Add(DbParam);
				byte[] res = (byte[]) DbCmd.ExecuteScalar();
				ByteArrayToFile(@"c:\home\docs\test-6.00.zip", res);
				this.txtResults.BringToFront();

				//DbTrans.Commit();
				DbConn.Close();
			}
			catch(Exception ex)
			{
				this.txtResults.BringToFront();
				this.txtResults.Text = ex.Source + "\r\n\r\n Error Message: "  + ex.Message +  "\r\n\r\n Stack Trace: " + ex.StackTrace;
			}
	
		}

		
		/// <summary>
		/// TODO: Summary of cmdTestWriterOTF_Click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cmdTestWriterOTF_Click(object sender, System.EventArgs e)
		{
			
			try
			{
				//Use DocDB and get a 2 for 1 by testing blobs in the writer also.
				this.m_connectionString = "Driver=SQL Server;Database=DocumentStorage;UID=sa;PWD=riskmaster;Server=localhost;";
				DbWriter writer = DbFactory.GetDbWriter(this.m_connectionString);
				writer.Tables.Add("DOCUMENT_STORAGE");
				writer.Fields.Add("STORAGE_ID",8);
				writer.Fields.Add("FILENAME","QuickReplace.log3");
				writer.Fields.Add("DIR_PATH",@"\");
				writer.Fields.Add("STORE_TYPE",1);
				writer.Fields.Add("DOC_BLOB",FileAsByteArray(@"c:\home\docs\temp\QuickReplace.log"));
				writer.Where.Add("STORAGE_ID= 8");
				writer.Execute();

				DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString);
				DbConn.Open();
				MessageBox.Show(String.Format("Connection Type:{0} DbType{1}",DbConn.ConnectionType, DbConn.DatabaseType));
				DbCommand DbCmd = DbConn.CreateCommand();
				DbCmd.CommandText = "SELECT DOC_BLOB FROM DOCUMENT_STORAGE WHERE STORAGE_ID=~ID~";
				DbParameter p = DbCmd.CreateParameter();
				p.ParameterName= "ID";
				p.Value = 8;
				DbCmd.Parameters.Add(p);
				byte[] res = (byte[]) DbCmd.ExecuteScalar();
				ByteArrayToFile(@"c:\home\docs\test.txt",res);
				
				DbCmd.CommandText = "SELECT * FROM DOCUMENT_STORAGE WHERE STORAGE_ID=~ID~";
				DbReader reader = DbCmd.ExecuteReader();
				ShowReaderResult(reader);
				//reader.Close();
				DbCmd.Cancel();

			}
			catch(Exception ex)
			{
				this.txtResults.BringToFront();
				this.txtResults.Text = ex.Source + "\r\n\r\n Error Message: "  + ex.Message +  "\r\n\r\n Stack Trace: " + ex.StackTrace;
			}

		}
		
		/// <summary>
		/// TODO: Summary of FileAsByteArray.
		/// </summary>
		/// <param name="sFileName"></param>
		private byte[] FileAsByteArray(string sFileName)
		{
			byte[] ret  = null;
			System.IO.FileStream fstream=null;
			System.IO.BinaryReader breader=null;
			try
			{
				fstream = new System.IO.FileStream(sFileName,System.IO.FileMode.Open);
				breader = 	new System.IO.BinaryReader(fstream);
				ret = breader.ReadBytes((int)fstream.Length);
			}
			finally
			{
				if( fstream != null) fstream.Close();
				if( breader != null) breader.Close();
			}
			return ret;
		}		
		
		/// <summary>
		/// TODO: Summary of ByteArrayToFile.
		/// </summary>
		/// <param name="sFileName"></param>
		/// <param name="arr"></param>
		private void ByteArrayToFile(string sFileName, byte[] arr)
		{
			System.IO.FileStream fstream=null;
			try
			{
					
				fstream = System.IO.File.OpenWrite(sFileName);
				fstream.Write(arr,0,arr.Length);
			}
			finally
			{
				if( fstream != null) fstream.Close();
			}
			return;
		}

		private void cmdPerfTest_Click(object sender, System.EventArgs e)
		{
			string SQL = "SELECT DTTM_RCD_ADDED,BRIEF_DESC,ACCOUNT_ID,TIME_REPORTED,INJURY_TO_DATE,RPTD_BY_EID,COUNTY_OF_INJURY,PHYS_NOTES,TIME_PHYS_ADVISED,ADDR1,TREATMENT_GIVEN,LOCATION_AREA_DESC,DTTM_RCD_LAST_UPD,ZIP_CODE,UPDATED_BY_USER,TIME_OF_EVENT,DATE_OF_EVENT,DATE_TO_FOLLOW_UP,DATE_RPTD_TO_RM,DEPT_HEAD_ADVISED,EVENT_IND_CODE,COMMENTS,RELEASE_SIGNED,LOCATION_TYPE_CODE,EVENT_DESCRIPTION,ADDR2,INJURY_FROM_DATE,DATE_REPORTED,ON_PREMISE_FLAG,COUNTRY_CODE,ADDED_BY_USER,NO_OF_FATALITIES,EVENT_TYPE_CODE,CITY,EVENT_NUMBER,DATE_CARRIER_NOTIF,EVENT_STATUS_CODE,DEPT_EID,PRIMARY_LOC_CODE,STATE_ID,NO_OF_INJURIES,EVENT_ID,DEPT_INVOLVED_EID,DATE_PHYS_ADVISED,CAUSE_CODE FROM EVENT WHERE EVENT_ID=1";
			string sODBC = "DSN=Wizard_ODBC;UID=sa;PWD=riskmaster;";
			string sNative = "Driver={SQL Server};Server=SODBNT01;Database=Wizard_BSB;UID=sa;PWD=riskmaster;";
			DbConnection DbConn = DbFactory.GetDbConnection(sNative);
			//			DbConnection DbConn = DbFactory.GetDbConnection(sODBC);
			DbConn.Open();
			
			DbReader objReader =null;
			
			for(int i=0;i <50;i++)
			{
				objReader =  DbConn.ExecuteReader(SQL);
				objReader.Read();
				objReader.Close();
			}
			MessageBox.Show("Done with Perf Test");
		}

		//Test Function
		private void TestSqlClientDisconnect()
		{
			string SQL = "SELECT COMMENTS ,EVENT_DESCRIPTION FROM EVENT WHERE EVENT_ID =1";
			SqlConnection DbConn = new  SqlConnection("Server=SODBNT01;Database=Wizard_BSB;UID=sa;PWD=riskmaster;");
			DbConn.Open();
			
			SqlDataReader objReader =null;
			SqlCommand objCmd = null;
			for(int i=0;i <50;i++)
		{
			objCmd = DbConn.CreateCommand();
			objCmd.CommandText = SQL;
			objReader = objCmd.ExecuteReader();
			objReader.Read();
			objCmd.Cancel(); //This must confuse sql state somewhere...
			objCmd.Dispose();
			objCmd = null;
			//Successfull w/o the Cancel call.
			// But with the Cancel call, this line causes DbConn to slip from "open" to "closed" on the second iteration.
			objReader.Close();
		}
		MessageBox.Show("Done with Disconnect Test");
}
		//Test Disconnect problem with native SQL ADO.Net 
		private void button1_Click(object sender, System.EventArgs e)
		{
			TestSqlClientDisconnect();
			return;

//			string SQL = "SELECT DTTM_RCD_ADDED,BRIEF_DESC,ACCOUNT_ID,TIME_REPORTED,INJURY_TO_DATE,RPTD_BY_EID,COUNTY_OF_INJURY,PHYS_NOTES,TIME_PHYS_ADVISED,ADDR1,TREATMENT_GIVEN,LOCATION_AREA_DESC,DTTM_RCD_LAST_UPD,ZIP_CODE,UPDATED_BY_USER,TIME_OF_EVENT,DATE_OF_EVENT,DATE_TO_FOLLOW_UP,DATE_RPTD_TO_RM,DEPT_HEAD_ADVISED,EVENT_IND_CODE,COMMENTS,RELEASE_SIGNED,LOCATION_TYPE_CODE,EVENT_DESCRIPTION,ADDR2,INJURY_FROM_DATE,DATE_REPORTED,ON_PREMISE_FLAG,COUNTRY_CODE,ADDED_BY_USER,NO_OF_FATALITIES,EVENT_TYPE_CODE,CITY,EVENT_NUMBER,DATE_CARRIER_NOTIF,EVENT_STATUS_CODE,DEPT_EID,PRIMARY_LOC_CODE,STATE_ID,NO_OF_INJURIES,EVENT_ID,DEPT_INVOLVED_EID,DATE_PHYS_ADVISED,CAUSE_CODE FROM EVENT WHERE EVENT_ID IN (1,2)";
			string SQL = "SELECT COMMENTS ,EVENT_DESCRIPTION FROM EVENT WHERE EVENT_ID =1";
/*			string SQLStar = @"SELECT 
													DTTM_RCD_ADDED,
													BRIEF_DESC,
													ACCOUNT_ID,
													TIME_REPORTED,
													INJURY_TO_DATE,
													RPTD_BY_EID,
													COUNTY_OF_INJURY,
													PHYS_NOTES,
													TIME_PHYS_ADVISED,
													ADDR1,
													TREATMENT_GIVEN,
													LOCATION_AREA_DESC,
													DTTM_RCD_LAST_UPD,
													ZIP_CODE,
													UPDATED_BY_USER,
													TIME_OF_EVENT,
													DATE_OF_EVENT,
													DATE_TO_FOLLOW_UP,
													DATE_RPTD_TO_RM,
													DEPT_HEAD_ADVISED,
													EVENT_IND_CODE,
													COMMENTS,
													EVENT_DESCRIPTION,
													 RELEASE_SIGNED,
													LOCATION_TYPE_CODE,
													ADDR2,
													INJURY_FROM_DATE,
													DATE_REPORTED,
													ON_PREMISE_FLAG,
													COUNTRY_CODE,
													ADDED_BY_USER,
													NO_OF_FATALITIES,
													EVENT_TYPE_CODE,
													CITY,
													EVENT_NUMBER,
													DATE_CARRIER_NOTIF,
													EVENT_STATUS_CODE,
													DEPT_EID,
													PRIMARY_LOC_CODE,
													STATE_ID,
													NO_OF_INJURIES,
													EVENT_ID,
													DEPT_INVOLVED_EID,
													DATE_PHYS_ADVISED,
													CAUSE_CODE" 
	+ " FROM EVENT WHERE EVENT_ID = 1";*/
			string sODBC = "DSN=Wizard_ODBC;UID=sa;PWD=riskmaster;";
			//			DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString);
			SqlConnection DbConn = new  SqlConnection(this.m_connectionString);
			DbConn.Open();
			
			SqlDataReader objReader =null;
			SqlCommand objCmd = null;
			for(int i=0;i <50;i++)
			{
				objCmd = DbConn.CreateCommand();
				objCmd.CommandText = SQL;
				objReader = objCmd.ExecuteReader();
				objReader.Read();
				objCmd.Cancel();
				objCmd.Dispose();
				objCmd = null;
//Successfull w/o following Cancel call.
				objReader.Close();
			}
			MessageBox.Show("Done with Disconnect Test");
	
		}

	}
}
