﻿namespace RMXdBUpgradeWizard
{
    partial class frmWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWizard));
            this.wizard1 = new DevComponents.DotNetBar.Wizard();
            this.wpDisclaimer = new DevComponents.DotNetBar.WizardPage();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblDisclaimer = new DevComponents.DotNetBar.LabelX();
            this.wpBuildConnect = new DevComponents.DotNetBar.WizardPage();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.lblSecDB = new DevComponents.DotNetBar.LabelX();
            this.btnSaveConfig = new DevComponents.DotNetBar.ButtonX();
            this.tbSecDB = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnSecConn = new DevComponents.DotNetBar.ButtonX();
            this.lblSecPWD = new DevComponents.DotNetBar.LabelX();
            this.tbSecPWD = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.lblSecUID = new DevComponents.DotNetBar.LabelX();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.tbSecUID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblSecDSNorDriver = new DevComponents.DotNetBar.LabelX();
            this.lblSecServer = new DevComponents.DotNetBar.LabelX();
            this.tbSecDSN = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.tbSecServer = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.wpDataSources = new DevComponents.DotNetBar.WizardPage();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblWarningWPDS = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lvDatabases = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.dsn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.pbOverall = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.pbCurrentFile = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.tbExecuting = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lblProcessingDB = new DevComponents.DotNetBar.LabelX();
            this.lblProcessingScript = new DevComponents.DotNetBar.LabelX();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.wpErrorPage = new DevComponents.DotNetBar.WizardPage();
            this.gbError = new System.Windows.Forms.GroupBox();
            this.lblError = new DevComponents.DotNetBar.LabelX();
            this.gbTechDesc = new System.Windows.Forms.GroupBox();
            this.lblException = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.wpFinish = new DevComponents.DotNetBar.WizardPage();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.lblDBConnResults = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lvFailed = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvPassed = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pbConnectionTest = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.lblTestingDB = new DevComponents.DotNetBar.LabelX();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.LblDsnListHeader = new System.Windows.Forms.Label();
            this.LblDsnList = new System.Windows.Forms.Label();
            this.wizard1.SuspendLayout();
            this.wpDisclaimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.wpBuildConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.wpDataSources.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.wpErrorPage.SuspendLayout();
            this.gbError.SuspendLayout();
            this.gbTechDesc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.wpFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // wizard1
            // 
            this.wizard1.BackColor = System.Drawing.Color.White;
            this.wizard1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wizard1.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.wizard1.Cursor = System.Windows.Forms.Cursors.Default;
            this.wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizard1.FinishButtonTabIndex = 3;
            // 
            // 
            // 
            this.wizard1.FooterStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.FooterStyle.BorderBottomWidth = 1;
            this.wizard1.FooterStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.FooterStyle.BorderLeftWidth = 1;
            this.wizard1.FooterStyle.BorderRightWidth = 1;
            this.wizard1.FooterStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.wizard1.FooterStyle.BorderTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.wizard1.FooterStyle.BorderTopWidth = 1;
            this.wizard1.FooterStyle.Class = "";
            this.wizard1.FooterStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wizard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.wizard1.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.wizard1.HeaderDescriptionVisible = false;
            this.wizard1.HeaderHeight = 90;
            this.wizard1.HeaderImage = global::RMXdBUpgradeWizard.Properties.Resources.WizardBanner2;
            this.wizard1.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.wizard1.HeaderImageSize = new System.Drawing.Size(688, 52);
            // 
            // 
            // 
            this.wizard1.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.HeaderStyle.BackColorGradientAngle = 90;
            this.wizard1.HeaderStyle.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.Center;
            this.wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.wizard1.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.wizard1.HeaderStyle.BorderBottomWidth = 1;
            this.wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.HeaderStyle.BorderLeftWidth = 1;
            this.wizard1.HeaderStyle.BorderRightWidth = 1;
            this.wizard1.HeaderStyle.BorderTopWidth = 1;
            this.wizard1.HeaderStyle.Class = "";
            this.wizard1.HeaderStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.HeaderTitleIndent = 702;
            this.wizard1.HelpButtonVisible = false;
            this.wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.wizard1.Location = new System.Drawing.Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Size = new System.Drawing.Size(700, 550);
            this.wizard1.TabIndex = 0;
            this.wizard1.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.wpDisclaimer,
            this.wpBuildConnect,
            this.wpDataSources,
            this.wpErrorPage,
            this.wpFinish});
            this.wizard1.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_FinishButtonClick);
            this.wizard1.WizardPageChanging += new DevComponents.DotNetBar.WizardCancelPageChangeEventHandler(this.wizard1_WizardPageChanging);
            this.wizard1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.wizard1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.wizard1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            // 
            // wpDisclaimer
            // 
            this.wpDisclaimer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpDisclaimer.BackButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpDisclaimer.BackColor = System.Drawing.Color.White;
            this.wpDisclaimer.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpDisclaimer.Controls.Add(this.labelX8);
            this.wpDisclaimer.Controls.Add(this.pictureBox2);
            this.wpDisclaimer.Controls.Add(this.lblDisclaimer);
            this.wpDisclaimer.FinishButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpDisclaimer.Location = new System.Drawing.Point(7, 102);
            this.wpDisclaimer.Name = "wpDisclaimer";
            this.wpDisclaimer.PageDescription = "Warn user of the importance of backing up thier data before proceeding.";
            this.wpDisclaimer.PageTitle = "Disclaimer";
            this.wpDisclaimer.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpDisclaimer.Style.Class = "";
            this.wpDisclaimer.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDisclaimer.StyleMouseDown.Class = "";
            this.wpDisclaimer.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDisclaimer.StyleMouseOver.Class = "";
            this.wpDisclaimer.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpDisclaimer.TabIndex = 7;
            this.wpDisclaimer.Text = "Disclaimer";
            this.wpDisclaimer.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wpDisclaimer_CancelButtonClick);
            this.wpDisclaimer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.wpDisclaimer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.wpDisclaimer.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.Class = "";
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.Silver;
            this.labelX8.Location = new System.Drawing.Point(492, 0);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(99, 23);
            this.labelX8.TabIndex = 2;
            this.labelX8.Text = "Confirmation";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::RMXdBUpgradeWizard.Properties.Resources.Database;
            this.pictureBox2.Location = new System.Drawing.Point(528, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // lblDisclaimer
            // 
            // 
            // 
            // 
            this.lblDisclaimer.BackgroundStyle.Class = "";
            this.lblDisclaimer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDisclaimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisclaimer.Location = new System.Drawing.Point(90, 114);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.Size = new System.Drawing.Size(502, 159);
            this.lblDisclaimer.TabIndex = 0;
            this.lblDisclaimer.Text = resources.GetString("lblDisclaimer.Text");            
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(20)))), ((int)(((byte)(25)))));
            this.lblStatus.Location = new System.Drawing.Point(90, 307);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(502, 76);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblStatus.WordWrap = true;
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.Class = "";
            this.labelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.Location = new System.Drawing.Point(90, 278);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(393, 23);
            this.labelX16.TabIndex = 13;
            this.labelX16.Text = "Connection Status";
            // 
            // labelX15
            // 
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.Class = "";
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX15.Location = new System.Drawing.Point(90, 19);
            this.labelX15.Name = "labelX15";
            this.labelX15.Size = new System.Drawing.Size(393, 23);
            this.labelX15.TabIndex = 12;
            this.labelX15.Text = "Security Connection";
            // 
            // lblSecDB
            // 
            // 
            // 
            // 
            this.lblSecDB.BackgroundStyle.Class = "";
            this.lblSecDB.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecDB.Location = new System.Drawing.Point(85, 111);
            this.lblSecDB.Name = "lblSecDB";
            this.lblSecDB.Size = new System.Drawing.Size(112, 23);
            this.lblSecDB.TabIndex = 11;
            this.lblSecDB.Text = "Configuration File:";
            this.lblSecDB.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // tbSecDB
            // 
            this.tbSecDB.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tbSecDB.Border.Class = "TextBoxBorder";
            this.tbSecDB.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbSecDB.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSecDB.Location = new System.Drawing.Point(203, 114);
            this.tbSecDB.Name = "tbSecDB";
            this.tbSecDB.ReadOnly = true;
            this.tbSecDB.Size = new System.Drawing.Size(280, 20);
            this.tbSecDB.TabIndex = 10;
            // 
            // btnSecConn
            // 
            this.btnSecConn.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSecConn.Location = new System.Drawing.Point(0, 0);
            this.btnSecConn.Name = "btnSecConn";
            this.btnSecConn.TabIndex = 15;
            // 
            // lblSecPWD
            // 
            // 
            // 
            // 
            this.lblSecPWD.BackgroundStyle.Class = "";
            this.lblSecPWD.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecPWD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecPWD.Location = new System.Drawing.Point(85, 161);
            this.lblSecPWD.Name = "lblSecPWD";
            this.lblSecPWD.Size = new System.Drawing.Size(112, 23);
            this.lblSecPWD.TabIndex = 9;
            this.lblSecPWD.Text = "PWD:";
            this.lblSecPWD.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // tbSecPWD
            // 
            this.tbSecPWD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tbSecPWD.Border.Class = "TextBoxBorder";
            this.tbSecPWD.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbSecPWD.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSecPWD.Location = new System.Drawing.Point(203, 164);
            this.tbSecPWD.Name = "tbSecPWD";
            this.tbSecPWD.PasswordChar = '●';
            this.tbSecPWD.ReadOnly = true;
            this.tbSecPWD.Size = new System.Drawing.Size(280, 20);
            this.tbSecPWD.TabIndex = 8;
            this.tbSecPWD.UseSystemPasswordChar = true;
            // 
            // labelX14
            // 
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.Class = "";
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX14.ForeColor = System.Drawing.Color.Silver;
            this.labelX14.Location = new System.Drawing.Point(438, 0);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(154, 23);
            this.labelX14.TabIndex = 7;
            this.labelX14.Text = "Connection Setup";
            this.labelX14.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblSecUID
            // 
            // 
            // 
            // 
            this.lblSecUID.BackgroundStyle.Class = "";
            this.lblSecUID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecUID.Location = new System.Drawing.Point(85, 136);
            this.lblSecUID.Name = "lblSecUID";
            this.lblSecUID.Size = new System.Drawing.Size(112, 23);
            this.lblSecUID.TabIndex = 7;
            this.lblSecUID.Text = "UID:";
            this.lblSecUID.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 50);
            this.pictureBox7.TabIndex = 16;
            this.pictureBox7.TabStop = false;
            // 
            // tbSecUID
            // 
            this.tbSecUID.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tbSecUID.Border.Class = "TextBoxBorder";
            this.tbSecUID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbSecUID.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSecUID.Location = new System.Drawing.Point(203, 139);
            this.tbSecUID.Name = "tbSecUID";
            this.tbSecUID.ReadOnly = true;
            this.tbSecUID.Size = new System.Drawing.Size(280, 20);
            this.tbSecUID.TabIndex = 6;
            // 
            // lblSecDSNorDriver
            // 
            // 
            // 
            // 
            this.lblSecDSNorDriver.BackgroundStyle.Class = "";
            this.lblSecDSNorDriver.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecDSNorDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecDSNorDriver.Location = new System.Drawing.Point(85, 61);
            this.lblSecDSNorDriver.Name = "lblSecDSNorDriver";
            this.lblSecDSNorDriver.Size = new System.Drawing.Size(112, 23);
            this.lblSecDSNorDriver.TabIndex = 3;
            this.lblSecDSNorDriver.Text = "DSN:";
            this.lblSecDSNorDriver.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblSecServer
            // 
            // 
            // 
            // 
            this.lblSecServer.BackgroundStyle.Class = "";
            this.lblSecServer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSecServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecServer.Location = new System.Drawing.Point(85, 86);
            this.lblSecServer.Name = "lblSecServer";
            this.lblSecServer.Size = new System.Drawing.Size(112, 23);
            this.lblSecServer.TabIndex = 5;
            this.lblSecServer.Text = "Server:";
            this.lblSecServer.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // tbSecDSN
            // 
            this.tbSecDSN.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tbSecDSN.Border.Class = "TextBoxBorder";
            this.tbSecDSN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbSecDSN.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSecDSN.Location = new System.Drawing.Point(203, 64);
            this.tbSecDSN.Name = "tbSecDSN";
            this.tbSecDSN.ReadOnly = true;
            this.tbSecDSN.Size = new System.Drawing.Size(280, 20);
            this.tbSecDSN.TabIndex = 2;
            // 
            // tbSecServer
            // 
            this.tbSecServer.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.tbSecServer.Border.Class = "TextBoxBorder";
            this.tbSecServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbSecServer.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSecServer.Location = new System.Drawing.Point(203, 89);
            this.tbSecServer.Name = "tbSecServer";
            this.tbSecServer.ReadOnly = true;
            this.tbSecServer.Size = new System.Drawing.Size(280, 20);
            this.tbSecServer.TabIndex = 4;
            // 
            // wpDataSources
            // 
            this.wpDataSources.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpDataSources.BackColor = System.Drawing.Color.White;
            this.wpDataSources.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpDataSources.Controls.Add(this.labelX9);
            this.wpDataSources.Controls.Add(this.pictureBox3);
            this.wpDataSources.Controls.Add(this.lblWarningWPDS);
            this.wpDataSources.Controls.Add(this.labelX1);
            this.wpDataSources.Controls.Add(this.labelX2);
            this.wpDataSources.Controls.Add(this.lvDatabases);
            this.wpDataSources.Location = new System.Drawing.Point(7, 102);
            this.wpDataSources.Name = "wpDataSources";
            this.wpDataSources.PageDescription = "Allow user to select which datasource(s) to upgrade";
            this.wpDataSources.PageTitle = "Configration File Selection";
            this.wpDataSources.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpDataSources.Style.Class = "";
            this.wpDataSources.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDataSources.StyleMouseDown.Class = "";
            this.wpDataSources.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpDataSources.StyleMouseOver.Class = "";
            this.wpDataSources.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpDataSources.TabIndex = 8;
            this.wpDataSources.Text = "Data Sources";
            this.wpDataSources.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wpDataSources_CancelButtonClick);
            this.wpDataSources.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.wpDataSources.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.wpDataSources.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.Class = "";
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Silver;
            this.labelX9.Location = new System.Drawing.Point(439, 0);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(154, 23);
            this.labelX9.TabIndex = 6;
            this.labelX9.Text = "Configration File Selection";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::RMXdBUpgradeWizard.Properties.Resources.DatabaseSearch;
            this.pictureBox3.Location = new System.Drawing.Point(528, 29);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(64, 64);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // lblWarningWPDS
            // 
            // 
            // 
            // 
            this.lblWarningWPDS.BackgroundStyle.Class = "";
            this.lblWarningWPDS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblWarningWPDS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPDS.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPDS.Name = "lblWarningWPDS";
            this.lblWarningWPDS.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPDS.TabIndex = 4;
            this.lblWarningWPDS.Text = "<font color=\"#BA1419\">You must select a Configuration File to continue.....</font" +
                ">";
            this.lblWarningWPDS.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPDS.Visible = false;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(90, 338);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(531, 23);
            this.labelX1.TabIndex = 3;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(90, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(393, 23);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "Select Configration File(s) to <b>UPGRADE</b>";
            // 
            // lvDatabases
            // 
            // 
            // 
            // 
            this.lvDatabases.Border.BorderColor = System.Drawing.Color.LightGray;
            this.lvDatabases.Border.Class = "ListViewBorder";
            this.lvDatabases.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lvDatabases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dsn,
            this.id});
            this.lvDatabases.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvDatabases.ForeColor = System.Drawing.Color.Sienna;
            this.lvDatabases.FullRowSelect = true;
            this.lvDatabases.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvDatabases.Location = new System.Drawing.Point(90, 48);
            this.lvDatabases.Name = "lvDatabases";
            this.lvDatabases.Size = new System.Drawing.Size(405, 284);
            this.lvDatabases.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDatabases.TabIndex = 0;
            this.lvDatabases.UseCompatibleStateImageBehavior = false;
            this.lvDatabases.View = System.Windows.Forms.View.Details;
            this.lvDatabases.Click += new System.EventHandler(this.lvDatabases_Click);
            // 
            // dsn
            // 
            this.dsn.Width = 400;
            // 
            // id
            // 
            this.id.Width = 0;            
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new System.Drawing.Point(436, -9);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(50, 50);
            this.pictureBox8.TabIndex = 13;
            this.pictureBox8.TabStop = false;
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.Class = "";
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX11.ForeColor = System.Drawing.Color.Silver;
            this.labelX11.Location = new System.Drawing.Point(492, 0);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(99, 23);
            this.labelX11.TabIndex = 12;
            this.labelX11.Text = "Processing";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX7
            // 
            this.labelX7.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.Class = "";
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX7.Location = new System.Drawing.Point(90, 330);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(405, 23);
            this.labelX7.TabIndex = 11;
            this.labelX7.Text = "Progress: <font color=\"#84A2C6\">Overall</font>";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.Class = "";
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(90, 269);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(405, 23);
            this.labelX6.TabIndex = 10;
            this.labelX6.Text = "Progress: <font color=\"#84A2C6\">Current File</font>";
            // 
            // pbOverall
            // 
            this.pbOverall.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.pbOverall.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.pbOverall.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbOverall.BackgroundStyle.BorderBottomWidth = 1;
            this.pbOverall.BackgroundStyle.BorderColor = System.Drawing.Color.LightGray;
            this.pbOverall.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbOverall.BackgroundStyle.BorderLeftWidth = 1;
            this.pbOverall.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbOverall.BackgroundStyle.BorderRightWidth = 1;
            this.pbOverall.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbOverall.BackgroundStyle.BorderTopWidth = 1;
            this.pbOverall.BackgroundStyle.Class = "";
            this.pbOverall.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pbOverall.ChunkColor = System.Drawing.Color.CornflowerBlue;
            this.pbOverall.ChunkColor2 = System.Drawing.Color.White;
            this.pbOverall.ChunkGradientAngle = 90;
            this.pbOverall.Location = new System.Drawing.Point(90, 355);
            this.pbOverall.Name = "pbOverall";
            this.pbOverall.Size = new System.Drawing.Size(508, 28);
            this.pbOverall.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2000;
            this.pbOverall.TabIndex = 9;
            // 
            // pbCurrentFile
            // 
            this.pbCurrentFile.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.pbCurrentFile.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.pbCurrentFile.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbCurrentFile.BackgroundStyle.BorderBottomWidth = 1;
            this.pbCurrentFile.BackgroundStyle.BorderColor = System.Drawing.Color.LightGray;
            this.pbCurrentFile.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbCurrentFile.BackgroundStyle.BorderLeftWidth = 1;
            this.pbCurrentFile.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbCurrentFile.BackgroundStyle.BorderRightWidth = 1;
            this.pbCurrentFile.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbCurrentFile.BackgroundStyle.BorderTopWidth = 1;
            this.pbCurrentFile.BackgroundStyle.Class = "";
            this.pbCurrentFile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pbCurrentFile.ChunkColor = System.Drawing.Color.Peru;
            this.pbCurrentFile.ChunkColor2 = System.Drawing.Color.White;
            this.pbCurrentFile.ChunkGradientAngle = 90;
            this.pbCurrentFile.Location = new System.Drawing.Point(90, 296);
            this.pbCurrentFile.Name = "pbCurrentFile";
            this.pbCurrentFile.Size = new System.Drawing.Size(508, 28);
            this.pbCurrentFile.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2000;
            this.pbCurrentFile.TabIndex = 8;
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.Class = "";
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(90, 77);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(405, 23);
            this.labelX5.TabIndex = 7;
            this.labelX5.Text = "Executing:<font color=\"#A0522D\"></font>";
            // 
            // tbExecuting
            // 
            // 
            // 
            // 
            this.tbExecuting.Border.BorderColor = System.Drawing.Color.LightGray;
            this.tbExecuting.Border.Class = "TextBoxBorder";
            this.tbExecuting.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbExecuting.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbExecuting.ForeColor = System.Drawing.Color.Sienna;
            this.tbExecuting.Location = new System.Drawing.Point(90, 106);
            this.tbExecuting.Multiline = true;
            this.tbExecuting.Name = "tbExecuting";
            this.tbExecuting.Size = new System.Drawing.Size(508, 157);
            this.tbExecuting.TabIndex = 6;
            // 
            // lblProcessingDB
            // 
            this.lblProcessingDB.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lblProcessingDB.BackgroundStyle.Class = "";
            this.lblProcessingDB.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProcessingDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessingDB.Location = new System.Drawing.Point(90, -9);
            this.lblProcessingDB.Name = "lblProcessingDB";
            this.lblProcessingDB.Size = new System.Drawing.Size(326, 51);
            this.lblProcessingDB.TabIndex = 5;
            this.lblProcessingDB.Text = "Processing File: ";
            // 
            // lblProcessingScript
            // 
            this.lblProcessingScript.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lblProcessingScript.BackgroundStyle.Class = "";
            this.lblProcessingScript.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblProcessingScript.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessingScript.Location = new System.Drawing.Point(90, 48);
            this.lblProcessingScript.Name = "lblProcessingScript";
            this.lblProcessingScript.Size = new System.Drawing.Size(405, 23);
            this.lblProcessingScript.TabIndex = 4;
            this.lblProcessingScript.Text = "Processing script: ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::RMXdBUpgradeWizard.Properties.Resources.DatabaseEdit;
            this.pictureBox5.Location = new System.Drawing.Point(528, 29);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 64);
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // wpErrorPage
            // 
            this.wpErrorPage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpErrorPage.BackColor = System.Drawing.Color.White;
            this.wpErrorPage.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpErrorPage.Controls.Add(this.gbError);
            this.wpErrorPage.Controls.Add(this.gbTechDesc);
            this.wpErrorPage.Controls.Add(this.pictureBox1);
            this.wpErrorPage.FinishButtonEnabled = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpErrorPage.Location = new System.Drawing.Point(7, 102);
            this.wpErrorPage.Name = "wpErrorPage";
            this.wpErrorPage.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpErrorPage.Style.Class = "";
            this.wpErrorPage.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpErrorPage.StyleMouseDown.Class = "";
            this.wpErrorPage.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpErrorPage.StyleMouseOver.Class = "";
            this.wpErrorPage.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpErrorPage.TabIndex = 9;
            this.wpErrorPage.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wpErrorPage_CancelButtonClick);
            this.wpErrorPage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.wpErrorPage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.wpErrorPage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            // 
            // gbError
            // 
            this.gbError.Controls.Add(this.lblError);
            this.gbError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbError.ForeColor = System.Drawing.Color.Red;
            this.gbError.Location = new System.Drawing.Point(115, 30);
            this.gbError.Name = "gbError";
            this.gbError.Size = new System.Drawing.Size(512, 201);
            this.gbError.TabIndex = 4;
            this.gbError.TabStop = false;
            this.gbError.Text = "Error";
            // 
            // lblError
            // 
            // 
            // 
            // 
            this.lblError.BackgroundStyle.Class = "";
            this.lblError.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblError.Location = new System.Drawing.Point(6, 25);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(500, 170);
            this.lblError.TabIndex = 0;
            this.lblError.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblError.WordWrap = true;
            // 
            // gbTechDesc
            // 
            this.gbTechDesc.Controls.Add(this.lblException);
            this.gbTechDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTechDesc.Location = new System.Drawing.Point(115, 237);
            this.gbTechDesc.Name = "gbTechDesc";
            this.gbTechDesc.Size = new System.Drawing.Size(512, 149);
            this.gbTechDesc.TabIndex = 3;
            this.gbTechDesc.TabStop = false;
            this.gbTechDesc.Text = "Technical Description";
            // 
            // lblException
            // 
            // 
            // 
            // 
            this.lblException.BackgroundStyle.Class = "";
            this.lblException.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblException.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblException.ForeColor = System.Drawing.Color.Gray;
            this.lblException.Location = new System.Drawing.Point(6, 25);
            this.lblException.Name = "lblException";
            this.lblException.Size = new System.Drawing.Size(500, 118);
            this.lblException.TabIndex = 2;
            this.lblException.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblException.WordWrap = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(61, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // wpFinish
            // 
            this.wpFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpFinish.BackColor = System.Drawing.Color.White;
            this.wpFinish.CancelButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpFinish.Controls.Add(this.labelX12);
            this.wpFinish.Controls.Add(this.pictureBox6);
            this.wpFinish.Controls.Add(this.labelX13);
            this.wpFinish.Location = new System.Drawing.Point(7, 102);
            this.wpFinish.Name = "wpFinish";
            this.wpFinish.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpFinish.Style.Class = "";
            this.wpFinish.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpFinish.StyleMouseDown.Class = "";
            this.wpFinish.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.wpFinish.StyleMouseOver.Class = "";
            this.wpFinish.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.wpFinish.TabIndex = 10;
            this.wpFinish.Text = "wpFinish";
            this.wpFinish.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.wpFinish.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.wpFinish.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            // 
            // labelX12
            // 
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.Class = "";
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX12.ForeColor = System.Drawing.Color.Silver;
            this.labelX12.Location = new System.Drawing.Point(492, 0);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(99, 23);
            this.labelX12.TabIndex = 5;
            this.labelX12.Text = "Finish";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::RMXdBUpgradeWizard.Properties.Resources.Database;
            this.pictureBox6.Location = new System.Drawing.Point(528, 29);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(64, 64);
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            // 
            // labelX13
            // 
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.Class = "";
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX13.Location = new System.Drawing.Point(90, 114);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(502, 159);
            this.labelX13.TabIndex = 3;
            this.labelX13.Text = "The <b>Configuration Section Data Encryption</b> has completed successfully.<br /><br /><b>Thank" +
                " You!</b>";
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.Class = "";
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX10.ForeColor = System.Drawing.Color.Silver;
            this.labelX10.Location = new System.Drawing.Point(492, 0);
            this.labelX10.Name = "labelX10";
            this.labelX10.Size = new System.Drawing.Size(99, 23);
            this.labelX10.TabIndex = 10;
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // lblDBConnResults
            // 
            // 
            // 
            // 
            this.lblDBConnResults.BackgroundStyle.Class = "";
            this.lblDBConnResults.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblDBConnResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDBConnResults.Location = new System.Drawing.Point(90, 323);
            this.lblDBConnResults.Name = "lblDBConnResults";
            this.lblDBConnResults.Size = new System.Drawing.Size(502, 60);
            this.lblDBConnResults.TabIndex = 9;
            this.lblDBConnResults.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblDBConnResults.WordWrap = true;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(311, 88);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(181, 20);
            this.labelX4.TabIndex = 8;
            this.labelX4.Text = "Failed";
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(92, 88);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(181, 20);
            this.labelX3.TabIndex = 7;
            this.labelX3.Text = "Passed";
            // 
            // lvFailed
            // 
            // 
            // 
            // 
            this.lvFailed.Border.BorderColor = System.Drawing.Color.LightGray;
            this.lvFailed.Border.Class = "ListViewBorder";
            this.lvFailed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lvFailed.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.lvFailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvFailed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lvFailed.Location = new System.Drawing.Point(311, 108);
            this.lvFailed.Name = "lvFailed";
            this.lvFailed.Size = new System.Drawing.Size(184, 209);
            this.lvFailed.TabIndex = 6;
            this.lvFailed.UseCompatibleStateImageBehavior = false;
            this.lvFailed.View = System.Windows.Forms.View.List;
            this.lvFailed.SelectedIndexChanged += new System.EventHandler(this.lvFailed_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 183;
            // 
            // lvPassed
            // 
            // 
            // 
            // 
            this.lvPassed.Border.BorderColor = System.Drawing.Color.LightGray;
            this.lvPassed.Border.Class = "ListViewBorder";
            this.lvPassed.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lvPassed.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvPassed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvPassed.ForeColor = System.Drawing.Color.ForestGreen;
            this.lvPassed.Location = new System.Drawing.Point(90, 108);
            this.lvPassed.Name = "lvPassed";
            this.lvPassed.Size = new System.Drawing.Size(184, 209);
            this.lvPassed.TabIndex = 5;
            this.lvPassed.UseCompatibleStateImageBehavior = false;
            this.lvPassed.View = System.Windows.Forms.View.List;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 183;
            // 
            // pbConnectionTest
            // 
            this.pbConnectionTest.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.pbConnectionTest.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.pbConnectionTest.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbConnectionTest.BackgroundStyle.BorderBottomWidth = 1;
            this.pbConnectionTest.BackgroundStyle.BorderColor = System.Drawing.Color.LightGray;
            this.pbConnectionTest.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbConnectionTest.BackgroundStyle.BorderLeftWidth = 1;
            this.pbConnectionTest.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbConnectionTest.BackgroundStyle.BorderRightWidth = 1;
            this.pbConnectionTest.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.pbConnectionTest.BackgroundStyle.BorderTopWidth = 1;
            this.pbConnectionTest.BackgroundStyle.Class = "";
            this.pbConnectionTest.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pbConnectionTest.ChunkColor = System.Drawing.Color.CornflowerBlue;
            this.pbConnectionTest.ChunkColor2 = System.Drawing.Color.White;
            this.pbConnectionTest.ChunkGradientAngle = 90;
            this.pbConnectionTest.Location = new System.Drawing.Point(90, 48);
            this.pbConnectionTest.Name = "pbConnectionTest";
            this.pbConnectionTest.Size = new System.Drawing.Size(405, 28);
            this.pbConnectionTest.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2000;
            this.pbConnectionTest.TabIndex = 4;
            // 
            // lblTestingDB
            // 
            this.lblTestingDB.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.lblTestingDB.BackgroundStyle.Class = "";
            this.lblTestingDB.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTestingDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestingDB.Location = new System.Drawing.Point(90, 19);
            this.lblTestingDB.Name = "lblTestingDB";
            this.lblTestingDB.Size = new System.Drawing.Size(405, 23);
            this.lblTestingDB.TabIndex = 3;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::RMXdBUpgradeWizard.Properties.Resources.ConnectWait;
            this.pictureBox4.Location = new System.Drawing.Point(528, 29);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(64, 64);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // LblDsnListHeader
            // 
            this.LblDsnListHeader.AutoSize = true;
            this.LblDsnListHeader.BackColor = System.Drawing.Color.White;
            this.LblDsnListHeader.Location = new System.Drawing.Point(7, 75);
            this.LblDsnListHeader.Name = "LblDsnListHeader";
            this.LblDsnListHeader.Size = new System.Drawing.Size(142, 13);
            this.LblDsnListHeader.TabIndex = 1;
            this.LblDsnListHeader.Text = "Configration File(s) selected :";
            this.LblDsnListHeader.Visible = false;
            // 
            // LblDsnList
            // 
            this.LblDsnList.AutoSize = true;
            this.LblDsnList.BackColor = System.Drawing.Color.White;
            this.LblDsnList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDsnList.Location = new System.Drawing.Point(136, 75);
            this.LblDsnList.Name = "LblDsnList";
            this.LblDsnList.Size = new System.Drawing.Size(66, 13);
            this.LblDsnList.TabIndex = 2;
            this.LblDsnList.Text = "LblDsnList";
            this.LblDsnList.Visible = false;
            // 
            // frmWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 550);
            this.Controls.Add(this.LblDsnList);
            this.Controls.Add(this.LblDsnListHeader);
            this.Controls.Add(this.wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RMX Configration File Encryption";
            this.TitleText = "RMX Configration File Encryption";
            this.Load += new System.EventHandler(this.frmWizard_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmWizard_MouseUp);
            this.wizard1.ResumeLayout(false);
            this.wpDisclaimer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.wpBuildConnect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.wpDataSources.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.wpErrorPage.ResumeLayout(false);
            this.gbError.ResumeLayout(false);
            this.gbTechDesc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.wpFinish.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private DevComponents.DotNetBar.Wizard wizard1;
        private DevComponents.DotNetBar.WizardPage wpDisclaimer;
        private DevComponents.DotNetBar.LabelX lblDisclaimer;
        private DevComponents.DotNetBar.WizardPage wpDataSources;
        private DevComponents.DotNetBar.LabelX lblError;
        public System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.WizardPage wpFinish;
        private DevComponents.DotNetBar.LabelX labelX2;
        public DevComponents.DotNetBar.Controls.ListViewEx lvDatabases;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX lblWarningWPDS;
        private DevComponents.DotNetBar.WizardPage wpValidateConnections;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private DevComponents.DotNetBar.LabelX lblTestingDB;
        private DevComponents.DotNetBar.Controls.ProgressBarX pbConnectionTest;
        private DevComponents.DotNetBar.Controls.ListViewEx lvFailed;
        private DevComponents.DotNetBar.Controls.ListViewEx lvPassed;
        private DevComponents.DotNetBar.LabelX lblDBConnResults;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.WizardPage wpProcess;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private DevComponents.DotNetBar.LabelX lblProcessingDB;
        private DevComponents.DotNetBar.LabelX lblProcessingScript;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX tbExecuting;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        public DevComponents.DotNetBar.Controls.ProgressBarX pbOverall;
        public DevComponents.DotNetBar.Controls.ProgressBarX pbCurrentFile;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.LabelX labelX12;
        private System.Windows.Forms.PictureBox pictureBox6;
        private DevComponents.DotNetBar.LabelX labelX13;
        private System.Windows.Forms.ColumnHeader dsn;
        private System.Windows.Forms.ColumnHeader id;
        private DevComponents.DotNetBar.LabelX lblException;
        private System.Windows.Forms.GroupBox gbTechDesc;
        private System.Windows.Forms.GroupBox gbError;
        private DevComponents.DotNetBar.WizardPage wpBuildConnect;
        private System.Windows.Forms.PictureBox pictureBox7;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.ButtonX btnSecConn;
        private DevComponents.DotNetBar.Controls.TextBoxX tbSecDSN;
        private DevComponents.DotNetBar.ButtonX btnSaveConfig;
        private DevComponents.DotNetBar.LabelX lblSecPWD;
        private DevComponents.DotNetBar.Controls.TextBoxX tbSecPWD;
        private DevComponents.DotNetBar.LabelX lblSecUID;
        private DevComponents.DotNetBar.Controls.TextBoxX tbSecUID;
        private DevComponents.DotNetBar.LabelX lblSecServer;
        private DevComponents.DotNetBar.Controls.TextBoxX tbSecServer;
        private DevComponents.DotNetBar.LabelX lblSecDSNorDriver;
        private DevComponents.DotNetBar.LabelX lblSecDB;
        private DevComponents.DotNetBar.Controls.TextBoxX tbSecDB;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX16;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label LblDsnListHeader;
        private System.Windows.Forms.Label LblDsnList;
        private DevComponents.DotNetBar.WizardPage wpErrorPage;
    }
}