﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Xml;
using DevComponents.DotNetBar;
using Riskmaster.Security.Encryption;
using System.IO;

namespace RMXdBUpgradeWizard
{
    public partial class frmWizard : Office2007Form
    {
        #region global variables
        private bool _dragging;
        private Point _offset;
        private string _errorCache;
        private Configuration config;
       
        #endregion

        public frmWizard()
        {
            InitializeComponent();
            Activate();
            config = ListConfigFiles();
        }

        #region wizard page events
        /// <summary>
        /// wizard page changing function - all navigation processing and call originate from here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (e.NewPage == wpErrorPage && e.PageChangeSource == eWizardPageChangeSource.NextButton )
            {
                e.NewPage = wpFinish;
            }

            //rsolanki2 : adding the back button to the finish page.
            if (e.OldPage == wpFinish && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                
                //reselecting the previously selected DSN's 
                if (!string.IsNullOrEmpty(LblDsnList.Text))
                {
                    string[] arrDsnList = LblDsnList.Text.Split(',');
                    foreach (string sDsnEntry in arrDsnList)
                    {
                        foreach (ListViewItem lvi in lvDatabases.Items)
                        {
                            if (lvi.Text.Equals(sDsnEntry))
                            {
                                lvi.Selected = true;
                                break;
                            }
                        }
                    }

                    LblDsnList.Visible = true;
                    LblDsnListHeader.Visible = true;
                }
                return;
            }
            //leaving disclaimer page and check connectivity to security database
            if (e.OldPage == wpDisclaimer && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //load the database list if validation passes
                if (LoadConfig())
                {
                    Cursor = Cursors.Arrow;
                    e.NewPage = wpDataSources;
                    Application.DoEvents();
                }
                else //else load up the build connection screen
                {
                    //e.Cancel = true;
                    Cursor = Cursors.Arrow;
                    Application.DoEvents();
                }
            }


            btnSaveConfig.Enabled = false;
            wpBuildConnect.BackButtonVisible = eWizardButtonState.False;
            wpBuildConnect.CancelButtonVisible = eWizardButtonState.True;
            wpBuildConnect.NextButtonVisible = eWizardButtonState.True;
            wpBuildConnect.NextButtonEnabled = eWizardButtonState.False;

                
                if (e.OldPage == wpBuildConnect && e.PageChangeSource == eWizardPageChangeSource.NextButton)
                {
                    //load the database list if validation passes
                    if (LoadConfig())
                    {
                        Cursor = Cursors.Arrow;
                        e.NewPage = wpDataSources;
                        Application.DoEvents();
                    }
                    else //else load up the build connection screen
                    {
                        e.Cancel = true;
                        Cursor = Cursors.Arrow;
                        //e.NewPage = wpBuildConnect;
                    }
                }

            //leaving database selection page and validating connection for those selected
            if (e.OldPage == wpDataSources && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                if (lvDatabases.SelectedItems.Count == 0)
                {
                    lblWarningWPDS.Visible = true;
                    e.Cancel = true;
                }
                else
                {
                    if (!DataEncrypt())
                    {
                        return;
                    }
                }

            }

            //rsolanki2: mits 21693
            if (e.OldPage == wpDataSources && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                LblDsnList.Visible = false;
                LblDsnListHeader.Visible = false;
            }
            //make sure that any selected items in the dB list are de-selected during navigation to
            //ensure selection before proceeding
            if (e.NewPage == wpDataSources)
            {
                lvDatabases.SelectedItems.Clear();
            }

            //if the error wasn't serious we'll allow starting over
            if (e.OldPage == wpErrorPage && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                e.NewPage = wpDisclaimer;
            }
        }

        /// <summary>
        /// after validation page opens test connections to selected data sources
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        

        /// <summary>
        /// wizard processing page all the upgrades will be done here with progress visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpProcess_AfterPageDisplayed(object sender, WizardPageChangeEventArgs e)
        {
            wpProcess.NextButtonVisible = eWizardButtonState.False;
            wpProcess.BackButtonVisible = eWizardButtonState.False;
            Application.DoEvents();

            //leave wizard to process the database upgrades
            //UpgradeScripts.UpgradeRMDatabase(_isCustom, false);

            wpProcess.NextButtonEnabled = eWizardButtonState.True;

            
            Application.DoEvents();
        }

        /// <summary>
        /// display error messages in the wizard rather than in a pop-up message box
        /// </summary>
        /// <param name="strErrorMsg"></param>
        /// <param name="strIcon"></param>
        /// <param name="bDisplayButton"></param>
        public static void ErrorDisplay(string sError, string sIcon, eWizardButtonState bDisplayButton, string sException)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                PictureBox myPB = myForm.wpErrorPage.Controls["picturebox1"] as PictureBox;

                switch (sIcon.ToUpper().Trim())
                {
                    case "ERROR":
                        myPB.Image = SystemIcons.Error.ToBitmap();
                        break;
                    case "INFO":
                        myPB.Image = SystemIcons.Information.ToBitmap();
                        break;
                    case "WARNING":
                        myPB.Image = SystemIcons.Warning.ToBitmap();
                        break;
                    default:
                        break;
                }

                myForm.wpErrorPage.BackButtonVisible = bDisplayButton;
                myForm.wpErrorPage.NextButtonVisible = eWizardButtonState.False;
                myForm.wpErrorPage.Controls["gbError"].Controls["lblError"].Text = sError;

                if (!String.IsNullOrEmpty(sException))
                {
                    myForm.wpErrorPage.Controls["gbTechDesc"].Visible = true;
                    myForm.wpErrorPage.Controls["gbTechDesc"].Text = "Technical Description";
                    myForm.wpErrorPage.Controls["gbTechDesc"].Controls["lblException"].Text = sException;
                }
                else
                {
                    myForm.wpErrorPage.Controls["gbTechDesc"].Visible = false;
                }

                myForm.wizard1.SelectedPage = myForm.wpErrorPage;
            }
        }
        #endregion

        #region Config functions        

        private bool DataEncrypt()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                ConfigurationSection configSection = config.GetSection("EncryptRegion");

                string sXml = configSection.SectionInformation.GetRawXml();
                xDoc.LoadXml(sXml);
                
                XmlNodeList XMLEncryptList = xDoc.GetElementsByTagName("EncryptRegion");

                XmlDocument XMLDoc = new XmlDocument();
                XmlDocument InnerDOC = new XmlDocument();
                string sSource = string.Empty;
                string sfindAttributeMatching = string.Empty;
                string sreplaceAttributeName = string.Empty;
                string sRootNode = string.Empty;

                string sTargerAttribute = string.Empty;
                string sTargerAttributeValue = string.Empty;

                XmlNode xEncryptTargetSection = null;

                foreach (XmlNode XMLEncryptNode in XMLEncryptList)
                {                    
                    XmlNodeList xFileList = XMLEncryptNode.SelectNodes("File");

                    foreach (XmlNode xFile in xFileList)
                    {
                        string sFileName = xFile.SelectSingleNode("source").Attributes["filename"].Value;
                        for (int i = 0; i < lvDatabases.SelectedItems.Count; i++)
                        {
                            if (sFileName == lvDatabases.SelectedItems[i].Text)
                            {

                                sSource = xFile.SelectSingleNode("source").Attributes["filepath"].Value;
                                XMLDoc.Load(sSource);                                

                                XmlNodeList xRootNode = xFile.SelectNodes("RootNode");
                                foreach (XmlNode xChildsNode in xRootNode)
                                {
                                    sRootNode = xChildsNode.Attributes["name"].Value;

                                    foreach (XmlNode xEncryptNOde in xChildsNode.ChildNodes)
                                    {
                                        if (xEncryptNOde.Attributes[0].Name != "findAttributeMatching" || xEncryptNOde.Attributes[1].Name != "replaceAttributeName")
                                        {
                                            string Error = "EncryptNodes did not match the desired nodes.";
                                            string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                                            LogErrors(strLogFilePath, Error);

                                            _errorCache = "An error has occurred encrypting the configuration file." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused when information is wrongly passed, " +
                              "please double the passed information before trying again.";
                                            string sNull = Error;
                                            ErrorDisplay(_errorCache, "error", eWizardButtonState.True, Error);

                                            return false;
                                        }

                                        sfindAttributeMatching = xEncryptNOde.Attributes["findAttributeMatching"].Value;

                                        sreplaceAttributeName = xEncryptNOde.Attributes["replaceAttributeName"].Value;

                                        sTargerAttribute = sfindAttributeMatching.Split('=')[0].Trim();
                                        sTargerAttributeValue = sfindAttributeMatching.Split('=')[1].Trim();

                                        xEncryptTargetSection = XMLDoc.SelectSingleNode("//" + sRootNode);
                                        if (xEncryptTargetSection == null)
                                        {
                                            string Error = "RootNode not found in the Configuration File.";
                                            string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                                            LogErrors(strLogFilePath, Error);

                                            _errorCache = "An error has occurred encrypting the configuration file." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused when information is wrongly passed, " +
                              "please double the passed information before trying again.";
                                            string sNull = Error;
                                            ErrorDisplay(_errorCache, "error", eWizardButtonState.True, Error);

                                            return false;
                                        }
                                            foreach (XmlNode xmlnodeTemp in xEncryptTargetSection.ChildNodes)
                                            {
                                                if (xmlnodeTemp.Name != "#comment")
                                                {
                                                    if (((XmlElement)xmlnodeTemp).Attributes[sTargerAttribute] != null && ((XmlElement)xmlnodeTemp).HasAttribute(sTargerAttribute))
                                                    {
                                                        if (((XmlElement)xmlnodeTemp).Attributes[sTargerAttribute].Value == sTargerAttributeValue)
                                                        {
                                                            if (((XmlElement)xmlnodeTemp).Attributes[sreplaceAttributeName] != null)
                                                            {                                                                
                                                                ((XmlElement)xmlnodeTemp).Attributes[sreplaceAttributeName].Value = RMCryptography.EncryptString(((XmlElement)xmlnodeTemp).Attributes[sreplaceAttributeName].Value);
                                                                string EncryptSuccess = "Encryption successful for <" + sreplaceAttributeName + "> of Attribute: " + sfindAttributeMatching;
                                                                string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                                                                LogErrors(strLogFilePath, EncryptSuccess);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }                                        
                                    }
                                }
                            }
                        }                        
                    }
                    XMLDoc.Save(sSource);
                }                
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("Could not find file") != -1)
                {
                    string Error = "File not found on the given address.";
                    string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                    LogErrors(strLogFilePath, Error);
                }
                _errorCache = "An error has occurred encrypting the configuration file." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused when information is wrongly passed, " +
                              "please double the passed information before trying again.";
                string sNull = ex.Message;
                ErrorDisplay(_errorCache, "error", eWizardButtonState.True, ex.Message);


                string End = "Configuration process ended at :" + DateTime.Now.ToString();
                string strLogFilePathEnd = Application.StartupPath + "\\EncryptionWizard.log";
                LogErrors(strLogFilePathEnd, End);
                

                return false;
            }
            string EndSuccess = "Configuration process ended at :" + DateTime.Now.ToString();
            string strLogFilePathEndSuccess = Application.StartupPath + "\\EncryptionWizard.log";
            LogErrors(strLogFilePathEndSuccess, EndSuccess);
            return true;
        }

        public static Configuration ListConfigFiles()
        {
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();

            fileMap.ExeConfigFilename = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe.config";

            //fileMap.ExeConfigFilename = @"RMXEncrypt.exe.config";
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            return config;
        }
        
        /// <summary>
        /// load the Config list
        /// </summary>
        private bool LoadConfig()
        {
            string Start = "Configuration process started at :" + DateTime.Now.ToString();
            string strLogFilePathStart = Application.StartupPath + "\\EncryptionWizard.log";
            LogErrors(strLogFilePathStart, Start);

            //clear list box of any previous items
            lvDatabases.Items.Clear();

            try
            {
                XmlDocument xDoc = new XmlDocument();
                ConfigurationSection configSection = config.GetSection("EncryptRegion");
                if (configSection == null)
                {
                    string Error = "Section Name <" + configSection.SectionInformation.Name + "> not found in the configuration file.";
                    string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                    LogErrors(strLogFilePath, Error);

                    _errorCache = "An error has occurred encrypting the configuration file." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused when information is wrongly passed, " +
                              "please double the passed information before trying again.";
                    string sNull = Error;
                    ErrorDisplay(_errorCache, "error", eWizardButtonState.True, Error);

                    return false;
                }              

                string sXml = configSection.SectionInformation.GetRawXml();
                xDoc.LoadXml(sXml);

                XmlNodeList EncryptList = xDoc.GetElementsByTagName("EncryptRegion");

                foreach (XmlNode EncryptNode in EncryptList)
                {
                    XmlNodeList InnerList = EncryptNode.SelectNodes("File");
                    foreach (XmlNode InnerNode in InnerList)
                    {
                        string sKey = InnerNode.SelectSingleNode("source").Attributes["filename"].Value.ToString();
                        ListViewItem lvi = new ListViewItem(sKey);
                        lvDatabases.Items.Add(lvi);
                    }
                }

                if (lvDatabases.Items.Count <= 0)
                {
                    _errorCache = "An error has occurred while loading the configuration file." + 
                                  Environment.NewLine + Environment.NewLine + 
                                  "There are no config Files configured. " + 
                                  "Please correct and try again later.";
                    ErrorDisplay(_errorCache, "error", eWizardButtonState.True, String.Empty);

                    string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                    LogErrors(strLogFilePath, _errorCache);

                    return false;
                }

                lvDatabases.SelectedItems.Clear();

                return true;
            }
            catch (Exception ex)
            {
                _errorCache = "An error has occurred while loading the configuration file." +
                              Environment.NewLine + Environment.NewLine +
                              "This is usually caused by not being able to find the file name, " +
                              "please double the passed information before trying again.";
                string sNull = ex.Message;
                ErrorDisplay(_errorCache, "error", eWizardButtonState.True, ex.Message);

                string strLogFilePath = Application.StartupPath + "\\EncryptionWizard.log";
                LogErrors(strLogFilePath, ex.Message);

                return false;
            }

                
        }


        /// <summary>
        /// fill public list with database selections
        /// </summary>
        /// <returns></returns>
        

        /// <summary>
        /// disable warning if an item is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvDatabases_Click(object sender, EventArgs e)
        {
            if (lvDatabases.SelectedItems.Count != 0)
            {
                lblWarningWPDS.Visible = false;
            }
        }

        /// <summary>
        /// lvFailed_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvFailed_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sCaption = String.Empty;

            ListView.SelectedListViewItemCollection objItems = this.lvFailed.SelectedItems;
            int iCnt = objItems.Count;

            if (iCnt > 0)
            {
                ListViewItem lvi = objItems[0];
                sCaption = String.Format("Selected Configuration File: {0}", lvi.SubItems[0].Text);
                MessageBox.Show(lvi.ImageKey, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region progress bar and display text functions
        /// <summary>
        /// set maximum properties on overall progress bars
        /// </summary>
        /// <param name="iOverallMaximum"></param>
        public static void SetOverallProgressBarProperties(int iOverallMaximum)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbOverall.Maximum = iOverallMaximum;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// set maximum properties on current progress bars
        /// </summary>
        /// <param name="iCurrentMaximum"></param>
        public static void SetCurrentProgressBarProperties(int iCurrentMaximum)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbCurrentFile.Maximum = iCurrentMaximum;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// update the overall progress bar on process page
        /// </summary>
        /// <param name="iOverall"></param>
        public static void UpdateOverallProgressBar(int iOverall)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbOverall.Value = iOverall;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// update the current progress bar on process page
        /// </summary>
        /// <param name="iCurrentFile"></param>
        public static void UpdateCurrentProgressBar(int iCurrentFile)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbCurrentFile.Value = iCurrentFile;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// updates the name of the script file being processed
        /// </summary>
        /// <param name="sScriptName"></param>
        public static void UpdateScriptName(string sScriptName)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                if (sScriptName.Length > 33)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-4' color='#000000'>" + sScriptName + " </font>";
                }
                else if (sScriptName.Length > 30)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-3' color='#000000'>" + sScriptName + " </font>";
                }
                else if (sScriptName.Length > 25)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-2' color='#000000'>" + sScriptName + " </font>";
                }
                else
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font color='#000000'>" + sScriptName + " </font>";
                }

                Application.DoEvents();
            }
        }


        /// <summary>
        /// update the executing text box with current SQL
        /// </summary>
        /// <param name="sSQL"></param>
        public static void UpdateExecutingText(string sSQL)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.tbExecuting.Text = sSQL;
                Application.DoEvents();
            }
        }
        #endregion

        #region wizard mouse events
        /// <summary>
        /// frmWizard_MouseDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseDown(object sender, MouseEventArgs e)
        {
            _offset.X = e.X;
            _offset.Y = e.Y;
            _dragging = true;
        }

        /// <summary>
        /// frmWizard_MouseUp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }
        
        /// <summary>
        /// frmWizard_MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - _offset.X, currentScreenPos.Y - _offset.Y);
            }
        }
        #endregion

        #region wizard cancel/finish events
        /// <summary>
        /// finish button clicked close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_FinishButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on disclaimer page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpDisclaimer_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on build connection page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpBuildConnect_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on data sources page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpDataSources_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// cancel was selected on validation page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpValidateConnections_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// cancel button pressed on error page - close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpErrorPage_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }
        #endregion
                       

        private void frmWizard_Load(object sender, EventArgs e)
        {

        }

        public static void LogErrors(string strLogFilePath, string sMsg)
        {
            File.AppendAllText(strLogFilePath, sMsg);

            //Insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }

        public IEnumerable<XmlNode> InnerList { get; set; }
    }
}
