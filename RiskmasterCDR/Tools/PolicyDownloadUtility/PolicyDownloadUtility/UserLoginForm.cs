﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Riskmaster.Db;


namespace PolicyDownloadUtility
{
    public partial class frmUserLoginForm : Form
    {
        public frmUserLoginForm()
        {
            InitializeComponent();

            string sSecurityConnectionString = Riskmaster.Security.SecurityDatabase.Dsn;
            string sSQL = "SELECT DSN, DSNID FROM DATA_SOURCE_TABLE ORDER BY DSN";
            string sDsnId = string.Empty;
            string sDsnName = string.Empty;
            using (DbReader oDbReader = DbFactory.GetDbReader(sSecurityConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    sDsnId = oDbReader["DSNID"].ToString();
                    sDsnName = oDbReader["DSN"].ToString();
                    cbDSNNm.Items.Add(new KeyValuePair<string, string>(sDsnId, sDsnName));
                }
            }
            
            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (txtUserNm.Text == string.Empty)
            {
                MessageBox.Show("Please enter User Name");
            }
            else if (cbDSNNm.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a DSN Name");
            }
            else
            {
                this.Hide();
                KeyValuePair<string, string> objList = (KeyValuePair<string,string>)cbDSNNm.SelectedItem;
                new frmPolicy(txtUserNm.Text, objList.Value,objList.Key).Show();
            }
        }
    }
}
