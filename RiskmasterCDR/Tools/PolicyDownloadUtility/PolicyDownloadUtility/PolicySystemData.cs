﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using PolicyDownloadUtility.PolicyInterfaceService;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Data.OleDb;
using PolicyDownloadUtility.AuthenticationService;
using Riskmaster.DataModel;
using System.Configuration;
using System.Threading;

namespace PolicyDownloadUtility
{
    public partial class frmPolicy : Form
    {
        #region Variables Declaration
        private static UserLogin m_objUserLogin = null;
        string sUserNm = string.Empty; 
        string sDataBaseNm = string.Empty;  
        string sDSNId = string.Empty;
        int iPolicyId = Int32.MinValue;
        XmlDocument objPolicySystemList = null;
        DataSet oPolicyData = null;
   //     PolicyInterfaceServiceClient rmservice = null;
     //   PolicySaveRequest oPolicySaveRequest = null, oPolicySaveResponse = null;
        private string sPolicyNumber = string.Empty, sModule = string.Empty, sLocCompny = string.Empty, sClaimNumber = string.Empty, sPolicySymbol = string.Empty, sPolMasterCompny = string.Empty, sLOBCd = string.Empty;
        private string  sUnitNumber = string.Empty, sRiskLoc = string.Empty, sRiskSubLoc = string.Empty;
        private string sPolicySytemID = string.Empty;
        string sClaimID=string.Empty;
        private string sSession=string.Empty;
        private string sDSNConnectionstring = string.Empty;
        private int iClaimId = Int32.MinValue;
        private string sBaseLOB = string.Empty;
        private int iEntityRole = Int32.MinValue;
        private string sStatUnitNo = string.Empty;
        private string sRange = string.Empty;
        int iLowerIndex = Int32.MinValue, iUpperIndex = Int32.MinValue;
        private int m_iClientId = 0;
        
        #endregion

        #region Properties
        public string PolicySystemID
        {
            get{  return sPolicySytemID;  }
            set{  sPolicySytemID = value; }
        }
        public string PolicyNumber
        {
            get { return sPolicyNumber; }
            set { sPolicyNumber = value; }

        }
        public string ClaimNumber
        {
            get { return sClaimNumber; }
            set { sClaimNumber = value; }
                
        }
        public string PolicySymbol
        {
            get { return sPolicySymbol; }
            set { sPolicySymbol = value; }
        }
        public string MasterCompany
        {
            get { return sPolMasterCompny; }
            set { sPolMasterCompny = value; }
        }
        public string LocationCompny
        {
            get { return sLocCompny; }
            set { sLocCompny = value; }
        }
        public string Module
        {
            get { return sModule; }
            set { sModule = value; }
        }
        public string LOBCd
        {
            set { sLOBCd = value; }
            get { return sLOBCd; }
        }
        public string UnitNumber
        {
            get { return sUnitNumber; }
            set { sUnitNumber = value; }
        }
        public string RiskLoc
        {
            get { return sRiskLoc; }
            set { sRiskLoc = value; }
        }
        public string RiskSubLoc
        {
            get { return sRiskSubLoc; }
            set { sRiskSubLoc = value; }
        }
        public string DSNConnectionString
        {
            get { return sDSNConnectionstring;}
            set { sDSNConnectionstring = value; }
        }
        public int ClaimId
        {
            set { iClaimId = value; }
            get { return iClaimId; }
        }
        public int PolicyId
        {
            get { return iPolicyId; }
            set { iPolicyId = value; }
        }
        public string Session
        {
            get { return sSession; }
            set { sSession = value; }
        }
        public string BaseLOB
        {
            get { return sBaseLOB; }
            set { sBaseLOB = value; }
        }
        public int EntityRole
        {
            get { return iEntityRole; }
            set { iEntityRole = value; }
        }
        public string StatUnitNo
        {
            get { return sStatUnitNo; }
            set { sStatUnitNo = value; }
        }
        public string Range
        {
            get { return sRange; }
            set { sRange = value; }
        }
        public int LowerIndex
        {
            get { return iLowerIndex; }
            set { iLowerIndex = value; }
        }
        public int UpperIndex
        {
            get { return iUpperIndex; }
            set { iUpperIndex = value; }
        }
        
        #endregion

        public frmPolicy(string pUserNm, string pDSNNm,string pDSNId)
        {
            InitializeComponent();
            objPolicySystemList = new XmlDocument();
            oPolicyData = new DataSet();

            sUserNm = pUserNm;
            sDataBaseNm = pDSNNm;
            sDSNId = pDSNId;
            PolicyInterfaceServiceClient rmservice = null;
            try
            {
                m_objUserLogin = new UserLogin(sUserNm, sDataBaseNm, m_iClientId);
                DSNConnectionString = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
                AuthenticationServiceClient objauth = new AuthenticationServiceClient();
                Session = objauth.GetUserSessionID(sUserNm, sDataBaseNm);

                PolicySystemList oRequest = new PolicySystemList();
                PolicySystemList oResponse = new PolicySystemList();

                rmservice = new PolicyInterfaceServiceClient();
                oRequest.Token = Session;
                oResponse = rmservice.GetPolicySystemList(oRequest);

                oPolicyData.ReadXml(new StringReader(oResponse.ResponseXML));
                cbPolicySysID.DataSource = oPolicyData.Tables[1];

                cbPolicySysID.ValueMember = "value";
                cbPolicySysID.DisplayMember = "option_text";
            }
            catch (Exception ex)
            {
                Log.Write("Exception occured. Details : " + ex.Message, m_iClientId);
                return;
            }
            finally
            {
                rmservice = null;
            }

        }

        private void GetEntityRole()
        {
            LocalCache objCache = null;
            string sTableNm = string.Empty;
            try
            {
                objCache = new LocalCache(DSNConnectionString, m_iClientId);
                sTableNm = ConfigurationSettings.AppSettings["EntityRole"];

                EntityRole = objCache.GetTableId(sTableNm);
            }
            catch (Exception e)
            {
                Log.Write("Exception occured while trying to get entity role. Details =" + e.Message, m_iClientId);
            }
            finally
            {
                if(objCache!=null)
                    objCache.Dispose();
            }

        }
             
        private bool GetBaseLOB()
        {
            LocalCache objcache = null;
            bool bResult = true;

            try
            {
                objcache = new LocalCache(DSNConnectionString, m_iClientId);

                BaseLOB = objcache.GetRelatedShortCode(objcache.GetCodeId(LOBCd, "POLICY_CLAIM_LOB"));
                if (string.IsNullOrEmpty(BaseLOB))
                {
                    bResult = false;
                }
            }
            catch (Exception e)
            {
                Log.Write("Exception occured while trying to get base lob. Details =" + e.Message, m_iClientId);
            }
            finally
            {
                if (objcache != null)
                    objcache.Dispose();
                
            }
            return bResult;
        }

        
        private int GetClaimId(string pClaimNumber)
        {
            int iClaimID = Int32.MinValue;
            DataModelFactory objFactory = new DataModelFactory(m_objUserLogin, m_iClientId);
            
            Claim objClaim = (Claim)objFactory.GetDataModelObject("Claim", false);

            string sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + pClaimNumber+"'";
          
            using (DbReader oDbReader = DbFactory.GetDbReader(DSNConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    iClaimID = Conversion.ConvertObjToInt(oDbReader["CLAIM_ID"], m_iClientId);
                    break;
                }
            }
            return iClaimID;
        }

        private void SavePolicyData()
        {
            PolicySaveRequest oPolicySaveRequest = null;
            PolicySaveRequest oPolicySaveResponse = null;
            PolicyInterfaceServiceClient rmservice = null;
            try
            {
                Log.Write("Trying to Call SaveExternalPolicy() method of PolicyInterfaceService to download policy information and generate policy id...", m_iClientId);
                oPolicySaveRequest = new PolicySaveRequest();
                oPolicySaveRequest.PolicyIdentfier = PolicySystemID;
                oPolicySaveRequest.PolicySystemId = Conversion.ConvertStrToInteger(PolicySystemID);
                oPolicySaveRequest.PolicyNumber = PolicyNumber;
                oPolicySaveRequest.PolicySymbol = PolicySymbol;
                oPolicySaveRequest.MasterCompany = MasterCompany;
                oPolicySaveRequest.Location = LocationCompny;
                oPolicySaveRequest.Module = Module;

                oPolicySaveRequest.LOB = LOBCd ;
                oPolicySaveRequest.ClaimId = ClaimId;
                oPolicySaveRequest.Token = Session;

                rmservice = new PolicyInterfaceServiceClient();
                oPolicySaveResponse = new PolicySaveRequest();
                try
                {
                    //Get additional details to save policy
                    oPolicySaveRequest = SetoPolicySaveRequestWithAdditionalInfo(oPolicySaveRequest);
                    oPolicySaveResponse = rmservice.SaveExternalPolicy(oPolicySaveRequest);
                }
                catch (Exception Ex)
                {
                    Log.Write("Error occured while trying to download and save policy information in SaveExternalPolicy() method of PolicyInterfaceService.", m_iClientId);
                    Log.Write("Exception details: " + Ex.Message, m_iClientId);
                    Log.Write(String.Format("Skipping  Claim Number = {0} for further processing. Excpetion: {1}", ClaimNumber, Ex.Message), "RecordFailedLog", m_iClientId);
                    
                    PolicyId = Int32.MinValue;
                }
                if (oPolicySaveResponse != null)
                {
                    PolicyId = oPolicySaveResponse.AddedPolicyId;
                }

                //TODO: Check if this works
                //            BaseLOB = oPolicySaveRequest.BaseLOBLine;
                //PolicySystemInterfaceAdaptor oAdaptor = new PolicySystemInterfaceAdaptor();
                //BusinessAdaptorErrors objErrors = new BusinessAdaptorErrors();
                //try
                //{
                //    BaseLOB = oAdaptor.GetPolicyLOBRelatedShortCode(LOBCd.Trim().ToUpper(), "POLICY_CLAIM_LOB", ref objErrors);
                //}
                //catch (Exception ex)
                //{
                //    Log.Write("Exception whilet trying to get data Base LOB value. Exception details: "+ex.Message);
                //}

                //TODO: end
            }
            finally
            {
                oPolicySaveRequest = null;
                oPolicySaveResponse = null;
                rmservice = null;
            }
        }
        private PolicySaveRequest SetoPolicySaveRequestWithAdditionalInfo(PolicySaveRequest oPolicySaveRequest)
        {
            try
            {
                PolicySearch oSearchRequest = new PolicySearch();
                oSearchRequest.objSearchFilters = new SearchFilters();
                PolicySearch oSearchResponse = null;
                PolicyInterfaceServiceClient rmservice = null;

                LocalCache objcache = null;
                objcache = new LocalCache(DSNConnectionString, m_iClientId);
                
                oSearchRequest.objSearchFilters.PolicyNumber = PolicyNumber;
                oSearchRequest.objSearchFilters.PolicySymbol = PolicySymbol;
                oSearchRequest.objSearchFilters.Module = Module;
                oSearchRequest.objSearchFilters.PolicySystemId = Conversion.ConvertStrToInteger(PolicySystemID);
                oSearchRequest.objSearchFilters.LOB = objcache.GetCodeId(LOBCd, "POLICY_CLAIM_LOB");
                oSearchRequest.objSearchFilters.LocationCompany = LocationCompny;
                oSearchRequest.Token = Session;

                rmservice = new PolicyInterfaceServiceClient();
                oSearchResponse = rmservice.GetPolicySearchResult(oSearchRequest);

                XmlDocument oResponce = new XmlDocument();
                oResponce.LoadXml(oSearchResponse.ResponseXML.ToString());

                XmlNode oNode = oResponce.SelectSingleNode("SearchResults/Row");
                if (oNode != null)
                {
                    oPolicySaveRequest.InsurerNm = oNode["InsurerName"].InnerText;
                    oPolicySaveRequest.InsurerAddr1 = oNode["InsurerAddr1"].InnerText;
                    oPolicySaveRequest.InsurerCity = oNode["InsurerCity"].InnerText;
                    oPolicySaveRequest.InsurerPostalCd = oNode["InsurerPostalCode"].InnerText;
                    oPolicySaveRequest.TaxId = oNode["TaxId"].InnerText;
                    oPolicySaveRequest.BirthDt = oNode["BirthDt"].InnerText;
                    oPolicySaveRequest.AddressSeqNo = oNode["AddressSeqNo"].InnerText;
                    oPolicySaveRequest.ClientSeqNo = oNode["ClientSeqNo"].InnerText;
                }

            }
            catch (Exception Ex)
            {
                Log.Write("Error occured while getting policy information in Policy System.", m_iClientId);
                Log.Write("Exception details: " + Ex.Message, m_iClientId);
                Log.Write(String.Format("Skipping  Claim Number = {0} for further processing. Excpetion: {1}", ClaimNumber, Ex.Message), "RecordFailedLog", m_iClientId);
            }

            return oPolicySaveRequest;
        }

        private bool SavePolicyRelatedData()
        {
            PolicyInterfaceServiceClient rmservice = null;
            bool bResponse = false;
            try
            {
               
                rmservice = new PolicyInterfaceServiceClient();
                SaveDownloadOptions oDownloadRequest = new SaveDownloadOptions();

                Log.Write("Trying to call SavePolicyDataForUtility() method of PolicyInterfaceService to download policy objects(units/driver/interest details)...", m_iClientId);
                //set parameters for request 
                oDownloadRequest.PolicyId = PolicyId;
                oDownloadRequest.Token = Session;
                oDownloadRequest.PolicySystemId = Conversion.ConvertStrToInteger(PolicySystemID);

                //lets suppose
                oDownloadRequest.UnitNumber = UnitNumber;
                oDownloadRequest.RiskLocation = RiskLoc;
                oDownloadRequest.RiskSubLocation = RiskSubLoc;
                oDownloadRequest.StatUnitNumber = StatUnitNo;
                // end

                // set entity role here..
                oDownloadRequest.EntityRole = EntityRole.ToString();

                try
                {

                    bResponse = rmservice.SavePolicyDataForUtility(ref oDownloadRequest);
                }
                catch (Exception ex)
                {
                    Log.Write("Exception occured while trying to download policy objects(units/driver/interest details) in SavePolicyDataForUtility() method of PolicyInterfaceService.", m_iClientId);
                    Log.Write("Exception details :", ex.Message, m_iClientId);
                    Log.Write(String.Format("Skipping  Claim number={0} for further processing.Exception message = {1}", ClaimNumber, ex.Message), "RecordFailedLog", m_iClientId);
                    bResponse = false;
                }
            }
            finally
            {
                rmservice = null;
            }
            return bResponse;
        }

        private bool AttachPolicyToClaim()
        {
            ClaimXPolicy objClaimXPolicy =null;
            Claim objClaim = null;
            DataModelFactory oFactory = new DataModelFactory(m_objUserLogin, m_iClientId);
            objClaimXPolicy = (ClaimXPolicy)oFactory.GetDataModelObject("ClaimXPolicy", false);
            

            bool bResult = false;
            int iRowId = Int32.MinValue;

            using (DbReader objReader = DbFactory.GetDbReader(DSNConnectionString, "SELECT ROW_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + ClaimId + " AND POLICY_ID = " + PolicyId))
            {
                if (objReader.Read())
                    iRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
            }

            if (iRowId != Int32.MinValue)
            {
                Log.Write("Entry already exists. No need to add new entry to CLAIM_X_POLICY table.", m_iClientId);
                bResult = true;
                return bResult;
            }
            objClaimXPolicy.PolicyId = PolicyId;
            objClaimXPolicy.ClaimId = ClaimId;
            try
            {
                objClaimXPolicy.Save();

                objClaim = (Claim)objClaimXPolicy.Context.Factory.GetDataModelObject("Claim", false);
                if (objClaim != null)
                {
                    objClaim.MoveTo(ClaimId);
                    objClaim.PrimaryPolicyId = PolicyId;
                    objClaim.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Write("Exception occured while trying to save data into CLAIM_X_POLICY table...", m_iClientId);
                Log.Write("Exception details:" + ex.Message, m_iClientId);
                bResult = false;
            }
            finally
            {
                if (objClaim != null)
                    objClaim = null;
            }
            if (objClaimXPolicy.RowId != 0)
            {
                bResult = true;
            }

            return bResult;
        }

        private void AddExternalPolicyEntities()
        {
            DataModelFactory oFactory = new DataModelFactory(m_objUserLogin, m_iClientId);
            Claim objClaim = (Claim)oFactory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(ClaimId);

            foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
            {
                Policy objPolicy = (Policy)objClaim.Context.Factory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(objClaimXPolicy.PolicyId);
                if (objPolicy.PolicySystemId > 0)
                {
                    foreach (PolicyXUnit objPolicXUnit in objPolicy.PolicyXUnitList)
                    {
                        if (string.Equals(objPolicXUnit.UnitType, "V", StringComparison.InvariantCultureIgnoreCase))
                        {
                            int iUnitRowId = 0;
                            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT UNIT_ROW_ID FROM UNIT_X_CLAIM WHERE CLAIM_ID = " + objClaim.ClaimId + " AND UNIT_ID = " + objPolicXUnit.UnitId))
                            {
                                if (objReader.Read())
                                    iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                            }

                            if (iUnitRowId == 0)
                            {
                                UnitXClaim objUnitXClaim = (UnitXClaim)objClaim.Context.Factory.GetDataModelObject("UnitXClaim", false);
                                Vehicle objVehicle = (Vehicle)objClaim.Context.Factory.GetDataModelObject("Vehicle", false);
                                //if (iUnitRowId > 0)
                                //    objUnitXClaim.MoveTo(iUnitRowId);

                                objVehicle.MoveTo(objPolicXUnit.UnitId);

                                objUnitXClaim.ClaimId = objClaim.ClaimId;
                                objUnitXClaim.UnitId = objVehicle.UnitId;
                                objUnitXClaim.Vin = objVehicle.Vin;
                                objUnitXClaim.VehicleMake = objVehicle.VehicleMake;
                                objUnitXClaim.VehicleYear = objVehicle.VehicleYear;
                                objUnitXClaim.StateRowId = objVehicle.StateRowId;
                                objUnitXClaim.HomeDeptEid = objVehicle.HomeDeptEid;
                                objUnitXClaim.LicenseNumber = objVehicle.LicenseNumber;
                                objUnitXClaim.UnitTypeCode = objVehicle.UnitTypeCode;
                                //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                objUnitXClaim.Insured = true;
                                //End : Ankit
                                objUnitXClaim.Save();

                                objVehicle.Dispose();
                                objUnitXClaim.Dispose();
                            }
                        }
                        else if (string.Equals(objPolicXUnit.UnitType, "P", StringComparison.InvariantCultureIgnoreCase))
                        {

                            int iUnitRowId = 0;
                            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_PROPERTYLOSS WHERE CLAIM_ID = " + objClaim.ClaimId + " AND PROPERTY_ID = " + objPolicXUnit.UnitId))
                            {
                                if (objReader.Read())
                                    iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

                            }
                            if (iUnitRowId == 0)
                            {

                                ClaimXPropertyLoss objClaimXPropertyLoss = (ClaimXPropertyLoss)objClaim.Context.Factory.GetDataModelObject("ClaimXPropertyLoss", false);
                                //if (iUnitRowId > 0)
                                //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                                objClaimXPropertyLoss.ClaimId = objClaim.ClaimId;
                                objClaimXPropertyLoss.PropertyID = objPolicXUnit.UnitId;
                                //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                objClaimXPropertyLoss.Insured = true;
                                //End : Ankit
                                objClaimXPropertyLoss.Save();

                                objClaimXPropertyLoss.Dispose();
                            }
                        }

                        else if (string.Equals(objPolicXUnit.UnitType, "S", StringComparison.InvariantCultureIgnoreCase))
                        {

                            int iUnitRowId = 0;
                            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_SITELOSS WHERE CLAIM_ID = " + objClaim.ClaimId + " AND SITE_ID = " + objPolicXUnit.UnitId))
                            {
                                if (objReader.Read())
                                    iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

                            }
                            if (iUnitRowId == 0)
                            {

                                ClaimXSiteLoss objClaimXSiteLoss = (ClaimXSiteLoss)objClaim.Context.Factory.GetDataModelObject("ClaimXSiteLoss", false);
                                //if (iUnitRowId > 0)
                                //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                                objClaimXSiteLoss.ClaimId = objClaim.ClaimId;
                                objClaimXSiteLoss.SiteId = objPolicXUnit.UnitId;
                                //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                objClaimXSiteLoss.Insured = true;
                                //End : Ankit
                                objClaimXSiteLoss.Save();

                                objClaimXSiteLoss.Dispose();
                            }
                        }
                        else if (string.Equals(objPolicXUnit.UnitType, "SU", StringComparison.InvariantCultureIgnoreCase))
                        {

                            int iUnitRowId = 0;
                            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_OTHERUNIT WHERE CLAIM_ID = " + objClaim.ClaimId + " AND OTHER_UNIT_ID = " + objPolicXUnit.UnitId))
                            {
                                if (objReader.Read())
                                    iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

                            }
                            if (iUnitRowId == 0)
                            {

                                ClaimXOtherUnit objClaimXOtherUnit = (ClaimXOtherUnit)objClaim.Context.Factory.GetDataModelObject("ClaimXOtherUnit", false);
                                //if (iUnitRowId > 0)
                                //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                                objClaimXOtherUnit.ClaimId = objClaim.ClaimId;
                                objClaimXOtherUnit.OtherUnitId = objPolicXUnit.UnitId;
                                //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                                objClaimXOtherUnit.Insured = true;
                                //End : Ankit
                                objClaimXOtherUnit.Save();

                                objClaimXOtherUnit.Dispose();
                            }
                        }
                    }

                    foreach (PolicyXEntity objPolicyEntity in objPolicy.PolicyXEntityList)
                    {
                        CreateSubTypeEntity(objPolicyEntity.EntityId.ToString(), objClaim, objPolicyEntity.PolicyUnitRowid);
                    }
                }
            }

        }

        private void CreateSubTypeEntity(string sEntityId, Claim p_objClaim, int p_iPolicyUnitRowId)
        {
            LocalCache objCache = null;
            objCache = new LocalCache(DSNConnectionString, m_iClientId);
            
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            bool blnSuccess = false;
            int iEntityId = 0;
            Entity objEntity = null;
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            Patient objPatient = null;
            PiPatient objPiPatient = null;
            MedicalStaff objMedicalStaff = null;
            Physician objPhysician = null;

            try
            {
                iEntityId = Conversion.CastToType<Int32>(sEntityId, out blnSuccess);
                objEntity = (Entity)p_objClaim.Context.Factory.GetDataModelObject("Entity", false);
                objEntity.MoveTo(iEntityId);
                entityTableName = objCache.GetTableName(objEntity.EntityTableId);

                switch (entityTableName.ToUpper())
                {
                    case "EMPLOYEES":
                        string sEmpNumber = string.Empty;
                        sSQL = "SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                sEmpNumber = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            }
                        }

                        sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID = " + p_objClaim.EventId + " AND PI_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            objPiEmployee = (PiEmployee)p_objClaim.Context.Factory.GetDataModelObject("PiEmployee", false);
                            if (objReader.Read())
                            {
                                objPiEmployee.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));
                            }
                            else
                            {
                                objPiEmployee.EventId = p_objClaim.EventId;
                                objPiEmployee.PiEid = iEntityId;
                            }
                            if (!string.IsNullOrEmpty(sEmpNumber))
                                objPiEmployee.EmployeeNumber = sEmpNumber;
                            objPiEmployee.PiTypeCode = objCache.GetCodeId("E", "PERSON_INV_TYPE");
                            objPiEmployee.PolicyUnitRowId = p_iPolicyUnitRowId;
                            objPiEmployee.Save();
                            objPiEmployee.Dispose();
                        }
                        break;
                    case "PATIENTS":
                        int iPatientId = 0;
                        sSQL = "SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPatientId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                            }

                        }
                        sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID = " + p_objClaim.EventId + " AND PI_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            objPiPatient = (PiPatient)p_objClaim.Context.Factory.GetDataModelObject("PiPatient", false);
                            if (objReader.Read())
                            {
                                objPiPatient.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));
                            }
                            else
                            {
                                objPiPatient.EventId = p_objClaim.EventId;
                                objPiPatient.PiEid = iEntityId;
                            }
                            objPiPatient.PatientId = iPatientId;
                            objPiPatient.PiTypeCode = objCache.GetCodeId("P", "PERSON_INV_TYPE");
                            objPiPatient.PolicyUnitRowId = p_iPolicyUnitRowId;
                            objPiPatient.Save();
                            objPiPatient.Dispose();
                        }
                        break;
                    case "MEDICAL_STAFF":
                        CreatePersonInvolvedEntities(iEntityId, "MED", p_objClaim, p_iPolicyUnitRowId);
                        break;
                    case "PHYSICIANS":
                        CreatePersonInvolvedEntities(iEntityId, "PHYS", p_objClaim, p_iPolicyUnitRowId);
                        break;
                    case "WITNESS":
                        CreatePersonInvolvedEntities(iEntityId, "W", p_objClaim, p_iPolicyUnitRowId);
                        break;
                    case "DRIVERS":
                        CreatePersonInvolvedEntities(iEntityId, "D", p_objClaim, p_iPolicyUnitRowId);
                        break;
                    default:
                        CreatePersonInvolvedEntities(iEntityId, "O", p_objClaim, p_iPolicyUnitRowId);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objEmployee != null)
                    objEmployee.Dispose();
                if (objPiEmployee != null)
                    objPiEmployee.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if (objPiPatient != null)
                    objPiPatient.Dispose();
                if (objMedicalStaff != null)
                    objMedicalStaff.Dispose();
                if (objPhysician != null)
                    objPhysician.Dispose();
            }
        }

        private void AddExternalPolicyInsureds(bool p_IsWCClaim, string p_sPolicyId)
        {
            string sSQL = string.Empty;
            int iInsuredId = 0;
            string sDownloadedEntitiesIds = string.Empty;
            string sClaimantSQL = string.Empty;
            string sSaveInsuredAsClaimantSetting = string.Empty;

            Policy objPolicy = null;
            Claimant objClaimant = null;

            
            Claim objClaim = null;
            DataModelFactory oFactory = new DataModelFactory(m_objUserLogin, m_iClientId);
            objClaim = (Claim)oFactory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(ClaimId);

            sSaveInsuredAsClaimantSetting = ConfigurationSettings.AppSettings["AddInsuredAsClaimant"];

            try
            {
                //foreach (string spolicyId in test)//base.GetSysExDataNodeText("AddPolicyInsuredAsClaimant").Split(','))
                //{

                    objPolicy = (Policy)oFactory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(Conversion.ConvertStrToInteger(p_sPolicyId));
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sSQL = "SELECT INSURED_EID FROM POLICY_X_INSURED WHERE POLICY_ID= " + objPolicy.PolicyId;

                        using (DbReader oReader = DbFactory.GetDbReader(DSNConnectionString, sSQL))
                        {
                            while (oReader.Read())
                            {
                                iInsuredId = Conversion.ConvertObjToInt(oReader.GetValue("INSURED_EID"), m_iClientId);
                                if (p_IsWCClaim)
                                {
                                    CreateSubTypeEntity(iInsuredId.ToString(), objClaim, 0); ;
                                }
                                else
                                {
                                    sClaimantSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID= " + ClaimId + " AND CLAIMANT_EID =" + iInsuredId;
                                    //sClaimantSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID= " + objClaim.ClaimId + " AND CLAIMANT_EID =" + iInsuredId;
                                    using (DbReader objRdr = DbFactory.GetDbReader(DSNConnectionString, sClaimantSQL))
                                    {
                                        if (!objRdr.Read())
                                        {
                                            if (sSaveInsuredAsClaimantSetting.ToUpper() == "YES")
                                            {
                                                objClaimant = (Claimant)oFactory.GetDataModelObject("Claimant", false);
                                                objClaimant.ClaimantEid = iInsuredId;
                                                objClaimant.ClaimId = ClaimId; //objClaim.ClaimId;
                                                objClaimant.Save();
                                                objClaimant.Dispose();

                                                CreatePersonInvolvedEntities(iInsuredId, "O", objClaim, 0);
                                            }
                                           
                                        }

                                    }
                                    
                                }
                            }
                        }
                    }
                    objPolicy.Dispose();

               // }
            }
            finally
            {
                if (objPolicy != null)
                    objPolicy.Dispose();
                if (objClaimant != null)
                    objClaimant.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
            }

        }

        private void CreatePersonInvolvedEntities(int iEntityId, string sPiType, Claim p_objClaim, int iPolicyUnitRowId)
        {
           
            LocalCache objCache = null;
            objCache = new LocalCache(DSNConnectionString, m_iClientId);

            PersonInvolved objPersonInvloved;
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            int iPiRowId = 0, iDriverRowId = 0;

            sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID = " + p_objClaim.EventId + " AND PI_EID = " + iEntityId;
            using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
            {
                if (objReader.Read())
                {
                    iPiRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                }
            }

            objPersonInvloved = (PersonInvolved)p_objClaim.Context.Factory.GetDataModelObject("PersonInvolved", false);
            if (iPiRowId > 0)
                objPersonInvloved.MoveTo(iPiRowId);

            //for driver only
            if (sPiType.Trim().ToUpper() == "D" && iPiRowId == 0)
            {
                sSQL = "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID = " + iEntityId;
                using (DbReader objReader = DbFactory.GetDbReader(p_objClaim.Context.DbConn.ConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iDriverRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        objPersonInvloved.DriverRowId = iDriverRowId;
                    }
                }
            }

            objPersonInvloved.EventId = p_objClaim.EventId;
            objPersonInvloved.PiEid = iEntityId;
            objPersonInvloved.PiTypeCode = objCache.GetCodeId(sPiType, "PERSON_INV_TYPE");
            objPersonInvloved.PolicyUnitRowId = iPolicyUnitRowId; // TODO: check if we need WC/GC check here
            objPersonInvloved.Save();
            objPersonInvloved.Dispose();
        }

        private void frmPolicy_Load(object sender, EventArgs e)
        {
            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
               // pnlImage.Visible = true;

                PolicySystemID = cbPolicySysID.SelectedValue.ToString();
                pnlUserInput.Visible = false;

                lblSrcPath.Text = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath", DSNConnectionString, m_iClientId)["BaseFilePath"]) + "\\" + Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath", DSNConnectionString, m_iClientId)["FileName"]);
                //pbStatus.Visible = true;
                pnlImage.Visible = true;
               
                //pbStatus.WaitOnLoad = true;
                //ReadDatafromExcel();
            }
            catch (Exception ex)
            {
                Log.Write("exception occured. Details of exception:" + ex.Message, m_iClientId);
                rtbStatus.AppendText("Unable to read data from excel.. Please check log files.");
                return;
            }
            //ReadDatafromCSV();

            /* To be added later 
            // based on some setting, check which method to call
            if (true)
            {
                ReadDatafromCSV();
            }
            else
            {
                ReadDatafromSupplemental();
            }
             * */
        }

        private void ReadDatafromExcel()
       {
           string sFilePath =string.Empty, sFileNm = string.Empty, ExcelConnectionString= string.Empty, sCompletePath=string.Empty, sSheetNm=string.Empty;
           int iRecCount = 1;
           bool bProcessAll = false;
           int iLength = Int32.MinValue;

           OleDbCommand objCommand = null;
           OleDbDataReader objReader = null;


           if (RMConfigurationManager.GetDictionarySectionSettings("FilePath", DSNConnectionString, m_iClientId) != null)
           {
               sFileNm = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath", DSNConnectionString, m_iClientId)["FileName"]);
               sFilePath = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath", DSNConnectionString, m_iClientId)["BaseFilePath"]);
               sSheetNm = ConfigurationSettings.AppSettings["SheetName"];
           }
           if (sFilePath == string.Empty)
           {

               Log.Write("File Path value is not specified in the app.config file. Aborting the process.", m_iClientId);
               
               rtbStatus.Text = "File Path value is not specified in the app.config file. Aborting the process.";
               return;
           }
           if (sFileNm == string.Empty)
           {
               Log.Write("FileName is not present in the app.config file. Aborting the process.", m_iClientId);
               rtbStatus.AppendText ("\n FileName is not present in the app.config file. Aborting the process.");
               return;
           }


           Log.Write("Trying to read data from excel file specified...", m_iClientId);

           sCompletePath = sFilePath + "\\" + sFileNm;

           ExcelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + sCompletePath + "';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'"; 
           using (OleDbConnection connection = new OleDbConnection(ExcelConnectionString))
           {
               //objCommand = new OleDbCommand("SELECT * from [Sheet1$]", connection);
               objCommand = new OleDbCommand("SELECT * from ["+sSheetNm+"$]", connection);

               try
               {
                   connection.Open();
               }
               catch (Exception ex)
               {
                   Log.Write("Exception occured while trying to read data from the file. Aborting further process.", m_iClientId);
                   Log.Write("Check that the excel file is not open. The Work Sheet name is correct in app.config.", m_iClientId);
                   rtbStatus.AppendText("\n Exception occured while trying to read data from the file. Aborting further process.");
                   Log.Write("exception detials: " + ex.Message, m_iClientId);
                   return;
               }

               Log.Write("Connection established successfully to read data from file", m_iClientId);

               // Create DbDataReader to Data Worksheet
               objReader = objCommand.ExecuteReader();

               
               // logic to read or skip records start
               //string []sRangeList;

               int iTemp = Int32.MinValue;
               int.TryParse(txtLowerIndex.Text,out iTemp);
               LowerIndex = iTemp;

               int.TryParse(txtUpperIndex.Text, out iTemp);
               UpperIndex = iTemp;


               if (UpperIndex == 0 && LowerIndex == 0)
               {
                   bProcessAll = true;
               }

               /*
               Range = txtRange.Text.Trim();
               if (Range.Trim() == string.Empty)
               {
                   rtbStatus.AppendText("\n Range cannot be empty. Please check again.");
                   return;
               }

               if (String.Compare(Range, "All", true) == 0)
               {
                   bProcessAll = true;
                   // process all records, no logic required.
               }
               else
               {
                   sRangeList = Range.Split('-');

                   iLowerIndex = Conversion.ConvertStrToInteger( sRangeList[0].Trim());
                   iUpperIndex = Conversion.ConvertStrToInteger(sRangeList[1].Trim());
               }
               */
               // logic end 

               if (bProcessAll)
               {
                   OleDbCommand objCommand2 = new OleDbCommand("SELECT count(*) from [" + sSheetNm + "$]", connection);

                   frmPolicy.SetOverallProgressBarProperties(Conversion.ConvertObjToInt(objCommand2.ExecuteScalar(), m_iClientId));
                   if (objCommand2 != null)
                   {
                       objCommand2.Dispose();
                       objCommand2 = null;
                   }
               }
               else
               {
                   frmPolicy.SetOverallProgressBarProperties(iUpperIndex);
               }
               while (objReader.Read())
               {
                   
                   if (!bProcessAll && iUpperIndex < iRecCount)
                   {
                       rtbStatus.AppendText(" \n Required number of records have been processed.");
                       // the required no of records have been processed so exit the loop.
                       Log.Write("Required no of records have been processed. Record Count = " + iRecCount + " and Upper Index = " + iUpperIndex, m_iClientId);
                       break;
                   }

                   if (!bProcessAll && iLowerIndex > iRecCount)
                   {
                       Log.Write("Skipping this record since it is before the begining range of records. Record Count = " + iRecCount + " and Lower Index = " + iLowerIndex, m_iClientId);
                       iRecCount++;
                       continue;

                   }
                   if (rtbStatus.Text == string.Empty)
                   {
                       //rtbStatus.AppendText(String.Format("Reading Record = {0}", iRecCount));
                       frmPolicy.UpdateScriptName(String.Format("Reading Record = {0}", iRecCount));
                       frmPolicy.UpdateOverallProgressBar(iRecCount);
                   }
                   else
                   {
                       //rtbStatus.AppendText(String.Format("\n \n Reading Record = {0}", iRecCount));
                       frmPolicy.UpdateScriptName(String.Format("\n \n Reading Record = {0}", iRecCount));
                       frmPolicy.UpdateOverallProgressBar(iRecCount);
                   }
                   Log.Write(String.Format("Reading Record ={0}", iRecCount), m_iClientId);
                    //frmPolicy.UpdateScriptName("\n reading record new " + iRecCount);
                    ClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    PolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue(1)).PadLeft(7, '0'); ;
                    PolicySymbol = Conversion.ConvertObjToStr(objReader.GetValue(2));
                    MasterCompany = Conversion.ConvertObjToStr(objReader.GetValue(3)).PadLeft(2, '0'); ;
                    LocationCompny = Conversion.ConvertObjToStr(objReader.GetValue(4)).PadLeft(2,'0');
                    Module = Conversion.ConvertObjToStr(objReader.GetValue(5)).PadLeft(2,'0');
                    LOBCd = Conversion.ConvertObjToStr(objReader.GetValue(6));

                    StatUnitNo = Conversion.ConvertObjToStr(objReader.GetValue(10));
                    RiskLoc = Conversion.ConvertObjToStr(objReader.GetValue(8));
                    RiskSubLoc = Conversion.ConvertObjToStr(objReader.GetValue(9));
                    UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(7));


                   // ThreadInitiate("Hello");
                    if (StatUnitNo.Trim() == string.Empty && RiskSubLoc.Trim()==string.Empty && RiskSubLoc.Trim()==string.Empty && UnitNumber.Trim()==string.Empty) // case of any lob policy which requires to download all units
                    {
                        UnitNumber = string.Empty;
                        RiskLoc = string.Empty;
                        RiskSubLoc = string.Empty;
                        StatUnitNo = string.Empty;
                    }
                    //else
                    //{
                    //    iLength = StatUnitNo.Length;
                    //    string[] sList = StatUnitNo.Split('|');
                    //    bool bUseStatNo = false;
                    //    foreach (string sItem in sList)
                    //    {
                    //        if (sItem != "00000")
                    //        {
                    //            bUseStatNo = true;
                    //            break;
                    //        }
                    //    }

                    //    if (bUseStatNo) // case of WC policy or manual rated policy
                    //    {
                    //        UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(10));  // read value of column StatUnitNumber 
                    //        RiskLoc = string.Empty;
                    //        RiskSubLoc = string.Empty;
                    //    }
                    //    else // case of any other lob except WC or manual rated..
                    //    {
                    //        UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(7));     // read value of column ALRUnitNumber
                    //        RiskLoc = Conversion.ConvertObjToStr(objReader.GetValue(8));
                    //        RiskSubLoc = Conversion.ConvertObjToStr(objReader.GetValue(9));
                    //    }
                    //}
                    

                    

                   /* not required since we have 0's appended already...
                   if(UnitNumber.Trim()!=string.Empty && UnitNumber.IndexOf('|')==-1) // i.e Unit number = 00001; due to excel will be read as 1 so add 0's to it
                   {
                       UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(7)).PadLeft(5,'0');
                   }

                   if (RiskLoc.Trim() != string.Empty && RiskLoc.IndexOf('|') == -1) // i.e risk loc = 00001; due to excel will be read as 1 so add 0's to it
                   {
                       RiskLoc = Conversion.ConvertObjToStr(objReader.GetValue(8)).PadLeft(5, '0');
                   }

                   if (RiskSubLoc.Trim() != string.Empty && RiskSubLoc.IndexOf('|') == -1)// i.e risk sub loc = 00001; due to excel will be read as 1 so add 0's to it
                   {
                       RiskSubLoc = Conversion.ConvertObjToStr(objReader.GetValue(9)).PadLeft(5, '0');
                   }
                    * */ 


                   Log.Write(string.Format("Processing record with details: Claim Number={0} ; Policy Number={1} ; Policy Symbol={2} ; Master Company={3}"+
                        "Location Company={4} ; Module={5} ; LobCd={6} ; UnitNumber={7} ; RiskLoc={8} ; RiskSubLoc={9} ; StatUnitNumber={10}",ClaimNumber,PolicyNumber,PolicySymbol,MasterCompany,
                        LocationCompny,Module,LOBCd,UnitNumber,RiskLoc,RiskSubLoc,StatUnitNo), m_iClientId);

                   // get claim id from claim number
                    ClaimId = GetClaimId(ClaimNumber);
                    if (ClaimId == Int32.MinValue)
                    {
                        //claim id not found in DB; skip this record for further processing
                        Log.Write(String.Format("Claim Id does not exist for Claim Number={0}. Skipping this record for further processing.",sClaimNumber), m_iClientId);
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure: Claim ID does not exist for this claim number = "+ClaimNumber, "RecordFailedLog", m_iClientId);
                        rtbStatus.AppendText(String.Format("\n Claim Id does not exist for Claim Number={0}. Skipping this record for further processing.",sClaimNumber));
                        iRecCount++;
                        continue;
                    }
                   // search and download policy
                    //ThreadInitiate("Hello2");
                    SavePolicyData();
                    //ThreadInitiate("Hello3");
                    if (PolicyId == 0)
                    {
                        // policy did not got inserted, need to move to next record and skip further processing for this record.
                        Log.Write("Policy Id value is zero. Either policy search has failed or policy could not be saved. Please see service logs for more information.", m_iClientId);
                        Log.Write("Skipping this record for further processing.", m_iClientId);
                        rtbStatus.AppendText("\n Error occured while trying to save policy informtaion. Skipping this record for further processing.");
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure:Policy Id value is zero. Either policy search has failed or policy could not be saved. Please see service logs for more information", "RecordFailedLog", m_iClientId); 
                        iRecCount++;
                        continue;
                    }
                    Log.Write(String.Format("Policy ID generated successfully. Claim number ={0} ; claim id ={1} ; Policy ID ={2}", sClaimNumber, ClaimId, PolicyId), m_iClientId);

                   
                    rtbStatus.AppendText("\n Policy id has been generated successfully. Processing details data related to policy.");
                   
                    Log.Write("Trying to get Base LOB value for the record...", m_iClientId);
                    if (!GetBaseLOB())
                    {
                        Log.Write("Unable to get Base LOB value for this record.. Skipping this record for further processing. ", m_iClientId);
                        rtbStatus.AppendText("\n Unable to get Base LOB value for this record.. Skipping this record for further processing. ");
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure: Unable to get Base lOB value.", "RecordFailedLog", m_iClientId);
                        iRecCount++;
                        continue;
                    }
                    Log.Write(String.Format("Base LOB value found successfully for this record. Base LOB ={0}", BaseLOB), m_iClientId);
                    rtbStatus.AppendText(String.Format("\n Base LOB value found successfully for this record. Base LOB ={0}", BaseLOB));
                   
                   // set entity role
                    GetEntityRole();
                    //ThreadInitiate("Hello4");
                    // save the remaining data here.. call another method of service..
                    //frmPolicy.UpdateScriptName("Before service hit for details");
                    if (!SavePolicyRelatedData())
                    {
                        //Log.Write(String.Format("Policy related data was not saved properly for policy id ={0}", PolicyId.ToString()));
                        //rtbStatus.AppendText(String.Format("\n Error occured while trying to save data for policy. Skipping this record for further processing.. "));

                        Log.Write(String.Format("Error occured while trying to download policy objects(units/driver/interest details). Please see service logs for more information."), m_iClientId);
                        Log.Write("Skipping this record for further processing", m_iClientId);
                        rtbStatus.AppendText(String.Format("\n Error occured while trying to download policy objects(units/driver/interest details). Skipping this record for further processing.. "));

                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure: error while trying to download policy objects(units/driver/interest details)", "RecordFailedLog", m_iClientId);
                        // skip this record for further processing...
                        iRecCount++;
                        continue;
                    }

                    Log.Write("Policy objects(units/driver/interest details) have been downloaded sucessfully.", m_iClientId);
                    rtbStatus.AppendText("\n Policy objects(units/driver/interest details) have been downloaded sucessfully.");


                    Log.Write(String.Format("Trying to attach policy to the claim... Policy Id={0} and Claim Id={1}", PolicyId, ClaimId), m_iClientId);
                    // attach policy to claim; 
                    if (!AttachPolicyToClaim())
                    {
                        Log.Write(String.Format("Error occured while trying to insert data into CLAIM_X_POLICY for ClaimId={0} and PolicyId={1}", ClaimId.ToString(), PolicyId.ToString()), m_iClientId);
                        rtbStatus.AppendText("\n Unable to attach the generated policy id to claim. Skipping this record for further processing.");
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure: Error while trying to insert data into CLAIM_X_POLICY", "RecordFailedLog", m_iClientId);
                        //skip this record for further processing
                        iRecCount++;
                        continue;
                    }
                    
                    //add entities to person involved; add units to claim
                    try
                    {
                        Log.Write(String.Format("Trying to attach external entities to claim for claim id = {0}...", ClaimId), m_iClientId);
                        AddExternalPolicyEntities();
                    }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format("Exception occured while trying to attach external entities to claim for claim id = {0}...", ClaimId), m_iClientId);
                        Log.Write("exception details: " + ex.Message, m_iClientId);
                        rtbStatus.AppendText("\n Unable to attach the external entities of policy with claim. Skipping this record for further processing.");
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure: Exception occured while trying to attach external entities to claim. exception details= " + ex.Message, "RecordFailedLog", m_iClientId);
                      
                        // skip this record for further processing..
                        iRecCount++;
                        continue;
                    }

                    Log.Write("External entities of policy attached successfully with Claim.", m_iClientId);
                    rtbStatus.AppendText("\n External entities of policy attached successfully with Claim");

                    //frmPolicy.UpdateScriptName("attch insured to claim");
                    Log.Write("Trying to attach insured info with Claim", m_iClientId);
                    rtbStatus.AppendText("\n Trying to attach insured info with Claim");
                    try
                    {
                        if (BaseLOB == "WL")
                        {
                            AddExternalPolicyInsureds(true, PolicyId.ToString());
                        }
                        else
                        {
                            AddExternalPolicyInsureds(false, PolicyId.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write("Exception ocuured while trying to attach Insured info with Claim.", m_iClientId);
                        Log.Write("Exception Details:" + ex.Message, m_iClientId);
                        rtbStatus.AppendText("\n Error occured while trying to attach Insured info with Claim. Skipping this record for further processing.");
                        Log.Write(String.Format("Skipping  Record count = {0} with Claim Number = {1} for further processing.", iRecCount, ClaimNumber), "RecordFailedLog", m_iClientId);
                        Log.Write("Reason of failure:Exception ocuured while trying to attach Insured info with Claim. Exception details:" + ex.Message, "RecordFailedLog", m_iClientId);
                        iRecCount++; 
                        continue;
                    }

                    Log.Write(String.Format("Record {0} processing completed successfully!!", iRecCount), m_iClientId);
                    Log.Write(String.Format("Claim Number = {0} processed succeessfully. Policy ID generated = {1}", ClaimNumber, PolicyId), "RecordCompletedLog", m_iClientId);
                   // rtbStatus.AppendText(String.Format(" \n Record {0} processing completed successfully!!", iRecCount));
                    rtbStatus.AppendText(String.Format("\n Claim Number = {0} processed successfully.", ClaimNumber));
                   iRecCount++;

               }

               rtbStatus.AppendText("\n\n End of File reached.");
               rtbStatus.SelectionStart = rtbStatus.Text.Length;
               rtbStatus.ScrollToCaret();
               Log.Write("End of File reached.", m_iClientId);

           }
          
            
       }

        private void btnStartProcessing_Click(object sender, EventArgs e)
        {
            int iResult = Int32.MinValue;

            if (txtLowerIndex.Text.Trim() != string.Empty)
            {
                if (!int.TryParse(txtLowerIndex.Text, out iResult) || txtLowerIndex.Text.Trim() == "0")
                {
                    lblError.Text = " Please enter a non zero numeric value.";
                    txtLowerIndex.Focus();
                    return;
                }
            }
            LowerIndex = iResult;

            if (txtUpperIndex.Text.Trim() != string.Empty)
            {
                if (!int.TryParse(txtUpperIndex.Text, out iResult) || txtUpperIndex.Text.Trim() == "0")
                {
                    lblError.Text = " Please enter a non zero numeric value.";
                    txtUpperIndex.Focus();
                    return;
                }
            }
            UpperIndex = iResult;
            if (LowerIndex > UpperIndex)
            {
                lblError.Text = "From Value cannot be greater than To value.";
                txtLowerIndex.Focus();
                return;
            }

            lblError.Text = "";
            btnStartProcessing.Enabled = false;
            txtLowerIndex.ReadOnly = true;
            txtUpperIndex.ReadOnly = true;

            //pbStatus.Visible = true;
            pnlDownload.Visible = true;
            
            ReadDatafromExcel();

            // after all processing done, no need to show the image
            //pbStatus.Visible = false;
        }
        /*
        private void ThreadInitiate(string Text)
        {
            Thread t1 = null;
            ParameterizedThreadStart del = new ParameterizedThreadStart(WriteToUI);
            //del.Method.GetParameters(
                t1 = new Thread(del);
                t1.Name = "Thread1";
           
           
                t1.Start(Text);
        }
        private void WriteToUI(object Text)
        {
            //richTextBox1.Text = "hello!!";
            richTextBox1.Invoke(new new_WriteTOUI_delegate(new_WriteToUI),Text);
            //Delegate new_WriteTOUI_delegate = new new_WriteTOUI_delegate(new_WriteToUI);
            //new_WriteTOUI_delegate.
        }
        private delegate void new_WriteTOUI_delegate(object Text);
        private void new_WriteToUI(object Text)
        {
            richTextBox1.Text = (string)Text;
        }
        */
        public static void UpdateScriptName(string sScriptName)
        {
            //frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;
            frmPolicy myForm = Application.OpenForms["frmPolicy"] as frmPolicy;

            if (myForm != null)
            {
                //if (sScriptName.Length > 33)
                //{
                //    myForm.lblProcessingScript.Text = "Processing script: <font size='-4' color='#000000'>" + sScriptName + " </font>";
                //}
                //else if (sScriptName.Length > 30)
                //{
                //    myForm.lblProcessingScript.Text = "Processing script: <font size='-3' color='#000000'>" + sScriptName + " </font>";
                //}
                //else if (sScriptName.Length > 25)
                //{
                //    myForm.lblProcessingScript.Text = "Processing script: <font size='-2' color='#000000'>" + sScriptName + " </font>";
                //}
                //else
                //{
                //    myForm.lblProcessingScript.Text = "Processing script: <font color='#000000'>" + sScriptName + " </font>";
                //}
                myForm.rtbStatus.AppendText(sScriptName);
                myForm.rtbStatus.SelectionStart = myForm.rtbStatus.Text.Length;
                myForm.rtbStatus.ScrollToCaret();

                Application.DoEvents();
            }
        }

        public static void UpdateOverallProgressBar(int iOverall)
        {
            frmPolicy myForm = Application.OpenForms["frmPolicy"] as frmPolicy;

            if (myForm != null)
            {
                myForm.pbOverall.Value = iOverall;
                Application.DoEvents();
            }
        }

        public static void SetOverallProgressBarProperties(int iOverallMaximum)
        {
            frmPolicy myForm = Application.OpenForms["frmPolicy"] as frmPolicy;

            if (myForm != null)
            {
                myForm.pbOverall.Maximum = iOverallMaximum;
                Application.DoEvents();
            }
        }

        /* CSV logic to be used later
        private void ReadDatafromCSV()
        {
            pnlDownload.Visible = true;
            pnlUserInput.Visible = false;
            string sFilePath, sFileNm;
            int iRecCount = 1;
            OleDbCommand objCommand;
            OleDbDataReader objReader;
            // KeyValuePair<int, string> oListItem = new KeyValuePair<int, string>();

            sFileNm = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath")["FileName"]);
            sFilePath = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("FilePath")["BaseFilePath"]);// @"C:\Users\aaggarwal29\Desktop";
            if (sFilePath == string.Empty)
            {
                // lblStatusMessage.Text = String.Format("File Does not exist at path {0}. Aborting the process.", sFilePath);
                Log.Write(String.Format("File Does not exist at path {0}. Aborting the process.", sFilePath));
                return;
            }

            string strExcelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + sFilePath + "';Extended Properties='text; HDR=Yes; FMT=Delimited'";
            using (OleDbConnection objConnection = new OleDbConnection(strExcelConnectionString))
            {

                //objCommand = new OleDbCommand("sELECT * from [Sample.csv]", objConnection);
                objCommand = new OleDbCommand("sELECT * from [" + sFileNm + "]", objConnection);
                try
                {
                    objConnection.Open();
                }
                catch (Exception ex)
                {
                    Log.Write(String.Format("Exception occured while trying to read data from file at path {0}. Please check file format and contents.", sFilePath));
                    Log.Write(String.Format("Exception Details: {0}", ex.ToString()));
                }


                // Create DbDataReader to Data Worksheet
                objReader = objCommand.ExecuteReader();
                while (objReader.Read())
                {
                    Log.Write(string.Format("Reading record  {0} .........", iRecCount.ToString()));


                    ClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    PolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue(1)).PadLeft(7, '0'); ;
                    PolicySymbol = Conversion.ConvertObjToStr(objReader.GetValue(2));
                    MasterCompany = Conversion.ConvertObjToStr(objReader.GetValue(3)).PadLeft(2, '0'); ;
                    LocationCompny = Conversion.ConvertObjToStr(objReader.GetValue(4)).PadLeft(2, '0');
                    Module = Conversion.ConvertObjToStr(objReader.GetValue(5)).PadLeft(2, '0');
                    LOBCd = Conversion.ConvertObjToStr(objReader.GetValue(6));
                    UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(7));
                    RiskLoc = Conversion.ConvertObjToStr(objReader.GetValue(8));
                    RiskSubLoc = Conversion.ConvertObjToStr(objReader.GetValue(9));

                    // need to test here begin
                    //if (UnitNumber.Trim() != string.Empty && UnitNumber.IndexOf('|') == 0) // i.e Unit number = 00001; due to excel will be read as 1 so add 0's to it
                    //{
                    //    UnitNumber = Conversion.ConvertObjToStr(objReader.GetValue(7)).PadLeft(5, '0');
                    //    RiskLoc = Conversion.ConvertObjToStr(objReader.GetValue(8)).PadLeft(5, '0');
                    //    RiskSubLoc = Conversion.ConvertObjToStr(objReader.GetValue(9)).PadLeft(5, '0');
                    //}
                    // need to test here end..

                    Log.Write(string.Format("Processing record with details: Claim Number={0} ; Policy Number={1} ; Policy Symbol={2} ; Master Company={3}" +
                        "Location Company={4} ; Module={5} ; LobCd={6} ; UnitNumber={7} ; RiskLoc={8} ; RiskSubLoc={9}", ClaimNumber, PolicyNumber, PolicySymbol, MasterCompany,
                        LocationCompny, Module, LOBCd, UnitNumber, RiskLoc, RiskSubLoc));



                    // get claim id from claim number
                    ClaimId = GetClaimId(ClaimNumber);
                    if (ClaimId == Int32.MinValue)
                    {
                        //claim id not found in DB; skip this record for further processing
                        Log.Write(String.Format("Claim Id does not exist for Claim Number={0}. Skipping this record for further processing.", sClaimNumber));
                        iRecCount++;
                        continue;
                    }
                    // search and download policy
                    SavePolicyData();
                    if (PolicyId == 0)
                    {
                        // policy did not got inserted, need to move to next record and skip further processing for this record.
                        Log.Write(String.Format("Record has not been inserted in POLICY table properly. Skipping this record for further processing.", ""));
                        iRecCount++;
                        continue;
                    }
                    Log.Write(String.Format("Policy ID inserted successfully. Claim number ={0} ; claim id ={1} ; Policy ID ={2}", sClaimNumber, ClaimId, PolicyId));

                    Log.Write("Trying to get Base LOB value for the record...");
                    if (!GetBaseLOB())
                    {
                        Log.Write("Unable to set Base LOB value for this record.. Skipping this record for further processing. ");
                        iRecCount++;
                        continue;
                    }
                    Log.Write(String.Format("Base LOB value set successfully for this record. Base LOB ={0}", BaseLOB));

                    // save the remaining data here.. call another method of service..
                    if (!SavePolicyRelatedData())
                    {
                        Log.Write(String.Format("Policy related data was not saved properly for policy id ={0}", PolicyId.ToString()));
                        // skip this record for further processing...
                        iRecCount++;
                        continue;
                    }

                    // attach policy to claim; 
                    if (!AttachPolicyToClaim())
                    {
                        Log.Write(String.Format("Error occured while trying to insert data into CLAIM_X_POLICY for ClaimId={0} and PolicyId={1}", ClaimId.ToString(), PolicyId.ToString()));
                        //skip this record for further processing
                        iRecCount++;
                        continue;
                    }

                    //add entities to person involved; add units to claim
                    AddExternalPolicyEntities();
                    if (BaseLOB == "WL")
                    {
                        AddExternalPolicyInsureds(true, PolicyId.ToString());
                    }
                    else
                    {
                        AddExternalPolicyInsureds(false, PolicyId.ToString());
                    }
                    iRecCount++;
                }
            }
        }
         * 
         * 
         *  */

        /*  Supplemental Logic to be used later
   private void ReadDatafromSupplemental()
   {
       string sSQL = string.Empty;
       DbConnection oConn = null;
       DbReader oReader = null;
       string sPolicyNumber;

       sSQL = "SELECT * FROM CLAIM_SUPP ";
       oConn.ConnectionString = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
       oConn.Open();
       oReader = oConn.ExecuteReader(sSQL);

       // read records one by one from claim_supp table.
       while (oReader.Read())
       {
           // set values here like below..
           sPolicyNumber =Conversion.ConvertObjToStr(  oReader["COLUMN_NAME"]  );

           // get claim id from claim number
           sClaimID = GetClaimId(sClaimNumber).ToString();

           // call policy save method then..
           SavePolicyData();
           if (iPolicyId == 0)
           {
               // policy did not got inserted, need to move to next record and skip further processing for this record.
               continue;
           }
       }
            
            

   }
   */

        /*  To get connection string; not required now
         * private void SetDSNConnectionString(string pDSNId)
        {
            string sSQL = "SELECT CONNECTION_STRING FROM DATA_SOURCE_TABLE WHERE DSNID=" + pDSNId;
            string sSecurityConnectionString = Riskmaster.Security.SecurityDatabase.Dsn;
            using (DbReader oDbReader = DbFactory.GetDbReader(sSecurityConnectionString, sSQL))
            {
                while (oDbReader.Read())
                {
                    DSNConnectionString = oDbReader["CONNECTION_STRING"].ToString();
                }
            }
            

        }
         */
    }
}

