﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoMailMergeScheduler
{
    /// <summary>
    ///Author  :   Nitika Gupta
    ///Dated   :   01,Nov 2012
    ///Purpose :   Contains Auto mail Merge support classes/structures
    /// </summary>
    internal struct FilterDefinition
    {
        public int TemplateId;
        public string Name;
        public int ID;
        public int FilterType;
        public long FilterMin;
        public long FilterMax;
        public string DefValue;
        public string table;
        public string SQLFill;
        public string SQLFrom;
        public string SQLWhere;
        public string Database;
        public FilterDefinition[] FilterDef;
    }

    /// <summary>
    /// Struct InfoDefinition
    /// </summary>
    internal struct InfoDefinition
    {
        public string Name;
        public int ID;
        public string AttachTable;
        public string ParentTable;
        public string AttachCol;
        public string AttachDesc;
        public int AttFormCode;
        public string AttSecRecID;
        public string tmpSQL;
        public string SQL;
        public string SQLRegard;
        public int NumFilters;
        public FilterDefinition[] FilterDef;
    }

    /// <summary>
    /// Struct FilterSetting
    /// </summary>
    internal struct FilterSetting
    {
        public string Name;
        public int Number;
        public object Data;
    }

    /// <summary>
    /// Struct FilterSetting
    /// </summary>
    internal struct TemplateFilterSetting
    {
        public string CategoryName;
        public int Number;
        public object Data;
    }

    /// <summary>
    /// Struct InfoSetting
    /// </summary>
    internal struct InfoSetting
    {
        public int RowID;
        public string Name;
        public string DefName;
        public int Index;
        public string ProcessDate;
        public int TemplateID;
        public string TemplateName;
        public string PrinterName;
        public string PaperBin;
        public int NumFilters;
        public int TempNumFilters;
        public string PersonInvolvedValue;
        public string AdjusterValue;
        public string CasemanagerValue;
        public int UnitInvolvedValue;
        public string DefendantValue;
        public string ClaimantValue;
        public string ExpertWitnessValue;
        public string LastRun;



        public int SendCreate;           //SEND_CREATED
        public int SendUpdate;           //SEND_UPDATED
        public int SendAdj;              //SEND_ADJUSTER
        //Start Naresh MITS 8349 07/11/2006
        public int SendAdjSupervisor;	 //SEND_ADJUSTER_SUPERVISOR
        //End Naresh MITS 8349 07/11/2006
        public int NotifyDays;           //NOTIFICATION_DAYS
        public int RouteDays;            //ROUTE_DAYS
        public int Priority;             //PRIORITY
        public int TaskEst;              //TASK_ESTIMATE
        public int TaskEstType;          //TASK_EST_TYPE
        public int TimeBillable;         //TIME_BILLABLE_FLAG
        public int ExpSchedule;          //EXPORT_SCHEDULE
        public string Instruct;          //INSTRUCTIONS


        public string Activities;
        public string SendUsers;
        public string NotifyUsers;
        public string RouteUsers;

        public FilterSetting[] FilterSet;
        public TemplateFilterSetting[] TemplateFilterSet;

    }
    class AutoMail
    {
    }
}
