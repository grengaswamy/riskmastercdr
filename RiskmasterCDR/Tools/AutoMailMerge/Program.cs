using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using Riskmaster.Security;
using Riskmaster.Application.EnhancePolicy.Billing;
using Riskmaster.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;
using System.IO;


namespace AutoMailMergeScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
             UserLogin oUserLogin = null;
             int iClientId = 0;//Add & Change by rkaur27 for Cloud.
             string sTempClientId = string.Empty;
             bool blnSuccess = false;
             //args = new string[5];
             //args[0] = "rmACloud";
             //args[1] = "cloud";
             //args[2] = "fa6db697515ef18b";
       
             try
             {
                 string Tracelogfilename = string.Empty;
                 string sJobId = string.Empty;
                 //Anu Tennyson : Added date time stamp : This process log is generated every time the exe is executed. Starts
                 //This is a common file that is created for all the criteria that we have set up.
                 Tracelogfilename = "Processlog" + "_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
                 //Ends

                 
                 string TracelogfilePath = string.Empty;
                 string sBasePath = RMConfigurator.BasePath;
                 string sTempFolderPath = string.Empty;

                 //int iWCFService = sBasePath.IndexOf("WCFService");
                 //sTempFolderPath = sBasePath.Substring(0, iWCFService) + "\\UI\\riskmaster\\temp\\AutoMailMerge\\";
                 sTempFolderPath = sBasePath + "\\temp\\AutoMailMerge\\";

                 if (!System.IO.Directory.Exists(sTempFolderPath))
                 {
                     System.IO.Directory.CreateDirectory(sTempFolderPath);
                 }

                 Constants.sFilePath = sTempFolderPath;
                 //ErrorLog.sErrorFilePath = sBasePath.Substring(0, iWCFService) + "\\UI\\riskmaster\\temp\\AutoMailMerge\\";
                 ErrorLog.sErrorFilePath = sBasePath + "\\temp\\AutoMailMerge\\";
                 TracelogfilePath = sTempFolderPath + Tracelogfilename;

                

                 Console.WriteLine("0 ^*^*^ AutoMail Merge start successfully");
                 TraceLog.sb.AppendLine(DateTime.Now + " " + "AutoMail Merge start successfully");
                 TraceLog.WriteFile(TracelogfilePath, TraceLog.sb.ToString(), Tracelogfilename);


                 //Add & Change by rkaur27 for Cloud----Start
                 if (args != null && args.Length > 5 && (args[5] != null))
                 {
                     sTempClientId = args[5] ?? string.Empty;
                     iClientId = Convert.ToInt32(sTempClientId);
                 }
                 //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                 //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                 else
                 {
                     iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                 }
                 //Add & Change by rkaur27 for Cloud----End


                 if (args.Length > 0)
                 {
                     sJobId = args[3];
                     ErrorLog.sJobId = sJobId;
                     //MITS 22996 Ignore password when called from TaskManager
                     oUserLogin = new UserLogin(args[1], args[0], iClientId);//Add & Change by rkaur27 for Cloud.
                     
                     if (oUserLogin.DatabaseId <= 0)
                     {
                         Console.WriteLine("0 ^*^*^ Authentication failure.");
                         TraceLog.sb.AppendLine(DateTime.Now + " " + "Authentication failure.");
                         TraceLog.WriteFile(TracelogfilePath, TraceLog.sb.ToString(), Tracelogfilename);
                         throw new Exception("Authentication failure.");

                     }
                 }
                 else
                 {
                     Console.WriteLine("0 ^*^*^ Command line argument are missing.");
                     TraceLog.sb.AppendLine(DateTime.Now + " " + "Command line argument are missing.");
                     TraceLog.WriteFile(TracelogfilePath, TraceLog.sb.ToString(), Tracelogfilename);
                 }


                 if (oUserLogin.DocumentPath.Length > 0)
                 {
                     DataFilter ss = new DataFilter(oUserLogin.objRiskmasterDatabase.ConnectionString, (int)oUserLogin.objRiskmasterDatabase.DbType, oUserLogin.DocumentPath, 0, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.LoginName, oUserLogin.LoginName, oUserLogin, Tracelogfilename, TracelogfilePath, sJobId, iClientId);//rkaur27
                     ss.Data();
                 }
                 else
                 {
                     DataFilter ss = new DataFilter(oUserLogin.objRiskmasterDatabase.ConnectionString, (int)oUserLogin.objRiskmasterDatabase.DbType, oUserLogin.objRiskmasterDatabase.GlobalDocPath, oUserLogin.objRiskmasterDatabase.DocPathType, oUserLogin.objRiskmasterDatabase.DataSourceName, oUserLogin.LoginName, oUserLogin.LoginName, oUserLogin, Tracelogfilename, TracelogfilePath, sJobId, iClientId);//rkaur27
                     ss.Data();
                 }

                 Console.WriteLine("0 ^*^*^AutoMail Merge executed successfully");
                 TraceLog.sb.AppendLine(DateTime.Now + " " + "AutoMail Merge executed successfully");
                 TraceLog.WriteFile(TracelogfilePath, TraceLog.sb.ToString(), Tracelogfilename);

             }
             catch (Exception exc)
             {
                 Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                 //Log.Write(exc.Message); 
                 ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
             }
             finally
             {
                 //ErrorLog.WriteErrorFile();
             }
        }
    }
}
