﻿using System;
using System.Windows.Forms;
using System.Configuration;

namespace Riskmaster.PrintCheck
{
    public partial class frmChangeSettings : Form
    {
        public frmChangeSettings()
        {
            InitializeComponent();
            lblCopyright.Text = "©" + " " + DateTime.Now.Year + " Computer Sciences Corporation. All rights reserved.";
        }
        private void frmSettings_Click(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (!txtUid.Text.Trim().Equals(""))
            {
                config.AppSettings.Settings.Remove("Uid");
                config.AppSettings.Settings.Add("Uid", Riskmaster.Security.Encryption.RMCryptography.EncryptString(txtUid.Text));
            }
            if (!txtPwd.Text.Trim().Equals(""))
            {
                config.AppSettings.Settings.Remove("Pwd");
                config.AppSettings.Settings.Add("Pwd", Riskmaster.Security.Encryption.RMCryptography.EncryptString(txtPwd.Text));
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            this.Close();
        }
    }
}
