﻿using System;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Drawing.Printing;
using ceTe.DynamicPDF.Printing;
using System.Configuration;
using PrintCheck.CommonWCF;
using PrintCheck.AuthService;
namespace Riskmaster.PrintCheck
{

    public partial class frmPrintCheck : Form
    {
        //Print Job id
        private string JobId = "";
        //Print Job DataSource
        private string DSN = "";
        //Message template for Authorization
        private string MessageTemplate = "<Message><Authorization></Authorization><Call>" +
            "<Function>PrintChecksAdaptor.GetPrintJob</Function></Call><Document><PrintChecks><JobId></JobId></PrintChecks></Document></Message>";
        //Message template for Print check
        private string FileRequestMessageTemplate = "<Message><Authorization></Authorization><Call>" +
      "<Function>PrintChecksAdaptor.GetPrintCheckFile</Function></Call><Document><PrintChecks><FileName/><FileType/></PrintChecks></Document></Message>";
        public frmPrintCheck()
        {
            InitializeComponent();
        }
        //Initial form load hnndler
        public frmPrintCheck(string sJobId,string sDSN)
        {
            InitializeComponent();
            lblCopyright.Text = "©" + " " + DateTime.Now.Year + " Computer Sciences Corporation. All rights reserved.";
            JobId = sJobId;
            DSN = sDSN;
            lblCheckStatusMsg.Text = "";
            lblCheckEobStatusMsg.Text = "";
            if (sJobId == "-1")
            {
                
                btnPrint.Visible = false;
                pbOverall.Visible = false;
            }
            else
            {
                
                btnPrint.Visible = true;
            }
        }
        private void frmPrintCheck_Load(object sender, EventArgs e)
        {
            try
            {
                //on load of form Delete all existing pdfs.
                string[] filePaths = Directory.GetFiles(@".", "*.pdf");
                foreach (string filePath in filePaths)
                    File.Delete(filePath);
                PrintDocument oPrintDocument = new PrintDocument();
                //get all installed printers
                foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {

                    ComboboxItem item = new ComboboxItem();

                    item.Text = printer;
                    item.Value = printer;
                    cmbPrinters.Items.Add(item);

                }
                //get user prefrence at startup
                string[] s = GetStoredPref();
                if (s != null)
                {
                    if (s[0] != null)
                        cmbPrinters.SelectedIndex = Convert.ToInt32(s[0]);
                    if (s[1] != null)
                        cmbCheckBin.SelectedIndex = Convert.ToInt32(s[1]);
                    if (s[2] != null)
                        cmbEOBBin.SelectedIndex = Convert.ToInt32(s[2]);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
        }
        //function to handle printer combo change event
        private void cmbPrinters_SelectedIndexChanged(object sender, EventArgs e)
        {
            PrintDocument oPrintDocument = new PrintDocument();

            while (cmbCheckBin.Items.Count > 0)
            {
                cmbCheckBin.Items.RemoveAt(0);
            }
            while (cmbEOBBin.Items.Count > 0)
            {
                cmbEOBBin.Items.RemoveAt(0);
            }

            oPrintDocument.PrinterSettings.PrinterName = ((ComboboxItem)cmbPrinters.SelectedItem).Value.ToString();
            foreach (System.Drawing.Printing.PaperSource oPaperSource in oPrintDocument.PrinterSettings.PaperSources)
            {

                ComboboxItem item = new ComboboxItem();

                item.Text = oPaperSource.SourceName;
                item.Value = oPaperSource.RawKind;

                cmbCheckBin.Items.Add(item);
                cmbEOBBin.Items.Add(item);


            }
            cmbCheckBin.SelectedIndex = 0;
            cmbEOBBin.SelectedIndex = 0;
        }

        // Update Progrees bar on every check print
        private void UpdateOverallProgressBar(int iOverall)
        {
            frmPrintCheck myForm = Application.OpenForms["frmPrintCheck"] as frmPrintCheck;

            if (myForm != null)
            {
                myForm.pbOverall.Value = iOverall;
                Application.DoEvents();
            }
        }
        // Set Progrees bar Overall value
        private void SetOverallProgressBarProperties(int iOverallMaximum)
        {
            frmPrintCheck myForm = Application.OpenForms["frmPrintCheck"] as frmPrintCheck;

            if (myForm != null)
            {
                myForm.pbOverall.Maximum = iOverallMaximum;
                Application.DoEvents();
            }
        }
        // Get this exe execution path
        private string GetExecutionPath()
        {
            return System.IO.Path.GetDirectoryName(Application.ExecutablePath)+"\\";
        }
        // function handles the check print job
        private void PrintCheckJob(string filename, string BinName)
        {
            PrintJob printJob = null;
            try
            {
                ceTe.DynamicPDF.Printing.PrintJob.AddLicense("PMG20NXDLGPFIAeGqB9mYQ263av9j9DzMT58X3zE3kFBh8j//PDlC9d+HqvJm5dwEi3LUmebA+afrlOpKzF8X51YS0qKWo1WY8Zg");
                string sPath = GetExecutionPath();
                //Open Password protected pdf
                string sPwd = Riskmaster.Security.Encryption.RMCryptography.DecryptString(ConfigurationManager.AppSettings["Pwd"]);
                InputPdf pdf = new InputPdf(sPath + "\\" + filename, sPwd);
                printJob = new PrintJob(((ComboboxItem)cmbPrinters.SelectedItem).Value.ToString(), pdf);
                printJob.PrintOptions.SetPaperSourceByName(BinName);
                //fire print job after setting bin & printer name
                printJob.Print();
                printJob.Dispose();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                if (printJob != null)
                    printJob.Dispose();
            }
        }
        // updates xml nodes
        private string ChangeMessageValue(string xml, string nodetochange, string nodevalue)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            if (doc.SelectSingleNode("//" + nodetochange) != null)
                doc.SelectSingleNode("//" + nodetochange).InnerText = nodevalue;
            return doc.OuterXml;

        }
        //Get user preferences
        private string[] GetStoredPref()
        {
            string[] s = null;
            string sPath = GetExecutionPath() + "userpref.txt";
            if (File.Exists(sPath))
            {
                s = File.ReadAllLines(sPath);

            }
            return s;
        }
        //store user preferences
        private void StorePref()
        {
            string sPath = GetExecutionPath() + "userpref.txt";
            if (File.Exists(sPath))
            {
                File.Delete(sPath);
            }
            StreamWriter s = File.CreateText(sPath);
            s.WriteLine(cmbPrinters.SelectedIndex.ToString());
            s.WriteLine(cmbCheckBin.SelectedIndex.ToString());
            s.WriteLine(cmbEOBBin.SelectedIndex.ToString());

            s.Dispose();

        }

        //Main function which handles the check print 
        private void btnPrint_Click(object sender, EventArgs e)
        {
            AuthenticationServiceClient auth = null;
            CommonWCFServiceClient process = null;
            try
            {
                StorePref();
                btnPrint.Enabled = false;
                lblCheckStatusMsg.Text = "Downloading checks...";
                Application.DoEvents();
                auth = new AuthenticationServiceClient();
                string sDSN = DSN;
                //Decrypt UID
                string sUID = Riskmaster.Security.Encryption.RMCryptography.DecryptString(ConfigurationManager.AppSettings["Uid"]);
                string session = auth.GetUserSessionID(sUID, sDSN);//get session- Initial handshake

                process = new CommonWCFServiceClient();
                MessageTemplate = ChangeMessageValue(MessageTemplate, "JobId", JobId);
                MessageTemplate = ChangeMessageValue(MessageTemplate, "Authorization", session);
                XmlDocument objTemp = new XmlDocument();
                //call webservice to all get files
                objTemp.LoadXml(process.ProcessRequest(MessageTemplate));

                int iFileCounter = 0;
                string sPath = GetExecutionPath() + "\\";
                //loop thru all files & save
                foreach (XmlNode nd in objTemp.SelectNodes("//File"))
                {
                    iFileCounter++;
                    string fname = nd.Attributes["Name"].Value;
                    string filetype = nd.Attributes["Type"].Value;
                    FileRequestMessageTemplate = ChangeMessageValue(FileRequestMessageTemplate, "Authorization", session);
                    FileRequestMessageTemplate = ChangeMessageValue(FileRequestMessageTemplate, "FileName", fname);
                    FileRequestMessageTemplate = ChangeMessageValue(FileRequestMessageTemplate, "FileType", filetype);
                    string sReturn = process.ProcessRequest(FileRequestMessageTemplate);
                    Application.DoEvents();
                    XmlDocument objFile = new XmlDocument();
                    objFile.LoadXml(sReturn);
                    byte[] pdfbytes = Convert.FromBase64String(objFile.SelectSingleNode("//File").InnerText);
                    if (File.Exists(sPath + "check" + iFileCounter.ToString() + ".pdf"))
                        File.Delete(sPath + "check" + iFileCounter.ToString() + ".pdf");
                    File.WriteAllBytes(sPath + "check" + iFileCounter.ToString() + ".pdf", pdfbytes);
                }

                lblCheckStatusMsg.Text = "Printing checks...";
                Application.DoEvents();
                int checkcount = (objTemp.SelectNodes("//File[@Type='check']").Count);
                SetOverallProgressBarProperties(checkcount);

                iFileCounter = 1;
                int iTotalFileCounter = 0;
                //loop thru all files & fire Print Job
                foreach (XmlNode nd in objTemp.SelectNodes("//File"))
                {
                    iTotalFileCounter++;
                    lblCheckEobStatusMsg.Text = "";
                    string sname = nd.Attributes["Name"].Value;
                    string filetype = nd.Attributes["Type"].Value;



                    if (filetype == "check")
                    {

                        PrintCheckJob("check" + iTotalFileCounter.ToString() + ".pdf", ((ComboboxItem)cmbCheckBin.SelectedItem).Text.ToString());

                        lblCheckStatusMsg.Text = "Printing check " + iFileCounter.ToString() + " of " + checkcount.ToString();
                        UpdateOverallProgressBar(iFileCounter);
                        iFileCounter++;
                    }
                    else
                    {

                        lblCheckEobStatusMsg.Text = "Printing check eob ";
                        Application.DoEvents();
                        PrintCheckJob("check" + iTotalFileCounter.ToString() + ".pdf", ((ComboboxItem)cmbEOBBin.SelectedItem).Text.ToString());

                    }


                }
                lblCheckStatusMsg.Text = "Checks printing done";
                MessageBox.Show("Finished check printing");
            }

            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
            finally
            {
                if (auth != null)
                    auth.Close();
                if (process != null)
                    process.Close();
            }
        }


        private void btnSettings_Click(object sender, EventArgs e)
        {
            frmChangeSettings form2 = new frmChangeSettings();
            form2.Visible = true;
            form2.TopMost = true;
            form2.StartPosition = FormStartPosition.CenterScreen;
            this.Activate();
        }

      







    }
    //combo box items handler class
    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}