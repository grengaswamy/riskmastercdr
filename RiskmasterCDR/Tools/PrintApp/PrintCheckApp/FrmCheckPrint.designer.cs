﻿using System.Windows.Forms;
using PrintCheck.Properties;
namespace Riskmaster.PrintCheck
{
    partial class frmPrintCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbPrinters = new System.Windows.Forms.ComboBox();
            this.cmbCheckBin = new System.Windows.Forms.ComboBox();
            this.cmbEOBBin = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pbOverall = new System.Windows.Forms.ProgressBar();
            this.lblCheckStatusMsg = new System.Windows.Forms.Label();
            this.lblCheckEobStatusMsg = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCopyright = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Printers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Check Bin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "EOB Bin";
            // 
            // cmbPrinters
            // 
            this.cmbPrinters.FormattingEnabled = true;
            this.cmbPrinters.Location = new System.Drawing.Point(166, 66);
            this.cmbPrinters.Name = "cmbPrinters";
            this.cmbPrinters.Size = new System.Drawing.Size(188, 21);
            this.cmbPrinters.TabIndex = 7;
            this.cmbPrinters.SelectedIndexChanged += new System.EventHandler(this.cmbPrinters_SelectedIndexChanged);
            // 
            // cmbCheckBin
            // 
            this.cmbCheckBin.FormattingEnabled = true;
            this.cmbCheckBin.Location = new System.Drawing.Point(166, 102);
            this.cmbCheckBin.Name = "cmbCheckBin";
            this.cmbCheckBin.Size = new System.Drawing.Size(188, 21);
            this.cmbCheckBin.TabIndex = 8;
            // 
            // cmbEOBBin
            // 
            this.cmbEOBBin.FormattingEnabled = true;
            this.cmbEOBBin.Location = new System.Drawing.Point(166, 138);
            this.cmbEOBBin.Name = "cmbEOBBin";
            this.cmbEOBBin.Size = new System.Drawing.Size(188, 21);
            this.cmbEOBBin.TabIndex = 9;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(175, 241);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(121, 28);
            this.btnPrint.TabIndex = 10;
            this.btnPrint.Text = "Print Check";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pbOverall
            // 
            this.pbOverall.Location = new System.Drawing.Point(88, 204);
            this.pbOverall.Name = "pbOverall";
            this.pbOverall.Size = new System.Drawing.Size(303, 21);
            this.pbOverall.TabIndex = 15;
            // 
            // lblCheckStatusMsg
            // 
            this.lblCheckStatusMsg.AutoSize = true;
            this.lblCheckStatusMsg.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckStatusMsg.Location = new System.Drawing.Point(100, 169);
            this.lblCheckStatusMsg.Name = "lblCheckStatusMsg";
            this.lblCheckStatusMsg.Size = new System.Drawing.Size(187, 16);
            this.lblCheckStatusMsg.TabIndex = 16;
            this.lblCheckStatusMsg.Text = "Status                                                  ";
            // 
            // lblCheckEobStatusMsg
            // 
            this.lblCheckEobStatusMsg.AutoSize = true;
            this.lblCheckEobStatusMsg.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckEobStatusMsg.ForeColor = System.Drawing.Color.Blue;
            this.lblCheckEobStatusMsg.Location = new System.Drawing.Point(100, 185);
            this.lblCheckEobStatusMsg.Name = "lblCheckEobStatusMsg";
            this.lblCheckEobStatusMsg.Size = new System.Drawing.Size(187, 16);
            this.lblCheckEobStatusMsg.TabIndex = 17;
            this.lblCheckEobStatusMsg.Text = "Status                                                  ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PrintCheck.Properties.Resources.WizardBanner;
            this.pictureBox1.Location = new System.Drawing.Point(12, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(483, 50);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCopyright.Location = new System.Drawing.Point(12, 285);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(214, 13);
            this.lblCopyright.TabIndex = 19;
            this.lblCopyright.Text = "@Copyright Computer Sciences Corporation";
            // 
            // frmPrintCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(505, 304);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblCheckEobStatusMsg);
            this.Controls.Add(this.lblCheckStatusMsg);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.cmbEOBBin);
            this.Controls.Add(this.cmbCheckBin);
            this.Controls.Add(this.cmbPrinters);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbOverall);
            this.MaximizeBox = false;
            this.Name = "frmPrintCheck";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print Check Application - © CSC";
            this.Load += new System.EventHandler(this.frmPrintCheck_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbPrinters;
        private System.Windows.Forms.ComboBox cmbCheckBin;
        private System.Windows.Forms.ComboBox cmbEOBBin;
        private System.Windows.Forms.Button btnPrint;
        private ProgressBar pbOverall;
        private Label lblCheckStatusMsg;
        private Label lblCheckEobStatusMsg;
        private PictureBox pictureBox1;
        private Label lblCopyright;
    }
}