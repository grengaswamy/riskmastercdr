'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 2007
'
' NAME: AssemblyBinding Debugging
'
' AUTHOR: CSC
' DATE  : 8/27/2008
'
' COMMENT: Writes the appropriate registry keys needed for Assembly Binding
' debugging using fuslogvw
' Please refer to the following MSDN article regarding the usage of fuslogvw
' for debugging .Net assembly bindings and loading problems:
' http://msdn.microsoft.com/en-us/library/e74a18c4(VS.80).aspx
'==========================================================================
Const LOG_SATELLITE_ASSEMBLIES = "HKLM\Software\Microsoft\Fusion\LogResourceBinds"
Const LOG_FORCE_LOG = "HKLM\Software\Microsoft\Fusion\ForceLog"
Const LOG_BINDING_LOG_PATH = "HKLM\Software\Microsoft\Fusion\LogPath"
Const REG_DWORD = "REG_DWORD"
Const REG_SZ = "REG_SZ"
Dim strLoggingPath

strLoggingPath = InputBox("Please enter an assembly logging path", "Assembly Logging Path")

Call WriteRegistryKey(LOG_FORCE_LOG, 1, REG_DWORD)

Call WriteRegistryKey(LOG_BINDING_LOG_PATH, strLoggingPath, REG_SZ)

Call WriteRegistryKey(LOG_SATELLITE_ASSEMBLIES, 1, REG_DWORD)

Sub WriteRegistryKey(strRegKey, strRegKeyValue, strRegType)
	Dim objWSH
	
	'Create the Windows Scripting Host object
	Set objWSH = CreateObject("WScript.Shell")
	
	'Writ the key out to the registry
	objWSH.RegWrite strRegKey, strRegKeyValue, strRegType
	
	'Clean up
	Set objWSH = Nothing
End Sub

