﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 05/20/2015 | RMA-4606        | nshah28   |  Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Configuration;
using Riskmaster.Application.RMUtilities;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using C1.C1Zip;

namespace CurrencyExchangeInterface
{
    class CurrencyUploader
    {
        public string sUserName = "";
        public string sPassword = ""; public string sDataSource = "";
        public string sConnstring = "";
        public string sFileSource = "";
        public int sDocPathType = 0;
        public int iClientId = 0;
        public string Tracelogfilename = "Processlog" + "_" + DateTime.Now.ToString("ddMMyyyy") + ".log";
        public string sBasePath = ConfigurationManager.AppSettings["BasePath"].ToString();
        public string sLogFilePath = RMConfigurator.UserDataPath + "\\CurrencyExchangeInterface" + "\\temp";
        public string m_JobId = "0";
        public string msgtxt = string.Empty;
       
        List<CurrencyColumn> listOfCurrencyColumn = new List<CurrencyColumn>();
        List<CurrencyExchageCodes> listOfCurrencyExchageCodes = new List<CurrencyExchageCodes>();
        string[] allLines = null;
        XmlDocument returnXmlResponse = null;

        
        public string errorInputFilePath
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorInputFilePath", iClientId).ToString()); }
        }
        public string errorInputFileName
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorInputFileName", iClientId).ToString()); }
        }
        public string errorFileNotFound
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorFileNotFound", iClientId).ToString()); }
        }
        public string errorInputFTPServerName
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorInputFTPServerName", iClientId).ToString()); }
        }
        public string errorInputFTPUserName
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorInputFTPUserName", iClientId).ToString()); }
        }
        public string errorInputFTPPassword
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorInputFTPPassword", iClientId).ToString()); }
        }
        public string errorNoCodeMatchFound
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errorNoCodeMatchFound", iClientId).ToString()); }
        }
        public string errArgMissing
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errArgMissing", iClientId).ToString()); }
        }
        public string errAutheticationFail
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errAutheticationFail", iClientId).ToString()); }
        }
        public string msgProcessStartSucessfully
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.ProcessStartSucessfully", iClientId).ToString()); }
        }
        public string successfullyReadTheFile
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.successfullyReadTheFile", iClientId).ToString()); }
        }

        public string msgCurrencyInsertUpdate
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgCurrencyInsertUpdate", iClientId).ToString()); }
        }
        public string msgCreatingZipFile
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgCreatingZipFile", iClientId).ToString()); }
        }
        public string msgZipFileCreated
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgZipFileCreated", iClientId).ToString()); }
        }
        public string msgFileAttachingToDb
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgFileAttachingToDb", iClientId).ToString()); }
        }
        public string msgZipCreatedForFileCount
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgZipCreatedForFileCount", iClientId).ToString()); }
        }
        
        public string successfullyAttachedFile
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.successfullyAttachedFile", iClientId).ToString()); }
        }
        public string errReadZippingFile
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errReadZippingFile", iClientId).ToString()); }
        }
        public string errInZippingFile
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errInZippingFile", iClientId).ToString()); }
        }
        public string errSavingZipFileToDb
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errSavingZipFileToDb", iClientId).ToString()); }
        }
        public string errInCurrencyExchange
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.errInCurrencyExchange", iClientId).ToString()); }
        }
        public string msgNoDataFound
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgNoDataFoundInCSV", iClientId).ToString()); }
        }
        public string msgStart
        {
            get { return CommonFunctions.FilterBusinessMessage(GlobalResource.GetResource("CurrencyExchangeInt.msgStart", iClientId).ToString()); }
        }
        
        /// <summary>
        /// UpdateCurrency for File System
        /// </summary>
        /// <param name="objFileSystem"></param>
        /// <returns></returns>
        public string UpdateCurrency(FileSystem objFileSystem)
        { 
            try
            {
                if (string.IsNullOrEmpty(objFileSystem.sFilePath))
                {
                    LogMessage(errorInputFilePath);
                    return errorInputFilePath;
                }
                if (string.IsNullOrEmpty(objFileSystem.sFileName))
                {
                    LogMessage(errorInputFileName);
                    return errorInputFileName;
                }

                fetchCodesFromDB();

                ReadFromSharedLocation(objFileSystem.sFilePath, objFileSystem.sFileName);

                if (allLines != null)
                {
                    if (allLines.Length > 0)
                    {
                        InsertUpdateCurrencyRates();
                    }
                    else
                    {
                        LogMessage(msgNoDataFound);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
           
            return "";
        }

        /// <summary>
        /// Update Currency for FTP
        /// </summary>
        /// <param name="objFTP"></param>
        /// <returns></returns>
        public string UpdateCurrency(FTP objFTP)
        {
            try
            {
                if (string.IsNullOrEmpty(objFTP.sFTPServerName))
                {
                    LogMessage(errorInputFTPServerName);
                    return errorInputFTPServerName;
                }
                if (string.IsNullOrEmpty(objFTP.sFTPUserName))
                {
                    LogMessage(errorInputFTPUserName);
                    return errorInputFTPUserName;
                }
                if (string.IsNullOrEmpty(objFTP.sFTPPassword))
                {
                    LogMessage(errorInputFTPPassword);
                    return errorInputFTPPassword;
                }
                if (string.IsNullOrEmpty(objFTP.sFTPFilePath))
                {
                    LogMessage(errorInputFilePath);
                    return errorInputFilePath;
                }
                if (string.IsNullOrEmpty(objFTP.sFTPFileName))
                {
                    LogMessage(errorInputFileName);
                    return errorInputFileName;
                }

                fetchCodesFromDB();
                
                ReadFileFromFTP(objFTP);
                InsertUpdateCurrencyRates();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return "";
        }

        /// <summary>
        /// This read file from shared location
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="FileName"></param>
        public void ReadFromSharedLocation(string FilePath, string FileName)
        {
            Stream remoteStream = null;
            Stream localStream = null;
            WebResponse response = null;
            StreamReader reader = null;

            // Create a request for the specified remote file name
            try
            {
                string _remoteFilename = "";

                _remoteFilename = FilePath + "\\" + FileName;

                WebRequest request = WebRequest.Create(_remoteFilename);
                //request.Credentials = new NetworkCredential("", "");
                if (request != null)
                {
                    // Send the request to the server and retrieve the
                    // WebResponse object
                    response = request.GetResponse();
                    if (response != null)
                    {
                        // Once the WebResponse object has been retrieved,
                        // get the stream object associated with the response's data

                        remoteStream = response.GetResponseStream();

                        //Directly read file and get all data in "allLines" array
                        reader = new StreamReader(remoteStream, Encoding.Default, true); //Set Encoding.Default to read all currrency symbol
                        allLines = reader.ReadToEnd().Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        LogMessage(successfullyReadTheFile);
                        //Add data in list object
                        CopyDataInListObj();
                    }
                }
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Could not find file"))
                {
                    LogMessage(errorFileNotFound);
                }
                else
                {
                    throw new Exception(e.Message, e.InnerException);
                }
            }

            finally
            {
                // Close the response and streams objects here
                // to make sure they're closed even if an exception is thrown at some point
                if (reader != null) reader.Close();
                if (response != null) response.Close();
                if (remoteStream != null) remoteStream.Close();
                if (localStream != null) localStream.Close();

            }
        }

       /// <summary>
       /// This will add data in List type object
       /// </summary>
        private void CopyDataInListObj()
        {
            try
            {
                if (allLines != null)
                {
                    foreach (var item in allLines)
                    {
                        CurrencyColumn objCurrencyColumn = new CurrencyColumn();
                        string[] values = item.Split(',');
                        if (values.Length == 4)
                        {
                            objCurrencyColumn.FromCurrencyColumn = values[0];
                            objCurrencyColumn.ToCurrencyColumn = values[1];
                            objCurrencyColumn.ExchangeRate = values[2];
                            objCurrencyColumn.ExchangeRateDate = values[3];
                            listOfCurrencyColumn.Add(objCurrencyColumn);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// This will Read file from FTP
        /// </summary>
        /// <param name="objFTP"></param>
        public void ReadFileFromFTP(FTP objFTP)
        {
           //Concept to read file from FTP
             string sReturn = "";
            Stream localStream = null;
            Stream responseStream = null;
            StreamReader reader = null;

            try
            {
                FtpWebRequest reqFTP = null;
                String ftpserver = "";

                if (!string.IsNullOrEmpty(objFTP.sFTPPort))
                {
                    objFTP.sFTPServerName = objFTP.sFTPServerName + ":" + objFTP.sFTPPort;
                }

                if (!objFTP.sFTPServerName.Contains("ftp://"))
                {
                    ftpserver = "ftp://" + objFTP.sFTPServerName + "/" + objFTP.sFTPFilePath + "/" + objFTP.sFTPFileName;
                }
                else
                {
                    ftpserver = objFTP.sFTPServerName + "/" + objFTP.sFTPFilePath + "/" + objFTP.sFTPFileName;
                }

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpserver));
                reqFTP.UsePassive = false;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(objFTP.sFTPUserName, objFTP.sFTPPassword);
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.Proxy = WebRequest.GetSystemWebProxy();
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                //use the response like below
                responseStream = response.GetResponseStream();
                reader = new StreamReader(responseStream,Encoding.Default,true); //Set Encoding.Default to read all currrency symbol
                allLines = reader.ReadToEnd().Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                sReturn = successfullyReadTheFile;
                LogMessage(successfullyReadTheFile);
                //Add data in list object
                CopyDataInListObj();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("file not found"))
                {
                    LogMessage(errorFileNotFound);
                }
                throw new Exception(ex.Message);

            }
            finally
            {
                if (responseStream != null) responseStream.Close();
                if (localStream != null) localStream.Close();
                if (reader != null) reader.Close();
            }
            
        }

        
        /// <summary>
        /// This function will perform insert, update operation in Currency_Rate and Currency_Rate_Hist table
        /// </summary>
        /// <returns></returns>
        public string InsertUpdateCurrencyRates()
        {
            int sourceCurrencyCodeId = 0;
            int destinationCurrencyCodeId = 0;
            string sReturn = "";
            //string success = "";
            int insertUpdateCount = 0;

            try
            {
                foreach (var item in listOfCurrencyColumn)
                {
                    sourceCurrencyCodeId = (from x in listOfCurrencyExchageCodes
                                            where x.ShortCode == item.FromCurrencyColumn
                                            select x.CodeID).FirstOrDefault();

                    destinationCurrencyCodeId = (from x in listOfCurrencyExchageCodes
                                                 where x.ShortCode == item.ToCurrencyColumn
                                                 select x.CodeID).FirstOrDefault();

                    if (sourceCurrencyCodeId == 0 || destinationCurrencyCodeId == 0)
                    {
                        if (sourceCurrencyCodeId == 0)
                            sReturn = errorNoCodeMatchFound + " " + item.FromCurrencyColumn;

                        else if (destinationCurrencyCodeId == 0)
                            sReturn = errorNoCodeMatchFound + " " + item.ToCurrencyColumn;
                        LogMessage(sReturn);
                        
                    }
                    else
                    {
                        //Insert update operation
                        //success = success + " , " + sourceCurrencyCodeId + ":" + destinationCurrencyCodeId + ":" + item.ExchangeRate;
                        
                        string sCurrencyRowId = string.Empty;
                        string sSQL = "SELECT CURR_ROW_ID FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE = " +
                       sourceCurrencyCodeId + " AND DESTINATION_CURRENCY_CODE = " + destinationCurrencyCodeId;

                        DbReader objDBReader = DbFactory.GetDbReader(sConnstring, sSQL);

                        while (objDBReader.Read())
                        {
                            sCurrencyRowId = objDBReader["CURR_ROW_ID"].ToString();
                        }

                        string sExchangeRateDate = string.Empty;
                        DateTime dateValue;
                        if (DateTime.TryParse(item.ExchangeRateDate, out dateValue))
                        {
                            sExchangeRateDate = Conversion.GetDateTime(Convert.ToDateTime(item.ExchangeRateDate).ToString("yyyyMMddHHmmss"));
                        }

                        XmlDocument objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(GetMessageTemplate(sCurrencyRowId, sourceCurrencyCodeId, destinationCurrencyCodeId, item.FromCurrencyColumn, item.ToCurrencyColumn, item.ExchangeRate, sExchangeRateDate));

                        MultiCurrency objMultiCurrency = new MultiCurrency(sUserName, sConnstring, iClientId);
                        returnXmlResponse = objMultiCurrency.Save(objXmlDoc);
                        LogMessage("0 ^*^*^ " + string.Format(msgCurrencyInsertUpdate, item.FromCurrencyColumn,item.ToCurrencyColumn));
                        insertUpdateCount++;
                    }

                } //End of foreach
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return sReturn;
        }

        /// <summary>
        /// This fetch currency code's ID from DB
        /// </summary>
        /// <returns></returns>
        public List<CurrencyExchageCodes> fetchCodesFromDB()
        {
            //Now bring the values from database
            listOfCurrencyExchageCodes = new List<CurrencyExchageCodes>();

            try
            {
                string sSql = "SELECT CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES_TEXT INNER JOIN CODES ON CODES.CODE_ID=CODES_TEXT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'CURRENCY_TYPE')";
                DbReader objRead = DbFactory.GetDbReader(sConnstring, sSql);
                while (objRead.Read())
                {
                    CurrencyExchageCodes objCurrencyExchangeCode = new CurrencyExchageCodes();
                    objCurrencyExchangeCode.CodeID = Convert.ToInt32(objRead.GetValue("CODE_ID"));
                    objCurrencyExchangeCode.ShortCode = Convert.ToString(objRead.GetValue("SHORT_CODE"));
                    objCurrencyExchangeCode.CodeDesc = Convert.ToString(objRead.GetValue("CODE_DESC"));
                    listOfCurrencyExchageCodes.Add(objCurrencyExchangeCode);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return listOfCurrencyExchageCodes;
        }

        /// <summary>
        /// This create XML which goes for save
        /// </summary>
        /// <param name="strMultiCurrencyId"></param>
        /// <param name="sourceCurrencyCodeId"></param>
        /// <param name="destinationCurrencyCodeId"></param>
        /// <param name="FromCurrencyColumn"></param>
        /// <param name="ToCurrencyColumn"></param>
        /// <param name="ExchangeRate"></param>
        /// <param name="ExchangeRateDate"></param>
        /// <returns></returns>
        private string GetMessageTemplate(string strMultiCurrencyId, int sourceCurrencyCodeId, int destinationCurrencyCodeId, string FromCurrencyColumn, string ToCurrencyColumn, string ExchangeRate, string ExchangeRateDate)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            try
            {

                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");
                sXml = sXml.Append("<form name='CurrencyExchangeInterface' title='CurrencyExchangeInterface'>");
                sXml = sXml.Append("<group name='CurrencyExchangeInterface' title='CurrencyExchangeInterface'>");
                sXml = sXml.Append("<displaycolumn>");
                sXml = sXml.Append("<control name='CurrencyRowId' type='id'>");
                sXml = sXml.Append(strMultiCurrencyId);
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='SourceCurrencyId' type='id'>");
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='DestinationCurrencyId' type='id'>");
                sXml = sXml.Append("</control>");
                sXml = sXml.Append("<control name='SourceCurrency' type='code' codeid='");
                sXml = sXml.Append(sourceCurrencyCodeId);
                sXml = sXml.Append("'>" + FromCurrencyColumn + "</control>");

                sXml = sXml.Append("<control name='DestinationCurrency' type='code' codeid='" + destinationCurrencyCodeId + "'>" + ToCurrencyColumn + "</control>");
                sXml = sXml.Append("<control name='ExchangeRate' type='numeric'>" + ExchangeRate + "</control>");
                sXml = sXml.Append("<control name='ExchangeRateDate' type='date'>" + ExchangeRateDate + "</control>");
                sXml = sXml.Append("</displaycolumn>");
                sXml = sXml.Append("</group></form>");
                sXml = sXml.Append("</Document>");
                sXml = sXml.Append("</Message>");
                // XElement oElement = XElement.Parse(sXml.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return sXml.ToString();
        }

        /// <summary>
        /// Create log file
        /// </summary>
        /// <param name="message"></param>
        public void LogMessage(string message)
        {
            try
            {
                Console.OutputEncoding = System.Text.Encoding.Default;
                Console.WriteLine(message);
                string sErrorLog = "";
                //Get the log path 
                FileStream objFileStream = null;
                StreamWriter objStreamWriter = null;
                msgtxt = msgtxt + message;

                if (sLogFilePath != "")
                {
                   string sLogFileName = sLogFilePath + "\\" + Tracelogfilename;
                  
                    objFileStream = new FileStream(sLogFileName, FileMode.OpenOrCreate | FileMode.Append);
                    objStreamWriter = new StreamWriter(objFileStream);
                    sErrorLog = "\r\n" + DateTime.Now.ToString();
                    sErrorLog += " : " + message;
                    objStreamWriter.Write(sErrorLog);
                    objStreamWriter.Close();
                    objFileStream.Close();
                    
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        /// <summary>
        /// Create the zipfile with name dictated by caller.  Zipfiles created in this way will not be added to the database.
        /// </summary>
        /// <param name="filename">Name (only) of the zipfile to create</param>
        public void CreateZip(string filename,string sPath)
        {
            string sTempPath = Path.Combine(sPath, "temp");
            C1ZipFile ZipFile = null;
            try
            {
                int fileCount = 0;
                if (Directory.Exists(sTempPath))
                {
                    LogMessage("0 ^*^*^ " + msgCreatingZipFile);
                    ZipFile = new C1ZipFile(sPath + "\\" + filename);

                    foreach (string file in Directory.GetFiles(sTempPath))
                    {
                        if (!file.EndsWith(".zip"))
                        {
                            fileCount++;
                            ZipFile.Entries.Add(file);
                        }
                    }
                    ZipFile.Close();
                    LogMessage("0 ^*^*^ " + string.Format(msgZipFileCreated));
                    
                }
            }
            catch (System.Exception e)
            {
                LogMessage("0 ^*^*^ "+errInZippingFile);
                throw e;
            }
            finally
            {
                if (ZipFile != null)
                {
                    ZipFile.Close();
                    ZipFile = null;
                }
            }
        }

        /// <summary>
        /// Add a file to the task manager database TM_JOBS_DOCUMENT table.
        /// </summary>
        /// <param name="filePath">The full path of the file to insert into the DB.</param>
        public void FileToDatabase(string filePath, string fileName)
        {
            FileStream stream = null;
            byte[] fileData = null;
            if (File.Exists(filePath))
            {
                try
                {
                    stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    fileData = new byte[stream.Length];
                    stream.Read(fileData, 0, (int)stream.Length);
                    stream.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("0 ^*^*^ " + errReadZippingFile);
                    throw e;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
                try
                {
                    Console.WriteLine("0 ^*^*^ "+msgFileAttachingToDb);
                    using (DbConnection dbConnection = DbFactory.GetDbConnection(RMConfigurationManager.GetConnectionString("TaskManagerDataSource", iClientId)))//dvatsa-cloud
                    {
                        dbConnection.Open();
                        DbCommand cmdInsert = dbConnection.CreateCommand();
                        DbParameter paramFileId = cmdInsert.CreateParameter();
                        DbParameter paramJobId = cmdInsert.CreateParameter();
                        DbParameter paramFileName = cmdInsert.CreateParameter();
                        DbParameter paramFileData = cmdInsert.CreateParameter();
                        DbParameter paramContentType = cmdInsert.CreateParameter();
                        DbParameter paramLength = cmdInsert.CreateParameter();

                        cmdInsert.CommandText = "INSERT INTO TM_JOBS_DOCUMENT " +
                            "(TM_FILE_ID, JOB_ID, FILE_NAME, FILE_DATA, CONTENT_TYPE, CONTENT_LENGTH) VALUES " +
                            "(~FileId~, ~JobId~, ~FileName~, ~FileData~, ~ContentType~, ~ContentLength~)";

                        paramFileId.Direction = ParameterDirection.Input;
                        paramFileId.Value = int.Parse(m_JobId);
                        //paramFileId.Value = JobId;
                        paramFileId.ParameterName = "FileId";
                        paramFileId.SourceColumn = "TM_FILE_ID";
                        cmdInsert.Parameters.Add(paramFileId);

                        paramJobId.Direction = ParameterDirection.Input;
                        paramJobId.Value = int.Parse(m_JobId);
                        //paramJobId.Value = JobId;
                        paramJobId.ParameterName = "JobId";
                        paramJobId.SourceColumn = "JOB_ID";
                        cmdInsert.Parameters.Add(paramJobId);

                        paramFileName.Direction = ParameterDirection.Input;
                        paramFileName.Value = fileName;
                        paramFileName.ParameterName = "FileName";
                        paramFileName.SourceColumn = "FILE_NAME";
                        cmdInsert.Parameters.Add(paramFileName);

                        paramFileData.Direction = ParameterDirection.Input;
                        paramFileData.Value = fileData;
                        paramFileData.ParameterName = "FileData";
                        paramFileData.SourceColumn = "FILE_DATA";
                        cmdInsert.Parameters.Add(paramFileData);

                        paramContentType.Direction = ParameterDirection.Input;
                        paramContentType.Value = "";
                        paramContentType.ParameterName = "ContentType";
                        paramContentType.SourceColumn = "CONTENT_TYPE";
                        cmdInsert.Parameters.Add(paramContentType);

                        paramLength.Direction = ParameterDirection.Input;
                        paramLength.Value = fileData.Length;
                        paramLength.ParameterName = "ContentLength";
                        paramLength.SourceColumn = "CONTENT_LENGTH";
                        cmdInsert.Parameters.Add(paramLength);

                        cmdInsert.ExecuteNonQuery();

                    }
                    Console.WriteLine("0 ^*^*^ " + successfullyAttachedFile);
                    
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("0 ^*^*^ " + errSavingZipFileToDb);
                    throw e;
                }

                finally
                {
                    if (fileData != null)
                    {
                        fileData = null;
                    }
                }
            }
        }

    }




    class FileSystem
    {
        public string sFilePath = "";
        public string sFileName = "";
    }

    class FTP
    {
        public string sFTPServerName = "";
        public string sFTPPort = "";
        public string sFTPUserName = "";
        public string sFTPPassword = "";
        public string sFTPFilePath = ""; public string sFTPFileName = "";
    }

    class CurrencyColumn
    {
        public string FromCurrencyColumn { get; set; }
        public string ToCurrencyColumn { get; set; }
        public string ExchangeRate { get; set; }
        public string ExchangeRateDate { get; set; }
    }

    class CurrencyExchageCodes
    {
        public int CodeID { get; set; }
        public string ShortCode { get; set; }
        public string CodeDesc { get; set; }
    }
}
