﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 05/20/2015 | RMA-4606        | nshah28   |  Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Application.RMUtilities;
using System.IO;
using Riskmaster.Security.Encryption;

namespace CurrencyExchangeInterface
{
    class Program
    {
        private static CurrencyUploader objCurrencyUploader = new CurrencyUploader();
        private static FileSystem objFileSystem = new FileSystem();
        private static FTP objFTP = new FTP();
        private static string strMsg = string.Empty;


        public static void Main(string[] args)
        {
            try
            {

                Console.WriteLine(objCurrencyUploader.msgStart);
                UserLogin oUserLogin = null;
                bool blnSuccess = false;

                int argCount = (args == null) ? 0 : args.Length;

                //Add & Change by kuladeep for Cloud----Start
                if (args.Length > 0)
                {
                    GetParameters(args);//m_iClientId will populate from arguments for Cloud
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    objCurrencyUploader.iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }


                if (args.Length > 0)
                {
                    oUserLogin = new UserLogin(objCurrencyUploader.sUserName, objCurrencyUploader.sDataSource, objCurrencyUploader.iClientId);
                    if (oUserLogin.DatabaseId <= 0)
                    {
                        Console.WriteLine("0 ^*^*^ " + objCurrencyUploader.errAutheticationFail);
                        throw new Exception(objCurrencyUploader.errAutheticationFail);
                    }
                    else
                    {
                        objCurrencyUploader.sConnstring = oUserLogin.objRiskmasterDatabase.ConnectionString;
                        objCurrencyUploader.sDocPathType = oUserLogin.objRiskmasterDatabase.DocPathType;
                        objCurrencyUploader.LogMessage(objCurrencyUploader.msgProcessStartSucessfully);

                        switch (objCurrencyUploader.sFileSource)
                        {
                            case "1": //File
                                strMsg = objCurrencyUploader.UpdateCurrency(objFileSystem);
                                break;

                            case "2": //FTP
                                strMsg = objCurrencyUploader.UpdateCurrency(objFTP);
                                break;
                        }
                    }

                }
                else
                {
                    objCurrencyUploader.LogMessage(objCurrencyUploader.errArgMissing);
                }
            }
            catch (Exception exp)
            {
                
                //Console.WriteLine("0 ^*^*^ " + objCurrencyUploader.errInCurrencyExchange);
                Console.WriteLine("1001 ^*^*^ {0} ", objCurrencyUploader.errInCurrencyExchange + ": " + exp.Message + " " + exp.InnerException);
                objCurrencyUploader.LogMessage(exp.Message);
            }
            finally
            {
                Log.Write(objCurrencyUploader.msgtxt, "CommonWebServiceLog", objCurrencyUploader.iClientId);

                string sZipFileName = "CurrencyExchange_" + objCurrencyUploader.m_JobId + "_" + Conversion.ToDbDateTime(DateTime.Now) + ".zip";
                string sPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "CurrencyExchangeInterface");

                objCurrencyUploader.CreateZip(sZipFileName, sPath);

                objCurrencyUploader.FileToDatabase(Path.Combine(sPath, sZipFileName), sZipFileName);

                if (File.Exists(objCurrencyUploader.sLogFilePath + "\\" + objCurrencyUploader.Tracelogfilename))
                {
                    File.Delete(objCurrencyUploader.sLogFilePath + "\\" + objCurrencyUploader.Tracelogfilename);  //delete log file from folder as we have alredy move this into ZIP and ZIP into DB
                }

                if (File.Exists(sPath + "\\" + sZipFileName))
                {
                    File.Delete(sPath + "\\" + sZipFileName); //delete ZIP file from folder as we have alredy move this in to DB
                }
            }

        }

        /// <summary>
        /// Get parameters sent from command line
        /// </summary>
        /// <param name="p_args">Array of argument sent from Command line</param>

        private static void GetParameters(string[] p_args)
        {
            try
            {
                int iLength = 0;
                string sPrefix = string.Empty;
                string sRebuildAll = string.Empty;
                iLength = p_args.Length;

                /*string interfaceSettingsArg = "";
                string[] p_argsForInterfacesetting = new string[] { "|^|" }; */
                //comment this from here
                /* for (int i = 0; i < iLength; i++)
                 {
                     if (p_args[5].Trim() == "2" && i == 9)
                     {
                         sRebuildAll = sRebuildAll + i + ": " + RMCryptography.EncryptString(p_args[9].Trim()) + " , ";
                     }
                     else
                     {
                         sRebuildAll = sRebuildAll + i + ": " + p_args[i].Trim() + " , ";
                     }
                 }
                 objCurrencyUploader.LogMessage(sRebuildAll); //creating a log to check parameters
                  //comment this to here */

                for (int i = 0; i < 6; i++)
                {

                    sPrefix = p_args[i].Trim();
                    if (sPrefix.Length > 3)
                    {
                        sPrefix = sPrefix.Substring(0, 3);
                    }
                    switch (sPrefix.ToLower())
                    {
                        case "-ds":
                            objCurrencyUploader.sDataSource = p_args[i].Trim().Substring(3);
                            break;
                        case "-ru":
                            objCurrencyUploader.sUserName = p_args[i].Trim().Substring(3);
                            break;
                        case "-rp":
                            objCurrencyUploader.sPassword = p_args[i].Trim().Substring(3);
                            break;
                        case "-ci"://Add by kuladeep for Cloud.
                            objCurrencyUploader.iClientId = Convert.ToInt32(p_args[i].Trim().Substring(3));
                            break;
                        case "-jb":
                            objCurrencyUploader.m_JobId = p_args[i].Trim().Substring(3);
                            break;
                        default:
                            // interfaceSettingsArg = p_args[i].Trim();
                            objCurrencyUploader.sFileSource = p_args[5].Trim();
                            break;
                    }
                }
                    if (objCurrencyUploader.sFileSource == "1")
                    {
                        objFileSystem.sFilePath = p_args[6].Trim();
                        objFileSystem.sFileName = p_args[7].Trim();
                    }
                    if (objCurrencyUploader.sFileSource == "2")
                    {
                        objFTP.sFTPServerName = p_args[6].Trim();
                        objFTP.sFTPPort = p_args[7].Trim();
                        objFTP.sFTPUserName = p_args[8].Trim();
                        objFTP.sFTPPassword = RMCryptography.DecryptString(p_args[9].Trim());
                        objFTP.sFTPFilePath = p_args[10].Trim();
                        objFTP.sFTPFileName = p_args[11].Trim();
                    }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
