using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace SecDBSetUtility
{
    public class RegistryManipulation
    {

        /// <summary>
        /// Method to write the required Riskmaster security key into the database
        /// </summary>
        /// <param name="strDBUserID"></param>
        /// <param name="strDBPassword"></param>
        public static void WriteSecurityKey(string strDBUserID, string strDBPassword)
        {
            RegistryKey rkDBUserID = Registry.LocalMachine.CreateSubKey(@"Software\DTG\Riskmaster\Security\EncDBUserID");
            rkDBUserID.SetValue("", strDBUserID);

            RegistryKey rkDBUserPassword = Registry.LocalMachine.CreateSubKey(@"Software\DTG\Riskmaster\Security\EncDBPassword");
            rkDBUserPassword.SetValue("", strDBPassword);

            //Clean up
            rkDBUserID = null;
            rkDBUserPassword = null;
        }//method: WriteSecurityKey()

       /// <summary>
       /// Reads the Encrypted values from the Registry
       /// </summary>
       /// <param name="strDBUserID"></param>
       /// <param name="strDBPassword"></param>
        public static void ReadSecurityKey(out string strDBUserID, out string strDBPassword)
        {
            RegistryKey rkDBUserID = Registry.LocalMachine.OpenSubKey(@"Software\DTG\Riskmaster\Security\EncDBUserID");

            //Verify that the Registry key exists
            if (rkDBUserID != null)
            {
                strDBUserID = rkDBUserID.GetValue("").ToString();

                RegistryKey rkDBUserPassword = Registry.LocalMachine.OpenSubKey(@"Software\DTG\Riskmaster\Security\EncDBPassword");

                strDBPassword = rkDBUserPassword.GetValue("").ToString();
            }//if
            else
            {
                strDBUserID = string.Empty;
                strDBPassword = string.Empty;
            }//else
        } // method: ReadSecurityKey

    }
}
