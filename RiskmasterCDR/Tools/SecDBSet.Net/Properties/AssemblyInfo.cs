﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SecDBSetUtility")]
[assembly: AssemblyDescription("Writes the required settings for legacy COM applications to the Windows Registry.")]
[assembly: AssemblyProduct("RISKMASTER X r5")]
[assembly: AssemblyCompany("CSC")]
[assembly: AssemblyCopyright("Copyright (c) 2009, CSC")]
[assembly: AssemblyTrademark("CSC")]
[assembly: AssemblyCulture("")]
[assembly: CLSCompliant(true)]



// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8df531d0-0291-418d-98b7-4aace8244c5d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.2.1437.0")]
[assembly: AssemblyFileVersion("2.2.1437.0")]
[assembly: AssemblyInformationalVersion("2.2.1437.0")]
