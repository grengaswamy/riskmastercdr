﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;

namespace OCX
{
    static class Program
    {
        private const string m_sUrlProtocol = "IR";

        public static string Drawer { get; set; }
        public static string FileType { get; set; }
        public static string FileNumber { get; set; }
        public static long DocumentID { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!GetArgs())
            {
                //Register Application by adding entry in registry
                RegisterUrlProtocol();
                Application.Run(new Form1());
            }
            else
            {
                new Form1().OpenFileOnUrlProtocol(Drawer, FileType, FileNumber, DocumentID);
            }
            
        }

        
        public static void RegisterUrlProtocol()
        {
            RegistryKey rKey = null;
            try
            {
                rKey = Registry.ClassesRoot.OpenSubKey(m_sUrlProtocol, true);
                if (rKey == null)
                {
                    rKey = Registry.ClassesRoot.CreateSubKey(m_sUrlProtocol);
                }
                rKey.SetValue("", "URL: IR Protocol");
                rKey.SetValue("URL Protocol", "");

                rKey = rKey.CreateSubKey(@"shell\open\command");
                rKey.SetValue("", "\"" + Application.ExecutablePath + "\" %1");

                MessageBox.Show("Application Registerred!");

                //rNewKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers", true);
                //if (rNewKey!=null)
                //{
                //    rNewKey.SetValue("\"" + Application.ExecutablePath + "\" %1", "RUNASADMIN");
                //    rNewKey.Close();
                //}
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("An Error Occurred. " + ex.Message);
            }
            finally
            {
                if (rKey != null)
                {
                    rKey.Close();
                }
            }
            
            
        }

        public static bool GetArgs()
        {
            bool bSuccess = false;
            string[] arguments = Environment.GetCommandLineArgs();
            string[] sQueryStrings = null;
            long lDocId = 0L;
            if (arguments.Length > 1)
            {
                // Format = "IR:OpenImageRight?drawer=Claim&fileType=Claim&fileNumber=12345"
                string[] args = arguments[1].Split(':');
                if (args[0].Trim().ToUpper() == "IR" && args.Length > 1)
                { // Means this is a URL protocol
                    string[] actionDetail = args[1].Split('?');

                    if (actionDetail.Length>1)
                    {
                        sQueryStrings = actionDetail[1].Split('&');
                    }

                    foreach (string sQString in sQueryStrings)
                    {
                        if (sQString.Split('=').Length>1)
                        {
                            switch (sQString.Split('=')[0].ToUpper())
                            {
                                case "DRAWER":
                                    Drawer = sQString.Split('=')[1];
                                    break;
                                case "FILETYPE":
                                    FileType = sQString.Split('=')[1];
                                    break;
                                case "FILENUMBER":
                                    FileNumber = sQString.Split('=')[1];
                                    break;
                                case "DOCUMENTID":
                                    long.TryParse(sQString.Split('=')[1], out lDocId);
                                    DocumentID = lDocId;
                                    break;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(FileType))
                    {
                        FileType = Drawer;
                    }
                    if (!String.IsNullOrEmpty(Drawer) && !string.IsNullOrEmpty(FileNumber))
                    {
                        bSuccess = true;
                    }
                }
            }
            return bSuccess;
        }
    }
}
