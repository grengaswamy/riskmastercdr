Read Me File for BillingScheduler.exe

Things to do: 

1.) Open BillingScheduler.sln and build the solution. This will generate BillingScheduler.exe and          BillingScheduler.exe.config files in the bin\Debug folder. 

2.) Copy these two files and place them in the folder where all the RismasterX assemblies reside.

3.) Edit BillingScheduler.exe.config. Provide correct paths for Riskmaster.config,     loggingConfiguration.config and loggingDistributorConfiguration.config in this file.

4.) Specify the path where the invoice/notices pdf will be generated in the Riskmaster.config
    file under EnhancedPolicyBilling_ExportToFilePath node.

5.) Edit Schedule Invoices.bat file
a)  Specify the path of BillingScheduler.exe in the third line
b)  Specify Invoice as first parameter,data source as second, user name and password as third and            fourth parameters to BillingScheduler in the fourth line.
c)  for eg.    
    BillingScheduler Invoice Mississippi csc csc 
    where Invoice soecifies Invoice generationis the first parameter
	  Mississippi is datasource
	  csc is user name
	  csc is password.

6.) Edit Schedule Notices.bat file
a)  Specify the path of BillingScheduler.exe in the third line
b)  Specify Notice as first parameter,data source as second, user name and password as third and           fourth parameters to BillingScheduler in the fourth line.
c)  for eg.    
    BillingScheduler Notice Mississippi csc csc 
    where Notice is the first parameter
	  Mississippi is datasource
	  csc is user name
	  csc is password.

7.) Edit Schedule Installment.bat file
a)  Specify the path of BillingScheduler.exe in the third line
b)  Specify Installment as first parameter,data source as second, user name and password as third             and fourth parameters to BillingScheduler in the fourth line.
c)  for eg.    
    BillingScheduler Installment Mississippi csc csc 
    where Notice is the first parameter
	  Mississippi is datasource
	  csc is user name
	  csc is password.

8.) Go to Start-> All Programs -> Accessories -> System Tools -> Scheduled Tasks  
    Add new Scheduled Tasks, select Invoices.bat or Notices.bat and specify date and time
    for running the task.


Good Luck
