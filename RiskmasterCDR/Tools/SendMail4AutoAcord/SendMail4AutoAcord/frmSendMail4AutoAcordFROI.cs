﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions; //RegEX
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor;
using System.Net.Mail; 
//using Riskmaster.Application.FileStorage;
//using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Settings;
//using Riskmaster.Application.DocumentManagement;
using Amyuni.PDFCreator;
using Riskmaster.BusinessAdaptor.AutoFroiAcord;
using System.Xml;
using System.IO;


namespace SendMail4AutoAcord
{
    public partial class frmSendMail4AutoAcordFROI : Form
    {
        private DataModelFactory m_objDataModelFactory = null;
        private string m_sConnectionString = string.Empty;
        public frmSendMail4AutoAcordFROI(DataModelFactory objDataModelFactory)
        {
            InitializeComponent();

            m_objDataModelFactory = objDataModelFactory;
        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            string[] ClaimIds = null;
            int iClaimId = 0;
            string sResult = string.Empty;
            Claim objClaim = null;
            bool bSendMail = false;
            bool bValidClaimId = false;
            AutoFroiAcordAdaptor objAutoFroiAcord = new AutoFroiAcordAdaptor();
            rtbOutput.Text = string.Empty;
            pbClaimProgress.Step = 0;
            this.Cursor = Cursors.WaitCursor;
            if (txtClaimIds.Text.Trim() != string.Empty)
            {
                ClaimIds = txtClaimIds.Text.Split(',');
                pbClaimProgress.Minimum = 0;
                pbClaimProgress.Maximum = ClaimIds.Length;
                for (int iClaimCount = 0; iClaimCount < ClaimIds.Length; iClaimCount++)
                {
                    bValidClaimId = Int32.TryParse(ClaimIds[iClaimCount].Trim(), out iClaimId);
                    if (!bValidClaimId)
                    {
                        sResult = "Invalid Claim Id " + ClaimIds[iClaimCount].Trim();
                    }
                    else
                    {
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimId);

                        if (rdbDoNotCheckCriteria.Checked)
                            bSendMail = true;
                        else if (rdbCheckWithMail.Checked)
                        {
                            bSendMail = CheckTriggeringCriteria(objClaim, true, ref sResult);
                        }
                        else if (rdbCheckWithoutMail.Checked)
                        {
                            bSendMail = CheckTriggeringCriteria(objClaim, false, ref sResult);
                        }

                        if (bSendMail)
                        {
                            sResult = GenerateFROIACORD(objClaim);
                        }
                        else
                        {
                            sResult += "For Claim Id " + iClaimId.ToString() + ".";
                        }
                    }
                    UpdateStatus(sResult, iClaimCount + 1);
                    this.Refresh();
                }
                this.Cursor = Cursors.Default;
                MessageBox.Show("Process Completed");
            }
        }
        private bool CheckTriggeringCriteria(Claim objClaim, bool bCheckMail, ref string sStatus)
        {
            int iRowID = 0;
            if (objClaim.Context.InternalSettings.SysSettings.AutoFROIACORDFlag)
            {
                if (objClaim.CurrentAdjuster != null)
                {
                    if (objClaim.CurrentAdjuster.CurrentAdjFlag == true)
                    {
                        if (!bCheckMail || (bCheckMail && objClaim.MailSent == "" ) )
                        {
                            iRowID = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ROW_ID FROM ADJUSTER_FROI_ACORD WHERE ADJUSTER_EID = " + objClaim.CurrentAdjuster.AdjusterEid));
                        }
                        if (iRowID > 0)
                        {

                            sStatus = "Success";
                            return true;
                        }
                        else
                        {
                            sStatus = "Current Adjuster was not found in Assigned Adjuster List";
                            return false;
                        }
                    }
                    else if (objClaim.CurrentAdjuster.CurrentAdjFlag == false)
                    {
                        if (!bCheckMail || (bCheckMail && objClaim.MailSent == "" ))
                        {
                            sStatus = "Current Adjuster Flag is Off ";
                            return false;
                        }
                    }

                }
                else
                {
                    sStatus = "Current Adjuster is not Set ";
                    return false;
                }
            }
            else
            {
                sStatus = "AutoFROIACORDFlag is turned off ";
                return false;
            }
            return false;
        }
        public string GenerateFROIACORD(Claim objClaim)
        {
            string sSQL = "";
            string sOrgEid = "";
            string[] sOrgList;
            string sPassword = "";
            DbReader rdr = null;
            string sEmail = "";
            string sFroiFormId = "";
            string sAcordFormId = "";
            string sSubjectAttach = "";
            string sSubjectPwd = "";
            string sBodyPwd = "";
            string sBodyAttach = "";
            string sAtchSub = "";
            string sFDFPath = "";
            string sPDFPath = "";
            string sPDFFile = "";
            bool bFileGenerated = false;
            string sError = "";
            bool bMailSent = false;
            string sMergePDFPath = "";
            string sMergePwdPDFPath = "";
            MemoryStream objMemory = null;
            XmlDocument p_objXmlIn;
            XmlDocument p_objXmlOut;
            
            BusinessAdaptorErrors p_objErrOut;

            FROIAdaptor objAutoFROI = null;
            DbConnection objCon = null;
            string sAttachFilePath = "";
            bool bFroi = false;
            bool bAcord = false;
            Event objEvent = null;

            try
            {
                p_objXmlIn = new XmlDocument();
                p_objXmlOut = new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors();
                
                objEvent = (Event)objClaim.Context.Factory.GetDataModelObject("Event", false);
                
                objEvent.MoveTo(objClaim.EventId);

                m_sConnectionString = objClaim.Context.DbConnLookup.ConnectionString;

                objCon = DbFactory.GetDbConnection(m_sConnectionString);
                objCon.Open();
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2393F36D688F37892D5BE2B2C416C18F0392811E5704FE6F85DEC44D8612B23570A07A0DBFEB34A4C4FE97D411672E61CA5B43CA");
                //Fetch All the Org Levels
                if (objEvent.DeptEid == 0)
                {
                    return("The Department associated with Claim is blank. The Claim Id " + objClaim.ClaimId.ToString() + "Will Not be Processed");
                }

                sSQL = "SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;
                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    if (rdr.Read())
                    {
                        sOrgEid = rdr.GetInt("DEPARTMENT_EID") + "," + rdr.GetInt("FACILITY_EID") + "," + rdr.GetInt("LOCATION_EID") + "," + rdr.GetInt("DIVISION_EID") + "," + rdr.GetInt("REGION_EID") + "," + rdr.GetInt("OPERATION_EID") + "," + rdr.GetInt("COMPANY_EID") + "," + rdr.GetInt("CLIENT_EID");
                    }
                    rdr.Close();
                }

                sOrgList = sOrgEid.Split(new char[] { ',' });

                //Fetch Password for Both FROI AND ACORD
                //Password will be searched from Dept Level to Client Level in the same order.
                //Moment its found, it will exit the Loop.
                foreach (string sOrgLevel in sOrgList)
                {
                    sSQL = "SELECT FROI_ACORD_PASSWORD FROM ENTITY WHERE ENTITY_ID = " + sOrgLevel;
                    sPassword = objCon.ExecuteString(sSQL);
                    if (!string.IsNullOrEmpty(sPassword))
                    {
                        break;
                    }
                }


                objAutoFROI = new FROIAdaptor();
                if (!string.IsNullOrEmpty(sPassword))
                {
                    if (objClaim.LineOfBusCode == 243)
                    {
                        //Fetch Email List for FROI - Contact Should Have E-mail FROI checkbox checked and should be WC Contact
                        //E-mail Address should not be blank
                        sSQL = "SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN (" + sOrgEid + ") AND EMAIL_FROI = -1";
                        sSQL = sSQL + " AND EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode;
                        using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            while (rdr.Read())
                            {
                                sEmail = rdr.GetString("EMAIL_ADDRESS") + "," + sEmail;
                            }
                        }
                        //Add e-mail Address from the Claim Screen too if the Checkbox is checked
                        if (chkSendMailtoAssociatedClaim.Checked)
                        {
                            sEmail = sEmail + objClaim.EmailAddresses;
                        }
                        if (sEmail != "")
                        {
                            //FormID for that Particular Jurisdiction
                            sFroiFormId = objCon.ExecuteString("SELECT FORM_ID FROM JURIS_FORMS,CLAIM WHERE CLAIM_ID = " + objClaim.ClaimId + " AND STATE_ROW_ID = FILING_STATE_ID AND PRIMARY_FORM_FLAG = -1");
                            //HardCode XML to Invode AutoFROI function
                            p_objXmlIn = GetFROIXml(objClaim.ClaimId, sFroiFormId);
                            bFroi = objAutoFROI.InvokeAutoFROI(p_objXmlIn, objClaim, ref p_objXmlOut, ref p_objErrOut, ref sFDFPath, ref sPDFPath, ref sMergePDFPath, ref sMergePwdPDFPath);
                            if (sMergePDFPath != "")
                                //PDF template path and Name to which FDF is pointing to.
                                sPDFFile = sPDFPath + objCon.ExecuteString("SELECT PDF_FILE_NAME FROM JURIS_FORMS WHERE FORM_ID =  " + sFroiFormId);
                        }
                        else
                        {
                            return "No E-mail addresses were found at Claim Level and at any level of Organization Hierarchy for Claim Id "  + objClaim.ClaimId + ".";
                        }
                    }
                    else if ((objClaim.LineOfBusCode == 241) || (objClaim.LineOfBusCode == 242))
                    {
                        //Fetch Email List for ACORD- Contact Should Have E-mail FROI checkbox checked and should be WC Contact
                        //E-mail Address should not be blank

                        sSQL = "SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO,CONTACT_LOB WHERE ENTITY_ID IN (" + sOrgEid + ") AND EMAIL_ACORD = -1";
                        sSQL = sSQL + " AND EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode;
                        using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                        {
                            while (rdr.Read())
                            {
                                sEmail = rdr.GetString("EMAIL_ADDRESS") + "," + sEmail;
                            }
                        }
                        //Add e-mail Address from the Claim Screen too if the Checkbox is checked
                        if (chkSendMailtoAssociatedClaim.Checked)
                        {
                            sEmail = sEmail + objClaim.EmailAddresses;
                        }
                        if (sEmail != "")
                        {
                            //FormID for that Particular Claim Type Code
                            sAcordFormId = objCon.ExecuteString("SELECT ACORD_FORM_ID FROM CLAIM_ACCORD_MAPPI WHERE CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode + " AND ACORD_FORM_ID > 0");
                            //HardCode XML to Invode AutoAcord function
                            p_objXmlIn = GetFROIXml(objClaim.ClaimId, sAcordFormId);
                            bAcord = objAutoFROI.InvokeAutoACORD(p_objXmlIn, objClaim, ref p_objXmlOut, ref p_objErrOut, ref sFDFPath, ref sPDFPath, ref sMergePDFPath, ref sMergePwdPDFPath);
                            if (sMergePDFPath != "")
                                //PDF template path and Name to which FDF is pointing to.
                                sPDFFile = sPDFPath + objCon.ExecuteString("SELECT FILE_NAME FROM CL_FORMS WHERE FORM_ID = " + sAcordFormId);
                        }
                        else
                        {
                            return "No E-mail addresses found at Claim Level and at any level of Organization Hierarchy for Claim Id" + objClaim.ClaimId + ".";
                        }
                    }
                }
                else
                {
                    return "Email FROI-ACORD Password is not set at any level of Organization Hierarchy for Claim Id" + objClaim.ClaimId + ".";
                }

                //if FDF file was made, then Merge FDF and PDF USING Acrobat Writer
                if ((bFroi || bAcord) && (sMergePDFPath != ""))
                {
                    //Name of the file to be attached to Claim
                    sAttachFilePath = Path.GetTempPath() + objClaim.ClaimNumber;
                    if (objClaim.LineOfBusCode == 243)
                        sAttachFilePath = sAttachFilePath + "_FROIATTACH.pdf";
                    if ((objClaim.LineOfBusCode == 241) || (objClaim.LineOfBusCode == 242))
                        sAttachFilePath = sAttachFilePath + "_ACORDATTACH.pdf";
                    //Merge FDF and PDF into one PDF File
                    if (MergeFDFTOPDF(sPDFFile, sFDFPath, sAttachFilePath, ref sError))
                    {
                        if (EncryptMergePDF(sAttachFilePath, sMergePwdPDFPath, sPassword, ref sError))
                            bFileGenerated = true;
                        else
                            return sError + "For Claim Id " + objClaim.ClaimId + ".";
                    }
                    else
                        return sError + "For Claim Id " + objClaim.ClaimId + ".";
                }
                else
                {
                    return "FROI ACORD PDF/FDF form was not generated" + "For Claim Id " + objClaim.ClaimId + ".";
                }
                //Merged File was created using Adobe writer
                if (bFileGenerated)
                {
                    //objMemory = new MemoryStream();
                    ////get the binary format of the file to be attached to Claim
                    //GetFileStream(sAttachFilePath, ref objMemory);
                    ////PDF Form Generated - it will be attached to Claim
                    //AttachDocument(objMemory, objClaim);
                    if (bFroi)
                    {
                        sSubjectAttach = objClaim.ClaimNumber + ", First Report of Injury";
                        sSubjectPwd = objClaim.ClaimNumber + ", First Report of Injury Password ";
                        sBodyPwd = "Password of First Report of Injury for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                        sBodyAttach = "Attached is the First Report of Injury for Claim " + objClaim.ClaimNumber + "." + "\r\n" + "\r\n" + "The attached file has been encrypted and password protected for your data security." + "\r\n" + "\r\n" + "Password has been sent in a separate email." + "\r\n";
                        sAtchSub = "First Report of Injury or Illness";
                    }
                    else if (bAcord)
                    {
                        sSubjectAttach = objClaim.ClaimNumber + ", ACORD Form";
                        sSubjectPwd = objClaim.ClaimNumber + ", ACORD Form Password ";
                        sBodyPwd = "Password of ACORD Form for Claim " + objClaim.ClaimNumber + " is " + sPassword + ".";
                        sBodyAttach = "Attached is the ACORD Form for Claim " + objClaim.ClaimNumber + "." + "\r\n" + "\r\n" + "The attached file has been encrypted and password protected for your data security." + "\r\n" + "\r\n" + "Password has been sent in a separate email." + "\r\n";
                        sAtchSub = "ACORD Form";
                    }
                    //First Mail with the attachement
                    bMailSent = sendMail(sSubjectAttach, sBodyAttach, sEmail, sMergePwdPDFPath, ref sError);
                    if (bMailSent)
                    {
                        //Second Mail with the details of Password.
                        bMailSent = sendMail(sSubjectPwd, sBodyPwd, sEmail, "", ref sError);
                        if (bMailSent)
                        {
                            if (chkUpdateTime.Checked)
                            {
                                objClaim.MailSent = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                                objClaim.Save();
                            }
                            //objMemory = new MemoryStream();
                            ////get the binary format of the file to be attached to Claim
                            //GetFileStream(sAttachFilePath, ref objMemory);
                            ////PDF Form Generated - it will be attached to Claim
                            ////AttachDocument(objMemory, objClaim);
                            return "Success" + "For Claim Id " + objClaim.ClaimId + ".";
                        }
                        else
                        {
                            return sError + "For Claim Id " + objClaim.ClaimId + ".";
                        }
                    }
                    else
                    {
                        return sError + "For Claim Id " + objClaim.ClaimId + ".";
                    }
                }
                else
                {
                    //return "Merged PDF was not generated.";
                    return sError + "For Claim Id " + objClaim.ClaimId + ".";
                }
            }
            catch (Exception ex)
            {
                return "Error Message in generateFROIACORD function: " + ex.Message + "For Claim Id " + objClaim.ClaimId + ".";
            }
            finally
            {
                if (System.IO.File.Exists(sAttachFilePath))
                    System.IO.File.Delete(sAttachFilePath);
                if (System.IO.File.Exists(sMergePDFPath))
                    System.IO.File.Delete(sMergePDFPath);
                if (System.IO.File.Exists(sFDFPath))
                    System.IO.File.Delete(sFDFPath);
                if (System.IO.File.Exists(sMergePwdPDFPath))
                    System.IO.File.Delete(sMergePwdPDFPath);
                objAutoFROI = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
                if (rdr != null)
                    rdr.Close();
                rdr = null;
            }

        }
        private XmlDocument GetFROIXml(int iClaimId, string iFormId)
        {

            XmlDocument objXmlDocument = null;
            StringBuilder sbSql = null;

            objXmlDocument = new XmlDocument();
            sbSql = new StringBuilder();
            sbSql.Append("<FROI>");
            sbSql.Append("<ClaimId>" + iClaimId + "</ClaimId>");
            sbSql.Append("<FormId>" + iFormId + "</FormId>");
            sbSql.Append("<State></State>");
            sbSql.Append("<Name></Name>");
            sbSql.Append("<Title></Title>");
            sbSql.Append("<Phone></Phone>");
            sbSql.Append("<DocumentPath></DocumentPath>");
            sbSql.Append("<AttachForm></AttachForm>");
            sbSql.Append("<DocStorageType></DocStorageType>");
            sbSql.Append("<RequestHost></RequestHost>");
            sbSql.Append("</FROI>");
            objXmlDocument.LoadXml(sbSql.ToString());
            return objXmlDocument;
        }
        public bool MergeFDFTOPDF(string sTemplateFile, string sFDFFile, string finalFile, ref string sError)
        {

            try
            {

                using (FileStream file1 = new FileStream(sTemplateFile, FileMode.Open, FileAccess.Read))
                using (FileStream file2 = new FileStream(sFDFFile, FileMode.Open, FileAccess.Read))
                using (FileStream file3 = new FileStream(finalFile, FileMode.Create, FileAccess.Write))
                using (IacDocument doc2 = new IacDocument(null))
                using (IacDocument doc1 = new IacDocument(null))
                {
                    // Opend the pdf file we want to add watermarks to
                    doc1.Open(file1, "");

                    // Open the watermark pdf file
                    doc2.Open(file2, "");

                    // merge the two documents: doc2 is merged into doc1.
                    doc1.Merge(doc2, 1);  // 2nd arg: 1=merge doc2 to every page of doc1
                    //          2=merge doc2 to the first page of doc1

                    // save the result to a third file                    
                    doc1.Save(file3);
                    file3.Close();
                    file3.Dispose();
                    doc1.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                sError = "Failed, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Encrypt Merged PDF using Amyuni DLL
        /// </summary>
        /// <param name="sMergeFinalPDF"></param>
        /// <param name="sSecuredFile"></param>
        /// <param name="sPassword"></param>
        /// <returns></returns>
        public bool EncryptMergePDF(string sMergeFinalPDF, string sSecuredFile, string sPassword, ref string sError)
        {

            UInt32 permissions;
            IacDocument document = new IacDocument(null);
            try
            {
                using (FileStream MergedFile = new System.IO.FileStream(sMergeFinalPDF, FileMode.Open))
                {
                    using (FileStream encryptfile = new System.IO.FileStream(sSecuredFile, FileMode.Create))
                    {

                        permissions = 0xFFFFFFC0 + 0x4 + 0x8;

                        //Open the pdf document, and save the Encrypted file
                        document.Open(MergedFile, "");
                        document.Encrypt128(sPassword, sPassword, permissions);
                        document.Save(encryptfile, 0);

                        //Close and Dispose Encrypted File
                        encryptfile.Close();
                    }//using

                    //Close and Dispose Merged File
                    MergedFile.Close();
                }//using

                document.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                sError = "Failed, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
                if (document != null)
                    document.Dispose();
            }
        }

        public bool sendMail(string sSubject, string sBody, string sToAddress, string sAttachmentPath, ref string sError)
        {
            MailMessage objMailMessage = null;
            Attachment objAttachment = null;
            string m_sSecurityDSN = string.Empty, sSQL = string.Empty, sFromAddress = string.Empty;

            try
            {

                sSQL = "SELECT EMAILFROIACORDFROM FROM EMAIL_FROI_ACORD";
                string strEmailAcordFrom = DbFactory.ExecuteScalar(m_sConnectionString, sSQL) as string;
                if (!string.IsNullOrEmpty(strEmailAcordFrom))
                {
                    sFromAddress = strEmailAcordFrom;
                }

                sToAddress = validEmails(sToAddress);
                objMailMessage = new MailMessage(sFromAddress, sToAddress);
                objMailMessage.Subject = sSubject;
                objMailMessage.Body = sBody;
                if (File.Exists(sAttachmentPath))
                {
                    objAttachment = new Attachment(sAttachmentPath);
                    objMailMessage.Attachments.Add(objAttachment);
                }
                Mailer.SendMail(objMailMessage);
                return true;
            }
            catch (SmtpException ex)
            {
                sError = "Failed in Send Mail Function, Error Message: " + ex.Message;
                return false;
            }
            finally
            {
                if (objMailMessage != null)
                    objMailMessage.Dispose();
                if (objAttachment != null)
                    objAttachment.Dispose();
            }
        }
        public string validEmails(string strEmails)
        {
            try
            {
                string sValidAddresses;
                string[] strEmail = strEmails.Split(new char[] { ',' });
                bool blnReturn = true;
                sValidAddresses = "";
                foreach (string strMail in strEmail)
                {
                    blnReturn = validateEmailAddress(strMail);
                    if (blnReturn)
                    {
                        sValidAddresses = sValidAddresses + strMail + ",";
                    }
                }
                if (sValidAddresses != "")
                { sValidAddresses = sValidAddresses.Substring(0, sValidAddresses.Length - 1); }

                return sValidAddresses;
            }
            catch (Exception ex)
            {
                return "validEmails, Error Message is:" + ex.Message;
            }
        }

        /// <summary>
        /// Function to Validate Single e-mail address
        /// </summary>
        /// <param name="strEmailAdd"></param>
        /// <returns></returns>
        public bool validateEmailAddress(string strEmailAdd)
        {
            Regex objRegex = null;
            try
            {
                objRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                {
                    if (objRegex.IsMatch(strEmailAdd))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
            finally
            { objRegex = null; }
        }

        private void UpdateStatus(string sMessage, int iProgressBar)
        {
            rtbOutput.Text = rtbOutput.Text + sMessage + Environment.NewLine;
            //pbClaimProgress.Step += 1;
            pbClaimProgress.PerformStep();
        }

        private void chkAttach_Click(object sender, EventArgs e)
        {
            if (!chkAttach.Checked)
            {
                MessageBox.Show("Not Implemented");
            }
        }


    }
    
}
