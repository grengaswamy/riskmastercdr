﻿namespace SendMail4AutoAcord
{
    partial class frmSendMail4AutoAcordFROI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblClaimIds = new DevComponents.DotNetBar.LabelX();
            this.chkSendMailtoAssociatedClaim = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.pbClaimProgress = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.rdbCheckWithMail = new System.Windows.Forms.RadioButton();
            this.rdbCheckWithoutMail = new System.Windows.Forms.RadioButton();
            this.rdbDoNotCheckCriteria = new System.Windows.Forms.RadioButton();
            this.lblOutput = new DevComponents.DotNetBar.LabelX();
            this.btnSendMail = new DevComponents.DotNetBar.ButtonX();
            this.chkAttach = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkUpdateTime = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.txtClaimIds = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // lblClaimIds
            // 
            // 
            // 
            // 
            this.lblClaimIds.BackgroundStyle.Class = "";
            this.lblClaimIds.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblClaimIds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClaimIds.Location = new System.Drawing.Point(1, 1);
            this.lblClaimIds.Name = "lblClaimIds";
            this.lblClaimIds.Size = new System.Drawing.Size(406, 42);
            this.lblClaimIds.TabIndex = 4;
            this.lblClaimIds.Text = "Enter the Comma Separated <b>Claim Ids</b> for which the <b>Mail </b>needs to be " +
    "Sent";
            this.lblClaimIds.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblClaimIds.WordWrap = true;
            // 
            // chkSendMailtoAssociatedClaim
            // 
            // 
            // 
            // 
            this.chkSendMailtoAssociatedClaim.BackgroundStyle.Class = "";
            this.chkSendMailtoAssociatedClaim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkSendMailtoAssociatedClaim.Location = new System.Drawing.Point(1, 195);
            this.chkSendMailtoAssociatedClaim.Name = "chkSendMailtoAssociatedClaim";
            this.chkSendMailtoAssociatedClaim.Size = new System.Drawing.Size(238, 23);
            this.chkSendMailtoAssociatedClaim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkSendMailtoAssociatedClaim.TabIndex = 6;
            this.chkSendMailtoAssociatedClaim.Text = "Send EMail to Mail Id associated with Claim";
            // 
            // pbClaimProgress
            // 
            // 
            // 
            // 
            this.pbClaimProgress.BackgroundStyle.Class = "";
            this.pbClaimProgress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pbClaimProgress.Location = new System.Drawing.Point(1, 219);
            this.pbClaimProgress.Name = "pbClaimProgress";
            this.pbClaimProgress.Size = new System.Drawing.Size(501, 23);
            this.pbClaimProgress.TabIndex = 7;
            this.pbClaimProgress.Text = "progressBarX1";
            // 
            // rdbCheckWithMail
            // 
            this.rdbCheckWithMail.AutoSize = true;
            this.rdbCheckWithMail.Location = new System.Drawing.Point(1, 140);
            this.rdbCheckWithMail.Name = "rdbCheckWithMail";
            this.rdbCheckWithMail.Size = new System.Drawing.Size(234, 17);
            this.rdbCheckWithMail.TabIndex = 8;
            this.rdbCheckWithMail.TabStop = true;
            this.rdbCheckWithMail.Text = "Check Triggering Criteria With Mail Id Option";
            this.rdbCheckWithMail.UseVisualStyleBackColor = true;
            // 
            // rdbCheckWithoutMail
            // 
            this.rdbCheckWithoutMail.AutoSize = true;
            this.rdbCheckWithoutMail.Location = new System.Drawing.Point(1, 159);
            this.rdbCheckWithoutMail.Name = "rdbCheckWithoutMail";
            this.rdbCheckWithoutMail.Size = new System.Drawing.Size(249, 17);
            this.rdbCheckWithoutMail.TabIndex = 9;
            this.rdbCheckWithoutMail.TabStop = true;
            this.rdbCheckWithoutMail.Text = "Check Triggering Criteria Without Mail Id Option";
            this.rdbCheckWithoutMail.UseVisualStyleBackColor = true;
            // 
            // rdbDoNotCheckCriteria
            // 
            this.rdbDoNotCheckCriteria.AutoSize = true;
            this.rdbDoNotCheckCriteria.ForeColor = System.Drawing.Color.Black;
            this.rdbDoNotCheckCriteria.Location = new System.Drawing.Point(1, 177);
            this.rdbDoNotCheckCriteria.Name = "rdbDoNotCheckCriteria";
            this.rdbDoNotCheckCriteria.Size = new System.Drawing.Size(176, 17);
            this.rdbDoNotCheckCriteria.TabIndex = 10;
            this.rdbDoNotCheckCriteria.TabStop = true;
            this.rdbDoNotCheckCriteria.Text = "Do not Check Triggering Criteria";
            this.rdbDoNotCheckCriteria.UseVisualStyleBackColor = true;
            // 
            // lblOutput
            // 
            // 
            // 
            // 
            this.lblOutput.BackgroundStyle.Class = "";
            this.lblOutput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(1, 241);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(68, 22);
            this.lblOutput.TabIndex = 12;
            this.lblOutput.Text = "Output:-";
            this.lblOutput.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblOutput.WordWrap = true;
            // 
            // btnSendMail
            // 
            this.btnSendMail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSendMail.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSendMail.Location = new System.Drawing.Point(220, 390);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(75, 23);
            this.btnSendMail.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSendMail.TabIndex = 13;
            this.btnSendMail.Text = "Send Mail";
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // chkAttach
            // 
            // 
            // 
            // 
            this.chkAttach.BackgroundStyle.Class = "";
            this.chkAttach.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAttach.Location = new System.Drawing.Point(245, 117);
            this.chkAttach.Name = "chkAttach";
            this.chkAttach.Size = new System.Drawing.Size(273, 67);
            this.chkAttach.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAttach.TabIndex = 14;
            this.chkAttach.Text = "Check whether the Attachment is already Attached \r\nIf the Attachment is not alrea" +
    "dy Attached it will \r\nalso attach the Attachment";
            this.chkAttach.Click += new System.EventHandler(this.chkAttach_Click);
            // 
            // chkUpdateTime
            // 
            // 
            // 
            // 
            this.chkUpdateTime.BackgroundStyle.Class = "";
            this.chkUpdateTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkUpdateTime.Location = new System.Drawing.Point(245, 190);
            this.chkUpdateTime.Name = "chkUpdateTime";
            this.chkUpdateTime.Size = new System.Drawing.Size(166, 23);
            this.chkUpdateTime.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkUpdateTime.TabIndex = 15;
            this.chkUpdateTime.Text = "Update Email Sending Time";
            // 
            // rtbOutput
            // 
            this.rtbOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rtbOutput.Location = new System.Drawing.Point(1, 260);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(501, 124);
            this.rtbOutput.TabIndex = 16;
            this.rtbOutput.Text = "";
            // 
            // txtClaimIds
            // 
            this.txtClaimIds.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtClaimIds.Location = new System.Drawing.Point(1, 49);
            this.txtClaimIds.Name = "txtClaimIds";
            this.txtClaimIds.Size = new System.Drawing.Size(501, 85);
            this.txtClaimIds.TabIndex = 17;
            this.txtClaimIds.Text = "";
            // 
            // frmSendMail4AutoAcordFROI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 425);
            this.Controls.Add(this.txtClaimIds);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.chkUpdateTime);
            this.Controls.Add(this.chkAttach);
            this.Controls.Add(this.btnSendMail);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.rdbDoNotCheckCriteria);
            this.Controls.Add(this.rdbCheckWithoutMail);
            this.Controls.Add(this.rdbCheckWithMail);
            this.Controls.Add(this.pbClaimProgress);
            this.Controls.Add(this.chkSendMailtoAssociatedClaim);
            this.Controls.Add(this.lblClaimIds);
            this.Name = "frmSendMail4AutoAcordFROI";
            this.Text = "Send Mail 4 Auto Acord/FROI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblClaimIds;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkSendMailtoAssociatedClaim;
        private DevComponents.DotNetBar.Controls.ProgressBarX pbClaimProgress;
        private System.Windows.Forms.RadioButton rdbCheckWithMail;
        private System.Windows.Forms.RadioButton rdbCheckWithoutMail;
        private System.Windows.Forms.RadioButton rdbDoNotCheckCriteria;
        private DevComponents.DotNetBar.LabelX lblOutput;
        private DevComponents.DotNetBar.ButtonX btnSendMail;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkUpdateTime;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAttach;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.RichTextBox txtClaimIds;
    }
}

