﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.DataModel;

namespace SendMail4AutoAcord
{
    public partial class frmMain : Form
    {
        private DataModelFactory m_objDataModelFactory = null;
        private string m_sConnectionString = string.Empty;
        private UserLogin m_objUserLogin = null;

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                frmSendMail4AutoAcknowledgement frmSendMailAutoAck = new frmSendMail4AutoAcknowledgement(m_objUserLogin, m_objDataModelFactory);
                frmSendMailAutoAck.ShowDialog();
            }
            else if (radioButton2.Checked)
            {
                frmSendMail4AutoAcordFROI frmSendMailAutoAccord = new frmSendMail4AutoAcordFROI(m_objDataModelFactory);
                frmSendMailAutoAccord.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please select an option");
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Login objLogin = null;
            //UserLogin oUserLogin = null;
            objLogin = new Login();
            bool bLogin = false;

            bLogin = objLogin.RegisterApplication(0, 0, ref m_objUserLogin);
            if (bLogin)
            {
                m_objDataModelFactory = new DataModelFactory(m_objUserLogin);
                //rdbDoNotCheckCriteria.Checked = true;
                //rtbOutput.ReadOnly = true;
            }
            else
            {
                MessageBox.Show("Please provide valid credentials", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }
    }
}
