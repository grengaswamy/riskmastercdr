﻿namespace SendMail4AutoAcord
{
    partial class frmSendMail4AutoAcknowledgement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnSendMail = new DevComponents.DotNetBar.ButtonX();
            this.lblOutput = new DevComponents.DotNetBar.LabelX();
            this.rdbClosingClaim = new System.Windows.Forms.RadioButton();
            this.rdbAcknowledgement = new System.Windows.Forms.RadioButton();
            this.pbClaimProgress = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.lblClaimIds = new DevComponents.DotNetBar.LabelX();
            this.txtClaimIds = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbOutput
            // 
            this.rtbOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rtbOutput.Location = new System.Drawing.Point(-1, 265);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(501, 124);
            this.rtbOutput.TabIndex = 28;
            this.rtbOutput.Text = "";
            // 
            // btnSendMail
            // 
            this.btnSendMail.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSendMail.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSendMail.Location = new System.Drawing.Point(218, 395);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(75, 23);
            this.btnSendMail.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSendMail.TabIndex = 25;
            this.btnSendMail.Text = "Send Mail";
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // lblOutput
            // 
            // 
            // 
            // 
            this.lblOutput.BackgroundStyle.Class = "";
            this.lblOutput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(-1, 246);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(68, 22);
            this.lblOutput.TabIndex = 24;
            this.lblOutput.Text = "Output:-";
            this.lblOutput.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblOutput.WordWrap = true;
            // 
            // rdbClosingClaim
            // 
            this.rdbClosingClaim.AutoSize = true;
            this.rdbClosingClaim.Location = new System.Drawing.Point(287, 23);
            this.rdbClosingClaim.Name = "rdbClosingClaim";
            this.rdbClosingClaim.Size = new System.Drawing.Size(87, 17);
            this.rdbClosingClaim.TabIndex = 22;
            this.rdbClosingClaim.Text = "Closing Claim";
            this.rdbClosingClaim.UseVisualStyleBackColor = true;
            // 
            // rdbAcknowledgement
            // 
            this.rdbAcknowledgement.AutoSize = true;
            this.rdbAcknowledgement.Checked = true;
            this.rdbAcknowledgement.Location = new System.Drawing.Point(56, 23);
            this.rdbAcknowledgement.Name = "rdbAcknowledgement";
            this.rdbAcknowledgement.Size = new System.Drawing.Size(113, 17);
            this.rdbAcknowledgement.TabIndex = 21;
            this.rdbAcknowledgement.TabStop = true;
            this.rdbAcknowledgement.Text = "Acknowledgement";
            this.rdbAcknowledgement.UseVisualStyleBackColor = true;
            // 
            // pbClaimProgress
            // 
            // 
            // 
            // 
            this.pbClaimProgress.BackgroundStyle.Class = "";
            this.pbClaimProgress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pbClaimProgress.Location = new System.Drawing.Point(-1, 224);
            this.pbClaimProgress.Name = "pbClaimProgress";
            this.pbClaimProgress.Size = new System.Drawing.Size(501, 23);
            this.pbClaimProgress.TabIndex = 20;
            this.pbClaimProgress.Text = "progressBarX1";
            // 
            // lblClaimIds
            // 
            // 
            // 
            // 
            this.lblClaimIds.BackgroundStyle.Class = "";
            this.lblClaimIds.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblClaimIds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClaimIds.Location = new System.Drawing.Point(-1, 6);
            this.lblClaimIds.Name = "lblClaimIds";
            this.lblClaimIds.Size = new System.Drawing.Size(406, 42);
            this.lblClaimIds.TabIndex = 18;
            this.lblClaimIds.Text = "Enter the Comma Separated <b>Claim Ids</b> for which the <b>Mail </b>needs to be " +
    "Sent";
            this.lblClaimIds.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblClaimIds.WordWrap = true;
            // 
            // txtClaimIds
            // 
            // 
            // 
            // 
            this.txtClaimIds.Border.Class = "TextBoxBorder";
            this.txtClaimIds.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtClaimIds.Location = new System.Drawing.Point(-1, 43);
            this.txtClaimIds.Multiline = true;
            this.txtClaimIds.Name = "txtClaimIds";
            this.txtClaimIds.Size = new System.Drawing.Size(501, 96);
            this.txtClaimIds.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbClosingClaim);
            this.groupBox1.Controls.Add(this.rdbAcknowledgement);
            this.groupBox1.Location = new System.Drawing.Point(-1, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(501, 64);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose the Type of Letter to be Sent:";
            // 
            // frmSendMail4AutoAcknowledgement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 425);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtClaimIds);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.btnSendMail);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.pbClaimProgress);
            this.Controls.Add(this.lblClaimIds);
            this.Name = "frmSendMail4AutoAcknowledgement";
            this.Text = "Send Mail 4 Auto Acord/FROI";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbOutput;
        private DevComponents.DotNetBar.ButtonX btnSendMail;
        private DevComponents.DotNetBar.LabelX lblOutput;
        private System.Windows.Forms.RadioButton rdbClosingClaim;
        private System.Windows.Forms.RadioButton rdbAcknowledgement;
        private DevComponents.DotNetBar.Controls.ProgressBarX pbClaimProgress;
        private DevComponents.DotNetBar.LabelX lblClaimIds;
        private DevComponents.DotNetBar.Controls.TextBoxX txtClaimIds;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}

