﻿namespace RiskmasterSettingsEditor
{
    partial class frmRiskmasterSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRiskmasterSettings));
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Registry Settings");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Reporting Settings");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Save");
            Infragistics.Win.UltraWinToolbars.RibbonTab ribbonTab1 = new Infragistics.Win.UltraWinToolbars.RibbonTab("ComputerAdmin");
            Infragistics.Win.UltraWinToolbars.RibbonGroup ribbonGroup1 = new Infragistics.Win.UltraWinToolbars.RibbonGroup("rmSettingsGroup");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("IIS Management");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Services Management");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ODBC Management");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Save");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Save");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Settings");
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Close");
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Reporting Settings");
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("IIS Management");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Services Management");
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ODBC Management");
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Riskmaster World Registry Settings");
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Load Registry Settings");
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.lblRiskSecPwd = new System.Windows.Forms.Label();
            this.lblRiskSecUserID = new System.Windows.Forms.Label();
            this.txtSecPwd = new System.Windows.Forms.TextBox();
            this.txtSecUID = new System.Windows.Forms.TextBox();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.lblRMXSession = new System.Windows.Forms.Label();
            this.lblRMXSecurity = new System.Windows.Forms.Label();
            this.txtRMXSessionConnString = new System.Windows.Forms.TextBox();
            this.txtRMXSecurityConnString = new System.Windows.Forms.TextBox();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.lblSMTPServer = new System.Windows.Forms.Label();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.Form1_Fill_Panel = new System.Windows.Forms.Panel();
            this.utTabsRiskmasterSettings = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this._Form1_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.utMgrRiskmasterSettings = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._Form1_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Form1_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._Form1_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.txtRMSecPwd = new System.Windows.Forms.TextBox();
            this.lblRMSecPwd = new System.Windows.Forms.Label();
            this.lblRMSecUserID = new System.Windows.Forms.Label();
            this.txtRMSecUserID = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ofdRMConfigPath = new System.Windows.Forms.OpenFileDialog();
            this.ofdRMGlobalASAPath = new System.Windows.Forms.OpenFileDialog();
            this.lblSortmasterUserID = new System.Windows.Forms.Label();
            this.lblSortmasterPwd = new System.Windows.Forms.Label();
            this.txtSMServerUID = new System.Windows.Forms.TextBox();
            this.txtSMServerPwd = new System.Windows.Forms.TextBox();
            this.ultraTabPageControl1.SuspendLayout();
            this.ultraTabPageControl5.SuspendLayout();
            this.Form1_Fill_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utTabsRiskmasterSettings)).BeginInit();
            this.utTabsRiskmasterSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utMgrRiskmasterSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.lblRiskSecPwd);
            this.ultraTabPageControl1.Controls.Add(this.lblRiskSecUserID);
            this.ultraTabPageControl1.Controls.Add(this.txtSecPwd);
            this.ultraTabPageControl1.Controls.Add(this.txtSecUID);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(625, 351);
            // 
            // lblRiskSecPwd
            // 
            this.lblRiskSecPwd.AutoSize = true;
            this.lblRiskSecPwd.Location = new System.Drawing.Point(27, 99);
            this.lblRiskSecPwd.Name = "lblRiskSecPwd";
            this.lblRiskSecPwd.Size = new System.Drawing.Size(56, 13);
            this.lblRiskSecPwd.TabIndex = 3;
            this.lblRiskSecPwd.Text = "Password:";
            // 
            // lblRiskSecUserID
            // 
            this.lblRiskSecUserID.AutoSize = true;
            this.lblRiskSecUserID.Location = new System.Drawing.Point(27, 54);
            this.lblRiskSecUserID.Name = "lblRiskSecUserID";
            this.lblRiskSecUserID.Size = new System.Drawing.Size(46, 13);
            this.lblRiskSecUserID.TabIndex = 1;
            this.lblRiskSecUserID.Text = "User ID:";
            // 
            // txtSecPwd
            // 
            this.txtSecPwd.Location = new System.Drawing.Point(94, 96);
            this.txtSecPwd.Name = "txtSecPwd";
            this.txtSecPwd.Size = new System.Drawing.Size(100, 20);
            this.txtSecPwd.TabIndex = 4;
            this.txtSecPwd.UseSystemPasswordChar = true;
            // 
            // txtSecUID
            // 
            this.txtSecUID.Location = new System.Drawing.Point(94, 51);
            this.txtSecUID.Name = "txtSecUID";
            this.txtSecUID.Size = new System.Drawing.Size(100, 20);
            this.txtSecUID.TabIndex = 2;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.txtSMServerPwd);
            this.ultraTabPageControl5.Controls.Add(this.txtSMServerUID);
            this.ultraTabPageControl5.Controls.Add(this.lblSortmasterPwd);
            this.ultraTabPageControl5.Controls.Add(this.lblSortmasterUserID);
            this.ultraTabPageControl5.Controls.Add(this.lblRMXSession);
            this.ultraTabPageControl5.Controls.Add(this.lblRMXSecurity);
            this.ultraTabPageControl5.Controls.Add(this.txtRMXSessionConnString);
            this.ultraTabPageControl5.Controls.Add(this.txtRMXSecurityConnString);
            this.ultraTabPageControl5.Controls.Add(this.txtSMTPServer);
            this.ultraTabPageControl5.Controls.Add(this.lblSMTPServer);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(625, 351);
            // 
            // lblRMXSession
            // 
            this.lblRMXSession.AutoSize = true;
            this.lblRMXSession.Location = new System.Drawing.Point(31, 76);
            this.lblRMXSession.Name = "lblRMXSession";
            this.lblRMXSession.Size = new System.Drawing.Size(207, 13);
            this.lblRMXSession.TabIndex = 18;
            this.lblRMXSession.Text = "RISKMASTER Session Connection String:";
            // 
            // lblRMXSecurity
            // 
            this.lblRMXSecurity.AutoSize = true;
            this.lblRMXSecurity.Location = new System.Drawing.Point(31, 36);
            this.lblRMXSecurity.Name = "lblRMXSecurity";
            this.lblRMXSecurity.Size = new System.Drawing.Size(208, 13);
            this.lblRMXSecurity.TabIndex = 17;
            this.lblRMXSecurity.Text = "RISKMASTER Security Connection String:";
            // 
            // txtRMXSessionConnString
            // 
            this.txtRMXSessionConnString.Location = new System.Drawing.Point(244, 73);
            this.txtRMXSessionConnString.Name = "txtRMXSessionConnString";
            this.txtRMXSessionConnString.Size = new System.Drawing.Size(217, 20);
            this.txtRMXSessionConnString.TabIndex = 16;
            // 
            // txtRMXSecurityConnString
            // 
            this.txtRMXSecurityConnString.Location = new System.Drawing.Point(245, 33);
            this.txtRMXSecurityConnString.Name = "txtRMXSecurityConnString";
            this.txtRMXSecurityConnString.Size = new System.Drawing.Size(217, 20);
            this.txtRMXSecurityConnString.TabIndex = 15;
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.Location = new System.Drawing.Point(245, 184);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(217, 20);
            this.txtSMTPServer.TabIndex = 14;
            // 
            // lblSMTPServer
            // 
            this.lblSMTPServer.AutoSize = true;
            this.lblSMTPServer.Location = new System.Drawing.Point(31, 187);
            this.lblSMTPServer.Name = "lblSMTPServer";
            this.lblSMTPServer.Size = new System.Drawing.Size(115, 13);
            this.lblSMTPServer.TabIndex = 13;
            this.lblSMTPServer.Text = "SMTP Server Address:";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "");
            this.imageList2.Images.SetKeyName(4, "");
            this.imageList2.Images.SetKeyName(5, "");
            this.imageList2.Images.SetKeyName(6, "");
            this.imageList2.Images.SetKeyName(7, "");
            this.imageList2.Images.SetKeyName(8, "");
            this.imageList2.Images.SetKeyName(9, "");
            this.imageList2.Images.SetKeyName(10, "");
            this.imageList2.Images.SetKeyName(11, "");
            this.imageList2.Images.SetKeyName(12, "");
            this.imageList2.Images.SetKeyName(13, "");
            this.imageList2.Images.SetKeyName(14, "");
            this.imageList2.Images.SetKeyName(15, "");
            this.imageList2.Images.SetKeyName(16, "");
            this.imageList2.Images.SetKeyName(17, "");
            this.imageList2.Images.SetKeyName(18, "");
            this.imageList2.Images.SetKeyName(19, "");
            this.imageList2.Images.SetKeyName(20, "");
            this.imageList2.Images.SetKeyName(21, "");
            this.imageList2.Images.SetKeyName(22, "");
            this.imageList2.Images.SetKeyName(23, "");
            this.imageList2.Images.SetKeyName(24, "");
            this.imageList2.Images.SetKeyName(25, "");
            this.imageList2.Images.SetKeyName(26, "");
            this.imageList2.Images.SetKeyName(27, "");
            this.imageList2.Images.SetKeyName(28, "");
            this.imageList2.Images.SetKeyName(29, "");
            this.imageList2.Images.SetKeyName(30, "");
            this.imageList2.Images.SetKeyName(31, "");
            this.imageList2.Images.SetKeyName(32, "");
            this.imageList2.Images.SetKeyName(33, "");
            this.imageList2.Images.SetKeyName(34, "");
            this.imageList2.Images.SetKeyName(35, "");
            this.imageList2.Images.SetKeyName(36, "");
            this.imageList2.Images.SetKeyName(37, "");
            this.imageList2.Images.SetKeyName(38, "");
            this.imageList2.Images.SetKeyName(39, "");
            this.imageList2.Images.SetKeyName(40, "");
            this.imageList2.Images.SetKeyName(41, "");
            this.imageList2.Images.SetKeyName(42, "");
            this.imageList2.Images.SetKeyName(43, "");
            this.imageList2.Images.SetKeyName(44, "");
            this.imageList2.Images.SetKeyName(45, "");
            this.imageList2.Images.SetKeyName(46, "");
            this.imageList2.Images.SetKeyName(47, "");
            this.imageList2.Images.SetKeyName(48, "");
            this.imageList2.Images.SetKeyName(49, "");
            this.imageList2.Images.SetKeyName(50, "");
            this.imageList2.Images.SetKeyName(51, "");
            this.imageList2.Images.SetKeyName(52, "");
            // 
            // Form1_Fill_Panel
            // 
            this.Form1_Fill_Panel.Controls.Add(this.utTabsRiskmasterSettings);
            this.Form1_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Form1_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Form1_Fill_Panel.Location = new System.Drawing.Point(4, 155);
            this.Form1_Fill_Panel.Name = "Form1_Fill_Panel";
            this.Form1_Fill_Panel.Size = new System.Drawing.Size(630, 270);
            this.Form1_Fill_Panel.TabIndex = 0;
            // 
            // utTabsRiskmasterSettings
            // 
            this.utTabsRiskmasterSettings.Controls.Add(this.ultraTabSharedControlsPage1);
            this.utTabsRiskmasterSettings.Controls.Add(this.ultraTabPageControl1);
            this.utTabsRiskmasterSettings.Controls.Add(this.ultraTabPageControl5);
            this.utTabsRiskmasterSettings.Location = new System.Drawing.Point(3, 0);
            this.utTabsRiskmasterSettings.Name = "utTabsRiskmasterSettings";
            this.utTabsRiskmasterSettings.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.utTabsRiskmasterSettings.Size = new System.Drawing.Size(627, 374);
            this.utTabsRiskmasterSettings.TabIndex = 0;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Riskmaster Security";
            ultraTab7.TabPage = this.ultraTabPageControl5;
            ultraTab7.Text = "Sortmaster Settings";
            this.utTabsRiskmasterSettings.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab7});
            this.utTabsRiskmasterSettings.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(1, 22);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(625, 351);
            // 
            // _Form1_Toolbars_Dock_Area_Left
            // 
            this._Form1_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._Form1_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4;
            this._Form1_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 155);
            this._Form1_Toolbars_Dock_Area_Left.Name = "_Form1_Toolbars_Dock_Area_Left";
            this._Form1_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(4, 270);
            this._Form1_Toolbars_Dock_Area_Left.ToolbarsManager = this.utMgrRiskmasterSettings;
            // 
            // utMgrRiskmasterSettings
            // 
            this.utMgrRiskmasterSettings.DesignerFlags = 1;
            this.utMgrRiskmasterSettings.DockWithinContainer = this;
            this.utMgrRiskmasterSettings.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.utMgrRiskmasterSettings.ImageListLarge = this.imageList2;
            this.utMgrRiskmasterSettings.ImageListSmall = this.imageList2;
            this.utMgrRiskmasterSettings.Ribbon.ApplicationMenu.ToolAreaLeft.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool16,
            buttonTool2,
            buttonTool3});
            ribbonTab1.Caption = "Computer Administration";
            ribbonGroup1.Caption = "";
            ribbonGroup1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool4,
            buttonTool5,
            buttonTool6});
            ribbonTab1.Groups.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonGroup[] {
            ribbonGroup1});
            this.utMgrRiskmasterSettings.Ribbon.NonInheritedRibbonTabs.AddRange(new Infragistics.Win.UltraWinToolbars.RibbonTab[] {
            ribbonTab1});
            this.utMgrRiskmasterSettings.Ribbon.QuickAccessToolbar.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool7});
            this.utMgrRiskmasterSettings.Ribbon.Visible = true;
            appearance1.Image = 51;
            buttonTool8.SharedProps.AppearancesSmall.Appearance = appearance1;
            buttonTool8.SharedProps.Caption = "Save";
            buttonTool8.SharedProps.DescriptionOnMenu = "Save pending Riskmaster Setting changes";
            appearance2.Image = 52;
            buttonTool9.SharedProps.AppearancesLarge.Appearance = appearance2;
            buttonTool9.SharedProps.Caption = "Load Settings";
            buttonTool9.SharedProps.DescriptionOnMenu = "Load Riskmaster Configuration Settings";
            buttonTool10.SharedProps.Caption = "Close";
            appearance3.Image = 52;
            buttonTool11.SharedProps.AppearancesLarge.Appearance = appearance3;
            buttonTool11.SharedProps.Caption = "Load Reporting Settings";
            buttonTool11.SharedProps.DescriptionOnMenu = "Load Riskmaster.Net/Sortmaster Settings";
            buttonTool11.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
            buttonTool12.SharedProps.AppearancesSmall.Appearance = appearance4;
            buttonTool12.SharedProps.Caption = "IIS Management";
            appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
            buttonTool13.SharedProps.AppearancesSmall.Appearance = appearance5;
            buttonTool13.SharedProps.Caption = "Services Management";
            appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
            buttonTool14.SharedProps.AppearancesSmall.Appearance = appearance6;
            buttonTool14.SharedProps.Caption = "ODBC Management";
            appearance17.Image = 52;
            buttonTool15.SharedProps.AppearancesLarge.Appearance = appearance17;
            appearance18.Image = 52;
            buttonTool15.SharedProps.AppearancesSmall.Appearance = appearance18;
            buttonTool15.SharedProps.Caption = "Load Riskmaster World Registry Settings";
            buttonTool15.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            appearance20.Image = 52;
            buttonTool17.SharedProps.AppearancesLarge.Appearance = appearance20;
            appearance19.Image = 52;
            buttonTool17.SharedProps.AppearancesSmall.Appearance = appearance19;
            buttonTool17.SharedProps.Caption = "Load Registry Settings";
            buttonTool17.SharedProps.CustomizerCaption = "Load RISKMASTER World Settings";
            buttonTool17.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
            this.utMgrRiskmasterSettings.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool8,
            buttonTool9,
            buttonTool10,
            buttonTool11,
            buttonTool12,
            buttonTool13,
            buttonTool14,
            buttonTool15,
            buttonTool17});
            this.utMgrRiskmasterSettings.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.utMgrRiskmasterSettings_ToolClick);
            // 
            // _Form1_Toolbars_Dock_Area_Right
            // 
            this._Form1_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._Form1_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4;
            this._Form1_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(634, 155);
            this._Form1_Toolbars_Dock_Area_Right.Name = "_Form1_Toolbars_Dock_Area_Right";
            this._Form1_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(4, 270);
            this._Form1_Toolbars_Dock_Area_Right.ToolbarsManager = this.utMgrRiskmasterSettings;
            // 
            // _Form1_Toolbars_Dock_Area_Top
            // 
            this._Form1_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._Form1_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._Form1_Toolbars_Dock_Area_Top.Name = "_Form1_Toolbars_Dock_Area_Top";
            this._Form1_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(638, 155);
            this._Form1_Toolbars_Dock_Area_Top.ToolbarsManager = this.utMgrRiskmasterSettings;
            // 
            // _Form1_Toolbars_Dock_Area_Bottom
            // 
            this._Form1_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._Form1_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this._Form1_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._Form1_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Form1_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 4;
            this._Form1_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 425);
            this._Form1_Toolbars_Dock_Area_Bottom.Name = "_Form1_Toolbars_Dock_Area_Bottom";
            this._Form1_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(638, 4);
            this._Form1_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.utMgrRiskmasterSettings;
            // 
            // txtRMSecPwd
            // 
            this.txtRMSecPwd.Location = new System.Drawing.Point(118, 136);
            this.txtRMSecPwd.Name = "txtRMSecPwd";
            this.txtRMSecPwd.Size = new System.Drawing.Size(100, 20);
            this.txtRMSecPwd.TabIndex = 1;
            // 
            // lblRMSecPwd
            // 
            this.lblRMSecPwd.AutoSize = true;
            this.lblRMSecPwd.BackColor = System.Drawing.Color.Transparent;
            this.lblRMSecPwd.Location = new System.Drawing.Point(33, 143);
            this.lblRMSecPwd.Name = "lblRMSecPwd";
            this.lblRMSecPwd.Size = new System.Drawing.Size(56, 13);
            this.lblRMSecPwd.TabIndex = 3;
            this.lblRMSecPwd.Text = "Password:";
            // 
            // lblRMSecUserID
            // 
            this.lblRMSecUserID.AutoSize = true;
            this.lblRMSecUserID.BackColor = System.Drawing.Color.Transparent;
            this.lblRMSecUserID.Location = new System.Drawing.Point(43, 92);
            this.lblRMSecUserID.Name = "lblRMSecUserID";
            this.lblRMSecUserID.Size = new System.Drawing.Size(46, 13);
            this.lblRMSecUserID.TabIndex = 2;
            this.lblRMSecUserID.Text = "User ID:";
            // 
            // txtRMSecUserID
            // 
            this.txtRMSecUserID.Location = new System.Drawing.Point(118, 85);
            this.txtRMSecUserID.Name = "txtRMSecUserID";
            this.txtRMSecUserID.Size = new System.Drawing.Size(100, 20);
            this.txtRMSecUserID.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(118, 136);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(33, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(43, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "User ID:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(118, 85);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(623, 348);
            // 
            // ofdRMConfigPath
            // 
            this.ofdRMConfigPath.Filter = "|*.config";
            this.ofdRMConfigPath.InitialDirectory = "C:\\Program Files\\CSC\\Riskmaster";
            // 
            // ofdRMGlobalASAPath
            // 
            this.ofdRMGlobalASAPath.FileName = "Global.asa";
            this.ofdRMGlobalASAPath.Filter = "ASA Files (*.asa)|*.asa";
            // 
            // lblSortmasterUserID
            // 
            this.lblSortmasterUserID.AutoSize = true;
            this.lblSortmasterUserID.Location = new System.Drawing.Point(31, 120);
            this.lblSortmasterUserID.Name = "lblSortmasterUserID";
            this.lblSortmasterUserID.Size = new System.Drawing.Size(125, 13);
            this.lblSortmasterUserID.TabIndex = 19;
            this.lblSortmasterUserID.Text = "Sortmaster DSN User ID:";
            // 
            // lblSortmasterPwd
            // 
            this.lblSortmasterPwd.AutoSize = true;
            this.lblSortmasterPwd.Location = new System.Drawing.Point(31, 152);
            this.lblSortmasterPwd.Name = "lblSortmasterPwd";
            this.lblSortmasterPwd.Size = new System.Drawing.Size(135, 13);
            this.lblSortmasterPwd.TabIndex = 20;
            this.lblSortmasterPwd.Text = "Sortmaster DSN Password:";
            // 
            // txtSMServerUID
            // 
            this.txtSMServerUID.Location = new System.Drawing.Point(245, 113);
            this.txtSMServerUID.Name = "txtSMServerUID";
            this.txtSMServerUID.Size = new System.Drawing.Size(100, 20);
            this.txtSMServerUID.TabIndex = 21;
            // 
            // txtSMServerPwd
            // 
            this.txtSMServerPwd.Location = new System.Drawing.Point(245, 149);
            this.txtSMServerPwd.Name = "txtSMServerPwd";
            this.txtSMServerPwd.Size = new System.Drawing.Size(100, 20);
            this.txtSMServerPwd.TabIndex = 22;
            this.txtSMServerPwd.UseSystemPasswordChar = true;
            // 
            // frmRiskmasterSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 429);
            this.Controls.Add(this.Form1_Fill_Panel);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._Form1_Toolbars_Dock_Area_Bottom);
            this.Name = "frmRiskmasterSettings";
            this.Text = "Riskmaster Settings Editor";
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.ultraTabPageControl5.PerformLayout();
            this.Form1_Fill_Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utTabsRiskmasterSettings)).EndInit();
            this.utTabsRiskmasterSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utMgrRiskmasterSettings)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager utMgrRiskmasterSettings;
        private System.Windows.Forms.Panel Form1_Fill_Panel;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _Form1_Toolbars_Dock_Area_Bottom;
        private System.Windows.Forms.ImageList imageList2;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl utTabsRiskmasterSettings;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private System.Windows.Forms.TextBox txtRMSecPwd;
        private System.Windows.Forms.Label lblRMSecPwd;
        private System.Windows.Forms.Label lblRMSecUserID;
        private System.Windows.Forms.TextBox txtRMSecUserID;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.OpenFileDialog ofdRMConfigPath;
        private System.Windows.Forms.OpenFileDialog ofdRMGlobalASAPath;
        private System.Windows.Forms.TextBox txtSecPwd;
        private System.Windows.Forms.TextBox txtSecUID;
        private System.Windows.Forms.Label lblRiskSecPwd;
        private System.Windows.Forms.Label lblRiskSecUserID;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.Label lblSMTPServer;
        private System.Windows.Forms.TextBox txtRMXSecurityConnString;
        private System.Windows.Forms.TextBox txtRMXSessionConnString;
        private System.Windows.Forms.Label lblRMXSession;
        private System.Windows.Forms.Label lblRMXSecurity;
        private System.Windows.Forms.TextBox txtSMServerPwd;
        private System.Windows.Forms.TextBox txtSMServerUID;
        private System.Windows.Forms.Label lblSortmasterPwd;
        private System.Windows.Forms.Label lblSortmasterUserID;
    }
}

