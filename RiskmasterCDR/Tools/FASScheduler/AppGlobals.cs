﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections;
using System.Configuration;

namespace Riskmaster.Tools.FASScheduler
{
    public class AppGlobals
    {
        public static UserLogin Userlogin;
        public static string ConnectionString;


        public static bool bDeleteFileAfterUpload = false;
        public static bool bUploadFile = false;
        public static bool bEmailFile = false;
        public static bool bCreateDiary = false;
        public static string sSmtpServer = string.Empty;
        public static string sFromEmailAddress = string.Empty;

        public static Hashtable htErrors = new Hashtable();

        public static string SecurityDSN
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
            }//get
        }//property: SecurityDS
    }
}
