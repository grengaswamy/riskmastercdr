﻿using System;
using System.Xml;
using System.Configuration;

namespace Riskmaster.Db
{
    class DbXMLConnection
    {
        public static string GetConnection(string sDbName)
        {
            XmlTextReader reader = new XmlTextReader("connectionStrings.config");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            XmlElement root = doc.DocumentElement;
            XmlNodeList xnList = doc.SelectNodes("/connectionStrings");
            XmlNode xmlEncryptedNode = xnList[0];
            string sChildElement = xmlEncryptedNode.FirstChild.Name;
            if (sChildElement.ToLower() == "encrypteddata")
            {
                XmlNode xmlEncryptedData = xmlEncryptedNode.SelectSingleNode("EncryptedData");
                DpapiProtectedConfigurationProvider objDpapi = new DpapiProtectedConfigurationProvider();
                XmlNode xmlDecryptedData = objDpapi.Decrypt(xmlEncryptedData);
                foreach (XmlNode chldNode in xmlDecryptedData.ChildNodes)
                {
                    if (!(chldNode.Attributes == null))
                    {
                        if (chldNode.Attributes["name"].Value == sDbName)
                        {
                            return chldNode.Attributes["connectionString"].Value;
                        }
                    }

                }
            }
            else if (sChildElement.ToLower() == "add")
            {
                reader = new XmlTextReader("connectionStrings.config");
                while (reader.Read())
                {
                    if (reader.GetAttribute("name") == sDbName)
                        return reader.GetAttribute("connectionString");
                }
            }
            return "";
        }
    }
}
