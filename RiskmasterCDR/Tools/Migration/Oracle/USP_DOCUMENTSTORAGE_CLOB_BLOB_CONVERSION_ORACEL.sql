create or replace
PROCEDURE USP_CLOB_BLOB_CONVERSION
AS 
BEGIN
	DECLARE 
		v_clob Clob;
		v_blob Blob;
		v_in Pls_Integer := 1;
		v_out Pls_Integer := 1;
		v_lang Pls_Integer := 0;
		v_warning Pls_Integer := 0;
                sSQL VARCHAR2(1000);
		v_id number(10);

	BEGIN
		FOR num IN (SELECT STORAGE_ID,DOC_BLOB FROM DOCUMENT_STORAGE)
		LOOP
                  v_id:=num.STORAGE_ID;
                  IF num.DOC_BLOB is null then 
                      v_blob:=null;
                  ELSE 
                    v_clob:=num.DOC_BLOB;
                    v_in:=1;
                    v_out:=1;
                    dbms_lob.createtemporary(v_blob,TRUE);
                    dbms_lob.convertToBlob(v_blob,v_clob,DBMS_lob.getlength(v_clob),v_in,v_out,DBMS_LOB.default_csid,v_lang,v_warning);
                  END IF;
                  UPDATE DOCUMENT_STORAGE SET DOC_BLOB_TEMP=v_blob WHERE STORAGE_ID=v_id;
                END LOOP;
		COMMIT;
                
                sSQL:= 'ALTER TABLE DOCUMENT_STORAGE DROP COLUMN DOC_BLOB';
                EXECUTE IMMEDIATE sSQL;  
                
                sSQL:='ALTER TABLE DOCUMENT_STORAGE RENAME COLUMN DOC_BLOB_TEMP to DOC_BLOB';
                EXECUTE IMMEDIATE sSQL;  
                COMMIT;
                
	END;
	
END;
