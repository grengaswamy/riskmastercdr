/* This script is for updating the WPA_DIARY_ENTRY table with the parent record id to the ATT_PARENT_CODE field.
This will work for the existing diaries which are not in void or deleted status */             
	
	MERGE INTO wpa_diary_entry
USING
(
  select attach_table,attach_recordid,att_parent_code,entry_id from wpa_diary_entry where (ATT_PARENT_CODE = 0 OR ATT_PARENT_CODE is NULL) AND (diary_void <> 0 OR diary_deleted <> 0 OR is_attached <> 0 OR attach_recordid <> 0) AND attach_table IS NOT NULL AND (att_parent_code = 0 OR att_parent_code IS NULL)
) source
   ON (wpa_diary_entry.entry_id = source.entry_id)
WHEN MATCHED THEN
 UPDATE SET att_parent_code = 	
            CASE WHEN source.attach_table = 'PERSON_INVOLVED' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'EVENTDATEDTEXT' THEN (select event_id from EVENT_X_DATED_TEXT where ev_dt_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'OSHA' THEN source.attach_recordid
								 WHEN source.attach_table = 'FALLINFO' THEN source.attach_recordid
								 WHEN source.attach_table = 'MEDWATCH' THEN source.attach_recordid
								 WHEN source.attach_table = 'MEDWATCHTEST' THEN source.attach_recordid
								 WHEN source.attach_table = 'PIPHYSICIAN' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIMEDSTAFF' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIEMPLOYEE' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIPATIENT' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIDRIVER' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIWITNESS' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIOTHER' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PIDEPENDENT' THEN (select event_id from PERSON_INVOLVED where pi_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'CONCOMITANT' THEN (select event_id from EV_X_CONCOM_PROD where EV_CONCOM_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table = 'SUBROGATION' THEN (select claim_id from claim_x_subro where subrogation_row_id = source.attach_recordid )
								 WHEN source.attach_table = 'SCHEDULED_ACTIVITY' THEN (select claim_id from claim_x_subro where subrogation_row_id = source.attach_recordid )
								 WHEN source.attach_table = 'CLAIMANT' THEN (select claim_id from claim_x_subro where subrogation_row_id =  source.attach_recordid )
								 WHEN source.attach_table = 'LITIGATION' THEN (select claim_id from CLAIM_X_LITIGATION where litigation_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'CLAIM_X_LITIGATION' THEN (select claim_id from CLAIM_X_LITIGATION where litigation_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'FUND_AUTO_BATCH' THEN (select claim_id from funds where trans_id = source.attach_recordid)
								 WHEN source.attach_table =	'AUTO_CLAIM_CHECKS' THEN (select claim_id from FUNDS_AUTO where AUTO_batch_ID = source.attach_recordid)
								 WHEN source.attach_table =	'CMXTREATMENTPLN' THEN (select claim_id from CM_X_TREATMENT_PLN tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmtp_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'CM_X_TREATMENT_PLN' THEN (select claim_id from CM_X_TREATMENT_PLN tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where cmtp_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'ADJUSTER' THEN (select claim_id from CLAIM_ADJUSTER where adj_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'CMXMEDMGTSAVINGS'THEN (select claim_id from CM_X_MEDMGTSAVINGS tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where CMMS_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table = 'PIRESTRICTION'THEN (select claim_id from CLAIM where claim_id  = (select claim_id from PI_X_RESTRICT pr INNER JOIN PERSON_INVOLVED pin ON pr.pi_row_id = pin.pi_row_id INNER JOIN CASE_MANAGEMENT cm ON cm.PI_ROW_ID = pin.pi_row_id where pi_restrict_row_id = source.attach_recordid))
								 WHEN source.attach_table = 'PIWORKLOSS'THEN (select claim_id from CM_X_MEDMGTSAVINGS tp INNER JOIN CASE_MANAGEMENT cm on tp.CASEMGT_ROW_ID = cm.CASEMGT_ROW_ID  where CMMS_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table =	'LEAVE' THEN (select claim_id from CLAIM_LEAVE where leave_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'UNIT' THEN (select claim_id from UNIT_X_CLAIM where unit_row_id = source.attach_recordid)
								 WHEN source.attach_table = 'PROPERTYLOSS' THEN (select claim_id from CLAIM_X_PROPERTYLOSS  where property_id = source.attach_recordid)
								 WHEN source.attach_table =	'CMXACCOMMODATION' THEN (select claim_id from CM_X_ACCOMMODATION ca INNER JOIN CASE_MANAGEMENT cm on ca.cm_row_id = casemgt_row_id where cm_accomm_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'CMXVOCREHAB' THEN (select claim_id from CM_X_VOCREHAB cv INNER JOIN CASE_MANAGEMENT cm on cv.casemgt_row_id = cm.casemgt_row_id where cmvr_row_id = source.attach_recordid)
								 WHEN source.attach_table =	'EXPERT' THEN (select ex.litigation_row_id from EXPERT ex INNER JOIN claim_x_Litigation li on ex.litigation_row_id = li.litigation_row_id where EXPERT_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table =	'SALVAGE' THEN (select claim_id from SALVAGE s INNER JOIN CLAIM_X_PROPERTYLOSS cp ON s.parent_id = cp.property_id where salvage_row_id =  source.attach_recordid)
								 WHEN source.attach_table =	'DEFENDANT' THEN (select claim_id from DEFENDANT where defendant_row_id =  source.attach_recordid)
								 WHEN source.attach_table =	'CMXCMGRHIST' THEN (select claim_id from CASE_MANAGEMENT where casemgt_row_id =  source.attach_recordid)
								 WHEN source.attach_table =	'POLICYCOVERAGE' THEN (select policy_id from POLICY_X_CVG_TYPE where POLCVG_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table =	'POLICYBILLING' THEN (SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_ID = source.attach_recordid)
								 WHEN source.attach_table = 'FUNDS' THEN (select claim_id from funds where trans_id = source.attach_recordid)
								 WHEN source.attach_table = 'RESERVE_HISTORY' THEN (select claim_id from RESERVE_HISTORY where RSV_ROW_ID = source.attach_recordid)
								 WHEN source.attach_table = 'RESERVE_CURRENT' THEN (select claim_id from RESERVE_CURRENT where rc_row_id = source.attach_recordid)
	END 
 -- select * from wpa_diary_entry