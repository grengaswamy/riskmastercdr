CREATE TABLE NET_VIEWS
(
 DATA_SOURCE_ID                                     NUMBER(10) NOT NULL,
 VIEW_ID                                            NUMBER(10) NOT NULL,
 VIEW_NAME                                          VARCHAR2(50),
 VIEW_DESC                                          VARCHAR2(250),
 HOME_PAGE                                          VARCHAR2(250),
 PAGE_MENU                                          VARCHAR2(50) 
)
TABLESPACE RISK_TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE UNIQUE INDEX PK_DATA_VIEW_ID ON NET_VIEWS(DATA_SOURCE_ID,VIEW_ID);

CREATE TABLE NET_VIEWS_MEMBERS
(
 DATA_SOURCE_ID                                     NUMBER(10) NOT NULL,
 VIEW_ID                                            NUMBER(10) NOT NULL,
 MEMBER_ID                                          NUMBER(10),
 ISGROUP                                            NUMBER(5) 
)
TABLESPACE RISK_TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE UNIQUE INDEX PK_VIEW_MEMBER_ID ON NET_VIEWS_MEMBERS(DATA_SOURCE_ID,VIEW_ID,MEMBER_ID);

CREATE TABLE NET_VIEW_FORMS
(
 DATA_SOURCE_ID                                     NUMBER(10) NOT NULL,
 VIEW_ID                                            NUMBER(10) NOT NULL,
 FORM_NAME                                          VARCHAR2(50),
 TOPLEVEL                                           NUMBER(5),
 CAPTION                                            VARCHAR2(100),
 VIEW_XML                                           CLOB,
 LAST_UPDATED                                       VARCHAR2(50),
 PAGE_NAME                                          VARCHAR2(50),
 PAGE_CONTENT                                       CLOB
)
TABLESPACE RISK_TBS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       2147483645
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

CREATE UNIQUE INDEX PK_DATA_VIEW_FORM_ID ON NET_VIEW_FORMS(DATA_SOURCE_ID,VIEW_ID,FORM_NAME);