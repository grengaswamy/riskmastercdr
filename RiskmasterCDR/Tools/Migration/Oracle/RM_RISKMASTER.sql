
CREATE TABLE ACCOUNT
(
  ACCOUNT_NUMBER     VARCHAR2(20 BYTE)              NULL,
  OWNER_EID          NUMBER(10)                     NULL,
  ACCOUNT_ID         NUMBER(10)                     NULL,
  ACCOUNT_NAME       VARCHAR2(50 BYTE)              NULL,
  ACCT_TYPE_CODE     NUMBER(10)                     NULL,
  BANK_EID           NUMBER(10)                     NULL,
  NEXT_CHECK_NUMBER  NUMBER(10)                     NULL,
  NEXT_BATCH_NUMBER  NUMBER(10)                     NULL,
  DEFAULT_ACCOUNT    NUMBER(10)                     NULL,
  OWNER_LOB          NUMBER(10)                     NULL,
  EFF_TRIGGER        VARCHAR2(50 BYTE)              NULL,
  TRIGGER_FROM_DATE  VARCHAR2(8 BYTE)               NULL,
  TRIGGER_TO_DATE    VARCHAR2(8 BYTE)               NULL,
  MAST_BANK_ACCT_ID  NUMBER(10)                     NULL,
  SERVICE_CODE       NUMBER(10)                     NULL,
  PRIORITY           NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ACCOUNT_SUPP
(
  ACCOUNT_ID  NUMBER(10)                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ACCT_REC
(
  AR_ROW_ID          NUMBER(10)                     NULL,
  CUSTOMER_EID       NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  TRANS_ID           NUMBER(10)                     NULL,
  INV_TRANS_ID       NUMBER(10)                     NULL,
  ALLOCATED          NUMBER(10)                     NULL,
  INV_NUMBER         VARCHAR2(30 BYTE)              NULL,
  INV_TOTAL          NUMBER                         NULL,
  INV_DATE           VARCHAR2(8 BYTE)               NULL,
  WORK_PERF_BY_EID   NUMBER(10)                     NULL,
  POSTED             NUMBER(10)                     NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  POSTED_ID          NUMBER(10)                     NULL,
  VOID_FLAG          NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ACCT_REC_DETAIL
(
  AR_ROW_ID          NUMBER(10)                     NULL,
  AR_DETAIL_ROW_ID   NUMBER(10)                     NULL,
  RATE_TABLE_ID      NUMBER(10)                     NULL,
  SPLIT_ROW_ID       NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  TRANS_TYPE_CODE    NUMBER(10)                     NULL,
  LINE_ITEM_AMOUNT   NUMBER                         NULL,
  FROM_DATE          VARCHAR2(8 BYTE)               NULL,
  TO_DATE            VARCHAR2(8 BYTE)               NULL,
  UNIT               VARCHAR2(10 BYTE)              NULL,
  QUANTITY           NUMBER                         NULL,
  RATE               NUMBER                         NULL,
  TOTAL              NUMBER                         NULL,
  OVERRIDE           NUMBER(10)                     NULL,
  BILLABLE           NUMBER(10)                     NULL,
  NON_BILL_REASON    NUMBER(10)                     NULL,
  POSTABLE           NUMBER(10)                     NULL,
  REASON             VARCHAR2(150 BYTE)             NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  LOCAL_COMMENT      CLOB                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ACC_SERVICE_CODE
(
  ACCOUNT_ID    NUMBER(10)                          NULL,
  SERVICE_CODE  NUMBER(10)                          NULL,
  SUB_ACC_ID    NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ACTIVITY_LOG
(
  ACTLOG_ROW_ID     NUMBER(10)                      NULL,
  PATIENT_EID       NUMBER(10)                      NULL,
  EVENT_ID          NUMBER(10)                      NULL,
  CLAIM_ID          NUMBER(10)                      NULL,
  OPERATION         VARCHAR2(100 BYTE)              NULL,
  ACCESS_TYPE_CODE  NUMBER(10)                      NULL,
  USER_LOGIN        VARCHAR2(14 BYTE)               NULL,
  DTTM_LOG          VARCHAR2(30 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ADJUST_DATED_TEXT
(
  TEXT_TYPE_CODE     NUMBER(10)                     NULL,
  ADJ_DTTEXT_ROW_ID  NUMBER(10)                     NULL,
  ADJ_ROW_ID         NUMBER(10)                     NULL,
  DATE_ENTERED       VARCHAR2(8 BYTE)               NULL,
  ENTERED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  TIME_ENTERED       VARCHAR2(6 BYTE)               NULL,
  DATED_TEXT         CLOB                           NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE AD_FORMS
(
  FORM_CATEGORY      NUMBER(10)                     NULL,
  FORM_ID            NUMBER(10)                     NULL,
  FORM_TITLE         VARCHAR2(128 BYTE)             NULL,
  FORM_NAME          VARCHAR2(64 BYTE)              NULL,
  FILE_NAME          VARCHAR2(64 BYTE)              NULL,
  ACTIVE_FLAG        NUMBER(10)                     NULL,
  PRIMARY_FORM_FLAG  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE AD_FORMS_CAT_LKUP
(
  FORM_CAT       NUMBER(10)                         NULL,
  FORM_CAT_DESC  VARCHAR2(64 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE AD_FORMS_HIST
(
  FORM_ID       NUMBER(10)                          NULL,
  DATE_PRINTED  VARCHAR2(8 BYTE)                    NULL,
  TIME_PRINTED  VARCHAR2(6 BYTE)                    NULL,
  USER_ID       VARCHAR2(20 BYTE)                   NULL,
  HOW_PRINTED   VARCHAR2(5 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BANK_ACC_RECON
(
  RECORD_ID   NUMBER(10)                            NULL,
  ACCOUNT_ID  NUMBER(10)                            NULL,
  BEGIN_DATE  VARCHAR2(8 BYTE)                      NULL,
  END_DATE    VARCHAR2(8 BYTE)                      NULL,
  BALANCE     NUMBER                                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BANK_ACC_SUB
(
  SUB_ROW_ID         NUMBER(10)                     NULL,
  ACCOUNT_ID         NUMBER(10)                     NULL,
  OWNER_EID          NUMBER(10)                     NULL,
  SUB_ACC_NAME       VARCHAR2(50 BYTE)              NULL,
  MIN_BALANCE        NUMBER                         NULL,
  SUB_ACC_NUMBER     VARCHAR2(20 BYTE)              NULL,
  USE_MM_FLAG        NUMBER(5)                      NULL,
  TARGET_BALANCE     NUMBER                         NULL,
  OWNER_LOB          NUMBER(10)                     NULL,
  EFF_TRIGGER        VARCHAR2(50 BYTE)              NULL,
  TRIGGER_FROM_DATE  VARCHAR2(8 BYTE)               NULL,
  TRIGGER_TO_DATE    VARCHAR2(8 BYTE)               NULL,
  SERVICE_CODE       NUMBER(10)                     NULL,
  PRIORITY           NUMBER(10)                     NULL,
  STOCK_ID           NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_ACCOUNT
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  BILL_ACCOUNT_ROWID  NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_NUMBER       VARCHAR2(20 BYTE)             NULL,
  PAY_PLAN_ROWID      NUMBER(10)                    NULL,
  BILLING_RULE_ROWID  NUMBER(10)                    NULL,
  AMOUNT_DUE          NUMBER                        NULL,
  TERM_NUMBER         NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_ADJSTMNT
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  ADJUSTMENT_ROWID    NUMBER(10)                    NULL,
  ADJUSTMENT_TYPE     NUMBER(10)                    NULL,
  BILLING_ITEM_ROWID  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_BATCH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  BATCH_ROWID         NUMBER(10)                    NULL,
  BATCH_TYPE          NUMBER(10)                    NULL,
  DATE_OPENED         VARCHAR2(8 BYTE)              NULL,
  DATE_CLOSED         VARCHAR2(8 BYTE)              NULL,
  BATCH_AMOUNT        NUMBER                        NULL,
  BATCH_COUNT         NUMBER(10)                    NULL,
  STATUS              NUMBER(10)                    NULL,
  BILL_ACCOUNT_ROWID  NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_BILL_ITEM
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  BILLING_ITEM_ROWID  NUMBER(10)                    NULL,
  AMOUNT              NUMBER                        NULL,
  STATUS              NUMBER(10)                    NULL,
  BILLING_ITEM_TYPE   NUMBER(10)                    NULL,
  ENTRY_BATCH_NUM     NUMBER(10)                    NULL,
  ENTRY_BATCH_TYPE    NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_NUMBER       VARCHAR2(20 BYTE)             NULL,
  FINAL_BATCH_NUM     NUMBER(10)                    NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  FINAL_BATCH_TYPE    NUMBER(10)                    NULL,
  DATE_RECONCILED     VARCHAR2(8 BYTE)              NULL,
  INVOICE_ID          NUMBER(10)                    NULL,
  NOTICE_ID           NUMBER(10)                    NULL,
  "COMMENT"           CLOB                          NULL,
  DATE_ENTERED        VARCHAR2(8 BYTE)              NULL,
  DUE_DATE            VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_DSBRSMNT
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DISBURSEMENT_ROWID  NUMBER(10)                    NULL,
  DISBURSEMENT_TYPE   NUMBER(10)                    NULL,
  BILLING_ITEM_ROWID  NUMBER(10)                    NULL,
  FUND_TRAN_ID        NUMBER(10)                    NULL,
  CHECK_NUMBER        NUMBER(10)                    NULL,
  DATE_PRINTED        VARCHAR2(8 BYTE)              NULL,
  PRINTED_FLAG        NUMBER(5)                     NULL,
  TRANSACTION_TYPE    NUMBER(10)                    NULL,
  BANK_ACCT_CODE      NUMBER(10)                    NULL,
  SUB_ACCT_CODE       NUMBER(10)                    NULL,
  PAYEE               NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_INSTLMNT
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  INSTALLMENT_ROWID   NUMBER(10)                    NULL,
  INSTALLMENT_NUM     NUMBER(10)                    NULL,
  AMOUNT              NUMBER                        NULL,
  INSTALLMENT_DATE    VARCHAR2(8 BYTE)              NULL,
  BILL_ACCOUNT_ROWID  NUMBER(10)                    NULL,
  INSTALL_DUE_DATE    VARCHAR2(8 BYTE)              NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_TRAN_ID      NUMBER(10)                    NULL,
  GENERATED_FLAG      NUMBER(5)                     NULL,
  TERM_NUMBER         NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_INVOICE
(
  DTTM_RCD_LAST_UPD    VARCHAR2(14 BYTE)            NULL,
  UPDATED_BY_USER      VARCHAR2(8 BYTE)             NULL,
  DTTM_RCD_ADDED       VARCHAR2(14 BYTE)            NULL,
  ADDED_BY_USER        VARCHAR2(8 BYTE)             NULL,
  INVOICE_ROWID        NUMBER(10)                   NULL,
  AMOUNT               NUMBER                       NULL,
  DUE_DATE             VARCHAR2(8 BYTE)             NULL,
  BILL_DATE            VARCHAR2(8 BYTE)             NULL,
  HIERARCHY_LEVEL      NUMBER(10)                   NULL,
  POLICY_ID            NUMBER(10)                   NULL,
  POLICY_NUMBER        VARCHAR2(20 BYTE)            NULL,
  BILL_ACCOUNT_ROWID   NUMBER(10)                   NULL,
  PREVIOUS_BALANCE     NUMBER                       NULL,
  PAYMENTS             NUMBER                       NULL,
  OUTSTANDING_BALANCE  NUMBER                       NULL,
  CURRENT_CHARGES      NUMBER                       NULL,
  POLICY_TRANS_ID      NUMBER(10)                   NULL,
  TERM_NUMBER          NUMBER(10)                   NULL,
  PREV_INVOICE_ROWID   NUMBER(10)                   NULL,
  PREV_NOTICE_ROWID    NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_INVOICE_DET
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  INVOICE_DET_ROWID  NUMBER(10)                     NULL,
  INVOICE_ROWID      NUMBER(10)                     NULL,
  BILL_ITEM_ID       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_NOTICE
(
  DTTM_RCD_LAST_UPD    VARCHAR2(14 BYTE)            NULL,
  UPDATED_BY_USER      VARCHAR2(8 BYTE)             NULL,
  DTTM_RCD_ADDED       VARCHAR2(14 BYTE)            NULL,
  ADDED_BY_USER        VARCHAR2(8 BYTE)             NULL,
  NOTICE_ROWID         NUMBER(10)                   NULL,
  AMOUNT               NUMBER                       NULL,
  DUE_DATE             VARCHAR2(8 BYTE)             NULL,
  BILL_DATE            VARCHAR2(8 BYTE)             NULL,
  HIERARCHY_LEVEL      NUMBER(10)                   NULL,
  POLICY_ID            NUMBER(10)                   NULL,
  POLICY_NUMBER        VARCHAR2(20 BYTE)            NULL,
  BILL_ACCOUNT_ROWID   NUMBER(10)                   NULL,
  PREVIOUS_BALANCE     NUMBER                       NULL,
  PAYMENTS             NUMBER                       NULL,
  OUTSTANDING_BALANCE  NUMBER                       NULL,
  CURRENT_CHARGES      NUMBER                       NULL,
  POLICY_TRANS_ID      NUMBER(10)                   NULL,
  TERM_NUMBER          NUMBER(10)                   NULL,
  PREV_INVOICE_ROWID   NUMBER(10)                   NULL,
  PREV_NOTICE_ROWID    NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_NOTICE_DET
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  NOTICE_DET_ROWID   NUMBER(10)                     NULL,
  NOTICE_ROWID       NUMBER(10)                     NULL,
  BILL_ITEM_ID       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_PREM_ITEM
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  PREMIUM_ITEM_ROWID  NUMBER(10)                    NULL,
  PREMIUM_ITEM_TYPE   NUMBER(10)                    NULL,
  BILLING_ITEM_ROWID  NUMBER(10)                    NULL,
  POLICY_TRAN_ID      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BILL_X_RECEIPT
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  RECEIPT_ROWID       NUMBER(10)                    NULL,
  RECEIPT_TYPE        NUMBER(10)                    NULL,
  BILLING_ITEM_ROWID  NUMBER(10)                    NULL,
  CHECK_NUMBER        NUMBER(10)                    NULL,
  CHECK_DATE          VARCHAR2(8 BYTE)              NULL,
  DATE_RECEIVED       VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BOND_ABS
(
  BOND_ABS_ID       NUMBER(10)                      NULL,
  BOND_NUM_TEXT     VARCHAR2(20 BYTE)               NULL,
  BOND_TYPE_TXCD    CLOB                            NULL,
  BOND_LINE_CODE    NUMBER(10)                      NULL,
  EFF_DATE_DATE     VARCHAR2(8 BYTE)                NULL,
  EXPIR_DATE_DATE   VARCHAR2(8 BYTE)                NULL,
  BOND_AMT_AMT      NUMBER                          NULL,
  PREMIUM_AMT       NUMBER                          NULL,
  BILL_NAME_TEXT    VARCHAR2(50 BYTE)               NULL,
  BILL_ADDR_TEXT    VARCHAR2(50 BYTE)               NULL,
  BILL_CITY_TEXT    VARCHAR2(25 BYTE)               NULL,
  BILL_ZIP_TEXT     VARCHAR2(15 BYTE)               NULL,
  BILL_ACCT_TEXT    VARCHAR2(50 BYTE)               NULL,
  PRINCIPAL_TXCD    CLOB                            NULL,
  STATE_TEXT        VARCHAR2(3 BYTE)                NULL,
  BILL_STATE_TEXT   VARCHAR2(25 BYTE)               NULL,
  OBLIGEE_TXCD      CLOB                            NULL,
  BOND_CLASS_CODE   NUMBER(10)                      NULL,
  OLD_BOND_NO_TEXT  VARCHAR2(25 BYTE)               NULL,
  REQUESTER_TEXT    VARCHAR2(50 BYTE)               NULL,
  REQ_ADDR_TEXT     VARCHAR2(50 BYTE)               NULL,
  REQ_CITY_TEXT     VARCHAR2(25 BYTE)               NULL,
  REQ_STATE_TEXT    VARCHAR2(25 BYTE)               NULL,
  REQ_ZIP_TEXT      VARCHAR2(15 BYTE)               NULL,
  REQ_PHONE_TEXT    VARCHAR2(15 BYTE)               NULL,
  ORDERED_BY_TEXT   VARCHAR2(50 BYTE)               NULL,
  ORDER_ADDR_TEXT   VARCHAR2(50 BYTE)               NULL,
  ORDER_CITY_TEXT   VARCHAR2(25 BYTE)               NULL,
  ORDER_STATE_TEXT  VARCHAR2(15 BYTE)               NULL,
  ORDER_ZIP_TEXT    VARCHAR2(15 BYTE)               NULL,
  ORDER_PHONE_TEXT  VARCHAR2(25 BYTE)               NULL,
  AUTH_BY_TEXT      VARCHAR2(50 BYTE)               NULL,
  AUTH_ADDR_TEXT    VARCHAR2(50 BYTE)               NULL,
  AUTH_CITY_TEXT    VARCHAR2(25 BYTE)               NULL,
  AUTH_STATE_TEXT   VARCHAR2(15 BYTE)               NULL,
  AUTH_ZIP_TEXT     VARCHAR2(15 BYTE)               NULL,
  AUTH_PHONE_TEXT   VARCHAR2(15 BYTE)               NULL,
  DATE_EXEC_DATE    VARCHAR2(8 BYTE)                NULL,
  PAID_DATE_DATE    VARCHAR2(8 BYTE)                NULL,
  BOND_STATUS_CODE  NUMBER(10)                      NULL,
  DATE_CANC_DATE    VARCHAR2(8 BYTE)                NULL,
  BOND_DETAIL_TXCD  CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BRS_FEESCHD
(
  CALC_TYPE     NUMBER(10)                          NULL,
  DB_PATH       VARCHAR2(255 BYTE)                  NULL,
  FEESCHD_NAME  VARCHAR2(30 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BRS_MOD_VALUES
(
  MODIFIER_ROW_ID  NUMBER(10)                       NULL,
  FEETABLE_ID      NUMBER(10)                       NULL,
  MODIFIER_CODE    NUMBER(10)                       NULL,
  START_CPT        VARCHAR2(10 BYTE)                NULL,
  END_CPT          VARCHAR2(10 BYTE)                NULL,
  MODIFIER_VALUE   NUMBER                           NULL,
  MAX_RLV          NUMBER                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE BRS_SYS_PARAMS
(
  ROW_ID              NUMBER(10)                    NULL,
  KEEP_ON_NEXT        VARCHAR2(255 BYTE)            NULL,
  GEN_EOB_CODE_FLAG   NUMBER(10)                    NULL,
  DISC_ON_SCHD_FLAG   NUMBER(10)                    NULL,
  DISC_ON_BILL_FLAG   NUMBER(10)                    NULL,
  DISC_2ND_SCHD_FLAG  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CASE_MANAGEMENT
(
  CASEMGT_ROW_ID     NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  REF_TYPE_CODE      NUMBER(10)                     NULL,
  REF_DATE           VARCHAR2(8 BYTE)               NULL,
  CASE_STATUS_CODE   NUMBER(10)                     NULL,
  DATE_CLOSED        VARCHAR2(8 BYTE)               NULL,
  REF_TO_CODE        NUMBER(10)                     NULL,
  REF_REASON_CODE    NUMBER(10)                     NULL,
  RTW_STATUS_CODE    NUMBER(10)                     NULL,
  EST_REL_RST_DATE   VARCHAR2(8 BYTE)               NULL,
  EST_RET_WORK_DATE  VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CASE_MGR_NOTES
(
  NOTES_TYPE_CODE    NUMBER(10)                     NULL,
  CMGR_NOTES_ROW_ID  NUMBER(10)                     NULL,
  CASEMGR_ROW_ID     NUMBER(10)                     NULL,
  DATE_ENTERED       VARCHAR2(8 BYTE)               NULL,
  ENTERED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  TIME_ENTERED       VARCHAR2(6 BYTE)               NULL,
  CMGR_NOTES         CLOB                           NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CERTIFICATE
(
  CERTIFICATE_ID    NUMBER(10)                      NULL,
  JOB_NO_TEXT       VARCHAR2(15 BYTE)               NULL,
  JOB_DESC_TXCD     CLOB                            NULL,
  AGENCY_NAME_TEXT  VARCHAR2(50 BYTE)               NULL,
  AGENCY_PHON_TEXT  VARCHAR2(20 BYTE)               NULL,
  INSURED_NAM_TEXT  VARCHAR2(50 BYTE)               NULL,
  INSURED_ADD_TEXT  VARCHAR2(50 BYTE)               NULL,
  INSURED_CIT_TEXT  VARCHAR2(25 BYTE)               NULL,
  INSURED_ST_TEXT   VARCHAR2(15 BYTE)               NULL,
  INSURED_ZIP_TEXT  VARCHAR2(15 BYTE)               NULL,
  INSURED_PH_TEXT   VARCHAR2(20 BYTE)               NULL,
  GEN_LIAB_TXCD     CLOB                            NULL,
  INS_NAME_TEXT     VARCHAR2(50 BYTE)               NULL,
  POL_NUMBER_NUM    NUMBER                          NULL,
  EXPIR_DATE_DATE   VARCHAR2(8 BYTE)                NULL,
  BI_EA_AGG_TEXT    VARCHAR2(25 BYTE)               NULL,
  PD_EA_AGG_TEXT    VARCHAR2(25 BYTE)               NULL,
  BI_PD_COMB_NUM    NUMBER                          NULL,
  PI_AGG_NUM        NUMBER                          NULL,
  AUTO_LIAB_TEXT    VARCHAR2(25 BYTE)               NULL,
  BI_EA_PERS_NUM    NUMBER                          NULL,
  BI_EA_ACC_NUM     NUMBER                          NULL,
  PD_EA_OCC_NUM     NUMBER                          NULL,
  BI_PD_COMB_TEXT   VARCHAR2(15 BYTE)               NULL,
  EXCESS_LIAB_TXCD  CLOB                            NULL,
  BI_PD_AGG_TEXT    VARCHAR2(15 BYTE)               NULL,
  WORK_COMP_NUM     NUMBER                          NULL,
  WORK_COMP_TEXT    VARCHAR2(25 BYTE)               NULL,
  WC_EA_ACC_NUM     NUMBER                          NULL,
  OPER_DESC_TXCD    CLOB                            NULL,
  ISSUE_DT_DATE     VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CHECK_BATCHES
(
  BATCH_NUMBER    NUMBER(10)                        NULL,
  ACCOUNT_ID      NUMBER(10)                        NULL,
  ORDER_BY_FIELD  VARCHAR2(50 BYTE)                 NULL,
  DATE_RUN        VARCHAR2(8 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CHECK_OPTIONS
(
  CHECK_TO_FILE       NUMBER(5)                     NULL,
  ALLOW_POST_DATE     NUMBER(5)                     NULL,
  ROLLUP_CHECKS       NUMBER(5)                     NULL,
  DUPLICATE_FLAG      NUMBER(5)                     NULL,
  DUP_PAY_CRITERIA    NUMBER(10)                    NULL,
  DUP_NUM_DAYS        NUMBER(10)                    NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(16 BYTE)             NULL,
  MAX_AUTO_CHECKS     NUMBER(10)                    NULL,
  SH_PAYMENT          NUMBER(5)                     NULL,
  SH_PAY_DET          NUMBER(5)                     NULL,
  SH_NEW_PAYEE        NUMBER(5)                     NULL,
  SH_TRANS_TYPES      NUMBER(5)                     NULL,
  SH_DIARY            NUMBER(10)                    NULL,
  DO_NOT_SAVE_DUPS    NUMBER(5)                     NULL,
  USER_SPECIFIC_FLAG  NUMBER(5)                     NULL,
  DAYS_FOR_APPROVAL   NUMBER(10)                    NULL,
  QUEUED_PAYMENTS     NUMBER(10)                    NULL,
  CHECK_PAPER_BIN     NUMBER(10)                    NULL,
  EOB_PAPER_BIN       NUMBER(10)                    NULL,
  PRT_EOB_WITH_CHKS   NUMBER(10)                    NULL,
  PRINT_STUB_DETAIL   NUMBER(10)                    NULL,
  PRINTER_NAME        VARCHAR2(100 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CHECK_STOCK
(
  DOL_SIGN_FLAG       NUMBER(5)                     NULL,
  MEMO_FONT_NAME      VARCHAR2(40 BYTE)             NULL,
  MEMO_FONT_SIZE      NUMBER                        NULL,
  MEMO_ROW_LENGTH     NUMBER(10)                    NULL,
  MEMO_ROWS           NUMBER(10)                    NULL,
  PRINT_ADDR_FLAG     NUMBER(5)                     NULL,
  PRINT_MEMO_FLAG     NUMBER(5)                     NULL,
  STOCK_ID            NUMBER(10)                    NULL,
  STOCK_NAME          VARCHAR2(30 BYTE)             NULL,
  ACCOUNT_ID          NUMBER(10)                    NULL,
  CHK_DATE_X          NUMBER(10)                    NULL,
  CHK_DATE_Y          NUMBER(10)                    NULL,
  CHK_NUMBER_X        NUMBER(10)                    NULL,
  CHK_NUMBER_Y        NUMBER(10)                    NULL,
  CHK_AMOUNT_X        NUMBER(10)                    NULL,
  CHK_AMOUNT_Y        NUMBER(10)                    NULL,
  CHK_AMOUNT_TEXT_X   NUMBER(10)                    NULL,
  CHK_AMOUNT_TEXT_Y   NUMBER(10)                    NULL,
  PAYEE_X             NUMBER(10)                    NULL,
  PAYEE_Y             NUMBER(10)                    NULL,
  CHK_MEMO_X          NUMBER(10)                    NULL,
  CHK_MEMO_Y          NUMBER(10)                    NULL,
  CHK_DETAIL_X        NUMBER(10)                    NULL,
  CHK_DETAIL_Y        NUMBER(10)                    NULL,
  CHK_PAYER_X         NUMBER(10)                    NULL,
  CHK_PAYER_Y         NUMBER(10)                    NULL,
  FONT_NAME           VARCHAR2(40 BYTE)             NULL,
  FONT_SIZE           NUMBER                        NULL,
  CHK_LAYOUT          NUMBER(5)                     NULL,
  CHK_OFFSET_X        NUMBER(10)                    NULL,
  CHK_MICR_X          NUMBER(10)                    NULL,
  CHK_OFFSET_Y        NUMBER(10)                    NULL,
  CHK_MICR_Y          NUMBER(10)                    NULL,
  CHK_RXLASER_FLAG    NUMBER(5)                     NULL,
  CHK_MICR_FLAG       NUMBER(5)                     NULL,
  CHK_MICR_SEQUENCE   VARCHAR2(80 BYTE)             NULL,
  CHK_RXLASER_FORM    NUMBER(5)                     NULL,
  PRE_NUM_STOCK       NUMBER(5)                     NULL,
  SPECIAL_FLAG        NUMBER(5)                     NULL,
  SPECIAL_AMOUNT      NUMBER                        NULL,
  RX_VOID_FORM        NUMBER(10)                    NULL,
  RX_SPECIAL_FORM     NUMBER(10)                    NULL,
  PRINT_INSURER       NUMBER(5)                     NULL,
  MICR_TYPE           NUMBER(10)                    NULL,
  MICR_FONT           VARCHAR2(100 BYTE)            NULL,
  MICR_FONT_SIZE      NUMBER                        NULL,
  CHK_SOFT_FORM       NUMBER(5)                     NULL,
  PAYER_LEVEL         NUMBER(10)                    NULL,
  SECD_OFFSET_Y       NUMBER(10)                    NULL,
  SECD_OFFSET_X       NUMBER(10)                    NULL,
  ADDRESS_FLAG        NUMBER(10)                    NULL,
  CLM_NUM_OFFSET_X    NUMBER(10)                    NULL,
  CLM_NUM_OFFSET_Y    NUMBER(10)                    NULL,
  PRNT_CLM_NUM_CHECK  NUMBER(5)                     NULL,
  PRNT_TAXID_ON_STUB  NUMBER(10)                    NULL,
  PRNT_ADJ_ON_STUB    NUMBER(10)                    NULL,
  PRNT_EVENT_ON_STUB  NUMBER(10)                    NULL,
  PRNT_CTL_ON_STUB    NUMBER(10)                    NULL,
  PRNT_CHKDT_ON_STUB  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CHECK_STOCK_FORMS
(
  STOCK_ID     NUMBER(10)                           NULL,
  IMAGE_IDX    NUMBER(10)                           NULL,
  FORM_TYPE    NUMBER(10)                           NULL,
  FORM_SOURCE  CLOB                                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM
(
  CLAIM_ID            NUMBER(10)                    NULL,
  EVENT_ID            NUMBER(10)                    NULL,
  EVENT_NUMBER        VARCHAR2(25 BYTE)             NULL,
  CLAIM_NUMBER        VARCHAR2(25 BYTE)             NULL,
  CLAIM_STATUS_CODE   NUMBER(10)                    NULL,
  LINE_OF_BUS_CODE    NUMBER(10)                    NULL,
  OPEN_FLAG           NUMBER(5)                     NULL,
  CLAIM_TYPE_CODE     NUMBER(10)                    NULL,
  COMMENTS            CLOB                          NULL,
  DTTM_CLOSED         VARCHAR2(14 BYTE)             NULL,
  DATE_OF_CLAIM       VARCHAR2(8 BYTE)              NULL,
  TIME_OF_CLAIM       VARCHAR2(6 BYTE)              NULL,
  FILE_NUMBER         VARCHAR2(32 BYTE)             NULL,
  METHOD_CLOSED_CODE  NUMBER(10)                    NULL,
  PAYMNT_FROZEN_FLAG  NUMBER(5)                     NULL,
  PRIMARY_POLICY_ID   NUMBER(10)                    NULL,
  SERVICE_CODE        NUMBER(10)                    NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ACCIDENT_DESC_CODE  NUMBER(10)                    NULL,
  ACCIDENT_TYPE_CODE  NUMBER(10)                    NULL,
  DATE_FDDOT_RPT      VARCHAR2(8 BYTE)              NULL,
  DATE_STDOT_RPT      VARCHAR2(8 BYTE)              NULL,
  IN_TRAFFIC_FLAG     NUMBER(5)                     NULL,
  POLICE_AGENCY_EID   NUMBER(10)                    NULL,
  PREVENTABLE_FLAG    NUMBER(5)                     NULL,
  REPORTABLE_FLAG     NUMBER(5)                     NULL,
  ST_DOT_RPT_ID       VARCHAR2(15 BYTE)             NULL,
  EST_COLLECTION      NUMBER                        NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  DATE_RPTD_TO_RM     VARCHAR2(8 BYTE)              NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DURATION            NUMBER(10)                    NULL,
  FILING_STATE_ID     NUMBER(10)                    NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL,
  MCO_EID             NUMBER(10)                    NULL,
  RATE_TABLE_ID       NUMBER(10)                    NULL,
  PLAN_ID             NUMBER(10)                    NULL,
  CLASS_ROW_ID        NUMBER(10)                    NULL,
  TAX_FLAGS           NUMBER(10)                    NULL,
  BEN_CALC_PAY_START  VARCHAR2(8 BYTE)              NULL,
  BEN_CALC_PAY_TO     VARCHAR2(8 BYTE)              NULL,
  DIS_TYPE_CODE       NUMBER(10)                    NULL,
  BENEFITS_START      VARCHAR2(8 BYTE)              NULL,
  BENEFITS_THROUGH    VARCHAR2(8 BYTE)              NULL,
  DISABIL_FROM_DATE   VARCHAR2(8 BYTE)              NULL,
  DISABIL_TO_DATE     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIMANT
(
  CLAIMANT_ROW_ID     NUMBER(10)                    NULL,
  CLAIM_ID            NUMBER(10)                    NULL,
  CLAIMANT_EID        NUMBER(10)                    NULL,
  CLAIMANT_TYPE_CODE  NUMBER(10)                    NULL,
  ATTORNEY_EID        NUMBER(10)                    NULL,
  PRIMARY_CLMNT_FLAG  NUMBER(5)                     NULL,
  INJURY_DESCRIPTION  CLOB                          NULL,
  DAMAGE_DESCRIPTION  CLOB                          NULL,
  SOL_DATE            VARCHAR2(8 BYTE)              NULL,
  COMMENTS            CLOB                          NULL,
  CLAIM_AMOUNT        NUMBER                        NULL,
  INSURER_EID         NUMBER(10)                    NULL,
  CLAIMANT_NUMBER     NUMBER(10)                    NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIMANT_SUPP
(
  CLAIMANT_ROW_ID  NUMBER(10)                       NULL,
  CLAIMANT_EID     NUMBER(10)                       NULL,
  CLAIM_ID         NUMBER(10)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_ACC_LIMITS
(
  GROUP_ID           NUMBER(10)                     NULL,
  USER_ID            NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  CLAIM_TYPE_CODE    NUMBER(10)                     NULL,
  CREATE_FLAG        NUMBER(5)                      NULL,
  ACCESS_FLAG        NUMBER(5)                      NULL,
  CLAIM_STATUS_CODE  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_ADJUSTER
(
  ADJ_ROW_ID        NUMBER(10)                      NULL,
  ADJUSTER_EID      NUMBER(10)                      NULL,
  CLAIM_ID          NUMBER(10)                      NULL,
  CURRENT_ADJ_FLAG  NUMBER(5)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_ADJ_SUPP
(
  ADJ_ROW_ID  NUMBER(10)                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_AWW
(
  CLAIM_ID            NUMBER(10)                    NULL,
  CONSTANT_WAGE       NUMBER                        NULL,
  LAST_WORK_WEEK      VARCHAR2(8 BYTE)              NULL,
  INCLUDE_ZEROS       NUMBER(10)                    NULL,
  BONUSES             NUMBER                        NULL,
  WEEK_00             NUMBER                        NULL,
  WEEK_01             NUMBER                        NULL,
  WEEK_02             NUMBER                        NULL,
  WEEK_03             NUMBER                        NULL,
  WEEK_04             NUMBER                        NULL,
  WEEK_05             NUMBER                        NULL,
  WEEK_06             NUMBER                        NULL,
  WEEK_07             NUMBER                        NULL,
  WEEK_08             NUMBER                        NULL,
  WEEK_09             NUMBER                        NULL,
  WEEK_10             NUMBER                        NULL,
  WEEK_11             NUMBER                        NULL,
  WEEK_12             NUMBER                        NULL,
  WEEK_13             NUMBER                        NULL,
  WEEK_14             NUMBER                        NULL,
  WEEK_15             NUMBER                        NULL,
  WEEK_16             NUMBER                        NULL,
  WEEK_17             NUMBER                        NULL,
  WEEK_18             NUMBER                        NULL,
  WEEK_19             NUMBER                        NULL,
  WEEK_20             NUMBER                        NULL,
  WEEK_21             NUMBER                        NULL,
  WEEK_22             NUMBER                        NULL,
  WEEK_23             NUMBER                        NULL,
  WEEK_24             NUMBER                        NULL,
  WEEK_25             NUMBER                        NULL,
  WEEK_26             NUMBER                        NULL,
  WEEK_27             NUMBER                        NULL,
  WEEK_28             NUMBER                        NULL,
  WEEK_29             NUMBER                        NULL,
  WEEK_30             NUMBER                        NULL,
  WEEK_31             NUMBER                        NULL,
  WEEK_32             NUMBER                        NULL,
  WEEK_33             NUMBER                        NULL,
  WEEK_34             NUMBER                        NULL,
  WEEK_35             NUMBER                        NULL,
  WEEK_36             NUMBER                        NULL,
  WEEK_37             NUMBER                        NULL,
  WEEK_38             NUMBER                        NULL,
  WEEK_39             NUMBER                        NULL,
  WEEK_40             NUMBER                        NULL,
  WEEK_41             NUMBER                        NULL,
  WEEK_42             NUMBER                        NULL,
  WEEK_43             NUMBER                        NULL,
  WEEK_44             NUMBER                        NULL,
  WEEK_45             NUMBER                        NULL,
  WEEK_46             NUMBER                        NULL,
  WEEK_47             NUMBER                        NULL,
  WEEK_48             NUMBER                        NULL,
  WEEK_49             NUMBER                        NULL,
  WEEK_50             NUMBER                        NULL,
  WEEK_51             NUMBER                        NULL,
  AWW                 NUMBER                        NULL,
  DAYS_00             NUMBER                        NULL,
  DAYS_01             NUMBER                        NULL,
  DAYS_02             NUMBER                        NULL,
  DAYS_03             NUMBER                        NULL,
  DAYS_04             NUMBER                        NULL,
  DAYS_05             NUMBER                        NULL,
  DAYS_06             NUMBER                        NULL,
  DAYS_07             NUMBER                        NULL,
  DAYS_08             NUMBER                        NULL,
  DAYS_09             NUMBER                        NULL,
  DAYS_10             NUMBER                        NULL,
  DAYS_11             NUMBER                        NULL,
  DAYS_12             NUMBER                        NULL,
  DAYS_13             NUMBER                        NULL,
  DAYS_14             NUMBER                        NULL,
  DAYS_15             NUMBER                        NULL,
  DAYS_16             NUMBER                        NULL,
  DAYS_17             NUMBER                        NULL,
  DAYS_18             NUMBER                        NULL,
  DAYS_19             NUMBER                        NULL,
  DAYS_20             NUMBER                        NULL,
  DAYS_21             NUMBER                        NULL,
  DAYS_22             NUMBER                        NULL,
  DAYS_23             NUMBER                        NULL,
  DAYS_24             NUMBER                        NULL,
  DAYS_25             NUMBER                        NULL,
  DAYS_26             NUMBER                        NULL,
  DAYS_27             NUMBER                        NULL,
  DAYS_28             NUMBER                        NULL,
  DAYS_29             NUMBER                        NULL,
  DAYS_30             NUMBER                        NULL,
  DAYS_31             NUMBER                        NULL,
  DAYS_32             NUMBER                        NULL,
  DAYS_33             NUMBER                        NULL,
  DAYS_34             NUMBER                        NULL,
  DAYS_35             NUMBER                        NULL,
  DAYS_36             NUMBER                        NULL,
  DAYS_37             NUMBER                        NULL,
  DAYS_38             NUMBER                        NULL,
  DAYS_39             NUMBER                        NULL,
  DAYS_40             NUMBER                        NULL,
  DAYS_41             NUMBER                        NULL,
  DAYS_42             NUMBER                        NULL,
  DAYS_43             NUMBER                        NULL,
  DAYS_44             NUMBER                        NULL,
  DAYS_45             NUMBER                        NULL,
  DAYS_46             NUMBER                        NULL,
  DAYS_47             NUMBER                        NULL,
  DAYS_48             NUMBER                        NULL,
  DAYS_49             NUMBER                        NULL,
  DAYS_50             NUMBER                        NULL,
  DAYS_51             NUMBER                        NULL,
  HOURS_00            NUMBER                        NULL,
  HOURS_01            NUMBER                        NULL,
  HOURS_02            NUMBER                        NULL,
  HOURS_03            NUMBER                        NULL,
  HOURS_04            NUMBER                        NULL,
  HOURS_05            NUMBER                        NULL,
  HOURS_06            NUMBER                        NULL,
  HOURS_07            NUMBER                        NULL,
  HOURS_08            NUMBER                        NULL,
  HOURS_09            NUMBER                        NULL,
  HOURS_10            NUMBER                        NULL,
  HOURS_11            NUMBER                        NULL,
  HOURS_12            NUMBER                        NULL,
  HOURS_13            NUMBER                        NULL,
  HOURS_14            NUMBER                        NULL,
  HOURS_15            NUMBER                        NULL,
  HOURS_16            NUMBER                        NULL,
  HOURS_17            NUMBER                        NULL,
  HOURS_18            NUMBER                        NULL,
  HOURS_19            NUMBER                        NULL,
  HOURS_20            NUMBER                        NULL,
  HOURS_21            NUMBER                        NULL,
  HOURS_22            NUMBER                        NULL,
  HOURS_23            NUMBER                        NULL,
  HOURS_24            NUMBER                        NULL,
  HOURS_25            NUMBER                        NULL,
  HOURS_26            NUMBER                        NULL,
  HOURS_27            NUMBER                        NULL,
  HOURS_28            NUMBER                        NULL,
  HOURS_29            NUMBER                        NULL,
  HOURS_30            NUMBER                        NULL,
  HOURS_31            NUMBER                        NULL,
  HOURS_32            NUMBER                        NULL,
  HOURS_33            NUMBER                        NULL,
  HOURS_34            NUMBER                        NULL,
  HOURS_35            NUMBER                        NULL,
  HOURS_36            NUMBER                        NULL,
  HOURS_37            NUMBER                        NULL,
  HOURS_38            NUMBER                        NULL,
  HOURS_39            NUMBER                        NULL,
  HOURS_40            NUMBER                        NULL,
  HOURS_41            NUMBER                        NULL,
  HOURS_42            NUMBER                        NULL,
  HOURS_43            NUMBER                        NULL,
  HOURS_44            NUMBER                        NULL,
  HOURS_45            NUMBER                        NULL,
  HOURS_46            NUMBER                        NULL,
  HOURS_47            NUMBER                        NULL,
  HOURS_48            NUMBER                        NULL,
  HOURS_49            NUMBER                        NULL,
  HOURS_50            NUMBER                        NULL,
  HOURS_51            NUMBER                        NULL,
  OVERTIME            NUMBER                        NULL,
  EMPLOYED_WEEKS      NUMBER(10)                    NULL,
  AW_TAX_STATUS_CODE  NUMBER(10)                    NULL,
  AWW_GROSS           NUMBER                        NULL,
  AWW_JURIS_CAPPED    NUMBER                        NULL,
  TTD_BASE            NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_AWW_X_ADD
(
  TABLE_ROW_ID       NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  FN_TABLE_ROW_ID    NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  PERIOD_TYPE_CODE   NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_AWW_X_PERIOD
(
  TABLE_ROW_ID       NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  PERIOD_TYPE_CODE   NUMBER(10)                     NULL,
  PERIOD_DATE        VARCHAR2(8 BYTE)               NULL,
  PERIOD_AMOUNT      NUMBER                         NULL,
  DAYS               NUMBER(10)                     NULL,
  HOURS              NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_STATUS_HIST
(
  CL_STATUS_ROW_ID  NUMBER(10)                      NULL,
  CLAIM_ID          NUMBER(10)                      NULL,
  DATE_STATUS_CHGD  VARCHAR2(8 BYTE)                NULL,
  STATUS_CODE       NUMBER(10)                      NULL,
  STATUS_CHGD_BY    VARCHAR2(8 BYTE)                NULL,
  REASON            CLOB                            NULL,
  APPROVED_BY       VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  TYPEOFREPORT_CODE   NUMBER(10)                    NULL,
  EMPL_ENRL_MCO_CODE  NUMBER(10)                    NULL,
  ABLE_RTW_DATE       VARCHAR2(8 BYTE)              NULL,
  PERM_LOSS_NUMB      NUMBER                        NULL,
  PERM_LOSS_TEXT      VARCHAR2(50 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_X_LITIGATION
(
  LIT_TYPE_CODE       NUMBER(10)                    NULL,
  LITIGATION_ROW_ID   NUMBER(10)                    NULL,
  CLAIM_ID            NUMBER(10)                    NULL,
  DOCKET_NUMBER       VARCHAR2(22 BYTE)             NULL,
  SUIT_DATE           VARCHAR2(8 BYTE)              NULL,
  COURT_DATE          VARCHAR2(8 BYTE)              NULL,
  JUDGE_EID           NUMBER(10)                    NULL,
  VENUE_STATE_ID      NUMBER(10)                    NULL,
  COUNTY              VARCHAR2(32 BYTE)             NULL,
  CO_ATTORNEY_EID     NUMBER(10)                    NULL,
  DEMAND_ALLEGATIONS  CLOB                          NULL,
  COMMENTS            CLOB                          NULL,
  LIT_STATUS_CODE     NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLAIM_X_REHAB
(
  REHAB_ID           NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  REHAB_STATUS_CODE  NUMBER(10)                     NULL,
  OPEN_DATE          VARCHAR2(8 BYTE)               NULL,
  CLOSE_DATE         VARCHAR2(8 BYTE)               NULL,
  WAIVER_DATE        VARCHAR2(8 BYTE)               NULL,
  PROVIDER_EID       NUMBER(10)                     NULL,
  RESOLUTION_CODE    NUMBER(10)                     NULL,
  RTW_DATE           VARCHAR2(8 BYTE)               NULL,
  CLOSURE_STAT_CODE  NUMBER(10)                     NULL,
  JOB_PROV_EID       NUMBER(10)                     NULL,
  REHAB_TYPE_CODE    NUMBER(10)                     NULL,
  COMMENTS           CLOB                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CLIENT_LIMITS
(
  CLIENT_EID         NUMBER(10)                     NULL,
  LOB_CODE           NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  PAYMENT_FLAG       NUMBER(5)                      NULL,
  MAX_AMOUNT         NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CL_CHG_OPTIONS
(
  CLCHGOPT_ROW_ID    NUMBER(10)                     NULL,
  LOB                NUMBER(10)                     NULL,
  SOURCE_CLAIM_TYPE  NUMBER(10)                     NULL,
  DEST_CLAIM_TYPE    NUMBER(10)                     NULL,
  TRIG_TRANS_TYPE    NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CL_FORMS
(
  STATE_ROW_ID       NUMBER(10)                     NULL,
  FORM_CATEGORY      NUMBER(10)                     NULL,
  FORM_ID            NUMBER(10)                     NULL,
  FORM_TITLE         VARCHAR2(128 BYTE)             NULL,
  FORM_NAME          VARCHAR2(64 BYTE)              NULL,
  FILE_NAME          VARCHAR2(64 BYTE)              NULL,
  ACTIVE_FLAG        NUMBER(10)                     NULL,
  HASH_CRC           VARCHAR2(128 BYTE)             NULL,
  PRIMARY_FORM_FLAG  NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CL_FORMS_CAT_LKUP
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  FORM_CAT       NUMBER(10)                         NULL,
  FORM_CAT_DESC  VARCHAR2(64 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CL_RES_PAY_LMTS
(
  GROUP_ID          NUMBER(10)                      NULL,
  USER_ID           NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL,
  RES_TYPE_CODE     NUMBER(10)                      NULL,
  MAX_AMOUNT        NUMBER                          NULL,
  CLAIM_TYPE_CODE   NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CM_X_ACCOMMODATION
(
  CM_ACCOMM_ROW_ID    NUMBER(10)                    NULL,
  CM_ROW_ID           NUMBER(10)                    NULL,
  ACCOMM_STATUS       NUMBER(10)                    NULL,
  ACCOMM_SCOPE        NUMBER(10)                    NULL,
  ACCOMM_TYPE         NUMBER(10)                    NULL,
  ACCOMM_POSITION     NUMBER(10)                    NULL,
  ACCOMM_DEPARTMENT   NUMBER(10)                    NULL,
  JOB_CLASS_CODE      NUMBER(10)                    NULL,
  MMI_DATE            VARCHAR2(8 BYTE)              NULL,
  ACCOMM_REQUESTED    VARCHAR2(8 BYTE)              NULL,
  ACCOMM_OFFERED      VARCHAR2(8 BYTE)              NULL,
  ACCOMM_ACCEPTED     VARCHAR2(8 BYTE)              NULL,
  ACCOMM_RESTRICTION  CLOB                          NULL,
  ACCOMM_TEXT         CLOB                          NULL,
  NET_ACCOMM_COST     NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CM_X_CMGR_HIST
(
  CMCMH_ROW_ID       NUMBER(10)                     NULL,
  CASEMGT_ROW_ID     NUMBER(10)                     NULL,
  REF_TYPE_CODE      NUMBER(10)                     NULL,
  REF_DATE           VARCHAR2(8 BYTE)               NULL,
  CASE_STATUS_CODE   NUMBER(10)                     NULL,
  DATE_CLOSED        VARCHAR2(8 BYTE)               NULL,
  REF_TO_EID         NUMBER(10)                     NULL,
  REF_REASON_CODE    NUMBER(10)                     NULL,
  CASE_MGR_EID       NUMBER(10)                     NULL,
  PRIMARY_CMGR_FLAG  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CM_X_MEDMGTSAVINGS
(
  CMMS_ROW_ID            NUMBER(10)                 NULL,
  CASEMGT_ROW_ID         NUMBER(10)                 NULL,
  PROVIDER_EID           NUMBER(10)                 NULL,
  TREATMENT_PROVIDER     CLOB                       NULL,
  SAVINGS_TYPE_CODE      NUMBER(10)                 NULL,
  TRANSACTION_TYPE_CODE  NUMBER(10)                 NULL,
  QUOTED_AMOUNT          NUMBER                     NULL,
  AGREED_AMOUNT          NUMBER                     NULL,
  SAVINGS                NUMBER                     NULL,
  SUMMARY                CLOB                       NULL,
  CASE_MGR_EID           NUMBER(10)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CM_X_TREATMENT_PLN
(
  CMTP_ROW_ID          NUMBER(10)                   NULL,
  CASEMGT_ROW_ID       NUMBER(10)                   NULL,
  PROVIDER_EID         NUMBER(10)                   NULL,
  TREATMENT_PROVIDER   CLOB                         NULL,
  DATE_REQUESTED       VARCHAR2(8 BYTE)             NULL,
  REQUESTED_TREATMENT  CLOB                         NULL,
  PLAN_STATUS_CODE     NUMBER(10)                   NULL,
  TREATMENT_TYPE_CODE  NUMBER(10)                   NULL,
  PROCEDURE_CODE       NUMBER(10)                   NULL,
  REASON               CLOB                         NULL,
  APPROVED_TREATMENT   CLOB                         NULL,
  APPROVED_BY_EID      NUMBER(10)                   NULL,
  APPROVED_DATE        VARCHAR2(8 BYTE)             NULL,
  CRITERIA_CODE        NUMBER(10)                   NULL,
  NO_REQ_VISITS        NUMBER(10)                   NULL,
  NO_APP_VISITS        NUMBER(10)                   NULL,
  REQUEST_TYPE_CODE    NUMBER(10)                   NULL,
  DATE_AUTH_START      VARCHAR2(8 BYTE)             NULL,
  DATE_AUTH_END        VARCHAR2(8 BYTE)             NULL,
  REVIEW_TYPE_CODE     NUMBER(10)                   NULL,
  REVIEWER_EID         NUMBER(10)                   NULL,
  DATE_REFERRAL        VARCHAR2(8 BYTE)             NULL,
  DATE_RECEIVED        VARCHAR2(8 BYTE)             NULL,
  DATE_APPEAL          VARCHAR2(8 BYTE)             NULL,
  APP_DECISION_CODE    NUMBER(10)                   NULL,
  NO_HOURS_BILLED      NUMBER                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CM_X_VOCREHAB
(
  CMVR_ROW_ID        NUMBER(10)                     NULL,
  CASEMGT_ROW_ID     NUMBER(10)                     NULL,
  REHAB_TYPE_CODE    NUMBER(10)                     NULL,
  REHAB_STATUS_CODE  NUMBER(10)                     NULL,
  PROVIDER_EID       NUMBER(10)                     NULL,
  OCC_PROPOSED       CLOB                           NULL,
  OUTCOMES_CODE      NUMBER(10)                     NULL,
  START_DATE         VARCHAR2(8 BYTE)               NULL,
  END_DATE           VARCHAR2(8 BYTE)               NULL,
  EST_COST_OF_PLAN   NUMBER                         NULL,
  ACT_COST_OF_PLAN   NUMBER                         NULL,
  DIFFERENCE         NUMBER                         NULL,
  BUY_OUT_COST       NUMBER                         NULL,
  STATE_REFUND       NUMBER                         NULL,
  PLAN_DEFINITION    CLOB                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CODES
(
  IND_STANDARD_CODE   NUMBER(10)                    NULL,
  LINE_OF_BUS_CODE    NUMBER(10)                    NULL,
  CODE_ID             NUMBER(10)                    NULL,
  TABLE_ID            NUMBER(10)                    NULL,
  SHORT_CODE          VARCHAR2(25 BYTE)             NULL,
  RELATED_CODE_ID     NUMBER(10)                    NULL,
  DELETED_FLAG        NUMBER(5)                     NULL,
  TRIGGER_DATE_FIELD  VARCHAR2(50 BYTE)             NULL,
  EFF_START_DATE      VARCHAR2(8 BYTE)              NULL,
  EFF_END_DATE        VARCHAR2(8 BYTE)              NULL,
  ORG_GROUP_EID       NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CODES_TEXT
(
  CODE_ID        NUMBER(10)                         NULL,
  LANGUAGE_CODE  NUMBER(10)                         NULL,
  SHORT_CODE     VARCHAR2(25 BYTE)                  NULL,
  CODE_DESC      VARCHAR2(50 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE COMMANDS_TO_LOG
(
  COMMAND_CODE  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE COMPL_MGT
(
  COMPL_MGT_ID      NUMBER(10)                      NULL,
  COMP_NO_TEXT      VARCHAR2(25 BYTE)               NULL,
  DT_RECV_DATE      VARCHAR2(8 BYTE)                NULL,
  COMP_TYPE_CODE    NUMBER(10)                      NULL,
  COMP_SRC_CODE     NUMBER(10)                      NULL,
  ACT_REQ_TXCD      CLOB                            NULL,
  FILED_AGNST_TEXT  VARCHAR2(50 BYTE)               NULL,
  REF_TO_TXCD       CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CONTRCT_MGT
(
  CONTRCT_MGT_ID    NUMBER(10)                      NULL,
  CONT_TYPE_CODE    NUMBER(10)                      NULL,
  CONTR_CAT_TXCD    CLOB                            NULL,
  SUBJECTOR_TEXT    VARCHAR2(50 BYTE)               NULL,
  SUBJECTEE_TEXT    VARCHAR2(50 BYTE)               NULL,
  DETAILS_TXCD      CLOB                            NULL,
  EFF_DATE_DATE     VARCHAR2(8 BYTE)                NULL,
  EXPIR_DATE_DATE   VARCHAR2(8 BYTE)                NULL,
  RENEW_DATE_DATE   VARCHAR2(8 BYTE)                NULL,
  INDEMNITY_TXCD    CLOB                            NULL,
  SUBROGATION_TXCD  CLOB                            NULL,
  TERM_INFO_TEXT    VARCHAR2(50 BYTE)               NULL,
  CONDITIONS_TXCD   CLOB                            NULL,
  AUTH_SIGN_TXCD    CLOB                            NULL,
  PAYMENTS_TXCD     CLOB                            NULL,
  WARRANTY_TXCD     CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE COVERAGE_DETAIL
(
  ROW_ID              NUMBER(10)                    NULL,
  UNIT_ID             NUMBER(10)                    NULL,
  CLAIM_ID            NUMBER(10)                    NULL,
  COVERAGE_CODE       NUMBER(10)                    NULL,
  COVERAGE_RECORD_ID  NUMBER(10)                    NULL,
  CVG_TABLE_NAME      VARCHAR2(50 BYTE)             NULL,
  LIMIT               NUMBER                        NULL,
  DEDUCTIBLE          NUMBER                        NULL,
  CAUSE               VARCHAR2(6 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_UPD        VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE COVERAGE_X_FUNDS
(
  TRANS_ID       NUMBER(10)                         NULL,
  POLCVG_ROW_ID  NUMBER(10)                         NULL,
  AMOUNT         NUMBER                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CUST_LABELS
(
  FORMNAME   VARCHAR2(40 BYTE)                      NULL,
  CONTROLID  NUMBER(10)                             NULL,
  CTRLNAME   VARCHAR2(40 BYTE)                      NULL,
  CTRLINDEX  NUMBER(10)                             NULL,
  NEW_LABEL  VARCHAR2(255 BYTE)                     NULL,
  LABEL      NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CUST_RATE
(
  RATE_TABLE_ID      NUMBER(10)                     NULL,
  TABLE_NAME         VARCHAR2(100 BYTE)             NULL,
  CUSTOMER_EID       NUMBER(10)                     NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CUST_RATE_DETAIL
(
  RATE_TABLE_ID      NUMBER(10)                     NULL,
  TRANS_TYPE_CODE    NUMBER(10)                     NULL,
  UNIT               VARCHAR2(40 BYTE)              NULL,
  RATE               NUMBER                         NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CVG_ENH_SUPP
(
  POLCVG_ROW_ID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE CVG_TYPE_SUPP
(
  POLCVG_ROW_ID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DCC_REPORTS
(
  REPORT_ID     NUMBER(10)                          NULL,
  TEMPLATE_ID   NUMBER(10)                          NULL,
  REPORT_NAME   VARCHAR2(250 BYTE)                  NULL,
  REPORT_XML    CLOB                                NULL,
  DELETED_FLAG  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DCC_REPORTS_USER
(
  USERID     NUMBER(10)                             NULL,
  REPORT_ID  NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DCC_TEMPLATES
(
  TEMPLATE_ID    NUMBER(10)                         NULL,
  TEMPLATE_NAME  VARCHAR2(250 BYTE)                 NULL,
  TEMPLATE_XML   CLOB                               NULL,
  DELETED_FLAG   NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DEFENDANT
(
  DEFENDANT_ROW_ID  NUMBER(10)                      NULL,
  CLAIM_ID          NUMBER(10)                      NULL,
  DEFENDANT_EID     NUMBER(10)                      NULL,
  INSURER_EID       NUMBER(10)                      NULL,
  ATTORNEY_EID      NUMBER(10)                      NULL,
  POLICY_NUMBER     VARCHAR2(30 BYTE)               NULL,
  POLICY_TYPE_CODE  NUMBER(10)                      NULL,
  POLICY_LIMIT      NUMBER                          NULL,
  POL_EXPIRE_DATE   VARCHAR2(8 BYTE)                NULL,
  DFNDNT_TYPE_CODE  NUMBER(10)                      NULL,
  COMMENTS          CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DEFENDANT_SUPP
(
  DEFENDANT_ROW_ID  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DIRECTORY
(
  DIRECTORY_ID      NUMBER(10)                      NULL,
  LAST_NAME_TEXT    VARCHAR2(50 BYTE)               NULL,
  FIRST_NAME_TEXT   VARCHAR2(50 BYTE)               NULL,
  ADDRESS_TEXT      VARCHAR2(50 BYTE)               NULL,
  CITY_TEXT         VARCHAR2(50 BYTE)               NULL,
  STATE_TEXT        VARCHAR2(15 BYTE)               NULL,
  ZIP_CODE_TEXT     VARCHAR2(15 BYTE)               NULL,
  HOME_PHONE_TEXT   VARCHAR2(25 BYTE)               NULL,
  WORK_PHONE_TEXT   VARCHAR2(25 BYTE)               NULL,
  FAX_TEXT          VARCHAR2(25 BYTE)               NULL,
  TITLE_TEXT        VARCHAR2(25 BYTE)               NULL,
  ORGANIZ_TEXT      VARCHAR2(50 BYTE)               NULL,
  CONTACT_TYP_CODE  NUMBER(10)                      NULL,
  CONTACT_STA_CODE  NUMBER(10)                      NULL,
  RECALL_DT_DATE    VARCHAR2(8 BYTE)                NULL,
  NOTES_TXCD        CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DOCUMENT
(
  DOCUMENT_ID         NUMBER(10)                    NULL,
  ARCHIVE_LEVEL       NUMBER(10)                    NULL,
  CREATE_DATE         VARCHAR2(14 BYTE)             NULL,
  DOCUMENT_CATEGORY   NUMBER(10)                    NULL,
  DOCUMENT_NAME       VARCHAR2(32 BYTE)             NULL,
  DOCUMENT_CLASS      NUMBER(10)                    NULL,
  DOCUMENT_SUBJECT    VARCHAR2(50 BYTE)             NULL,
  DOCUMENT_TYPE       NUMBER(10)                    NULL,
  NOTES               VARCHAR2(200 BYTE)            NULL,
  SECURITY_LEVEL      NUMBER(10)                    NULL,
  USER_ID             VARCHAR2(8 BYTE)              NULL,
  DOC_INTERNAL_TYPE   NUMBER(10)                    NULL,
  DOCUMENT_CREAT_APP  VARCHAR2(60 BYTE)             NULL,
  DOCUMENT_EXPDTTM    VARCHAR2(14 BYTE)             NULL,
  DOCUMENT_ACTIVE     NUMBER(5)                     NULL,
  DOCUMENT_FILENAME   VARCHAR2(255 BYTE)            NULL,
  DOCUMENT_FILEPATH   VARCHAR2(255 BYTE)            NULL,
  FOLDER_ID           NUMBER(10)                    NULL,
  KEYWORDS            VARCHAR2(200 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DOCUMENT_ATTACH
(
  TABLE_NAME   VARCHAR2(18 BYTE)                    NULL,
  RECORD_ID    NUMBER(10)                           NULL,
  DOCUMENT_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DOCUMENT_MMTMPL
(
  SYS_TABLE_NAME  VARCHAR2(18 BYTE)                 NULL,
  SUB_ID          NUMBER(10)                        NULL,
  DOCUMENT_ID     NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE DOC_FOLDERS
(
  FOLDER_ID    NUMBER(10)                           NULL,
  PARENT_ID    NUMBER(10)                           NULL,
  FOLDER_NAME  VARCHAR2(128 BYTE)                   NULL,
  FOLDER_PATH  VARCHAR2(128 BYTE)                   NULL,
  USER_ID      NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EDI_MTC_MAP
(
  STATUS_CODE_ID  NUMBER(10)                        NULL,
  MTC_CODE_ID     NUMBER(10)                        NULL,
  MTC_CODE_DESC   VARCHAR2(50 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EMPLOYEE
(
  COUNTY_OF_RESIDNC   VARCHAR2(50 BYTE)             NULL,
  DRIVLIC_STATE       NUMBER(10)                    NULL,
  INSURABLE_FLAG      NUMBER(5)                     NULL,
  LAST_VERIFIED_DATE  VARCHAR2(8 BYTE)              NULL,
  NUM_OF_VIOLATIONS   NUMBER(10)                    NULL,
  TERM_DATE           VARCHAR2(8 BYTE)              NULL,
  WORK_PERMIT_DATE    VARCHAR2(8 BYTE)              NULL,
  WORK_PERMIT_NUMBER  VARCHAR2(50 BYTE)             NULL,
  EMPLOYEE_EID        NUMBER(10)                    NULL,
  ACTIVE_FLAG         NUMBER(5)                     NULL,
  EMPLOYEE_NUMBER     VARCHAR2(20 BYTE)             NULL,
  MARITAL_STAT_CODE   NUMBER(10)                    NULL,
  DATE_HIRED          VARCHAR2(8 BYTE)              NULL,
  POSITION_CODE       NUMBER(10)                    NULL,
  DEPT_ASSIGNED_EID   NUMBER(10)                    NULL,
  SUPERVISOR_EID      NUMBER(10)                    NULL,
  EXEMPT_STATUS_FLAG  NUMBER(5)                     NULL,
  NO_OF_EXEMPTIONS    NUMBER(10)                    NULL,
  FULL_TIME_FLAG      NUMBER(5)                     NULL,
  HOURLY_RATE         NUMBER                        NULL,
  WEEKLY_HOURS        NUMBER(10)                    NULL,
  WEEKLY_RATE         NUMBER                        NULL,
  WORK_SUN_FLAG       NUMBER(5)                     NULL,
  WORK_MON_FLAG       NUMBER(5)                     NULL,
  WORK_TUE_FLAG       NUMBER(5)                     NULL,
  WORK_WED_FLAG       NUMBER(5)                     NULL,
  WORK_THU_FLAG       NUMBER(5)                     NULL,
  WORK_FRI_FLAG       NUMBER(5)                     NULL,
  WORK_SAT_FLAG       NUMBER(5)                     NULL,
  PAY_TYPE_CODE       NUMBER(10)                    NULL,
  PAY_AMOUNT          NUMBER                        NULL,
  DRIVERS_LIC_NO      VARCHAR2(20 BYTE)             NULL,
  DRIVERSLICTYPECODE  NUMBER(10)                    NULL,
  DATE_DRIVERSLICEXP  VARCHAR2(8 BYTE)              NULL,
  DRIVLIC_RSTRCTCODE  NUMBER(10)                    NULL,
  NCCI_CLASS_CODE     NUMBER(10)                    NULL,
  DATE_OF_DEATH       VARCHAR2(8 BYTE)              NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL,
  JOB_CLASS_CODE      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EMP_SUPP
(
  EMPLOYEE_EID  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EMP_X_DEPENDENT
(
  EMP_DEP_ROW_ID    NUMBER(10)                      NULL,
  EMPLOYEE_EID      NUMBER(10)                      NULL,
  DEPENDENT_EID     NUMBER(10)                      NULL,
  HEALTH_PLAN_FLAG  NUMBER(5)                       NULL,
  RELATION_CODE     NUMBER(10)                      NULL,
  DEPENDENT_ROW_ID  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EMP_X_DEP_SUPP
(
  EMP_DEP_ROW_ID  NUMBER(10)                        NULL,
  DEPENDENT_EID   NUMBER(10)                        NULL,
  EMPLOYEE_EID    NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EMP_X_VIOLATION
(
  VIOLATION_ID    NUMBER(10)                        NULL,
  EMPLOYEE_EID    NUMBER(10)                        NULL,
  VIOLATION_CODE  NUMBER(10)                        NULL,
  VIOLATION_DATE  VARCHAR2(8 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY
(
  BUSINESS_TYPE_CODE  NUMBER(10)                    NULL,
  COUNTY              VARCHAR2(30 BYTE)             NULL,
  NATURE_OF_BUSINESS  VARCHAR2(50 BYTE)             NULL,
  SIC_CODE            NUMBER(10)                    NULL,
  SIC_CODE_DESC       VARCHAR2(150 BYTE)            NULL,
  WC_FILING_NUMBER    VARCHAR2(30 BYTE)             NULL,
  ENTITY_ID           NUMBER(10)                    NULL,
  ENTITY_TABLE_ID     NUMBER(10)                    NULL,
  LAST_NAME           VARCHAR2(50 BYTE)             NULL,
  LAST_NAME_SOUNDEX   VARCHAR2(8 BYTE)              NULL,
  FIRST_NAME          VARCHAR2(50 BYTE)             NULL,
  ALSO_KNOWN_AS       VARCHAR2(50 BYTE)             NULL,
  ABBREVIATION        VARCHAR2(25 BYTE)             NULL,
  COST_CENTER_CODE    NUMBER(10)                    NULL,
  ADDR1               VARCHAR2(50 BYTE)             NULL,
  ADDR2               VARCHAR2(50 BYTE)             NULL,
  CITY                VARCHAR2(50 BYTE)             NULL,
  COUNTRY_CODE        NUMBER(10)                    NULL,
  STATE_ID            NUMBER(10)                    NULL,
  ZIP_CODE            VARCHAR2(10 BYTE)             NULL,
  PARENT_EID          NUMBER(10)                    NULL,
  TAX_ID              VARCHAR2(20 BYTE)             NULL,
  CONTACT             VARCHAR2(50 BYTE)             NULL,
  COMMENTS            CLOB                          NULL,
  EMAIL_TYPE_CODE     NUMBER(10)                    NULL,
  EMAIL_ADDRESS       VARCHAR2(100 BYTE)            NULL,
  SEX_CODE            NUMBER(10)                    NULL,
  BIRTH_DATE          VARCHAR2(8 BYTE)              NULL,
  PHONE1              VARCHAR2(30 BYTE)             NULL,
  PHONE2              VARCHAR2(30 BYTE)             NULL,
  FAX_NUMBER          VARCHAR2(30 BYTE)             NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DELETED_FLAG        NUMBER(5)                     NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL,
  TRIGGER_DATE_FIELD  VARCHAR2(50 BYTE)             NULL,
  EFF_START_DATE      VARCHAR2(8 BYTE)              NULL,
  EFF_END_DATE        VARCHAR2(8 BYTE)              NULL,
  PARENT_1099_EID     NUMBER(10)                    NULL,
  REPORT_1099_FLAG    NUMBER(5)                     NULL,
  MIDDLE_NAME         VARCHAR2(50 BYTE)             NULL,
  TITLE               VARCHAR2(50 BYTE)             NULL,
  NAICS_CODE          NUMBER(10)                    NULL,
  RM_USER_ID          NUMBER(10)                    NULL,
  FREEZE_PAYMENTS     NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_EXPOSURE
(
  EXPOSURE_ROW_ID     NUMBER(10)                    NULL,
  ENTITY_ID           NUMBER(10)                    NULL,
  START_DATE          VARCHAR2(8 BYTE)              NULL,
  END_DATE            VARCHAR2(8 BYTE)              NULL,
  NO_OF_EMPLOYEES     NUMBER(10)                    NULL,
  NO_OF_WORK_HOURS    NUMBER(10)                    NULL,
  PAYROLL_AMOUNT      NUMBER                        NULL,
  ASSET_VALUE         NUMBER                        NULL,
  SQUARE_FOOTAGE      NUMBER(10)                    NULL,
  VEHICLE_COUNT       NUMBER(10)                    NULL,
  TOTAL_REVENUE       NUMBER                        NULL,
  OTHER_BASE          NUMBER                        NULL,
  RISK_MGMT_OVERHEAD  NUMBER                        NULL,
  USER_GENERATD_FLAG  NUMBER(5)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_MAP
(
  ENTITY_ID  NUMBER(10)                             NULL,
  GROUP_ID   NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_MAP_USER
(
  USER_NAME  VARCHAR2(16 BYTE)                      NULL,
  GROUP_ID   NUMBER(10)                             NULL,
  DEPT_EID   NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_SUPP
(
  ENTITY_ID          NUMBER(10)                     NULL,
  INDEX_BUREAU_NUMB  VARCHAR2(12 BYTE)              NULL,
  STE_AGNT_BRU_IDNT  VARCHAR2(12 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_X_CODELICEN
(
  TABLE_ROW_ID        NUMBER(10)                    NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ENTITY_ID           NUMBER(10)                    NULL,
  DELETED_FLAG        NUMBER(10)                    NULL,
  JURIS_ROW_ID        NUMBER(10)                    NULL,
  CODELICENSE_NUMBER  VARCHAR2(64 BYTE)             NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  LOB_ALL             NUMBER(10)                    NULL,
  LOB_WC              NUMBER(10)                    NULL,
  LOB_STD             NUMBER(10)                    NULL,
  LOB_VA              NUMBER(10)                    NULL,
  LOB_GL              NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENTITY_X_SELFINSUR
(
  SI_ROW_ID          NUMBER(10)                     NULL,
  ENTITY_ID          NUMBER(10)                     NULL,
  JURIS_ROW_ID       NUMBER(10)                     NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  LEGAL_ENTITY_EID   NUMBER(10)                     NULL,
  CERT_NAME_EID      NUMBER(10)                     NULL,
  CERT_NUMBER        VARCHAR2(64 BYTE)              NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL,
  SI_AUTHORIZATION   NUMBER(10)                     NULL,
  SI_ORGANIZATION    NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENT_EXPOSURE_SUPP
(
  EXPOSURE_ROW_ID  NUMBER(10)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENT_INSTRUCTIONS
(
  ENTITY_ID       NUMBER(10)                        NULL,
  ENT_INSTR_TEXT  CLOB                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENT_X_CONTACTINFO
(
  ENTITY_ID      NUMBER(10)                         NULL,
  CONTACT_ID     NUMBER(10)                         NULL,
  CONTACT_NAME   VARCHAR2(64 BYTE)                  NULL,
  TITLE          VARCHAR2(64 BYTE)                  NULL,
  INITIALS       VARCHAR2(10 BYTE)                  NULL,
  ADDR1          VARCHAR2(64 BYTE)                  NULL,
  ADDR2          VARCHAR2(64 BYTE)                  NULL,
  CITY           VARCHAR2(32 BYTE)                  NULL,
  STATE_ID       NUMBER(10)                         NULL,
  ZIP_CODE       VARCHAR2(10 BYTE)                  NULL,
  PHONE1         VARCHAR2(30 BYTE)                  NULL,
  FAX_NUMBER     VARCHAR2(30 BYTE)                  NULL,
  EMAIL_ADDRESS  VARCHAR2(64 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ENT_X_OPERATINGAS
(
  ENTITY_ID     NUMBER(10)                          NULL,
  OPERATING_ID  NUMBER(10)                          NULL,
  "INITIAL"     VARCHAR2(10 BYTE)                   NULL,
  OPERATING_AS  VARCHAR2(64 BYTE)                   NULL,
  INITIALS      VARCHAR2(10 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT
(
  COUNTY_OF_INJURY    VARCHAR2(50 BYTE)             NULL,
  EVENT_ID            NUMBER(10)                    NULL,
  EVENT_NUMBER        VARCHAR2(25 BYTE)             NULL,
  EVENT_TYPE_CODE     NUMBER(10)                    NULL,
  EVENT_STATUS_CODE   NUMBER(10)                    NULL,
  EVENT_IND_CODE      NUMBER(10)                    NULL,
  EVENT_DESCRIPTION   CLOB                          NULL,
  BRIEF_DESC          VARCHAR2(250 BYTE)            NULL,
  DEPT_EID            NUMBER(10)                    NULL,
  DEPT_INVOLVED_EID   NUMBER(10)                    NULL,
  ADDR1               VARCHAR2(50 BYTE)             NULL,
  ADDR2               VARCHAR2(50 BYTE)             NULL,
  CITY                VARCHAR2(50 BYTE)             NULL,
  STATE_ID            NUMBER(10)                    NULL,
  ZIP_CODE            VARCHAR2(10 BYTE)             NULL,
  COUNTRY_CODE        NUMBER(10)                    NULL,
  LOCATION_AREA_DESC  CLOB                          NULL,
  PRIMARY_LOC_CODE    NUMBER(10)                    NULL,
  LOCATION_TYPE_CODE  NUMBER(10)                    NULL,
  ON_PREMISE_FLAG     NUMBER(5)                     NULL,
  NO_OF_INJURIES      NUMBER(10)                    NULL,
  NO_OF_FATALITIES    NUMBER(10)                    NULL,
  CAUSE_CODE          NUMBER(10)                    NULL,
  DATE_OF_EVENT       VARCHAR2(8 BYTE)              NULL,
  TIME_OF_EVENT       VARCHAR2(6 BYTE)              NULL,
  DATE_REPORTED       VARCHAR2(8 BYTE)              NULL,
  TIME_REPORTED       VARCHAR2(6 BYTE)              NULL,
  RPTD_BY_EID         NUMBER(10)                    NULL,
  DATE_RPTD_TO_RM     VARCHAR2(8 BYTE)              NULL,
  DATE_TO_FOLLOW_UP   VARCHAR2(8 BYTE)              NULL,
  COMMENTS            CLOB                          NULL,
  ACCOUNT_ID          NUMBER(10)                    NULL,
  DATE_PHYS_ADVISED   VARCHAR2(8 BYTE)              NULL,
  TIME_PHYS_ADVISED   VARCHAR2(6 BYTE)              NULL,
  TREATMENT_GIVEN     NUMBER(5)                     NULL,
  RELEASE_SIGNED      NUMBER(5)                     NULL,
  DEPT_HEAD_ADVISED   NUMBER(5)                     NULL,
  PHYS_NOTES          CLOB                          NULL,
  DATE_CARRIER_NOTIF  VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL,
  INJURY_FROM_DATE    VARCHAR2(8 BYTE)              NULL,
  INJURY_TO_DATE      VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_ACC_LIMITS
(
  GROUP_ID         NUMBER(10)                       NULL,
  USER_ID          NUMBER(10)                       NULL,
  EVENT_TYPE_CODE  NUMBER(10)                       NULL,
  CREATE_FLAG      NUMBER(5)                        NULL,
  ACCESS_FLAG      NUMBER(5)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_QM
(
  EVENT_ID           NUMBER(10)                     NULL,
  MED_CASE_NUMBER    VARCHAR2(20 BYTE)              NULL,
  MED_FILE_CODE      NUMBER(10)                     NULL,
  MED_IND_CODE       NUMBER(10)                     NULL,
  IR_STATUS_CODE     NUMBER(10)                     NULL,
  IR_REVIEWER_UID    NUMBER(10)                     NULL,
  IR_REVIEWER_NAME   VARCHAR2(20 BYTE)              NULL,
  IR_DETERM_CODE     NUMBER(10)                     NULL,
  IR_REVIEW_DATE     VARCHAR2(8 BYTE)               NULL,
  IR_FOLLOWUP_DATE   VARCHAR2(8 BYTE)               NULL,
  IR_COMMENTS        CLOB                           NULL,
  IR_REC_ACTIONS     CLOB                           NULL,
  PA_STATUS_CODE     NUMBER(10)                     NULL,
  PA_REVIEWER_EID    NUMBER(10)                     NULL,
  PA_DETERM_CODE     NUMBER(10)                     NULL,
  PA_REVIEW_DATE     VARCHAR2(8 BYTE)               NULL,
  PA_FOLLOWUP_DATE   VARCHAR2(8 BYTE)               NULL,
  PA_COMMENTS        CLOB                           NULL,
  PA_REC_ACTIONS     CLOB                           NULL,
  CD_STATUS_CODE     NUMBER(10)                     NULL,
  CD_COMMITTEE_CODE  NUMBER(10)                     NULL,
  CD_DEPT_EID        NUMBER(10)                     NULL,
  CD_DETERM_CODE     NUMBER(10)                     NULL,
  CD_REVIEW_DATE     VARCHAR2(8 BYTE)               NULL,
  CD_FOLLOWUP_DATE   VARCHAR2(8 BYTE)               NULL,
  CD_COMMENTS        CLOB                           NULL,
  CD_REC_ACTIONS     CLOB                           NULL,
  QM_STATUS_CODE     NUMBER(10)                     NULL,
  QM_REVIEWER_UID    NUMBER(10)                     NULL,
  QM_REVIEWER_NAME   VARCHAR2(20 BYTE)              NULL,
  QM_DETERM_CODE     NUMBER(10)                     NULL,
  QM_REVIEW_DATE     VARCHAR2(8 BYTE)               NULL,
  QM_CLOSE_DATE      VARCHAR2(8 BYTE)               NULL,
  QM_COMMENTS        CLOB                           NULL,
  QM_REC_ACTIONS     CLOB                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_SENTINEL
(
  EVENT_ID           NUMBER(10)                     NULL,
  SENT_EVENT_DESC    CLOB                           NULL,
  DAMAGE_CONS        VARCHAR2(255 BYTE)             NULL,
  DIFF_CHANGED       VARCHAR2(255 BYTE)             NULL,
  PREVENT            VARCHAR2(255 BYTE)             NULL,
  PROC_FOLLOWED      NUMBER(5)                      NULL,
  AVOID_WORSE        NUMBER(5)                      NULL,
  STAFF_KNOW_HANDLE  NUMBER(5)                      NULL,
  HAPPENED_BEFORE    NUMBER(5)                      NULL,
  PREVIOUS_FIX       VARCHAR2(255 BYTE)             NULL,
  UNUSUAL_REPORT     VARCHAR2(255 BYTE)             NULL,
  COMMENTS           CLOB                           NULL,
  REPORT_LEVEL       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_SUPP
(
  EVENT_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_ACTION
(
  EVENT_ID     NUMBER(10)                           NULL,
  ACTION_CODE  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_DATED_TEXT
(
  EV_DT_ROW_ID       NUMBER(10)                     NULL,
  EVENT_ID           NUMBER(10)                     NULL,
  ENTERED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DATE_ENTERED       VARCHAR2(8 BYTE)               NULL,
  TIME_ENTERED       VARCHAR2(6 BYTE)               NULL,
  DATED_TEXT         CLOB                           NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_MEDWATCH
(
  EVENT_ID            NUMBER(10)                    NULL,
  UNIT_CODE           NUMBER(10)                    NULL,
  DATE_RPTD_TO_FDA    VARCHAR2(8 BYTE)              NULL,
  DATE_RPTD_TO_MFG    VARCHAR2(8 BYTE)              NULL,
  RPTD_BY_EID         NUMBER(10)                    NULL,
  RPTD_BY_PRO_FLAG    NUMBER(5)                     NULL,
  RPTD_BY_POS_CODE    NUMBER(10)                    NULL,
  RELEVANT_HISTORY    CLOB                          NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  LAST_REPORT_DATE    VARCHAR2(8 BYTE)              NULL,
  REPORT_YEAR         NUMBER(10)                    NULL,
  REPORT_SERIAL_NO    NUMBER(10)                    NULL,
  ADVERSE_EVENT_FLAG  NUMBER(5)                     NULL,
  PRODUCT_PROB_FLAG   NUMBER(5)                     NULL,
  LIFE_THREAT_FLAG    NUMBER(5)                     NULL,
  HOSPITALIZ_FLAG     NUMBER(5)                     NULL,
  DISABILITY_FLAG     NUMBER(5)                     NULL,
  CONGENITAL_FLAG     NUMBER(5)                     NULL,
  REQD_INTERV_FLAG    NUMBER(5)                     NULL,
  FOLLOW_UP_COUNT     NUMBER(10)                    NULL,
  RPT_MANDATORY       NUMBER(5)                     NULL,
  MED_TYPE            NUMBER(10)                    NULL,
  MED_NAME            VARCHAR2(25 BYTE)             NULL,
  DOSE                VARCHAR2(25 BYTE)             NULL,
  FREQUENCY           VARCHAR2(25 BYTE)             NULL,
  ROUTE               VARCHAR2(25 BYTE)             NULL,
  THERAPY_FROM_DATE   VARCHAR2(8 BYTE)              NULL,
  THERAPY_TO_DATE     VARCHAR2(8 BYTE)              NULL,
  DURATION            NUMBER(10)                    NULL,
  LOT_NUMBER          VARCHAR2(15 BYTE)             NULL,
  NDC_NUMBER          NUMBER(10)                    NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  MW_EVENT_ABATED     NUMBER(5)                     NULL,
  MW_EVNT_REAPPEARED  NUMBER(5)                     NULL,
  DIAGNOSIS_TEST      VARCHAR2(25 BYTE)             NULL,
  BRAND_NAME          VARCHAR2(25 BYTE)             NULL,
  EQUIP_TYPE_CODE     NUMBER(10)                    NULL,
  MANUFACTURER_EID    NUMBER(10)                    NULL,
  MODEL_NUMBER        VARCHAR2(15 BYTE)             NULL,
  CATALOG_NUMBER      VARCHAR2(15 BYTE)             NULL,
  SERIAL_NUMBER       VARCHAR2(15 BYTE)             NULL,
  OTHER_NUMBER        VARCHAR2(15 BYTE)             NULL,
  DEVICE_OPER_CODE    NUMBER(10)                    NULL,
  IMPLANT_DATE        VARCHAR2(8 BYTE)              NULL,
  EXPLANT_DATE        VARCHAR2(8 BYTE)              NULL,
  AGE_OF_DEVICE       VARCHAR2(12 BYTE)             NULL,
  DEV_AVAIL_FOR_EVAL  NUMBER(5)                     NULL,
  DEVICE_RETURN_DATE  VARCHAR2(8 BYTE)              NULL,
  EQ_LOT_NUMBER       VARCHAR2(15 BYTE)             NULL,
  EQ_EXPIRATION_DATE  VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_MEDW_SUPP
(
  EVENT_ID            NUMBER(10)                    NULL,
  PATINT_CODE_A_TEXT  VARCHAR2(16 BYTE)             NULL,
  PATINT_CODE_B_TEXT  VARCHAR2(16 BYTE)             NULL,
  PATINT_CODE_C_TEXT  VARCHAR2(16 BYTE)             NULL,
  DEVICE_CODE_A_TEXT  VARCHAR2(16 BYTE)             NULL,
  DEVICE_CODE_B_TEXT  VARCHAR2(16 BYTE)             NULL,
  DEVICE_CODE_C_TEXT  VARCHAR2(16 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_MEDW_TEST
(
  EV_MW_TEST_ROW_ID  NUMBER(10)                     NULL,
  EVENT_ID           NUMBER(10)                     NULL,
  LAB_TEST           VARCHAR2(50 BYTE)              NULL,
  RESULT             VARCHAR2(50 BYTE)              NULL,
  TEST_DATE          VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_OSHA
(
  ACTIVITY_WHEN_INJ   CLOB                          NULL,
  EVENT_ID            NUMBER(10)                    NULL,
  HOW_ACC_OCCURRED    CLOB                          NULL,
  NO_RULES_FLAG       NUMBER(5)                     NULL,
  OBJ_SUBST_THAT_INJ  CLOB                          NULL,
  RECORDABLE_FLAG     NUMBER(5)                     NULL,
  SAFEG_NOTUSED_FLAG  NUMBER(5)                     NULL,
  SAFEGUARD_FLAG      NUMBER(5)                     NULL,
  PRIVACY_CASE_FLAG   NUMBER(10)                    NULL,
  SHARPS_OBJECT       NUMBER(10)                    NULL,
  SHARPS_BRAND_MAKE   NUMBER(10)                    NULL,
  SAFEG_NOTUSED       NUMBER(10)                    NULL,
  SAFEG_PROVIDED      NUMBER(10)                    NULL,
  RULES_NOT_FOLLOWED  NUMBER(10)                    NULL,
  OSHA_ESTAB_EID      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_OSHA_SUPP
(
  EVENT_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EVENT_X_OUTCOME
(
  EVENT_ID      NUMBER(10)                          NULL,
  OUTCOME_CODE  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EV_CONCOM_SUPP
(
  EV_CONCOM_ROW_ID  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EV_FORMS
(
  STATE_ROW_ID       NUMBER(10)                     NULL,
  FORM_CATEGORY      NUMBER(10)                     NULL,
  FORM_ID            NUMBER(10)                     NULL,
  FORM_TITLE         VARCHAR2(128 BYTE)             NULL,
  FORM_NAME          VARCHAR2(64 BYTE)              NULL,
  FILE_NAME          VARCHAR2(64 BYTE)              NULL,
  ACTIVE_FLAG        NUMBER(10)                     NULL,
  HASH_CRC           VARCHAR2(128 BYTE)             NULL,
  PRIMARY_FORM_FLAG  NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EV_FORMS_CAT_LKUP
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  FORM_CAT       NUMBER(10)                         NULL,
  FORM_CAT_DESC  VARCHAR2(64 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EV_MW_TEST_SUPP
(
  EV_MW_TEST_ROW_ID  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EV_X_CONCOM_PROD
(
  EV_CONCOM_ROW_ID   NUMBER(10)                     NULL,
  EVENT_ID           NUMBER(10)                     NULL,
  CONCOM_PRODUCT_ID  NUMBER(10)                     NULL,
  FROM_DATE          VARCHAR2(8 BYTE)               NULL,
  TO_DATE            VARCHAR2(8 BYTE)               NULL,
  CONCOM_PRODUCT     VARCHAR2(40 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EXPERT
(
  EXPERT_TYPE        NUMBER(10)                     NULL,
  EXPERT_ROW_ID      NUMBER(10)                     NULL,
  EXPERT_EID         NUMBER(10)                     NULL,
  LITIGATION_ROW_ID  NUMBER(10)                     NULL,
  SPECIALTY          CLOB                           NULL,
  RATE               NUMBER                         NULL,
  NOTES              CLOB                           NULL,
  CASE_DESCRIPTION   CLOB                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EXPERT_SUPP
(
  EXPERT_ROW_ID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE EXP_ENH_SUPP
(
  EXPOSURE_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FALL_INDICATOR
(
  EVENT_ID            NUMBER(10)                    NULL,
  COND_PRIOR_CODE     NUMBER(10)                    NULL,
  RAIL_POSITION_CODE  NUMBER(10)                    NULL,
  RESTRAINT_CODE      NUMBER(10)                    NULL,
  BED_POSITION_CODE   NUMBER(10)                    NULL,
  CALL_LIGHT_FLAG     NUMBER(5)                     NULL,
  CALL_LIGHT_CODE     NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FALL_IND_SUPP
(
  EVENT_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FEE_TABLES
(
  USER_TABLE_NAME     VARCHAR2(30 BYTE)             NULL,
  TABLE_ID            NUMBER(10)                    NULL,
  TABLE_TYPE          NUMBER(5)                     NULL,
  CALC_TYPE           NUMBER(5)                     NULL,
  ODBC_NAME           VARCHAR2(30 BYTE)             NULL,
  START_DATE          VARCHAR2(8 BYTE)              NULL,
  END_DATE            VARCHAR2(8 BYTE)              NULL,
  CALCULATION_TYPE    NUMBER(5)                     NULL,
  STATE               NUMBER(10)                    NULL,
  PRIORITY            NUMBER(5)                     NULL,
  UCR_PERCENTILES     NUMBER(5)                     NULL,
  DEFAULT_PERCENTILE  NUMBER(5)                     NULL,
  HOSPITAL_PAY_MULT   NUMBER                        NULL,
  EOB_FORM            VARCHAR2(50 BYTE)             NULL,
  EOB_CODE_FILE       VARCHAR2(50 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FISCAL_YEAR
(
  FY_START_DATE     VARCHAR2(8 BYTE)                NULL,
  FY_END_DATE       VARCHAR2(8 BYTE)                NULL,
  QTR1_START_DATE   VARCHAR2(8 BYTE)                NULL,
  QTR1_END_DATE     VARCHAR2(8 BYTE)                NULL,
  QTR2_START_DATE   VARCHAR2(8 BYTE)                NULL,
  QTR2_END_DATE     VARCHAR2(8 BYTE)                NULL,
  QTR3_START_DATE   VARCHAR2(8 BYTE)                NULL,
  QTR3_END_DATE     VARCHAR2(8 BYTE)                NULL,
  QTR4_START_DATE   VARCHAR2(8 BYTE)                NULL,
  QTR4_END_DATE     VARCHAR2(8 BYTE)                NULL,
  NUM_PERIODS       NUMBER(10)                      NULL,
  WEEK_END_DAY      NUMBER(10)                      NULL,
  FISCAL_YEAR       VARCHAR2(4 BYTE)                NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL,
  ORG_EID           NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FISCAL_YEAR_CLIENT
(
  CLIENT         VARCHAR2(50 BYTE)                  NULL,
  FY_START_DATE  VARCHAR2(8 BYTE)                   NULL,
  FY_END_DATE    VARCHAR2(8 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FISCAL_YEAR_PERIOD
(
  PERIOD_NUMBER      NUMBER(10)                     NULL,
  PERIOD_START_DATE  VARCHAR2(8 BYTE)               NULL,
  PERIOD_END_DATE    VARCHAR2(8 BYTE)               NULL,
  FISCAL_YEAR        VARCHAR2(4 BYTE)               NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  ORG_EID            NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FL_CTL_NUMBER
(
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  FUNDS_TRANS_ID     NUMBER(10)                     NULL,
  CLAIM_CTL_NUMBER   VARCHAR2(13 BYTE)              NULL,
  SEQ_NUMBER         NUMBER(10)                     NULL,
  YEAR               VARCHAR2(2 BYTE)               NULL,
  JULIAN_DATE        VARCHAR2(2 BYTE)               NULL,
  BILL_TYPE          NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FORM_PRINT_DATA
(
  ROW_ID        NUMBER(5)                           NULL,
  ID_NUM        NUMBER(5)                           NULL,
  TABLE_NAME    VARCHAR2(20 BYTE)                   NULL,
  FIELD_NAME    VARCHAR2(31 BYTE)                   NULL,
  X_POS         NUMBER                              NULL,
  Y_POS         NUMBER                              NULL,
  FONT          VARCHAR2(20 BYTE)                   NULL,
  SIZE_C        VARCHAR2(4 BYTE)                    NULL,
  STYLE         VARCHAR2(4 BYTE)                    NULL,
  SQL_PROC_NUM  NUMBER(5)                           NULL,
  PRIMARY_ID    NUMBER(5)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FROI_OPTIONS
(
  BASE_LEVEL          NUMBER(10)                    NULL,
  SELECTED_EID        NUMBER(10)                    NULL,
  CARRIER_ORG_LEVEL   NUMBER(10)                    NULL,
  CLMADMIN_ORG_LEVEL  NUMBER(10)                    NULL,
  ATTACH_TO_CLAIM     NUMBER(10)                    NULL,
  DEF_CLAM_ADMIN_EID  NUMBER(10)                    NULL,
  DEF_CARRIER_EID     NUMBER(10)                    NULL,
  EMPLOYER_LEVEL      NUMBER(10)                    NULL,
  FROI_PREPARER       NUMBER(10)                    NULL,
  PRINT_CLAIM_ADMIN   NUMBER(10)                    NULL,
  PRINT_OSHA_DESC     NUMBER(10)                    NULL,
  USE_DEF_CARRIER     NUMBER(10)                    NULL,
  VIRGIN_PARNT_LEVEL  NUMBER(10)                    NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ACORD_COPIES        NUMBER(10)                    NULL,
  PRINT_ACORD_FORMS   NUMBER(10)                    NULL,
  USE_TITLE           NUMBER(10)                    NULL,
  FORCE_PRINT_NCCI    NUMBER(10)                    NULL,
  WORK_LOSS_FIELD     NUMBER(10)                    NULL,
  SUSPRES_DEFLT_TIME  NUMBER(10)                    NULL,
  CONTACT_OPTION      NUMBER(10)                    NULL,
  JURIS_ROW_ID        NUMBER(10)                    NULL,
  NE_PARENT_OPTION    NUMBER(10)                    NULL,
  NE_PARENT_ORG_HIER  NUMBER(10)                    NULL,
  NE_PARENT_DEF_ID    NUMBER(10)                    NULL,
  TN_PARENT_OPTION    NUMBER(10)                    NULL,
  TN_PARENT_ORG_HIER  NUMBER(10)                    NULL,
  TN_PARENT_DEF_ID    NUMBER(10)                    NULL,
  VA_PARENT_OPTION    NUMBER(10)                    NULL,
  VA_PARENT_DEF_ID    NUMBER(10)                    NULL,
  PRINT_CLMNUMB_TOP   NUMBER(10)                    NULL,
  CLAIMADMIN_TPA_EID  NUMBER(10)                    NULL,
  PERSON_NOTFIED_OPT  NUMBER(10)                    NULL,
  CARR_CLAIM_NUM_OPT  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS
(
  AUTO_CHECK_DETAIL  VARCHAR2(30 BYTE)              NULL,
  ROLLUP_ID          NUMBER(10)                     NULL,
  VOID_DATE          VARCHAR2(8 BYTE)               NULL,
  ADDR1              VARCHAR2(50 BYTE)              NULL,
  ADDR2              VARCHAR2(50 BYTE)              NULL,
  AUTO_CHECK_FLAG    NUMBER(5)                      NULL,
  CITY               VARCHAR2(50 BYTE)              NULL,
  CLAIMANT_EID       NUMBER(10)                     NULL,
  COUNTRY_CODE       NUMBER(10)                     NULL,
  FIRST_NAME         VARCHAR2(50 BYTE)              NULL,
  LAST_NAME          VARCHAR2(255 BYTE)             NULL,
  PRECHECK_FLAG      NUMBER(5)                      NULL,
  STATE_ID           NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  ZIP_CODE           VARCHAR2(10 BYTE)              NULL,
  TRANS_ID           NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  CLAIM_NUMBER       VARCHAR2(25 BYTE)              NULL,
  CTL_NUMBER         VARCHAR2(25 BYTE)              NULL,
  VOID_FLAG          NUMBER(5)                      NULL,
  DATE_OF_CHECK      VARCHAR2(8 BYTE)               NULL,
  CHECK_MEMO         VARCHAR2(255 BYTE)             NULL,
  TRANS_NUMBER       NUMBER(10)                     NULL,
  TRANS_DATE         VARCHAR2(8 BYTE)               NULL,
  PAYEE_EID          NUMBER(10)                     NULL,
  PAYEE_TYPE_CODE    NUMBER(10)                     NULL,
  ACCOUNT_ID         NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  STATUS_CODE        NUMBER(10)                     NULL,
  FILED_1099_FLAG    NUMBER(5)                      NULL,
  CLEARED_FLAG       NUMBER(5)                      NULL,
  PAYMENT_FLAG       NUMBER(5)                      NULL,
  COLLECTION_FLAG    NUMBER(5)                      NULL,
  COMMENTS           CLOB                           NULL,
  NOTES              CLOB                           NULL,
  CRC                NUMBER(10)                     NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  BATCH_NUMBER       NUMBER(10)                     NULL,
  SEC_DEPT_EID       NUMBER(10)                     NULL,
  ENCLOSURE_FLAG     NUMBER(5)                      NULL,
  APPROVE_USER       VARCHAR2(8 BYTE)               NULL,
  DTTM_APPROVAL      VARCHAR2(14 BYTE)              NULL,
  SUB_ACCOUNT_ID     NUMBER(10)                     NULL,
  SETTLEMENT_FLAG    NUMBER(5)                      NULL,
  APPROVER_ID        NUMBER(10)                     NULL,
  VOUCHER_FLAG       NUMBER(5)                      NULL,
  WEEKS_PAID_CODE    NUMBER(10)                     NULL,
  NUM_OF_PAID_DAYS   NUMBER(10)                     NULL,
  TAX_PAYMENT_FLAG   NUMBER(5)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_AUTO
(
  AUTO_BATCH_ID      NUMBER(10)                     NULL,
  AUTO_TRANS_ID      NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  CLAIMANT_EID       NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  FIRST_NAME         VARCHAR2(50 BYTE)              NULL,
  LAST_NAME          VARCHAR2(255 BYTE)             NULL,
  ADDR1              VARCHAR2(50 BYTE)              NULL,
  ADDR2              VARCHAR2(50 BYTE)              NULL,
  CITY               VARCHAR2(50 BYTE)              NULL,
  STATE_ID           NUMBER(10)                     NULL,
  ZIP_CODE           VARCHAR2(10 BYTE)              NULL,
  CHECK_MEMO         VARCHAR2(255 BYTE)             NULL,
  PAYEE_EID          NUMBER(10)                     NULL,
  PAYEE_TYPE_CODE    NUMBER(10)                     NULL,
  CTL_NUMBER         VARCHAR2(25 BYTE)              NULL,
  AMOUNT             NUMBER                         NULL,
  ACCOUNT_ID         NUMBER(10)                     NULL,
  PRINT_DATE         VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(16 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(16 BYTE)              NULL,
  THIRD_PARTY_FLAG   NUMBER(10)                     NULL,
  PERCENT_NUMBER     NUMBER                         NULL,
  PRECHECK_FLAG      NUMBER(5)                      NULL,
  CHECK_BATCH_NUM    NUMBER(10)                     NULL,
  CLAIM_NUMBER       VARCHAR2(21 BYTE)              NULL,
  PAY_NUMBER         NUMBER(10)                     NULL,
  END_PAY_NUMBER     NUMBER(10)                     NULL,
  ENCLOSURE_FLAG     NUMBER(5)                      NULL,
  STATUS_CODE        NUMBER(10)                     NULL,
  SUB_ACC_ID         NUMBER(10)                     NULL,
  MIDDLE_NAME        VARCHAR2(50 BYTE)              NULL,
  SETTLEMENT_FLAG    NUMBER(5)                      NULL,
  VOUCHER_FLAG       NUMBER(5)                      NULL,
  NUM_OF_PAID_DAYS   NUMBER(10)                     NULL,
  WEEKS_PAID_CODE    NUMBER(10)                     NULL,
  TAX_PAYMENT_FLAG   NUMBER(5)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_AUTO_BATCH
(
  AUTO_BATCH_ID      NUMBER(10)                     NULL,
  TOTAL_PAYMENTS     NUMBER(10)                     NULL,
  CURRENT_PAYMENT    NUMBER(10)                     NULL,
  PAYMENT_INTERVAL   NUMBER(10)                     NULL,
  START_DATE         VARCHAR2(8 BYTE)               NULL,
  END_DATE           VARCHAR2(8 BYTE)               NULL,
  GROSS_TO_DATE      NUMBER                         NULL,
  PAYMENTS_TO_DATE   NUMBER(10)                     NULL,
  DISABILITY_FLAG    NUMBER(5)                      NULL,
  DAILY_AMOUNT       NUMBER                         NULL,
  FREEZE_BATCH_FLAG  NUMBER(10)                     NULL,
  PAY_PERIOD_START   VARCHAR2(8 BYTE)               NULL,
  BEN_START_DATE     VARCHAR2(8 BYTE)               NULL,
  BEN_END_DATE       VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_AUTO_SPLIT
(
  AUTO_SPLIT_ID      NUMBER(10)                     NULL,
  AUTO_TRANS_ID      NUMBER(10)                     NULL,
  TRANS_TYPE_CODE    NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  GL_ACCOUNT_CODE    NUMBER(10)                     NULL,
  FROM_DATE          VARCHAR2(8 BYTE)               NULL,
  TO_DATE            VARCHAR2(8 BYTE)               NULL,
  INVOICED_BY        VARCHAR2(20 BYTE)              NULL,
  INVOICE_AMOUNT     NUMBER                         NULL,
  INVOICE_NUMBER     VARCHAR2(14 BYTE)              NULL,
  PO_NUMBER          VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(16 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(16 BYTE)              NULL,
  TAG                NUMBER(10)                     NULL,
  INVOICE_DATE       VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_DEPOSIT
(
  DEPOSIT_ID         NUMBER(10)                     NULL,
  CTL_NUMBER         VARCHAR2(25 BYTE)              NULL,
  BANK_ACC_ID        NUMBER(10)                     NULL,
  SUB_ACC_ID         NUMBER(10)                     NULL,
  TRANS_DATE         VARCHAR2(8 BYTE)               NULL,
  AMOUNT             NUMBER                         NULL,
  CLEARED_FLAG       NUMBER(5)                      NULL,
  VOID_FLAG          NUMBER(5)                      NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATE_BY_USER     VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DESCRIPTION        VARCHAR2(40 BYTE)              NULL,
  VOIDCLEAR_DATE     VARCHAR2(8 BYTE)               NULL,
  DEPOSIT_TYPE       NUMBER(5)                      NULL,
  ADJUST_CODE        NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_DET_LIMITS
(
  GROUP_ID           NUMBER(10)                     NULL,
  USER_ID            NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  MAX_AMOUNT         NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_LIMITS
(
  GROUP_ID          NUMBER(10)                      NULL,
  USER_ID           NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL,
  MAX_AMOUNT        NUMBER                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_SUPP
(
  TRANS_ID        NUMBER(10)                        NULL,
  INT_CLAIM_TEXT  VARCHAR2(21 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE FUNDS_TRANS_SPLIT
(
  SUM_AMOUNT         NUMBER                         NULL,
  SPLIT_ROW_ID       NUMBER(10)                     NULL,
  TRANS_ID           NUMBER(10)                     NULL,
  TRANS_TYPE_CODE    NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  GL_ACCOUNT_CODE    NUMBER(10)                     NULL,
  FROM_DATE          VARCHAR2(8 BYTE)               NULL,
  TO_DATE            VARCHAR2(8 BYTE)               NULL,
  INVOICED_BY        VARCHAR2(20 BYTE)              NULL,
  INVOICE_AMOUNT     NUMBER                         NULL,
  INVOICE_NUMBER     VARCHAR2(25 BYTE)              NULL,
  PO_NUMBER          VARCHAR2(14 BYTE)              NULL,
  CRC                NUMBER(10)                     NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  INVOICE_DATE       VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE GLOSSARY
(
  IND_STAND_TABLE_ID  NUMBER(10)                    NULL,
  LINE_OF_BUS_FLAG    NUMBER(5)                     NULL,
  REQD_IND_TABL_FLAG  NUMBER(5)                     NULL,
  TABLE_ID            NUMBER(10)                    NULL,
  SYSTEM_TABLE_NAME   VARCHAR2(18 BYTE)             NULL,
  GLOSSARY_TYPE_CODE  NUMBER(10)                    NULL,
  ATTACHMENTS_FLAG    NUMBER(5)                     NULL,
  RELATED_TABLE_ID    NUMBER(10)                    NULL,
  REQD_REL_TABL_FLAG  NUMBER(5)                     NULL,
  NEXT_UNIQUE_ID      NUMBER(10)                    NULL,
  RM_USER_ID          VARCHAR2(20 BYTE)             NULL,
  DTTM_LAST_UPDATE    VARCHAR2(14 BYTE)             NULL,
  DELETED_FLAG        NUMBER(5)                     NULL,
  TREE_DISPLAY_FLAG   NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE GLOSSARY_TEXT
(
  TABLE_ID       NUMBER(10)                         NULL,
  TABLE_NAME     VARCHAR2(50 BYTE)                  NULL,
  LANGUAGE_CODE  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE GRAPH_USERDEFINED
(
  GRAPH_ID          NUMBER(10)                      NULL,
  GRAPH_NAME        VARCHAR2(40 BYTE)               NULL,
  DATA_ID           NUMBER(5)                       NULL,
  CAGEEDGECOLOR     NUMBER(5)                       NULL,
  CAGEFLIP          NUMBER(5)                       NULL,
  CAGESTYLE         NUMBER(5)                       NULL,
  CAGEWALLCOLOR     NUMBER(5)                       NULL,
  ELEVATION         NUMBER(5)                       NULL,
  PERSPECTIVE       NUMBER(5)                       NULL,
  ROTATION          NUMBER(5)                       NULL,
  SURFACECOLORMAX   NUMBER(5)                       NULL,
  SURFACECOLORMIN   NUMBER(5)                       NULL,
  SURFACEWALLCOLOR  NUMBER(5)                       NULL,
  TRUE3D            NUMBER(5)                       NULL,
  TRUE3DDEPTH       NUMBER(5)                       NULL,
  TRUE3DXGAP        NUMBER(5)                       NULL,
  TRUE3DZGAP        NUMBER(5)                       NULL,
  TICKEVERY         NUMBER(5)                       NULL,
  TICKS             NUMBER(5)                       NULL,
  XAXISMAX          NUMBER(5)                       NULL,
  XAXISMIN          NUMBER(5)                       NULL,
  XAXISMINORTICKS   NUMBER(5)                       NULL,
  XAXISSTYLE        NUMBER(5)                       NULL,
  XAXISTICKS        NUMBER(5)                       NULL,
  YAXISMAX          NUMBER(5)                       NULL,
  YAXISMIN          NUMBER(5)                       NULL,
  YAXISMINORTICKS   NUMBER(5)                       NULL,
  YAXISSTYLE        NUMBER(5)                       NULL,
  YAXISTICKS        NUMBER(5)                       NULL,
  BACKGROUND        NUMBER(5)                       NULL,
  GRAPHSTYLE        NUMBER(5)                       NULL,
  GRAPHTYPE         NUMBER(5)                       NULL,
  GRIDLINESTYLE     NUMBER(5)                       NULL,
  GRIDSTYLE         NUMBER(5)                       NULL,
  BACKDROP          VARCHAR2(50 BYTE)               NULL,
  BACKDROPSTYLE     NUMBER(5)                       NULL,
  BACKGROUNDSTYLE   NUMBER(5)                       NULL,
  GRAPHTITLE        VARCHAR2(80 BYTE)               NULL,
  RIGHTTITLESTYLE   NUMBER(5)                       NULL,
  LABELS            NUMBER(5)                       NULL,
  LABELSTYLE        NUMBER(5)                       NULL,
  LABELEVERY        NUMBER(5)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE GRID_SYS_PARMS
(
  MAX_COLS  NUMBER(10)                              NULL,
  MAX_ROWS  NUMBER(10)                              NULL,
  MIN_COLS  NUMBER(10)                              NULL,
  MIN_ROWS  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE GROUP_PERMISSIONS
(
  GROUP_ID  NUMBER(10)                              NULL,
  FUNC_ID   NUMBER(10)                              NULL,
  CRC       NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE HAZ_MAT
(
  HAZ_MAT_ID      NUMBER(10)                        NULL,
  PROD_NAME_TEXT  VARCHAR2(75 BYTE)                 NULL,
  OTH_NAMES_TXCD  CLOB                              NULL,
  NOTES_TXCD      CLOB                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE HELP_DEF
(
  HELP_ID    NUMBER(10)                             NULL,
  DESC_TEXT  VARCHAR2(80 BYTE)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE HELP_DEF_TEXT
(
  HELP_ID        NUMBER(10)                         NULL,
  LANGUAGE_CODE  NUMBER(10)                         NULL,
  RCLICK_TEXT    CLOB                               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE IAIABC_FUNDS_MAPPI
(
  IA_ROW_ID          NUMBER(10)                     NULL,
  DN_NUMBER          VARCHAR2(3 BYTE)               NULL,
  TRANS_CODE_ID      NUMBER(10)                     NULL,
  DN_CODE_ID         NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INDICATOR
(
  INDICATOR_CODE  VARCHAR2(4 BYTE)                  NULL,
  INDICATOR_DESC  VARCHAR2(50 BYTE)                 NULL,
  MINUTES         NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INVDETAIL_X_DIAG
(
  INVOICE_DETAIL_ID  NUMBER(10)                     NULL,
  DIAGNOSIS_CODE     NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INVDETAIL_X_EOB
(
  INVOICE_DETAIL_ID  NUMBER(10)                     NULL,
  EOB_CODE           NUMBER(10)                     NULL,
  EOB_TABLE          NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INVDETAIL_X_MOD
(
  INVOICE_DETAIL_ID  NUMBER(10)                     NULL,
  MODIFIER_CODE      NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INVOICE
(
  INVOICE_ID         NUMBER(10)                     NULL,
  FUNDS_TRANS_ID     NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  CLAIM_NUMBER_TEXT  VARCHAR2(50 BYTE)              NULL,
  CLAIMANT_EID       NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  PAYEE_TYPE_CODE    NUMBER(10)                     NULL,
  PAYEE_EID          NUMBER(10)                     NULL,
  PAYEE_SPEC_CODE    NUMBER(10)                     NULL,
  LAST_NAME_TEXT     VARCHAR2(50 BYTE)              NULL,
  FIRST_NAME_TEXT    VARCHAR2(50 BYTE)              NULL,
  ADDR1_TEXT         VARCHAR2(50 BYTE)              NULL,
  ADDR2_TEXT         VARCHAR2(50 BYTE)              NULL,
  CITY_TEXT          VARCHAR2(50 BYTE)              NULL,
  STATE_ID           NUMBER(10)                     NULL,
  COUNTRY_CODE       NUMBER(10)                     NULL,
  POSTAL_CODE_TEXT   VARCHAR2(15 BYTE)              NULL,
  TRANS_DATE_TEXT    VARCHAR2(8 BYTE)               NULL,
  TOT_SCHEDULED_AMT  NUMBER                         NULL,
  TOT_REDUCTION_AMT  NUMBER                         NULL,
  TOT_AMT_TO_PAY     NUMBER                         NULL,
  TOT_AMT_SAVED      NUMBER                         NULL,
  TOT_AMT_BILLED     NUMBER                         NULL,
  NUM_DETAIL_LINES   NUMBER(5)                      NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_EOB_PRINTED   VARCHAR2(14 BYTE)              NULL,
  EOB_PRINTED_USER   VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE INVOICE_DETAIL
(
  INVOICE_DETAIL_ID   NUMBER(10)                    NULL,
  INVOICE_ID          NUMBER(10)                    NULL,
  FUNDS_SPLIT_ROW_ID  NUMBER(10)                    NULL,
  TABLE_CODE          NUMBER(10)                    NULL,
  PERCENTILE          VARCHAR2(4 BYTE)              NULL,
  PLACE_OF_SER_CODE   NUMBER(10)                    NULL,
  TYPE_OF_SER_CODE    NUMBER(10)                    NULL,
  BILLING_CODE_TEXT   VARCHAR2(64 BYTE)             NULL,
  UNITS_BILLED_NUM    NUMBER                        NULL,
  UNITS_BILLED_TYPE   NUMBER(10)                    NULL,
  AMOUNT_BILLED       NUMBER                        NULL,
  SCHEDULED_AMOUNT    NUMBER                        NULL,
  AMOUNT_REDUCED      NUMBER                        NULL,
  AMOUNT_TO_PAY       NUMBER                        NULL,
  AMOUNT_SAVED        NUMBER                        NULL,
  TOOTH_NUMBER        NUMBER(5)                     NULL,
  SURFACE_TEXT        VARCHAR2(50 BYTE)             NULL,
  MODIFIER_CODE       NUMBER(10)                    NULL,
  QUANTITY            VARCHAR2(6 BYTE)              NULL,
  STORE               VARCHAR2(11 BYTE)             NULL,
  CONTRACT_EXISTS     NUMBER(10)                    NULL,
  OVERRIDE_TYPE       NUMBER(10)                    NULL,
  CONTRACT_AMOUNT     NUMBER                        NULL,
  DISCOUNT            NUMBER                        NULL,
  AMOUNT_ALLOWED      NUMBER                        NULL,
  BASE_AMOUNT         NUMBER                        NULL,
  ZIP_CODE            VARCHAR2(10 BYTE)             NULL,
  FEE_TABLE_AMT       NUMBER                        NULL,
  PER_DIEM_AMT        NUMBER                        NULL,
  STOP_LOSS_AMT       NUMBER                        NULL,
  STOP_LOSS_FLAG      NUMBER(10)                    NULL,
  FEE_DATA_SOURCE     NUMBER(10)                    NULL,
  FEE_TABLE_STATE     VARCHAR2(2 BYTE)              NULL,
  FEE_TABLE_STUDY     VARCHAR2(5 BYTE)              NULL,
  FEE_DATA_REVISION   VARCHAR2(3 BYTE)              NULL,
  PRESCRIP_NO         VARCHAR2(30 BYTE)             NULL,
  DRUG_NAME           VARCHAR2(80 BYTE)             NULL,
  PRESCRIP_DATE       VARCHAR2(8 BYTE)              NULL,
  DAYS_SUPPLIED       NUMBER(10)                    NULL,
  DATE_FILLED         VARCHAR2(8 BYTE)              NULL,
  REV_CODE            VARCHAR2(4 BYTE)              NULL,
  PHY_EID             NUMBER(10)                    NULL,
  MED_SUPPLY_FLAG     NUMBER(10)                    NULL,
  DIAG_REF_NO         VARCHAR2(4 BYTE)              NULL,
  TABLE_CODE_2        NUMBER(10)                    NULL,
  BRS_FEE_AMT1        NUMBER                        NULL,
  BRS_FEE_AMT2        NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE JURIS_FORMS
(
  FORM_ID            NUMBER(10)                     NULL,
  STATE_ROW_ID       NUMBER(10)                     NULL,
  FORM_NAME          VARCHAR2(50 BYTE)              NULL,
  PDF_FILE_NAME      VARCHAR2(100 BYTE)             NULL,
  ACTIVE_FLAG        NUMBER(10)                     NULL,
  HASH_CRC           VARCHAR2(128 BYTE)             NULL,
  PRIMARY_FORM_FLAG  NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE JURIS_FORMS_HIST
(
  FORM_ID       NUMBER(10)                          NULL,
  DATE_PRINTED  VARCHAR2(8 BYTE)                    NULL,
  TIME_PRINTED  VARCHAR2(6 BYTE)                    NULL,
  CLAIM_ID      NUMBER(10)                          NULL,
  USER_ID       VARCHAR2(20 BYTE)                   NULL,
  HOW_PRINTED   VARCHAR2(5 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE JURIS_OPTIONS
(
  BASE_LEVEL          NUMBER(10)                    NULL,
  JURIS_ROW_ID        NUMBER(10)                    NULL,
  SELECTED_EID        NUMBER(10)                    NULL,
  CARRIER_ORG_LEVEL   NUMBER(10)                    NULL,
  CLMADMIN_ORG_LEVEL  NUMBER(10)                    NULL,
  ATTACH_TO_CLAIM     NUMBER(10)                    NULL,
  DEF_CLAM_ADMIN_EID  NUMBER(10)                    NULL,
  DEF_CARRIER_EID     NUMBER(10)                    NULL,
  EMPLOYER_LEVEL      NUMBER(10)                    NULL,
  FROI_PREPARER       NUMBER(10)                    NULL,
  PRINT_CLAIM_ADMIN   NUMBER(10)                    NULL,
  PRINT_OSHA_DESC     NUMBER(10)                    NULL,
  USE_DEF_CARRIER     NUMBER(10)                    NULL,
  VIRGIN_PARNT_LEVEL  NUMBER(10)                    NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  ACORD_COPIES        NUMBER(10)                    NULL,
  PRINT_ACORD_FORMS   NUMBER(10)                    NULL,
  CLAIMADMIN_TPA_EID  NUMBER(10)                    NULL,
  TPA_EID             NUMBER(10)                    NULL,
  CARR_CLAIM_NUM_OPT  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE LINKS
(
  LINK_ID  NUMBER(10)                               NULL,
  NAME     VARCHAR2(50 BYTE)                        NULL,
  URL      VARCHAR2(255 BYTE)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE LITIGATION_SUPP
(
  LITIGATION_ROW_ID  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE LOB_SUP_APPROVAL
(
  LOB_CODE     NUMBER(10)                           NULL,
  USER_ID      NUMBER(10)                           NULL,
  PAYMENT_MAX  NUMBER(10)                           NULL,
  RESERVE_MAX  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE LOCATION_POLICY
(
  ROW_ID              NUMBER(10)                    NULL,
  POLICY_ID           VARCHAR2(30 BYTE)             NULL,
  CLAIM_ID            NUMBER(10)                    NULL,
  POLICY_RECORD_ID    NUMBER(10)                    NULL,
  LOCATION_RECORD_ID  NUMBER(10)                    NULL,
  LOCATION_NUMBER     VARCHAR2(3 BYTE)              NULL,
  LOB_CODE            NUMBER(10)                    NULL,
  ADDRESS1            VARCHAR2(64 BYTE)             NULL,
  ADDRESS2            VARCHAR2(64 BYTE)             NULL,
  CITY                VARCHAR2(32 BYTE)             NULL,
  COUNTY              VARCHAR2(32 BYTE)             NULL,
  STATE               VARCHAR2(2 BYTE)              NULL,
  ZIP                 VARCHAR2(10 BYTE)             NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_UPD        VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE LOCATION_SELECTED
(
  ROW_ID              NUMBER(10)                    NULL,
  LOCATION_POLICY_ID  NUMBER(10)                    NULL,
  CLAIM_ID            NUMBER(10)                    NULL,
  POLICY_ID           VARCHAR2(30 BYTE)             NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  DTTM_RCD_UPD        VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MAIN_SCHEDULE
(
  PRIMARY_ID  NUMBER(10)                            NULL,
  GROUP_NAME  VARCHAR2(255 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MED_STAFF
(
  STAFF_EID          NUMBER(10)                     NULL,
  MED_STAFF_NUMBER   VARCHAR2(50 BYTE)              NULL,
  PRIMARY_POLICY_ID  NUMBER(10)                     NULL,
  HOME_ADDR1         VARCHAR2(50 BYTE)              NULL,
  HOME_ADDR2         VARCHAR2(50 BYTE)              NULL,
  HOME_CITY          VARCHAR2(50 BYTE)              NULL,
  HOME_STATE_ID      NUMBER(10)                     NULL,
  HOME_ZIP_CODE      VARCHAR2(10 BYTE)              NULL,
  MARITAL_STAT_CODE  NUMBER(10)                     NULL,
  BEEPER_NUMBER      VARCHAR2(30 BYTE)              NULL,
  CELLULAR_NUMBER    VARCHAR2(30 BYTE)              NULL,
  EMERGENCY_CONTACT  VARCHAR2(30 BYTE)              NULL,
  STAFF_STATUS_CODE  NUMBER(10)                     NULL,
  STAFF_POS_CODE     NUMBER(10)                     NULL,
  STAFF_CAT_CODE     NUMBER(10)                     NULL,
  DEPT_ASSIGNED_EID  NUMBER(10)                     NULL,
  HIRE_DATE          VARCHAR2(8 BYTE)               NULL,
  LIC_NUM            VARCHAR2(20 BYTE)              NULL,
  LIC_STATE          NUMBER(10)                     NULL,
  LIC_ISSUE_DATE     VARCHAR2(8 BYTE)               NULL,
  LIC_EXPIRY_DATE    VARCHAR2(8 BYTE)               NULL,
  LIC_DEA_NUM        VARCHAR2(20 BYTE)              NULL,
  LIC_DEA_EXP_DATE   VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MED_STAFF_SUPP
(
  STAFF_EID  NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MERGE_CAT
(
  CAT_ID    NUMBER(10)                              NULL,
  CAT_NAME  VARCHAR2(50 BYTE)                       NULL,
  CAT_DESC  VARCHAR2(250 BYTE)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MERGE_DICTIONARY
(
  FIELD_ID     NUMBER(10)                           NULL,
  CAT_ID       NUMBER(10)                           NULL,
  FIELD_NAME   VARCHAR2(25 BYTE)                    NULL,
  FIELD_DESC   VARCHAR2(250 BYTE)                   NULL,
  FIELD_TABLE  VARCHAR2(25 BYTE)                    NULL,
  FIELD_TYPE   NUMBER(10)                           NULL,
  OPT_MASK     VARCHAR2(50 BYTE)                    NULL,
  DISPLAY_CAT  VARCHAR2(100 BYTE)                   NULL,
  CODE_TABLE   VARCHAR2(50 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MERGE_FORM
(
  FORM_ID         NUMBER(10)                        NULL,
  CAT_ID          NUMBER(10)                        NULL,
  FORM_NAME       VARCHAR2(25 BYTE)                 NULL,
  FORM_DESC       VARCHAR2(250 BYTE)                NULL,
  TABLE_QUALIFY   VARCHAR2(100 BYTE)                NULL,
  FORM_FILE_NAME  VARCHAR2(250 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MERGE_FORM_DEF
(
  FORM_ID          NUMBER(10)                       NULL,
  FIELD_ID         NUMBER(10)                       NULL,
  ITEM_TYPE        NUMBER(10)                       NULL,
  SEQ_NUM          NUMBER(10)                       NULL,
  SUPP_FIELD_FLAG  NUMBER(10)                       NULL,
  MERGE_PARAM      VARCHAR2(100 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MERGE_FORM_PERM
(
  FORM_ID   NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL,
  GROUP_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE MODIFIER_VALUES
(
  FEETABLE_ID     NUMBER(10)                        NULL,
  MODIFIER_CODE   NUMBER(10)                        NULL,
  MODIFIER_VALUE  NUMBER                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE NET_LAYOUTS_MEMBERS
(
  LAYOUT_ID  NUMBER(10)                             NULL,
  MEMBER_ID  NUMBER(10)                             NULL,
  ISGROUP    NUMBER(5)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE NET_VIEWS
(
  VIEW_ID    NUMBER(10)                             NULL,
  VIEW_NAME  VARCHAR2(50 BYTE)                      NULL,
  VIEW_DESC  VARCHAR2(200 BYTE)                     NULL,
  HOME_PAGE  VARCHAR2(240 BYTE)                     NULL,
  PAGE_MENU  VARCHAR2(50 BYTE)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE NET_VIEWS_MEMBERS
(
  VIEW_ID    NUMBER(10)                             NULL,
  MEMBER_ID  NUMBER(10)                             NULL,
  ISGROUP    NUMBER(5)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE NET_VIEW_FORMS
(
  VIEW_ID    NUMBER(10)                             NULL,
  FORM_NAME  VARCHAR2(50 BYTE)                      NULL,
  TOPLEVEL   NUMBER(5)                              NULL,
  CAPTION    VARCHAR2(100 BYTE)                     NULL,
  VIEW_XML   CLOB                                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE OBJSTORAGE
(
  ITEMNAME   VARCHAR2(64 BYTE)                      NULL,
  ITEMVALUE  VARCHAR2(255 BYTE)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ORG_HIERARCHY
(
  DEPARTMENT_EID  NUMBER(10)                        NULL,
  FACILITY_EID    NUMBER(10)                        NULL,
  LOCATION_EID    NUMBER(10)                        NULL,
  DIVISION_EID    NUMBER(10)                        NULL,
  REGION_EID      NUMBER(10)                        NULL,
  OPERATION_EID   NUMBER(10)                        NULL,
  COMPANY_EID     NUMBER(10)                        NULL,
  CLIENT_EID      NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ORG_SECURITY
(
  GROUP_ID        NUMBER(10)                        NULL,
  GROUP_NAME      VARCHAR2(255 BYTE)                NULL,
  GROUP_ENTITIES  VARCHAR2(255 BYTE)                NULL,
  GROUP_DEPT      NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE ORG_SECURITY_USER
(
  GROUP_ID  NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE OSHA_OPTIONS
(
  ROW_ID              NUMBER(10)                    NULL,
  OSHA300A_SIGNATURE  CLOB                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PARMS_NAME_VALUE
(
  PARM_CATEGORY   NUMBER(10)                        NULL,
  PARM_NAME       VARCHAR2(20 BYTE)                 NULL,
  STR_PARM_VALUE  CLOB                              NULL,
  PARM_DESC       CLOB                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT
(
  PATIENT_ID          NUMBER(10)                    NULL,
  PATIENT_EID         NUMBER(10)                    NULL,
  PATIENT_ACCT_NO     VARCHAR2(20 BYTE)             NULL,
  MEDICAL_RCD_NO      VARCHAR2(20 BYTE)             NULL,
  DATE_OF_ADMISSION   VARCHAR2(8 BYTE)              NULL,
  TIME_OF_ADMISSION   VARCHAR2(6 BYTE)              NULL,
  DATE_OF_DISCHARGE   VARCHAR2(8 BYTE)              NULL,
  TIME_OF_DISCHARGE   VARCHAR2(6 BYTE)              NULL,
  ADMISSION_REASON    CLOB                          NULL,
  ADM_TYPE_CODE       NUMBER(10)                    NULL,
  ADM_SOURCE_CODE     NUMBER(10)                    NULL,
  ACUITY_LEVEL_CODE   NUMBER(10)                    NULL,
  CARDIAC_ARREST      NUMBER(5)                     NULL,
  PATIENT_STAT_CODE   NUMBER(10)                    NULL,
  PATIENT_COND_CODE   NUMBER(10)                    NULL,
  DISCHARGE_DSP_CODE  NUMBER(10)                    NULL,
  EXP_LENGTH_OF_STAY  NUMBER(10)                    NULL,
  EXPECTED_COST       NUMBER                        NULL,
  INS_PLAN_GROUP_NO   VARCHAR2(16 BYTE)             NULL,
  QI_SIGNIF_CODE      NUMBER(10)                    NULL,
  EPISODE_OF_CARE_ID  VARCHAR2(8 BYTE)              NULL,
  PATIENT_TYPE_CODE   NUMBER(10)                    NULL,
  HEIGHT              NUMBER                        NULL,
  WEIGHT              NUMBER                        NULL,
  MARITAL_STAT_CODE   NUMBER(10)                    NULL,
  RACE_ENTH_CODE      NUMBER(10)                    NULL,
  DATE_OF_DEATH       VARCHAR2(8 BYTE)              NULL,
  PRIMARY_PAY_CODE    NUMBER(10)                    NULL,
  SECOND_PAY_CODE     NUMBER(10)                    NULL,
  EMERGENCY_CONTACT   VARCHAR2(20 BYTE)             NULL,
  EMERGENCY_CONT_NO   VARCHAR2(20 BYTE)             NULL,
  PATIENT_ROOM_NO     VARCHAR2(14 BYTE)             NULL,
  FACILITY_UNIT_CODE  NUMBER(10)                    NULL,
  FACILITY_DEPT_EID   NUMBER(10)                    NULL,
  HCO_ID_NO           VARCHAR2(20 BYTE)             NULL,
  HCO_SITE_ID_NO      VARCHAR2(20 BYTE)             NULL,
  PRI_PHYSICIAN_EID   NUMBER(10)                    NULL,
  ADM_PHYSICIAN_EID   NUMBER(10)                    NULL,
  NB_APGAR_SCORE      NUMBER(10)                    NULL,
  NB_BIRTH_WEIGHT     NUMBER                        NULL,
  NB_LEN_OF_GEST      NUMBER(10)                    NULL,
  NB_NICU_STAY        NUMBER(10)                    NULL,
  NB_NICU_ADM_DATE    VARCHAR2(8 BYTE)              NULL,
  NB_NICU_DISCH_DATE  VARCHAR2(8 BYTE)              NULL,
  NB_CEPHALIC_MOLD    NUMBER(10)                    NULL,
  NB_EPS_OF_CARE_ID   VARCHAR2(8 BYTE)              NULL,
  NBM_PARITY          NUMBER(10)                    NULL,
  NBM_PRIOR_C_SECT    NUMBER(10)                    NULL,
  NBM_MED_IND_LABOR   NUMBER(10)                    NULL,
  NBM_LIVE_BIRTHS     NUMBER(10)                    NULL,
  NBM_STILL_BIRTHS    NUMBER(10)                    NULL,
  NBM_GRAVIDA         NUMBER(10)                    NULL,
  NBM_EPS_OF_CARE_ID  VARCHAR2(8 BYTE)              NULL,
  DISABILITY_CODE     NUMBER(10)                    NULL,
  ILLNESS_CODE        NUMBER(10)                    NULL,
  LOST_CONSC_FLAG     NUMBER(10)                    NULL,
  WHY_AT_FACILITY     VARCHAR2(255 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_ACT_TAKEN
(
  PATIENT_ID   NUMBER(10)                           NULL,
  ACTION_CODE  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_ATTN_PHYS
(
  PATIENT_ID     NUMBER(10)                         NULL,
  PHYSICIAN_EID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_BODY_PART
(
  PATIENT_ID      NUMBER(10)                        NULL,
  BODY_PART_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_DIAGNOSIS
(
  PATIENT_ID      NUMBER(10)                        NULL,
  DIAGNOSIS_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_DRG_CODES
(
  PATIENT_ID  NUMBER(10)                            NULL,
  DRG_CODE    NUMBER(10)                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_INJURY
(
  PATIENT_ID   NUMBER(10)                           NULL,
  INJURY_CODE  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_PROCEDURE
(
  PROC_ROW_ID         NUMBER(10)                    NULL,
  PATIENT_ID          NUMBER(10)                    NULL,
  PROCEDURE_CODE      NUMBER(10)                    NULL,
  PROC_TYPE_CODE      NUMBER(10)                    NULL,
  DATE_OF_PROCEDURE   VARCHAR2(8 BYTE)              NULL,
  SURGEON_EID         NUMBER(10)                    NULL,
  ANESTH_ADMIN_FLAG   NUMBER(5)                     NULL,
  ANESTH_TYPE_CODE    NUMBER(10)                    NULL,
  ANESTH_EID          NUMBER(10)                    NULL,
  ASA_PS_CLASS_CODE   NUMBER(10)                    NULL,
  COMPLICATION_DATE   VARCHAR2(8 BYTE)              NULL,
  PRIOR_TO_ANES_FLAG  NUMBER(5)                     NULL,
  ICC_LEVEL_CODE      NUMBER(10)                    NULL,
  LEN_OF_TIME         NUMBER                        NULL,
  COMPLICATIONS       CLOB                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_PROC_SUPP
(
  PROC_ROW_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_SUPP
(
  PATIENT_ID  NUMBER(10)                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PATIENT_TREATMENT
(
  PATIENT_ID      NUMBER(10)                        NULL,
  TREATMENT_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PAY_NOTIFY_USERS
(
  RECORD_ID         NUMBER(10)                      NULL,
  USER_ID           NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL,
  NOTIFY_TYPE       NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PERSON_INVOLVED
(
  COUNTY_OF_RESIDNC   VARCHAR2(50 BYTE)             NULL,
  DRIVLIC_STATE       NUMBER(10)                    NULL,
  EST_LEN_DISABILITY  NUMBER(10)                    NULL,
  HIRED_IN_STE_FLAG   NUMBER(5)                     NULL,
  INSURABLE_FLAG      NUMBER(5)                     NULL,
  LAST_VERIFIED_DATE  VARCHAR2(8 BYTE)              NULL,
  NUM_OF_VIOLATIONS   NUMBER(10)                    NULL,
  OSHA_ACC_DESC       VARCHAR2(120 BYTE)            NULL,
  OSHA_REC_FLAG       NUMBER(10)                    NULL,
  REGULAR_JOB_FLAG    NUMBER(5)                     NULL,
  TERM_DATE           VARCHAR2(8 BYTE)              NULL,
  WORK_PERMIT_DATE    VARCHAR2(8 BYTE)              NULL,
  WORK_PERMIT_NUMBER  VARCHAR2(50 BYTE)             NULL,
  WORKDAY_START_TIME  VARCHAR2(6 BYTE)              NULL,
  PI_ROW_ID           NUMBER(10)                    NULL,
  EVENT_ID            NUMBER(10)                    NULL,
  PI_EID              NUMBER(10)                    NULL,
  PI_TYPE_CODE        NUMBER(10)                    NULL,
  INJURY_ILLNESS_FLG  NUMBER(10)                    NULL,
  DATE_OF_DEATH       VARCHAR2(8 BYTE)              NULL,
  REHAB_TEXT          CLOB                          NULL,
  DESC_BY_WITNESS     CLOB                          NULL,
  PATIENT_ACCT_NO     VARCHAR2(20 BYTE)             NULL,
  MEDICAL_RCD_NO      VARCHAR2(12 BYTE)             NULL,
  INS_PLAN_GROUP_NO   VARCHAR2(16 BYTE)             NULL,
  DATE_OF_ADMISSION   VARCHAR2(8 BYTE)              NULL,
  DATE_OF_DISCHARGE   VARCHAR2(8 BYTE)              NULL,
  ADM_TYPE_CODE       NUMBER(10)                    NULL,
  ADM_SOURCE_CODE     NUMBER(10)                    NULL,
  ADMISSION_REASON    CLOB                          NULL,
  ACUITY_LEVEL_CODE   NUMBER(10)                    NULL,
  PATIENT_STAT_CODE   NUMBER(10)                    NULL,
  PATIENT_COND_CODE   NUMBER(10)                    NULL,
  DISCHARGE_DSP_CODE  NUMBER(10)                    NULL,
  EXP_LENGTH_OF_STAY  NUMBER(10)                    NULL,
  EXPECTED_COST       NUMBER                        NULL,
  QI_SIGNIF_CODE      NUMBER(10)                    NULL,
  EPISODE_OF_CARE_ID  VARCHAR2(8 BYTE)              NULL,
  PATIENT_TYPE_CODE   NUMBER(10)                    NULL,
  MARITAL_STAT_CODE   NUMBER(10)                    NULL,
  RACE_ENTH_CODE      NUMBER(10)                    NULL,
  WEIGHT              NUMBER(10)                    NULL,
  PRIMARY_PAY_CODE    NUMBER(10)                    NULL,
  EMERGENCY_CONTACT   VARCHAR2(20 BYTE)             NULL,
  EMERGENCY_CONT_NO   VARCHAR2(20 BYTE)             NULL,
  PATIENT_ROOM_NO     VARCHAR2(14 BYTE)             NULL,
  FACILITY_UNIT_CODE  NUMBER(10)                    NULL,
  FACILITY_DEPT_EID   NUMBER(10)                    NULL,
  HCO_ID_NO           VARCHAR2(20 BYTE)             NULL,
  HCO_SITE_ID_NO      VARCHAR2(20 BYTE)             NULL,
  WHY_AT_FACILITY     CLOB                          NULL,
  CLAIM_AMOUNT        NUMBER                        NULL,
  LOST_WORK_FLAG      NUMBER(5)                     NULL,
  LEN_OF_SVC_DAYS     NUMBER(10)                    NULL,
  POSITION_CODE       NUMBER(10)                    NULL,
  DEPT_ASSIGNED_EID   NUMBER(10)                    NULL,
  SUPERVISOR_EID      NUMBER(10)                    NULL,
  EXEMPT_STATUS_FLAG  NUMBER(5)                     NULL,
  NO_OF_EXEMPTIONS    NUMBER(10)                    NULL,
  FULL_TIME_FLAG      NUMBER(5)                     NULL,
  HOURLY_RATE         NUMBER                        NULL,
  WEEKLY_HOURS        NUMBER(10)                    NULL,
  WEEKLY_RATE         NUMBER                        NULL,
  WORK_SUN_FLAG       NUMBER(5)                     NULL,
  WORK_MON_FLAG       NUMBER(5)                     NULL,
  WORK_TUE_FLAG       NUMBER(5)                     NULL,
  WORK_WED_FLAG       NUMBER(5)                     NULL,
  WORK_THU_FLAG       NUMBER(5)                     NULL,
  WORK_FRI_FLAG       NUMBER(5)                     NULL,
  WORK_SAT_FLAG       NUMBER(5)                     NULL,
  DRIVERS_LIC_NO      VARCHAR2(20 BYTE)             NULL,
  DRIVERSLICTYPECODE  NUMBER(10)                    NULL,
  DATE_DRIVERSLICEXP  VARCHAR2(8 BYTE)              NULL,
  DRIVLIC_RSTRCTCODE  NUMBER(10)                    NULL,
  NCCI_CLASS_CODE     NUMBER(10)                    NULL,
  COMMENTS            CLOB                          NULL,
  INJURY_CAT_CODE     NUMBER(10)                    NULL,
  PAY_AMOUNT          NUMBER                        NULL,
  ACTIVE_FLAG         NUMBER(5)                     NULL,
  EMPLOYEE_NUMBER     VARCHAR2(20 BYTE)             NULL,
  DATE_HIRED          VARCHAR2(8 BYTE)              NULL,
  DISABILITY_CODE     NUMBER(10)                    NULL,
  ILLNESS_CODE        NUMBER(10)                    NULL,
  PAY_TYPE_CODE       NUMBER(10)                    NULL,
  LOST_CONSC_FLAG     NUMBER(5)                     NULL,
  SEC_DEPT_EID        NUMBER(10)                    NULL,
  PATIENT_ID          NUMBER(10)                    NULL,
  EST_RTW_DATE        VARCHAR2(8 BYTE)              NULL,
  OSHA_ESTAB_EID      NUMBER(10)                    NULL,
  MAJOR_HAND_CODE     NUMBER(10)                    NULL,
  HRANGE_START_DATE   VARCHAR2(8 BYTE)              NULL,
  HRANGE_END_DATE     VARCHAR2(8 BYTE)              NULL,
  JOB_CLASS_CODE      NUMBER(10)                    NULL,
  OTHER_TREATMENTS    CLOB                          NULL,
  OTHER_MEDCOND       CLOB                          NULL,
  STD_DISABIL_TYPE    NUMBER(10)                    NULL,
  CUSTOM_FED_TAX_PER  NUMBER                        NULL,
  CUSTOM_SS_TAX_PER   NUMBER                        NULL,
  CUSTOM_MED_TAX_PER  NUMBER                        NULL,
  CUSTOM_ST_TAX_PER   NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHARMACY_MOD
(
  PHMOD_ID    NUMBER(10)                            NULL,
  STATE       NUMBER(10)                            NULL,
  START_DATE  VARCHAR2(8 BYTE)                      NULL,
  STOP_DATE   VARCHAR2(8 BYTE)                      NULL,
  AWP_MULT    NUMBER                                NULL,
  DISP_FEE    NUMBER                                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYSICIAN
(
  PHYS_EID           NUMBER(10)                     NULL,
  PHYSICIAN_NUMBER   VARCHAR2(50 BYTE)              NULL,
  MED_STAFF_NUMBER   VARCHAR2(50 BYTE)              NULL,
  MEDICARE_NUMBER    VARCHAR2(20 BYTE)              NULL,
  PRIMARY_SPECIALTY  NUMBER(10)                     NULL,
  PRIMARY_POLICY_ID  NUMBER(10)                     NULL,
  HOME_ADDR1         VARCHAR2(50 BYTE)              NULL,
  HOME_ADDR2         VARCHAR2(50 BYTE)              NULL,
  HOME_CITY          VARCHAR2(50 BYTE)              NULL,
  HOME_STATE_ID      NUMBER(10)                     NULL,
  HOME_ZIP_CODE      VARCHAR2(10 BYTE)              NULL,
  MARITAL_STAT_CODE  NUMBER(10)                     NULL,
  BEEPER_NUMBER      VARCHAR2(30 BYTE)              NULL,
  CELLULAR_NUMBER    VARCHAR2(30 BYTE)              NULL,
  MAILING_ADDR1      VARCHAR2(50 BYTE)              NULL,
  MAILING_ADDR2      VARCHAR2(50 BYTE)              NULL,
  MAILING_CITY       VARCHAR2(50 BYTE)              NULL,
  MAILING_STATE_ID   NUMBER(10)                     NULL,
  MAILING_ZIP_CODE   VARCHAR2(10 BYTE)              NULL,
  EMERGENCY_CONTACT  VARCHAR2(30 BYTE)              NULL,
  STAFF_STATUS_CODE  NUMBER(10)                     NULL,
  STAFF_TYPE_CODE    NUMBER(10)                     NULL,
  STAFF_CAT_CODE     NUMBER(10)                     NULL,
  INTERNAL_NUMBER    VARCHAR2(20 BYTE)              NULL,
  DEPT_ASSIGNED_EID  NUMBER(10)                     NULL,
  APPOINT_DATE       VARCHAR2(8 BYTE)               NULL,
  REAPPOINT_DATE     VARCHAR2(8 BYTE)               NULL,
  LIC_STATE          NUMBER(10)                     NULL,
  LIC_NUM            VARCHAR2(20 BYTE)              NULL,
  LIC_ISSUE_DATE     VARCHAR2(8 BYTE)               NULL,
  LIC_EXPIRY_DATE    VARCHAR2(8 BYTE)               NULL,
  LIC_DEA_NUM        VARCHAR2(20 BYTE)              NULL,
  LIC_DEA_EXP_DATE   VARCHAR2(8 BYTE)               NULL,
  MEMBERSHIP         VARCHAR2(20 BYTE)              NULL,
  CONT_EDUCATION     VARCHAR2(20 BYTE)              NULL,
  TEACHING_EXP       VARCHAR2(20 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_CERTS
(
  CERT_ID      NUMBER(10)                           NULL,
  PHYS_EID     NUMBER(10)                           NULL,
  NAME_CODE    NUMBER(10)                           NULL,
  STATUS_CODE  NUMBER(10)                           NULL,
  BOARD_CODE   NUMBER(10)                           NULL,
  INT_DATE     VARCHAR2(8 BYTE)                     NULL,
  END_DATE     VARCHAR2(8 BYTE)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_EDUCATION
(
  EDUC_ID          NUMBER(10)                       NULL,
  PHYS_EID         NUMBER(10)                       NULL,
  EDUC_TYPE_CODE   NUMBER(10)                       NULL,
  INSTITUTION_EID  NUMBER(10)                       NULL,
  DEGREE_TYPE      NUMBER(10)                       NULL,
  DEGREE_DATE      VARCHAR2(8 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_PREV_HOSP
(
  PREV_HOSP_ID  NUMBER(10)                          NULL,
  PHYS_EID      NUMBER(10)                          NULL,
  STATUS_CODE   NUMBER(10)                          NULL,
  HOSPITAL_EID  NUMBER(10)                          NULL,
  PRIV_CODE     NUMBER(10)                          NULL,
  INT_DATE      VARCHAR2(8 BYTE)                    NULL,
  END_DATE      VARCHAR2(8 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_PRIVS
(
  PRIV_ID        NUMBER(10)                         NULL,
  PHYS_EID       NUMBER(10)                         NULL,
  CATEGORY_CODE  NUMBER(10)                         NULL,
  TYPE_CODE      NUMBER(10)                         NULL,
  STATUS_CODE    NUMBER(10)                         NULL,
  INT_DATE       VARCHAR2(8 BYTE)                   NULL,
  END_DATE       VARCHAR2(8 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_SUB_SPECIALTY
(
  PHYS_EID        NUMBER(10)                        NULL,
  SPECIALTY_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PHYS_SUPP
(
  PHYS_EID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_SUPP
(
  PI_ROW_ID  NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_BODY_MEMBER
(
  TABLE_ROW_ID        NUMBER(10)                    NULL,
  PI_ROW_ID           NUMBER(10)                    NULL,
  WCP_BODY_MEMBER_ID  NUMBER(10)                    NULL,
  IMPAIRMENT_PCNT     NUMBER                        NULL,
  AMPUTATED_CODE      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_BODY_PART
(
  PI_ROW_ID       NUMBER(10)                        NULL,
  BODY_PART_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_DEPENDENT
(
  PI_DEP_ROW_ID     NUMBER(10)                      NULL,
  PI_ROW_ID         NUMBER(10)                      NULL,
  DEPENDENT_EID     NUMBER(10)                      NULL,
  HEALTH_PLAN_FLAG  NUMBER(5)                       NULL,
  RELATION_CODE     NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_DEP_SUPP
(
  PI_DEP_ROW_ID  NUMBER(10)                         NULL,
  DEPENDENT_EID  NUMBER(10)                         NULL,
  PI_ROW_ID      NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_DIAGNOSIS
(
  PI_ROW_ID       NUMBER(10)                        NULL,
  DIAGNOSIS_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_DIAG_HIST
(
  PI_X_DIAGROW_ID   NUMBER(10)                      NULL,
  PI_ROW_ID         NUMBER(10)                      NULL,
  DIAGNOSIS_CODE    NUMBER(10)                      NULL,
  DATE_CHANGED      VARCHAR2(8 BYTE)                NULL,
  CHGD_BY_USER      VARCHAR2(8 BYTE)                NULL,
  APPROVED_BY_USER  VARCHAR2(8 BYTE)                NULL,
  REASON            CLOB                            NULL,
  DIS_MDA_ID        NUMBER(10)                      NULL,
  DIS_MDA_TOPIC     CLOB                            NULL,
  DIS_GUIDELINE_ID  NUMBER(10)                      NULL,
  DIS_FACTOR        CLOB                            NULL,
  DIS_MAP_ID        NUMBER(10)                      NULL,
  DIS_MIN_DURATION  NUMBER(10)                      NULL,
  DIS_OPT_DURATION  NUMBER(10)                      NULL,
  DIS_MAX_DURATION  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_HOSPITAL
(
  PI_ROW_ID     NUMBER(10)                          NULL,
  HOSPITAL_EID  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_ILLNESS
(
  PI_ROW_ID     NUMBER(10)                          NULL,
  ILLNESS_CODE  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_INJURY
(
  PI_ROW_ID    NUMBER(10)                           NULL,
  INJURY_CODE  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_MED_COND
(
  PI_ROW_ID      NUMBER(10)                         NULL,
  MED_COND_CODE  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_MMI_HIST
(
  PI_X_MMIROW_ID      NUMBER(10)                    NULL,
  PI_ROW_ID           NUMBER(10)                    NULL,
  MMI_DATE            VARCHAR2(8 BYTE)              NULL,
  EXAM_DATE           VARCHAR2(8 BYTE)              NULL,
  PERCENT_DISABILITY  NUMBER                        NULL,
  PHYSICIAN_EID       NUMBER(10)                    NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  APPROVED_BY_USER    VARCHAR2(8 BYTE)              NULL,
  REASON              CLOB                          NULL,
  DTTM_RCD_ADDED      VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_PHYSICIAN
(
  PI_ROW_ID      NUMBER(10)                         NULL,
  PHYSICIAN_EID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_PROCEDURE
(
  PI_PROC_ROW_ID      NUMBER(10)                    NULL,
  PI_ROW_ID           NUMBER(10)                    NULL,
  PROCEDURE_CODE      NUMBER(10)                    NULL,
  DATE_OF_PROCEDURE   VARCHAR2(8 BYTE)              NULL,
  SURGEON_EID         NUMBER(10)                    NULL,
  ANESTH_ADMIN_FLAG   NUMBER(5)                     NULL,
  ANESTH_TYPE_CODE    NUMBER(10)                    NULL,
  ANESTH_EID          NUMBER(10)                    NULL,
  ASA_PS_CLASS_CODE   NUMBER(10)                    NULL,
  COMPLICATION_DATE   VARCHAR2(8 BYTE)              NULL,
  PRIOR_TO_ANES_FLAG  NUMBER(5)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_PROC_SUPP
(
  PI_PROC_ROW_ID  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_RESTRCT_SUPP
(
  PI_RESTRICT_ROW_ID  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_RESTRICT
(
  PI_RESTRICT_ROW_ID  NUMBER(10)                    NULL,
  PI_ROW_ID           NUMBER(10)                    NULL,
  DATE_FIRST_RESTRCT  VARCHAR2(8 BYTE)              NULL,
  PERCENT_DISABLED    NUMBER(10)                    NULL,
  DATE_LAST_RESTRCT   VARCHAR2(8 BYTE)              NULL,
  DURATION            NUMBER(10)                    NULL,
  POSITION_CODE       NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_TREATMENT
(
  PI_ROW_ID       NUMBER(10)                        NULL,
  TREATMENT_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_VIOLATION
(
  VIOLATION_ID    NUMBER(10)                        NULL,
  PI_EID          NUMBER(10)                        NULL,
  VIOLATION_CODE  NUMBER(10)                        NULL,
  VIOLATION_DATE  VARCHAR2(8 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_WORK_LOSS
(
  PI_WL_ROW_ID      NUMBER(10)                      NULL,
  PI_ROW_ID         NUMBER(10)                      NULL,
  DATE_LAST_WORKED  VARCHAR2(8 BYTE)                NULL,
  DATE_RETURNED     VARCHAR2(8 BYTE)                NULL,
  DURATION          NUMBER(10)                      NULL,
  STATE_DURATION    NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PI_X_WRKLOSS_SUPP
(
  PI_WL_ROW_ID  NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY
(
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_NAME         VARCHAR2(20 BYTE)             NULL,
  POLICY_NUMBER       VARCHAR2(20 BYTE)             NULL,
  POLICY_STATUS_CODE  NUMBER(10)                    NULL,
  INSURER_EID         NUMBER(10)                    NULL,
  ISSUE_DATE          VARCHAR2(8 BYTE)              NULL,
  REVIEW_DATE         VARCHAR2(8 BYTE)              NULL,
  RENEWAL_DATE        VARCHAR2(8 BYTE)              NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  CANCEL_DATE         VARCHAR2(8 BYTE)              NULL,
  PREMIUM             NUMBER                        NULL,
  TRIGGER_CLAIM_FLAG  NUMBER(5)                     NULL,
  PRIMARY_POLICY_FLG  NUMBER(5)                     NULL,
  COMMENTS            CLOB                          NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  BROKER_EID          NUMBER(10)                    NULL,
  BANK_ACC_ID         NUMBER(10)                    NULL,
  SUB_ACC_ROW_ID      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_NAME         VARCHAR2(50 BYTE)             NULL,
  POLICY_STATUS_CODE  NUMBER(10)                    NULL,
  POLICY_TYPE         NUMBER(10)                    NULL,
  POLICY_INDICATOR    NUMBER(10)                    NULL,
  INSURER_EID         NUMBER(10)                    NULL,
  BROKER_EID          NUMBER(10)                    NULL,
  ISSUE_DATE          VARCHAR2(14 BYTE)             NULL,
  REVIEW_DATE         VARCHAR2(14 BYTE)             NULL,
  TRIGGER_CLAIM_FLAG  NUMBER(5)                     NULL,
  PRIMARY_POLICY_FLG  NUMBER(5)                     NULL,
  TRIGGER_EVENT_FLAG  NUMBER(5)                     NULL,
  BANK_ACC_ID         NUMBER(10)                    NULL,
  SUB_ACC_ROW_ID      NUMBER(10)                    NULL,
  STATE               NUMBER(10)                    NULL,
  COUNTRY             NUMBER(10)                    NULL,
  NON_RENEW_FLAG      NUMBER(5)                     NULL,
  NON_RENEW_REASON    NUMBER(10)                    NULL,
  COMMENTS            CLOB                          NULL,
  RETRO_DATE          VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_ENH_SUPP
(
  POLICY_ID     NUMBER(10)                          NULL,
  FIELD1_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD2_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD3_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD4_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD5_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD6_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD7_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD1_CODE   NUMBER(10)                          NULL,
  FIELD8_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD9_TEXT   VARCHAR2(50 BYTE)                   NULL,
  FIELD2_AMT    NUMBER                              NULL,
  FIELD10_TEXT  VARCHAR2(50 BYTE)                   NULL,
  FIELD1_DATE   VARCHAR2(8 BYTE)                    NULL,
  FIELD2_DATE   VARCHAR2(8 BYTE)                    NULL,
  FIELD11_TEXT  VARCHAR2(50 BYTE)                   NULL,
  FIELD12_TEXT  VARCHAR2(50 BYTE)                   NULL,
  FIELD13_TEXT  VARCHAR2(50 BYTE)                   NULL,
  FIELD1_AMT    NUMBER                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_SUPP
(
  POLICY_ID  NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_BILL_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  POLICY_BILLING_ID   NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  PAY_PLAN_ROWID      NUMBER(10)                    NULL,
  BILL_TO_EID         NUMBER(10)                    NULL,
  BILL_TO_TYPE        NUMBER(10)                    NULL,
  BILL_TO_OVERRIDE    NUMBER(5)                     NULL,
  NEW_BILL_TO_EID     NUMBER(10)                    NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  BILLING_RULE_ROWID  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_CVG_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  POLCVG_ROW_ID       NUMBER(10)                    NULL,
  COVERAGE_TYPE_CODE  NUMBER(10)                    NULL,
  STATUS              NUMBER(10)                    NULL,
  COVERAGE_NUMBER     NUMBER(10)                    NULL,
  SEQUENCE_ALPHA      NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  AMENDED_DATE        VARCHAR2(8 BYTE)              NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  CLAIM_LIMIT         NUMBER(10)                    NULL,
  OCCURRENCE_LIMIT    NUMBER(10)                    NULL,
  POLICY_LIMIT        NUMBER(10)                    NULL,
  TOTAL_PAYMENTS      NUMBER(10)                    NULL,
  NOTIFICATION_UID    NUMBER(10)                    NULL,
  SELF_INS_RETENTION  NUMBER(10)                    NULL,
  DEDUCTIBLE          NUMBER(10)                    NULL,
  REMARKS             CLOB                          NULL,
  EXCEPTIONS          CLOB                          NULL,
  CANCEL_NOTICE_DAYS  NUMBER(10)                    NULL,
  NEXT_POLICY_ID      NUMBER(10)                    NULL,
  BROKER_NAME         VARCHAR2(50 BYTE)             NULL,
  TRANSACTION_TYPE    NUMBER(10)                    NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  COV_RENEWABLE_FLAG  NUMBER(5)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_CVG_TYPE
(
  CLAIM_LIMIT         NUMBER                        NULL,
  NOTIFICATION_UID    NUMBER(10)                    NULL,
  POLCVG_ROW_ID       NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  COVERAGE_TYPE_CODE  NUMBER(10)                    NULL,
  POLICY_LIMIT        NUMBER                        NULL,
  OCCURRENCE_LIMIT    NUMBER                        NULL,
  TOTAL_PAYMENTS      NUMBER                        NULL,
  REMARKS             CLOB                          NULL,
  EXCEPTIONS          CLOB                          NULL,
  CANCEL_NOTICE_DAYS  NUMBER(10)                    NULL,
  SELF_INSURE_DEDUCT  NUMBER                        NULL,
  NEXT_POLICY_ID      NUMBER(10)                    NULL,
  BROKER_NAME         VARCHAR2(100 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_DCNT_ENH
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DISCOUNT_ID        NUMBER(10)                     NULL,
  POLICY_ID          NUMBER(10)                     NULL,
  TRANSACTION_ID     NUMBER(10)                     NULL,
  RATING_ID          NUMBER(10)                     NULL,
  DISCOUNT_NAME_ID   NUMBER(10)                     NULL,
  DISCOUNT_LEVEL_ID  NUMBER(10)                     NULL,
  DISCOUNT_FACTOR    NUMBER                         NULL,
  DISCOUNT_AMT       NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_DTIER_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DISCOUNT_TIER_ID    NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  RATING_ID           NUMBER(10)                    NULL,
  DISC_TIER_NAME_ID   NUMBER(10)                    NULL,
  DISC_TIER_LEVEL_ID  NUMBER(10)                    NULL,
  DISC_TIER_AMT       NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_EXP_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  EXPOSURE_ID         NUMBER(10)                    NULL,
  EXPOSURE_CODE       NUMBER(10)                    NULL,
  EXPOSURE_AMOUNT     NUMBER                        NULL,
  EXPOSURE_RATE       NUMBER                        NULL,
  EXPOSURE_BASE_RATE  NUMBER                        NULL,
  FULL_ANN_PREM_AMT   NUMBER                        NULL,
  PR_ANN_PREM_AMT     NUMBER                        NULL,
  PREM_ADJ_AMT        NUMBER                        NULL,
  AMENDED_DATE        VARCHAR2(8 BYTE)              NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  TRANSACTION_TYPE    NUMBER(10)                    NULL,
  COVERAGE_CODE       NUMBER(10)                    NULL,
  SEQUENCE_ALPHA      NUMBER(10)                    NULL,
  STATUS              NUMBER(10)                    NULL,
  HEIRARCHY_LEVEL     NUMBER(10)                    NULL,
  RENEWABLE_FLAG      NUMBER(10)                    NULL,
  EXPOSURE_COUNT      NUMBER(10)                    NULL,
  REMARKS             CLOB                          NULL,
  EXCEPTIONS          CLOB                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_FORMS_ENH
(
  POLICY_ID  NUMBER(10)                             NULL,
  FORM_ID    NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_INSRD_ENH
(
  POLICY_ID    NUMBER(10)                           NULL,
  INSURED_EID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_INSURED
(
  POLICY_ID    NUMBER(10)                           NULL,
  INSURED_EID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_MCO
(
  POLICY_ID       NUMBER(10)                        NULL,
  MCO_EID         NUMBER(10)                        NULL,
  MCO_BEGIN_DATE  VARCHAR2(8 BYTE)                  NULL,
  MCO_END_DATE    VARCHAR2(8 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_MCO_ENH
(
  POLICY_ID       NUMBER(10)                        NULL,
  MCO_EID         NUMBER(10)                        NULL,
  MCO_BEGIN_DATE  VARCHAR2(8 BYTE)                  NULL,
  MCO_END_DATE    VARCHAR2(8 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_RTNG_ENH
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  RATING_ID          NUMBER(10)                     NULL,
  POLICY_ID          NUMBER(10)                     NULL,
  TERM_NUMBER        NUMBER(10)                     NULL,
  SEQUENCE_ALPHA     NUMBER(10)                     NULL,
  TRANSACTION_ID     NUMBER(10)                     NULL,
  TRANSACTION_TYPE   NUMBER(10)                     NULL,
  MANUAL_PREMIUM     NUMBER                         NULL,
  EXP_MOD_FACTOR     NUMBER                         NULL,
  EXP_MOD_AMOUNT     NUMBER                         NULL,
  MODIFIED_PREMIUM   NUMBER                         NULL,
  EXPENSE_CONSTANT   NUMBER                         NULL,
  TOTAL_EST_PREMIUM  NUMBER                         NULL,
  AUDITED_PREMIUM    NUMBER                         NULL,
  TOTAL_BILLED_PREM  NUMBER                         NULL,
  TRANSACTION_PREM   NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_TERM_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  TERM_ID             NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  POLICY_NUMBER       VARCHAR2(20 BYTE)             NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  SEQUENCE_ALPHA      NUMBER(10)                    NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  TERM_STATUS         NUMBER(10)                    NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  RENEWAL_IND         NUMBER(10)                    NULL,
  RENEWAL_TERM_CODE   NUMBER(10)                    NULL,
  RENEWAL_TERM_AMT    NUMBER(10)                    NULL,
  RENEWAL_DATE        VARCHAR2(8 BYTE)              NULL,
  AUDITED_FLAG        NUMBER(5)                     NULL,
  CANCEL_DATE         VARCHAR2(8 BYTE)              NULL,
  CANCEL_TYPE         NUMBER(10)                    NULL,
  CANCEL_REASON       NUMBER(10)                    NULL,
  REINSTATEMENT_DATE  VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POLICY_X_TRANS_ENH
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  TRANSACTION_ID      NUMBER(10)                    NULL,
  POLICY_ID           NUMBER(10)                    NULL,
  TERM_NUMBER         NUMBER(10)                    NULL,
  TERM_SEQ_ALPHA      NUMBER(10)                    NULL,
  TRANSACTION_TYPE    NUMBER(10)                    NULL,
  SEQUENCE_ALPHA      NUMBER(10)                    NULL,
  TRANSACTION_STATUS  NUMBER(10)                    NULL,
  EFFECTIVE_DATE      VARCHAR2(8 BYTE)              NULL,
  EXPIRATION_DATE     VARCHAR2(8 BYTE)              NULL,
  TRANSACTION_DATE    VARCHAR2(8 BYTE)              NULL,
  DATE_APPROVED       VARCHAR2(8 BYTE)              NULL,
  TRANSACTION_PREM    NUMBER                        NULL,
  TOTAL_EST_PREMIUM   NUMBER                        NULL,
  TOTAL_BILLED_PREM   NUMBER                        NULL,
  CHG_BILLED_PREMIUM  NUMBER                        NULL,
  WAIVE_PREMIUM_IND   NUMBER(5)                     NULL,
  WAIVE_PREM_COMMENT  VARCHAR2(50 BYTE)             NULL,
  COVERAGE_RCD_COUNT  NUMBER(10)                    NULL,
  EXPOSURE_RCD_COUNT  NUMBER(10)                    NULL,
  POLICY_STATUS       NUMBER(10)                    NULL,
  BILLING_TRANS_NUM   NUMBER(10)                    NULL,
  INVOICE_NUMBER      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PORTALITEMS
(
  USERID    NUMBER(10)                              NULL,
  ITEM_ID   NUMBER(10)                              NULL,
  ITEM      VARCHAR2(50 BYTE)                       NULL,
  STATUS    NUMBER(10)                              NULL,
  ISDETAIL  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PORTALTEMPLATE
(
  USERID       NUMBER(10)                           NULL,
  XMLTEMPLATE  CLOB                                 NULL,
  ACTUALLOOK   CLOB                                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE POSTAL_CODES
(
  JURIS_ROW_ID       NUMBER(10)                     NULL,
  POSTAL_CODE        VARCHAR2(12 BYTE)              NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PROJ_TRACK
(
  PROJ_TRACK_ID     NUMBER(10)                      NULL,
  ACT_TYPE_CODE     NUMBER(10)                      NULL,
  ACTIV_DESC_TXCD   CLOB                            NULL,
  ASSIGN_BY_TEXT    VARCHAR2(50 BYTE)               NULL,
  ASSIGN_TO_TXCD    CLOB                            NULL,
  PRIORITY_CODE     NUMBER(10)                      NULL,
  EST_HOURS_NUM     NUMBER                          NULL,
  ACT_HOURS_NUM     NUMBER                          NULL,
  DUE_DATE_DATE     VARCHAR2(8 BYTE)                NULL,
  REVIEW_DATE_DATE  VARCHAR2(8 BYTE)                NULL,
  COMPLETE_DT_DATE  VARCHAR2(8 BYTE)                NULL,
  STATUS_CODE       NUMBER(10)                      NULL,
  MAT_USED_TXCD     CLOB                            NULL,
  MAT_COST_AMT      NUMBER                          NULL,
  NOTES_TXCD        CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PROP_MGT
(
  PROP_MGT_ID       NUMBER(10)                      NULL,
  PROP_ID_TEXT      VARCHAR2(50 BYTE)               NULL,
  LOCATION_TXCD     CLOB                            NULL,
  OWNERSHIP_CODE    NUMBER(10)                      NULL,
  PROP_TYPE_CODE    NUMBER(10)                      NULL,
  PROP_CAT_TEXT     VARCHAR2(50 BYTE)               NULL,
  REPORTED_BY_TXCD  CLOB                            NULL,
  DT_UPDATE_DATE    VARCHAR2(8 BYTE)                NULL,
  REV_DATE_DATE     VARCHAR2(8 BYTE)                NULL,
  DETAIL_TXCD       CLOB                            NULL,
  ORIG_VALUE_AMT    NUMBER                          NULL,
  CURR_VALUE_AMT    NUMBER                          NULL,
  REPLACE_VAL_AMT   NUMBER                          NULL,
  CONT_ORG_VL_AMT   NUMBER                          NULL,
  CONT_REP_VL_AMT   NUMBER                          NULL,
  CONT_CUR_VL_AMT   NUMBER                          NULL,
  INSTALL_DT_DATE   VARCHAR2(8 BYTE)                NULL,
  CONTACT_TEXT      VARCHAR2(50 BYTE)               NULL,
  CONTACT_ADD_TEXT  VARCHAR2(50 BYTE)               NULL,
  CONT_CITY_TEXT    VARCHAR2(25 BYTE)               NULL,
  CONT_STATE_TEXT   VARCHAR2(15 BYTE)               NULL,
  CONT_ZIP_TEXT     VARCHAR2(15 BYTE)               NULL,
  CONT_PHONE_TEXT   VARCHAR2(25 BYTE)               NULL,
  COVERAGE_CODE     NUMBER(10)                      NULL,
  DAMAGES_TXCD      CLOB                            NULL,
  IMPROVEMENT_TXCD  CLOB                            NULL,
  NOTES_TXCD        CLOB                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PROVIDER_CONTRACTS
(
  PRVD_CONT_ROW_ID    NUMBER(10)                    NULL,
  PROVIDER_EID        NUMBER(10)                    NULL,
  START_DATE          VARCHAR2(8 BYTE)              NULL,
  AMOUNT              NUMBER                        NULL,
  DISCOUNT            NUMBER                        NULL,
  COMMENTS            VARCHAR2(255 BYTE)            NULL,
  FEE_TABLE_ID        NUMBER(10)                    NULL,
  FIRST_RATE          NUMBER                        NULL,
  LAST_RATE           NUMBER                        NULL,
  RATE_CHG            NUMBER(10)                    NULL,
  DISC_ON_SCHD_FLAG   NUMBER(10)                    NULL,
  DISC_ON_BILL_FLAG   NUMBER(10)                    NULL,
  END_DATE            VARCHAR2(8 BYTE)              NULL,
  SECOND_RATE         NUMBER                        NULL,
  THIRD_RATE          NUMBER                        NULL,
  RATE_CHG_2ND        NUMBER(10)                    NULL,
  DISC_2ND_SCHD_FLAG  NUMBER(10)                    NULL,
  DISCOUNT_2ND        NUMBER                        NULL,
  DISCOUNT_BILL       NUMBER                        NULL,
  FEE_TABLE_ID_2ND    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE PRT_CHECK_LIMITS
(
  GROUP_ID          NUMBER(10)                      NULL,
  USER_ID           NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL,
  MAX_AMOUNT        NUMBER                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QENTRY_PERM
(
  VIEW_ID   NUMBER(10)                              NULL,
  GROUP_ID  NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QENTRY_VIEWDEFS
(
  VIEW_ID       NUMBER(10)                          NULL,
  CONTROLNAME   VARCHAR2(50 BYTE)                   NULL,
  CONTROLINDEX  NUMBER(10)                          NULL,
  SUBORDERNR    NUMBER(10)                          NULL,
  ORDERNR       NUMBER(10)                          NULL,
  BUTTON        NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QENTRY_VIEWS
(
  VIEW_ID       NUMBER(10)                          NULL,
  FRIENDLYNAME  VARCHAR2(50 BYTE)                   NULL,
  FORMNAME      VARCHAR2(50 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QE_DEF
(
  VIEW_ID   NUMBER(10)                              NULL,
  FIELD_ID  NUMBER(10)                              NULL,
  SEQ_NUM   NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QE_DICTIONARY
(
  FIELD_ID         NUMBER(10)                       NULL,
  ASSOC_FORM_CODE  NUMBER(10)                       NULL,
  TAG_ID           NUMBER(10)                       NULL,
  FIELD_LOCATION   NUMBER(10)                       NULL,
  FIELD_CATEGORY   NUMBER(10)                       NULL,
  FIELD_TYPE       NUMBER(10)                       NULL,
  NUM_FIELDS       NUMBER(10)                       NULL,
  REQUIRED_FLAG    NUMBER(5)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QE_PERMISSIONS
(
  GROUP_ID  NUMBER(10)                              NULL,
  VIEW_ID   NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE QE_VIEWS
(
  VIEW_ID            NUMBER(10)                     NULL,
  ASSOC_FORM_CODE    NUMBER(10)                     NULL,
  VIEW_DESC          VARCHAR2(50 BYTE)              NULL,
  SUPP_FLAG          NUMBER(5)                      NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE REHAB_SUPP
(
  REHAB_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE REQ_DEF
(
  LABEL_ID       NUMBER(10)                         NULL,
  REQUIRED_FLAG  NUMBER(5)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE REQ_FIELDS
(
  FORMNAME   VARCHAR2(40 BYTE)                      NULL,
  CONTROLID  NUMBER(10)                             NULL,
  CTRLNAME   VARCHAR2(40 BYTE)                      NULL,
  CTRLINDEX  NUMBER(10)                             NULL,
  SYSREQ     NUMBER(10)                             NULL,
  LABEL      NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RESERVE_CURRENT
(
  BALANCE_AMOUNT     NUMBER                         NULL,
  RC_ROW_ID          NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  CLAIMANT_EID       NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  RESERVE_AMOUNT     NUMBER                         NULL,
  INCURRED_AMOUNT    NUMBER                         NULL,
  COLLECTION_TOTAL   NUMBER                         NULL,
  PAID_TOTAL         NUMBER                         NULL,
  DATE_ENTERED       VARCHAR2(8 BYTE)               NULL,
  ENTERED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  REASON             VARCHAR2(30 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  CRC                NUMBER(10)                     NULL,
  SEC_DEPT_EID       NUMBER(10)                     NULL,
  RES_STATUS_CODE    NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RESERVE_HISTORY
(
  BALANCE_AMOUNT     NUMBER                         NULL,
  CHANGE_AMOUNT      NUMBER                         NULL,
  RSV_ROW_ID         NUMBER(10)                     NULL,
  CLAIM_ID           NUMBER(10)                     NULL,
  CLAIMANT_EID       NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  RESERVE_AMOUNT     NUMBER                         NULL,
  COLLECTION_TOTAL   NUMBER                         NULL,
  INCURRED_AMOUNT    NUMBER                         NULL,
  PAID_TOTAL         NUMBER                         NULL,
  DATE_ENTERED       VARCHAR2(8 BYTE)               NULL,
  ENTERED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  REASON             VARCHAR2(30 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  CRC                NUMBER(10)                     NULL,
  SEC_DEPT_EID       NUMBER(10)                     NULL,
  RES_STATUS_CODE    NUMBER(10)                     NULL,
  CLOSED_FLAG        NUMBER(10)                     NULL,
  AUTO_ADJ_FLAG      NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RESERVE_LIMITS
(
  GROUP_ID           NUMBER(10)                     NULL,
  USER_ID            NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  MAX_AMOUNT         NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RMNET_MODULES
(
  MODULE_ID  NUMBER(10)                             NULL,
  NAME       VARCHAR2(50 BYTE)                      NULL,
  LINK       VARCHAR2(255 BYTE)                     NULL,
  STATUS     NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RMNET_MODULES_USER
(
  MODULE_ID  NUMBER(10)                             NULL,
  USERID     NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE RM_USER
(
  USER_EID           NUMBER(10)                     NULL,
  USER_ID            NUMBER(10)                     NULL,
  RM_USER_TYPE_CODE  NUMBER(10)                     NULL,
  DOC_PATH           VARCHAR2(255 BYTE)             NULL,
  NOTES              CLOB                           NULL,
  SECURITY_CODE      NUMBER(5)                      NULL,
  LOGIN_NAME         VARCHAR2(8 BYTE)               NULL,
  DELETED_FLAG       NUMBER(5)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SEARCH_CAT
(
  CAT_ID    NUMBER(10)                              NULL,
  CAT_NAME  VARCHAR2(50 BYTE)                       NULL,
  CAT_DESC  VARCHAR2(250 BYTE)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SEARCH_DICTIONARY
(
  FIELD_ID     NUMBER(10)                           NULL,
  CAT_ID       NUMBER(10)                           NULL,
  FIELD_NAME   VARCHAR2(25 BYTE)                    NULL,
  FIELD_DESC   VARCHAR2(250 BYTE)                   NULL,
  FIELD_TABLE  VARCHAR2(25 BYTE)                    NULL,
  FIELD_TYPE   NUMBER(10)                           NULL,
  OPT_MASK     VARCHAR2(50 BYTE)                    NULL,
  DISPLAY_CAT  VARCHAR2(100 BYTE)                   NULL,
  CODE_TABLE   VARCHAR2(50 BYTE)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SEARCH_VIEW
(
  VIEW_ID        NUMBER(10)                         NULL,
  CAT_ID         NUMBER(10)                         NULL,
  VIEW_NAME      VARCHAR2(25 BYTE)                  NULL,
  VIEW_DESC      VARCHAR2(250 BYTE)                 NULL,
  VIEW_AT_TABLE  VARCHAR2(100 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SEARCH_VIEW_DEF
(
  VIEW_ID           NUMBER(10)                      NULL,
  FIELD_ID          NUMBER(10)                      NULL,
  ITEM_TYPE         NUMBER(10)                      NULL,
  SEQ_NUM           NUMBER(10)                      NULL,
  SUPP_FIELD_FLAG   NUMBER(10)                      NULL,
  QUERY_FIELD_TYPE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SEARCH_VIEW_PERM
(
  VIEW_ID   NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL,
  GROUP_ID  NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SECURITY
(
  ITEM_ID         NUMBER(10)                        NULL,
  ITEM_TYPE_CODE  NUMBER(10)                        NULL,
  SECURITY_LEVEL  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SM_FISCAL_YEAR
(
  FISCAL_YR_TEXT      VARCHAR2(4 BYTE)              NULL,
  FISCAL_YR_START_DT  VARCHAR2(10 BYTE)             NULL,
  FISCAL_YR_END_DT    VARCHAR2(10 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STAFF_CERTS
(
  STAFF_CERT_ID  NUMBER(10)                         NULL,
  STAFF_EID      NUMBER(10)                         NULL,
  NAME_CODE      NUMBER(10)                         NULL,
  INT_DATE       VARCHAR2(8 BYTE)                   NULL,
  END_DATE       VARCHAR2(8 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STAFF_PRIVS
(
  STAFF_PRIV_ID  NUMBER(10)                         NULL,
  STAFF_EID      NUMBER(10)                         NULL,
  CATEGORY_CODE  NUMBER(10)                         NULL,
  TYPE_CODE      NUMBER(10)                         NULL,
  STATUS_CODE    NUMBER(10)                         NULL,
  INT_DATE       VARCHAR2(8 BYTE)                   NULL,
  END_DATE       VARCHAR2(8 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STATES
(
  STATE_ROW_ID        NUMBER(10)                    NULL,
  STATE_ID            VARCHAR2(4 BYTE)              NULL,
  STATE_NAME          VARCHAR2(25 BYTE)             NULL,
  EMPLOYER_LIABILITY  NUMBER                        NULL,
  MAX_CLAIM_AMOUNT    NUMBER                        NULL,
  RATING_METHOD_CODE  NUMBER(10)                    NULL,
  USL_LIMIT           NUMBER                        NULL,
  WEIGHTING_STEP_VAL  NUMBER                        NULL,
  DELETED_FLAG        NUMBER(5)                     NULL,
  DUR_CALC_OPTION     NUMBER(10)                    NULL,
  AWW_FORM_OPTION     NUMBER(10)                    NULL,
  FROI_COPIES         NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STATE_FORM_AUX
(
  ID_NUM           NUMBER(10)                       NULL,
  AUX_FORM_ID_NUM  NUMBER(10)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STATE_FORM_HISTORY
(
  ID_NUM        NUMBER(10)                          NULL,
  DATE_PRINTED  VARCHAR2(8 BYTE)                    NULL,
  TIME_PRINTED  VARCHAR2(11 BYTE)                   NULL,
  CLAIM_ID      NUMBER(10)                          NULL,
  USER_ID       VARCHAR2(10 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE STATE_FORM_PRINT
(
  ACTIVE_FLAG      NUMBER(10)                       NULL,
  CARTRIDGE_ID     NUMBER(10)                       NULL,
  VALID_FLAG       NUMBER(10)                       NULL,
  ID_NUM           NUMBER(5)                        NULL,
  STATE_ROW_ID     NUMBER(5)                        NULL,
  FORM_NAME        VARCHAR2(50 BYTE)                NULL,
  LASER_CART_NUM   VARCHAR2(20 BYTE)                NULL,
  ACTIVE           VARCHAR2(1 BYTE)                 NULL,
  CRC              VARCHAR2(16 BYTE)                NULL,
  VALID_CRC        VARCHAR2(1 BYTE)                 NULL,
  OTHER_FORMS      VARCHAR2(250 BYTE)               NULL,
  PAPER_SIZE       VARCHAR2(50 BYTE)                NULL,
  FORM_SHORT_NAME  VARCHAR2(10 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUB_ACC_SERV_CODE
(
  ACCOUNT_ID    NUMBER(10)                          NULL,
  SERVICE_CODE  NUMBER(10)                          NULL,
  SUB_ACC_ID    NUMBER(10)                          NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUPP_ASSOC
(
  FIELD_ID       NUMBER(10)                         NULL,
  ASSOC_CODE_ID  NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUPP_DICTIONARY
(
  FIELD_ID         NUMBER(10)                       NULL,
  SEQ_NUM          NUMBER(10)                       NULL,
  SUPP_TABLE_NAME  VARCHAR2(20 BYTE)                NULL,
  SYS_FIELD_NAME   VARCHAR2(30 BYTE)                NULL,
  FIELD_TYPE       NUMBER(10)                       NULL,
  FIELD_SIZE       NUMBER(10)                       NULL,
  REQUIRED_FLAG    NUMBER(5)                        NULL,
  DELETE_FLAG      NUMBER(5)                        NULL,
  LOOKUP_FLAG      NUMBER(5)                        NULL,
  IS_PATTERNED     NUMBER(5)                        NULL,
  PATTERN          VARCHAR2(30 BYTE)                NULL,
  CODE_FILE_ID     NUMBER(10)                       NULL,
  GRP_ASSOC_FLAG   NUMBER(5)                        NULL,
  ASSOC_FIELD      VARCHAR2(50 BYTE)                NULL,
  HELP_CONTEXT_ID  NUMBER(10)                       NULL,
  NETVISIBLE_FLAG  NUMBER(5)                        NULL,
  UP_OLD           VARCHAR2(50 BYTE)                NULL,
  USER_PROMPT      VARCHAR2(50 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUPP_GRID_DICT
(
  GRID_DICT_ROW_ID  NUMBER(10)                      NULL,
  FIELD_ID          NUMBER(10)                      NULL,
  COL_HEADER        VARCHAR2(50 BYTE)               NULL,
  COL_NO            NUMBER(10)                      NULL,
  DELETED_FLAG      NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUPP_GRID_VALUE
(
  GRID_VALUE_ROW_ID  NUMBER(10)                     NULL,
  FIELD_ID           NUMBER(10)                     NULL,
  RECORD_ID          NUMBER(10)                     NULL,
  ROW_NUM            NUMBER(10)                     NULL,
  COL_NUM            NUMBER(10)                     NULL,
  CELL_VALUE         VARCHAR2(150 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUPP_MULTI_VALUE
(
  FIELD_ID   NUMBER(10)                             NULL,
  RECORD_ID  NUMBER(10)                             NULL,
  CODE_ID    NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SUP_HOLD_TRANS
(
  TRANS_TYPE_CODE  NUMBER(10)                       NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_BILL_BLNG_RULE
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  BILLING_RULE_ROWID  NUMBER(10)                    NULL,
  BILLING_RULE_CODE   NUMBER(10)                    NULL,
  FIRST_NOTICE_DAYS   VARCHAR2(8 BYTE)              NULL,
  SECOND_NOTICE_DAYS  VARCHAR2(8 BYTE)              NULL,
  THIRD_NOTICE_DAYS   VARCHAR2(8 BYTE)              NULL,
  FOURTH_NOTICE_DAYS  VARCHAR2(8 BYTE)              NULL,
  NOTICE_OFFSET_IND   NUMBER(10)                    NULL,
  MAX_NUM_REBILLS     NUMBER(10)                    NULL,
  DUE_DAYS            NUMBER(10)                    NULL,
  OUTSTAND_BAL_INDIC  NUMBER(10)                    NULL,
  IN_USE_FLAG         NUMBER(5)                     NULL,
  NON_PAY_IND         NUMBER(10)                    NULL,
  CANCEL_PEND_DIARY   NUMBER(10)                    NULL,
  CANCEL_DIARY        NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_BILL_OPTIONS
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  BILL_OPT_ROWID     NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_BILL_PAY_PLAN
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  PAY_PLAN_ROWID      NUMBER(10)                    NULL,
  PAY_PLAN_CODE       NUMBER(10)                    NULL,
  BILLING_RULE_ROWID  NUMBER(10)                    NULL,
  IN_USE_FLAG         NUMBER(5)                     NULL,
  LINE_OF_BUSINESS    NUMBER(10)                    NULL,
  STATE               NUMBER(10)                    NULL,
  INSTALL_GEN_TYPE    NUMBER(10)                    NULL,
  FIXED_FREQUENCY     NUMBER(10)                    NULL,
  FIXED_GEN_DAY       NUMBER(10)                    NULL,
  FIXED_DUE_DAYS      VARCHAR2(8 BYTE)              NULL,
  INSTALL1_AMOUNT     NUMBER                        NULL,
  INSTALL1_TYPE       NUMBER(10)                    NULL,
  INSTALL1_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL1_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL2_AMOUNT     NUMBER                        NULL,
  INSTALL2_TYPE       NUMBER(10)                    NULL,
  INSTALL2_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL2_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL3_AMOUNT     NUMBER                        NULL,
  INSTALL3_TYPE       NUMBER(10)                    NULL,
  INSTALL3_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL3_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL4_AMOUNT     NUMBER                        NULL,
  INSTALL4_TYPE       NUMBER(10)                    NULL,
  INSTALL4_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL4_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL5_AMOUNT     NUMBER                        NULL,
  INSTALL5_TYPE       NUMBER(10)                    NULL,
  INSTALL5_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL5_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL6_AMOUNT     NUMBER                        NULL,
  INSTALL6_TYPE       NUMBER(10)                    NULL,
  INSTALL6_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL6_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL7_AMOUNT     NUMBER                        NULL,
  INSTALL7_TYPE       NUMBER(10)                    NULL,
  INSTALL7_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL7_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL8_AMOUNT     NUMBER                        NULL,
  INSTALL8_TYPE       NUMBER(10)                    NULL,
  INSTALL8_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL8_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL9_AMOUNT     NUMBER                        NULL,
  INSTALL9_TYPE       NUMBER(10)                    NULL,
  INSTALL9_LAG_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL9_DUE_DAYS   VARCHAR2(8 BYTE)              NULL,
  INSTALL10_AMOUNT    NUMBER                        NULL,
  INSTALL10_TYPE      NUMBER(10)                    NULL,
  INSTALL10_LAG_DAYS  VARCHAR2(8 BYTE)              NULL,
  INSTALL10_DUE_DAYS  VARCHAR2(8 BYTE)              NULL,
  INSTALL11_AMOUNT    NUMBER                        NULL,
  INSTALL11_TYPE      NUMBER(10)                    NULL,
  INSTALL11_LAG_DAYS  VARCHAR2(8 BYTE)              NULL,
  INSTALL11_DUE_DAYS  VARCHAR2(8 BYTE)              NULL,
  INSTALL12_AMOUNT    NUMBER                        NULL,
  INSTALL12_TYPE      NUMBER(10)                    NULL,
  INSTALL12_LAG_DAYS  VARCHAR2(8 BYTE)              NULL,
  INSTALL12_DUE_DAYS  VARCHAR2(8 BYTE)              NULL,
  EXTRA_AMTS_INDIC    NUMBER(10)                    NULL,
  LATE_START_INDIC    NUMBER(10)                    NULL,
  OFFSET_INDIC        NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_CLM_TYPE_RES
(
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL,
  CLAIM_TYPE_CODE    NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_ERLY_PAY_DSCNT
(
  DTTM_RCD_LAST_UPD     VARCHAR2(14 BYTE)           NULL,
  UPDATED_BY_USER       VARCHAR2(8 BYTE)            NULL,
  DTTM_RCD_ADDED        VARCHAR2(14 BYTE)           NULL,
  ADDED_BY_USER         VARCHAR2(8 BYTE)            NULL,
  EARLY_DISCOUNT_ROWID  NUMBER(10)                  NULL,
  DISCOUNT_TYPE         NUMBER(10)                  NULL,
  AMOUNT                NUMBER                      NULL,
  LINE_OF_BUSINESS      NUMBER(10)                  NULL,
  DEADLINE_DATE         VARCHAR2(8 BYTE)            NULL,
  IN_USE_FLAG           NUMBER(5)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_EVENTS
(
  DTTM_OF_EVENT      VARCHAR2(14 BYTE)              NULL,
  EVENT_TYPE_CODE    NUMBER(10)                     NULL,
  LOGIN_ID           VARCHAR2(8 BYTE)               NULL,
  RM_USER_ID         NUMBER(10)                     NULL,
  ELAPSED_TIME       NUMBER(10)                     NULL,
  REPORT_CODE        NUMBER(10)                     NULL,
  ROWS_SELECTED      NUMBER(10)                     NULL,
  SUMMARY_FLAG       NUMBER(5)                      NULL,
  SORTMASTER_FLAG    NUMBER(5)                      NULL,
  ROW_COUNT          NUMBER(10)                     NULL,
  TABLE_ID           NUMBER(10)                     NULL,
  TABLE_NAME         VARCHAR2(18 BYTE)              NULL,
  TRANS_CODE         NUMBER(10)                     NULL,
  TRANS_DESC         CLOB                           NULL,
  SYSTEM_TABLE_NAME  VARCHAR2(18 BYTE)              NULL,
  RECORD_ID          NUMBER(10)                     NULL,
  SYS_FUNC_NAME      VARCHAR2(32 BYTE)              NULL,
  TRACE_CODE         NUMBER(10)                     NULL,
  EVENT_CODE         NUMBER(10)                     NULL,
  EVENT_DATA         NUMBER(10)                     NULL,
  ACTIVITY_CODE      NUMBER(10)                     NULL,
  ACTIVITY_DESC      CLOB                           NULL,
  RPT_NUM_SORTS      NUMBER(5)                      NULL,
  RPT_NUM_TOTALS     NUMBER(5)                      NULL,
  RPT_NUM_RANGES     NUMBER(5)                      NULL,
  RPT_NUM_FIELDS     NUMBER                         NULL,
  PHONE_NUM          VARCHAR2(25 BYTE)              NULL,
  MAPI_RECIP         VARCHAR2(200 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_EVENT_CAT
(
  EVENT_CAT_CODE  NUMBER(10)                        NULL,
  DISPLAY_ID      NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_HOLIDAYS
(
  HOL_DATE     VARCHAR2(8 BYTE)                     NULL,
  DESCRIPTION  VARCHAR2(50 BYTE)                    NULL,
  ORG_EID      NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_LOB_RESERVES
(
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL,
  RESERVE_TYPE_CODE  NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_PARMS
(
  ACC_LINK_FLAG           NUMBER(5)                 NULL,
  BLANK_FIELD_FLAG        VARCHAR2(3 BYTE)          NULL,
  FIN_HIST_EVAL           NUMBER(5)                 NULL,
  HIERARCHY_LEVEL         VARCHAR2(15 BYTE)         NULL,
  PRINT_CLAIMS_ADMIN      NUMBER(10)                NULL,
  PRINT_OSHA_DESC         NUMBER(10)                NULL,
  WC_PRINTER_SETUP        VARCHAR2(15 BYTE)         NULL,
  X_AXIS                  NUMBER                    NULL,
  Y_AXIS                  NUMBER                    NULL,
  ROW_ID                  NUMBER(10)                NULL,
  OC_FAC_ADDR1            VARCHAR2(50 BYTE)         NULL,
  OC_FAC_ADDR2            VARCHAR2(50 BYTE)         NULL,
  OC_FAC_ADDR3            VARCHAR2(50 BYTE)         NULL,
  OC_FAC_ADDR4            VARCHAR2(50 BYTE)         NULL,
  OC_FAC_CONTACT          VARCHAR2(50 BYTE)         NULL,
  OC_FAC_HCFA_NO          VARCHAR2(20 BYTE)         NULL,
  OC_FAC_NAME             VARCHAR2(50 BYTE)         NULL,
  OC_FAC_PHONE            VARCHAR2(20 BYTE)         NULL,
  WORK_FRI                NUMBER(5)                 NULL,
  WORK_MON                NUMBER(5)                 NULL,
  WORK_SAT                NUMBER(5)                 NULL,
  WORK_SUN                NUMBER(5)                 NULL,
  WORK_THU                NUMBER(5)                 NULL,
  WORK_TUE                NUMBER(5)                 NULL,
  WORK_WED                NUMBER(5)                 NULL,
  SECURITY_FLAG           NUMBER(5)                 NULL,
  DATE_STAMP_FLAG         NUMBER(5)                 NULL,
  SHIFT_1_NAME            VARCHAR2(50 BYTE)         NULL,
  SHIFT_2_NAME            VARCHAR2(50 BYTE)         NULL,
  SHIFT_3_NAME            VARCHAR2(50 BYTE)         NULL,
  SHIFT_1_START           VARCHAR2(6 BYTE)          NULL,
  SHIFT_2_START           VARCHAR2(6 BYTE)          NULL,
  SHIFT_3_START           VARCHAR2(6 BYTE)          NULL,
  SHIFT_1_END             VARCHAR2(6 BYTE)          NULL,
  SHIFT_2_END             VARCHAR2(6 BYTE)          NULL,
  SHIFT_3_END             VARCHAR2(6 BYTE)          NULL,
  CLIENT_NAME             VARCHAR2(50 BYTE)         NULL,
  ESSP                    NUMBER(10)                NULL,
  CLIENT_STATUS_CODE      NUMBER(10)                NULL,
  DATE_INSTALLED          VARCHAR2(8 BYTE)          NULL,
  ADDR1                   VARCHAR2(50 BYTE)         NULL,
  ADDR2                   VARCHAR2(50 BYTE)         NULL,
  CITY                    VARCHAR2(50 BYTE)         NULL,
  STATE                   VARCHAR2(4 BYTE)          NULL,
  ZIP_CODE                VARCHAR2(10 BYTE)         NULL,
  PHONE_NUMBER            VARCHAR2(25 BYTE)         NULL,
  FAX_NUMBER              VARCHAR2(25 BYTE)         NULL,
  PRIMARY_CONTACT         VARCHAR2(50 BYTE)         NULL,
  PRIMARY_TITLE           VARCHAR2(50 BYTE)         NULL,
  SECONDARY_CONTACT       VARCHAR2(50 BYTE)         NULL,
  SECONDARY_TITLE         VARCHAR2(50 BYTE)         NULL,
  DATA_ENTRY_CONTACT      VARCHAR2(50 BYTE)         NULL,
  DATA_ENTRY_TITLE        VARCHAR2(50 BYTE)         NULL,
  REPORTING_CONTACT       VARCHAR2(50 BYTE)         NULL,
  REPORTING_TITLE         VARCHAR2(50 BYTE)         NULL,
  DEPT_MGR_CONTACT        VARCHAR2(50 BYTE)         NULL,
  DEPT_MGR_TITLE          VARCHAR2(50 BYTE)         NULL,
  ACCOUNTING_CONTACT      VARCHAR2(50 BYTE)         NULL,
  ACCOUNTING_TITLE        VARCHAR2(50 BYTE)         NULL,
  DTG_PRODUCT             VARCHAR2(50 BYTE)         NULL,
  DTG_VERSION             VARCHAR2(50 BYTE)         NULL,
  HARDWARE                VARCHAR2(50 BYTE)         NULL,
  PRINTER                 VARCHAR2(50 BYTE)         NULL,
  DISTRIBUTION_MEDIA      VARCHAR2(50 BYTE)         NULL,
  MODEM_PHONE             VARCHAR2(20 BYTE)         NULL,
  DTG_SUPPORT_PHONE       VARCHAR2(25 BYTE)         NULL,
  EV_PREFIX               VARCHAR2(3 BYTE)          NULL,
  EV_INC_YEAR_FLAG        NUMBER(5)                 NULL,
  FREEZE_TEXT_FLAG        NUMBER(5)                 NULL,
  RELEASE_NUMBER          VARCHAR2(11 BYTE)         NULL,
  USE_SUB_ACCOUNT         NUMBER(5)                 NULL,
  EV_TYPE_ACC_FLAG        NUMBER(5)                 NULL,
  SM_LIMIT_DIRECT         NUMBER(5)                 NULL,
  RES_DO_NOT_RECALC       NUMBER(5)                 NULL,
  SMTP_SERVER             VARCHAR2(100 BYTE)        NULL,
  EXCL_HOLIDAYS           NUMBER(5)                 NULL,
  USE_DEFAULT_CARRIER     NUMBER(5)                 NULL,
  ATTACH_DIARY_VIS        NUMBER(5)                 NULL,
  USE_DEF_CARRIER         NUMBER(5)                 NULL,
  FROI_PREPARER           NUMBER(10)                NULL,
  USETANDE                NUMBER(10)                NULL,
  RATE_LEVEL              NUMBER(10)                NULL,
  TANDEACCT_ID            NUMBER(10)                NULL,
  FREEZE_DATE_FLAG        NUMBER(10)                NULL,
  DATE_ADJUSTER_FLAG      NUMBER(10)                NULL,
  FROI_BAT_HIGH_LVL       NUMBER(10)                NULL,
  FROI_BAT_LOW_LVL        NUMBER(10)                NULL,
  FROI_BAT_CLD_CLM        NUMBER(10)                NULL,
  AUTO_NUM_WC_EMP         NUMBER(5)                 NULL,
  WC_EMP_PREFIX           VARCHAR2(8 BYTE)          NULL,
  EDIT_WC_EMP_NUM         NUMBER(5)                 NULL,
  AUTO_NUM_VIN            NUMBER(5)                 NULL,
  AUTO_SELECT_POLICY      NUMBER(5)                 NULL,
  PREV_RES_BACKDATING     NUMBER(5)                 NULL,
  PREV_RES_MODIFYZERO     NUMBER(5)                 NULL,
  DIARY_ORG_LEVEL         NUMBER(10)                NULL,
  ALLOW_GLOBAL_PEEK       NUMBER(10)                NULL,
  RES_SHOW_ORPHANS        NUMBER(10)                NULL,
  NO_SUB_DEP_CHECK        NUMBER(10)                NULL,
  DEFAULT_TIME_FLAG       NUMBER(10)                NULL,
  DEFAULT_DEPT_FLAG       NUMBER(10)                NULL,
  FILTER_OTH_PEOPLE       NUMBER(10)                NULL,
  PRT_EOB_WITH_CHKS       NUMBER(10)                NULL,
  FREEZE_EVENT_DESC       NUMBER(10)                NULL,
  USE_MAST_BANK_ACCT      NUMBER(5)                 NULL,
  P2_LOGIN                VARCHAR2(64 BYTE)         NULL,
  P2_PASS                 VARCHAR2(64 BYTE)         NULL,
  DATE_EVENT_DESC         NUMBER(10)                NULL,
  ORDER_BANK_ACC          NUMBER(10)                NULL,
  SUPP_FLD_SEC_FLAG       NUMBER(10)                NULL,
  ENH_HIERARCHY_LIST      NUMBER(10)                NULL,
  EV_CAPTION_LEVEL        NUMBER(10)                NULL,
  USE_ENH_POL_FLAG        NUMBER(10)                NULL,
  PREV_RES_BELOWPAID      NUMBER(10)                NULL,
  ASSIGN_SUB_ACCOUNT      NUMBER(10)                NULL,
  P2_URL                  VARCHAR2(200 BYTE)        NULL,
  ORACLE_CASE_INS         NUMBER(10)                NULL,
  USE_CLAIMANT_PROC_FLAG  NUMBER(10)                NULL,
  CLAIMANT_PROCESS_URL    VARCHAR2(128 BYTE)        NULL,
  CLAIMANT_PROCESS_LOGIN  VARCHAR2(64 BYTE)         NULL,
  CLAIMANT_PROCESS_PASS   VARCHAR2(64 BYTE)         NULL,
  HOSP_PHYS_ANY_ENT       NUMBER(5)                 NULL,
  USE_BILLING_FLAG        NUMBER(10)                NULL,
  FREEZE_LOC_DESC         NUMBER(10)                NULL,
  LEAVE_RSV_STATUS        NUMBER(10)                NULL,
  OVERRIDE_EOB_PRINT      NUMBER(10)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_PARMS_LOB
(
  CLM_ACC_LMT_FLAG    NUMBER(5)                     NULL,
  DUPLICATE_FLAG      NUMBER(5)                     NULL,
  INC_YEAR_FLAG       NUMBER(5)                     NULL,
  INSUF_FUNDS_FLAG    NUMBER(5)                     NULL,
  PAY_DET_LMT_FLAG    NUMBER(5)                     NULL,
  PAY_LMT_FLAG        NUMBER(5)                     NULL,
  PRT_CHK_LMT_FLAG    NUMBER(5)                     NULL,
  RES_LMT_FLAG        NUMBER(5)                     NULL,
  RESERVE_TRACKING    NUMBER(10)                    NULL,
  LINE_OF_BUS_CODE    NUMBER(10)                    NULL,
  ADJ_RSV             NUMBER(5)                     NULL,
  COLL_IN_RSV_BAL     NUMBER(5)                     NULL,
  BAL_TO_ZERO         NUMBER(5)                     NULL,
  NEG_BAL_TO_ZERO     NUMBER(5)                     NULL,
  SET_TO_ZERO         NUMBER(5)                     NULL,
  EVENT_TRIGGER       NUMBER(5)                     NULL,
  INC_CLM_TYPE_FLAG   NUMBER(5)                     NULL,
  INC_LOB_CODE_FLAG   NUMBER(5)                     NULL,
  INC_ORG_FLAG        NUMBER(5)                     NULL,
  ORG_TABLE_ID        NUMBER(10)                    NULL,
  DURATION_CODE       NUMBER(10)                    NULL,
  EST_NUM_OF_CLAIMS   NUMBER(10)                    NULL,
  NONEG_RES_ADJ_FLAG  NUMBER(10)                    NULL,
  RES_BY_CLM_TYPE     NUMBER(5)                     NULL,
  USE_ADJTEXT         NUMBER(5)                     NULL,
  CAPTION_LEVEL       NUMBER(10)                    NULL,
  ADJ_RSV_CLAIM       NUMBER(5)                     NULL,
  PER_CL_LMT_FLAG     NUMBER(5)                     NULL,
  DUP_PAY_CRITERIA    NUMBER(10)                    NULL,
  DUP_NUM_DAYS        NUMBER(10)                    NULL,
  NO_WEEKS_PER_MONTH  NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_BILL_THRSH
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  THRESHOLD_ROWID    NUMBER(10)                     NULL,
  IN_USE_FLAG        NUMBER(5)                      NULL,
  LINE_OF_BUSINESS   NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_DISCOUNTS
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DISCOUNT_ROWID     NUMBER(10)                     NULL,
  DISCOUNT_NAME      VARCHAR2(30 BYTE)              NULL,
  LINE_OF_BUSINESS   NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  FLAT_OR_PERCENT    NUMBER(10)                     NULL,
  RANGE              NUMBER                         NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_DISC_RANGE
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DISC_RANGE_ROWID   NUMBER(10)                     NULL,
  DISCOUNT_ROWID     NUMBER(10)                     NULL,
  BEGINNING_RANGE    NUMBER                         NULL,
  END_RANGE          NUMBER                         NULL,
  AMOUNT             NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_DISC_TIER
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DISC_TIER_ROWID    NUMBER(10)                     NULL,
  IN_USE_FLAG        NUMBER(5)                      NULL,
  TIER_NAME          VARCHAR2(30 BYTE)              NULL,
  LINE_OF_BUSINESS   NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL,
  PARENT_LEVEL       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_EXP_CONST
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  EXP_CON_ROWID      NUMBER(10)                     NULL,
  IN_USE_FLAG        NUMBER(5)                      NULL,
  LINE_OF_BUSINESS   NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  FLAT_OR_PERCENT    NUMBER(10)                     NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_EXP_RATES
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  EXP_RATE_ROWID     NUMBER(10)                     NULL,
  EXPOSURE_ID        NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  AMOUNT             NUMBER                         NULL,
  FLAT_OR_PERCENT    NUMBER(10)                     NULL,
  FIXED_OR_PRORATE   NUMBER(10)                     NULL,
  BASE_RATE          NUMBER                         NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  EXPIRATION_DATE    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_OPTIONS
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  POL_OPT_ROWID      NUMBER(10)                     NULL,
  ROUND_AMTS_FLAG    NUMBER(5)                      NULL,
  USE_YEAR_FLAG      NUMBER(5)                      NULL,
  PLACEMENT          NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_RNWL_INFO
(
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  RENEWAL_INFO_ROWID  NUMBER(10)                    NULL,
  LINE_OF_BUSINESS    NUMBER(10)                    NULL,
  STATE               NUMBER(10)                    NULL,
  AMOUNT              NUMBER                        NULL,
  TERM_CODE           NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_POL_SEL_DISC
(
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  SEL_DISC_ROWID     NUMBER(10)                     NULL,
  IN_USE_FLAG        NUMBER(5)                      NULL,
  LINE_OF_BUSINESS   NUMBER(10)                     NULL,
  DISCOUNT_ROWID     NUMBER(10)                     NULL,
  STATE              NUMBER(10)                     NULL,
  PARENT_LEVEL       NUMBER(10)                     NULL,
  OVERRIDE_FLAG      NUMBER(5)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_RES_AUTOADJ
(
  RES_TYPE_CODE     NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_SENT_EVENT
(
  EVENT_IND_CODE  NUMBER(10)                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_TRACE_LOG
(
  EVENT_ID       NUMBER(10)                         NULL,
  SYS_FUNC_NAME  VARCHAR2(32 BYTE)                  NULL,
  TRACE_CODE     NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE SYS_TRANS_CLOSE
(
  TRANS_TYPE_CODE   NUMBER(10)                      NULL,
  LINE_OF_BUS_CODE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE TASK_HISTORY
(
  TASK_ID     NUMBER(10)                            NULL,
  DTTM_START  VARCHAR2(14 BYTE)                     NULL,
  DTTM_END    VARCHAR2(14 BYTE)                     NULL,
  USER_NAME   VARCHAR2(16 BYTE)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE TASK_OCCURRENCE
(
  TASK_ID        NUMBER(10)                         NULL,
  INC_EXC_FLAG   NUMBER(10)                         NULL,
  TASK_RUN_TYPE  NUMBER(10)                         NULL,
  RUN_DAYS       NUMBER(10)                         NULL,
  RUN_TIME       VARCHAR2(4 BYTE)                   NULL,
  RUN_DATE       VARCHAR2(8 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE TASK_PROPS
(
  TASK_ID          NUMBER(10)                       NULL,
  PROP_ID          NUMBER(10)                       NULL,
  START_DATE_TYPE  NUMBER(10)                       NULL,
  END_DATE_TYPE    NUMBER(10)                       NULL,
  END_DATE         VARCHAR2(8 BYTE)                 NULL,
  OPERATOR         VARCHAR2(3 BYTE)                 NULL,
  START_DATE       VARCHAR2(14 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE TASK_SCHEDULE
(
  TASK_ID            NUMBER(10)                     NULL,
  TASK_TYPE          NUMBER(10)                     NULL,
  DIS_IMPORT_AREA    VARCHAR2(50 BYTE)              NULL,
  FILE_NAME          VARCHAR2(255 BYTE)             NULL,
  SM_TITLE           VARCHAR2(80 BYTE)              NULL,
  SM_SUBTITLE        VARCHAR2(80 BYTE)              NULL,
  TASK_EXEC_TYPE     NUMBER(10)                     NULL,
  RUN_TIME           VARCHAR2(4 BYTE)               NULL,
  USER_NAME          VARCHAR2(16 BYTE)              NULL,
  LAST_RUN           VARCHAR2(12 BYTE)              NULL,
  TASK_STATUS        NUMBER(10)                     NULL,
  TASK_NAME          VARCHAR2(255 BYTE)             NULL,
  SM_HTML_OUT        NUMBER(10)                     NULL,
  NEXT_RUN_DATE      VARCHAR2(8 BYTE)               NULL,
  DEP_TASK_ID        NUMBER(10)                     NULL,
  OUTPUT_FILENAME    VARCHAR2(255 BYTE)             NULL,
  SM_PDF_OUT         NUMBER(5)                      NULL,
  LOGIN_DATA_SOURCE  VARCHAR2(100 BYTE)             NULL,
  LOGIN_USER_ID      VARCHAR2(100 BYTE)             NULL,
  LOGIN_PASSWORD     VARCHAR2(100 BYTE)             NULL,
  EMAIL_TO           CLOB                           NULL,
  EMAIL_MSG          VARCHAR2(255 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE TAX_MAPPING
(
  TAX_NAME            VARCHAR2(50 BYTE)             NULL,
  TAX_EID             NUMBER(10)                    NULL,
  TRANS_TYPE_CODE_ID  NUMBER(10)                    NULL,
  TAX_PERCENTAGE      NUMBER                        NULL,
  STATE_ID            NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_POLICY
(
  ROW_ID           NUMBER(10)                       NULL,
  LOCATION_ID      NUMBER(10)                       NULL,
  CLAIM_ID         NUMBER(10)                       NULL,
  UNIT_NUMBER      VARCHAR2(3 BYTE)                 NULL,
  UNIT_TABLE_NAME  VARCHAR2(50 BYTE)                NULL,
  UNIT_RECORD_ID   NUMBER(10)                       NULL,
  ADDRESS1         VARCHAR2(64 BYTE)                NULL,
  ADDRESS2         VARCHAR2(64 BYTE)                NULL,
  CITY             VARCHAR2(32 BYTE)                NULL,
  COUNTY           VARCHAR2(32 BYTE)                NULL,
  STATE            NUMBER(10)                       NULL,
  ZIP              VARCHAR2(10 BYTE)                NULL,
  CLASS_CODE       VARCHAR2(6 BYTE)                 NULL,
  TERRITORY        VARCHAR2(3 BYTE)                 NULL,
  YEAR             VARCHAR2(4 BYTE)                 NULL,
  MAKE             VARCHAR2(20 BYTE)                NULL,
  MODEL            VARCHAR2(50 BYTE)                NULL,
  VIN              VARCHAR2(20 BYTE)                NULL,
  VEHICLE_NUMBER   VARCHAR2(3 BYTE)                 NULL,
  DTTM_RCD_ADDED   VARCHAR2(14 BYTE)                NULL,
  DTTM_RCD_UPD     VARCHAR2(14 BYTE)                NULL,
  ADDED_BY_USER    VARCHAR2(8 BYTE)                 NULL,
  UPDATED_BY_USER  VARCHAR2(8 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_SELECTED
(
  ROW_ID           NUMBER(10)                       NULL,
  UNIT_ID          NUMBER(10)                       NULL,
  CLAIMANT_ID      NUMBER(10)                       NULL,
  DTTM_RCD_ADDED   VARCHAR2(14 BYTE)                NULL,
  DTTM_RCD_UPD     VARCHAR2(14 BYTE)                NULL,
  ADDED_BY_USER    VARCHAR2(8 BYTE)                 NULL,
  UPDATED_BY_USER  VARCHAR2(8 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_SELECTED_SUPP
(
  UNIT_ROW_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_THIRD_PARTY
(
  ROW_ID            NUMBER(10)                      NULL,
  CLAIM_ID          NUMBER(10)                      NULL,
  UNIT_NUMBER       VARCHAR2(3 BYTE)                NULL,
  CLAIMANT_ID       NUMBER(10)                      NULL,
  UNIT_DESCRIPTION  VARCHAR2(25 BYTE)               NULL,
  UNIT_VALUE        NUMBER                          NULL,
  UNIT_TYPE         NUMBER(10)                      NULL,
  ADDRESS           VARCHAR2(64 BYTE)               NULL,
  CITY              VARCHAR2(32 BYTE)               NULL,
  COUNTY            VARCHAR2(32 BYTE)               NULL,
  STATE             NUMBER(10)                      NULL,
  ZIP               VARCHAR2(10 BYTE)               NULL,
  YEAR              VARCHAR2(4 BYTE)                NULL,
  MAKE              VARCHAR2(20 BYTE)               NULL,
  MODEL             VARCHAR2(50 BYTE)               NULL,
  VIN               VARCHAR2(20 BYTE)               NULL,
  VEHICLE_NUMBER    VARCHAR2(3 BYTE)                NULL,
  DTTM_RCD_ADDED    VARCHAR2(14 BYTE)               NULL,
  DTTM_RCD_UPD      VARCHAR2(14 BYTE)               NULL,
  ADDED_BY_USER     VARCHAR2(8 BYTE)                NULL,
  UPDATED_BY_USER   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_THIRD_SUPP
(
  UNIT_ROW_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_X_CLAIM
(
  UNIT_ROW_ID     NUMBER(10)                        NULL,
  UNIT_ID         NUMBER(10)                        NULL,
  CLAIM_ID        NUMBER(10)                        NULL,
  VEHICLE_MAKE    VARCHAR2(20 BYTE)                 NULL,
  VEHICLE_YEAR    NUMBER(10)                        NULL,
  HOME_DEPT_EID   NUMBER(10)                        NULL,
  LICENSE_NUMBER  VARCHAR2(20 BYTE)                 NULL,
  STATE_ROW_ID    NUMBER(10)                        NULL,
  DAMAGE          CLOB                              NULL,
  UNIT_TYPE_CODE  NUMBER(10)                        NULL,
  VIN             VARCHAR2(20 BYTE)                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE UNIT_X_CLAIM_SUPP
(
  UNIT_ROW_ID  NUMBER(10)                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE USER_GROUPS
(
  GROUP_ID    NUMBER(10)                            NULL,
  GROUP_NAME  VARCHAR2(32 BYTE)                     NULL,
  CRC         NUMBER(10)                            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE USER_MEMBERSHIP
(
  GROUP_ID  NUMBER(10)                              NULL,
  USER_ID   NUMBER(10)                              NULL,
  CRC       NUMBER(10)                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE USER_PREF_XML
(
  USER_ID   NUMBER(10)                              NULL,
  PREF_XML  CLOB                                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VBS_OBJECTS
(
  OBJ_NAME       VARCHAR2(50 BYTE)                  NULL,
  FRIENDLY_NAME  VARCHAR2(75 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VBS_PROCS
(
  TYPE_ID      NUMBER(10)                           NULL,
  SCRIPTPROCS  CLOB                                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VBS_SYS_SCRIPTS
(
  KEY_NAME     VARCHAR2(50 BYTE)                    NULL,
  SCRIPT_TEXT  CLOB                                 NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VBS_TYPES
(
  TYPE_ID        NUMBER(10)                         NULL,
  FRIENDLY_NAME  VARCHAR2(50 BYTE)                  NULL,
  PROC_PREFIX    VARCHAR2(10 BYTE)                  NULL,
  DESCRIPTION    VARCHAR2(255 BYTE)                 NULL,
  FUNCTION       NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VEHICLE
(
  DISPOSAL_DATE       VARCHAR2(8 BYTE)              NULL,
  LEASE_AMOUNT        NUMBER                        NULL,
  LEASE_EXPIRE_DATE   VARCHAR2(8 BYTE)              NULL,
  LEASE_FLAG          NUMBER(5)                     NULL,
  LEASE_NUMBER        VARCHAR2(50 BYTE)             NULL,
  LEASE_TERM          NUMBER(10)                    NULL,
  LEASING_CO_EID      NUMBER(10)                    NULL,
  LICENSE_RNWL_DATE   VARCHAR2(8 BYTE)              NULL,
  VEHICLE_MODEL       VARCHAR2(50 BYTE)             NULL,
  UNIT_ID             NUMBER(10)                    NULL,
  VEHICLE_MAKE        VARCHAR2(20 BYTE)             NULL,
  VEHICLE_YEAR        NUMBER(10)                    NULL,
  UNIT_TYPE_CODE      NUMBER(10)                    NULL,
  HOME_DEPT_EID       NUMBER(10)                    NULL,
  LICENSE_NUMBER      VARCHAR2(20 BYTE)             NULL,
  STATE_ROW_ID        NUMBER(10)                    NULL,
  GROSS_WEIGHT        NUMBER(10)                    NULL,
  VIN                 VARCHAR2(20 BYTE)             NULL,
  ORIGINAL_COST       NUMBER                        NULL,
  PURCHASE_DATE       VARCHAR2(8 BYTE)              NULL,
  DEDUCTIBLE          NUMBER                        NULL,
  LAST_SERVICE_DATE   VARCHAR2(8 BYTE)              NULL,
  TYPE_OF_SERVICE     CLOB                          NULL,
  INSURANCE_COVERAGE  CLOB                          NULL,
  DELETED_FLAG        NUMBER(5)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VEHICLE_SUPP
(
  UNIT_ID  NUMBER(10)                               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VEHICLE_X_ACC_DATE
(
  UNIT_ID        NUMBER(10)                         NULL,
  ACCIDENT_DATE  VARCHAR2(8 BYTE)                   NULL,
  CLAIM_ID       NUMBER(10)                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE VEHICLE_X_INSPCT
(
  UNIT_INSP_ROW_ID   NUMBER(10)                     NULL,
  UNIT_ID            NUMBER(10)                     NULL,
  INSPECTION_DATE    VARCHAR2(8 BYTE)               NULL,
  INSPECTION_RESULT  VARCHAR2(255 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_AGNCY_SERV_ZIP
(
  STATE_ROW_ID       NUMBER(10)                     NULL,
  ZIP_CODE           VARCHAR2(10 BYTE)              NULL,
  AGENCY_ID          NUMBER(10)                     NULL,
  FUNCTION_CODE_ID   NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(12 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(12 BYTE)              NULL,
  DELETED_FLAG       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_BENEFIT_LKUP
(
  BENEFIT_LKUP_ID     NUMBER(10)                    NULL,
  JURIS_ROW_ID        NUMBER(10)                    NULL,
  JURIS_BENEFIT_DESC  VARCHAR2(60 BYTE)             NULL,
  TAB_INDEX           NUMBER(10)                    NULL,
  TAB_CAPTION         VARCHAR2(30 BYTE)             NULL,
  DELETE_FLAG         NUMBER(10)                    NULL,
  USE_IN_CALCULATOR   NUMBER(10)                    NULL,
  ADDED_BY_USER       VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_ADDED      VARCHAR2(14 BYTE)             NULL,
  UPDATED_BY_USER     VARCHAR2(8 BYTE)              NULL,
  DTTM_RCD_LAST_UPD   VARCHAR2(14 BYTE)             NULL,
  ABBREVIATION        VARCHAR2(12 BYTE)             NULL,
  TABLE_ROW_ID        NUMBER(10)                    NULL,
  TYPE_DESC_ROW_ID    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_BEN_TYPE_LKUP
(
  TABLE_ROW_ID  NUMBER(10)                          NULL,
  TYPE_DESC     VARCHAR2(32 BYTE)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_CDC_LIFE_EXPCT
(
  CDC_DECENNIAL  VARCHAR2(9 BYTE)                   NULL,
  STATE_ROW_ID   NUMBER(10)                         NULL,
  SEX            VARCHAR2(1 BYTE)                   NULL,
  AGE            NUMBER(10)                         NULL,
  YRS_REMAINING  NUMBER                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_CLAIMS
(
  CLAIM_ID           NUMBER(10)                     NULL,
  ROW_ID             NUMBER(10)                     NULL,
  BENEFIT_TYPE       VARCHAR2(10 BYTE)              NULL,
  BENEFIT_RATE       NUMBER                         NULL,
  DISABILITY_RATE    NUMBER                         NULL,
  FORM_ID            NUMBER(10)                     NULL,
  BEGIN_PRNT_DATE    VARCHAR2(8 BYTE)               NULL,
  END_RECVD_DATE     VARCHAR2(8 BYTE)               NULL,
  EST_COST           NUMBER                         NULL,
  PAID               NUMBER                         NULL,
  COMMUTATION        NUMBER                         NULL,
  WEEKS_PAYABLE      NUMBER                         NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  ATTY_FEE_CODE      NUMBER(10)                     NULL,
  ATTY_FEE_PERCENT   NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_DATA_BODY_MEMB
(
  TABLE_ROW_ID       NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  EFFECTIVE_DATE     VARCHAR2(8 BYTE)               NULL,
  JURIS_ROW_ID       NUMBER(10)                     NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  BODY_MEMB_DESC     VARCHAR2(255 BYTE)             NULL,
  MAX_AMOUNT         NUMBER                         NULL,
  MAX_WEEKS          NUMBER                         NULL,
  MAX_MONTHS         NUMBER                         NULL,
  AMPUTAT_RATE       NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_FORMS
(
  STATE_ROW_ID       NUMBER(10)                     NULL,
  FORM_CATEGORY      NUMBER(10)                     NULL,
  FORM_ID            NUMBER(10)                     NULL,
  FORM_TITLE         VARCHAR2(128 BYTE)             NULL,
  FORM_NAME          VARCHAR2(64 BYTE)              NULL,
  FILE_NAME          VARCHAR2(64 BYTE)              NULL,
  ACTIVE_FLAG        NUMBER(10)                     NULL,
  HASH_CRC           VARCHAR2(128 BYTE)             NULL,
  PRIMARY_FORM_FLAG  NUMBER(10)                     NULL,
  LINE_OF_BUS_CODE   NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_FORMS_CAT_LKUP
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  FORM_CAT       NUMBER(10)                         NULL,
  FORM_CAT_DESC  VARCHAR2(64 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_FRIENDLY_CALC
(
  TABLE_ROW_ID     NUMBER(10)                       NULL,
  CALCULATOR_NAME  VARCHAR2(32 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_FRIENDLY_NAME
(
  TABLE_ROW_ID       NUMBER(10)                     NULL,
  FORM_ID            NUMBER(10)                     NULL,
  USED_IN_CALC       NUMBER(10)                     NULL,
  FRIENDLY_NAME      VARCHAR2(64 BYTE)              NULL,
  FIELD_NAME         VARCHAR2(18 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_LIFEPEN_LIMITS
(
  STATE_ROW_ID  NUMBER(10)                          NULL,
  BEGIN_DATE    VARCHAR2(8 BYTE)                    NULL,
  END_DATE      VARCHAR2(8 BYTE)                    NULL,
  MIN_AWW       NUMBER                              NULL,
  MAX_AWW       NUMBER                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_PAYROL_PERID
(
  TABLE_ROW_ID       NUMBER(10)                     NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DELETED_FLAG       NUMBER(10)                     NULL,
  SHORT_DESC         VARCHAR2(32 BYTE)              NULL,
  LONG_DESC          VARCHAR2(64 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_PPD_LIMITS
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  BEGIN_PERCENT  NUMBER                             NULL,
  END_PERCENT    NUMBER                             NULL,
  BEGIN_DATE     VARCHAR2(8 BYTE)                   NULL,
  END_DATE       VARCHAR2(8 BYTE)                   NULL,
  MIN_BENEFIT    NUMBER                             NULL,
  MAX_BENEFIT    NUMBER                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_PPD_WEEKS
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  BEGIN_DATE     VARCHAR2(8 BYTE)                   NULL,
  BEGIN_PERCENT  NUMBER                             NULL,
  END_PERCENT    NUMBER                             NULL,
  BASE_WEEKS     NUMBER                             NULL,
  PD_MULTIPLIER  NUMBER(10)                         NULL,
  PD_DISTRACTOR  NUMBER                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_PRSNT_VALUE_LP
(
  STATE_ROW_ID  NUMBER(10)                          NULL,
  SEX_CODE_ID   NUMBER(10)                          NULL,
  AGE           NUMBER(10)                          NULL,
  YRS_0         NUMBER                              NULL,
  YRS_1         NUMBER                              NULL,
  YRS_2         NUMBER                              NULL,
  YRS_3         NUMBER                              NULL,
  YRS_4         NUMBER                              NULL,
  YRS_5         NUMBER                              NULL,
  YRS_6         NUMBER                              NULL,
  YRS_7         NUMBER                              NULL,
  YRS_8         NUMBER                              NULL,
  YRS_9         NUMBER                              NULL,
  YRS_10        NUMBER                              NULL,
  YRS_11        NUMBER                              NULL,
  YRS_12        NUMBER                              NULL,
  YRS_13        NUMBER                              NULL,
  YRS_14        NUMBER                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_PRSNT_VALUE_PD
(
  STATE_ROW_ID   NUMBER(10)                         NULL,
  WEEKS          NUMBER(10)                         NULL,
  PRESENT_VALUE  NUMBER                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_STATE_AGENCY
(
  AGENCY_ID          NUMBER(10)                     NULL,
  STATE_ROW_ID       NUMBER(10)                     NULL,
  FUNCTION_CODE_ID   NUMBER(10)                     NULL,
  LAST_NAME          VARCHAR2(50 BYTE)              NULL,
  ADDR1              VARCHAR2(50 BYTE)              NULL,
  ADDR2              VARCHAR2(50 BYTE)              NULL,
  CITY               VARCHAR2(50 BYTE)              NULL,
  ZIP_CODE           VARCHAR2(10 BYTE)              NULL,
  PHONE              VARCHAR2(14 BYTE)              NULL,
  CONTACT            VARCHAR2(50 BYTE)              NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(12 BYTE)              NULL,
  ADDED_BY_USER      VARCHAR2(12 BYTE)              NULL,
  DELETED_FLAG       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_TOTDIS_LIMITS
(
  STATE_ROW_ID  NUMBER(10)                          NULL,
  BEGIN_DATE    VARCHAR2(8 BYTE)                    NULL,
  END_DATE      VARCHAR2(8 BYTE)                    NULL,
  MIN_BENEFIT   NUMBER                              NULL,
  MAX_BENEFIT   NUMBER                              NULL,
  MAX_AWW       NUMBER                              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WCP_TRANS_TYPES
(
  ROW_ID             NUMBER(10)                     NULL,
  STATE_ROW_ID       NUMBER(10)                     NULL,
  SCREEN_ID          NUMBER(10)                     NULL,
  CODE_ID            NUMBER(10)                     NULL,
  UTIL32_CBO_ID      NUMBER(10)                     NULL,
  ADDED_BY_USER      VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_ADDED     VARCHAR2(14 BYTE)              NULL,
  UPDATED_BY_USER    VARCHAR2(8 BYTE)               NULL,
  DTTM_RCD_LAST_UPD  VARCHAR2(14 BYTE)              NULL,
  BENEFIT_LKUP_ID    NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_AB_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  REPORT_REASON       NUMBER(10)                    NULL,
  HEALTH_CARE_PROV    VARCHAR2(50 BYTE)             NULL,
  PROV_HEALTH_CARE    VARCHAR2(50 BYTE)             NULL,
  EMPLOYER_ACCT_NUM   VARCHAR2(50 BYTE)             NULL,
  PERSONAL_COVERAGE   NUMBER(10)                    NULL,
  OFF_PARTNER_CODE    NUMBER(10)                    NULL,
  OCCURED_IN_JURIS    NUMBER(10)                    NULL,
  BUSINESS_PURPOSES   NUMBER(10)                    NULL,
  REG_OCCUP_CODE      NUMBER(10)                    NULL,
  LT_DUTY_WORK_CODE   NUMBER(10)                    NULL,
  LIGHT_DUTY_CODE     NUMBER(10)                    NULL,
  PAY_ON_DISABILTY    NUMBER(10)                    NULL,
  DISABILTY_PAY_AMT   VARCHAR2(50 BYTE)             NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  AB_LAST_DAY         NUMBER(10)                    NULL,
  LAST_DAY_SCHEDULED  VARCHAR2(14 BYTE)             NULL,
  MONTHS_PER_YEAR     VARCHAR2(14 BYTE)             NULL,
  WORKER_TYPE         NUMBER(10)                    NULL,
  OTHER_EMPLOYE_TYPE  VARCHAR2(50 BYTE)             NULL,
  TAXABLE_BENEFITS    VARCHAR2(50 BYTE)             NULL,
  GROS_EARNINGS       VARCHAR2(50 BYTE)             NULL,
  GROS_EARNINGS_FROM  VARCHAR2(50 BYTE)             NULL,
  GROS_EARNINGS_TO    VARCHAR2(50 BYTE)             NULL,
  MISSED_TIME         NUMBER(10)                    NULL,
  MISSED_TIME_DAYS    VARCHAR2(50 BYTE)             NULL,
  MISSED_TIME_REASON  VARCHAR2(50 BYTE)             NULL,
  REPEAT_WORK_SCHED   NUMBER(10)                    NULL,
  DISABILT_PAY_AMT    NUMBER                        NULL,
  LAST_DAY_SCHE_DATE  VARCHAR2(14 BYTE)             NULL,
  MONTHS_PR_YEAR_NUM  NUMBER                        NULL,
  GROS_EARNINGS_AMT   NUMBER                        NULL,
  GROS_ENGS_FR_DATE   VARCHAR2(20 BYTE)             NULL,
  GROS_ENGS_TO_DATE   VARCHAR2(20 BYTE)             NULL,
  MISSED_TIME_DAYS_N  NUMBER(10)                    NULL,
  LEFT_WORK_TIME      VARCHAR2(6 BYTE)              NULL,
  BODY_SIDE_CODE      NUMBER(10)                    NULL,
  FIRST_MISS_DATE     VARCHAR2(14 BYTE)             NULL,
  MISS_WORK_TIME      VARCHAR2(6 BYTE)              NULL,
  RETURN_DATE         VARCHAR2(14 BYTE)             NULL,
  RETURN_TIME         VARCHAR2(6 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_AK_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  MACH_PART_TEXT     VARCHAR2(50 BYTE)              NULL,
  DOUBT_CLAIM_MEMO   CLOB                           NULL,
  OTHER_PERS_TEXT    VARCHAR2(20 BYTE)              NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  UNEM_NUM_TEXT      VARCHAR2(50 BYTE)              NULL,
  BODY_PART_LR_CODE  NUMBER(10)                     NULL,
  SELF_INSUR_CODE    NUMBER(10)                     NULL,
  OTHER_PERSON_TEXT  VARCHAR2(20 BYTE)              NULL,
  LEFT_WORK_TIME     VARCHAR2(6 BYTE)               NULL,
  FAILURE_OF_CODE    NUMBER(10)                     NULL,
  RTW_CODE           NUMBER(10)                     NULL,
  PERS_ADDR_TEXT     VARCHAR2(75 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_AL_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  OCCUP_START_DATE    VARCHAR2(8 BYTE)              NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  PRODUCTS_TEXT       VARCHAR2(50 BYTE)             NULL,
  RET_OCCUPAT_TEXT    VARCHAR2(20 BYTE)             NULL,
  WORK_PROCES_TEXT    VARCHAR2(50 BYTE)             NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  CARRIER_ID_TEXT     VARCHAR2(25 BYTE)             NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  UNEM_NUM_TEXT       VARCHAR2(50 BYTE)             NULL,
  SERV_COMPANY_TEXT   VARCHAR2(50 BYTE)             NULL,
  WC_PROVIDR_CODE     NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  RTW_CODE            NUMBER(10)                    NULL,
  TF_LAST_DATE        VARCHAR2(8 BYTE)              NULL,
  AL_MARITAL_CODE     NUMBER(10)                    NULL,
  CV_NUM_TEXT         VARCHAR2(50 BYTE)             NULL,
  RETURN_TOWORK_DATE  VARCHAR2(8 BYTE)              NULL,
  COMP_DENY_CODE      NUMBER(10)                    NULL,
  COMP_PAY_DATE       VARCHAR2(8 BYTE)              NULL,
  COUNTY_TEXT         VARCHAR2(50 BYTE)             NULL,
  MMI_TEXT            VARCHAR2(50 BYTE)             NULL,
  PERIODIC_PAY_MEMO   CLOB                          NULL,
  OMB_CODE            NUMBER(10)                    NULL,
  COMP_DENY_TEXT      VARCHAR2(50 BYTE)             NULL,
  CLAIM_SUM_CODE      NUMBER(10)                    NULL,
  NON_PAY_CODE        NUMBER(10)                    NULL,
  PROLONG_INV_TEXT    VARCHAR2(50 BYTE)             NULL,
  SUP_REP_CODE        NUMBER(10)                    NULL,
  DIS_CODE            NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_AR_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  PURPOSE_TEXT      VARCHAR2(25 BYTE)               NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  WORK_PROC_TEXT    VARCHAR2(100 BYTE)              NULL,
  WORK_STATUS_TYPE  NUMBER(10)                      NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_AZ_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  PAY_IN_KIND_AMT    NUMBER                         NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  SEASON_LGTH_NUM    NUMBER                         NULL,
  ON_OT_CODE         NUMBER(10)                     NULL,
  PERM_EMP_CODE      NUMBER(10)                     NULL,
  MOS_AVAIL_NUM      NUMBER                         NULL,
  PREV_WAGES_AMT     NUMBER                         NULL,
  NORM_OT_HRS_NUM    NUMBER                         NULL,
  MO12S_WAGES_AMT    NUMBER                         NULL,
  WAGE_INCR_DATE     VARCHAR2(8 BYTE)               NULL,
  WAGE_PRIOR_AMT     NUMBER                         NULL,
  WAGE_AFTER_AMT     NUMBER                         NULL,
  EARN_TO_INJ_AMT    NUMBER                         NULL,
  DEPT_FLOOR_TEXT    VARCHAR2(25 BYTE)              NULL,
  DOI_PAID_AMT       NUMBER                         NULL,
  DOUBT_CLAIM_MEMO   CLOB                           NULL,
  OTHER_PERS_TEXT    VARCHAR2(20 BYTE)              NULL,
  PERS_ADDR_TEXT     VARCHAR2(35 BYTE)              NULL,
  DEPT_NUM_TEXT      VARCHAR2(50 BYTE)              NULL,
  MOS12_WAGES_AMT    NUMBER                         NULL,
  OVERTIME_RATE_AMT  NUMBER                         NULL,
  COMP_DAYS_NUM      NUMBER                         NULL,
  SHIFT_END_TIME     VARCHAR2(50 BYTE)              NULL,
  DURING_EMP_CODE    NUMBER(10)                     NULL,
  BENEFIT_CODE       NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_BC_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  REGISTRATION_NUM    VARCHAR2(50 BYTE)             NULL,
  LOCATION_TEXT       VARCHAR2(50 BYTE)             NULL,
  CLASS_UNIT_NUMBER   VARCHAR2(50 BYTE)             NULL,
  CODED_BY_TEXT       VARCHAR2(50 BYTE)             NULL,
  EMPLOYEE_WEIGHT     VARCHAR2(50 BYTE)             NULL,
  EMPLOYEE_HEIGHT     VARCHAR2(50 BYTE)             NULL,
  HEALTH_CARE_PROV    VARCHAR2(50 BYTE)             NULL,
  REPED_FIRSTAID_CHK  NUMBER(10)                    NULL,
  REPED_SUPERR_CHK    NUMBER(10)                    NULL,
  CLAIM_ACCEPT        NUMBER(10)                    NULL,
  CLAIM_DENIAL_TEXT   VARCHAR2(50 BYTE)             NULL,
  PREVIUS_INJURY      NUMBER(10)                    NULL,
  PREVIUS_INJURY_TXT  VARCHAR2(50 BYTE)             NULL,
  WORKR_DISABILITY    NUMBER(10)                    NULL,
  WORKR_DISABILY_TXT  VARCHAR2(50 BYTE)             NULL,
  ANY_WITNESSES       NUMBER(10)                    NULL,
  WITNESS_CONFIRM     NUMBER(10)                    NULL,
  WORKER_STATUS       NUMBER(10)                    NULL,
  OCCUP_START_DATE    VARCHAR2(8 BYTE)              NULL,
  BUSINES_ACTION      NUMBER(10)                    NULL,
  BUSINES_ACTION_TXT  VARCHAR2(50 BYTE)             NULL,
  REGULAR_ACTION      NUMBER(10)                    NULL,
  REGULAR_ACTION_TXT  VARCHAR2(50 BYTE)             NULL,
  SUB_CONTRACTOR      NUMBER(10)                    NULL,
  SUB_CONTRACTOR_TXT  VARCHAR2(50 BYTE)             NULL,
  IS_RELATIVE         NUMBER(10)                    NULL,
  RELATION_TEXT       VARCHAR2(50 BYTE)             NULL,
  OTHER_PERSON_CODE   NUMBER(10)                    NULL,
  OTHER_PERSON_TEXT   VARCHAR2(50 BYTE)             NULL,
  LIGHT_DUTY_CODE     NUMBER(10)                    NULL,
  EARNINGS_3_MON      VARCHAR2(50 BYTE)             NULL,
  EARNINGS_1_YEAR     VARCHAR2(50 BYTE)             NULL,
  PAYT_HOLIDAY        NUMBER(10)                    NULL,
  PAYT_RENTAL         NUMBER(10)                    NULL,
  PAYT_DIFERENT       NUMBER(10)                    NULL,
  PAYT_SHIFT_PREM     NUMBER(10)                    NULL,
  PAYT_LODGING        NUMBER(10)                    NULL,
  PAYT_VEH_ALL        NUMBER(10)                    NULL,
  PAYT_EQUIPMENT      NUMBER(10)                    NULL,
  PAYT_OTHER          NUMBER(10)                    NULL,
  EMPLOYER_PAYT       NUMBER(10)                    NULL,
  PAYMENT_TEXT        VARCHAR2(50 BYTE)             NULL,
  LAST_DAY_WAGES      VARCHAR2(50 BYTE)             NULL,
  SHIFT_ROT_CODE      NUMBER(10)                    NULL,
  WORKDAY_START_DATE  VARCHAR2(50 BYTE)             NULL,
  WORKDAY_START_TIME  VARCHAR2(50 BYTE)             NULL,
  WORKDAY_END_DATE    VARCHAR2(50 BYTE)             NULL,
  WORKDAY_END_TIME    VARCHAR2(50 BYTE)             NULL,
  LEFT_WORK_TIME      VARCHAR2(6 BYTE)              NULL,
  BODY_SIDE_CODE      NUMBER(10)                    NULL,
  OFF_BEYOND_CODE     NUMBER(10)                    NULL,
  LAST_WORK_DATE      VARCHAR2(14 BYTE)             NULL,
  RETURN_WORK_TIME    VARCHAR2(6 BYTE)              NULL,
  PROVIDE_DETAILS     VARCHAR2(50 BYTE)             NULL,
  SHIFT_ROT_DETAILS   VARCHAR2(50 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_CA_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  OSHA_CASE_TEXT     VARCHAR2(50 BYTE)              NULL,
  PAY_IN_KIND_AMT    NUMBER                         NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  WAGE_CLASS_TEXT    VARCHAR2(20 BYTE)              NULL,
  OTHER_INJUR_CODE   NUMBER(10)                     NULL,
  CLAIM_FORM_DATE    VARCHAR2(8 BYTE)               NULL,
  UNEM_NUM_TEXT      VARCHAR2(20 BYTE)              NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  EMPL_TYPE_CODE     NUMBER(10)                     NULL,
  SALARY_BENG_CONTD  NUMBER(10)                     NULL,
  OTHER_GOVT_TEXT    VARCHAR2(50 BYTE)              NULL,
  WORK_STATUS_CODE   NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  UNABLE_WORK_CODE   NUMBER(10)                     NULL,
  OFF_WORK_CODE      NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_CO_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  EMP_ETHNIC_CODE     NUMBER(10)                    NULL,
  OCCUP_START_DATE    VARCHAR2(8 BYTE)              NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  COMMISSION_AMT      NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  YRS_EDU_NUM         NUMBER                        NULL,
  RELATIVE_TEXT       VARCHAR2(50 BYTE)             NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  INJURED_OCC_CODE    NUMBER(10)                    NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  EMPLOYEES_NUM       NUMBER                        NULL,
  RECV_OVERTIME_PAY   NUMBER(10)                    NULL,
  PAY_OVERTIME_AMT    NUMBER                        NULL,
  RECV_COMMSIONS_PAY  NUMBER(10)                    NULL,
  PAY_COMMSIONS_AMT   NUMBER                        NULL,
  RECV_PIECEWORK_PAY  NUMBER(10)                    NULL,
  PAY_PIECEWORK_AMT   NUMBER                        NULL,
  TIPS_CODE           NUMBER(10)                    NULL,
  MEALS_CODE          NUMBER(10)                    NULL,
  MEALS_BENE_CONT     NUMBER(10)                    NULL,
  MEALS_AMT           NUMBER                        NULL,
  LODGE_ROOM_CODE     NUMBER(10)                    NULL,
  LOGD_ROM_BENE_CONT  NUMBER(10)                    NULL,
  LODGE_AMT           NUMBER                        NULL,
  HLTH_INS_CODE       NUMBER(10)                    NULL,
  HLTH_INS_BENE_CONT  NUMBER(10)                    NULL,
  HLTH_INS_AMT        NUMBER                        NULL,
  EMP_HLTH_CONT_AMT   NUMBER                        NULL,
  CO_INJRY_CUSE_CODE  NUMBER(10)                    NULL,
  ASSIGN_JOB_TEXT     VARCHAR2(50 BYTE)             NULL,
  EMPLOYER_REP_TEXT   VARCHAR2(50 BYTE)             NULL,
  INTOCX_CODE         NUMBER(10)                    NULL,
  NOT_APPL_CODE       NUMBER(10)                    NULL,
  IN_AWW_HEALTH_CODE  NUMBER(10)                    NULL,
  IN_AWW_MEALS_CODE   NUMBER(10)                    NULL,
  IN_AWW_ROOM_CODE    NUMBER(10)                    NULL,
  IN_AWW_TIPS_CODE    NUMBER(10)                    NULL,
  WAGES_CON_PER_CODE  NUMBER(10)                    NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_CT_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  REGIS_NUM_TEXT     VARCHAR2(50 BYTE)              NULL,
  EXT_INSUR_TEXT     VARCHAR2(50 BYTE)              NULL,
  LAST_EXPOSU_DATE   VARCHAR2(8 BYTE)               NULL,
  DIAGNOSIS_DATE     VARCHAR2(8 BYTE)               NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  OCCUP_START_DATE   VARCHAR2(50 BYTE)              NULL,
  SELF_INSUR_CODE    NUMBER(10)                     NULL,
  WORK_PROCES_TEXT   VARCHAR2(50 BYTE)              NULL,
  JURIS_HIRED_CODE   NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  EMPLYR_PHONE_TEXT  VARCHAR2(15 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_DC_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  FORM7_CODE          NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  RTW_TIME            VARCHAR2(6 BYTE)              NULL,
  DISABILITY_TIME     VARCHAR2(6 BYTE)              NULL,
  HIRE_IN_STATE_CODE  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_DE_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  LAST_WAGE_DATE    VARCHAR2(8 BYTE)                NULL,
  SAME_WAGE_CODE    NUMBER(10)                      NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  OSHA_CASE_TEXT    VARCHAR2(50 BYTE)               NULL,
  WORK_PROC_TEXT    VARCHAR2(100 BYTE)              NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL,
  UNEM_NUM_TEXT     VARCHAR2(25 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_FD_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  FED_REPORTING_ACT   NUMBER(10)                    NULL,
  LONGSHR_ACT_LOCAT   NUMBER(10)                    NULL,
  STOPPED_WORK_IMMED  NUMBER(10)                    NULL,
  KNOWLDG_GAINED_BY   VARCHAR2(50 BYTE)             NULL,
  PLACE_OCCURRED      VARCHAR2(50 BYTE)             NULL,
  MED_CARE_AUTHOR     NUMBER(10)                    NULL,
  MED_CARE_DATE       VARCHAR2(50 BYTE)             NULL,
  EMP_CHOSEN_PHYSIN   NUMBER(10)                    NULL,
  INS_CARIER_NOTIFID  NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  WAS_LS_ONE_CODE     NUMBER(10)                    NULL,
  NO_MED_TREAT_TEXT   VARCHAR2(254 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_FL_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  PHYS_AUTH_CODE      NUMBER(10)                    NULL,
  PAY_WAGES_CODE      NUMBER(10)                    NULL,
  LAST_WAGE_DATE      VARCHAR2(8 BYTE)              NULL,
  AGREE_EMP_CODE      NUMBER(10)                    NULL,
  EMP_RISK_CODE       NUMBER(10)                    NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  FLDISABILITY1_CODE  NUMBER(10)                    NULL,
  FLDISABILITY2_CODE  NUMBER(10)                    NULL,
  FL_BENEFIT1_CODE    NUMBER(10)                    NULL,
  FL_BENEFIT2_CODE    NUMBER(10)                    NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  CERT_NUMBER_TEXT    VARCHAR2(50 BYTE)             NULL,
  LOST_TIME_CODE      NUMBER(10)                    NULL,
  DENIED_BEN_MEMO     CLOB                          NULL,
  RESCINDED_DATE      VARCHAR2(50 BYTE)             NULL,
  CAUSE_MEMO          CLOB                          NULL,
  CARRIER_ID_TEXT     VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  NEW_SS_TEXT         VARCHAR2(50 BYTE)             NULL,
  ACCIDENT_DATE       VARCHAR2(50 BYTE)             NULL,
  NEW_CARRIER_TEXT    VARCHAR2(50 BYTE)             NULL,
  NEW_NAME_TEXT       VARCHAR2(50 BYTE)             NULL,
  REMARKS_MEMO        CLOB                          NULL,
  FL_SUSPENSION_CODE  NUMBER(10)                    NULL,
  SUSPENDED_DATE      VARCHAR2(50 BYTE)             NULL,
  INDEMNITY_DATE      VARCHAR2(50 BYTE)             NULL,
  REINSTATED_DATE     VARCHAR2(50 BYTE)             NULL,
  MMI_DATE            VARCHAR2(50 BYTE)             NULL,
  ACT_RET_WK_DATE     VARCHAR2(50 BYTE)             NULL,
  SETTLEMENT_DATE     VARCHAR2(50 BYTE)             NULL,
  FIRST_EFF_DATE      VARCHAR2(50 BYTE)             NULL,
  FIRST_END_DATE      VARCHAR2(50 BYTE)             NULL,
  SECOND_END_DATE     VARCHAR2(50 BYTE)             NULL,
  SECOND_EFF_DATE     VARCHAR2(50 BYTE)             NULL,
  RETROACTIVE_CODE    NUMBER(10)                    NULL,
  FIRST_WK_AMT        NUMBER                        NULL,
  SECOND_WK_AMT       NUMBER                        NULL,
  CARRIER_FILE_TEXT   VARCHAR2(50 BYTE)             NULL,
  RET_EFF_DATE        VARCHAR2(50 BYTE)             NULL,
  PI_RATE_TEXT        VARCHAR2(50 BYTE)             NULL,
  PREV_AWW_AMT        NUMBER                        NULL,
  PREV_COMP_AMT       NUMBER                        NULL,
  AMENDED_AWW_AMT     NUMBER                        NULL,
  AMENDED_COMP_AMT    NUMBER                        NULL,
  PT_SUPP_AMT         NUMBER                        NULL,
  PER_IM_WK_AMT       NUMBER                        NULL,
  IMP_INCOME_AMT      NUMBER                        NULL,
  SERVICE_TPA_TEXT    VARCHAR2(50 BYTE)             NULL,
  TOT_ENT_NUM         NUMBER                        NULL,
  PT_ACC_DATE         VARCHAR2(50 BYTE)             NULL,
  PT_SUPP_DATE        VARCHAR2(50 BYTE)             NULL,
  PER_IMP_PY_DATE     VARCHAR2(50 BYTE)             NULL,
  IMP_INC_DATE        VARCHAR2(50 BYTE)             NULL,
  CC_TEXT             VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_STREET   VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_CITY     VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_ST_CODE  NUMBER(10)                    NULL,
  ADDR_DIFER_ZIP      VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_LOCAT    VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_GA_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  DISABILITY_CODE     NUMBER(10)                    NULL,
  PPD_PERCENT_NUM     NUMBER                        NULL,
  PPD_WEEKS_NUM       NUMBER                        NULL,
  PAY_START_DATE      VARCHAR2(8 BYTE)              NULL,
  PENALTY_PAY_CODE    NUMBER(10)                    NULL,
  PENALTY_PCT_NUM     NUMBER                        NULL,
  PENALTY_AMT         NUMBER                        NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  OFF_PARTNER_CODE    NUMBER(10)                    NULL,
  PRODUCTS_TEXT       VARCHAR2(50 BYTE)             NULL,
  DOUBT_CLAIM_MEMO    CLOB                          NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  COMMISSION_AMT      NUMBER                        NULL,
  AVG_WAGE_AMT        NUMBER                        NULL,
  WKLY_BENE_AMT       NUMBER                        NULL,
  COMP_PAID_AMT       NUMBER                        NULL,
  MED_ONLY_CODE       NUMBER(10)                    NULL,
  OSHA_TEXT           VARCHAR2(20 BYTE)             NULL,
  MCO_CODE            NUMBER(10)                    NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  ADDR_DIFER_STREET   VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_CITY     VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_ST_CODE  NUMBER(10)                    NULL,
  ADDR_DIFER_ZIP      VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  WORK_NEXTDAY_CODE   NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_GU_SUPP
(
  CLAIM_ID                      NUMBER(10)          NULL,
  EMP_LOST_TIME                 VARCHAR2(6 BYTE)     NULL,
  EMP_RTW_TIME                  VARCHAR2(6 BYTE)     NULL,
  DATE_PAY_STOPPED              VARCHAR2(8 BYTE)     NULL,
  HOUR_PAY_STOPPED              VARCHAR2(6 BYTE)     NULL,
  EMPLOYEE_WAGES                NUMBER              NULL,
  ANTHR_PERSON_ACC              NUMBER(10)          NULL,
  MEDICAL_ATTN_ATHRZD           NUMBER(10)          NULL,
  DATE_AUTHORIZED               VARCHAR2(8 BYTE)     NULL,
  CARRIER_NOTIFIED              NUMBER(10)          NULL,
  DATE_NOTIFIED                 VARCHAR2(8 BYTE)     NULL,
  NAME_TRTNG_FACILITY           VARCHAR2(50 BYTE)     NULL,
  NAME_PERSON_COMPLTNG_REPORT   VARCHAR2(50 BYTE)     NULL,
  TITLE_PERSON_COMPLTNG_REPORT  VARCHAR2(50 BYTE)     NULL,
  DATE_REPORT                   VARCHAR2(8 BYTE)     NULL,
  ETHNICITY                     NUMBER(10)          NULL,
  OTHER_ETHNICITY               VARCHAR2(50 BYTE)     NULL,
  CITIZENSHIP                   NUMBER(10)          NULL,
  OTHER_CITIZENSHIP             VARCHAR2(50 BYTE)     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_HI_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  PAY_CODE_TEXT     VARCHAR2(20 BYTE)               NULL,
  DOL_NUMBER_TEXT   VARCHAR2(20 BYTE)               NULL,
  DOUBT_CLAIM_MEMO  CLOB                            NULL,
  MONTHLY__AMT      NUMBER                          NULL,
  MEALS_LODG_CODE   NUMBER(10)                      NULL,
  MEDICAL_AMT       NUMBER                          NULL,
  CARRIER_ID_TEXT   VARCHAR2(25 BYTE)               NULL,
  ADJUSTING_TEXT    VARCHAR2(35 BYTE)               NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_IA_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  LAST_WAGE_DATE      VARCHAR2(8 BYTE)              NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  PERM_DIS_CODE       NUMBER(10)                    NULL,
  PAID_OTHER_CODE     NUMBER(10)                    NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  OSHA_CASE_TEXT      VARCHAR2(50 BYTE)             NULL,
  OCCUP_START_DATE    VARCHAR2(50 BYTE)             NULL,
  EMPL_TYPE_CODE      NUMBER(10)                    NULL,
  TAX_FILING_CODE     NUMBER(10)                    NULL,
  EMPL_UI_TEXT        VARCHAR2(20 BYTE)             NULL,
  SELF_INS_TEXT       VARCHAR2(20 BYTE)             NULL,
  EDUCATION_NUM       NUMBER                        NULL,
  MANUAL_CLASS_TEXT   VARCHAR2(30 BYTE)             NULL,
  DISC_FRINGE_AMT     NUMBER                        NULL,
  EMP_EXEMPTION_NUM   NUMBER                        NULL,
  EMP_EXEMPTION_CODE  NUMBER(10)                    NULL,
  PRE_EXIST_CODE      NUMBER(10)                    NULL,
  MNG_CARE_TEXT       VARCHAR2(30 BYTE)             NULL,
  ICD_DIAG_TEXT       VARCHAR2(30 BYTE)             NULL,
  REL_MED_CODE        NUMBER(10)                    NULL,
  REL_SSN_CODE        NUMBER(10)                    NULL,
  SALARY_BENG_CONTD   NUMBER(10)                    NULL,
  PREP_COMP_NAME      VARCHAR2(50 BYTE)             NULL,
  PREMISES_CODE       NUMBER(10)                    NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  LOC_NARRAT_MEMO     CLOB                          NULL,
  INSRD_RPT_TEXT      VARCHAR2(50 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_ID_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  OCCUP_START_DATE  VARCHAR2(8 BYTE)                NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  RTW_WAGE_AMT      NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  MACH_PART_TEXT    VARCHAR2(50 BYTE)               NULL,
  RELATIVE_TEXT     VARCHAR2(50 BYTE)               NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  WORK_PROCES_TEXT  VARCHAR2(50 BYTE)               NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_IL_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  PRODUCTS_TEXT     VARCHAR2(50 BYTE)               NULL,
  UNEM_NUM_TEXT     VARCHAR2(20 BYTE)               NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  EMP_AT_LOC_NUM    NUMBER                          NULL,
  HANDBOOK_CODE     NUMBER(10)                      NULL,
  HAZ_COND_MEMO     CLOB                            NULL,
  UNSAFE_ACT_MEMO   CLOB                            NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  WORK_PROC_TEXT    VARCHAR2(100 BYTE)              NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  WORK_STATUS_TYPE  NUMBER(10)                      NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_IN_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  PURPOSE_TEXT      VARCHAR2(25 BYTE)               NULL,
  WORK_PROCES_TEXT  VARCHAR2(50 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL,
  INSRD_RPT_TEXT    VARCHAR2(50 BYTE)               NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_KS_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  OSHA_CASE_TEXT    VARCHAR2(50 BYTE)               NULL,
  MORE_MEDS_CODE    NUMBER(10)                      NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  RELATIVE_TEXT     VARCHAR2(50 BYTE)               NULL,
  RTW_FULL_CODE     NUMBER(10)                      NULL,
  RTW_LIGHT_CODE    NUMBER(10)                      NULL,
  HOSPITAL_DATE     VARCHAR2(8 BYTE)                NULL,
  COMP_AMT_AMT      NUMBER                          NULL,
  COMP_CODE         NUMBER(10)                      NULL,
  COMP_PAY_DATE     VARCHAR2(8 BYTE)                NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_KY_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  OSHA_CASE_TEXT     VARCHAR2(50 BYTE)              NULL,
  LAST_WAGE_DATE     VARCHAR2(8 BYTE)               NULL,
  PAY_IN_KIND_AMT    NUMBER                         NULL,
  COMMISSION_AMT     NUMBER                         NULL,
  COMM_HR_NUM        NUMBER                         NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  RELATIVE_TEXT      VARCHAR2(50 BYTE)              NULL,
  DEPT_FLOOR_TEXT    VARCHAR2(25 BYTE)              NULL,
  UNEMPLY_INS_TEXT   VARCHAR2(20 BYTE)              NULL,
  SELF_INSUR_CODE    NUMBER(10)                     NULL,
  REASON_TEXT        VARCHAR2(35 BYTE)              NULL,
  REST_XFER_CODE     NUMBER(10)                     NULL,
  SHIFT_END_TIME     VARCHAR2(50 BYTE)              NULL,
  WORK_PROC_TEXT     VARCHAR2(100 BYTE)             NULL,
  PURPOSE_TEXT       VARCHAR2(20 BYTE)              NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  JURIS_HIRED_CODE   NUMBER(10)                     NULL,
  SALARY_CONT_CODE   NUMBER(10)                     NULL,
  WORK_STATUS_CODE   NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  LOCATION_NUM_TEXT  VARCHAR2(25 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_LA_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  EMP_RACE_CODE      NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  MECH_DEFECT_CODE   NUMBER(10)                     NULL,
  UNSAFE_ACT_CODE    NUMBER(10)                     NULL,
  AMPUTATION_CODE    NUMBER(10)                     NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  DIAGNOSIS_DATE     VARCHAR2(8 BYTE)               NULL,
  UNEM_NUM_TEXT      VARCHAR2(20 BYTE)              NULL,
  REASON_TEXT        VARCHAR2(35 BYTE)              NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  LAST_WAGE_DATE     VARCHAR2(50 BYTE)              NULL,
  SAME_WAGE_CODE     NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  R_SEV_DAY_CODE     NUMBER(10)                     NULL,
  R_POS_DIS_CODE     NUMBER(10)                     NULL,
  R_LS_COMPSET_CODE  NUMBER(10)                     NULL,
  R_MED_ONLY_CODE    NUMBER(10)                     NULL,
  R_OTHER_CODE       NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MA_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  RPT_WRK_REL_DATE    VARCHAR2(8 BYTE)              NULL,
  SAME_OCCUP_CODE     NUMBER(10)                    NULL,
  DEPT_FLOOR_TEXT     VARCHAR2(25 BYTE)             NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  SELF_INS_TEXT       VARCHAR2(20 BYTE)             NULL,
  NATURE_BUS_CODE     NUMBER(10)                    NULL,
  REP_POS_TEXT        VARCHAR2(30 BYTE)             NULL,
  INDUSTRY_CODE       NUMBER(10)                    NULL,
  INJU_ILLN_A_CODE    NUMBER(10)                    NULL,
  BODY_PART_A_CODE    NUMBER(10)                    NULL,
  INJU_ILLN_B_CODE    NUMBER(10)                    NULL,
  BODY_PART_B_CODE    NUMBER(10)                    NULL,
  INJU_ILLN_C_CODE    NUMBER(10)                    NULL,
  BODY_PART_C_CODE    NUMBER(10)                    NULL,
  NAT_BUS_OTH_TEXT    VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  FIFTH_DISABIL_DATE  VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MB_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  FIRM_NUM             VARCHAR2(50 BYTE)            NULL,
  USUAL_EMP            NUMBER(10)                   NULL,
  OUT_LONGR_THAN_SIX   NUMBER(10)                   NULL,
  LAST_DATE            VARCHAR2(8 BYTE)             NULL,
  HOUR_AFTER_ACCIDENT  VARCHAR2(6 BYTE)             NULL,
  TIME_RTW             VARCHAR2(6 BYTE)             NULL,
  CONT_PAY_DURING_LT   NUMBER(10)                   NULL,
  LAST_DAY_PAY         NUMBER                       NULL,
  LAST_YEAR_PAY        NUMBER                       NULL,
  LESS_YEAR_PAY        NUMBER                       NULL,
  TWELVE_MONTHS_PAY    NUMBER                       NULL,
  ALT_DUTIES           NUMBER(10)                   NULL,
  NOT_EMPLOYED         NUMBER(10)                   NULL,
  NOT_EMPLOYED_ADDR    CLOB                         NULL,
  PARTNER              NUMBER(10)                   NULL,
  RELATED_EMPLOYER     NUMBER(10)                   NULL,
  SPOUSE_OF_EMPLOYER   NUMBER(10)                   NULL,
  SAME_HOUSE           NUMBER(10)                   NULL,
  MARRIED_CODE         NUMBER(10)                   NULL,
  SUBCONTRACTOR_CODE   NUMBER(10)                   NULL,
  SUBCONTRACTOR_TYPE   NUMBER(10)                   NULL,
  OWNER_OPERATOR       NUMBER(10)                   NULL,
  OWNER_OP_TYPE        NUMBER(10)                   NULL,
  WCB_COVERAGE         NUMBER(10)                   NULL,
  WCB_REGISTERED       NUMBER(10)                   NULL,
  WORK_IN_PARTNER      NUMBER(10)                   NULL,
  EMPLOY_OTHERS        NUMBER(10)                   NULL,
  CON_MATERIAL_EQUIP   NUMBER(10)                   NULL,
  CON_MATERIAL_SUPPLY  VARCHAR2(50 BYTE)            NULL,
  LOG_MATERIAL_EQUIP   NUMBER(10)                   NULL,
  LOG_MATERIAL_SUPPLY  VARCHAR2(50 BYTE)            NULL,
  FIRMS_TIMBER_SALE    NUMBER(10)                   NULL,
  WHOSE_SALE           VARCHAR2(50 BYTE)            NULL,
  GROSS_VEH_WEIGHT     VARCHAR2(50 BYTE)            NULL,
  SIXTEEN_KM           NUMBER(10)                   NULL,
  LONG_DISTANCE        NUMBER(10)                   NULL,
  PROVIDE_VEH          NUMBER(10)                   NULL,
  HOW_MANY_VEH         VARCHAR2(50 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MD_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  OCCUP_START_DATE  VARCHAR2(8 BYTE)                NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  PAID_OTHER_CODE   NUMBER(10)                      NULL,
  OFF_PARTNER_CODE  NUMBER(10)                      NULL,
  PRODUCTS_TEXT     VARCHAR2(50 BYTE)               NULL,
  PERS_NOTIF_EID    NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  WORK_PROC_TEXT    VARCHAR2(100 BYTE)              NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  WORK_STATUS_TYPE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_ME_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  OCCUP_START_DATE     VARCHAR2(8 BYTE)             NULL,
  DOI_PAID_CODE        NUMBER(10)                   NULL,
  CARRIER_NO_TEXT      VARCHAR2(50 BYTE)            NULL,
  FULL_SALARY_CODE     NUMBER(10)                   NULL,
  LAST_EXPOSU_DATE     VARCHAR2(8 BYTE)             NULL,
  DIAGNOSIS_DATE       VARCHAR2(8 BYTE)             NULL,
  WRK_ANOTHER_CODE     NUMBER(10)                   NULL,
  UNEM_NUM_TEXT        VARCHAR2(20 BYTE)            NULL,
  ADDR_DIFER_MEMO      CLOB                         NULL,
  OCCUP_DISEASE_CODE   NUMBER(10)                   NULL,
  CORR_TO_REPRT_CODE   NUMBER(10)                   NULL,
  CORRECTION_DATE      VARCHAR2(8 BYTE)             NULL,
  CORRECT_SENT_DATE    VARCHAR2(8 BYTE)             NULL,
  WORK_ACTIVE_CODE     NUMBER(10)                   NULL,
  ME_PREPARER_CODE     NUMBER(10)                   NULL,
  OTHER_EMPLOYR_MEMO   CLOB                         NULL,
  NOTIFIED_TPA_DATE    VARCHAR2(8 BYTE)             NULL,
  FROI_SENT_DATE       VARCHAR2(8 BYTE)             NULL,
  TAX_FILING_CODE      NUMBER(10)                   NULL,
  FRINGE_BENEFIT_CODE  NUMBER(10)                   NULL,
  LOST_EARNINGS_CODE   NUMBER(10)                   NULL,
  DISABILITY_DATE      VARCHAR2(8 BYTE)             NULL,
  EMPR_NTDINCAP_DATE   VARCHAR2(8 BYTE)             NULL,
  MEDICL_HEALTH_CODE   NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MI_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  WK_AMT_2D           NUMBER                        NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  VOLUNTEER_CODE      NUMBER(10)                    NULL,
  VOC_HAND_CODE       NUMBER(10)                    NULL,
  TAXFILING_CODE      NUMBER(10)                    NULL,
  AWW_WEEKS_NUM       NUMBER                        NULL,
  FRINGES_VAL_AMT     NUMBER                        NULL,
  INJ_LOC_COD_TEXT    VARCHAR2(50 BYTE)             NULL,
  MAILING_LOC_TEXT    VARCHAR2(50 BYTE)             NULL,
  EMP_NAME_TEXT_2D    VARCHAR2(50 BYTE)             NULL,
  EMP_ADDR_TEXT_2D    VARCHAR2(50 BYTE)             NULL,
  EMP_CITY_TEXT_2D    VARCHAR2(50 BYTE)             NULL,
  EMP_STAT_STATE      NUMBER(10)                    NULL,
  EMP_ZIP_TEXT_2D     VARCHAR2(15 BYTE)             NULL,
  EMP_AWW_AMT_2D      NUMBER                        NULL,
  EMPNO_AWW_NUM_2D    NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  CLIENT_BUS_TEXT     VARCHAR2(25 BYTE)             NULL,
  CLIENT_ADDR_TEXT    VARCHAR2(30 BYTE)             NULL,
  MI_DISABILITY_CODE  NUMBER(10)                    NULL,
  REASON_CODE         NUMBER(10)                    NULL,
  MI_ADJ1_CODE        NUMBER(10)                    NULL,
  ADJ1_AMT            NUMBER                        NULL,
  MI_ADJ2_CODE        NUMBER(10)                    NULL,
  ADJ2_AMT            NUMBER                        NULL,
  MI_ADJ3_CODE        NUMBER(10)                    NULL,
  ADJ3_AMT            NUMBER                        NULL,
  MI_ADJ4_CODE        NUMBER(10)                    NULL,
  ADJ4_AMT            NUMBER                        NULL,
  MI_ADJ5_CODE        NUMBER(10)                    NULL,
  ADJ5_AMT            NUMBER                        NULL,
  MI_ADJ6_CODE        NUMBER(10)                    NULL,
  ADJ6_AMT            NUMBER                        NULL,
  MI_ADJ7_CODE        NUMBER(10)                    NULL,
  ADJ7_AMT            NUMBER                        NULL,
  MI_ADJ8_CODE        NUMBER(10)                    NULL,
  ADJ8_AMT            NUMBER                        NULL,
  CONDITION_MEMO      CLOB                          NULL,
  OTHER_MEMO          CLOB                          NULL,
  FIRST_PAY_DATE      VARCHAR2(50 BYTE)             NULL,
  UN_START_DATE       VARCHAR2(50 BYTE)             NULL,
  UN_STOP_DATE        VARCHAR2(50 BYTE)             NULL,
  TPA_NAME_TEXT       VARCHAR2(50 BYTE)             NULL,
  TPA_ID_TEXT         VARCHAR2(50 BYTE)             NULL,
  NAIC_TEXT           VARCHAR2(50 BYTE)             NULL,
  ISSUE_ZIP_TEXT      VARCHAR2(50 BYTE)             NULL,
  EFF_LOSS_OF_DATE    VARCHAR2(50 BYTE)             NULL,
  ORDER_NUM_TEXT      VARCHAR2(50 BYTE)             NULL,
  AVG_WK_WAGE_AMT     NUMBER                        NULL,
  SEC_FRING_AMT       NUMBER                        NULL,
  WK_BASE_AMT         NUMBER                        NULL,
  MTH_OLD_BEN_AMT     NUMBER                        NULL,
  TOT_WK_UN_AMT       NUMBER                        NULL,
  MI_REIM1_CODE       NUMBER(10)                    NULL,
  REIM1_AMT           NUMBER                        NULL,
  MI_REIM2_CODE       NUMBER(10)                    NULL,
  REIM2_AMT           NUMBER                        NULL,
  MI_REIM3_CODE       NUMBER(10)                    NULL,
  REIM3_AMT           NUMBER                        NULL,
  MI_REIM4_CODE       NUMBER(10)                    NULL,
  REIM4_AMT           NUMBER                        NULL,
  BASIS_PAY1_CODE     NUMBER(10)                    NULL,
  BASIS_PAY2_CODE     NUMBER(10)                    NULL,
  MI_BEN1_CODE        NUMBER(10)                    NULL,
  MI_BEN2_CODE        NUMBER(10)                    NULL,
  TOT_WK1_AMT         NUMBER                        NULL,
  TOT_WK2_AMT         NUMBER                        NULL,
  TOT_PAY1_AMT        NUMBER                        NULL,
  TOT_PAY2_AMT        NUMBER                        NULL,
  YEAR_PAID1_TEXT     VARCHAR2(50 BYTE)             NULL,
  YEAR_PAID2_TEXT     VARCHAR2(50 BYTE)             NULL,
  SPEC_PAY1_CODE      NUMBER(10)                    NULL,
  SPEC_PAY2_CODE      NUMBER(10)                    NULL,
  TERM1_CODE          NUMBER(10)                    NULL,
  TERM2_CODE          NUMBER(10)                    NULL,
  PTD_FROM1_DATE      VARCHAR2(50 BYTE)             NULL,
  PTD_TO1_DATE        VARCHAR2(50 BYTE)             NULL,
  PTD_FROM2_DATE      VARCHAR2(50 BYTE)             NULL,
  PTD_TO2_DATE        VARCHAR2(50 BYTE)             NULL,
  PEN_WK_BEN_AMT      NUMBER                        NULL,
  PEN_80_TAX_AMT      NUMBER                        NULL,
  PEN_100_TAX_AMT     NUMBER                        NULL,
  PEN_FICA_AMT        NUMBER                        NULL,
  PEN_ST_AMT          NUMBER                        NULL,
  PEN_CONT_AMT        NUMBER                        NULL,
  PEN_INC_AMT         NUMBER                        NULL,
  WAGE_WK_BEN_AMT     NUMBER                        NULL,
  WAGE_80_TAX_AMT     NUMBER                        NULL,
  WAGE_100_TAX_AMT    NUMBER                        NULL,
  WAGE_FICA_AMT       NUMBER                        NULL,
  WAGE_ST_AMT         NUMBER                        NULL,
  WAGE_CONT_AMT       NUMBER                        NULL,
  WAGE_INC_AMT        NUMBER                        NULL,
  DIS_WK_BEN_AMT      NUMBER                        NULL,
  DIS_80_TAX_AMT      NUMBER                        NULL,
  DIS_100_TAX_AMT     NUMBER                        NULL,
  DIS_FICA_AMT        NUMBER                        NULL,
  DIS_ST_AMT          NUMBER                        NULL,
  DIS_CONT_AMT        NUMBER                        NULL,
  DIS_INC_AMT         NUMBER                        NULL,
  SELF_WK_BEN_AMT     NUMBER                        NULL,
  SELF_80_TAX_AMT     NUMBER                        NULL,
  SELF_100_TAX_AMT    NUMBER                        NULL,
  SELF_FICA_AMT       NUMBER                        NULL,
  SELF_ST_AMT         NUMBER                        NULL,
  SELF_CONT_AMT       NUMBER                        NULL,
  SELF_INC_AMT        NUMBER                        NULL,
  OTH_WK_BEN_AMT      NUMBER                        NULL,
  OTH_80_TAX_AMT      NUMBER                        NULL,
  OTH_100_TAX_AMT     NUMBER                        NULL,
  OTH_FICA_AMT        NUMBER                        NULL,
  OTH_ST_AMT          NUMBER                        NULL,
  OTH_CONT_AMT        NUMBER                        NULL,
  OTH_INC_AMT         NUMBER                        NULL,
  SS_OLD_BEN_AMT      NUMBER                        NULL,
  TOT_SS_BEN_AMT      NUMBER                        NULL,
  ST_UNEM_INSUR_TEXT  VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MN_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  OSHA_CASE_TEXT    VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  APPRENTICE_CODE   NUMBER(10)                      NULL,
  APPRENTICE_DATE   VARCHAR2(8 BYTE)                NULL,
  LOST_TIME_DATE    VARCHAR2(8 BYTE)                NULL,
  UNEM_NUM_TEXT     VARCHAR2(20 BYTE)               NULL,
  CARR_CLASS_TEXT   VARCHAR2(20 BYTE)               NULL,
  MEALS_AMT         NUMBER                          NULL,
  LODGE_AMT         NUMBER                          NULL,
  SECOND_INC_AMT    NUMBER                          NULL,
  WORK_STATUS_TYPE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MO_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  MORE_MEDS_CODE    NUMBER(10)                      NULL,
  RTW_WAGE_AMT      NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  EST_MED_AMT       NUMBER                          NULL,
  WORK_PROCES_TEXT  VARCHAR2(50 BYTE)               NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MS_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  WORK_PROCES_TEXT  VARCHAR2(50 BYTE)               NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_MT_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  YRS_EDU_NUM         NUMBER                        NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  DURING_EMP_CODE     NUMBER(10)                    NULL,
  DOUBT_CLAIM_MEMO    CLOB                          NULL,
  QUEST_CLAIM_CODE    NUMBER(10)                    NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  RTW_SHIFT_CODE      NUMBER(10)                    NULL,
  SALARY_CONT_CODE    NUMBER(10)                    NULL,
  EMPLOYER_CODE       NUMBER(10)                    NULL,
  EMPLOYEE_CODE       NUMBER(10)                    NULL,
  PAY_PERIOD_1_AMT    NUMBER                        NULL,
  PAY_PERIOD_1_DATE   VARCHAR2(8 BYTE)              NULL,
  PAY_PERIOD_2_AMT    NUMBER                        NULL,
  PAY_PERIOD_2_DATE   VARCHAR2(8 BYTE)              NULL,
  PAY_PERIOD_3_AMT    NUMBER                        NULL,
  PAY_PERIOD_3_DATE   VARCHAR2(8 BYTE)              NULL,
  PAY_PERIOD_4_AMT    NUMBER                        NULL,
  PAY_PERIOD_4_DATE   VARCHAR2(8 BYTE)              NULL,
  REVC_ROOMS_CODE     NUMBER(10)                    NULL,
  REVC_OVERTIME_CODE  NUMBER(10)                    NULL,
  REVC_BONUS_CODE     NUMBER(10)                    NULL,
  REVC_COMMIS_CODE    NUMBER(10)                    NULL,
  REVC_OTHER_CODE     NUMBER(10)                    NULL,
  REVC_OTHER_TEXT     VARCHAR2(50 BYTE)             NULL,
  EST_WEEKLY_AMT      NUMBER                        NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  WORK_STATUS_CODE    NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  SIX_DISABIL_DATE    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NB_SUPP
(
  CLAIM_ID                        NUMBER(10)        NULL,
  MEDICARE_NO                     VARCHAR2(50 BYTE)     NULL,
  FIRM_NUM                        VARCHAR2(50 BYTE)     NULL,
  TRADE_NAME                      VARCHAR2(50 BYTE)     NULL,
  EXP_FROM_DATE                   VARCHAR2(8 BYTE)     NULL,
  EXP_TO_DATE                     VARCHAR2(8 BYTE)     NULL,
  WHOM_REPORTED                   VARCHAR2(50 BYTE)     NULL,
  TITLE                           VARCHAR2(50 BYTE)     NULL,
  WORK_AREA_DESC                  CLOB              NULL,
  WORK_EMPLYR_BUS_PURPOSE         NUMBER(10)        NULL,
  UNEXP_INJURY_CAUSE              VARCHAR2(50 BYTE)     NULL,
  PREV_INJUR_SAME_BODY            NUMBER(10)        NULL,
  YES_SPECIFY                     VARCHAR2(50 BYTE)     NULL,
  LOST_TIME_ACC                   NUMBER(10)        NULL,
  LAST_WORK_TIME                  VARCHAR2(6 BYTE)     NULL,
  HRS_PAID_DAY                    VARCHAR2(50 BYTE)     NULL,
  RTW_TEMP_PERIOD_ACC             NUMBER(10)        NULL,
  FROM_DATE                       VARCHAR2(8 BYTE)     NULL,
  TO_DATE                         VARCHAR2(8 BYTE)     NULL,
  NO_EXP                          CLOB              NULL,
  DAYS_PER_WEEK                   VARCHAR2(50 BYTE)     NULL,
  HOURS_PER_DAY                   VARCHAR2(50 BYTE)     NULL,
  POS_TYPE                        NUMBER(10)        NULL,
  DATE_COMMENCED                  VARCHAR2(8 BYTE)     NULL,
  EXP_DATE_TERMINATION            VARCHAR2(8 BYTE)     NULL,
  LONG_WORKER_EMPLOYER            VARCHAR2(50 BYTE)     NULL,
  IS_ALTER_EMPLYMNT               NUMBER(10)        NULL,
  YES_ALTER                       VARCHAR2(50 BYTE)     NULL,
  IS_RELATED_EMPLYR               NUMBER(10)        NULL,
  YES_RELATION                    VARCHAR2(50 BYTE)     NULL,
  TAX_EXEMP_CURR_YEAR             NUMBER            NULL,
  EMPLYR_ADVNC_PAYMNT             NUMBER(10)        NULL,
  YES_EXPLAIN                     VARCHAR2(50 BYTE)     NULL,
  WORKER_EMPLYMNT_INCOME          NUMBER(10)        NULL,
  EFF_AG_DATE                     VARCHAR2(8 BYTE)     NULL,
  END_AG_DATE                     VARCHAR2(8 BYTE)     NULL,
  WORKER_EMPLYMNT_INCOME_WAGES    NUMBER(10)        NULL,
  WAGES                           NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_PAY      NUMBER(10)        NULL,
  RETROACTIVE_PAY_INCREASE        NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_COVG     NUMBER(10)        NULL,
  SPONSERED_DISABILITY_COVERAGE   NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_VAC_PAY  NUMBER(10)        NULL,
  VACATION_PAY                    NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_SICK     NUMBER(10)        NULL,
  SICK                            NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_HOLIDAY  NUMBER(10)        NULL,
  HOLIDAY                         NUMBER            NULL,
  WORKER_EMPLYMNT_INCOME_EMPLYR   NUMBER(10)        NULL,
  EMPLYR_PYMNT                    NUMBER            NULL,
  COMMENTS                        CLOB              NULL,
  WORKER_LANG                     NUMBER(10)        NULL,
  EMPLYR_LANG                     NUMBER(10)        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NC_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  EMP_ETHNIC_CODE    NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  CODE_NUMBER_TEXT   VARCHAR2(50 BYTE)              NULL,
  SAME_WAGE_CODE     NUMBER(10)                     NULL,
  PAY_IN_KIND_AMT    NUMBER                         NULL,
  EQUIP_PWR_CODE     NUMBER(10)                     NULL,
  RTW_WAGE_AMT       NUMBER                         NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  MACH_PART_TEXT     VARCHAR2(50 BYTE)              NULL,
  DISA_LGTH_TEXT     VARCHAR2(20 BYTE)              NULL,
  FIRM_NAME_TEXT     VARCHAR2(40 BYTE)              NULL,
  RET_OCCUPAT_TEXT   VARCHAR2(20 BYTE)              NULL,
  SALARY_CONT_CONT   NUMBER(10)                     NULL,
  SALARY_BENG_CONTD  NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  RTW_TIME           VARCHAR2(6 BYTE)               NULL,
  DISABILITY_TIME    VARCHAR2(6 BYTE)               NULL,
  EMP_COD_TEXT       VARCHAR2(25 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_ND_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  COMMISSION_AMT      NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  PAID_OTHER_CODE     NUMBER(10)                    NULL,
  OFF_PARTNER_CODE    NUMBER(10)                    NULL,
  YRS_EDU_NUM         NUMBER                        NULL,
  PRIOR_INJ_CODE      NUMBER(10)                    NULL,
  SHIFT_END_TIME      VARCHAR2(6 BYTE)              NULL,
  SEASON_LGTH_NUM     NUMBER                        NULL,
  LIGHT_DUTY_CODE     NUMBER(10)                    NULL,
  LAST_WRK_ND_DATE    VARCHAR2(8 BYTE)              NULL,
  LEFT_WORK_TIME      VARCHAR2(6 BYTE)              NULL,
  WRKCMP_ACCT_TEXT    VARCHAR2(15 BYTE)             NULL,
  RATE_CLASS_TEXT     VARCHAR2(15 BYTE)             NULL,
  INJ_PREV_CODE       NUMBER(10)                    NULL,
  RTN_NXT_SHIFT_CODE  NUMBER(10)                    NULL,
  FIRST_TREAT_DATE    VARCHAR2(8 BYTE)              NULL,
  DOUBT_CLAIM_MEMO    CLOB                          NULL,
  JURIS_HIRED_CODE    NUMBER(10)                    NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NE_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  OCCUP_START_DATE    VARCHAR2(8 BYTE)              NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  RELATIVE_TEXT       VARCHAR2(50 BYTE)             NULL,
  ON_OT_CODE          NUMBER(10)                    NULL,
  UNEM_NUM_TEXT       VARCHAR2(20 BYTE)             NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  SELF_INS_TEXT       VARCHAR2(20 BYTE)             NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  TPA_CODE            NUMBER(10)                    NULL,
  SALARY_CONT_CODE    NUMBER(10)                    NULL,
  INSURED_DIFER_MEMO  CLOB                          NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  NE_TREATMENT_CODE   NUMBER(10)                    NULL,
  PURPOSE_TEXT        VARCHAR2(25 BYTE)             NULL,
  SROI_RPT_EFFV_DATE  VARCHAR2(8 BYTE)              NULL,
  PREXIT_DISABI_CODE  NUMBER(10)                    NULL,
  REPRESENTATON_DATE  VARCHAR2(8 BYTE)              NULL,
  SROI_RPT_PURP_CODE  NUMBER(10)                    NULL,
  RRTW_EFFV_DATE      VARCHAR2(8 BYTE)              NULL,
  RTWQUALIFI_CODE     NUMBER(10)                    NULL,
  DD_WIDOW_NUMB       NUMBER                        NULL,
  DD_WIDOWER_NUMB     NUMBER                        NULL,
  DD_CHILDREN_NUMB    NUMBER                        NULL,
  DD_SIBLINGS_NUMB    NUMBER                        NULL,
  DD_PARENTS_NUMB     NUMBER                        NULL,
  DD_OTHER_NUMB       NUMBER                        NULL,
  PI_BODYPART1_CODE   NUMBER(10)                    NULL,
  PI_BODYPART1_NUMB   NUMBER                        NULL,
  PI_BODYPART2_CODE   NUMBER(10)                    NULL,
  PI_BODYPART2_NUMB   NUMBER                        NULL,
  PI_BODYPART3_CODE   NUMBER(10)                    NULL,
  PI_BODYPART3_NUMB   NUMBER                        NULL,
  INSUR_REPRT_TEXT    VARCHAR2(40 BYTE)             NULL,
  COMP_RATE_AMT       NUMBER                        NULL,
  AGREE_COMP_CODE     NUMBER(10)                    NULL,
  IAIABC_CLM_ST_CODE  NUMBER(10)                    NULL,
  IAIABC_CLM_TY_CODE  NUMBER(10)                    NULL,
  IAIABC_LATE_CODE    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NH_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  RET_OCCUPAT_TEXT    VARCHAR2(20 BYTE)             NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  INJURED_OCC_CODE    NUMBER(10)                    NULL,
  NUM_PT_EMP_NUM      NUMBER                        NULL,
  NUM_FT_EMP_NUM      NUMBER                        NULL,
  REG_OCCUP_CODE      NUMBER(10)                    NULL,
  RTW_FULL_CODE       NUMBER(10)                    NULL,
  RTW_LIGHT_CODE      NUMBER(10)                    NULL,
  CLIENT_BUS_TEXT     VARCHAR2(25 BYTE)             NULL,
  SAFETY_PROG_CODE    NUMBER(10)                    NULL,
  SAFETY_CMTE_CODE    NUMBER(10)                    NULL,
  MGD_CARE_CODE       NUMBER(10)                    NULL,
  PROVIDER_TEXT       VARCHAR2(25 BYTE)             NULL,
  YOUTH_EMP_CODE      NUMBER(10)                    NULL,
  DISA_LGTH_TEXT      VARCHAR2(20 BYTE)             NULL,
  INS_AGENCY_TEXT     VARCHAR2(40 BYTE)             NULL,
  OTHER_TREAT_TEXT    VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  RTW_CODE            NUMBER(10)                    NULL,
  HIRE_IN_STATE_CODE  NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NJ_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  EMP_NUM             NUMBER                        NULL,
  OSHA_CASE_NUMB      VARCHAR2(20 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  PURPOSE_TEXT        VARCHAR2(25 BYTE)             NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  WORK_PROC_TEXT      VARCHAR2(100 BYTE)            NULL,
  JURIS_HIRED_CODE    NUMBER(10)                    NULL,
  SALARY_CONT_CODE    NUMBER(10)                    NULL,
  SROI_RPT_EFFV_DATE  VARCHAR2(8 BYTE)              NULL,
  PREXIT_DISABI_CODE  NUMBER(10)                    NULL,
  REPRESENTATON_DATE  VARCHAR2(8 BYTE)              NULL,
  SROI_RPT_PURP_CODE  NUMBER(10)                    NULL,
  RRTW_EFFV_DATE      VARCHAR2(8 BYTE)              NULL,
  RTWQUALIFI_CODE     NUMBER(10)                    NULL,
  JURIS_CLAIM_TEXT    VARCHAR2(40 BYTE)             NULL,
  DD_WIDOW_NUMB       NUMBER                        NULL,
  DD_WIDOWER_NUMB     NUMBER                        NULL,
  DD_CHILDREN_NUMB    NUMBER                        NULL,
  DD_SIBLINGS_NUMB    NUMBER                        NULL,
  DD_PARENTS_NUMB     NUMBER                        NULL,
  DD_JURIS_NUMB       NUMBER                        NULL,
  DD_HCHILDREN_NUMB   NUMBER                        NULL,
  DD_OTHER_NUMB       NUMBER                        NULL,
  MAX_MED_DATE        VARCHAR2(8 BYTE)              NULL,
  PI_BODYPART1_CODE   NUMBER(10)                    NULL,
  PI_BODYPART1_NUMB   NUMBER                        NULL,
  PI_BODYPART2_CODE   NUMBER(10)                    NULL,
  PI_BODYPART2_NUMB   NUMBER                        NULL,
  PI_BODYPART3_CODE   NUMBER(10)                    NULL,
  PI_BODYPART3_NUMB   NUMBER                        NULL,
  INSUR_REPRT_TEXT    VARCHAR2(40 BYTE)             NULL,
  COMP_RATE_AMT       NUMBER                        NULL,
  AGREE_COMP_CODE     NUMBER(10)                    NULL,
  MISC_IN_AWW_CODE    NUMBER(10)                    NULL,
  IAIABC_CLM_ST_CODE  NUMBER(10)                    NULL,
  IAIABC_CLM_TY_CODE  NUMBER(10)                    NULL,
  IAIABC_LATE_CODE    NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NM_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  SAME_WAGE_CODE    NUMBER(10)                      NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  EMP_NUM           NUMBER                          NULL,
  WORK_PROC_TEXT    VARCHAR2(100 BYTE)              NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NS_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  FIRM_NUM           VARCHAR2(50 BYTE)              NULL,
  TRADE_NAME         VARCHAR2(50 BYTE)              NULL,
  EMP_MLNG_ADDR      VARCHAR2(50 BYTE)              NULL,
  EMP_CELL_PH        VARCHAR2(50 BYTE)              NULL,
  HEALTH_CARD_NO     VARCHAR2(50 BYTE)              NULL,
  EMPLYR_AGREES      NUMBER(10)                     NULL,
  WORKER_AGREES      NUMBER(10)                     NULL,
  DATE_SYMP_NOTICED  VARCHAR2(8 BYTE)               NULL,
  BODY_PARTS         NUMBER(10)                     NULL,
  PERSON_FACTOR      NUMBER(10)                     NULL,
  PERSON_NAME        VARCHAR2(50 BYTE)              NULL,
  FACTOR_EXPLAIN     VARCHAR2(50 BYTE)              NULL,
  DATE_DOCTOR        VARCHAR2(8 BYTE)               NULL,
  LOST_TIME_ACC      NUMBER(10)                     NULL,
  LOSS_DATE          VARCHAR2(8 BYTE)               NULL,
  LOSS_TIME          VARCHAR2(6 BYTE)               NULL,
  LOST_EARNINGS_ACC  NUMBER(10)                     NULL,
  EARN_LOSS_DATE     VARCHAR2(8 BYTE)               NULL,
  EARN_LOSS_TIME     VARCHAR2(6 BYTE)               NULL,
  WORKER_TYPE        NUMBER(10)                     NULL,
  FAMILY_MEMB        NUMBER(10)                     NULL,
  WHOM_REPORTED      VARCHAR2(50 BYTE)              NULL,
  TITLE              VARCHAR2(50 BYTE)              NULL,
  PHONE              VARCHAR2(50 BYTE)              NULL,
  DELAY_REPORTING    VARCHAR2(50 BYTE)              NULL,
  WRKR_MAIN_JOB      VARCHAR2(50 BYTE)              NULL,
  WORKER_DOM_HAND    NUMBER(10)                     NULL,
  DURATIN_JOB        VARCHAR2(50 BYTE)              NULL,
  PREV_JOB           VARCHAR2(50 BYTE)              NULL,
  OVERTIME           VARCHAR2(50 BYTE)              NULL,
  CHNG_WORKER_RESP   VARCHAR2(50 BYTE)              NULL,
  EMPLYED_FOR        NUMBER(10)                     NULL,
  WORKER_EMP_TYPE    NUMBER(10)                     NULL,
  WORKER_EMP_TYPE1   NUMBER(10)                     NULL,
  ORG_EMPLYMNT_DATE  VARCHAR2(8 BYTE)               NULL,
  NORM_GROSS_ERNGS   NUMBER                         NULL,
  EARNINGS_PER       NUMBER(10)                     NULL,
  GROSS_ERNGS        NUMBER                         NULL,
  FROM_DATE          VARCHAR2(8 BYTE)               NULL,
  TO_DATE            VARCHAR2(8 BYTE)               NULL,
  TAX_DEDCN_CODE     VARCHAR2(50 BYTE)              NULL,
  HRS_SCHDLD         VARCHAR2(50 BYTE)              NULL,
  HRS_WRKD           VARCHAR2(50 BYTE)              NULL,
  HRS_PAID           VARCHAR2(50 BYTE)              NULL,
  WRKR_REG_DUTIES    NUMBER(10)                     NULL,
  REG_DATE           VARCHAR2(8 BYTE)               NULL,
  REG_TIME           VARCHAR2(6 BYTE)               NULL,
  OFF_DUTY_PYMNT     NUMBER(10)                     NULL,
  TYPE_BNFT_PAID     VARCHAR2(50 BYTE)              NULL,
  LONG_PYMNT_CONT    VARCHAR2(50 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NV_SUPP
(
  CLAIM_ID              NUMBER(10)                  NULL,
  DOI_PAID_CODE         NUMBER(10)                  NULL,
  CARRIER_NO_TEXT       VARCHAR2(50 BYTE)           NULL,
  FULL_SALARY_CODE      NUMBER(10)                  NULL,
  PERS_NOTIF_EID        NUMBER(10)                  NULL,
  ACC_PREVENT_MEMO      CLOB                        NULL,
  CORP_OFFICER_CODE     NUMBER(10)                  NULL,
  SOLE_PRO_CODE         NUMBER(10)                  NULL,
  PARTNER_CODE          NUMBER(10)                  NULL,
  SIIS_ACC_TEXT         VARCHAR2(20 BYTE)           NULL,
  LSTEARN_WAG_DATE      VARCHAR2(50 BYTE)           NULL,
  SIIS_CLASS_TEXT       VARCHAR2(20 BYTE)           NULL,
  BONUS_CODE            NUMBER(10)                  NULL,
  BONUS_AMT             NUMBER                      NULL,
  UNEMPLOYMENT_CODE     NUMBER(10)                  NULL,
  COMMISSIONS_CODE      NUMBER(10)                  NULL,
  COMM_AMT              NUMBER                      NULL,
  COMM_GROSS_CODE       NUMBER(10)                  NULL,
  COMM_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  COMM_END_DATE         VARCHAR2(50 BYTE)           NULL,
  LT_DUTY_WORK_CODE     NUMBER(10)                  NULL,
  SPANISH_ONLY_CODE     NUMBER(10)                  NULL,
  MEAL_LODGE_CODE       NUMBER(10)                  NULL,
  LODGE_AMT             NUMBER                      NULL,
  LODGE_PER_TEXT        VARCHAR2(20 BYTE)           NULL,
  MEALS_AMT             NUMBER                      NULL,
  MEALS_PER_TEXT        VARCHAR2(20 BYTE)           NULL,
  TIPS_CODE             NUMBER(10)                  NULL,
  WAGE_CONT_CODE        NUMBER(10)                  NULL,
  SHIFT_END_TIME        VARCHAR2(50 BYTE)           NULL,
  SELF_INSUR_CODE       NUMBER(10)                  NULL,
  PAY1_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY1_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY1_GROSS_AMT        NUMBER                      NULL,
  PAY1_TIP_AMT          NUMBER                      NULL,
  PAY2_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY2_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY2_GROSS_AMT        NUMBER                      NULL,
  PAY2_TIP_AMT          NUMBER                      NULL,
  PAY3_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY3_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY3_GROSS_AMT        NUMBER                      NULL,
  PAY3_TIP_AMT          NUMBER                      NULL,
  PAY4_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY4_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY4_GROSS_AMT        NUMBER                      NULL,
  PAY4_TIP_AMT          NUMBER                      NULL,
  PAY5_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY5_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY5_GROSS_AMT        NUMBER                      NULL,
  PAY5_TIP_AMT          NUMBER                      NULL,
  PAY6_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY6_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY6_GROSS_AMT        NUMBER                      NULL,
  PAY6_TIP_AMT          NUMBER                      NULL,
  PAY7_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY7_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY7_GROSS_AMT        NUMBER                      NULL,
  PAY7_TIP_AMT          NUMBER                      NULL,
  PAY8_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY8_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY8_GROSS_AMT        NUMBER                      NULL,
  PAY8_TIP_AMT          NUMBER                      NULL,
  PAY9_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY9_END_DATE         VARCHAR2(50 BYTE)           NULL,
  PAY9_GROSS_AMT        NUMBER                      NULL,
  PAY9_TIP_AMT          NUMBER                      NULL,
  PAY10_BEG_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY10_END_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY10_GROSS_AMT       NUMBER                      NULL,
  PAY10_TIP_AMT         NUMBER                      NULL,
  PAY11_BEG_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY11_END_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY11_GROSS_AMT       NUMBER                      NULL,
  PAY11_TIP_AMT         NUMBER                      NULL,
  PAY12_BEG_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY12_END_DATE        VARCHAR2(50 BYTE)           NULL,
  PAY12_GROSS_AMT       NUMBER                      NULL,
  PAY12_TIP_AMT         NUMBER                      NULL,
  CLASS_CODE_TEXT       VARCHAR2(20 BYTE)           NULL,
  IS_ACC_NUM_TEXT       VARCHAR2(20 BYTE)           NULL,
  DEEMED_AMT            NUMBER                      NULL,
  ABS1_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  ABS1_END_DATE         VARCHAR2(50 BYTE)           NULL,
  REASON1_TEXT          VARCHAR2(20 BYTE)           NULL,
  ABS2_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  ABS2_END_DATE         VARCHAR2(50 BYTE)           NULL,
  REASON2_TEXT          VARCHAR2(20 BYTE)           NULL,
  ABS3_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  ABS3_END_DATE         VARCHAR2(50 BYTE)           NULL,
  REASON3_TEXT          VARCHAR2(20 BYTE)           NULL,
  ABS4_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  ABS4_END_DATE         VARCHAR2(50 BYTE)           NULL,
  REASON4_TEXT          VARCHAR2(20 BYTE)           NULL,
  ABS5_BEG_DATE         VARCHAR2(50 BYTE)           NULL,
  ABS5_END_DATE         VARCHAR2(50 BYTE)           NULL,
  REASON5_TEXT          VARCHAR2(20 BYTE)           NULL,
  NV_PAY_TYPE_CODE      NUMBER(10)                  NULL,
  NV_PAY_END_CODE       NUMBER(10)                  NULL,
  NV_CLAIM_TYPE_CODE    NUMBER(10)                  NULL,
  REASON_MEMO           CLOB                        NULL,
  MCO_NAME_TEXT         VARCHAR2(30 BYTE)           NULL,
  PRIM_LANG_TEXT        VARCHAR2(20 BYTE)           NULL,
  ADDR_DIFER_MEMO       CLOB                        NULL,
  RTN_NXT_SHIFT_CODE    NUMBER(10)                  NULL,
  HIRE_STATE_TEXT       VARCHAR2(30 BYTE)           NULL,
  VACAT_PAID_CODE       NUMBER(10)                  NULL,
  VACAT_PAD_PED_NUM     NUMBER                      NULL,
  OVERTIME_PAID_CODE    NUMBER(10)                  NULL,
  SICK_PAID_CODE        NUMBER(10)                  NULL,
  HOLDAY_PAID_CODE      NUMBER(10)                  NULL,
  TERMINATIN_PAID_CODE  NUMBER(10)                  NULL,
  WAGE_EFFECT_DATE      VARCHAR2(8 BYTE)            NULL,
  OLD_WAGE_AMT          NUMBER                      NULL,
  OLD_WAGE_PER_CODE     NUMBER(10)                  NULL,
  JOB_CHANGE_CODE       NUMBER(10)                  NULL,
  JOB_CHANGE_DATE       VARCHAR2(8 BYTE)            NULL,
  JOB_CHANGE_MEMO       CLOB                        NULL,
  BONUS_BEG_DATE        VARCHAR2(8 BYTE)            NULL,
  BONUS_END_DATE        VARCHAR2(8 BYTE)            NULL,
  MEALS_PER_NUM         NUMBER                      NULL,
  MEALS_TIME_CODE       NUMBER(10)                  NULL,
  LODGE_TIME_CODE       NUMBER(10)                  NULL,
  PAYROLL_FROM_DATE     VARCHAR2(8 BYTE)            NULL,
  PAYROLL_TO_DATE       VARCHAR2(8 BYTE)            NULL,
  REASON1_ABS_CODE      NUMBER(10)                  NULL,
  REASON2_ABS_CODE      NUMBER(10)                  NULL,
  REASON3_ABS_CODE      NUMBER(10)                  NULL,
  WORK_DAYS_LT_NUMB     NUMBER                      NULL,
  SUPERVIR_TXT          VARCHAR2(50 BYTE)           NULL,
  INJURIED_CODE         NUMBER(10)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_NY_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  WK_AMT_2D         NUMBER                          NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  RTW_AWW_AMT       NUMBER                          NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  CODE_NUMBER_TEXT  VARCHAR2(50 BYTE)               NULL,
  OSHA_CASE_TEXT    VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  RELATIVE_TEXT     VARCHAR2(50 BYTE)               NULL,
  UNEM_NUM_TEXT     VARCHAR2(20 BYTE)               NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  RELATION_TEXT     VARCHAR2(50 BYTE)               NULL,
  MED_CARE_DATE     VARCHAR2(8 BYTE)                NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_OH_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  CERT_NUMBER_TEXT    VARCHAR2(50 BYTE)             NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  MOTION_MEMO         CLOB                          NULL,
  MO_SUPPORT_MEMO     CLOB                          NULL,
  REPRESENT_CODE      NUMBER(10)                    NULL,
  CERTIF_CODE         NUMBER(10)                    NULL,
  REJ_CODE            NUMBER(10)                    NULL,
  CLAR_CODE           NUMBER(10)                    NULL,
  EXPECT_PAYMT_CODE   NUMBER(10)                    NULL,
  MISS_DAYS_CODE      NUMBER(10)                    NULL,
  RELATE_IND_EV_CODE  NUMBER(10)                    NULL,
  SHIFT_END_TIME      VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  JURIS_HIRED_CODE    NUMBER(10)                    NULL,
  INITIAL_TREAT_DATE  VARCHAR2(8 BYTE)              NULL,
  PRINCAPAL_CODE      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_OK_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  DOI_PAID_CODE        NUMBER(10)                   NULL,
  CARRIER_NO_TEXT      VARCHAR2(50 BYTE)            NULL,
  FULL_SALARY_CODE     NUMBER(10)                   NULL,
  PRIOR_INJ_CODE       NUMBER(10)                   NULL,
  OK_INJURY_CODE       NUMBER(10)                   NULL,
  OK_OWNERSHIP_CODE    NUMBER(10)                   NULL,
  CWMP_PARTICIPA_CODE  NUMBER(10)                   NULL,
  NAME_OF_CWMP_TEXT    VARCHAR2(50 BYTE)            NULL,
  HIRE_IN_STATE_CODE   NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_ON_SUPP
(
  CLAIM_ID               NUMBER(10)                 NULL,
  PROV_CLAIM_NUM         VARCHAR2(50 BYTE)          NULL,
  WORKER_REFER_NUM       VARCHAR2(50 BYTE)          NULL,
  MINER_CERTIF_NUM       VARCHAR2(50 BYTE)          NULL,
  OCCUPATION_YEARS       VARCHAR2(50 BYTE)          NULL,
  LANGUAGE_CODE          NUMBER(10)                 NULL,
  LANGUAGE_OTHER         VARCHAR2(50 BYTE)          NULL,
  PRINCAPAL              NUMBER(10)                 NULL,
  EMPLOYER_FIRM_TXT      VARCHAR2(50 BYTE)          NULL,
  EMPLOYER_RATE_TXT      VARCHAR2(50 BYTE)          NULL,
  CLASS_UNIT_TXT         VARCHAR2(50 BYTE)          NULL,
  LGT_DUTY_PROG_CODE     NUMBER(10)                 NULL,
  TRADE_UNION_CODE       NUMBER(10)                 NULL,
  ABSENT_FROM_CODE       NUMBER(10)                 NULL,
  RESTRTED_WORK_CODE     NUMBER(10)                 NULL,
  EARNINGS_LESS_CODE     NUMBER(10)                 NULL,
  WORKDAY_END_TIME       VARCHAR2(6 BYTE)           NULL,
  EARNINGS_ACTAL_TXT     VARCHAR2(50 BYTE)          NULL,
  EARNINGS_NORML_TXT     VARCHAR2(50 BYTE)          NULL,
  EARLIER_RETURN_TXT     VARCHAR2(50 BYTE)          NULL,
  HEALTH_CARE_CODE       NUMBER(10)                 NULL,
  ALT_PHYSICIAN_TXT      VARCHAR2(50 BYTE)          NULL,
  AVE_WEEK_HOURS_TXT     VARCHAR2(50 BYTE)          NULL,
  WORKWK_CHANGE_CODE     NUMBER(10)                 NULL,
  TD_EXEMPTION_TXT       VARCHAR2(50 BYTE)          NULL,
  TD_CLAIM_CODE_TXT      VARCHAR2(50 BYTE)          NULL,
  BENEFIT_PLANC_CODE     NUMBER(10)                 NULL,
  BENEFIT_PLANM_CODE     NUMBER(10)                 NULL,
  R_PAYT_VACATN_CODE     NUMBER(10)                 NULL,
  PAYT_VACATN_CODE       NUMBER(10)                 NULL,
  PAYT_VACATN_AMT        VARCHAR2(50 BYTE)          NULL,
  PAYT_VACATN_PER        NUMBER(10)                 NULL,
  R_PAYT_BONUSS_CODE     NUMBER(10)                 NULL,
  PAYT_BONUSS_CODE       NUMBER(10)                 NULL,
  PAYT_BONUSS_AMT        VARCHAR2(50 BYTE)          NULL,
  PAYT_BONUSS_PER        NUMBER(10)                 NULL,
  R_PAYT_PROFIT_CODE     NUMBER(10)                 NULL,
  PAYT_PROFIT_CODE       NUMBER(10)                 NULL,
  PAYT_PROFIT_AMT        VARCHAR2(50 BYTE)          NULL,
  PAYT_PROFIT_PER        NUMBER(10)                 NULL,
  R_PAYT_LODGNG_CODE     NUMBER(10)                 NULL,
  PAYT_LODGNG_CODE       NUMBER(10)                 NULL,
  PAYT_LODGNG_AMT        VARCHAR2(50 BYTE)          NULL,
  PAYT_LODGNG_PER        NUMBER(10)                 NULL,
  R_PAYT_DIFRNT_CODE     NUMBER(10)                 NULL,
  PAYT_DIFRNT_CODE       NUMBER(10)                 NULL,
  PAYT_DIFRNT_AMT        VARCHAR2(50 BYTE)          NULL,
  PAYT_DIFRNT_PER        NUMBER(10)                 NULL,
  R_PAY_TIPGRAT_CODE     NUMBER(10)                 NULL,
  PAYT_TIP_GRAT_CODE     NUMBER(10)                 NULL,
  PAYT_TIP_GRAT_AMT      VARCHAR2(50 BYTE)          NULL,
  PAYT_TIP_GRAT_PER      NUMBER(10)                 NULL,
  R_PAYT_UNM_IN_CODE     NUMBER(10)                 NULL,
  PAYT_UNEM_INS_CODE     NUMBER(10)                 NULL,
  PAYT_UNEM_INS_AMT      VARCHAR2(50 BYTE)          NULL,
  PAYT_UNEM_INS_PER      NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_CAU     NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_SEA     NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_APP     NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_STU     NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_LEA     NUMBER(10)                 NULL,
  EMPLOYMNT_TYPE_OTH     NUMBER(10)                 NULL,
  EMPLOYT_STATUS_TXT     VARCHAR2(50 BYTE)          NULL,
  PAYMENT_TEXT           VARCHAR2(50 BYTE)          NULL,
  PAYMENT_ADDR_TEXT      VARCHAR2(50 BYTE)          NULL,
  PREVUS_INJURY_CODE     NUMBER(10)                 NULL,
  PREVUS_INJURY_TXT      VARCHAR2(50 BYTE)          NULL,
  OTHER_PERSON_CODE      NUMBER(10)                 NULL,
  OTHER_PERSON_TEXT      VARCHAR2(50 BYTE)          NULL,
  EQUIPMENT_TEXT         VARCHAR2(50 BYTE)          NULL,
  CLAIM_ACCEPT_CODE      NUMBER(10)                 NULL,
  CLAIM_DENIAL_TEXT      VARCHAR2(50 BYTE)          NULL,
  OBJECT_LETTER_CODE     NUMBER(10)                 NULL,
  RTW_CONTACT_TXT        VARCHAR2(50 BYTE)          NULL,
  PAYMT_VACATN_AMT       NUMBER                     NULL,
  PAYMT_BONUSS_AMT       NUMBER                     NULL,
  PAYMT_PROFIT_AMT       NUMBER                     NULL,
  PAYMT_LODGNG_AMT       NUMBER                     NULL,
  PAYMT_DIFRNT_AMT       NUMBER                     NULL,
  PAYMT_TIP_GRAT_AMT     NUMBER                     NULL,
  PAYMT_UNEM_INS_AMT     NUMBER                     NULL,
  LEFT_WORK_TIME         VARCHAR2(6 BYTE)           NULL,
  RETURN_WORK_TIME       VARCHAR2(6 BYTE)           NULL,
  WORK_AFTER_FT_DATE     VARCHAR2(14 BYTE)          NULL,
  WORK_AFTER_FT_TIME     VARCHAR2(6 BYTE)           NULL,
  WORK_AFTER_LT_DATE     VARCHAR2(14 BYTE)          NULL,
  WORK_AFTER_LT_TIME     VARCHAR2(6 BYTE)           NULL,
  LAST_WORKED_TIME       VARCHAR2(6 BYTE)           NULL,
  RETURNED_WORK_TIME     VARCHAR2(6 BYTE)           NULL,
  REPORTED_TO            VARCHAR2(50 BYTE)          NULL,
  REASON_DELAY           VARCHAR2(50 BYTE)          NULL,
  TD_EXEMPTION_FED_TXT   NUMBER                     NULL,
  TD_EXEMPTION_PRV_TXT   NUMBER                     NULL,
  TD_CLAIM_CODE_FED_TXT  VARCHAR2(50 BYTE)          NULL,
  TD_CLAIM_CODE_PRV_TXT  VARCHAR2(50 BYTE)          NULL,
  FROM_DATE              VARCHAR2(8 BYTE)           NULL,
  FROM_TIME              VARCHAR2(6 BYTE)           NULL,
  TO_DATE                VARCHAR2(8 BYTE)           NULL,
  TO_TIME                VARCHAR2(6 BYTE)           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_OR_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  EMP_OWNER_CODE       NUMBER(10)                   NULL,
  DOI_PAID_CODE        NUMBER(10)                   NULL,
  CARRIER_NO_TEXT      VARCHAR2(50 BYTE)            NULL,
  MECH_DEFECT_CODE     NUMBER(10)                   NULL,
  FULL_SALARY_CODE     NUMBER(10)                   NULL,
  OTHER_INJUR_CODE     NUMBER(10)                   NULL,
  DURING_EMP_CODE      NUMBER(10)                   NULL,
  HOSP_OVER_CODE       NUMBER(10)                   NULL,
  EMP_BIN_TEXT         VARCHAR2(15 BYTE)            NULL,
  PREM_EXEMPT_CODE     NUMBER(10)                   NULL,
  CLIENT_ADDR_TEXT     VARCHAR2(30 BYTE)            NULL,
  CLIENT_BIN_TEXT      VARCHAR2(15 BYTE)            NULL,
  CLIENT_FEIN_TEXT     VARCHAR2(15 BYTE)            NULL,
  YRS_EDU_NUM          NUMBER                       NULL,
  CLIENT_BUS_TEXT      VARCHAR2(50 BYTE)            NULL,
  LEFT_WORK_TIME       VARCHAR2(50 BYTE)            NULL,
  SHIFT_END_TIME       VARCHAR2(50 BYTE)            NULL,
  RTW_FULL_CODE        NUMBER(10)                   NULL,
  PAY_IN_KIND_AMT      NUMBER                       NULL,
  EMPLOYEE_EMAIL_TEXT  VARCHAR2(50 BYTE)            NULL,
  BODY_PART_LR_CODE    NUMBER(10)                   NULL,
  NOTICE_CODE          NUMBER(10)                   NULL,
  EMPLOYER_EMAIL_TEXT  VARCHAR2(50 BYTE)            NULL,
  CLIENT_EMAIL_TEXT    VARCHAR2(50 BYTE)            NULL,
  CLIENT_CITY_TEXT     VARCHAR2(20 BYTE)            NULL,
  CLIENT_STATE_ID      NUMBER(10)                   NULL,
  CLIENT_ZIP_TEXT      VARCHAR2(10 BYTE)            NULL,
  INJ_PREV_CODE        NUMBER(10)                   NULL,
  RET_RESTRICT_DATE    VARCHAR2(50 BYTE)            NULL,
  EMPLOYER_EMAIL_TXT   VARCHAR2(50 BYTE)            NULL,
  EMPLOYEE_EMAIL_TXT   VARCHAR2(50 BYTE)            NULL,
  IMMED_SUPERVR_CODE   NUMBER(10)                   NULL,
  LANGUAGE_CODE        NUMBER(10)                   NULL,
  LANGUAGE_OTHER       VARCHAR2(50 BYTE)            NULL,
  REG_PHYSICIAN_TXT    VARCHAR2(50 BYTE)            NULL,
  REG_PHY_PHONE_TXT    VARCHAR2(50 BYTE)            NULL,
  INJURIED_BEFO_MEMO   CLOB                         NULL,
  MULTIPLE_EMPR_CODE   NUMBER(10)                   NULL,
  AUTHOR_SSN_CODE      NUMBER(10)                   NULL,
  ADDR_DIFER_MEMO      CLOB                         NULL,
  CLIENT_PHONE_TEXT    VARCHAR2(30 BYTE)            NULL,
  JURIS_HIRED_CODE     NUMBER(10)                   NULL,
  CAUS_BY_OTHER_CODE   NUMBER(10)                   NULL,
  SUPER_PHONE_TEXT     VARCHAR2(30 BYTE)            NULL,
  OR_INJY_OCCUR_CODE   NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_PA_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  LAST_WAGE_DATE    VARCHAR2(8 BYTE)                NULL,
  SAME_WAGE_CODE    NUMBER(10)                      NULL,
  MECH_DEFECT_CODE  NUMBER(10)                      NULL,
  UNSAFE_ACT_CODE   NUMBER(10)                      NULL,
  AMPUTATION_CODE   NUMBER(10)                      NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  MOD_RATE_AMT      NUMBER                          NULL,
  NEW_RATE_AMT      NUMBER                          NULL,
  PAY_DAYS_NUM      NUMBER                          NULL,
  PAY_WEEK_NUM      NUMBER                          NULL,
  SUSPENDED_DATE    VARCHAR2(50 BYTE)               NULL,
  MODIFIED_DATE     VARCHAR2(50 BYTE)               NULL,
  DISAGREED_CODE    NUMBER(10)                      NULL,
  DENIAL_CODE       NUMBER(10)                      NULL,
  CONDITION_MEMO    CLOB                            NULL,
  CAUSE_MEMO        CLOB                            NULL,
  REPORT_REQ_MEMO   CLOB                            NULL,
  WKLY_WAGE_AMT     NUMBER                          NULL,
  AVG_INJ_WAG_AMT   NUMBER                          NULL,
  WEEKLY_RATE_AMT   NUMBER                          NULL,
  PART_COMP_AMT     NUMBER                          NULL,
  ISSUE_MEMO        CLOB                            NULL,
  MULT_PERIOD_MEMO  CLOB                            NULL,
  STATUS_CHAN_DATE  VARCHAR2(50 BYTE)               NULL,
  STAT_CHANGE_CODE  NUMBER(10)                      NULL,
  COMP_RATE_AMT     NUMBER                          NULL,
  COMP_START_DATE   VARCHAR2(50 BYTE)               NULL,
  FIRST_CHECK_DATE  VARCHAR2(50 BYTE)               NULL,
  HEAL_WEEKS_NUM    NUMBER                          NULL,
  HEAL_DAYS_NUM     NUMBER                          NULL,
  CREDIT_AMT        NUMBER                          NULL,
  DAY_21_RULE_CODE  NUMBER(10)                      NULL,
  AV_INJ_WAG_AMT    NUMBER                          NULL,
  PAY_MADE_CODE     NUMBER(10)                      NULL,
  WHY_TEXT          VARCHAR2(50 BYTE)               NULL,
  LOSS_USE_TEXT     VARCHAR2(50 BYTE)               NULL,
  REMARKS_MEMO      CLOB                            NULL,
  NAICS_CODE_TEXT   VARCHAR2(20 BYTE)               NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_PE_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  FIRM_NUM           VARCHAR2(50 BYTE)              NULL,
  COND_DEV_TIME      NUMBER(10)                     NULL,
  DIRECTOR_OFF       NUMBER(10)                     NULL,
  EARNINGS           NUMBER(10)                     NULL,
  FROM_TIME          VARCHAR2(6 BYTE)               NULL,
  TO_TIME            VARCHAR2(6 BYTE)               NULL,
  WHOM_REPORTED      VARCHAR2(50 BYTE)              NULL,
  TITLE              VARCHAR2(50 BYTE)              NULL,
  FLLY_AGR_RPT       NUMBER(10)                     NULL,
  ACTN_TAKEN         NUMBER(10)                     NULL,
  INJY_PEI           NUMBER(10)                     NULL,
  COUNTY             NUMBER(10)                     NULL,
  LFT_FC             NUMBER(10)                     NULL,
  FRNT_HND           NUMBER(10)                     NULL,
  BCK_HND            NUMBER(10)                     NULL,
  RT_FC              NUMBER(10)                     NULL,
  FRNT_BDY           NUMBER(10)                     NULL,
  BCK_BDY            NUMBER(10)                     NULL,
  FEET               NUMBER(10)                     NULL,
  ACTN_BSNS_PRPS     NUMBER(10)                     NULL,
  ACTN_REG_DTUS      NUMBER(10)                     NULL,
  REG_DUT_EXP        CLOB                           NULL,
  LOST_WAGES         NUMBER(10)                     NULL,
  WITNESS            NUMBER(10)                     NULL,
  CORP_SERV_DIV      VARCHAR2(50 BYTE)              NULL,
  CASE_ID            VARCHAR2(50 BYTE)              NULL,
  OFF_WORK           NUMBER(10)                     NULL,
  TOP_UP             NUMBER(10)                     NULL,
  TOP_UP_AMT         NUMBER                         NULL,
  GROSS_DLY_PAY      NUMBER                         NULL,
  TD1_CODE           VARCHAR2(50 BYTE)              NULL,
  WEEKLY_AMT         NUMBER                         NULL,
  BIWEEKLY_AMT       NUMBER                         NULL,
  GROSS_PRV_12MNTHS  NUMBER                         NULL,
  GROSS_TX_YR        NUMBER                         NULL,
  GROSS_PARTIAL_YR   NUMBER                         NULL,
  NO_WEEKS           VARCHAR2(50 BYTE)              NULL,
  EMPLYMNT_TYPE      NUMBER(10)                     NULL,
  MISSED_TIME        VARCHAR2(6 BYTE)               NULL,
  DAYS_MISSED        VARCHAR2(50 BYTE)              NULL,
  SHFT_HRS           VARCHAR2(50 BYTE)              NULL,
  RETND_TIME         VARCHAR2(6 BYTE)               NULL,
  WORK_TYPE          NUMBER(10)                     NULL,
  BK_DUTIES          NUMBER(10)                     NULL,
  WCB_CNTCT_DETAILS  VARCHAR2(50 BYTE)              NULL,
  HRS_PER_DAY        VARCHAR2(50 BYTE)              NULL,
  HRS_PER_ROTN       VARCHAR2(50 BYTE)              NULL,
  WRK_SCHDL_RPT      NUMBER(10)                     NULL,
  WEEKS_WRKD_PRV_YR  VARCHAR2(50 BYTE)              NULL,
  WEEK2_SUN          VARCHAR2(50 BYTE)              NULL,
  WEEK2_MON          VARCHAR2(50 BYTE)              NULL,
  WEEK2_TUES         VARCHAR2(50 BYTE)              NULL,
  WEEK2_WED          VARCHAR2(50 BYTE)              NULL,
  WEEK2_THUR         VARCHAR2(50 BYTE)              NULL,
  WEEK2_FRI          VARCHAR2(50 BYTE)              NULL,
  WEEK2_SAT          VARCHAR2(50 BYTE)              NULL,
  WEEK1_SUN          VARCHAR2(50 BYTE)              NULL,
  WEEK1_MON          VARCHAR2(50 BYTE)              NULL,
  WEEK1_TUES         VARCHAR2(50 BYTE)              NULL,
  WEEK1_WED          VARCHAR2(50 BYTE)              NULL,
  WEEK1_THUR         VARCHAR2(50 BYTE)              NULL,
  WEEK1_FRI          VARCHAR2(50 BYTE)              NULL,
  WEEK1_SAT          VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_SUN      VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_MON      VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_TUES     VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_WED      VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_THUR     VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_FRI      VARCHAR2(50 BYTE)              NULL,
  INJR_WEEK_SAT      VARCHAR2(50 BYTE)              NULL,
  NO_PAGES           VARCHAR2(50 BYTE)              NULL,
  ERNG_INFO_CNTCT    VARCHAR2(50 BYTE)              NULL,
  PHONE              VARCHAR2(50 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_RI_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  JURIS_HIRED_CODE   NUMBER(10)                     NULL,
  LANGUAGE_CODE      NUMBER(10)                     NULL,
  LANGUAGE_OTHER     VARCHAR2(50 BYTE)              NULL,
  INJURY_CHGE_CODE   NUMBER(10)                     NULL,
  MED_LT_DATE        VARCHAR2(8 BYTE)               NULL,
  RI_ILL_INJYA_CODE  NUMBER(10)                     NULL,
  RI_ILL_INJYB_CODE  NUMBER(10)                     NULL,
  RI_ILL_INJYC_CODE  NUMBER(10)                     NULL,
  RI_ILL_INJYD_CODE  NUMBER(10)                     NULL,
  RI_ILL_INJYE_CODE  NUMBER(10)                     NULL,
  RI_ILL_INJYF_CODE  NUMBER(10)                     NULL,
  SAME_PERSON_CODE   NUMBER(10)                     NULL,
  CORRECTION_CODE    NUMBER(10)                     NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_SC_SUPP
(
  CLAIM_ID           NUMBER(10)                     NULL,
  DOI_PAID_CODE      NUMBER(10)                     NULL,
  CARRIER_NO_TEXT    VARCHAR2(50 BYTE)              NULL,
  PAY_WAGES_CODE     NUMBER(10)                     NULL,
  LAST_WAGE_DATE     VARCHAR2(8 BYTE)               NULL,
  PAY_IN_KIND_AMT    NUMBER                         NULL,
  FULL_SALARY_CODE   NUMBER(10)                     NULL,
  OFF_PARTNER_CODE   NUMBER(10)                     NULL,
  WORK_PROCES_TEXT   VARCHAR2(50 BYTE)              NULL,
  SELF_INSUR_CODE    NUMBER(10)                     NULL,
  ADDR_DIFER_MEMO    CLOB                           NULL,
  PURPOSE_TEXT       VARCHAR2(20 BYTE)              NULL,
  JURIS_HIRED_CODE   NUMBER(10)                     NULL,
  SALARY_CONT_CODE   NUMBER(10)                     NULL,
  WORK_STATUS_CODE   NUMBER(10)                     NULL,
  DISABILITY_DATE    VARCHAR2(8 BYTE)               NULL,
  LOCATION_NUM_TEXT  VARCHAR2(25 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_SD_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  OCCUP_START_DATE  VARCHAR2(8 BYTE)                NULL,
  LAST_PAY_AMT      NUMBER                          NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  PAID_OTHER_CODE   NUMBER(10)                      NULL,
  OFF_PARTNER_CODE  NUMBER(10)                      NULL,
  YRS_EDU_NUM       NUMBER                          NULL,
  PERM_EMP_CODE     NUMBER(10)                      NULL,
  PERS_NOTIF_EID    NUMBER(10)                      NULL,
  DOUBT_CLAIM_MEMO  CLOB                            NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  EMPLOYEES_NUM     NUMBER                          NULL,
  MAIL_INS_DATE     VARCHAR2(50 BYTE)               NULL,
  NAIC_NUM_TEXT     VARCHAR2(20 BYTE)               NULL,
  MAIL_DOL_DATE     VARCHAR2(50 BYTE)               NULL,
  BODY_PART_A_CODE  NUMBER(10)                      NULL,
  CAUSE_CODE        NUMBER(10)                      NULL,
  NATURE_OF_CODE    NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  BODY_PARTB_CODE   NUMBER(10)                      NULL,
  CAUSE_INJB_CODE   NUMBER(10)                      NULL,
  NATUR_INJB_CODE   NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_SK_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  WCB_FIRM_NUM         VARCHAR2(50 BYTE)            NULL,
  INDSTRY_RT_CODE      VARCHAR2(50 BYTE)            NULL,
  SPECIFIC_DIV         VARCHAR2(50 BYTE)            NULL,
  PERSNL_HLTH_NUM      VARCHAR2(50 BYTE)            NULL,
  HLTH_CR_PRVDR        VARCHAR2(50 BYTE)            NULL,
  LAST_DAY_TIME        VARCHAR2(6 BYTE)             NULL,
  RETND_TIME           VARCHAR2(6 BYTE)             NULL,
  WORK_RELTD_INCDNT    NUMBER(10)                   NULL,
  MONTHLY_WAGE         NUMBER                       NULL,
  EMPLYMNT_TYPE        NUMBER(10)                   NULL,
  OTHR_EXP             CLOB                         NULL,
  GROSS_EARNINGS       NUMBER                       NULL,
  FROM_DATE            VARCHAR2(8 BYTE)             NULL,
  TO_DATE              VARCHAR2(8 BYTE)             NULL,
  UNPAID_SICKNESS      VARCHAR2(50 BYTE)            NULL,
  PRIOR_WCB_CLMS       VARCHAR2(50 BYTE)            NULL,
  LACK_WORK            VARCHAR2(50 BYTE)            NULL,
  OTHER                VARCHAR2(50 BYTE)            NULL,
  OTHRS_EXP            CLOB                         NULL,
  FROM_TIME            VARCHAR2(6 BYTE)             NULL,
  TO_TIME              VARCHAR2(6 BYTE)             NULL,
  SHIFT_WORK           NUMBER(10)                   NULL,
  LOST_TIME            NUMBER(10)                   NULL,
  DAYS_OFF_BEFINJ      NUMBER(10)                   NULL,
  DAYS_OFF_INJ         NUMBER(10)                   NULL,
  DAYS_OFF_AFTINJ      NUMBER(10)                   NULL,
  TD1_EXMPTN           NUMBER(10)                   NULL,
  PRVNCL_AMT           NUMBER                       NULL,
  FEDRL_AMT            NUMBER                       NULL,
  OTH_AMT              NUMBER                       NULL,
  NO_CHILDREN          VARCHAR2(50 BYTE)            NULL,
  COMP_PYMNT_TO        NUMBER(10)                   NULL,
  STAT_HOLIDAYS_PYMNT  NUMBER(10)                   NULL,
  TITLE                VARCHAR2(50 BYTE)            NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_TN_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  COMMISSION_AMT      NUMBER                        NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  RELATIVE_TEXT       VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  CLAIM_TYPE_CODE     NUMBER(10)                    NULL,
  SALARY_BENG_CONTD   NUMBER(10)                    NULL,
  WORK_STATUS_TYPE    NUMBER(10)                    NULL,
  CLM_ADM_NOTID_DATE  VARCHAR2(8 BYTE)              NULL,
  PREP_COMP_NAME      VARCHAR2(50 BYTE)             NULL,
  DEPEND_WIDOW_CODE   NUMBER(10)                    NULL,
  DEPEND_WIDOER_CODE  NUMBER(10)                    NULL,
  DEPEND_MOTHER_CODE  NUMBER(10)                    NULL,
  DEPEND_FATHER_CODE  NUMBER(10)                    NULL,
  DEPEND_DAUGHT_AMT   NUMBER                        NULL,
  DEPEND_SON_AMT      NUMBER                        NULL,
  DEPEND_SISTER_AMT   NUMBER                        NULL,
  DEPEND_BROTHER_AMT  NUMBER                        NULL,
  DEPEND_CHILD_AMT    NUMBER                        NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_TX_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  EMP_LANG_CODE       NUMBER(10)                    NULL,
  EMP_RACE_CODE       NUMBER(10)                    NULL,
  EMP_ETHNIC_CODE     NUMBER(10)                    NULL,
  OCCUP_START_DATE    VARCHAR2(8 BYTE)              NULL,
  EMP_OWNER_CODE      NUMBER(10)                    NULL,
  LAST_PAY_AMT        NUMBER                        NULL,
  LAST_PAY_HR_NUM     NUMBER                        NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  PAY_CODE_TEXT       VARCHAR2(20 BYTE)             NULL,
  ACC_PREVENT_CODE    NUMBER(10)                    NULL,
  RCV_ACC_PRE_CODE    NUMBER(10)                    NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  REGIS_NUM_TEXT      VARCHAR2(50 BYTE)             NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  ADDR_DIFER_STREET   VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_CITY     VARCHAR2(50 BYTE)             NULL,
  ADDR_DIFER_ST_CODE  NUMBER(10)                    NULL,
  ADDR_DIFER_ZIP      VARCHAR2(50 BYTE)             NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  WORK_LOC_MEMO       CLOB                          NULL,
  IN_JURIS_CODE       NUMBER(10)                    NULL,
  LAST_PAY_DAY_NUM    NUMBER                        NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_UT_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  LAST_PAY_AMT      NUMBER                          NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  PAID_OTHER_CODE   NUMBER(10)                      NULL,
  OFF_PARTNER_CODE  NUMBER(10)                      NULL,
  WORK_PROCES_TEXT  VARCHAR2(50 BYTE)               NULL,
  SELF_INSUR_CODE   NUMBER(10)                      NULL,
  SALARY_CONT_CODE  NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  PURPOSE_TEXT      VARCHAR2(20 BYTE)               NULL,
  JURIS_HIRED_CODE  NUMBER(10)                      NULL,
  WORK_STATUS_CODE  NUMBER(10)                      NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_VA_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  EMP_LANG_CODE       NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  EQUIP_PWR_CODE      NUMBER(10)                    NULL,
  RTW_WAGE_AMT        NUMBER                        NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  MACH_PART_TEXT      VARCHAR2(50 BYTE)             NULL,
  PD_1ST_INC_CODE     NUMBER(10)                    NULL,
  PERS_NOTIF_EID      NUMBER(10)                    NULL,
  STATE_PROPT_CODE    NUMBER(10)                    NULL,
  ADDR_DIFER_MEMO     CLOB                          NULL,
  OCCUP_START_DATE    VARCHAR2(50 BYTE)             NULL,
  DO_INCAP_PAID_CODE  NUMBER(10)                    NULL,
  COVERED_PEO_CODE    NUMBER(10)                    NULL,
  INCAPACITY_TIME     VARCHAR2(6 BYTE)              NULL,
  MEALS_AMT           NUMBER                        NULL,
  LODGE_AMT           NUMBER                        NULL,
  TIPS_AMT            NUMBER                        NULL,
  DISABILITY_DATE     VARCHAR2(8 BYTE)              NULL,
  PAY_TYPE_CODE       NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_VT_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  RTW_AWW_AMT       NUMBER                          NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  LAST_WAGE_DATE    VARCHAR2(8 BYTE)                NULL,
  PAY_IN_KIND_AMT   NUMBER                          NULL,
  MECH_DEFECT_CODE  NUMBER(10)                      NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  MACH_PART_TEXT    VARCHAR2(50 BYTE)               NULL,
  EMPLOYER_PR_CODE  NUMBER(10)                      NULL,
  EMP_PREV_MEMO     CLOB                            NULL,
  INJ_PREV_CODE     NUMBER(10)                      NULL,
  INJ_PREV_MEMO     CLOB                            NULL,
  RELATIVE_TEXT     VARCHAR2(50 BYTE)               NULL,
  RELATION_TEXT     VARCHAR2(15 BYTE)               NULL,
  REG_OCCUP_CODE    NUMBER(10)                      NULL,
  ADDR_DIFER_MEMO   CLOB                            NULL,
  EMPLOYEES_NUM     NUMBER                          NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL,
  MACH_DEFECT_MEMO  CLOB                            NULL,
  HIRED_IN_VT_CODE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_WA_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  DOI_PAID_CODE     NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_WI_SUPP
(
  CLAIM_ID            NUMBER(10)                    NULL,
  DOI_PAID_CODE       NUMBER(10)                    NULL,
  CARRIER_NO_TEXT     VARCHAR2(50 BYTE)             NULL,
  OSHA_CASE_TEXT      VARCHAR2(50 BYTE)             NULL,
  FULL_SALARY_CODE    NUMBER(10)                    NULL,
  RELATIVE_TEXT       VARCHAR2(50 BYTE)             NULL,
  NORM_OT_HRS_NUM     NUMBER                        NULL,
  RELATION_TEXT       VARCHAR2(15 BYTE)             NULL,
  PAID_OT_CODE        NUMBER(10)                    NULL,
  DURING_EMP_CODE     NUMBER(10)                    NULL,
  SUBST_ABUSE_CODE    NUMBER(10)                    NULL,
  PT_SAME_CODE        NUMBER(10)                    NULL,
  NUM_PT_EMP_NUM      NUMBER                        NULL,
  NUM_FT_EMP_NUM      NUMBER                        NULL,
  PAY_IN_KIND_AMT     NUMBER                        NULL,
  SELF_INSUR_CODE     NUMBER(10)                    NULL,
  REVC_MEALS_CODE     NUMBER(10)                    NULL,
  REVC_ROOMS_CODE     NUMBER(10)                    NULL,
  REVC_TIPS_CODE      NUMBER(10)                    NULL,
  REVC_MEALS_NUM      NUMBER                        NULL,
  REVC_ROOMS_NUM      NUMBER                        NULL,
  REVC_TIPS_AMT       NUMBER                        NULL,
  SAME_WORK_NUM       NUMBER                        NULL,
  SAME_WORK_AMT       NUMBER                        NULL,
  STD_HRS_DY_NUM      NUMBER                        NULL,
  STD_HRS_WK_NUM      NUMBER                        NULL,
  STD_DY_WK_NUM       NUMBER                        NULL,
  ST_UNEM_INSUR_TEXT  VARCHAR2(50 BYTE)             NULL,
  PIC_WK_HOURS_NUMB   NUMBER                        NULL,
  LOST_TIME_CODE      NUMBER(10)                    NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_WV_SUPP
(
  CLAIM_ID          NUMBER(10)                      NULL,
  OCCUP_START_DATE  VARCHAR2(8 BYTE)                NULL,
  EMP_OWNER_CODE    NUMBER(10)                      NULL,
  CARRIER_NO_TEXT   VARCHAR2(50 BYTE)               NULL,
  FULL_SALARY_CODE  NUMBER(10)                      NULL,
  LEFT_WORK_TIME    VARCHAR2(6 BYTE)                NULL,
  PAST_CLAIMS_CODE  NUMBER(10)                      NULL,
  PAST_CLAIMS_MEMO  CLOB                            NULL,
  PREV_EMPLOY_MEMO  CLOB                            NULL,
  HOSP_VISIT_DATE   VARCHAR2(8 BYTE)                NULL,
  PT_HOURLY_AMT     NUMBER                          NULL,
  PT_HOURS_WK_NUM   NUMBER                          NULL,
  QUEST_CLAIM_CODE  NUMBER(10)                      NULL,
  AVG_DAILY_AMT     NUMBER                          NULL,
  FISK_NUMBER_TEXT  VARCHAR2(15 BYTE)               NULL,
  CLASS_NUM_TEXT    VARCHAR2(15 BYTE)               NULL,
  QTRLY_RPT_CODE    NUMBER(10)                      NULL,
  OCCUP_CODE_TEXT   VARCHAR2(15 BYTE)               NULL,
  DISABILITY_DATE   VARCHAR2(8 BYTE)                NULL,
  INITIALS_TEXT     VARCHAR2(50 BYTE)               NULL,
  WCD_POL_TEXT      VARCHAR2(50 BYTE)               NULL,
  IND_CODE_TEXT     VARCHAR2(50 BYTE)               NULL,
  EMP_OWN_CODE      NUMBER(10)                      NULL,
  EMP_OFF_CODE      NUMBER(10)                      NULL,
  EMP_FULL_CODE     NUMBER(10)                      NULL,
  EMP_VOL_CODE      NUMBER(10)                      NULL,
  EMP_PART_CODE     NUMBER(10)                      NULL,
  EMP_LEA_CODE      NUMBER(10)                      NULL,
  LEA_COMP_TEXT     VARCHAR2(50 BYTE)               NULL,
  CLT_COMP_TEXT     VARCHAR2(50 BYTE)               NULL,
  RTW_TIME          VARCHAR2(6 BYTE)                NULL,
  FOUR_DAY_CODE     NUMBER(10)                      NULL,
  RTW_PLAN_CODE     NUMBER(10)                      NULL,
  RTW_LITE_CODE     NUMBER(10)                      NULL,
  RTW_WAGE_AMT      NUMBER                          NULL,
  RTW_HOUR_NUM      NUMBER                          NULL,
  INC_REPORT_CODE   NUMBER(10)                      NULL,
  OLD_INJURY_CODE   NUMBER(10)                      NULL,
  ASSIGNED_DATE     VARCHAR2(8 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WC_WY_SUPP
(
  CLAIM_ID             NUMBER(10)                   NULL,
  EMP_RACE_CODE        NUMBER(10)                   NULL,
  DOI_PAID_CODE        NUMBER(10)                   NULL,
  CARRIER_NO_TEXT      VARCHAR2(50 BYTE)            NULL,
  FULL_SALARY_CODE     NUMBER(10)                   NULL,
  OFF_PARTNER_CODE     NUMBER(10)                   NULL,
  PRIOR_INJ_CODE       NUMBER(10)                   NULL,
  PERS_NOTIF_EID       NUMBER(10)                   NULL,
  WAGE_CLASS_TEXT      VARCHAR2(20 BYTE)            NULL,
  WRK_ANOTHER_CODE     NUMBER(10)                   NULL,
  UNEM_NUM_TEXT        VARCHAR2(20 BYTE)            NULL,
  WRKCMP_ACCT_TEXT     VARCHAR2(15 BYTE)            NULL,
  WRKR_STATUS_TEXT     VARCHAR2(25 BYTE)            NULL,
  COVERAGE_TEXT        VARCHAR2(25 BYTE)            NULL,
  RPTED_TITLE_TEXT     VARCHAR2(15 BYTE)            NULL,
  WRK_RELATED_CODE     NUMBER(10)                   NULL,
  WRK_RELATED_MEMO     CLOB                         NULL,
  LOST_TIME_CODE       NUMBER(10)                   NULL,
  PAST_CLAIMS_CODE     NUMBER(10)                   NULL,
  PAST_CLAIMS_MEMO     CLOB                         NULL,
  PRIOR_INJ_MEMO       CLOB                         NULL,
  YRS_EDU_NUM          NUMBER                       NULL,
  WY_INJ_WORKER_CODE   NUMBER(10)                   NULL,
  WY_WKR_COVER_CODE    NUMBER(10)                   NULL,
  WY_INJUR_TYPE_CODE   NUMBER(10)                   NULL,
  EMPLYR_NUM           VARCHAR2(50 BYTE)            NULL,
  EMP_PHY_ADDR         VARCHAR2(50 BYTE)            NULL,
  EMP_PHY_CITY         VARCHAR2(50 BYTE)            NULL,
  EMP_PHY_STATE        NUMBER(10)                   NULL,
  EMP_PHY_ZIP          VARCHAR2(9 BYTE)             NULL,
  HIRE_STATE           NUMBER(10)                   NULL,
  US_CITIZEN           NUMBER(10)                   NULL,
  INS_NUM              VARCHAR2(50 BYTE)            NULL,
  HIGHEST_GRADE        VARCHAR2(50 BYTE)            NULL,
  OT_HRS               VARCHAR2(50 BYTE)            NULL,
  FULL_PAID            NUMBER(10)                   NULL,
  PYNG_JOB             NUMBER(10)                   NULL,
  EMP_TYPE             NUMBER(10)                   NULL,
  FAIL_ACC             NUMBER(10)                   NULL,
  BODY_INJ_PRV         NUMBER(10)                   NULL,
  EXPLAIN_BDY_PRT      CLOB                         NULL,
  PRIOR_INJ_WC         NUMBER(10)                   NULL,
  WHAT_STATE           NUMBER(10)                   NULL,
  DATE_PRIOR_INJ       VARCHAR2(8 BYTE)             NULL,
  DATE_INITIAL_EXAM    VARCHAR2(8 BYTE)             NULL,
  BODY_PART_1          NUMBER(10)                   NULL,
  BODY_SIDE_1          NUMBER(10)                   NULL,
  INJURY_NATURE_1      NUMBER(10)                   NULL,
  SOURCE_INJURY_1      NUMBER(10)                   NULL,
  EVENT_TYPE_1         NUMBER(10)                   NULL,
  ENV_FACTORS_1        NUMBER(10)                   NULL,
  BODY_PART_2          NUMBER(10)                   NULL,
  BODY_SIDE_2          NUMBER(10)                   NULL,
  INJURY_NATURE_2      NUMBER(10)                   NULL,
  SOURCE_INJURY_2      NUMBER(10)                   NULL,
  EVENT_TYPE_2         NUMBER(10)                   NULL,
  ENV_FACTORS_2        NUMBER(10)                   NULL,
  BODY_PART_3          NUMBER(10)                   NULL,
  BODY_SIDE_3          NUMBER(10)                   NULL,
  INJURY_NATURE_3      NUMBER(10)                   NULL,
  SOURCE_INJURY_3      NUMBER(10)                   NULL,
  EVENT_TYPE_3         NUMBER(10)                   NULL,
  ENV_FACTORS_3        NUMBER(10)                   NULL,
  BODY_PART_4          NUMBER(10)                   NULL,
  BODY_SIDE_4          NUMBER(10)                   NULL,
  INJURY_NATURE_4      NUMBER(10)                   NULL,
  SOURCE_INJURY_4      NUMBER(10)                   NULL,
  EVENT_TYPE_4         NUMBER(10)                   NULL,
  ENV_FACTORS_4        NUMBER(10)                   NULL,
  BODY_PART_5          NUMBER(10)                   NULL,
  BODY_SIDE_5          NUMBER(10)                   NULL,
  INJURY_NATURE_5      NUMBER(10)                   NULL,
  SOURCE_INJURY_5      NUMBER(10)                   NULL,
  EVENT_TYPE_5         NUMBER(10)                   NULL,
  ENV_FACTORS_5        NUMBER(10)                   NULL,
  EMP_RELATION         VARCHAR2(50 BYTE)            NULL,
  INJ_WORK_RELATED     NUMBER(10)                   NULL,
  PYMNT_TEMP_DISABLTY  NUMBER(10)                   NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_AUTO_ACT
(
  AUTO_ID        NUMBER(10)                         NULL,
  ACTIVITY_CODE  NUMBER(10)                         NULL,
  ACTIVITY_TEXT  VARCHAR2(50 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_AUTO_FILTER
(
  AUTO_ID       NUMBER(10)                          NULL,
  FILTER_INDEX  NUMBER(10)                          NULL,
  FILTER_DATA   CLOB                                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_AUTO_SET
(
  PROCESS_DATE        VARCHAR2(8 BYTE)              NULL,
  AUTO_ROW_ID         NUMBER(10)                    NULL,
  AUTO_NAME           VARCHAR2(255 BYTE)            NULL,
  DEF_INDEX           NUMBER(10)                    NULL,
  DEF_NAME            VARCHAR2(255 BYTE)            NULL,
  SEND_CREATED        NUMBER(5)                     NULL,
  SEND_UPDATED        NUMBER(5)                     NULL,
  SEND_ADJUSTER       NUMBER(5)                     NULL,
  NOTIFICATION_DAYS   NUMBER(10)                    NULL,
  ROUTE_DAYS          NUMBER(10)                    NULL,
  PRIORITY            NUMBER(10)                    NULL,
  TASK_ESTIMATE       NUMBER(10)                    NULL,
  TASK_EST_TYPE       NUMBER(10)                    NULL,
  EXPORT_SCHEDULE     NUMBER(10)                    NULL,
  LAST_RUN            VARCHAR2(8 BYTE)              NULL,
  TIME_BILLABLE_FLAG  NUMBER(5)                     NULL,
  BEST_PRACT_ID       NUMBER(10)                    NULL,
  INSTRUCTIONS        CLOB                          NULL,
  DUE_DAYS            NUMBER(10)                    NULL,
  SEND_SUPPFIELD      NUMBER(5)                     NULL,
  SEND_SUPPFIELDNAME  VARCHAR2(50 BYTE)             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_AUTO_USER
(
  AUTO_ID    NUMBER(10)                             NULL,
  USER_ID    NUMBER(10)                             NULL,
  USER_TYPE  NUMBER(10)                             NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_DIARY_ACT
(
  ACTIVITY_ID      NUMBER(10)                       NULL,
  PARENT_ENTRY_ID  NUMBER                           NULL,
  ACT_CODE         NUMBER(10)                       NULL,
  ACT_TEXT         VARCHAR2(50 BYTE)                NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WPA_DIARY_ENTRY
(
  ATT_FORM_CODE    NUMBER(10)                       NULL,
  ATT_FORM_SCRTY   NUMBER(10)                       NULL,
  ATT_PARENT_CODE  NUMBER(10)                       NULL,
  ATT_SEC_REC_ID   NUMBER(10)                       NULL,
  AUTO_ID          NUMBER(10)                       NULL,
  DIARY_DELETED    NUMBER(5)                        NULL,
  DIARY_VOID       NUMBER(5)                        NULL,
  ESTIMATE_TIME    NUMBER                           NULL,
  NOTIFY_FLAG      NUMBER(5)                        NULL,
  ROUTE_FLAG       NUMBER(5)                        NULL,
  ENTRY_ID         NUMBER(10)                       NULL,
  ENTRY_NAME       VARCHAR2(50 BYTE)                NULL,
  ENTRY_NOTES      VARCHAR2(200 BYTE)               NULL,
  CREATE_DATE      VARCHAR2(14 BYTE)                NULL,
  COMPLETE_TIME    VARCHAR2(6 BYTE)                 NULL,
  COMPLETE_DATE    VARCHAR2(8 BYTE)                 NULL,
  PRIORITY         NUMBER(10)                       NULL,
  STATUS_OPEN      NUMBER(5)                        NULL,
  AUTO_CONFIRM     NUMBER(5)                        NULL,
  RESPONSE_DATE    VARCHAR2(8 BYTE)                 NULL,
  RESPONSE         VARCHAR2(200 BYTE)               NULL,
  ASSIGNED_USER    VARCHAR2(8 BYTE)                 NULL,
  ASSIGNING_USER   VARCHAR2(8 BYTE)                 NULL,
  ASSIGNED_GROUP   VARCHAR2(10 BYTE)                NULL,
  IS_ATTACHED      NUMBER(5)                        NULL,
  ATTACH_TABLE     VARCHAR2(18 BYTE)                NULL,
  ATTACH_RECORDID  NUMBER(10)                       NULL,
  REGARDING        VARCHAR2(70 BYTE)                NULL,
  TE_TRACKED       NUMBER(5)                        NULL,
  TE_START_TIME    VARCHAR2(6 BYTE)                 NULL,
  TE_END_TIME      VARCHAR2(6 BYTE)                 NULL,
  TE_TOTAL_HOURS   NUMBER                           NULL,
  TE_EXPENSES      NUMBER                           NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WRCMP_CPT
(
  TABLE_ID        NUMBER(10)                        NULL,
  CPT             VARCHAR2(6 BYTE)                  NULL,
  CPT_DESC        VARCHAR2(80 BYTE)                 NULL,
  AMOUNT          NUMBER                            NULL,
  PROF_COMPONENT  NUMBER                            NULL,
  ANESTHESIA_VU   NUMBER                            NULL,
  ANES_BR         VARCHAR2(2 BYTE)                  NULL,
  ANES_NA         VARCHAR2(2 BYTE)                  NULL,
  TOS             VARCHAR2(2 BYTE)                  NULL,
  POS             VARCHAR2(2 BYTE)                  NULL,
  SPECIALTY       VARCHAR2(2 BYTE)                  NULL,
  FOLLUP          NUMBER(10)                        NULL,
  SUBCODE         VARCHAR2(3 BYTE)                  NULL,
  BYREPORT        VARCHAR2(2 BYTE)                  NULL,
  STARRED         VARCHAR2(1 BYTE)                  NULL,
  QUESTION        VARCHAR2(1 BYTE)                  NULL,
  ASSISTSURG      VARCHAR2(1 BYTE)                  NULL,
  NOTCOVERED      VARCHAR2(2 BYTE)                  NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;


CREATE TABLE WRCMP_FACTOR
(
  TABLE_ID           NUMBER(10)                     NULL,
  CLINICAL_CAT_CODE  NUMBER(10)                     NULL,
  BEGPROC            VARCHAR2(7 BYTE)               NULL,
  ENDPROC            VARCHAR2(7 BYTE)               NULL,
  TOS                VARCHAR2(2 BYTE)               NULL,
  POS                VARCHAR2(2 BYTE)               NULL,
  SPECIALTY          VARCHAR2(2 BYTE)               NULL,
  FACTOR             NUMBER                         NULL
)
TABLESPACE RISK_TBS
LOGGING 
NOCACHE
NOPARALLEL
NOMONITORING;



